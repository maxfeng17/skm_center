﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Reflection;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebApi.Core.OAuth;
using LunchKingSite.WebLib;
using LunchKingSite.WebApi.Core.Attributes;
using System.Threading.Tasks;
using LunchKingSite.BizLogic.Models.Ppon;
using Helper = LunchKingSite.Core.Helper;

namespace LunchKingSite.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [RequireHttps(false)]
    [MvcStyleBinding]//不要再使用MvcStyleBinding
    public class PponDealServiceController : BaseController
    {
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();

        private static string baseVerion = "5.9.0"; //App 大改版起始版號

        /// <summary>
        /// 頻道檔次列表
        /// </summary>
        [HttpPost]
        [OAuthScope(TokenScope.Deal)]
        public ApiResult PponDealSynopsesGetList(int channelId, int areaId, string categories, int sortBy, string filters,
            string userId, bool getAll = false, int startIndex = 0, string appVersion = "")
        {
            bool isNewAppVersion = ApiSystemManager.IsNewAppVersion(appVersion, baseVerion, baseVerion, userId);

            ISerializer serializer = ProviderFactory.Instance().GetSerializer();
            int totalCount = 0;
            List<PponDealSynopsisBasic> dealBasicList = new List<PponDealSynopsisBasic>();
            List<PponDealSynopsis> dealList;

            //檢查傳入的參數
            List<int> categoryCriteria = serializer.Deserialize<List<int>>(categories);
            if (categoryCriteria == null)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = "參數錯誤"
                };
            }

            if (!Enum.IsDefined(typeof(CategorySortType), sortBy))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = "參數錯誤"
                };
            }

            //確認篩選條件
            List<int> filterList = serializer.Deserialize<List<int>>(filters);
            if (filterList == null)
            {
                filterList = new List<int>();
            }
            int? area = null;
            if (areaId > 0)
            {
                area = areaId;
            }

            bool IsExcludeFamily = true;
            if (channelId == PponDealPreviewManager._INSTANT_BUY_CATEGORY_ID)
            {
                //即買即用
                filterList.Add(137); //Categories:137 即買即用 filter篩選==Categories
                dealList = PponDealApiManager.PponDealSynopsesGetList(CategoryManager.Default.PponDeal.CategoryId
                    , area, categoryCriteria, (CategorySortType)sortBy, filterList, ref totalCount, true, true, 0, IsExcludeFamily);
                dealList = dealList.Where(x => x.DealTags.Contains((int)DealLabelSystemCode.CanBeUsedImmediately)).ToList();
            }
            else if (channelId == PponDealPreviewManager._MAIN_THEME_CATEGORY_ID)
            {
                dealList = PponDealApiManager.Get24hDealSynopses(categoryCriteria, getAll, startIndex, ref totalCount);
            }
            else
            {
                //一般頻道
                dealList = PponDealApiManager.PponDealSynopsesGetList(channelId, area, categoryCriteria, (CategorySortType)sortBy, filterList,
                    ref totalCount, true, getAll, startIndex, IsExcludeFamily);
            }

            //APP 大改版後，新版本使用新的class，變成原本的檔次列表要搬好幾次....
            //快取 List<MultipleMainDealPreview> → 舊版列表 List<PponDealSynopsis> → 新版列表 List<PponDealSynopsisBasic> 
            if (isNewAppVersion)
            {
                foreach (PponDealSynopsis synopsis in dealList)
                {
                    var pponDealSynopsisBasic = PponDealApiManager.PponDealSynopsisToBasic(synopsis);
                    dealBasicList.Add(pponDealSynopsisBasic);
                }
            }

            ApiResult result = new ApiResult();
            result.Code = ApiResultCode.Success;
            if (isNewAppVersion)
            {
                result.Data = new { DealList = dealBasicList, TotalCount = totalCount };
            }
            else
            {
                result.Data = new { DealList = dealList, TotalCount = totalCount };
            }

            SetApiLog(GetMethodName(MethodBase.GetCurrentMethod().Name), userId,
                new { channelId, areaId, categories, sortBy, filters, userId, appVersion },
                new ApiResult
                {
                    Code = result.Code,
                    Message = result.Message,
                    Data = null
                }, LunchKingSite.Core.Helper.GetClientIP());
            return result;
        }

        /// <summary>
        /// APP首頁檔次列表
        /// </summary>
        [HttpPost]
        [OAuthScope(TokenScope.Deal)]
        public ApiResult NewPponDealSynopsesGetDefaultList(int areaId, string userId, bool getAll, int startIndex, string appVersion)
        {
            var dealList = new List<PponDealSynopsisBasic>();
            int totalCount = 0;

            if (areaId == 0)
            {
                areaId = PponCityGroup.DefaultPponCityGroup.TaipeiCity.CategoryId;
            }

            dealList = config.IsDefaultPageSettingByCache
                ? PponDealApiManager.GetTodayChoicePponDealSynopsesByCache(areaId)
                : PponDealApiManager.GetTodayChoicePponDealSynopses(areaId);
            totalCount = dealList.Count;
            dealList = PponDealApiManager.PponDealSynopsisBasicSkipSelect(dealList, getAll, startIndex);

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = new { DealList = dealList, TotalCount = totalCount }
            };
        }

        /// <summary>
        /// 以BID查詢檔次資料
        /// </summary>
        [HttpPost]
        [OAuthScope(TokenScope.Deal)]
        public ApiResult PponDealGetByBidJSon(string bid, string imageSize, string userId, string appVersion)
        {
            ApiResult result = new ApiResult();

            Guid bidToGuid;
            if (Guid.TryParse(bid, out bidToGuid))
            {
                LunchKingSite.Core.Helper.SetContextItem(LkSiteContextItem.Bid, bidToGuid);
            }
            var apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser != null)
            {
                LunchKingSite.Core.Helper.SetContextItem(LkSiteContextItem._DEVICE_TYPE, apiUser.UserDeviceType);
            }

            Guid bGuid;
            ApiImageSize imgSize;

            if (!Guid.TryParse(bid, out bGuid) || !Enum.TryParse(imageSize, out imgSize))
            {
                result.Code = ApiResultCode.Error;
                result.Message = "錯誤的帳號";
                return result;
            }


            ApiPponDeal rtnObj = PponDealApiManager.PponDealForApiGet(bGuid, imgSize, true);
            ApiPponDealBasic apiPponDealSynopsisDetail = PponDealApiManager.ApiPponDealToApiPponDealSynopsisDetail(rtnObj);
            if (rtnObj != null)
            {
                //紀錄使用Log
                var context = HttpContext.Current;
                HttpCookie rsrcCookie = context.Request.Cookies[LkSiteCookie.ReferrerSourceId.ToString()];
                HttpCookie ReferenceId = context.Request.Cookies[LkSiteCookie.ReferenceId.ToString()];
                Guid? visitorIdentity = CookieManager.GetVisitorIdentity();
                string userIp = LunchKingSite.Core.Helper.GetClientIP(true);
                OrderFromType deviceType = LunchKingSite.Core.Helper.GetOrderFromType();
                Task.Run(() => TrackFacade.RecordAppTrack(
                    bGuid, context.User.Identity.Name, "", ReferenceId == null ? string.Empty : ReferenceId.Value, context.Request.UserAgent, rsrcCookie == null ? string.Empty : rsrcCookie.Value, visitorIdentity,
                    userIp, deviceType));

                //可以使用折價券才載入策展
                if (rtnObj.DiscountCodeUsed)
                {
                    //商品相關策展
                    apiPponDealSynopsisDetail.RelatedCurations = PponDealApiManager.GetCurationRelayPageByBid(apiPponDealSynopsisDetail.DealListInfo.Bid);
                }

                //Top Banner
                string bannerfilePath = LunchKingSite.Core.Helper.MapPath("~/Images/17P/active/coupon/number.jpg");
                FileInfo fiBanner = new FileInfo(bannerfilePath);
                if (fiBanner.Exists && ViewPponDealManager.DefaultManager.BaseCostGrossMarginGet(bGuid).BaseGrossMargin * 100 >= config.DiscountCodeMinGrossMarginSet)
                {
                    apiPponDealSynopsisDetail.TopBanner = LunchKingSite.Core.Helper.CombineUrl(
                        config.SSLSiteUrl,
                        string.Format("/Images/17P/active/coupon/number.jpg?v={0}", fiBanner.LastWriteTime.Ticks));
                }
                else
                {
                    apiPponDealSynopsisDetail.TopBanner = string.Empty;
                }

                result.Code = ApiResultCode.Success;
                result.Data = apiPponDealSynopsisDetail;
            }
            else
            {
                result.Code = ApiResultCode.DataNotFound;
                return result;
            }

            SetApiLog(GetMethodName(MethodBase.GetCurrentMethod().Name), userId, new { bid, userId, imageSize }, result, LunchKingSite.Core.Helper.GetClientIP());

            return result;
        }

        /// <summary>
        /// 熱門搜尋 
        /// </summary>
        /// <param name="isAll">是否取全部熱搜</param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Deal)]
        public ApiResult GetHotTags(bool isAll)
        {
            var take = 12;
            List<AppHotTag> hotTags = isAll
                ? PromotionFacade.GetAppPromoSearchKeys()
                : PromotionFacade.GetAppPromoSearchKeys(take); //config.AppHotSearchCount

            return new ApiResult { Code = ApiResultCode.Success, Data = hotTags };
        }

        /// <summary>
        /// 主題企劃
        /// 指行銷Banner，排除非DeepLink，保留「頻道/單檔/策展/策展列表」
        /// </summary>
        /// <param name="isAll">是否取全部主題企劃</param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Deal)]
        public ApiResult GetAppRandomParagraph(bool isAll)
        {
            int take = 4;

            ViewCmsRandomCollection vcrCol = CmsRandomFacade.GetOrAddData("/ppon/default.aspx_pponnews", RandomCmsType.PponRandomCms);
            List<AppRandomParagraph> arpList = new List<AppRandomParagraph>();

            DateTime nowDateTime = DateTime.Now;
            var vcrBodyCol = vcrCol.Where(x => x.StartTime < nowDateTime && nowDateTime < x.EndTime)
                .Select(x => x.Body).Distinct().OrderBy(t => Guid.NewGuid()).ToList();

            foreach (var vcrBody in vcrBodyCol)
            {
                var navigatorUrl = Helper.RegularToGetHtmlAttribute(vcrBody, "href").ToLower();
                if (IsMatchDeepLink(navigatorUrl))
                {
                    arpList.Add(new AppRandomParagraph
                    {
                        PicUrl = Helper.RegularToGetHtmlAttribute(vcrBody, "src"),
                        NavigatorUrl = navigatorUrl
                    });
                }
                if (!isAll && arpList.Count == take)
                {
                    break;
                }
            }

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = new
                {
                    TotalCount = vcrBodyCol.Count, //總筆數
                    MatchDeepLinkCount = arpList.Count, //符合DeepLink筆數
                    AppRandomParagraph = arpList
                }
            };
        }


        /// <summary>
        /// 相關推薦/猜你喜歡
        /// </summary>
        /// <param name="bid"></param>
        /// <param name="userId"></param>
        /// <param name="type">0: 相關推薦 1: 猜你喜歡</param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Deal)]
        public ApiResult GetRecommendDealListByBid(string bid, string userId, int type = 0)
        {
            ApiResult result = new ApiResult();
            Guid bGuid;

            if (!Guid.TryParse(bid, out bGuid))
            {
                result.Code = ApiResultCode.Error;
                result.Message = I18N.Phrase.ApiReturnCodeInputError;
            }

            var dealList = new List<PponDealSynopsisBasic>();
            if (type == 0)
            {
                //推薦檔次 數量規格是 1 or 2 or 4 筆 最多4筆
                int recommendDealsCount = 4;
                int logicType;
                dealList = PponDealApiManager.GetPponDealSynopsisBasicRecommendDeal(bGuid, recommendDealsCount, out logicType);
                result.Message += string.Format("({0})", logicType);
            }
            else if (type == 1)
            {
                //猜你喜歡，暫以推薦檔次代替
                int recommendDealsCount = 12;
                int logicType = 0;
                dealList = new List<PponDealSynopsisBasic>();
                //PponDealApiManager.GetPponDealSynopsisBasicRecommendDeal(bGuid, recommendDealsCount, out logicType);
                result.Message += string.Format("({0})", logicType);
            }

            result.Data = new { DealList = dealList, TotalCount = dealList.Count };
            return result;
        }

        /// <summary>
        /// 只取全家咖啡寄杯檔次，目前在MGM當禮物盒為空時，會推薦
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="count">筆數</param>
        /// <param name="appVersion"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Deal)]
        public ApiResult GetRecomendFamiCoffeeDeals(string userId, int count, string appVersion = "")
        {
            bool isNewAppVersion = ApiSystemManager.IsNewAppVersion(appVersion, baseVerion, baseVerion, userId);
            var filterList = new List<int>();
            int sortBy = 0;
            bool getAll = true;
            int totalCount = 0;
            int channelId = CategoryManager.Default.Family.CategoryId; //hard code 
            bool isExcludeFamily = false;
            int startIndex = 0;
            int area = 0;
            List<int> categoryCriteria = new List<int>();

            categoryCriteria.Add(CategoryManager.Default.Family.CategoryId);

            //取全家專區檔次
            var dealList = PponDealApiManager.PponDealSynopsesGetList(channelId, area, categoryCriteria, (CategorySortType)sortBy, filterList,
                    ref totalCount, true, getAll, startIndex, isExcludeFamily).Where(x => x.Price > 0).Take(count).ToList(); //過濾零元檔

            List<PponDealSynopsisBasic> dealBasicList = new List<PponDealSynopsisBasic>();
            if (isNewAppVersion)
            {
                foreach (var dealtmp in dealList)
                {
                    var pponDealSynopsisBasic = PponDealApiManager.PponDealSynopsisToBasic(dealtmp);
                    dealBasicList.Add(pponDealSynopsisBasic);
                }
            }

            ApiResult result = new ApiResult();
            result.Code = ApiResultCode.Success;
            if (isNewAppVersion)
            {
                result.Data = new { DealList = dealBasicList, TotalCount = totalCount };
            }
            else
            {
                result.Data = new { DealList = dealList, TotalCount = totalCount };
            }

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = dealList
            };
        }

        /// <summary>
        /// 依據檔次bid回傳檔次的推薦分享連結，有登入情況下回傳分享送紅利的連結。
        /// </summary>
        [HttpPost]
        [Authorize]
        [MemberValidate(true, "userId")]
        public ApiResult GetDealPromoUrl(string bid, string userId, string userName = null)
        {
            ApiResult result = new ApiResult() { Code = ApiResultCode.Success };
            var jsonS = new JsonSerializer();
            string returnLog;

            Guid bGuid;

            if (!Guid.TryParse(bid, out bGuid))
            {
                result.Code = ApiResultCode.Error;
                result.Message = I18N.Phrase.ApiReturnCodeInputError;
                returnLog = "TryParse bid 錯誤!";
            }
            else
            {
                //檢查使用者身分
                int uId;
                if (int.TryParse(userName, out uId) == false || string.IsNullOrEmpty(userName))
                {
                    userName = HttpContext.Current.User.Identity.Name;
                    //已登入，取得登入會員編號
                    uId = MemberFacade.GetUniqueId(userName);
                }

                string url;

                ApiDealPromoUrl promoUrl = new ApiDealPromoUrl();
                promoUrl.UrlType = PponDealApiManager.TryGetPponDealPromoUrl(bGuid, uId, out url);

                string origUrl = url;
                if (promoUrl.UrlType == PponDealPromoUrlType.BonusFeedback)
                {
                    //分享送紅利連結，轉換為短網址
                    url = WebUtility.RequestShortUrl(url);
                    if (uId != 0)
                    {
                        PromotionFacade.SetDiscountReferrerUrl(origUrl, url, uId, userName, bGuid, 0);
                    }
                }
                promoUrl.Url = url;
                //填入回傳直
                result.Data = promoUrl;

                returnLog = jsonS.Serialize(new { UserId = uId, OrigUrl = origUrl, PromoUrl = promoUrl });
            }

            SetApiLog(GetMethodName(MethodBase.GetCurrentMethod().Name), userId,
                    jsonS.Serialize(new { Bid = bid, UserName = userName }),
                    returnLog,
                    LunchKingSite.Core.Helper.GetClientIP());
            return result;
        }

        /// <summary>
        /// 關鍵字搜尋
        /// </summary>
        [HttpGet, HttpPost, RequireHttps]
        public ApiResult PponSearch(string queryString, int clickRecordId = 0, string userId = null, string appVersion = null)
        {
            var result = new ApiResult();
            if (clickRecordId != 0)
            {
                userId = User == null ? "" : User.Identity.Id.ToString();
                ApiAppManager.SetSearchKeyRecord(clickRecordId, userId, "from api");
            }

            //暫時以有帶userId、appVersion來區分新舊版 --- 阿吉
            bool isNewPrototype = !string.IsNullOrEmpty(userId) && !string.IsNullOrEmpty(appVersion);
            var datas = PponDealApiManager.GetAppSearchResult(queryString, isNewPrototype);
            result.Data = datas;

            string userName = HttpContext.Current.User.Identity.Name;
            PponFacade.AddPponSearchLog(queryString, datas.ResultCount, string.Empty, userName);

            return result;
        }

        /// <summary>
        /// 給外部搜尋外部服務，提供需要的檢索的內容
        /// </summary>
        [HttpGet, RequireHttps]
        [InternalOnly]
        public ApiResult GetPponSearchDeals()
        {
            List<IViewPponDeal> deals;
            if (config.EnabledSearchExcludeSKMChannel)
            {
                deals = ViewPponDealManager.DefaultManager.ViewPponDealGetList(true, true)
                    .Where(t => t.SellerCityId != PponCityGroup.DefaultPponCityGroup.Skm.CityId)
                    .ToList();
            }
            else
            {
                deals = ViewPponDealManager.DefaultManager.ViewPponDealGetList(true, true);
            }

            List<ViewPponDealSeachModel> data = new List<ViewPponDealSeachModel>();
            foreach (var deal in deals)
            {
                data.Add(new ViewPponDealSeachModel(deal));
            }
            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = data
            };
        }

        /// <summary>
        /// CpaTracking
        /// </summary>
        /// <param name="cpaKey"></param>
        /// <param name="rsrc"></param>
        /// <param name="referrerUrl"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [ApiUserValidator]
        [HttpGet, HttpPost]
        public ApiResult CpaTracking(Guid? cpaKey, string rsrc, string referrerUrl, string userId)
        {
            if (string.IsNullOrEmpty(rsrc))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }

            if (cpaKey == null)
            {
                cpaKey = Guid.NewGuid();
            }
            MarketingFacade.TryAddCpaTracking(cpaKey.ToString(), rsrc, User.Identity.Id, referrerUrl);

            SetApiLog(GetMethodName("CpaTracking"), userId,
                new { cpaKey, rsrc, referrerUrl, userId },
                new { cpaKey }, Helper.GetClientIP());

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = cpaKey
            };
        }

        #region private method

        /// <summary>
        /// 是否符合DeepLink規則
        /// </summary>
        /// <param name="navigatorUrl"></param>
        /// <returns></returns>
        private bool IsMatchDeepLink(string navigatorUrl)
        {
            if (navigatorUrl.Contains("/channel/")) return true;
            if (navigatorUrl.Contains("/deal/")) return true;

            if (navigatorUrl.Contains("/event/brandevent")) return true;
            if (navigatorUrl.Contains("/event/brandevent.aspx")) return true;
            if (navigatorUrl.Contains("/event/brandmobile")) return true;
            if (navigatorUrl.Contains("/event/brandmobile.aspx")) return true;

            if (navigatorUrl.Contains("/event/exhibitionlist")) return true;
            if (navigatorUrl.Contains("/event/exhibitionlist.aspx")) return true;
            if (navigatorUrl.Contains("/event/exhibitionlistmobile")) return true;
            if (navigatorUrl.Contains("/event/exhibitionlistmobile.aspx")) return true;
            if (navigatorUrl.Contains("/event/exhibitionlistapp")) return true;
            if (navigatorUrl.Contains("/event/exhibitionlistapp.aspx")) return true;

            return false;
        }

        #endregion

    }

    /// <summary>
    /// 
    /// </summary>
    [RoutePrefix("api/PponDealService")]
    public class PponDealService2Controller : BaseController
    {
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        /// <summary>
        /// APP首頁24H推薦區塊
        /// 2020年後改成提供 美食 玩美 旅遊 6小時內的熱銷檔次
        /// </summary>
        /// <param name="areaId"></param>
        /// <param name="userId"></param>
        /// <param name="getAll"></param>
        /// <param name="startIndex"></param>
        /// <param name="appVersion"></param>
        /// <returns></returns>

        [HttpPost]
        [OAuthScope(TokenScope.Deal)]
        [Route("GetChannel24HHotDeals")]
        [Route("GetHotDeals")]
        public ApiResult GetHotDeals([FromBody]APIRequestModel model)
        {
            List<PponDealSynopsisBasic> dealBasicList = PponFacade2.GetChannelFoodBeautyTravelHotDeals();

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = new { DealList = dealBasicList, TotalCount = dealBasicList.Count }
            };
        }
        /// <summary>
        /// 
        /// </summary>
        public class APIRequestModel
        {
            public string UserId { get; set; }

            public string AppVersion { get; set; }
        }
    }
}

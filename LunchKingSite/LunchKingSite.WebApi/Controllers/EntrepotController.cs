﻿using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.WebApi.Controllers.Base;
using LunchKingSite.WebApi.Core.Attributes;
using LunchKingSite.WebApi.Core.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace LunchKingSite.WebApi.Controllers
{
    [RequireHttps]
    [ForceCamelCase]
    public class EntrepotController : ApiControllerBase
    {

        /// <summary>
        /// 測試
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [OAuthScope(TokenScope.Entrepot)]
        public ApiResult GetVersion ()
        {
            return new ApiResult
            {
                Code = ApiResultCode.Success
            };
        }
    }
}

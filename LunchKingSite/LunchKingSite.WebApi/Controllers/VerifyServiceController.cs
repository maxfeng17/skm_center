﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

using System.Web;
using System.Web.Http;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.BizLogic.Model.Sales;
using LunchKingSite.BizLogic.Model.VBS;
using LunchKingSite.BizLogic.Models.API;
using LunchKingSite.BizLogic.Models.Verification;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebApi.Core;
using LunchKingSite.WebApi.Core.Attributes;

namespace LunchKingSite.WebApi.Controllers
{
    /// <summary>
    /// 核銷API
    /// </summary>
    [RequireHttps]
    public class VerifyServiceController : BaseController
    {
        private IMemberProvider _memberProvider = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private ISellerProvider _sellerProvider = ProviderFactory.Instance().GetProvider<ISellerProvider>();

        /// <summary>
        /// 解析QRCode作用API
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ApiUserValidator]
        public ApiResult GetQrActionPokeball(QrActionPokeballModel model)
        {
            var userName = HttpContext.Current.User.Identity.Name;
            if (string.IsNullOrWhiteSpace(userName))
            {
                return new ApiResult { Code = ApiResultCode.UserNoSignIn };
            }

            var uniqueId = MemberFacade.GetUniqueId(userName);

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = QrCodeFacade.GetQrActionPokeball(model.Code, uniqueId)
            };
        }

        /// <summary>
        /// 以分店查詢會員訂單資料
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ApiUserValidator]
        public ApiResult GetMemberOrdersByVerifyStore(MemberOrdersByVerifyStoreModel model)
        {
            string userName = User.Identity.Name;
            if (string.IsNullOrWhiteSpace(userName))
            {
                return new ApiResult { Code = ApiResultCode.UserNoSignIn };
            }

            if (model.VerifyStoreGuid == Guid.Empty)
            {
                return new ApiResult { Code = ApiResultCode.InputError };
            }

            List<ApiOrderByVerifyStore> orderList = new List<ApiOrderByVerifyStore>();
            Guid? rootSellerGuid = _sellerProvider.SellerTreeGetRootSellerGuidBySellerGuid(model.VerifyStoreGuid);
            if (rootSellerGuid != null)
            {
                //check 是否所分店都可核銷?
                orderList.AddRange(PponDealApiManager.GetMemberOrdersBySellerGuid(User.Identity.Id, (Guid)rootSellerGuid));
                //送禮訂單
                orderList.AddRange(MGMFacade.GetGiftOrdersBySellerGuid(User.Identity.Id, (Guid)rootSellerGuid));
            }

            return new ApiResult { Code = ApiResultCode.Success, Data = orderList.OrderByDescending(x => x.UseEndDate).ToList() };
        }

        /// <summary>
        /// 以分店查詢檔次列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ApiUserValidator]
        public ApiResult GetDealListByVerifyStore(MemberOrdersByVerifyStoreModel model)
        {
            List<PponDealSynopsisBasic> data = new List<PponDealSynopsisBasic>();
            Guid? rootSellerGuid = _sellerProvider.SellerTreeGetRootSellerGuidBySellerGuid(model.VerifyStoreGuid);
            if (rootSellerGuid != null)
            {
                data = PponDealApiManager.GetAppDealsBySellerGuid((Guid)rootSellerGuid);
            }

            return new ApiResult { Code = ApiResultCode.Success, Data = data };
        }

        /// <summary>
        /// 複合核銷，支援三種憑證(一般單檔憑證、成套憑證、廠商序號憑證)核銷
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ApiUserValidator]
        public ApiResult ComplexVerifyCoupon(ComplexVerifyCouponModel model)
        {
            var result = new ApiResult();

            #region check

            var userName = User.Identity.Name;
            if (string.IsNullOrWhiteSpace(userName))
            {
                return new ApiResult { Code = ApiResultCode.UserNoSignIn };
            }

            //取出核銷分店guid
            QrActionCode qrActionCode = QrCodeFacade.GetQrActionAndCode(model.VerifyStoreCode);
            if (qrActionCode.Action != QrCodeActionType.VerifyStore || qrActionCode.Code == null)
            {
                result.Code = ApiResultCode.InputError;
                SetApiLog(GetMethodName(MethodBase.GetCurrentMethod().Name), model.UserId, model, result, Helper.GetClientIP());
                return result;
            }

            //取得可核銷CashTrustLog
            var ctlList = _memberProvider.CashTrustLogListGetByOrderId(model.OrderGuid).Where(x => x.UsageVerifiedTime == null).ToList();
            if (!ctlList.Any())
            {
                result.Code = ApiResultCode.InputError;
                SetApiLog(GetMethodName(MethodBase.GetCurrentMethod().Name), model.UserId, model, result, Helper.GetClientIP());
                return result;
            }
            
            var firstCtl = ctlList.FirstOrDefault();
            if (firstCtl == null || firstCtl.BusinessHourGuid == null)
            {
                result.Code = ApiResultCode.InputError;
                SetApiLog(GetMethodName(MethodBase.GetCurrentMethod().Name), model.UserId, model, result, Helper.GetClientIP());
                return result;
            }
            IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid((Guid)firstCtl.BusinessHourGuid);

            //check 支援反核銷
            if (vpd.VerifyActionType != (int)VerifyActionType.Reverse)
            {
                result.Code = ApiResultCode.NotSupportThisWayToVerify;
                SetApiLog(GetMethodName(MethodBase.GetCurrentMethod().Name), model.UserId, model, result, Helper.GetClientIP());
                return result;
            }
            
            //check store
            Guid storeGuid = qrActionCode.Code;
            VbsMembership memOriginal = SellerFacade.GetVbsMemberUserIdByStoreGuid(storeGuid);
            if (memOriginal == null)
            {
                result.Code = ApiResultCode.PcpStoreNotFound;
                SetApiLog(GetMethodName(MethodBase.GetCurrentMethod().Name), model.UserId,
                    new { storeGuid = qrActionCode.Code, model }, result, Helper.GetClientIP());
                return result;
            }

            #endregion
            
            List<Guid> trustIdList;
            var verifyDataList = new List<VerifyCode>();
            if (vpd.GroupCouponDealType == (int)GroupCouponDealType.None)
            {
                //一般單筆/多筆核銷

                #region Quantity Check

                //依核銷數量取得Coupon，只能取到自己的
                var selfCtlList = VBSFacade.GetSelfCashTrustLog(User.Identity.Id, ctlList);
                if (model.VerifyCount > selfCtlList.Count)
                {
                    result.Code = ApiResultCode.CouponQtyNotEnough;
                    SetApiLog(GetMethodName(MethodBase.GetCurrentMethod().Name), model.UserId,
                           new { storeGuid = qrActionCode.Code, model }, result, Helper.GetClientIP());
                    return result;
                }

                #endregion

                verifyDataList.AddRange(
                    selfCtlList.Take(model.VerifyCount).ToList().Select(verifyCoupon => new VerifyCode
                    {
                        CouponSerial = verifyCoupon.CouponSequenceNumber,
                        CouponCode = verifyCoupon.CouponId.ToString(),
                        TrustId = verifyCoupon.TrustId
                    }));
            }
            else 
            {
                //成套核銷

                #region Quantity Check
                
                var quantityCheckResult = VBSFacade.CheckCouponGroupTrustIdQuantity(User.Identity.Id, model.OrderGuid, model.VerifyCount, out trustIdList);
                if (quantityCheckResult != GroupCouponVerifyCheckType.Success)
                {
                    switch (quantityCheckResult)
                    {
                        case GroupCouponVerifyCheckType.QtyNotEnough:
                            result.Message = "可核銷數量不足";
                            break;
                        case GroupCouponVerifyCheckType.CouponNotEnough:
                            result.Message = "需使用完一般憑證才可核銷贈品憑證";
                            break;
                        case GroupCouponVerifyCheckType.GiveawayCouponNotEnouth:
                            result.Message = "可核銷數量不足";
                            break;
                    }

                    result.Code = ApiResultCode.CouponQtyNotEnough;
                    SetApiLog(GetMethodName("VerifyCouponGroup"), model.UserId, model, result, Helper.GetClientIP());
                    return result;
                }

                #endregion
                
                verifyDataList.AddRange(
                    trustIdList.Select(trustId => new VerifyCode
                    {
                        CouponSerial = string.Empty,
                        CouponCode = string.Empty,
                        TrustId = trustId
                    }));
            }

            #region Verify Check

            var verifyCheckResult = VBSFacade.VerifyCheckInfo(memOriginal, verifyDataList);
            if (verifyCheckResult.Item1 != ApiResultCode.Success)
            {
                result.Code = verifyCheckResult.Item1;
                result.Data = verifyCheckResult.Item2;
                result.Message = verifyCheckResult.Item3;
                return result;
            }

            #endregion

            #region Verify

            trustIdList = verifyCheckResult.Item2.Select(x => new Guid(x.TrustId)).ToList();
            MultiCouponVerifyResult verifyResultDetail;
            var timestamp = DateTime.Now.Ticks.ToString();
            var verifyResult = VBSFacade.MultiCouponVerify(memOriginal, trustIdList, model.OrderGuid, timestamp, Helper.GetClientIP(), out verifyResultDetail);
            var couponResult = new VerifyCouponResultModel();
            switch (verifyResult)
            {
                case ApiResultCode.CouponPartialFail:
                    result.Code = ApiResultCode.CouponPartialFail;
                    result.Message = "此批憑證部份核銷失敗";
                    break;
                case ApiResultCode.CouponUsed:
                    result.Code = ApiResultCode.CouponUsed;
                    result.Message = "code 已使用過，請消費者重新選擇兌換數量，再生成 QR code";
                    break;
                default:
                    result.Code = ApiResultCode.Success;
                    result.Message = "核銷成功，是否繼續核銷?";

                    var reloadCtl = _memberProvider.CashTrustLogGet(trustIdList.FirstOrDefault());
                    var usedDate = reloadCtl.UsageVerifiedTime.ToString();
                    var verifyStore = reloadCtl.VerifiedStoreGuid == null
                        ? string.Empty : _sellerProvider.SellerGet((Guid)reloadCtl.VerifiedStoreGuid).SellerName;
                    var image = ImageFacade.GetMediaPathsFromRawData(vpd.EventImagePath, MediaType.PponDealPhoto).Where((x, i) => i == 0).ToList();

                    couponResult.VerifyCount = trustIdList.Count;
                    couponResult.Unit = "份";
                    couponResult.UsedDate = usedDate;
                    couponResult.VerifyStore = verifyStore;
                    couponResult.ImageUrl = image[0];
                    couponResult.VerifyResultDetail = verifyResultDetail;
                    break;
            }
            result.Data = couponResult;
            SetApiLog(GetMethodName("VerifyCoupon"), model.UserId, new { storeGuid = qrActionCode.Code, model }, result, Helper.GetClientIP());
            return result;

            #endregion
        }
        


        /// <summary>
        /// [測試輔助] SellerGuid 轉 Code
        /// </summary>
        /// <param name="verifyStore"></param>
        /// <returns></returns>
        [TestServerOnly]
        public ApiResult GetVerifyStoreCode(Guid verifyStore)
        {
            return new ApiResult { Code = ApiResultCode.Success, Data = string.Format("1|{0}", QrCodeFacade.EncryptVerifyStore(verifyStore)) };
        }

        /// <summary>
        /// [測試輔助] 以分店guid取得禮物
        /// </summary>
        /// <returns></returns>
        public ApiResult GetGiftsByVerifyStore(Guid sellerGuid)
        {
            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = MGMFacade.GetGiftOrdersBySellerGuid(User.Identity.Id, sellerGuid)
            };
        }
    }


    #region input model
    
    /// <summary>
    /// 解析QRCode作用 input Model
    /// </summary>
    public class QrActionPokeballModel : IApiUserInputModel
    {
        /// <summary>
        /// APP UserId
        /// </summary>
        public string UserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Code { get; set; }
    }

    /// <summary>
    /// 反向核銷 input Model
    /// </summary>
    public class ComplexVerifyCouponModel : IApiUserInputModel
    {
        /// <summary>
        /// APP UserId
        /// </summary>
        public string UserId { get; set; }
        /// <summary>
        /// 核銷訂單
        /// </summary>
        public Guid OrderGuid { get; set; }
        /// <summary>
        /// 核銷張數
        /// </summary>
        public int VerifyCount { get; set; }
        /// <summary>
        /// 商家編碼
        /// </summary>
        public string VerifyStoreCode { get; set; }
    }
    
    /// <summary>
    /// 以分店查詢會員訂單資料 input Model
    /// </summary>
    public class MemberOrdersByVerifyStoreModel : IApiUserInputModel
    {
        /// <summary>
        /// AppId
        /// </summary>
        public string UserId { get; set; }
        /// <summary>
        /// 分店Guid
        /// </summary>
        public Guid VerifyStoreGuid { get; set; }
    }

    #endregion
}

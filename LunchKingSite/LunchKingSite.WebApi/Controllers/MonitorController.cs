﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.BizLogic.Models;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.WebApi.Core.OAuth;
using LunchKingSite.WebLib.Component;
using Newtonsoft.Json;

namespace LunchKingSite.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class MonitorController : ApiController
    {
        private static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        /// <summary>
        /// 取得檔次排序
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.SystemManagement)]
        public ApiResult GetDealTimeSlotByBid(PreviewModel model)
        {
            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = ViewPponDealManager.DefaultManager.DealTimeSlotGetByBidAndTime(model.Bid, DateTime.Now)
            };
        }

        /// <summary>
        /// 取得檔次內容
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.SystemManagement)]
        public ApiResult GetMultipleMainDealPreview(PreviewModel model)
        {
            PponDealListCondition condition = new PponDealListCondition(model.Url);
            string cacheKey = condition.GetCacheKey(CategorySortType.Default);
            List<MultipleMainDealPreview> multideals = MemoryCache2.Get<List<MultipleMainDealPreview>>(cacheKey);
            if (multideals == null)
            {
                multideals = PponFacade.GetDealListByCondition(condition);
                multideals = ViewPponDealManager.DefaultManager.SortMultipleMainDealPreview(multideals, CategorySortType.Default).ToList();
                MemoryCache2.Set(cacheKey, multideals, 1);
            }

            multideals = multideals.Where(x => x.PponDeal.BusinessHourGuid == model.Bid).ToList();

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = multideals
            };
        }

        /// <summary>
        /// 取得檔次內容
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.SystemManagement)]
        public ApiResult Debug(CategoryBidDebugModel model)
        {
            string s = ViewPponDealManager.DefaultManager.GetDebugData(model.Bid);

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = new
                {
                    ViewPponDealManager.RoundNo,
                    s
                }
            };
        }

        /// <summary>
        /// 取得檔次內容
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.SystemManagement)]
        public ApiResult GetViewPponDeal(Guid bid)
        {
            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid)
            };
        }

        /// <summary>
        /// 清快取
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.SystemManagement)]
        public ApiResult ClearDealCache(Guid bid)
        {
            ViewPponDealManager.DefaultManager.ClearViewPponDealCache(bid);
            return new ApiResult
            {
                Code = ApiResultCode.Success
            };
        }

        /// <summary>
        /// 取得檔次內容
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.SystemManagement)]
        public ApiResult GetViewPponStore(Guid bid)
        {
            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = pp.ViewPponStoreGetListByBidWithNoLock(bid, VbsRightFlag.VerifyShop)
            };
        }

        #region Model

        /// <summary>
        /// 
        /// </summary>
        public class PreviewModel
        {
            /// <summary>
            /// 
            /// </summary>
            public string Url { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public Guid Bid { get; set; }
        }

        public class CategoryBidDebugModel
        {
            /// <summary>
            /// 
            /// </summary>
            public Guid Bid { get; set; }
        }

        #endregion
    }
}

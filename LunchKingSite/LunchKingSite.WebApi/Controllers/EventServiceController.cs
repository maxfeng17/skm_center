﻿using System;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Models;
using LunchKingSite.WebApi.Core;
using LunchKingSite.WebApi.Core.OAuth;
using LunchKingSite.WebApi.Core.Attributes;
using LunchKingSite.WebApi.Models;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Models.API;

namespace LunchKingSite.WebApi.Controllers
{
    /// <summary>
    /// 商品主題活動/策展1.0/策展2.0
    /// </summary>
    public class EventServiceController : BaseController
    {
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        /// <summary>
        /// 取得蓋版廣告
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Deal)]
        public ApiResult GetEventNews()
        {
            try
            {
                ApiEventNews eventNews = EventFacade.GetCurrentApiEventNews();
                if (eventNews == null)
                {
                    return new ApiResult
                    {
                        Code = ApiResultCode.DataNotFound
                    };
                }
                return new ApiResult
                {
                    Code = ApiResultCode.Success,
                    Data = eventNews
                };
            }
            catch
            {
                return new ApiResult
                {
                    Code = ApiResultCode.Error
                };
            }
        }

        /// <summary>
        /// 取得首頁活動資訊(新版)(8個圓形按鈕上方)
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Deal)]
        public ApiResult GetMainPageCurations(GetMainPageCurationsModel data)
        {
            List<ApiCurationInfo> curationInfos = MarketingFacade.GetApiCurationsByDefaultBanner();

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = curationInfos
            };
        }

        /// <summary>
        /// 取得首頁策展列表資訊(8個圓形按鈕底下)
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Deal)]
        public ApiResult GetMainPageCurationEntryInfo(GetMainPageCurationsModel data)
        {
            List<CurationEntryInfo> curationInfos = PromotionFacade.GetAppNowCurationBanner();

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = curationInfos
            };
        }

        /// <summary>
        /// 取得頻道活動資訊(新版)
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Deal)]
        public ApiResult GetCurationsByChannel(GetCurationInfoByChannelInputModel data)
        {
            var isAndroid = data.UserId.IndexOf("AND", StringComparison.OrdinalIgnoreCase) >= 0;
            List<ApiCurationInfo> curationInfos = MarketingFacade.GetApiCurationsByChannel(data.ChannelId, data.AreaId, isAndroid);

            var appVersion = CookieManager.GetAppOsVersion();
            if (config.EnableCurationLimitCountInApp && (int)appVersion >= (int)AppOsVersion.Android70)
            {
                curationInfos = curationInfos.Take(config.CurationCountInApp).ToList();
            }

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = curationInfos
            };
        }

        /// <summary>
        /// 依活動編號取得活動資訊(新版)
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        [ApiUserValidator]
        [OAuthScope(TokenScope.Deal)]
        public ApiResult GetCurationByEventId(GetEventInfoByEventIdInputModel data)
        {
            var result = new ApiResult { Code = ApiResultCode.Success };

            var isFromCache = false;
            ApiCurationInfo curationInfo = MarketingFacade.GetCurationInfoOldToNew(data.Type, data.EventId, isFromCache);
            
            if (curationInfo != null && curationInfo.TotalCount > 0) //有匯入檔次才show
            {
                result.Data = curationInfo;
                return result;
            }
            else
            {
                result.Data = null;
                return result;
            }
        }

        /// <summary>
        /// 取得活動檔次列表(舊版程式，新版續用)
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        [ApiUserValidator]
        [RequireHttps]
        public ApiResult GetEventPromoDealList(GetEventPromoDealListInputModel data)
        {
            ApiPagedOption pagedOption = new ApiPagedOption
            {
                GetAll = data.GetAll.GetValueOrDefault(true),
                PageSize = 50,
                StartIndex = data.StartIndex.GetValueOrDefault()
            };

            int id = data.EventId; //id 可能是 EventId, BrandId，視data.Type而定

            ApiPagedEventPromoDeaList pagedPromoDeals = MarketingFacade.GetApiEventPromoDealList(
                data.Type, id, data.Category, data.SubCategory, pagedOption, data.Reload.GetValueOrDefault());

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = pagedPromoDeals
            };
        }

        /// <summary>
        /// 取得策展折價券列表
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        [RequireHttps(true)]
        [OAuthScope(TokenScope.Deal)]
        public ApiResult GetCurationDiscountList(GetEventDiscountModel data)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.UserNoSignIn,
                    Message = I18N.Phrase.ApiReturnCodeUserNoSignIn
                };
            }
            int? userId = null;
            string userName = HttpContext.Current.User.Identity.Name;
            if (!string.IsNullOrWhiteSpace(userName))
            {
                userId = MemberFacade.GetUniqueId(userName);
            }

            var campaigns = PromotionFacade.GetCurationDiscountCampaignList(data.EventId, userId);

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = campaigns
            };
        }

        /// <summary>
        /// 商品策展中繼頁
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        [RequireHttps(true)]
        [OAuthScope(TokenScope.Deal)]
        public ApiResult GetCurationRelayPageInfo(GetEventDiscountModel data)
        {            
            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.UserNoSignIn,
                    Message = I18N.Phrase.ApiReturnCodeUserNoSignIn
                };
            }
            int? userId = null;
            string userName = HttpContext.Current.User.Identity.Name;
            if (!string.IsNullOrWhiteSpace(userName))
            {
                userId = MemberFacade.GetUniqueId(userName);
            }

            var relayPageInfo = new CurationRelayPageInfo();
            relayPageInfo.CurationDiscountList = PromotionFacade.GetCurationDiscountCampaignList(data.EventId, userId);

            var eventInfo = MarketingFacade.GetApiEventPromoOrBrandByIdFromDB(data.EventId, data.Type);
            if (eventInfo != null)
            {
                relayPageInfo.EventDescription = eventInfo.Description ?? string.Empty;
            }


            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = relayPageInfo
            };
        }

        /// <summary>
        /// 領取策展折價券
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        [RequireHttps(true)]
        [OAuthScope(TokenScope.Deal)]
        public ApiResult GetCurationDiscountCode(GetCurationDiscountCodeModel data)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.UserNoSignIn,
                    Message = I18N.Phrase.ApiReturnCodeUserNoSignIn
                };
            }
            var userName = HttpContext.Current.User.Identity.Name;
            if (string.IsNullOrWhiteSpace(userName))
            {
                return new ApiResult { Code = ApiResultCode.UserNoSignIn };
            }

            var userId = MemberFacade.GetUniqueId(userName);

            string code;
            int amount;
            DateTime? endTime;
            int discountCodeId;
            int minimumAmount;
            
            var resultCode = PromotionFacade.InstantGenerateDiscountCode(data.DiscountId, userId, true,
                out code, out amount, out endTime, out discountCodeId, out minimumAmount);

            var result = new ApiResult();
            result.Code = resultCode == InstantGenerateDiscountCampaignResult.Success
                    ? ApiResultCode.Success
                    : ApiResultCode.Error;

            ApiGetDiscountCodeResult dataResultCata;
            switch (resultCode)
            {
                case InstantGenerateDiscountCampaignResult.Success:
                    dataResultCata = ApiGetDiscountCodeResult.Success;
                    break;
                case InstantGenerateDiscountCampaignResult.AlreadySent:
                    dataResultCata = ApiGetDiscountCodeResult.Received;
                    break;
                case InstantGenerateDiscountCampaignResult.Exceed:
                    dataResultCata = ApiGetDiscountCodeResult.Complete;
                    break;
                default:
                    dataResultCata = ApiGetDiscountCodeResult.Faild;
                    break;
            }

            result.Data = new GetCurationDiscountCodeResult { ResultCode = dataResultCata };

            SetApiLog(GetMethodName(MethodBase.GetCurrentMethod().Name), string.Empty, 
                new { data.DiscountId, UserName = userName }, new { ResultCode = resultCode }, LunchKingSite.Core.Helper.GetClientIP());
            
            return result;
        }

        /// <summary>
        /// 取得主題策展列表
        /// </summary>
        /// <returns></returns>
        public ApiResult GetCurationsByTheme()
        {
            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = PromotionFacade.GetCurationsByTheme()
            };
        }

        /// <summary>
        /// 重設個人策展折價券
        /// </summary>
        /// <param name="type"></param>
        /// <param name="eventId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        [TestServerOnly]
        public ApiResult ResetDiscount(int type, int eventId, int userId)
        {
            var updateCount = PromotionFacade.ResetCurationDiscountByUser(type, eventId, userId);
            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = new { UpdateCount = updateCount }
            };
        }


        /// <summary>
        /// 重設快取
        /// </summary>
        /// <param name="cacheKey"></param>
        /// <returns></returns>
        [HttpGet]
        [TestServerOnly]
        public ApiResult ResetApiEventPromo(string cacheKey)
        {
            var result = new ApiResult();
            result.Code = ApiResultCode.Success;
            result.Data = SystemFacade.ClearCacheByObjectNameAndCacheKey("ApiEventPromo", cacheKey);
            return result;
        }


        #region 舊版

        /// <summary>
        /// 依活動編號取得活動資訊(舊版)
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        [ApiUserValidator]
        [RequireHttps]
        public ApiResult GetEventInfoByEventId(GetEventInfoByEventIdInputModel data)
        {
            var result = new ApiResult {Code = ApiResultCode.Success};

            ApiEventPromo eventInfo;
            if (data.Reload || config.MemoryCache2Mode == 0)
            {
                eventInfo = MarketingFacade.GetApiEventPromoByIdFromDB(data.EventId);
            }
            else
            {
                eventInfo = MarketingFacade.GetApiEventPromoOrBrandByIdFromDB(data.EventId, data.Type);
            }

            if (eventInfo != null && eventInfo.TotalCount > 0) //有匯入檔次才show
            {
                result.Data = eventInfo;
                return result;
            }
            else
            {
                result.Data = null;
                return result;
            }
        }

        /// <summary>
        /// 依頻道取得活動資訊(舊版)
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        [ApiUserValidator]
        [RequireHttps]
        public ApiResult GetCurationInfoByChannel(GetCurationInfoByChannelInputModel data)
        {
            List<ApiEventPromo> eventInfos = MarketingFacade.GetApiEventPromoListByChannel(data.ChannelId, data.AreaId, false);

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = eventInfos
            };
        }

        /// <summary>
        /// 取得頻道活動Banner資訊(舊版)
        /// </summary>
        /// <returns></returns>
        [RequireHttps(false)]
        [HttpPost]
        [ApiUserValidator]
        [OAuthScope(TokenScope.Deal)]
        public ApiResult GetAppBannerByChannel(GetAppBannerByChannelInputModel data)
        {
            List<ApiPromoEvent> eventPromoCollection = ApiPromoEventManager.ApiPromoEventGetList(false);
            List<ShowEventPromo> showEventPromoList = new List<ShowEventPromo>();

            foreach (ApiPromoEvent promoEvent in eventPromoCollection.Where(x => string.IsNullOrEmpty(x.PromoImage) == false))
            {
                ShowEventPromo showEventPromo = new ShowEventPromo();
                ShowEventPromoPokeball promoPokeball = new ShowEventPromoPokeball();
                showEventPromo.BannerImage = ImageFacade.GetMediaPath(promoEvent.PromoImage, MediaType.EventPromoAppMainImage);
                promoPokeball.EventPromoId = promoEvent.EventId;
                showEventPromo.PokeballList.Add(promoPokeball);

                showEventPromoList.Add(showEventPromo);
            }

            if (config.PokeBallBannerStart < DateTime.Now && config.PokeBallBannerEnd >= DateTime.Now)
            {
                ShowEventPromo showEventPromo = new ShowEventPromo();
                showEventPromo.BannerImage = config.PokeBallBannerImage;
                showEventPromo.PokeballList.Add(new PushCustomUrlPokeball { Url = config.PokeBallBannerUrl });

                showEventPromoList.Add(showEventPromo);
            }

            ApiResult result = new ApiResult();
            if (showEventPromoList.Count > 0)
            {
                result.Code = ApiResultCode.Success;
                result.Data = showEventPromoList;
            }
            else
            {
                result.Code = ApiResultCode.DataNotFound;
                result.Message = I18N.Phrase.ApiReturnCodeDataNotFound;
            }

            return result;
        }

        #endregion

        #region 限時優惠

        /// <summary>
        /// 限時優惠
        /// </summary>
        /// <param name="model">是否取全部熱搜</param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Deal)]
        public ApiResult RushBuy(RushBuyInputModel model)
        {
            var result = new ApiResult();
            if (model == null || model.IsAll)
            {
                result.Data = PponDealApiManager.GetLimitedTimeSelectionResult();                
            }
            else
            {
                result.Data = PponDealApiManager.GetLimitedTimeSelectionResult(12);
            }
            result.Code = ApiResultCode.Success;

            return result;
        }

        #endregion

    }
}

﻿using System.Linq;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebApi.Core;
using LunchKingSite.WebApi.Core.OAuth;
using LunchKingSite.WebLib.Component;
using Newtonsoft.Json;
using Helper = LunchKingSite.Core.Helper;

namespace LunchKingSite.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class PponController : BaseController
    {
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        [HttpGet]
        [OAuthScope(TokenScope.Deal)]
        public List<DealInfoTranslate> GetAllDeals(string accessToken)
        {
            try
            {
                string cpaCode = ""; // todo: 可改為由取得token時一併取得cpa Code
                List<DealInfoTranslate> deals = PponFacade.DealInfoTranslateGetList(cpaCode, DateTime.Today);
                ApiReturnObject rtnObject = new ApiReturnObject();
                rtnObject.Code = ApiReturnCode.Success;
                rtnObject.Data = string.Format("資料筆數共{0}筆", deals.Count);
                SetApiLog("GetAllDeals", AppId, new { AccessToken = accessToken }, rtnObject, Helper.GetClientIP());
                return deals;
            }
            catch (Exception ex)
            {
                ApiReturnObject rtnObject = new ApiReturnObject();
                rtnObject.Code = ApiReturnCode.InputError;
                rtnObject.Data = ex;
                rtnObject.Message = "執行階段發生錯誤";
                SetApiLog("GetAllDeals", AppId, new { AccessToken = accessToken }, rtnObject, Helper.GetClientIP());
                throw new HttpErrorResponseException("B001", "執行階段發生錯誤");
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [OAuthScope(TokenScope.Deal)]
        public DealInfoTranslate GetDealById(string accessToken, string id)
        {
            try
            {
                Guid businessHourGuid;
                if (Guid.TryParse(id, out businessHourGuid))
                {
                    string cpaCode = ""; // todo: 可改為由取得token時一併取得cpa Code
                    DealInfoTranslate deal = PponFacade.DealInfoTranslateGet(businessHourGuid, cpaCode);
                    ApiReturnObject rtnObject = new ApiReturnObject();
                    rtnObject.Code = ApiReturnCode.Success;
                    rtnObject.Data = new { DealId = deal.DealId, Title = deal.Title, DealDesc = deal.Description };
                    SetApiLog("GetDealById", AppId, new { AccessToken = accessToken, Id = id }, rtnObject, Helper.GetClientIP());
                    return deal;
                }
                else
                {
                    throw new HttpErrorResponseException("C001", "參數檢核失敗");
                }
            }
            catch (Exception ex)
            {
                ApiReturnObject rtnObject = new ApiReturnObject();
                rtnObject.Code = ApiReturnCode.InputError;
                rtnObject.Data = ex;
                rtnObject.Message = "執行階段發生錯誤";
                SetApiLog("GetDealById", AppId, new { AccessToken = accessToken, Id = id }, rtnObject, Helper.GetClientIP());
                throw new HttpErrorResponseException("B001", "執行階段發生錯誤");
            }
        }

        /// <summary>
        /// 取得 團購資料 使用的頻道資料及各頻道之下的各種分類資料
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ApiVersionResult PponChannelCategoryData(string apiVersion = null, string userId = null)
        {
            ApiCategoryTypeNode typeNode = PponDealPreviewManager.GetPponChannelCategoryTree();

            if (typeNode == null)
            {
                return new ApiVersionResult
                {
                    Code = ApiResultCode.DataNotFound,
                    Message = I18N.Phrase.ApiResultCodeDataNotFound
                };
            }
            ApiVersionResult result = new ApiVersionResult
            {
                Version = CategoryManager.PponChannelCategoryVersion,
                Data = typeNode
            };
            return result;
        }

        /// <summary>
        /// 以關鍵字搜尋檔次
        /// </summary>
        /// <param name="querystring"></param>
        /// <param name="max">要顯示的筆數(已不使用，統一由config設定)</param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage PponIndexSearch(string querystring, int max)
        {
            string querttrans;
            List<IViewPponDeal> deals = SearchFacade.GetPponMainDealsByQueryString(querystring, 0, out querttrans);
            List<string> bids = deals.Select(t => t.BusinessHourGuid.ToString()).ToList();
            PponFacade.AddPponSearchLog(querystring, bids.Count, string.Empty, User.Identity.Name);
            if (bids.Count > 0)
            {
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JsonConvert.SerializeObject(bids), Encoding.UTF8, "application/json")
                };
            }
            else
            {
                return new HttpResponseMessage()
                {
                    Content = new StringContent("[]", Encoding.UTF8, "application/json")
                };
            }
        }
        /// <summary>
        /// 以關鍵字搜尋檔次
        /// </summary>
        /// <param name="querystring"></param>
        /// <param name="max">要顯示的筆數(已不使用，統一由config設定)</param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage PponIndexSearchTest(string querystring, int max)
        {
            string querttrans;
            List<IViewPponDeal> deals = SearchFacade.GetPponMainDealsByQueryString(querystring, null, out querttrans);
            List<string> bids = deals.Select(t => t.BusinessHourGuid.ToString()).ToList();
            if (bids.Count > 0)
            {
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JsonConvert.SerializeObject(new { Bid = bids, QueryTrans = querttrans }), Encoding.UTF8, "application/json")
                };
            }
            else
            {
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JsonConvert.SerializeObject(new { Bid = new string[] { }, QueryTrans = querttrans }), Encoding.UTF8, "application/json")
                };
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage PponCityGet()
        {
            List<PponCity> excludedCities = new List<PponCity>() { PponCityGroup.DefaultPponCityGroup.PEZ, PponCityGroup.DefaultPponCityGroup.Piinlife,
                PponCityGroup.DefaultPponCityGroup.Tmall,PponCityGroup.DefaultPponCityGroup.ArriveIn24Hrs,PponCityGroup.DefaultPponCityGroup.ArriveIn72Hrs,PponCityGroup.DefaultPponCityGroup.NewTaipeiCity };
            return new HttpResponseMessage()
            {
                Content = new StringContent(JsonConvert.SerializeObject(PponCityGroup.DefaultPponCityGroup.Except(excludedCities)), Encoding.UTF8, "application/json")
            };
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage CategoryDependencyGet()
        {
            return new HttpResponseMessage()
            {
                Content = new StringContent(JsonConvert.SerializeObject(CategoryManager.CategoryDependencyGetAll().Where(x => x.CategoryStatus == 1)), Encoding.UTF8, "application/json")
            };
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="channel_id"></param>
        /// <param name="channel_area_id"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage PponDealGet(int channel_id, int? channel_area_id = null)
        {
            PponCity city = PponCityGroup.GetPponCityByChannel(channel_id, channel_area_id);
            List<TodayDealCityCategorySynopsis> smalldeals = new List<TodayDealCityCategorySynopsis>();
            if (city != null)
            {
                List<MultipleMainDealPreview> deals = ViewPponDealManager.DefaultManager.MultipleMainDealPreviewGetListByChannel(city.ChannelId.Value, city.ChannelAreaId, null);
                smalldeals = deals.Select(x => new TodayDealCityCategorySynopsis()
                {
                    Deal = new TodayDealSynopsis(x.PponDeal)
                    {
                        DealIcon = PponFacade.GetDealIconHtmlContent(x.PponDeal, 2),
                        DealPromoImageTag = (string.IsNullOrEmpty(x.PponDeal.DealPromoImage)) ? string.Empty : string.Format(@"<img src='{0}' alt='' />"
                                            , ImageFacade.GetMediaPath(x.PponDeal.DealPromoImage, MediaType.DealPromoImage)),
                        EventImageTag = ImageFacade.GetMediaPathsFromRawData(x.PponDeal.EventImagePath, MediaType.PponDealPhoto).DefaultIfEmpty(string.Empty).First(),
                        CityId = city.CityId,
                        ChannelId = channel_id,
                        ChannelAreaId = channel_area_id,
                        Seq = x.Sequence,
                        SubstringTitle = CommonFacade.GetStringByByteLength(x.PponDeal.EventTitle, 114, "...")
                        //SubstringName = GetTravelPlace(x.BusinessHourGuid, x),
                        //CountDownString = GetCountdown(x.BusinessHourOrderTimeE, DateTime.Now),
                        //OrderedQuantityShow = OrderedQuantityHelper.Show(x, OrderedQuantityHelper.ShowType.TodayDeals)
                    },
                    CategoryIds = x.DealCategoryIdList
                }).ToList();
            }
            return new HttpResponseMessage()
            {
                Content = new StringContent(JsonConvert.SerializeObject(smalldeals), Encoding.UTF8, "application/json")
            };
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        [HttpGet]
        public ApiResult ReSendAuthMail(string email)
        {
            ApiResult result = new ApiResult();

            string mail = HttpUtility.UrlDecode(email);
            IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            Member m = mp.MemberGet(mail);
            if (m.IsLoaded)
            {
                bool isFrom17Life = mp.MemberLinkGetByExternalId(m.UniqueId.ToString()).ExternalOrg == (int)SingleSignOnSource.ContactDigitalIntegration;
                bool isAccountActive = (m.Status & (int)MemberStatusFlag.Active17Life) > 0;
                if (isFrom17Life && isAccountActive)
                {
                    result.Code = ApiResultCode.Success;
                    result.Message = "您的帳號已經認證囉！可直接登入使用！";
                }
                else
                {
                    try
                    {
                        string[] authPair = NewMemberUtility.GenEmailAuthCode();
                        MemberAuthInfo mai = mp.MemberAuthInfoGet(m.UniqueId);
                        mai.AuthKey = authPair[0];
                        mai.AuthCode = authPair[1];
                        mai.AuthDate = DateTime.Now;

                        mp.MemberAuthInfoSet(mai);

                        MemberFacade.SendAccountConfirmMail(m.UserEmail, m.UniqueId.ToString(), mai.AuthKey, mai.AuthCode);
                    }
                    catch
                    {
                        result.Code = ApiResultCode.Error;
                        result.Message = "喔喔！好像有什麼東西出錯了！";
                        return result;
                    }
                    result.Code = ApiResultCode.Success;
                    result.Message = "17Life已將認證信寄到您的信箱中，請您前往查看並盡速認證，謝謝！";
                }
            }
            else
            {
                result.Code = ApiResultCode.Success;
                result.Message = "您輸入的信箱並無紀錄，請您填寫曾訂購過的信箱，或是直接註冊會員喔！";
            }
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetHotSearchKey()
        {
            var result = new ApiResult() { Code = ApiResultCode.Success };

            result.Data = ApiAppManager.GetSearchKeyList((int)PromoSearchKeyUseMode.APP);


            return new HttpResponseMessage()
            {
                Content = new StringContent(JsonConvert.SerializeObject(result), Encoding.UTF8, "application/json")
            };
        }
    }
}

﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using LunchKingSite.WebApi.Core;
using LunchKingSite.WebApi.Core.OAuth;
using Helper = LunchKingSite.Core.Helper;

namespace LunchKingSite.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class IChannelController : BaseController
    {
        ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        /// <summary>
        /// 
        /// </summary>
        public class GetOrderModel
        {
            /// <summary>
            /// 
            /// </summary>
            public DateTime start_time { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public DateTime end_time { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string token { get; set; }
        }
        /// <summary>
        /// 
        /// </summary>
        public class TestModel
        {
            /// <summary>
            /// 
            /// </summary>
            public string mcode { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string gid { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string oid { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string amount { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string bid { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string date { get; set; }
        }

        /// <summary>
        /// 提供給iChannel查詢訂單API
        /// http://localhost/api/ichannel/GetOrder?accessToken=a9ec607dee30492ca5380b98405a7b88_00380d86417c3b4671fe5078f9c73723&amp;start_time=2015-04-07&amp;end_time=2015-04-08
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpGet]
        [OAuthScope(TokenScope.Deal)]
        public HttpResponseMessage GetOrder([FromUri]GetOrderModel model)
        {
            try
            {
                string result = string.Empty;
                var vic = OrderFacade.GetViewIChannelInfoByOrderTimePeriod(model.start_time, model.end_time);
                vic = vic.Where(x => OrderFacade.IchannelCheckPass(x));
                foreach (var ico in vic.GroupBy(x => new { OrderGuid = x.OrderGuid, DeliveryType = x.DeliveryType, OrderId = x.OrderId, OrderTime = x.OrderTime, Gid = x.Gid, OrderStatus = x.OrderStatus, Bid = x.Bid, ItemName = x.ItemName }, y => y))
                {
                    if (ico.Key.DeliveryType == (int)DeliveryType.ToShop)
                    {
                        #region 憑證 (因憑證狀態不同，以多筆訂單型態送出)

                        var returnCode = OrderFacade.GetCommissionRuleReturnCode(ChannelSource.iChannel, DeliveryType.ToShop, ico.Key.Bid, ico.Key.OrderTime);

                        foreach (var item in ico)
                        {
                            result += ico.Key.OrderTime.ToString("yyyy-MM-dd HH:mm:ss")
                                + "," + ico.Key.Gid
                                + "," + ico.Key.OrderId + "-" + item.CouponId
                                + "," + ((item.CashTrustLogStatus.EqualsAny((int)TrustStatus.Refunded, (int)TrustStatus.Returned)) ? (int)IChannelInfoStatus.Invalid : ((item.CashTrustLogStatus.Equals((int)TrustStatus.Verified)) ? (int)IChannelInfoStatus.Valid : (int)IChannelInfoStatus.Unconfirmed))
                                + "," + item.Amount.ToString("F0")
                                + "," + returnCode
                                + ",,,,,,"
                                + "," + ico.Key.Bid
                                + "," + ico.Key.ItemName + "\n";
                        }

                        #endregion
                    }
                    else if (ico.Key.DeliveryType == (int)DeliveryType.ToHouse)
                    {
                        #region 宅配 (因宅配狀態一致，多筆商品金額以"|"合併資料送出)

                        var returnCode = OrderFacade.GetCommissionRuleReturnCode(ChannelSource.iChannel, DeliveryType.ToHouse, ico.Key.Bid, ico.Key.OrderTime);

                        if (ico.Any(x => x.CashTrustLogStatus < (int)TrustStatus.Returned))
                        {
                            int ichannelStatus = ico.Key.OrderTime.AddDays(config.IChannelRunDay) > DateTime.Now ? (int)IChannelInfoStatus.Unconfirmed : (int)IChannelInfoStatus.Valid;

                            // 尚有未退貨，需將狀態標記為未確認，並刪除已退貨的金額資料
                            result += ico.Key.OrderTime.ToString("yyyy-MM-dd HH:mm:ss")
                            + "," + ico.Key.Gid
                            + "," + ico.Key.OrderId
                            + "," + ichannelStatus
                            + "," + string.Join("|", ico.Where(x => !x.CashTrustLogStatus.EqualsAny((int)TrustStatus.Refunded, (int)TrustStatus.Returned)).Select(x => x.Amount.ToString("F0")))
                            + "," + string.Join("|", ico.Where(x => !x.CashTrustLogStatus.EqualsAny((int)TrustStatus.Refunded, (int)TrustStatus.Returned)).Select(x => returnCode))
                            + ",,,,,,"
                            + "," + ico.Key.Bid
                            + "," + ico.Key.ItemName + "\n";
                        }
                        else
                        {
                            // 已全數退貨，直接將狀態標記為無效，並顯示所有金額資料
                            result += ico.Key.OrderTime.ToString("yyyy-MM-dd HH:mm:ss")
                            + "," + ico.Key.Gid
                            + "," + ico.Key.OrderId
                            + "," + (int)IChannelInfoStatus.Invalid
                            + "," + string.Join("|", ico.Select(x => x.Amount.ToString("F0")))
                            + "," + string.Join("|", ico.Select(x => returnCode))
                            + ",,,,,,"
                            + "," + ico.Key.Bid
                            + "," + ico.Key.ItemName + "\n";
                        }

                        #endregion
                    }
                }
                return new HttpResponseMessage() { Content = new StringContent(result, Encoding.UTF8, "text/html") };
            }
            catch (Exception ex)
            {
                var rtnObject = new ApiReturnObject { Code = ApiReturnCode.Error, Data = ex, Message = "執行階段發生錯誤" };
                SetApiLog("iChannel_GetOrder", AppId, new { AccessToken = model.token }, rtnObject, Helper.GetClientIP());
                throw new HttpErrorResponseException(rtnObject.Code.ToString(), rtnObject.Message);
            }
        }

        /// <summary>
        /// 模擬iChannels S2S API
        /// http://localhost/api/ichannel/GetOrder?accessToken=a9ec607dee30492ca5380b98405a7b88_00380d86417c3b4671fe5078f9c73723&amp;start_time=2015-04-07&amp;end_time=2015-04-08
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpGet]
        public dynamic GetTest([FromUri]TestModel model)
        {
            if (!string.IsNullOrWhiteSpace(model.mcode) && !string.IsNullOrWhiteSpace(model.gid) && !string.IsNullOrWhiteSpace(model.oid) && !string.IsNullOrWhiteSpace(model.amount) && !string.IsNullOrWhiteSpace(model.bid) && !string.IsNullOrWhiteSpace(model.date))
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// 手動執行iChannel API訂單拋送
        /// http://localhost/api/ichannel/iChannelOrderSend?accesstoken=a9ec607dee30492ca5380b98405a7b88_00380d86417c3b4671fe5078f9c73723&amp;dateStart=2015-04-07&amp;dateEnd=2015-04-08
        /// </summary>
        /// <param name="start_time"></param>
        /// <param name="end_time"></param>
        /// <returns></returns>
        [HttpGet]
        [OAuthScope(TokenScope.Deal)]
        public HttpResponseMessage IChannelOrderSend(DateTime start_time, DateTime end_time)
        {
            string result = string.Empty;
            if (config.EnableIChannel)
            {
                 result = OrderFacade.IChannelAPI(start_time, end_time);
                 if (string.IsNullOrWhiteSpace(result))
                 {
                     result = string.Format("沒有{0} ~ {1}之間的iChannel訂單", start_time.ToString("yyyy/MM/dd HH:mm"), end_time.ToString("yyyy/MM/dd HH:mm"));
                 }
            }
            return new HttpResponseMessage() { Content = new StringContent(result, Encoding.UTF8, "text/html") };
        }
    }
}

﻿using System.Linq;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.BizLogic.Model.OAuth;
using LunchKingSite.Core;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Http;
using System.Xml;
using LunchKingSite.BizLogic.Models.Channel;
using LunchKingSite.Core.Component;
using Newtonsoft.Json;
using LunchKingSite.Core.Models;
using LunchKingSite.DataOrm;
using LunchKingSite.WebApi.Core.OAuth;
using LunchKingSite.WebApi.Models.Channel;
using Helper = LunchKingSite.Core.Helper;
using LunchKingSite.BizLogic.Models;
using LunchKingSite.Core.Interface;

namespace LunchKingSite.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class LionTravelController : BaseController
    {
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private static ISystemProvider sp = ProviderFactory.Instance().GetProvider<ISystemProvider>();
        private static IChannelProvider cp = ProviderFactory.Instance().GetProvider<IChannelProvider>();
        private static IDealProvider dp = ProviderFactory.Instance().GetProvider<IDealProvider>();
        private static IFamiportProvider _fami = ProviderFactory.Instance().GetProvider<IFamiportProvider>();
        private static IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        private List<int> ChannelIdList = new List<int>() { 87, 88, 89, 90, 148 };
        private const int GroupCouponCid = 135, GroupCouponChannelId = 87, GroupCouponCategoryType = 5, GroupCouponSequence = 999;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="accesstoken"></param>
        /// <param name="includeDelivery"></param>
        /// <returns></returns>
        [OAuthScope(TokenScope.LionTravel)]
        [HttpGet]
        public HttpResponseMessage CategoryRootList(string accesstoken, bool includeDelivery = false)
        {
            OauthToken oauthToken = CommonFacade.OauthTokenGet(accesstoken);
            OAuthClientModel client = OAuthFacade.GetClient(oauthToken.AppId);
            var channelClientProperty = OAuthFacade.GetChannelClientProperty(client.AppId);

            List<ChannelPponCity> channels = new List<ChannelPponCity> {
                new ChannelPponCity(PponCityGroup.DefaultPponCityGroup.TaipeiCity, CategoryType.PponChannel, 1)
                , new ChannelPponCity(PponCityGroup.DefaultPponCityGroup.Piinlife, CategoryType.PponChannel, 2)
                , new ChannelPponCity(PponCityGroup.DefaultPponCityGroup.Travel, CategoryType.PponChannel, 3)
                , new ChannelPponCity(PponCityGroup.DefaultPponCityGroup.PBeautyLocation, CategoryType.PponChannel, 4) };

            if (includeDelivery)
            {
                channels.Add(new ChannelPponCity(PponCityGroup.DefaultPponCityGroup.AllCountry, CategoryType.PponChannel, 5));
            }

            
            var data = channels.Select(x => new CategoryModel(x)).ToList();

            if (channelClientProperty.IsEnableGroupCoupon) 
            {
                data.Add(new CategoryModel
                {
                    ChannelId = GroupCouponChannelId,
                    CategoryId = GroupCouponCid,
                    CategoryName = "成套票券",
                    CategoryType = GroupCouponCategoryType,
                    Sequence = GroupCouponSequence
                });
            }

            return new HttpResponseMessage()
            {
                Content = new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json")
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="accesstoken"></param>
        /// <param name="includeDelivery"></param>
        /// <returns></returns>
        [OAuthScope(TokenScope.LionTravel)]
        [HttpGet]
        public HttpResponseMessage CategoryList(string accesstoken, bool includeDelivery = false)
        {
            OauthToken oauthToken = CommonFacade.OauthTokenGet(accesstoken);
            OAuthClientModel client = OAuthFacade.GetClient(oauthToken.AppId);
            var channelClientProperty = OAuthFacade.GetChannelClientProperty(client.AppId);

            List<int> channel_ids = new List<int> { 
                PponCityGroup.DefaultPponCityGroup.TaipeiCity.ChannelId.Value
                , PponCityGroup.DefaultPponCityGroup.Piinlife.ChannelId.Value
                , PponCityGroup.DefaultPponCityGroup.Travel.ChannelId.Value
                , PponCityGroup.DefaultPponCityGroup.PBeautyLocation.ChannelId.Value };

            if (includeDelivery)
            {
                channel_ids.Add(PponCityGroup.DefaultPponCityGroup.AllCountry.ChannelId.Value);
            }

            var data = CategoryManager.CategoryDependencyGetAll().Where(x => x.CategoryStatus == 1 && channel_ids.Any(y => y == x.ParentId))
                    .Select(x => new CategoryModel(x)).ToList();

            if (channelClientProperty.IsEnableGroupCoupon)
            {
                data.Add(new CategoryModel
                {
                    ChannelId = GroupCouponChannelId,
                    CategoryId = GroupCouponCid,
                    CategoryName = "成套票券",
                    CategoryType = GroupCouponCategoryType,
                    Sequence = GroupCouponSequence
                });
            }

            return new HttpResponseMessage()
            {
                Content = new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json")
            };
        }

        /// <summary>
        /// 檔次搜尋
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="queryString"></param>
        /// <param name="max"></param>
        /// <param name="deliverytype"></param>
        /// <param name="removeStyle"></param>
        /// <returns></returns>
        [OAuthScope(TokenScope.LionTravel)]
        [HttpGet]
        public HttpResponseMessage DealSearch(string accessToken, string queryString, int max, int? deliverytype = null, bool? removeStyle = false)
        {
            ApiResult result = new ApiResult();

            string querttrans;
            List<IViewPponDeal> deals = SearchFacade.GetPponMainDealsByQueryString(queryString, null, out querttrans).Take(max).ToList();

            if (deals.Count > 0)
            {
                //取最低毛利率的值
                OauthToken oauthToken = CommonFacade.OauthTokenGet(accessToken);
                OAuthClientModel client = OAuthFacade.GetClient(oauthToken.AppId);
                var channelClientProperty = OAuthFacade.GetChannelClientProperty(client.AppId);

                //取得通路商
                AgentChannel channel = ChannelFacade.GetOrderClassificationByHeaderToken();

                if (deliverytype.HasValue)
                {
                    deals = deals.Where(x => (x.DeliveryType ?? 1) == deliverytype).ToList();
                }

                bool isRemoveStyle = removeStyle.HasValue && removeStyle == true;

                List<ChannelPponDeal> queryResult = new List<ChannelPponDeal>();
                foreach (var deal in deals)
                {
                    var pd = new ChannelPponDeal(channel, deal, ChannelIdList, channelClientProperty, config.SiteUrl, isRemoveStyle);
                    queryResult.Add(pd);
                }

                var excludedGuid = ChannelFacade.ChannelExcludedGuid(deals, channelClientProperty, ChannelIdList, (int)channel);
                var content = queryResult.Where(x => !excludedGuid.Contains(x.DealGuid));
                if (content.Any())
                {
                    return new HttpResponseMessage()
                    {
                        Content =
                            new StringContent(JsonConvert.SerializeObject(content), Encoding.UTF8, "application/json")
                    };
                }
            }

            return new HttpResponseMessage()
            {
                Content = new StringContent(JsonConvert.SerializeObject(new { Error = "查無檔次" }), Encoding.UTF8, "application/json")
            };
        }

        /// <summary>
        /// 單一檔次
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="dealguid"></param>
        /// <param name="removeStyle"></param>
        /// <returns></returns>
        [OAuthScope(TokenScope.LionTravel)]
        [HttpGet]
        public HttpResponseMessage Deal(string accessToken, Guid dealguid, bool? removeStyle = false)
        {
            IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(dealguid);
            Dictionary<int, string> sp_labeltaglist = CategoryManager.CategoryDependencyGetAll().Where(x => x.CategoryStatus == 1 && x.CategoryType == (int)CategoryType.DealSpecialCategory && x.ParentType == (int)CategoryType.DeliveryType)
                 .Select(x => new { CategoryId = x.CategoryId, Name = x.CategoryName }).Distinct().ToDictionary(x => x.CategoryId, x => x.Name);

            List<int> category_list = PponDealPreviewManager.GetDealCategoryIdList(dealguid);
            category_list = category_list.Except(ChannelIdList).ToList();
            if (deal.BusinessHourGuid == Guid.Empty)
            {
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JsonConvert.SerializeObject(new { Error = "查無檔次" }), Encoding.UTF8, "application/json")
                };
            }
            else
            {
                OauthToken oauthToken = CommonFacade.OauthTokenGet(accessToken);
                OAuthClientModel client = OAuthFacade.GetClient(oauthToken.AppId);
                var channelClientProperty = OAuthFacade.GetChannelClientProperty(client.AppId);

                //取得通路商
                AgentChannel channel = ChannelFacade.GetOrderClassificationByHeaderToken();

                List<Guid> excludedGuid = ChannelFacade.ChannelExcludedGuid(new List<IViewPponDeal>{ deal }, channelClientProperty, ChannelIdList,(int)channel);
                bool isForcePass = ChannelFacade.IsChannleForcePass(channel, deal);
                if (excludedGuid.Contains(deal.BusinessHourGuid) && !isForcePass)
                {
                    return new HttpResponseMessage()
                    {
                        Content = new StringContent(JsonConvert.SerializeObject(new { Error = "查無檔次" }), Encoding.UTF8, "application/json")
                    };
                }

                bool isRemoveStyle = removeStyle.HasValue && removeStyle == true;

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JsonConvert.SerializeObject(
                        new ChannelPponDeal(channel, deal, ChannelIdList, channelClientProperty, config.SiteUrl, isRemoveStyle)
                        ), Encoding.UTF8, "application/json")
                };
            }
        }

        /// <summary>
        /// 檔次內容
        /// </summary>
        /// <param name="accesstoken"></param>
        /// <param name="channelid"></param>
        /// <param name="areaid"></param>
        /// <param name="categoryid"></param>
        /// <param name="minprice"></param>
        /// <param name="maxprice"></param>
        /// <param name="deliverytype"></param>
        /// <param name="lastChangeTime"></param>
        /// <param name="removeStyle"></param>
        /// <returns></returns>
        [OAuthScope(TokenScope.LionTravel)]
        [HttpGet]
        public HttpResponseMessage DealList(string accesstoken, int channelid, int? areaid = null, string categoryid = null, int? minprice = null, int? maxprice = null, int? deliverytype = null , DateTime ? lastChangeTime=null, bool? removeStyle = false)
        {
            Dictionary<int, string> sp_labeltaglist = CategoryManager.CategoryDependencyGetAll().Where(x => x.CategoryStatus == 1 && x.CategoryType == (int)CategoryType.DealSpecialCategory && x.ParentType == (int)CategoryType.DeliveryType)
                 .Select(x => new { CategoryId = x.CategoryId, Name = x.CategoryName }).Distinct().ToDictionary(x => x.CategoryId, x => x.Name);
            List<int> categoryid_list = new List<int>();

            int category_id;
            if (!string.IsNullOrEmpty(categoryid))
            {
                string[] category_split_list = categoryid.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string item in category_split_list)
                {
                    if (int.TryParse(item, out category_id))
                    {
                        categoryid_list.Add(category_id);
                    }
                }
            }
            List<MultipleMainDealPreview> deals = ViewPponDealManager.DefaultManager.MultipleMainDealPreviewGetListByCategoryCriteria(channelid, areaid, categoryid_list);

            //取最低毛利率的值
            OauthToken oauth_token = CommonFacade.OauthTokenGet(accesstoken);
            OAuthClientModel client = OAuthFacade.GetClient(oauth_token.AppId);
            var channelClientProperty = OAuthFacade.GetChannelClientProperty(client.AppId);

            if (channelClientProperty.IsEnableGroupCoupon && areaid == GroupCouponCid) 
            {
                var bids = dp.GetOnlineGroupCouponBid();
                deals.AddRange(bids.Select(x=> 
                    new MultipleMainDealPreview(87, 0, ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(x), new List<int>()
                    , DealTimeSlotStatus.Default, ViewPponDealManager.DefaultManager.BaseCostGrossMarginGet(x).BaseGrossMargin)));
            }

            bool isRemoveStyle = removeStyle.HasValue && removeStyle == true;

            //大於等於最後異動時間
            if (lastChangeTime.HasValue)
            {
                deals = deals.Where(x => x.PponDeal.EventModifyTime >= lastChangeTime.Value).ToList();
            }
            if (minprice.HasValue)
            {
                deals = deals.Where(x => x.PponDeal.ItemPrice >= minprice.Value).ToList();
            }
            if (maxprice.HasValue)
            {
                deals = deals.Where(x => x.PponDeal.ItemPrice <= maxprice.Value).ToList();
            }
            if (deliverytype.HasValue)
            {
                deals = deals.Where(x => (x.PponDeal.DeliveryType ?? 1) == deliverytype).ToList();
            }

            //取得通路商
            AgentChannel channel = ChannelFacade.GetOrderClassificationByHeaderToken();

            List<Guid> excludedGuid = ChannelFacade.ChannelExcludedGuid(deals.Select(x=>x.PponDeal).ToList(), channelClientProperty, ChannelIdList,(int)channel);
            deals = ChannelFacade.ChannelComboDealBlackWhiteList(deals, channelid, areaid, categoryid, (int)channel, (int)DeliveryType.ToShop, ref excludedGuid);
            deals = deals.Where(x => !excludedGuid.Contains(x.PponDeal.BusinessHourGuid)).ToList();
            return new HttpResponseMessage()
            {
                Content = new StringContent(JsonConvert.SerializeObject(
                   deals.Select(x => new ChannelPponDeal(channel, x.PponDeal, ChannelIdList, channelClientProperty, config.SiteUrl, isRemoveStyle))
                   ), Encoding.UTF8, "application/json")
            };
        }


        [OAuthScope(TokenScope.LionTravel)]
        [HttpGet]
        public HttpResponseMessage DeliveryDealList(string accesstoken, int channelid, string categoryid = null)
        {
            Dictionary<int, string> sp_labeltaglist = CategoryManager.CategoryDependencyGetAll().Where(x => x.CategoryStatus == 1 && x.CategoryType == (int)CategoryType.DealSpecialCategory && x.ParentType == (int)CategoryType.DeliveryType)
                 .Select(x => new { CategoryId = x.CategoryId, Name = x.CategoryName }).Distinct().ToDictionary(x => x.CategoryId, x => x.Name);
            List<int> categoryid_list = new List<int>();

            int category_id;
            if (!string.IsNullOrEmpty(categoryid))
            {
                string[] category_split_list = categoryid.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string item in category_split_list)
                {
                    if (int.TryParse(item, out category_id))
                    {
                        categoryid_list.Add(category_id);
                    }
                }
            }

            //取得通路商
            AgentChannel channel = ChannelFacade.GetOrderClassificationByHeaderToken();

            List<MultipleMainDealPreview> deals = ViewPponDealManager.DefaultManager.MultipleMainDealPreviewGetListByCategoryCriteria(channelid, null, categoryid_list);

            //取最低毛利率的值
            OauthToken oauth_token = CommonFacade.OauthTokenGet(accesstoken);
            OAuthClientModel client = OAuthFacade.GetClient(oauth_token.AppId);
            var channelClientProperty = OAuthFacade.GetChannelClientProperty(client.AppId);

            //業績銷售分類
            List<DealTypeNode> nodes = DealTypeFacade.GetDealTypeNodes(false);

            List<Guid> excludedGuid = ChannelFacade.ChannelExcludedGuid(deals.Select(x => x.PponDeal).ToList(), channelClientProperty, ChannelIdList, (int)channel);

            deals = ChannelFacade.ChannelComboDealRemoveMulti(deals);
            deals = ChannelFacade.ChannelComboDealBlackWhiteList(deals, channelid, null, categoryid, (int)channel, (int)DeliveryType.ToHouse, ref excludedGuid);

            deals = deals.Where(x => !excludedGuid.Contains(x.PponDeal.BusinessHourGuid)).ToList();

            ChannelPostListCollection cplc = new ChannelPostListCollection();
            foreach (MultipleMainDealPreview deal in deals)
            {
                foreach (ViewComboDeal v in deal.PponDeal.ComboDelas)
                {
                    ChannelPostList cpl = new ChannelPostList();
                    cpl = new ChannelPostList
                    {
                        Bid = v.BusinessHourGuid,
                        ChannelSource = (int)channel,
                        CreateTime = DateTime.Now

                    };
                    cplc.Add(cpl);
                }
            }
            cp.ChannelPostListDelete((int)channel);
            cp.ChannelPostListSet(cplc);


            return new HttpResponseMessage()
            {
                Content = new StringContent(JsonConvert.SerializeObject(
                   deals.Select(x => new ChannelDeliveryPponDeal(x.PponDeal, ChannelIdList, channelClientProperty, nodes, config.SiteUrl))
                   ), Encoding.UTF8, "application/json")
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="accesstoken"></param>
        /// <param name="dealguid"></param>
        /// <returns></returns>
        [OAuthScope(TokenScope.LionTravel)]
        [HttpGet]
        public HttpResponseMessage DealStores(string accesstoken, Guid dealguid)
        {
            ViewPponStoreCollection stores = pp.ViewPponStoreGetListByBidWithNoLock(dealguid, VbsRightFlag.VerifyShop);
            IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(dealguid);
            int deal_order_total_limit = (int)(deal.OrderTotalLimit ?? 0);
            int deal_ordered_quantity = deal.OrderedQuantity ?? 0;

            //通用券例外處理, 通用券無分店以賣家GUID代替, 數量以vpd為準
            if (Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.NoRestrictedStore))
            {
                List<DealStores> dsCol = new List<Models.Channel.DealStores>();
                dsCol.Add(new DealStores(deal.SellerName, deal.SellerGuid, deal_order_total_limit, deal_ordered_quantity, 0));
                return new HttpResponseMessage()
                {
                    Content = new StringContent(JsonConvert.SerializeObject(dsCol), Encoding.UTF8, "application/json")
                };
            }

            //if (stores.Count == 1)
            //{
            //    stores.First().OrderedQuantity = deal_ordered_quantity;
            //    stores.First().TotalQuantity = deal_order_total_limit;
            //}
            return new HttpResponseMessage()
            {
                Content = new StringContent(JsonConvert.SerializeObject(
                    stores.Select(x => new DealStores(x, deal_order_total_limit,
                        ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(x.BusinessHourGuid).SaleMultipleBase ?? 0)
            )), Encoding.UTF8, "application/json")};
        }

        /// <summary>
        /// 宅配Checkout
        /// </summary>
        /// <param name="accesstoken"></param>
        /// <param name="dealguid"></param>
        /// <returns></returns>
        [OAuthScope(TokenScope.LionTravel)]
        [HttpGet]
        public ApiResult DealDelivery(string accesstoken, Guid dealguid)
        {
            ApiResult result = new ApiResult();
            string userName = config.TmallDefaultUserName;
            var pponDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(dealguid);
            string message;
            bool isForcePass = false;

            //確認是否在白名單
            AgentChannel channel = ChannelFacade.GetOrderClassificationByHeaderToken();
            isForcePass = ChannelFacade.IsChannleForcePass(channel, pponDeal);
            
            


            var apiCheckout = PponDealApiManager.ApiPponDealCheckoutDataGet(dealguid, userName, null, isForcePass, out message, false);
            
            if (!string.IsNullOrEmpty(message))
            {
                result.Code = ApiResultCode.Error;
                result.Message = message;
                return result;
            }
            
            result.Code = ApiResultCode.Success;
            result.Data = new DeliveryCheckout(apiCheckout, pponDeal.MaxItemCount ?? 20);
            return result;
        }

        /// <summary>
        /// 業績歸屬分類
        /// </summary>
        /// <param name="accesstoken"></param>
        /// <returns></returns>
        [OAuthScope(TokenScope.LionTravel)]
        [HttpGet]
        public HttpResponseMessage DealTypeList(string accesstoken)
        {
            SystemCodeCollection dealType = sp.SystemCodeGetListByCodeGroup("DealType");
            return new HttpResponseMessage()
            {
                Content = new StringContent(JsonConvert.SerializeObject(
                    dealType
                    .Select(x => new
                    {
                        CodeId = x.CodeId,
                        CodeName = x.CodeName
                    })
                    ), Encoding.UTF8, "application/json")
            };
        }

        /// <summary>
        /// 全家分店
        /// </summary>
        /// <param name="accesstoken"></param>
        /// <returns></returns>
        [OAuthScope(TokenScope.LionTravel)]
        [HttpGet]
        public HttpResponseMessage FamiStoreList(string accesstoken)
        {
            var tempStoreInfoList = _fami.GetFamilyStoreInfoList();
            return new HttpResponseMessage()
            {
                Content = new StringContent(JsonConvert.SerializeObject(
                    tempStoreInfoList
                    .Select(x => new
                    {
                        StoreCode = x.StoreCode,
                        StoreName = x.StoreName,
                        Address = x.Address,
                        CreateTime = x.CreateTime
                    })
                    ), Encoding.UTF8, "application/json")
            };
        }

        /// <summary>
        /// 銀行代碼
        /// </summary>
        /// <param name="accesstoken"></param>
        /// <returns></returns>
        [OAuthScope(TokenScope.LionTravel)]
        [HttpGet]
        public HttpResponseMessage BankInfoList(string accesstoken)
        {
            var tempStoreInfoList = op.BankInfoGetList();
            return new HttpResponseMessage()
            {
                Content = new StringContent(JsonConvert.SerializeObject(
                    tempStoreInfoList
                    .Select(x => new
                    {
                        BankNo = x.BankNo,
                        BankName = x.BankName,
                        BranchNo = x.BranchNo,
                        BranchName = x.BranchName,
                        Address = x.Address,
                        Telephone = x.Telephone
                    })
                    ), Encoding.UTF8, "application/json")
            };
        }


        /// <summary>
        /// 測試宅配Json
        /// </summary>
        /// <param name="accesstoken"></param>
        /// <returns></returns>
        [OAuthScope(TokenScope.LionTravel)]
        [HttpGet]
        public ApiResult DeliveryTestModel(string accesstoken)
        {
            ApiResult result = new ApiResult();
            ChannelMakeOrderModel orderModel = new ChannelMakeOrderModel
            {
                DealGuid = Guid.Parse("09a50214-26b4-424f-b986-45df76492ca9"),
                Quantity = 5,
                RelatedOrderId = "evan0012341",
                IsSendSms = false,
                IsConfirmAmount = false,
                Amount = 188,
                IsDelivery = true,
                DeliveryInfo = new ChannelDeliveryInfo
                {
                    WriteAddress = "台北市中山區中山北路一段11號14F",
                    WriteZipCode = "104",
                    ShippingFee = 0,
                    WriteName = "簡文勇",
                    Phone = "0931195772"
                },
                ItemOption = new List<ItemOptionsModel>
                {
                    new ItemOptionsModel
                    {
                        ItemSpec = new List<ItemCategory>
                        {
                            new ItemCategory
                            {
                                OptionGuid = Guid.Parse("e0716fe2-18b7-4c49-8f41-b035494fb24a"),
                                OptionName = "綠色（GR）"
                            }
                        },
                        Qty = 3
                    },
                    new ItemOptionsModel
                    {
                        ItemSpec = new List<ItemCategory>
                        {
                            new ItemCategory
                            {
                                OptionGuid = Guid.Parse("7c6ad71a-7f75-45d3-a05d-06cf93fa2a70"),
                                OptionName = "粉紅色（PA）"
                            }
                        },
                        Qty = 2
                    }
                }
            };

            result.Code = ApiResultCode.Success;
            result.Data = orderModel;
            return result;
        }
    }

}

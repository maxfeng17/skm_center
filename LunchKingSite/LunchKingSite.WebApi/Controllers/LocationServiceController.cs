﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.WebApi.Core;
using LunchKingSite.WebApi.Core.OAuth;
using LunchKingSite.WebLib;
using System;
using System.Linq;
using System.Web.Http;
using LunchKingSite.WebApi.Core.Attributes;

namespace LunchKingSite.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [RequireHttps(false)]    
    [MvcStyleBinding]//不要再使用MvcStyleBinding
    public class LocationServiceController : BaseController
    {
        /// <summary>
        /// 依據城市id取得鄉鎮市區清單
        /// </summary>
        /// <param name="cid"></param>
        /// <returns></returns>
        [HttpGet, HttpPost]
        public ApiResult GetTownships(string cid)
        {
            const string methodName = "GetTownships";
            ApiResult result = new ApiResult() { Code = ApiResultCode.Success };

            try
            {
                if (!string.IsNullOrWhiteSpace(cid))
                {
                    var cities = CityManager.TownShipGetListByCityId(int.Parse(cid));
                    var jsonCol = from x in cities select new { id = x.Id, name = x.CityName };

                    result.Data = jsonCol;
                }
            }
            catch (Exception e)
            {
                WebUtility.LogExceptionAnyway(e);
            }


            SetApiLog(GetMethodName(methodName), string.Empty, new { cid}, result, LunchKingSite.Core.Helper.GetClientIP());

            return result;
        }

        /// <summary>
        /// 依據城市id取得鄉鎮市區清單(排除離島)
        /// </summary>
        /// <param name="cid"></param>
        /// <returns></returns>
        [HttpGet, HttpPost]
        public ApiResult GetNotDeliveryIslandsTownships(string cid)
        {
            const string methodName = "GetNotDeliveryIslandsTownships";
            ApiResult result = new ApiResult() { Code = ApiResultCode.Success };

            try
            {
                if (!string.IsNullOrWhiteSpace(cid))
                {
                    int parentId = int.TryParse(cid, out parentId) ? parentId : PponCityGroup.DefaultPponCityGroup.AllCountry.CityId;
                    var cities = CityManager.TownShipGetListByParentCityIdWithoutIsland(parentId);
                    var jsonCol = from x in cities select new { id = x.Id, name = x.CityName };

                    result.Data = jsonCol;
                }
            }
            catch (Exception e)
            {
                WebUtility.LogExceptionAnyway(e);
            }

            SetApiLog(GetMethodName(methodName), string.Empty, new { cid}, result, LunchKingSite.Core.Helper.GetClientIP());

            return result;
        }

        /// <summary>
        /// 依據傳入的cityId取得Building的資料
        /// </summary>
        /// <param name="cid"></param>
        /// <returns></returns>
        [HttpGet, HttpPost]
        public ApiResult GetLocations(string cid)
        {
            const string methodName = "GetLocations";
            ApiResult result = new ApiResult() { Code = ApiResultCode.Success };

            try
            {
                if (!string.IsNullOrWhiteSpace(cid))
                {
                    var buildings = CityManager.BuildingListGetByTownshipId(int.Parse(cid));
                    var jsonCol = from x in buildings select new { id = x.Guid, name = x.BuildingName };

                    result.Data = jsonCol;
                }
            }
            catch (Exception e)
            {
                WebUtility.LogExceptionAnyway(e);
            }

            SetApiLog(GetMethodName(methodName), string.Empty, new { cid}, result, LunchKingSite.Core.Helper.GetClientIP());

            return result;
        }

        /// <summary>
        /// 取得城市列表
        /// </summary>
        /// <param name="withOutLands"></param>
        /// <returns></returns>
        [HttpGet, HttpPost]
        public ApiResult GetCitysJSon(bool withOutLands)
        {
            const string methodName = "GetCitysJSon";
            ApiResult result = new ApiResult() { Code = ApiResultCode.Success };

            result.Data = from x in CityManager.Citys.Where(x => x.Code != "SYS") select new { Id = x.Id, Name = x.CityName };

            SetApiLog(GetMethodName(methodName), string.Empty, new { withOutLands}, result, LunchKingSite.Core.Helper.GetClientIP());

            return result;
        }

        /// <summary>
        /// 依據城市id取得鄉鎮市區清單
        /// </summary>
        /// <param name="cid"></param>
        /// <param name="withOutLands"></param>
        /// <returns></returns>
        [HttpGet, HttpPost]
        public ApiResult GetTownshipsJSon(string cid, bool withOutLands)
        {
            const string methodName = "GetTownshipsJSon";
            ApiResult result = new ApiResult() { Code = ApiResultCode.Success };

            var cities = CityManager.TownShipGetListByCityId(int.Parse(cid));
            result.Data = from x in cities select new { Id = x.Id, Name = x.CityName };

            SetApiLog(GetMethodName(methodName), string.Empty, new { cid, withOutLands}, result, LunchKingSite.Core.Helper.GetClientIP());

            return result;
        }

        /// <summary>
        /// 依據傳入的cityId取得Building的資料
        /// </summary>
        /// <param name="cid"></param>
        /// <returns></returns>
        [HttpGet, HttpPost]
        public ApiResult GetLocationsJSon(string cid)
        {
            const string methodName = "GetTownshipsJSon";
            ApiResult result = new ApiResult() { Code = ApiResultCode.Success };

            var buildings = CityManager.BuildingListGetByTownshipId(int.Parse(cid));
            result.Data = from x in buildings select new { Id = x.Guid, Name = x.BuildingName };

            SetApiLog(GetMethodName(methodName), string.Empty, new { cid}, result, LunchKingSite.Core.Helper.GetClientIP());

            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet, HttpPost]
        public ApiResult GetCityListWithTownshipJSon()
        {
            const string methodName = "GetCityListWithTownshipJSon";
            ApiResult result = new ApiResult() { Code = ApiResultCode.Success };

            result.Data = LocationApiManager.GetApiCityList();

            SetApiLog(GetMethodName(methodName), string.Empty, null, result, LunchKingSite.Core.Helper.GetClientIP());

            return result;
        }
    }
}

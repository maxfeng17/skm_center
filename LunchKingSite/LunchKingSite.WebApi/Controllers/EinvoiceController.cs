﻿using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace LunchKingSite.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class EinvoiceController : BaseController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public HttpResponseMessage Post(HttpRequestMessage request)
        {
            var cardType = "EG0071";
            var config = ProviderFactory.Instance().GetConfig();
            var comUniqueId = config.ContactCompanyId;
            try
            {
                EasyDigitCipher cipher = new EasyDigitCipher();
                var name_value_collection = request.Content.ReadAsFormDataAsync().Result;
                bool valid;
                //統編 70553216 -> 24317014
                string card_ban = name_value_collection["card_ban"];
                //user uniqueid 
                string card_no1_base64 = name_value_collection["card_no1"];
                string card_no1 = string.IsNullOrEmpty(card_no1_base64) ? string.Empty : ConvertFromBase64String(card_no1_base64);
                string card_no2_base64 = name_value_collection["card_no2"];
                //載具類別 17Life設定為EJ0018 -> EG0071
                string card_type_base64 = name_value_collection["card_type"];
                string card_type = string.IsNullOrEmpty(card_type_base64) ? string.Empty : ConvertFromBase64String(card_type_base64);
                string token = name_value_collection["token"];
                string token_decrypt;
                if (!string.IsNullOrEmpty(card_ban) && card_ban == comUniqueId
                    && !string.IsNullOrEmpty(card_type_base64) && card_type == cardType
                    && !string.IsNullOrEmpty(card_no1_base64) && card_no1_base64 == card_no2_base64)
                {
                    if (!string.IsNullOrEmpty(token) && cipher.Decrypt(token, out token_decrypt))
                    {
                        if (token_decrypt == string.Format("{0}{1}{2}", DateTime.Now.Month, card_no1, DateTime.Now.Day))
                        {
                            valid = true;
                        }
                        else
                        {
                            valid = false;
                        }
                    }
                    else
                    {
                        valid = false;
                    }
                }
                else
                {
                    valid = false;
                }
                return new HttpResponseMessage() { Content = new StringContent(valid ? "Y" : "N") };
            }
            catch (Exception error)
            {
                SetApiLog("EinvoicePost", "Einvoice", error.Message, 0, Helper.GetClientIP());
                return new HttpResponseMessage() { Content = new StringContent("N") };
            }

        }

        private string ConvertFromBase64String(string input)
        {
            return Encoding.UTF8.GetString(Convert.FromBase64String(input));
        }
    }

    /// <summary>
    /// Extends the HttpRequestMessage collection
    /// </summary>
    public static class HttpRequestMessageExtensions
    {

        /// <summary>
        /// Returns a dictionary of QueryStrings that's easier to work with 
        /// than GetQueryNameValuePairs KevValuePairs collection.
        /// 
        /// If you need to pull a few single values use GetQueryString instead.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static Dictionary<string, string> GetQueryStrings(this HttpRequestMessage request)
        {
            return request.GetQueryNameValuePairs()
                          .ToDictionary(kv => kv.Key, kv => kv.Value, StringComparer.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Returns an individual querystring value
        /// </summary>
        /// <param name="request"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetQueryString(this HttpRequestMessage request, string key)
        {
            // IEnumerable<KeyValuePair<string,string>> - right!
            var queryStrings = request.GetQueryNameValuePairs();
            if (queryStrings == null)
                return null;

            var match = queryStrings.FirstOrDefault(kv => string.Compare(kv.Key, key, true) == 0);
            if (string.IsNullOrEmpty(match.Value))
                return null;

            return match.Value;
        }

        /// <summary>
        /// Returns an individual HTTP Header value
        /// </summary>
        /// <param name="request"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetHeader(this HttpRequestMessage request, string key)
        {
            IEnumerable<string> keys = null;
            if (!request.Headers.TryGetValues(key, out keys))
                return null;

            return keys.First();
        }

        /// <summary>
        /// Retrieves an individual cookie from the cookies collection
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cookieName"></param>
        /// <returns></returns>
        public static string GetCookie(this HttpRequestMessage request, string cookieName)
        {
            CookieHeaderValue cookie = request.Headers.GetCookies(cookieName).FirstOrDefault();
            if (cookie != null)
                return cookie[cookieName].Value;

            return null;
        }

    }
}

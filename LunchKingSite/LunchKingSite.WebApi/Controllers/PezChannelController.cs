﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using LunchKingSite.WebApi.Core;
using LunchKingSite.WebApi.Core.OAuth;
using Helper = LunchKingSite.Core.Helper;

namespace LunchKingSite.WebApi.Controllers
{
    /// <summary>
    /// Payeasy 導購
    /// </summary>
    public class PezChannelController : BaseController
    {
        ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        public class GetOrderParameterModel
        {
            public DateTime start { get; set; }
            public DateTime end { get; set; }
            public string token { get; set; }
        }

        public class OrderModel
        {
            public bool status { get; set; }
            public List<OrderDetailModel> data { get; set; }

            public class OrderDetailModel
            {
                public string price { get; set; }
                public string order_id { get; set; }
                public string session_id { get; set; }
                public int status { get; set; }
                public string order_date { get; set; }
                public string cat_id { get; set; }
                public string prod_data { get; set; }
                public string memo { get; set; }
                public string promotion { get; set; }
            }
        }

        [HttpGet]
        [OAuthScope(TokenScope.Deal)]
        public OrderModel GetOrder([FromUri]GetOrderParameterModel model)
        {
            var result = new OrderModel()
            {
                status = true,
                data = new List<OrderModel.OrderDetailModel>(),
            };

            try
            {
                model.end = model.end.AddDays(1); //以當日的23:59:59算
                var vpo = OrderFacade.GetViewPezChannelByOrderTimePeriod(model.start, model.end);

                foreach (var po in vpo.GroupBy(x => new { OrderGuid = x.OrderGuid, DeliveryType = x.DeliveryType, OrderId = x.OrderId, OrderTime = x.OrderTime, Gid = x.Gid, OrderStatus = x.OrderStatus, Bid = x.Bid,}, y => y))
                {
                    if (po.Key.DeliveryType == (int)DeliveryType.ToShop)
                    {
                        #region 憑證 (因憑證狀態不同，以多筆訂單型態送出)

                        var returnCode = OrderFacade.GetCommissionRuleReturnCode(ChannelSource.Payeasy, DeliveryType.ToShop, po.Key.Bid, po.Key.OrderTime);

                        foreach (var item in po)
                        {
                            result.data.Add(new OrderModel.OrderDetailModel
                            {
                                price = item.Amount.ToString("F0"),
                                order_id = po.Key.OrderId + "-" + item.CouponId,
                                session_id = po.Key.Gid,
                                status = ((item.CashTrustLogStatus.EqualsAny((int)TrustStatus.Refunded, (int)TrustStatus.Returned)) ? (int)PezChannelStatus.Invalid : ((item.CashTrustLogStatus.Equals((int)TrustStatus.Verified)) ? (int)PezChannelStatus.Valid : (int)PezChannelStatus.Unconfirmed)),
                                order_date = po.Key.OrderTime.ToString("yyyy-MM-dd"),
                                cat_id = returnCode,
                                prod_data = item.ItemName,
                                promotion = po.Any(x => x.DiscountAmount > 0) ? "yes" : "",
                            });
                        }

                        #endregion
                    }
                    else if (po.Key.DeliveryType == (int)DeliveryType.ToHouse)
                    {
                        #region 宅配 (因宅配狀態一致，多筆商品金額以"||"合併資料送出)

                        var returnCode = OrderFacade.GetCommissionRuleReturnCode(ChannelSource.Payeasy, DeliveryType.ToHouse, po.Key.Bid, po.Key.OrderTime);
                        var poRefundedReturned = po.Where(x => !x.CashTrustLogStatus.EqualsAny((int)TrustStatus.Refunded, (int)TrustStatus.Returned)).ToList();

                        var returnCodes = Enumerable.Repeat(returnCode, poRefundedReturned.Count());

                        if (po.Any(x => x.CashTrustLogStatus < (int)TrustStatus.Returned))
                        {
                            int pezChannelStatus = po.Key.OrderTime.AddDays(config.PezChannelRunDay) > DateTime.Now ? (int)PezChannelStatus.Unconfirmed : (int)PezChannelStatus.Valid;

                            result.data.Add(new OrderModel.OrderDetailModel
                            {
                                price = string.Join("||", poRefundedReturned.Select(x => x.Amount.ToString("F0"))),
                                order_id = po.Key.OrderId,
                                session_id = po.Key.Gid,
                                status = pezChannelStatus,
                                order_date = po.Key.OrderTime.ToString("yyyy-MM-dd"),
                                cat_id = string.Join("||", returnCodes),
                                prod_data = string.Join("||", poRefundedReturned.Select(x => x.ItemName)),
                                promotion = poRefundedReturned.Any(x => x.DiscountAmount > 0) ? "yes" : "",
                            });
                        }
                        else
                        {
                            result.data.Add(new OrderModel.OrderDetailModel
                            {
                                price = string.Join("||", poRefundedReturned.Select(x => x.Amount.ToString("F0"))),
                                order_id = po.Key.OrderId,
                                session_id = po.Key.Gid,
                                status = (int)PezChannelStatus.Invalid,
                                order_date = po.Key.OrderTime.ToString("yyyy-MM-dd"),
                                cat_id = string.Join("||", returnCodes),
                                prod_data = string.Join("||", poRefundedReturned.Select(x => x.ItemName)),
                                promotion = poRefundedReturned.Any(x => x.DiscountAmount > 0) ? "yes" : "",
                            });
                        }

                        #endregion
                    }
                }
                
                return result;
            }
            catch (Exception ex)
            {
                var rtnObject = new ApiReturnObject { Code = ApiReturnCode.Error, Data = ex, Message = "執行階段發生錯誤" };
                SetApiLog("PezChannel_GetOrder", AppId, new { AccessToken = model.token }, rtnObject, Helper.GetClientIP());
                result.status = false;
                return result;
            }
        }
    }
}

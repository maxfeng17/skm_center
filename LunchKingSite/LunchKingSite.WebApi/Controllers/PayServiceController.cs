﻿using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.BizLogic.Models.Payment;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebApi.Core.Attributes;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Net.Http;
using LunchKingSite.Core.ModelCustom;

namespace LunchKingSite.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class PayServiceController : BaseController
    {
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static IOrderProvider op = ProviderFactory.Instance().GetDefaultProvider<IOrderProvider>();
        private static ILog logger = LogManager.GetLogger(typeof(PaymentFacade));

        /// <summary>
        /// 國泰PG OTP Step1
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [RequireHttps]
        public HttpResponseMessage CathayOtpResultUrl()
        {
            logger.Info("[CathayOtpResultUrl]");
            try
            {
                var stream = this.Request.Content.ReadAsStreamAsync().Result;
                string msg = Encoding.UTF8.GetString(stream.ReadAllBytes());
                logger.Info("[Result]" + msg);

                Order o = null;
                string transId = string.Empty;

                //step1
                logger.Info("[step1]取得授權結果");
                if (!string.IsNullOrEmpty(HttpContext.Current.Request.Form.GetValues("strRsXML")[0].ToString()))
                {
                    string strRsXML = HttpContext.Current.Request.Form.GetValues("strRsXML")[0].ToString();
                    System.Xml.XmlDocument xml = new System.Xml.XmlDocument();
                    xml.LoadXml(strRsXML);
                    string json = Newtonsoft.Json.JsonConvert.SerializeXmlNode(xml);
                    var authResult = Newtonsoft.Json.JsonConvert.DeserializeObject<CathayPaymentOtpFirstResult>(json);

                    transId = authResult.CUBXML.ORDERINFO.ORDERNUMBER;
                    PaymentTransaction ptCreditCard = op.PaymentTransactionGet(transId, LunchKingSite.Core.PaymentType.Creditcard, PayTransType.Authorization);
                    o = op.OrderGet(ptCreditCard.OrderGuid.Value);
                    PaymentFacade.OTPApiLog(transId.ToLower(), strRsXML);

                    string code = authResult.CUBXML.AUTHINFO.AUTHCODE;//授權碼
                    string message = authResult.CUBXML.AUTHINFO.AUTHMSG;//回傳訊息
                    //string orderStatus = authResult.CUBXML.AUTHINFO.AUTHSTATUS;//授權結果
                    string retCode = authResult.CUBXML.AUTHINFO.AUTHSTATUS;//授權回應碼

                    int retCoderesult = 0;
                    if (ptCreditCard.IsLoaded)
                    {
                        ptCreditCard.AuthCode = code;
                        ptCreditCard.Result = int.TryParse(retCode, out retCoderesult) ? retCoderesult : -9527;
                        ptCreditCard.Message = retCode + "|" + message + ptCreditCard.Message.Replace("OTP驗證中", "");
                        op.PaymentTransactionSet(ptCreditCard);
                    }

                    var cubKey = o.Installment > 0 ? config.CathayInstallmentCubKey : config.CathayCubKey;
                    if (authResult.Verify(cubKey))
                    {
                        var host = new Uri(config.SiteUrl).Host;
                        var data = "<?xml version=\'1.0\' encoding=\'UTF-8\'?><MERCHANTXML><CAVALUE>" + Security.MD5Hash(host + cubKey, false) + "</CAVALUE><RETURL>" + config.SSLSiteUrl + "/ppon/CathayOtpRedirect" + "</RETURL></MERCHANTXML>";
                        return new HttpResponseMessage
                        {
                            Content = new StringContent(data, Encoding.UTF8, "application/xml")
                        };
                    }
                    else
                    {
                        //todo:驗證失敗做取消授權
                        logger.Info("[Verify Error]");
                    }

                    //其他錯誤同步更新CreditcardOrder
                    CreditcardOrder co = op.CreditcardOrderGetByOrderGuid((Guid)ptCreditCard.OrderGuid);
                    if (co.IsLoaded)
                        co.Result = (int)ptCreditCard.Result;
                    op.SetCreditcardOrder(co);
                    //return new HttpResponseMessage
                    //{
                    //    Content = new StringContent(data, Encoding.UTF8, "application/xml")
                    //};
                }
            }
            catch(Exception ex)
            {
                logger.Error(ex.Message);
            }
            return null;
        }

        /// <summary>
        /// TSPG非同步
        /// </summary>
        /// <returns></returns>
        [RequireHttps]
        [HttpPost]
        public PayResult taishinOTPResultUrl()
        {
            try
            {
                //取得非同步資訊
                var stream = this.Request.Content.ReadAsStreamAsync().Result;
                string msg = Encoding.UTF8.GetString(stream.ReadAllBytes());
                stream.Position = 0;
                OTPAsync asyncResult = Newtonsoft.Json.JsonConvert.DeserializeObject<OTPAsync>(msg);
                string transId = asyncResult.@params.order_no;
                Guid orderGuid = Guid.Empty;
                PaymentTransaction ptCreditCard = op.PaymentTransactionGet(transId, LunchKingSite.Core.PaymentType.Creditcard, PayTransType.Authorization);
                PaymentFacade.OTPApiLog(transId, msg);

                
                string code = asyncResult.@params.auth_id_resp;//授權碼
                string message = asyncResult.@params.ret_msg;//回傳訊息
                string orderStatus = asyncResult.@params.order_status;//授權結果
                string retCode = asyncResult.@params.ret_code;//授權回應碼

                int retCoderesult = 0;
                if (ptCreditCard.IsLoaded)
                {
                    //非同步更新
                    ptCreditCard.AuthCode = code;
                    ptCreditCard.Message = message + ptCreditCard.Message.Replace("OTP驗證中", "");

                    if (retCode == "00" && orderStatus != "02")
                    {
                        //兩種狀態不一致
                        retCode = "-9527";
                        logger.Error("OTPWarn:非同步兩種狀態不一致 transid=" + transId);
                    }

                    if (orderStatus == "02" && ptCreditCard.Result != -1 && ptCreditCard.Result != 0)
                    {
                        //查詢不成功但非同步成功,已查詢為主
                        //訂單未成立必須直接取消授權
                        retCode = ptCreditCard.Result.ToString();
                        CreditCardUtility.AuthenticationReverse(transId);
                        logger.Error("OTPWarn:查詢不成功但非同步成功 transid=" + transId);
                    }
                    else if (orderStatus != "02" && ptCreditCard.Result == 0)
                    {
                        //查詢成功但非同步失敗(?)
                        logger.Error("OTPWarn:查詢成功但非同步失敗 transid=" + transId);
                    }
                    else
                    {
                        ptCreditCard.Result = int.TryParse(retCode, out retCoderesult) ? retCoderesult : -9527;
                    }
                    op.PaymentTransactionSet(ptCreditCard);

                }
                if (orderStatus == "02")
                {
                    //成功
                }
                else if (retCode == "19")
                {
                    //TSPG若休息
                    logger.Error("OTPWarn:oGuid=" + ptCreditCard.OrderGuid.ToString() + " ret_code=" + retCode + " ret_msg=" + message);
                }
                else
                {
                    //其他錯誤同步更新CreditcardOrder
                    CreditcardOrder co = op.CreditcardOrderGetByOrderGuid((Guid)ptCreditCard.OrderGuid);
                    if (co.IsLoaded)
                        co.Result = (int)ptCreditCard.Result;
                    op.SetCreditcardOrder(co);

                }

                var result = new PayResult
                {
                    ReturnCode = ApiReturnCode.Success,
                    ResultCode = ApiResultCode.Success,
                    Message = msg
                };

                return result;
            }
            catch (Exception ex)
            {
                logger.Error("OTPWarn:taishinOTPResultUrl", ex);
                return null;
            }
            
        }

        /// <summary>
        /// LinePay付款確認
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [RequireHttps]
        [HttpPost]
        public ApiReturnObject LinePayConfirm(LinePayConfirm input)
        {
            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };

            logger.Info("LinePayConfirm:" + new JsonSerializer().Serialize(input));

            //驗證使用者
            ApiUser apiUser = ApiUserManager.ApiUserGetByUserId(input.UserId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                return rtnObject;
            }

            PaymentTransaction pt = op.PaymentTransactionGet(input.TransId, PaymentType.LinePay, PayTransType.Authorization);
            if (pt != null)
            {
                try
                {
                    string transactionId = pt.TransId;
                    string userName = pt.CreateId;
                    var linePayTrans = op.LinePayTransLogGet(input.TicketId);
                    Order o = op.OrderGet(pt.OrderGuid.Value);
                    //檢查金額、幣值與PT是否相同
                    if (!input.TradeAmount.Equals(((int)pt.Amount).ToString()))
                    {
                        logger.Error("付款金額與實際金額不符");

                        #region LinePay 取消授權/請款

                        //不在因金額不符做取消授權
                        //if (linePayTrans.IsCaptureSeparate) //取消授權
                        //{
                        //    LinePayUtility.VoidTrans(linePayTrans);
                        //}
                        //else //退款
                        //{
                        //    LinePayUtility.BuyFailRefund(linePayTrans, (int)pt.Amount, false);
                        //}

                        PaymentFacade.UpdateTransaction(transactionId, pt.OrderGuid, PaymentType.LinePay, pt.Amount
                            , pt.AuthCode
                            , PayTransType.Authorization, DateTime.Now
                            , "LinePay 付款金額與份數不符"
                            , Helper.SetPaymentTransactionPhase(pt.Status, PayTransPhase.Failed)
                            , (int)pt.Result, PaymentAPIProvider.LinePay);


                        #endregion LinePay 取消授權/請款

                        rtnObject.Code = ApiReturnCode.InputError;
                        rtnObject.Message = "輸入金額錯誤";
                        return rtnObject;
                    }

                    logger.Info("Line Response:" + System.Web.HttpContext.Current.Request.RawUrl);
                    if (string.IsNullOrEmpty(input.TransId))
                    {

                        var msg = string.Format("LinePay 無法取得交易序號 ticketId:{0}", input.TicketId);
                        linePayTrans.TransStatus = (byte)LinePayTransStatus.RequestTransactionIdFail;
                        linePayTrans.TransMsg = msg;
                        op.LinePayTransLogSet(linePayTrans);
                        PaymentFacade.UpdateTransaction(transactionId, pt.OrderGuid, PaymentType.LinePay, pt.Amount, string.Empty
                        , PayTransType.Authorization, DateTime.Now
                        , msg
                        , Helper.SetPaymentTransactionPhase(pt.Status, PayTransPhase.Failed)
                        , 0, PaymentAPIProvider.LinePay);

                        rtnObject.Code = ApiReturnCode.DataNotExist;
                        rtnObject.Message = "無此交易序號";
                        return rtnObject;
                    }

                    //消費者取消交易
                    if (input.LineCancel)
                    {
                        var msg = string.Format("消費者取消交易{0}", linePayTrans.LinePayTransactionId);
                        linePayTrans.TransStatus = (byte)LinePayTransStatus.UserCancel;
                        linePayTrans.TransMsg = msg;
                        op.LinePayTransLogSet(linePayTrans);
                        PaymentFacade.UpdateTransaction(transactionId, pt.OrderGuid, PaymentType.LinePay, pt.Amount, string.Empty
                            , PayTransType.Authorization, DateTime.Now
                            , msg
                            , Helper.SetPaymentTransactionPhase(pt.Status, PayTransPhase.Canceled)
                            , 0, PaymentAPIProvider.LinePay);

                        rtnObject.Code = ApiReturnCode.PaymentError;
                        rtnObject.Message = "取消交易";
                        return rtnObject;
                    }

                    ////檢查Line導過來的transactionid 是否與建立 reserve 時一致
                    //if (input.TransId != linePayTrans.LinePayTransactionId)
                    //{
                    //    var msg = string.Format("交易序號不一致 reserve:{0}, confirm:{1}", linePayTrans.LinePayTransactionId,
                    //        input.TransId);
                    //    linePayTrans.TransStatus = (byte)LinePayTransStatus.LinePayTransactionIdNotEqual;
                    //    linePayTrans.TransMsg = msg;
                    //    op.LinePayTransLogSet(linePayTrans);
                    //    PaymentFacade.UpdateTransaction(transactionId, pt.OrderGuid, PaymentType.LinePay, pt.Amount, string.Empty
                    //        , PayTransType.Authorization, DateTime.Now
                    //        , msg
                    //        , Helper.SetPaymentTransactionPhase(pt.Status, PayTransPhase.Failed)
                    //        , 0, PaymentAPIProvider.LinePay);

                    //    rtnObject.Code = ApiReturnCode.InputError;
                    //    rtnObject.Message = "交易失敗";
                    //    return rtnObject;
                    //}

                    //取得Line授權/請款 (LinePay尚未支援授權與請款分開)
                    var confirmResult = LinePayUtility.Confirm(linePayTrans, (int)pt.Amount);
                    if (confirmResult.Item1 == false)
                    {
                        PaymentFacade.UpdateTransaction(transactionId, pt.OrderGuid, PaymentType.LinePay, pt.Amount
                            , confirmResult.Item2.ToString()
                            , PayTransType.Authorization, DateTime.Now
                            , confirmResult.Item3
                            , Helper.SetPaymentTransactionPhase(pt.Status, PayTransPhase.Failed)
                            , confirmResult.Item2, PaymentAPIProvider.LinePay);

                        rtnObject.Code = ApiReturnCode.PaymentError;
                        rtnObject.Message = "交易失敗";
                        return rtnObject;
                    }

                    PaymentFacade.UpdateTransaction(transactionId, pt.OrderGuid, PaymentType.LinePay, pt.Amount
                        , confirmResult.Item2.ToString()
                        , PayTransType.Authorization, DateTime.Now
                        , confirmResult.Item3
                        , Helper.SetPaymentTransactionPhase(pt.Status, PayTransPhase.Successful)
                        , confirmResult.Item2, PaymentAPIProvider.LinePay);

                    if (linePayTrans.IsCaptureSeparate == false)
                    {
                        //授權一併請款時直接新增請款
                        DepartmentTypes dType = Helper.GetPaymentTransactionDepartmentTypes(pt.Status);
                        PaymentFacade.NewTransaction(pt.TransId, pt.OrderGuid ?? Guid.Empty, PaymentType.LinePay, pt.Amount,
                            pt.AuthCode, PayTransType.Charging, DateTime.Now, userName, "LinePay 授權一併請款",
                            Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, dType), PayTransPhase.Successful),
                            PayTransResponseType.OK, (OrderClassification)pt.OrderClassification.GetValueOrDefault(), null
                            , (PaymentAPIProvider)pt.ApiProvider.GetValueOrDefault());

                        PaymentDTO paymentDto = PaymentFacade.GetPaymentDTOFromSession(input.TicketId);
                        //補完MakePayment
                        var result = PaymentFacade.MakePayment(paymentDto, userName);

                        rtnObject.Code = result.ReturnCode;
                        rtnObject.Message = result.Message;
                        rtnObject.Data = result.Data;

                        PaymentFacade.PushRecardOrder(paymentDto, o.UserId, result);
                        SystemFacade.SetApiLog("LinePayConfirm", input.UserId, paymentDto, null);
                    }
                }
                catch (Exception ex)
                {
                    logger.Error(string.Format("LinePay確認付款發生失敗 transId:{0} ticketId:{1} errMsg:{2}", input.TransId, input.TicketId, ex.Message));
                    rtnObject.Code = ApiReturnCode.Error;
                    rtnObject.Data = null;
                }
            }
            else
            {
                rtnObject.Code = ApiReturnCode.Error;
                rtnObject.Message = "未定義錯誤";
                rtnObject.Data = null;
            }

            return rtnObject;
        }
    }


}

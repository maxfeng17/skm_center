﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebApi.Core.OAuth;
using LunchKingSite.WebApi.Core.Attributes;
using LunchKingSite.BizLogic.Models.CustomerService;
using System.Text.RegularExpressions;
using LunchKingSite.BizLogic.Model.OAuth;

namespace LunchKingSite.WebApi.Controllers
{
    /// <summary>
    /// 客服相關 API
    /// </summary>
    public class UserServiceController : BaseController
    {
        private IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        protected LunchKingSite.Core.IServiceProvider sp = ProviderFactory.Instance().GetProvider<LunchKingSite.Core.IServiceProvider>();
        
        /// <summary>
        /// 取得客服信箱預設問題類別
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        [OAuthScope(TokenScope.Deal)]
        public ApiResult GetServiceMessageCategory(int? categoryId = null)
        {
            ApiResult result = new ApiResult();
            result.Code = ApiResultCode.Success;

            var serviceCates = mp.ServiceMessageCategoryCollectionGetListByParentId(categoryId);
            var items = new List<KeyValuePair<int, string>>();

            if (serviceCates.Any())
            {
                foreach (var cate in serviceCates)
                {
                    items.Add(new KeyValuePair<int, string>(cate.CategoryId, cate.CategoryName));
                }
                result.Data = new { data = items };
                return result;
            }

            result.Code = ApiResultCode.Error;
            return result;
        }

        /// <summary>
        /// 客服信箱送出
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [RequireHttps]
        [OAuthScope(TokenScope.Deal)]
        public ApiResult SaveServiceMessage(ServiceMessageModel model)
        {
            ApiResult result = new ApiResult();
            result.Code = ApiResultCode.Success;
            string errMsg;
            if (model.CheckMessage(out errMsg))
            {
                var email = HttpUtility.HtmlEncode(model.Email.Trim());
                ServiceMessage sm = new ServiceMessage();
                sm.Email = email;
                sm.Phone = HttpUtility.HtmlEncode(model.Phone);
                sm.Name = HttpUtility.HtmlEncode(model.Name);
                sm.OrderId = model.OrderId;
                sm.Category = model.CategoryId;
                sm.SubCategory = model.SubCategoryId;
                sm.Message = HttpUtility.HtmlEncode(model.Message);
                sm.Status = "wait";
                sm.MessageType = "來信";
                sm.CreateId = email;
                sm.CreateTime = DateTime.Now;

                int? userId = null;
                if (User.Identity.IsAuthenticated)
                {
                    userId = MemberFacade.GetUniqueId(User.Identity.Name);
                }
                mp.ServiceMessageSet(sm, userId);

                result.Data = new
                {
                    isSuccess = true,
                    failedReason = errMsg
                };
                return result;
            }
            result.Data = new
            {
                isSuccess = false,
                failedReason = errMsg
            };
            return result;
        }

        /// <summary>
        /// 客服信箱
        /// </summary>
        public class ServiceMessageModel
        {
            /// <summary>
            /// 姓名
            /// </summary>
            public string Name { get; set; }
            /// <summary>
            /// 聯絡電話
            /// </summary>
            public string Phone { get; set; }
            /// <summary>
            /// 聯絡Email
            /// </summary>
            public string Email { get; set; }
            /// <summary>
            /// 訂單編號 (非必填)
            /// </summary>
            public string OrderId { get; set; }
            /// <summary>
            /// 問題-主類別
            /// </summary>
            public int CategoryId { get; set; }
            /// <summary>
            /// 問題-子類別
            /// </summary>
            public int SubCategoryId { get; set; }
            /// <summary>
            /// 問題描述
            /// </summary>
            public string Message { get; set; }

            /// <summary>
            /// 驗證
            /// </summary>
            /// <returns></returns>
            public bool CheckMessage(out string errMsg)
            {
                errMsg = string.Empty;

                if (string.IsNullOrEmpty(Name))
                {
                    errMsg = "姓名不得為空";
                    return false;
                }

                if (string.IsNullOrEmpty(Phone))
                {
                    errMsg = "聯絡電話不得為空";
                    return false;
                }
                if (string.IsNullOrEmpty(Email))
                {
                    errMsg = "Email不得為空";
                    return false;
                }
                if (!RegExRules.CheckEmail(Email))
                {
                    errMsg = "Email錯誤";
                    return false;
                }
                if (CategoryId == 0)
                {
                    errMsg = "未選問題主類別";
                    return false;
                }
                if (SubCategoryId == 0)
                {
                    errMsg = "未選問題子類別";
                    return false;
                }
                if (string.IsNullOrEmpty(Message))
                {
                    errMsg = "問題描述不得為空";
                    return false;
                }

                return true;
            }

        }

        /// <summary>
        /// 首頁新資料提示
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ApiResult GetNtfyValuesCount(ApiGetData getNews)
        {
            if (getNews.UserId == null)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }

            if (User.Identity.Id == 0)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.UserNoSignIn
                };
            }

            List<NewDetail> data = CustomerServiceFacade.GetNtfyValuesCount(User.Identity.Id);

            string errMsg = string.Empty;

            if (!string.IsNullOrEmpty(errMsg))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = errMsg
                };
            }

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = data
            };
        }

        /// <summary>
        /// 問題紀錄列表
        /// </summary>
        /// <param name="getData"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult GetIssueList(ApiGetData getData)
        {
            if (getData.UserId == null)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }
            if (User.Identity.Id == 0)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.UserNoSignIn
                };
            }

            //Type: 1.一般問題 2.訂單問題
            var data = CustomerServiceFacade.GetDataList(User.Identity.Id, getData.Type);

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = data
            };
        }

        /// <summary>
        /// 建立客服問題
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ApiResult CreateIssue(ApiSendData sendData)
        {
            if (sendData.UserId == null)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }
            if (User.Identity.Id == 0)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.UserNoSignIn
                };
            }
            if (string.IsNullOrEmpty(sendData.Name))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }
            if (string.IsNullOrEmpty(sendData.Email))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }
            if (string.IsNullOrEmpty(sendData.Image) && string.IsNullOrEmpty(sendData.Content))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }

            ServiceData serviceData;
            string erroMessage;
            if (!CustomerServiceFacade.CreateIssue(User.Identity.Name, User.Identity.Id, sendData, out serviceData, out erroMessage))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.SaveFail,
                };
            }

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = serviceData
            };
        }

        /// <summary>
        /// 建立訂單問題
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ApiResult CreateOrderIssue(ApiSendData sendData)
        {
            if (sendData.UserId == null)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }
            if (User.Identity.Id == 0)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.UserNoSignIn
                };
            }
            if (string.IsNullOrEmpty(sendData.Name))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }
            if (string.IsNullOrEmpty(sendData.Email))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }
            if (string.IsNullOrEmpty(sendData.Image) && string.IsNullOrEmpty(sendData.Content))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }

            string serviceNo = "";
            if (!CustomerServiceFacade.CreateOrderIssue(User.Identity.Name, User.Identity.Id, sendData, false, out serviceNo))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.SaveFail,
                };
            }

            return new ApiResult
            {
                Code = ApiResultCode.Success,
            };
        }

        /// <summary>
        /// 更新聯絡人資訊
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ApiResult UpdateMemberInfo(ApiGetData getData)
        {
            if (getData.UserId == null)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }
            if (User.Identity.Id == 0)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.UserNoSignIn
                };
            }
            if (string.IsNullOrEmpty(getData.LastName))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }
            if (string.IsNullOrEmpty(getData.FirstName))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }
            if (string.IsNullOrEmpty(getData.Email))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }
            else
            {
                if (!Regex.IsMatch(getData.Email, @"^[\w-]+(\.[\w-]+)*@[\w-]+(\.[\w-]+)+$"))
                {
                    return new ApiResult
                    {
                        Code = ApiResultCode.InputError
                    };
                }
            }

            if (!string.IsNullOrEmpty(getData.Mobile))
            {
                if (!Regex.IsMatch(getData.Mobile, @"^09[0-9]{8}$"))
                {
                    return new ApiResult
                    {
                        Code = ApiResultCode.InputError
                    };
                }
            }

            string errMsg = string.Empty; ;

            errMsg = MemberFacade.UpdateMemberInfo(User.Identity.Id, getData.LastName, getData.FirstName, getData.Email, getData.Mobile);

            if (!string.IsNullOrEmpty(errMsg))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = errMsg
                };
            }

            return new ApiResult
            {
                Code = ApiResultCode.Success,
            };
        }

        /// <summary>
        /// 取得問題類別列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ApiResult GetCategoryList(GetCategoryListInput input)
        {
            if (input.UserId == null)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }
            if (User.Identity.Id == 0)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.UserNoSignIn
                };
            }

            var data = CustomerServiceFacade.GetCategoryList(input.CategoryId);

            if (!data.Category.Any())
            {
                return new ApiResult
                {
                    Code = ApiResultCode.DataNotFound,
                };
            }

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = data
            };
        }

        /// <summary>
        /// 取得客服對話ByServiceNo
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ApiResult GetCommunicateListByServiceNo(ApiGetData getData)
        {
            if (getData.UserId == null)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }
            if (User.Identity.Id == 0)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.UserNoSignIn
                };
            }
            if (string.IsNullOrEmpty(getData.ServiceNo))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }

            var data = CustomerServiceFacade.GetCommunicateListByServiceNo(User.Identity.Id, getData.ServiceNo, getData.IsGetAll, getData.PageCnt);

            return data.Communicate.Any()
                ? new ApiResult { Code = ApiResultCode.Success, Data = data }
                : new ApiResult { Code = ApiResultCode.DataNotFound };
        }

        /// <summary>
        /// 取得客服對話ByOrderGuid
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ApiResult GetCommunicateListByOrderGuid(ApiGetData getData)
        {
            if (getData.UserId == null)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }
            if (User.Identity.Id == 0)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.UserNoSignIn
                };
            }

            if (string.IsNullOrEmpty(getData.OrderGuid))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }

            var data = CustomerServiceFacade.GetCommunicateListByOrderGuid(User.Identity.Id, getData.OrderGuid, getData.IsGetAll, getData.PageCnt);

            if (data.Communicate.Count() == 0 && data.OrderId == "")
            {
                return new ApiResult
                {
                    Code = ApiResultCode.DataNotFound,
                };
            }
            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = data
            };
        }


        #region 手動執行

        /// <summary>
        /// 標記新檔
        /// </summary>
        /// <param name="isFirstExec"></param>
        /// <param name="sellerGuid"></param>
        /// <returns></returns>
        [DeveloperOnly]
        public ApiResult ProcessDealSellerRelationship(bool isFirstExec, Guid? sellerGuid)
        {
            bool isSuccess = SellerFacade.ProcessDealSellerRelationship(isFirstExec, sellerGuid);

            return new ApiResult { Code = isSuccess ? ApiResultCode.Success : ApiResultCode.Error };
        }

        /// <summary>
        /// 一次性 通知商家 Google shopping Ads 相關規定
        /// </summary>
        /// <returns></returns>
        [DeveloperOnly]
        [HttpPost, HttpGet]
        public ApiResult GsaNotifyToSeller()
        {
            int sellerCount = EmailFacade.GsaNotifyToSeller();
            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Message = "通知商家共計 " + sellerCount + " 筆"
            };
        }

        /// <summary>
        /// 手動執行
        /// </summary>
        /// <returns></returns>
        [DeveloperOnly]
        [HttpPost]
        public ApiResult ManualExecuteTask(ManualExecuteTaskInput input)
        {
            var result = new ApiResult { Code = ApiResultCode.Success };
            switch (input.BatchTask)
            {
                case BatchTask.New:
                    EmailFacade.SendNewShipOrderToSeller(input.Days);
                    break;
                case BatchTask.Expiring:
                    EmailFacade.SendExpiringOrdersToSeller(false);
                    break;
                case BatchTask.Expired:
                    EmailFacade.SendExpiringOrdersToSeller(true);
                    break;
                case BatchTask.SetLastShipDateOnOrderNotYetShipped:
                    int totalCount = OrderFacade.SetLastShipDateOnOrderNotYetShipped();
                    result.Data = "新增 " + totalCount + " 筆訂單最後出貨日";
                    return result;
            }

            return new ApiResult
            {
                Code = ApiResultCode.Success
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [DeveloperOnly]
        [HttpPost, HttpGet]
        public ApiResult SendTransferMailNoReplyToCs()
        {
            var now = DateTime.Now;
            //30分已讀未回
            int minute = now.Minute < 30 ? 0 : 30;
            DateTime endTime = new DateTime(now.Year, now.Month, now.Day, now.Hour, minute, 0);
            DateTime startTime = endTime.AddMinutes(-30);

            int notifyCount = EmailFacade.SendTransferNoReplyToCs(startTime, endTime);

            pp.ChangeLogInsert("CustomerServiceTransferNoReplyJob", "",
                startTime.ToString("yyyy-MM-dd HH:mm") + "~" + endTime.ToString("yyyy-MM-dd HH:mm") +
                " Job 寄出廠商已讀未回通知共: " + notifyCount + " 筆");

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = new
                {
                    StartTime = startTime,
                    EndTime = endTime,
                    C1 = "寄出廠商已讀未回通知共: " + notifyCount + " 筆",
                }
            };
        }

        public class ManualExecuteTaskInput
        {
            /// <summary>
            /// Task
            /// </summary>
            public BatchTask BatchTask { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public double Days { get; set; }
        }

        public enum BatchTask
        {
            /// <summary>
            /// 新訂單
            /// </summary>
            New = 1,
            /// <summary>
            /// 即將逾期
            /// </summary>
            Expiring = 2,
            /// <summary>
            /// 已逾期
            /// </summary>
            Expired = 3,
            /// <summary>
            /// 為未出貨訂單壓上最後出貨日
            /// </summary>
            SetLastShipDateOnOrderNotYetShipped = 4
        }

        #endregion

        #region 台新

        /// <summary>
        /// Ts問題紀錄列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Entrepot)]
        public ApiResult GetTsIssueList(GetTsIssueListCollection request)
        {
            #region Validation

            if (!ModelState.IsValid)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }

            #endregion

            var data = CustomerServiceFacade.GetTsIssueList(request);

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = data
            };
        }

        /// <summary>
        /// 建立Ts客服問題
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Entrepot)]
        public ApiResult CreateTsIssue(CreateTsIssueCollection request)
        {
            int userId = 0;
            if (!string.IsNullOrEmpty(request.TsMemberNo))
            {                
                OAuthClientModel client = OAuthFacade.GetClient(this.AppId);
                //get or add entrepot_member
                var entrepotMember = EntrepotFacade.MemberGetByToken(request.TsMemberNo, client.Id);
                if (entrepotMember == null || entrepotMember.Id <= 0)
                {
                    return new ApiResult
                    {
                        Code = ApiResultCode.Error,
                        Message = "虛擬帳號建立失敗"
                    };
                }
                userId = entrepotMember.UserId;
            }
            else
            {
                return new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = "虛擬帳號建立失敗"
                };
            }

            #region Validation

            if (!ModelState.IsValid)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }

            #endregion

            var preTsIssue = new PreTsIssue(request);


            var result = CustomerServiceFacade.CreateTsIssue(userId, preTsIssue);

            if (string.IsNullOrEmpty(result))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.SaveFail,
                };
            }
            else
            {
                return new ApiResult
                {
                    Code = ApiResultCode.Success,
                    Data = result
                };
            }


        }

        /// <summary>
        /// 建立Ts訂單問題
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Entrepot)]
        public ApiResult CreateTsOrderIssue(CreateTsOrderIssueCollection request)
        {

            #region Validation

            if (!ModelState.IsValid)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }

            #endregion

            var preTsIssue = new PreTsIssue(request);

            var result = CustomerServiceFacade.CreateTsOrderIssue(preTsIssue);

            if (string.IsNullOrEmpty(result))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.SaveFail,
                };
            }
            else
            {
                return new ApiResult
                {
                    Code = ApiResultCode.Success,
                    Data = result
                };
            }


        }

        /// <summary>
        /// 取得客服對話ByServiceNo(台新)
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Entrepot)]
        public ApiResult GetTsCommunicateListByServiceNo(GetTsCommunicateListByServiceNoCollection request)
        {

            #region Validation

            if (!ModelState.IsValid)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }

            #endregion

            var preCommunicate = new PreCommunicate(request);

            var data = CustomerServiceFacade.GetTsCommunicateListByServiceNo(preCommunicate);

            return data.Communicate.Any()
                ? new ApiResult { Code = ApiResultCode.Success, Data = data }
                : new ApiResult { Code = ApiResultCode.DataNotFound };
        }

        /// <summary>
        /// 取得客服對話ByOrderGuid(台新)
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Entrepot)]
        public ApiResult GetTsCommunicateListByOrderGuid(GetTsCommunicateListByOrderGuidCollection request)
        {

            #region Validation

            if (!ModelState.IsValid)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }

            #endregion

            var preCommunicate = new PreCommunicate(request);

            var data = CustomerServiceFacade.GetTsCommunicateListByOrderGuid(preCommunicate);

            if (!data.Communicate.Any() && data.OrderId == "")
            {
                return new ApiResult
                {
                    Code = ApiResultCode.DataNotFound,
                };
            }

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = data
            };
        }

        /// <summary>
        /// 取得問題分類列表 0 = 一般問題列表
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Entrepot)]
        public ApiResult GetTsCategoryList(GetTsCategoryListCollection collection)
        {

            if (!ModelState.IsValid)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }

            var data = CustomerServiceFacade.GetCategoryList(collection.CategoryType);

            if (!data.Category.Any())
            {
                return new ApiResult
                {
                    Code = ApiResultCode.DataNotFound,
                };
            }

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = data
            };
        }

        [HttpPost]
        [OAuthScope(TokenScope.Entrepot)]
        public ApiResult GetServiceInsideLog(InsideLogInput input) 
        {
            var data = sp.GetInsideLogByServiceNo(input.ServiceNo).Select(x=> new InsideLogData(x));
            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = data
            };
        }

        #endregion
    }
}

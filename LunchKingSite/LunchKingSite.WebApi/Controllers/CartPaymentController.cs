﻿using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebApi.Core.Attributes;
using LunchKingSite.WebLib.Component;
using LunchKingSite.Core.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Http;
using PaymentType = LunchKingSite.Core.PaymentType;
using System.Transactions;
using System.Web;
using LunchKingSite.WebApi.Core.OAuth;

namespace LunchKingSite.WebApi.Controllers
{

    /// <summary>
    /// MakeOrder
    /// </summary>
    [RequireHttps]
    [ForceCamelCase]
    public class CartPaymentController : BaseController
    {
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();        
        private static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private static ILog logger = LogManager.GetLogger("CartPayService");

        #region Property
        /// <summary>
        /// 使用者UniqueId
        /// </summary>
        public int UserId
        {
            get
            {
                WebLib.Component.PponIdentity pponUser = HttpContext.Current.User.Identity as WebLib.Component.PponIdentity;
                if (pponUser == null)
                {
                    return 0;
                }
                else
                {
                    return pponUser.Id;
                }
            }
        }
        #endregion

        #region Api
        /// <summary>
        /// 取得SessionId
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Deal)]
        public ApiResult GetTicketId()
        {
            ApiResult result = new ApiResult();

            string TicketId = "";

            if (!string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name))
            {
                //使用者已登入
                TicketId = CartFacade.GetTicketId(HttpContext.Current.User.Identity.Name);
            }
            else
            {
                //使用者未登入
                TicketId = CartFacade.GetTicketId(null);
            }

            result.Code = ApiResultCode.Success;
            result.Data = TicketId;

            return result;
        }
        /// <summary>
        /// MakeOrderByApiUserCart
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Deal)]
        public ApiReturnObject MakeOrderByApiUserCart(MainCartDTO mainCartDTO)
        {
            var rtnObject = new ApiReturnObject();
            string userName = HttpContext.Current.User.Identity.Name;

            if (string.IsNullOrWhiteSpace(userName))
            {
                rtnObject.Code = ApiReturnCode.UserNoSignIn;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeUserNoSignIn;
                return rtnObject;
            }

            mainCartDTO.TransactionId = PaymentFacade.GetTransactionId();

            //foreach (SmallCart subCart in mainCartDTO.smallCart)
            //{
            //    //處理訂單資料
            //    rtnObject = MakeOrderByCart(subCart.paymentDTO, userName, true);

            //    //處理每台購物車的運費
            //    ViewPponDealCollection vpds = pp.ViewPponDealGetBySellerGuid(subCart.SellerGuid);
            //    //ViewPponDeal freVpd = vpds.Where(x => x.IsFreightsDeal == true).FirstOrDefault();
            //    ViewPponDeal freVpd = vpds.FirstOrDefault();
            //    if (freVpd != null)
            //    {                    
            //        string fticketId = OrderFacade.MakeRegularTicketId(subCart.paymentDTO[0].SessionId, freVpd.BusinessHourGuid) + "_" + Path.GetRandomFileName().Replace(".", "");
            //        ViewPponDeal theDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(freVpd.BusinessHourGuid);
            //        MakeFreights(userName, freVpd.BusinessHourGuid, fticketId, theDeal, OrderFromType.ByMweb);
            //    }

            //}

            rtnObject = CartFacade.MakeOrderByCart(mainCartDTO, userName, true, true);
            return rtnObject;
        }


        /// <summary>
        /// 將商品放入購物車
        /// </summary>
        /// <param name="cartItemDTO"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Deal)]
        public ApiResult PutItemToCart(CartItemDTO cartItemDTO)
        {
            string userName = HttpContext.Current.User.Identity.Name;
            string ticketId = cartItemDTO.TicketId;
            if (string.IsNullOrWhiteSpace(userName))
            {
                //throw new Exception(String.Format("user {0} not found", userName));
            }
            ApiResult result = new ApiResult();

            LiteMainCart mainCartDTO = CartFacade.LiteCartListGet(cartItemDTO, userName, ticketId);

            result.Code = ApiResultCode.Success;
            result.Data = mainCartDTO;

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Deal)]
        public ApiResult GetCartItemCount(paramCartTicketId param)
        {
            string userName = HttpContext.Current.User.Identity.Name;
            if (string.IsNullOrWhiteSpace(userName))
            {
                //throw new Exception(String.Format("user {0} not found", userName));
            }
            ApiResult result = new ApiResult();

            int cnt = CartFacade.LiteCartListGetCount(null, userName, param.TicketId);


            result.Code = ApiResultCode.Success;
            result.Data = cnt;

            return result;
        }



        /// <summary>
        /// 移除購物車中商品
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Deal)]
        public ApiResult RemoveItemFromCart(paramDeleteOrder param)
        {
            string UserName = HttpContext.Current.User.Identity.Name;
            if (!string.IsNullOrWhiteSpace(UserName))
            {
                //throw new Exception(String.Format("user {0} not found", userName));
            }
            ApiResult result = new ApiResult();

            bool flag = Convert.ToBoolean(CartFacade.RemoveItemFromCart(param.Tid, param.Oid, UserName, UserId)[0]);

            result.Code = ApiResultCode.Success;
            result.Data = flag;

            return result;
        }

        /// <summary>
        /// 購物結帳頁
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Deal)]
        public ApiResult GetCartItem(paramCartTicketId param)
        {
            string userName = HttpContext.Current.User.Identity.Name;
            if (string.IsNullOrWhiteSpace(userName))
            {
                //throw new Exception(String.Format("user {0} not found", userName));
            }
            ApiResult result = new ApiResult();

            LiteMainCart mainCartDTO = CartFacade.LiteCartListGet(null, userName, param.TicketId);


            result.Code = ApiResultCode.Success;
            result.Data = mainCartDTO;

            return result;
        }

        /// <summary>
        /// 訂單明細頁
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Deal)]
        public ApiResult GetOrderList()
        {
            string userName = HttpContext.Current.User.Identity.Name;
            if (string.IsNullOrWhiteSpace(userName))
            {
                throw new Exception(string.Format("user {0} not found", userName));
            }
            ApiResult result = new ApiResult();

            List<MainCartDTO> mainCartList = new List<MainCartDTO>();

            CartItemDTO cartItemDTO = new CartItemDTO()
            {
                BusinessHourGuid = Guid.Parse("06d14e43-32b4-4b69-9499-6d5701de3294"),
            };


            MainCartDTO mainCartDTO = new MainCartDTO();//  CartFacade.SmallCartListGet(cartItemDTO, userName, cartItemDTO.TicketId);

            mainCartList.Add(mainCartDTO);

            result.Code = ApiResultCode.Success;
            result.Data = mainCartList;

            return result;
        }

        /// <summary>
        /// 湊單檔次
        /// </summary>
        /// <param name="param">參數</param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Deal)]
        public ApiResult OrderAddOn(paramAddOn param)
        {
            ApiResult result = new ApiResult();
            List<IViewPponDeal> vpds = CartFacade.OrderAddOn(param.Sid, param.Fid);

            result.Code = ApiResultCode.Success;
            result.Data = vpds;

            return result;
        }

        #endregion Api        
    }


}
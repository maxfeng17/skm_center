﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using Newtonsoft.Json.Linq;
using LunchKingSite.DataOrm;
using LunchKingSite.Core.Component;
using LunchKingSite.WebApi.Core.Attributes;

namespace LunchKingSite.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [RequireHttps(false)]
    [MvcStyleBinding]//不要再使用MvcStyleBinding
    public class ApplicationController : BaseController
    {
        private static ISystemProvider ss;
        /// <summary>
        /// 
        /// </summary>
        public ApplicationController()
        {
            ss = ProviderFactory.Instance().GetProvider<ISystemProvider>();
        }

        /// <summary>
        /// 取得系統各版本號(目前ios走get android 走post,過渡時期兩個都開,帶一個時間後改為只有post)
        /// </summary>
        /// <param name="appname"></param>
        /// <param name="version"></param>
        /// <param name="osversion"></param>
        /// <returns></returns>
        [HttpGet, HttpPost]
        public ApiResult Version(string appname, string version, string osversion = "")
        {
            Version inputVersion;
            if (!System.Version.TryParse(version, out inputVersion))
            {
                //轉型失敗，回傳錯誤
                return new ApiResult
                {
                    Code = ApiResultCode.InputError,
                    Message = I18N.Phrase.ApiReturnCodeInputError
                };
            }

            List<ApiVersionItem> versionList = new List<ApiVersionItem>();
            //頻道的Category
            versionList.Add(CategoryManager.PponChannelCategoryVersion);
            //城市與鄉鎮市區
            versionList.Add(LocationApiManager.CityItemVersion);
            //優惠券的地區 VourcherRegionVersion
            versionList.Add(ApiVourcherManager.ApiVourcherRegionVersion);
            //銀行列表
            versionList.Add(BankManager.BankNoDataVersion);
            //熟客卡區域
            versionList.Add(ApiMembershipCardRegionManager.MembershipCardRegionVersion);
            //熟客卡分類
            versionList.Add(CategoryManager.MembershipCardCategoryVersion);
            //個人資料蒐集告知事項
            versionList.Add(ApiPCPManager.PersonalDataCollectMatterFormatVersion);

            CheckAppVersionReply versionReply;

            if (!string.IsNullOrEmpty(osversion))
            {
                //version format:major.minor[.build[.revision]]
                if (osversion.IndexOf('.') == -1)
                {
                    osversion = osversion + ".0";
                }
                Version verOsversion;
                if (System.Version.TryParse(osversion, out verOsversion))
                {
                    versionReply = ApiSystemManager.CheckAppVersion(appname, inputVersion, verOsversion);
                }
                else
                {
                    versionReply = ApiSystemManager.CheckAppVersion(appname, inputVersion);
                }
            }
            else
            {
                versionReply = ApiSystemManager.CheckAppVersion(appname, inputVersion);
            }

            string appConfigSettingJson = ApiSystemManager.GetAppVersionCollection().Where(x => appname.StartsWith(x.AppName))
                .Select(x => x.AppConfig).FirstOrDefault();

            dynamic appConfig = null;

            if (!string.IsNullOrEmpty(appConfigSettingJson))
            {
                try
                {
                    appConfig = ProviderFactory.Instance().GetSerializer().DeserializeDynamic(appConfigSettingJson);
                }
                catch
                {
                    return new ApiResult
                    {
                        Code = ApiResultCode.Error,
                        Message = "AppConfig 格式錯誤"
                    };
                }
            }
            return new ApiResult()
            {
                Code = ApiResultCode.Success,
                Data = new
                {
                    DataVersion = versionList,
                    AppVersionCheck = versionReply,
                    AppConfig = appConfig
                }
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ob"></param>
        [HttpPost]
        public void GetGaToken(JObject ob)
        {
            try
            {
                var dtoModel = ob.ToObject<KeyValuePair<bool, KeyValuePair<string, DateTime>>>();
                if (dtoModel.Key)
                {
                    SystemCode systemCode = ss.SystemCodeGetByCodeGroupId(SingleOperator.GoogleAnalytics.ToString(), 1);
                    if (string.IsNullOrEmpty(systemCode.CodeGroup))
                    {
                        systemCode.CodeGroup = SingleOperator.GoogleAnalytics.ToString();
                        systemCode.CodeGroupName = "GoogleAnalytics Token";
                        systemCode.CodeId = 1;
                        systemCode.Enabled = true;
                        systemCode.CreateTime = DateTime.Now;
                        systemCode.ShortName = "GoogleAnalytics Token";
                        systemCode.CodeName = "GA Token";
                    }
                    else
                    {
                        systemCode.ModifyTime = DateTime.Now;
                    }
                    systemCode.Code = dtoModel.Value.Key;
                    systemCode.ExpiredTime = dtoModel.Value.Value;
                    ss.SystemCodeSet(systemCode);
                }
            }
            catch
            {
                //do nothing
            }
        }
    }
}

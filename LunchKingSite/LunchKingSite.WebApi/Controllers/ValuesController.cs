﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace LunchKingSite.WebApi.Controllers
{
    [Authorize]
    public class ValuesController : ApiController
    {
        // GET api/values
        [HttpGet]
        [HttpPost]
        public IEnumerable<string> GetValues()
        {
            return new string[] { "value1", "value2" };
        }

        public IEnumerable<string> Post()
        {
            return new string[] { "value1", "value2" };
        }

    }
}

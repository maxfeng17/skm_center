﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Http;
using System.Drawing;
using System.Text;
using System.Web;
using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Component.PCP;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.Interface;
using LunchKingSite.DataOrm;
using LunchKingSite.WebApi.Core.Attributes;
using LunchKingSite.WebApi.Core.OAuth;
using LunchKingSite.WebApi.Models;
using LunchKingSite.WebApi.Models.PCPVerifyModels;
using LunchKingSite.WebLib.Component;
using Newtonsoft.Json.Linq;
using Helper = LunchKingSite.Core.Helper;

namespace LunchKingSite.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [RequireHttps]
    public class PCPController : BaseController
    {
        private ILog logger = LogManager.GetLogger("pcp");
        private ISellerProvider sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private IPCPProvider pcp = ProviderFactory.Instance().GetProvider<IPCPProvider>();
        private IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        private INotificationProvider np = ProviderFactory.Instance().GetProvider<INotificationProvider>();
        private IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        #region 取得熟客點與公關點的所有交易及使用歷程

        /// <summary>
        /// 查詢商家的熟客點與公關點餘額摘要
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public ApiResult PointLogOverview()
        {
            ApiResult result = new ApiResult();
            int userId = MemberFacade.GetUniqueId(User.Identity.Name);

            if (userId == 0)
            {
                result.Code = ApiResultCode.BadUserInfo;
                result.Message = "錯誤的使用者資訊";

                return result;
            }

            List<IEnumerable<ViewPcpPointRecord>> collections = BonusFacade.GetStorePcpPointRecordsGroupByType(userId);
            List<int> theLockedPointList = BonusFacade.GetStoreLockedPointList(userId);
            PcpPointLogOverview theOverview = new PcpPointLogOverview
            {
                RegularsPointExpireTime =
                    collections[(int)PcpPointType.RegularsPoint].Any()
                        ? (collections[(int)PcpPointType.RegularsPoint].TakeWhile(r => r.ExpireTime != null)).First().ExpireTime
                        : null,
                RegularsPoint = collections[(int)PcpPointType.RegularsPoint].Any() ? collections[(int)PcpPointType.RegularsPoint].Last().Total.Value : 0,
                RegularsLockPoint = theLockedPointList[(int)PcpPointType.RegularsPoint],
                FavorPoint = collections[(int)PcpPointType.FavorPoint].Any() ? collections[(int)PcpPointType.FavorPoint].Last().Total.Value : 0,
                FavorLockPoint = theLockedPointList[(int)PcpPointType.FavorPoint],
                FavorPointExpireTime = collections[(int)PcpPointType.FavorPoint].Any() ? collections[(int)PcpPointType.FavorPoint].Last().ExpireTime : null
            };

            result.Data = theOverview;
            result.Code = ApiResultCode.Success;

            return result;
        }

        /// <summary>
        /// 查詢商家的熟客點與公關點交易及使用的月份摘要
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public ApiResult PointMonthlyReport([FromBody] PcpPointMonthlyReportInputModel theInputModel)
        {
            ApiResult result = new ApiResult();
            int userId = MemberFacade.GetUniqueId(User.Identity.Name);

            if (userId == 0)
            {
                result.Code = ApiResultCode.BadUserInfo;
                result.Message = "錯誤的使用者資訊";

                return result;
            }
            //in case wrong parameters input
            if (theInputModel == null || theInputModel.Year < 1 || theInputModel.Month < 1 || theInputModel.Month > 12 || theInputModel.PointType < 0)
            {
                result.Code = ApiResultCode.InputError;
                result.Message = "參數錯誤";

                return result;
            }

            List<IEnumerable<ViewPcpPointRecord>> collections = BonusFacade.GetStorePcpPointRecordsGroupByType(userId);
            List<int> theLockedPointList = BonusFacade.GetStoreLockedPointList(userId);
            PcpPointMonthlyReport theReport = new PcpPointMonthlyReport
            {
                Year = theInputModel.Year,
                Month = theInputModel.Month,
                PointType = theInputModel.PointType,
                Point = collections[theInputModel.PointType].Any() ? collections[theInputModel.PointType].Last().Total.Value : 0,
                LockPoint = theLockedPointList[theInputModel.PointType],
                Items = new List<PcpBalanceLog>() //temporarilly return empty collection for app construction 
            };
            //測試資料補充
            int previouMonth = theInputModel.Month == 1 ? 12 : theInputModel.Month - 1;
            theReport.Items.Add(new PcpBalanceLog { LockPoint = 0, Point = 0, Title = string.Format("{0}月結算", theInputModel.Month) });
            theReport.Items.Add(new PcpBalanceLog { LockPoint = 0, Point = 0, Title = string.Format("{0}月結算", previouMonth) });

            result.Data = theReport;
            result.Code = ApiResultCode.Success;

            return result;
        }

        /// <summary>
        /// 查詢商家的熟客點與公關點交易及使用總列
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public ApiResult PointLogs([FromBody] PcpPointLogsInputModel theInputModel)
        {
            ApiResult result = new ApiResult();
            int userId = MemberFacade.GetUniqueId(User.Identity.Name);

            if (userId == 0)
            {
                result.Code = ApiResultCode.BadUserInfo;
                result.Message = "錯誤的使用者資訊";

                return result;
            }

            //in case wrong parameters input
            if (theInputModel == null ||
                !((int[])Enum.GetValues(typeof(PcpPointType))).Contains(theInputModel.PointType))
            {
                result.Code = ApiResultCode.InputError;
                result.Message = "參數錯誤";

                return result;
            }

            List<IEnumerable<ViewPcpPointRecord>> collections = BonusFacade.GetStorePcpPointRecordsGroupByType(userId);

            //temporarilly return an empty object for app construction
            PcpPointLogs thePointLogs = new PcpPointLogs
            {
                PointType = 0,
                Logs = new List<PcpTransactionLog>()
            };
            //測試資料補充
            thePointLogs.Logs.Add(new PcpTransactionLog
            {
                Id = "D1000",
                LogTime = DateTime.Now.AddMinutes(-10),
                LogType = 1,
                Point = 100,
                Remind = "使用期限：2015/01/01～2015/01/31",
                Title = "當日熟客券兌換"
            });
            thePointLogs.Logs.Add(new PcpTransactionLog
            {
                Id = "D1001",
                LogTime = DateTime.Now.AddMinutes(-30),
                LogType = 1,
                Point = 100,
                Remind = "使用期限：2015/01/01～2015/01/31",
                Title = "當日熟客券兌換"
            });

            result.Data = thePointLogs;
            result.Code = ApiResultCode.Success;

            return result;
        }

        /// <summary>
        /// 查詢商家的熟客點與公關點凍結明細列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public ApiResult LockPoints([FromBody] PcpLockPointsInputModel theInputModel)
        {
            ApiResult result = new ApiResult();
            int userId = MemberFacade.GetUniqueId(User.Identity.Name);

            if (userId == 0)
            {
                result.Code = ApiResultCode.BadUserInfo;
                result.Message = "錯誤的使用者資訊";

                return result;
            }

            //in case wrong parameters input
            if (theInputModel == null ||
                !((int[])Enum.GetValues(typeof(PcpPointType))).Contains(theInputModel.PointType))
            {
                result.Code = ApiResultCode.InputError;
                result.Message = "參數錯誤";

                return result;
            }

            List<int> theLockedPointList = BonusFacade.GetStoreLockedPointList(userId);
            ViewPcpDiscountCampaignLockPointCollection theLockPointCollection =
                BonusFacade.GetStorePcpLockPointListOrderByStartTime(userId);

            PcpLockPoints theLockPoints;
            if (config.PCPApiDemo)
            {
                theLockPoints = new PcpLockPoints
                {
                    PointType = theInputModel.PointType,
                    TotalLockPoint = 100,
                    Logs = new List<PcpLockPointLog>(),
                };
                theLockPoints.Logs.Add(new PcpLockPointLog
                {
                    Id = 1000,
                    DiscountCampaignNo = "D201412010001",
                    Description = "D201412010001 滿1,000，出示公關券折抵100元",
                    StarTime = DateTime.Now.Date.AddDays(-5),
                    EndTime = DateTime.Now.Date.AddDays(5),
                    TotalLockPoint = 10000,
                    UsePoint = 500,
                    LockPoint = 500,
                });
                theLockPoints.Logs.Add(new PcpLockPointLog
                {
                    Id = 1000,
                    DiscountCampaignNo = "D201412010002",
                    Description = "D201412010002 滿500，出示公關券折抵50元",
                    StarTime = DateTime.Now.Date.AddDays(-5),
                    EndTime = DateTime.Now.Date.AddDays(5),
                    TotalLockPoint = 5000,
                    UsePoint = 500,
                    LockPoint = 1500,
                });
            }
            else
            {

                theLockPoints = new PcpLockPoints
                {
                    PointType = theInputModel.PointType,
                    TotalLockPoint = theLockedPointList[theInputModel.PointType],
                    Logs = theLockPointCollection.ToList().Select(x => new PcpLockPointLog
                    {
                        Id = x.Id,
                        DiscountCampaignNo = x.CampaignNo,
                        Description = x.Subject,
                        StarTime = x.StartTime.GetValueOrDefault(),
                        EndTime = x.EndTime.GetValueOrDefault(),
                        TotalLockPoint = x.LockPoint,
                        UsePoint = x.UsedPoint.GetValueOrDefault(),
                        LockPoint =
                            x.LockPoint - x.UsedPoint.GetValueOrDefault() > 0
                                ? x.LockPoint - x.UsedPoint.GetValueOrDefault()
                                : 0
                    }).ToList()
                };
            }

            result.Data = theLockPoints;
            result.Code = ApiResultCode.Success;

            return result;
        }

        /// <summary>
        /// 查詢商家的熟客點與公關點凍結明細列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public ApiResult PointLogDetail([FromBody] PcpPointLogDetailInputModel theInputModel)
        {
            ApiResult result = new ApiResult();
            int userId = MemberFacade.GetUniqueId(User.Identity.Name);

            if (userId == 0)
            {
                result.Code = ApiResultCode.BadUserInfo;
                result.Message = "錯誤的使用者資訊";

                return result;
            }


            //temporarilly return an empty object for app construction
            PcpPointLogDetail thePointLogDetail = new PcpPointLogDetail
            {
                Id = "D1000",
                Title = "當日熟客券兌換",
                PointType = 1,
                LogType = 1,
                LogTime = DateTime.Now,
                PaymentMethod = 1,
                Description = "交易明細項目顯示的文案",
                Amount = 100,
                TotalPoint = 100000,
                SubTotalPoint = 1000,
                DiscountCodeVerify = new List<PcpRedeemingLog>()
            };

            result.Data = thePointLogDetail;
            result.Code = ApiResultCode.Success;

            return result;
        }

        #endregion

        #region pcp 交易

        /// <summary>
        /// 驗證身份識別碼
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public ApiResult CheckPCPCode([FromBody] CheckPCPCodeInputModel model)
        {
            var result = new ApiResult();

            if (model == null || model.IsValid == false)
            {
                result.Code = ApiResultCode.InputError;
                result.Message = "參數錯誤";
                return result;
            }

            if (model.Code.Length != 10) //先判斷code 的格式，如果是userToken 
            {
                var ppus = PcpFacade.GetPollUserStatusByToken(model.Code, true);

                ////查無此token
                if (!ppus.IsLoaded)
                {
                    result.Code = ApiResultCode.OAuthTokerNotFound;
                    result.Message = "查無此Token";
                    return result;
                }

                if (ppus.ExpiredTime < DateTime.Now)
                {
                    ppus.Alive = (int)PcpPollingUserAlive.Dead;
                    pcp.PcpPollingUserStatusSet(ppus);
                    result.Code = ApiResultCode.OAuthTokenExpired;
                    result.Message = "QRCode已失效，請重新取得";
                    return result;
                }

                var mcg = PcpFacade.GetMembershipCardGroupBySellerGuid(model.StoreGuid);
                var userMemershipCard = pcp.ViewUserMembershipCardGet(ppus.UserId, mcg.Id);
                if (userMemershipCard.Any() == false)
                {
                    result.Code = ApiResultCode.MembershipCardNotFound;
                    return result;
                }
                //領取 identityCode
                var identityCode = BonusFacade.GenIdentityCode(userMemershipCard.First().CardId,
                       mcg.Id, DateTime.Now, ppus.UserId, 100);

                model.Code = identityCode.Code;
            }

            var sellerUserId = MemberFacade.GetUniqueId(User.Identity.Name);
            var viewIdentityCode = BonusFacade.GetViewIdentityCode(model.Code, sellerUserId, DateTime.Now);

            if (!BonusFacade.CheckViewIdentityCode(viewIdentityCode, out result, DateTime.Now))
            {
                return result;
            }

            #region 驗證會員卡是否可使用

            var mc = BonusFacade.MembershipCardGet(viewIdentityCode.CardId);
            if (!BonusFacade.CheckMembershipCardStatus(mc, out result, DateTime.Now))
            {
                return result;
            }

            if (!BonusFacade.MembershipCardGroupStoreGuidIsExists(mc.CardGroupId, model.StoreGuid))
            {
                result.Code = ApiResultCode.MembershipCardCanNotUsingThisStore;
                result.Message = "會員卡無法在這間店使用";
                return result;
            }

            #endregion

            //取得使用者在這間店可使用的抵用券
            var storeDiscountCode = new List<StoreDiscountCodeModel>();
            var discountCodes = BonusFacade.GetViewDiscountDetail(model.StoreGuid, viewIdentityCode.UserId,
                DiscountCampaignType.RegularsTicket);

            if (discountCodes.Any())
            {
                storeDiscountCode =
                    discountCodes.ToList().Select(x => new StoreDiscountCodeModel
                    {
                        CodeId = x.CodeId,
                        Type = (DiscountCampaignType)x.Type,
                        Selected = (viewIdentityCode.DiscountCodeId == x.CodeId),
                        CardCombineUse = x.CardCombineUse,
                        Amount = x.Amount ?? 0,
                        MinimumAmount = x.MinimumAmount ?? 0

                    }).ToList();
            }

            var pcpCode = new PCPCodeModel
            {
                IdentityCodeId = viewIdentityCode.Id,
                CardId = viewIdentityCode.UserMembershipCardId,
                Level = viewIdentityCode.Level,
                PaymentPercent = viewIdentityCode.PaymentPercent,
                OtherPremiums = viewIdentityCode.Others,
                DiscountCodes = storeDiscountCode,
                SuperBonus = BonusFacade.GetUserSuperBonus(User.Identity.Name)
            };

            result.Data = pcpCode;
            result.Code = ApiResultCode.Success;

            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public ApiResult Checkout([FromBody] CheckoutInputModel model)
        {
            var result = new ApiResult();

            if (model == null || model.IsValid == false)
            {
                result.Code = ApiResultCode.InputError;
                result.Message = "參數錯誤";
                return result;
            }

            #region 取得身份識別資料

            var identityCode = BonusFacade.GetViewIdentityCode(model.IdentityCodeId);

            if (!BonusFacade.CheckViewIdentityCode(identityCode, out result, DateTime.Now))
            {
                return result;
            }

            #endregion 取得身份識別資料

            #region 驗證會員卡是否可使用

            var mc = BonusFacade.MembershipCardGet(identityCode.CardId);
            if (!BonusFacade.CheckMembershipCardStatus(mc, out result, DateTime.Now))
            {
                return result;
            }

            var umc = BonusFacade.UserMembershipCardGet(identityCode.UserMembershipCardId);
            if (!BonusFacade.CheckUserMembershipCardStatus(umc, out result, DateTime.Now))
            {
                return result;
            }

            if (!BonusFacade.MembershipCardGroupStoreGuidIsExists(mc.CardGroupId, model.StoreGuid))
            {
                result.Code = ApiResultCode.MembershipCardCanNotUsingThisStore;
                result.Message = "會員卡無法在這間店使用";
                return result;
            }

            #endregion

            var discountCodeAmount = 0;

            #region 驗證抵用券

            if (model.DiscountCodeId != null && model.DiscountCodeId != 0)
            {
                int minimumAmount;
                var checkDiscountCode = PromotionFacade.GetDiscountCodeStatusForPcp((int)model.DiscountCodeId, DiscountCampaignUsedFlags.Partner,
                    model.Amount ?? 0, model.CardDiscount, MemberFacade.GetUserName(identityCode.UserId),
                    out minimumAmount, out discountCodeAmount);

                if (checkDiscountCode != DiscountCodeStatus.CanUse)
                {
                    result.Code = ApiResultCode.DiscountCodeError;

                    switch (checkDiscountCode)
                    {
                        case DiscountCodeStatus.CanNotCardCombineUse:
                            result.Message = "無法與熟客卡一起使用";
                            break;
                        case DiscountCodeStatus.Disabled:
                            result.Message = "已作廢";
                            break;
                        case DiscountCodeStatus.IsUsed:
                            result.Message = "已被使用";
                            break;
                        case DiscountCodeStatus.NotStartly:
                            result.Message = "時間尚未開始";
                            break;
                        case DiscountCodeStatus.OverTime:
                            result.Message = "已過期";
                            break;
                        case DiscountCodeStatus.OwnerUseOnlyWrong:
                            result.Message = "限本人使用";
                            break;
                        case DiscountCodeStatus.UnderMinimumAmount:
                            result.Message = "低於最低限制使用金額";
                            break;
                        case DiscountCodeStatus.UpToQuantity:
                            result.Message = "發送已達上限";
                            break;
                        case DiscountCodeStatus.Restricted:
                        case DiscountCodeStatus.NotAppplyAndStart:
                        case DiscountCodeStatus.NotApply:
                        case DiscountCodeStatus.None:
                        case DiscountCodeStatus.DiscountCodeCategoryWrong:
                            result.Message = "無法使用";
                            break;
                    }

                    return result;
                }
            }

            #endregion 驗證抵用券

            #region PCP交易

            Guid pcpOrderGuid;

            var check = BonusFacade.PcpTransCheckOut(model.IdentityCodeId, identityCode.UserMembershipCardId, model.Amount, model.CardDiscount
                , model.DiscountCodeId, discountCodeAmount, model.SuperBonus, identityCode.UserId, identityCode.SellerUserId
                , identityCode.CardId, model.StoreGuid, out pcpOrderGuid);

            switch (check)
            {
                case PcpCheckoutResult.Success:
                    result.Code = ApiResultCode.Success;
                    break;
                case PcpCheckoutResult.UpdateDiscountCodeFail:
                    result.Code = ApiResultCode.DiscountCodeError;
                    result.Message = "抵用券交易失敗";
                    break;
                case PcpCheckoutResult.NotEnoughBonus:
                    result.Code = ApiResultCode.PcpNotEnoughBonus;
                    result.Message = "無足夠的紅利點數可扣除";
                    break;
                case PcpCheckoutResult.SuperBonusOverAmount:
                    result.Code = ApiResultCode.PcpSuperBonusOverAmount;
                    result.Message = "超級紅利點折抵不可大於結帳金額";
                    break;
                case PcpCheckoutResult.PcpCodeIsUsed:
                    result.Code = ApiResultCode.PcpCodeIsUsed;
                    result.Message = "交易碼已使用過";
                    break;
                case PcpCheckoutResult.PcpCodeIsCancel:
                    result.Code = ApiResultCode.PcpCodeIsCancel;
                    result.Message = "交易碼已作廢";
                    break;
                default:
                    result.Code = ApiResultCode.PcpTransactionFail;
                    result.Message = "交易失敗";
                    break;
            }

            if (check != PcpCheckoutResult.Success)
            {
                return result;
            }

            #endregion PCP交易

            #region 會員卡升等

            var userId = MemberFacade.GetUniqueId(User.Identity.Name);
            var userMembershipCardLevel = BonusFacade.MembershipCardLevelUpCheck(mc, identityCode.UserMembershipCardId, userId, pcpOrderGuid);
            var isLevelUp = false;
            var levelUpMessage = string.Empty;

            if (userMembershipCardLevel > mc.Level)
            {
                isLevelUp = true;
                levelUpMessage = Helper.GetEnumDescription((MembershipCardLevel)userMembershipCardLevel);
            }

            #endregion 會員卡升等

            var viewVbsSellerMember = SellerFacade.GetViewVbsSellerMember(identityCode.SellerUserId, identityCode.UserId, null);

            DateTime theTime = DateTime.Now;
            ViewPcpDiscountTemplateCollection vdtsCol =
                pcp.ViewPcpDiscountTemplateGetList(
                    ViewPcpDiscountTemplate.Columns.SellerUserId + " = " + User.Identity.Id,
                    ViewPcpDiscountTemplate.Columns.StartTime + "<=" + theTime,
                    ViewPcpDiscountTemplate.Columns.EndTime + ">=" + theTime);

            CheckoutDiscountTemplateModel[] templates = CheckoutDiscountTemplateModel.Create(vdtsCol);

            result.Data = new CheckoutModel
            {
                CardId = identityCode.UserMembershipCardId,
                Level = userMembershipCardLevel,
                OrderCount = viewVbsSellerMember.OrderCount,
                AmountTotal = (int)viewVbsSellerMember.AmountTotal,
                LastUseTime = viewVbsSellerMember.LastUseTime ?? DateTime.Now,
                LastName = viewVbsSellerMember.SellerMemberLastName,
                FirstName = viewVbsSellerMember.SellerMemberFirstName,
                Mobile = viewVbsSellerMember.SellerMemberMobile,
                Gender = viewVbsSellerMember.SellerMemberGender ?? 0,
                Birthday = viewVbsSellerMember.SellerMemberBirthday,
                IsLevelUp = isLevelUp,
                LevelUpMsg = levelUpMessage,
                DiscountTemplates = templates
            };

            #region update polling status

            var ppus = PcpFacade.GetPollingStatusById(identityCode.UserId);
            ppus.Action = (int)PcpPollingUserActionType.Complete;
            ppus.ModifiedTime = DateTime.Now;
            ppus.UserMembershipCardId = identityCode.UserMembershipCardId;
            pcp.PcpPollingUserStatusSet(ppus);
            #endregion

            return result;
        }

        #endregion pcp 交易

        #region 熟客券

        /// <summary>
        /// 取得熟客券與公關券的活動設定資料，參數查詢類型與時間區間，搜尋截止時間在範圍內的資料。
        /// 
        /// /api/pcp/DiscountTemplates
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public ApiResult DiscountTemplates([FromBody]DiscountTemplatesInputModel model)
        {
            ApiResult result = new ApiResult();
            if (model == null || model.IsValid == false)
            {
                result.Code = ApiResultCode.InputError;
                result.Message = "參數錯誤";
                return result;
            }

            result.Code = ApiResultCode.Success;
            result.Message = "";

            if (config.IsPCPApiTestMode)
            {
                result.Data = DiscountTemplateModel.Create(null, true);
            }
            else
            {
                ViewPcpDiscountTemplateCollection vdtsCol = pcp.ViewPcpDiscountTemplateGetList(User.Identity.Id,
                    model.Status, model.BeginTime, model.EndTime);

                result.Data = DiscountTemplateModel.Create(vdtsCol);
            }

            return result;
        }
        /// <summary>
        /// 取得熟客券明細資料
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public ApiResult DiscountTemplateDetail([FromBody]DiscountTemplateDetailInputModel model)
        {
            ApiResult result = new ApiResult();
            if (model == null || model.IsValid == false)
            {
                result.Code = ApiResultCode.InputError;
                result.Message = "參數錯誤";
                return result;
            }

            if (config.IsPCPApiTestMode)
            {
                result.Data = DiscountTemplateDetailModel.Create(null, null, null, true);
            }
            else
            {
                ViewPcpDiscountTemplateCollection vpcdcCol = pcp.ViewPcpDiscountTemplateGetList(
                    model.TemplateId, User.Identity.Id);

                if (vpcdcCol.Count == 0)
                {
                    result.Code = ApiResultCode.DataNotFound;
                    result.Message = "找不到資料";
                    return result;
                }

                var allowStores = pcp.AllowStoreGetList(User.Identity.Id);
                ViewPcpAssignmentDiscountCampaignCollection vpadcCol = pcp.ViewPcpAssignmentDiscountCampaignGet(model.TemplateId);
                result.Data = DiscountTemplateDetailModel.Create(vpcdcCol, vpadcCol, allowStores, false);
            }

            result.Code = ApiResultCode.Success;
            return result;
        }
        /// <summary>
        /// 設定熟客券
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public ApiResult SetDiscountTemplate([FromBody] SetDiscountTemplateInputModel model)
        {
            ApiResult result = new ApiResult();
            if (model == null || model.IsValid == false)
            {
                result.Code = ApiResultCode.InputError;
                result.Message = "參數錯誤";
                return result;
            }

            DiscountTemplate template;
            if (model.TemplateId == 0)
            {
                template = new DiscountTemplate()
                {
                    IsDraft = true
                };
            }
            else
            {
                template = pcp.DiscountTemplateGet(model.TemplateId);
                if (template.IsDraft == false)
                {
                    result.Code = ApiResultCode.InputError;
                    result.Message = "已開始發送，不可修改";
                    return result;
                }
                if (template.SellerUserId != User.Identity.Id)
                {
                    result.Code = ApiResultCode.InputError;
                    result.Message = "無修改權限";
                    return result;
                }
            }

            try
            {
                Dictionary<Guid, Seller> allowStores = pcp.AllowStoreGetList(User.Identity.Id).ToDictionary(t => t.Guid);
                using (var trans = TransactionScopeBuilder.CreateReadCommitted())
                {
                    if (template.IsLoaded)
                    {
                        pcp.DiscountTemplateStoreDelete(template.Id);
                    }
                    template.Amount = model.Amount;
                    template.MinimumAmount = (int)model.MinimumAmount;
                    template.AvailableDateType = model.AvailableDateType;
                    template.StartTime = model.StartTime.GetValueOrDefault();
                    template.EndTime = model.EndTime.GetValueOrDefault();
                    template.CardCombineUse = model.CardCombineUse;
                    template.SituationType = model.SituationType;
                    template.SellerUserId = User.Identity.Id;
                    pcp.DiscountTemplateSet(template);
                    if (model.Stores != null)
                    {
                        foreach (Guid storeGuid in model.Stores)
                        {
                            if (allowStores.ContainsKey(storeGuid) == false)
                            {
                                continue;
                            }

                            DiscountTemplateStore ds = new DiscountTemplateStore
                            {
                                TemplateId = template.Id,
                                StoreGuid = storeGuid
                            };
                            pcp.DiscountStoreSet(ds);
                        }
                    }
                    trans.Complete();
                }

                result.Code = ApiResultCode.Success;
                return result;
            }
            catch (Exception ex)
            {
                logger.Error("SetDiscountTemplate error", ex);
                result.Code = ApiResultCode.SaveFail;

                return result;
            }
        }
        /// <summary>
        /// 取得熟客券或公關券的使用者列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public ApiResult DiscountCodeRecipients([FromBody] DiscountCodeRecipientsInputModel model)
        {
            ApiResult result = new ApiResult();

            if (model == null || model.IsValid == false)
            {
                result.Code = ApiResultCode.InputError;
                result.Message = "參數錯誤";
                return result;
            }

            result.Code = ApiResultCode.Success;

            if (config.IsPCPApiTestMode)
            {
                result.Data = DiscountCodeRecipientModel.Create(0, User.Identity.Id, true);
            }
            else
            {
                ViewDiscountDetailCollection vddCol = pcp.ViewDiscountDetailByCampiagnId(model.CampaignId,
                    User.Identity.Id);
                result.Data = DiscountCodeRecipientModel.Create(vddCol);
            }

            return result;
        }

        /// <summary>
        /// 直接發放某張熟客券給某個消費者
        /// </summary>
        /// <returns></returns>
        [Authorize]
        public ApiResult GiveDiscountCodeToMember([FromBody] GiveDiscountCodeToMemberInputModel model)
        {
            var result = new ApiResult();
            var userId = MemberFacade.GetUniqueId(User.Identity.Name);

            //會員卡是否存在
            var userCard = BonusFacade.UserMembershipCardVerify(model.UserCardId);
            if (!userCard.IsLoaded || userCard.SellerUserId != userId)
            {
                result.Code = ApiResultCode.MembershipCardNotFound;
                return result;
            }

            //會員卡是否啟用
            if (!userCard.Enabled)
            {
                result.Code = ApiResultCode.MembershipCardUnable;
                return result;
            }

            //會員卡是否過期
            if (!DateTime.Now.IsBetween(userCard.StartTime, userCard.EndTime))
            {
                result.Code = ApiResultCode.MembershipCardExpire;
                return result;
            }

            //驗證商家點數是否足夠
            var storePcpPoint = BonusFacade.GetStorePcpPointRecordsGroupByType(userId)[(int)model.PointType].ToList();
            var template = BonusFacade.DiscountTemplateGet(model.TemplateId);

            if (!storePcpPoint.Any() || (int)template.Amount > (storePcpPoint.Last().Total ?? 0))
            {
                switch (model.PointType)
                {
                    case PcpPointType.RegularsPoint:
                        result.Code = ApiResultCode.StoreRegularsPointNotEnough;
                        break;
                    case PcpPointType.FavorPoint:
                        result.Code = ApiResultCode.StoreFavorPointNotEnough;
                        break;
                    default:
                        result.Code = ApiResultCode.StorePcpPointNotEnough;
                        break;
                }
                return result;
            }

            result.Code = BonusFacade.GiveDiscountCodeToMember(userId, userId, userCard, template, model.PointType);

            return result;
        }

        #endregion

        #region 熟客卡

        /// <summary>
        /// 取得商家的熟客卡設定資訊
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public ApiResult GetCardsByQuarter([FromBody]JObject input)
        {
            var result = new ApiResult();

            Guid sellerGuid;
            try
            {
                Guid.TryParse(input["SellerGuid"].ToString(), out sellerGuid);
            }
            catch (Exception ex)
            {
                result.Code = ApiResultCode.InputError;
                result.Message = "熟客系統已調整，請更新到最新版本方可使用。";
                return result;
            }

            int year;
            int quarter;
            try
            {
                year = (int)input["Year"];
                quarter = (int)input["Quarter"];
            }
            catch
            {
                result.Code = ApiResultCode.InputError;
                result.Message = I18N.Phrase.ApiReturnCodeInputError;
                return result;
            }

            if (quarter > 4 || quarter < 1)
            {
                result.Code = ApiResultCode.InputError;
                result.Message = "請輸入正確季度(1~4)";
                return result;
            }

            int cardGroupId = 0;
            int versionId = 0;

            var vbsCompany = VbsMemberUtility.VbsCompanyDetailGet(sellerGuid);
            var membershipCardGroup = BonusFacade.GetMembershipCardGroupBySellerUserId(sellerGuid);
            if (membershipCardGroup.IsLoaded)
            {
                cardGroupId = membershipCardGroup.Id;
            }
            var version = BonusFacade.ViewMembershipCardGroupVersionGetByQuarter(membershipCardGroup.Id, year, (Quarter)quarter);

            //設定時間的預設值，若為version查無資料，預設回傳openTime closeTime
            DateTime versionOpenTime, versionCloseTime;
            Helper.GetQuarterDateRange(year, (Quarter)quarter, out versionOpenTime, out versionCloseTime);

            //view_membership_card_group_version 找不到資料代表未新增卡片
            var cards = new List<MembershipCard>();
            //當查詢的該季資料為null時，表示商家尚未新增任何熟客卡
            bool firstVersion = true;
            if (version != null && version.IsLoaded)
            {
                cards = BonusFacade.MembershipCardGetByGroupAndVersion(version.CardGroupId, version.Id).ToList();
                versionOpenTime = version.OpenTime;
                versionCloseTime = version.CloseTime;
                firstVersion = !cards.Any(x => x.Level > 0); //若沒有大於0的卡片代表第一次新增
                versionId = version.Id;
            }


            #region 此段邏輯有問題，不應該用帳號去撈分店資料；且分店已改從別的地方設定，此段待移除 By Bill 2017/3/10
            //分店資料
            //var userId = MemberFacade.GetUniqueId(User.Identity.Name);
            //var stores = BonusFacade.StoreGetByUserId(userId);
            //var membercardStore = BonusFacade.MembershipCardGroupStoreGet(stores.Select(x => x.Guid).ToList());
            //var storeModels = stores.Select(x => new OptionStoreModel
            //{
            //    Guid = x.Guid,
            //    StoreName = x.SellerName,
            //    Phone = x.StoreTel,
            //    Address = x.StoreAddress,
            //    OpenTime = x.OpenTime,
            //    CloseDate = x.CloseDate,
            //    Remarks = x.StoreRemark,
            //    Mrt = x.Mrt,
            //    Car = x.Car,
            //    Bus = x.Bus,
            //    OtherVehicles = x.OtherVehicles,
            //    WebUrl = x.WebUrl,
            //    FbUrl = x.FacebookUrl,
            //    PlurkUrl = string.Empty,
            //    BlogUrl = x.BlogUrl,
            //    OtherUrl = x.OtherUrl,
            //    CreditcardAvailable = x.CreditcardAvailable,
            //    Coordinates = ApiCoordinates.FromSqlGeography(LocationFacade.GetGeographyByCoordinate(x.Coordinate)),
            //    //如果有被此會員卡群組選取 true
            //    Selected = cardGroupId != -1 && versionId != -1 && membercardStore.Any(n => n.StoreGuid == x.Guid && n.CardGroupId == cardGroupId),
            //    //如果已被別的會員卡選取則 false (如果根本還未新增卡片，任何有被註冊的店家自然都為false)
            //    Enabled = cardGroupId != -1 ? (!membercardStore.Any(n => n.StoreGuid == x.Guid && n.CardGroupId != cardGroupId)) :
            //    (membercardStore.All(n => n.StoreGuid != x.Guid))

            //}).ToList();


            #endregion

            //相關圖片資料
            var imageData = VBSFacade.GetSellerImage(cardGroupId);

            //回傳資料
            var data = new MembershipCardQuarterModel();
            data.CardGroupId = cardGroupId;
            data.Version = versionId;
            data.SellerName = vbsCompany.SellerName;
            data.SellerImagePath = Helper.GetPropValue(VBSFacade.GetImagePathByType(imageData, PcpImageType.CorporateImage), "result").ToString();
            data.SellerFullCardPath = Helper.GetPropValue(VBSFacade.GetImagePathByType(imageData, PcpImageType.Card), "result").ToString();
            data.SellerFullCardThumbnailPath = Helper.GetPropValue(VBSFacade.GetImagePathByType(imageData, PcpImageType.Card, true), "result").ToString();
            data.SellerCardType = membershipCardGroup.CardType;
            data.SellerIntro = pcp.PcpIntroGet(PcpIntroType.Seller, cardGroupId).Select(x => x.IntroContent).ToArray();
            data.SellerServiceImgPath = GetImagePathByTypeWithOrder(imageData, PcpImageType.Service);
            data.SellerServiceImgThumbnailPath = GetImagePathByTypeWithOrder(imageData, PcpImageType.Service, true);
            data.SellerSrndgImgPath = GetImagePathByTypeWithOrder(imageData, PcpImageType.EnvironmentAroundStore);
            data.SellerSrndgThumbnailPath = GetImagePathByTypeWithOrder(imageData, PcpImageType.EnvironmentAroundStore, true);

            data.VersionOpenTime = versionOpenTime;
            data.VersionCloseTime = versionCloseTime;
            data.Cards = cards.Select(x => new MembershipCardModel(x)).ToList();
            //data.SelectedStoreCount = storeModels.Count(n => n.Selected);
            //data.Stores = storeModels;
            data.SelectedStoreCount = 0;
            data.Stores = new List<OptionStoreModel>();


            data.CardValidDate = vbsCompany.ContractValidDate.HasValue ? vbsCompany.ContractValidDate.Value : DateTime.Now.AddYears(1);
            if (firstVersion)
            {
                data.IsLoad = false;
                data.Status = (int)MembershipCardStatus.Draft;
                data.CardImage = string.Empty;
                data.BackgroundColorId = BonusFacade.DefaultBackgroundColorId;
                data.BackgroundImageId = BonusFacade.DefaultCardBackgroundImageId;
                data.IconImageId = BonusFacade.DefaultCardIconImageId;
            }
            else
            {
                data.IsLoad = true;
                data.Status = version.Status;
                data.CardImage = version.ImagePath;
                data.BackgroundColorId = version.BackgroundColorId;
                data.BackgroundImageId = version.BackgroundImageId;
                data.IconImageId = version.IconImageId;
            }

            #region 部分參數儲存於membership_card中，不過企劃修改後，定義為每個版本四張卡片需相同
            data.CombineUse = false;
            data.AvailableDateType = (int)LunchKingSite.Core.AvailableDateType.AllDay;
            if (cards.Count > 0)
            {
                var card = cards.First();
                data.CombineUse = card.CombineUse;
                data.AvailableDateType = card.AvailableDateType;
            }
            #endregion 部分參數儲存於membership_card中，不過企劃修改後，定義為每個版本四張卡片需相同

            result.Code = ApiResultCode.Success;
            result.Data = data;
            return result;
        }

        private static ImageDetail[] GetImagePathByTypeWithOrder(List<PcpImageInfo> datas, PcpImageType imgType, bool isCompressed = false)
        {
            var resultObjList = new ImageDetail[] { };

            switch (imgType)
            {
                case PcpImageType.Service:
                case PcpImageType.EnvironmentAroundStore:

                    if (datas.Where(x => x.ImgType == imgType).FirstOrDefault() == null)
                    {
                        break;
                    }

                    var tmpResult = new List<ImageDetail>();

                    if (isCompressed)
                    {
                        tmpResult = datas.Where(x => x.ImgType == imgType)
                            .Select(x => new ImageDetail { Seq = x.Seq, ImagePath = x.ImageCompressedPathUrl ?? x.ImagePathUrl }).ToList();
                    }
                    else
                    {
                        tmpResult = datas.Where(x => x.ImgType == imgType)
                            .Select(x => new ImageDetail { Seq = x.Seq, ImagePath = x.ImagePathUrl }).ToList();
                    }

                    resultObjList = tmpResult
                            .Select(x => new ImageDetail
                            {
                                Seq = x.Seq,
                                ImagePath = ImageFacade.GetMediaPath(x.ImagePath, MediaType.PCPImage),
                            }).OrderBy(x => x.Seq).ToArray();

                    break;
            }

            return resultObjList;
        }

        /// <summary>
        /// 新增或修改熟客卡資訊，Server端以OpenTime判斷屬於第幾季的異動資料，若日期設定有錯誤，無法進行異動。
        /// </summary>
        /// <param name="inputObj"></param>
        /// <returns></returns>
        //[VbsRoleValidate(VbsRoleEnum.PcpAdmin)]
        [HttpPost]
        [Authorize]
        public ApiResult SetCards([FromBody]PCPSetCardsModel inputObj)
        {
            var result = new ApiResult();
            if (inputObj == null)
            {
                result.Code = ApiResultCode.InputError;
                result.Message = I18N.Phrase.ApiResultCodeInputError;
                return result;
            }

            if (inputObj.SellerGuid == Guid.Empty)
            {
                result.Code = ApiResultCode.InputError;
                result.Message = "請先選擇開卡賣家資料";
                return result;
            }

            if (inputObj.Cards.Count > 4)
            {
                result.Code = ApiResultCode.InputError;
                result.Message = "輸入的卡片資料不正確";
                return result;
            }

            if (inputObj.Cards.GroupBy(x => x.Level).Any(grp => grp.Count() > 1))
            {
                result.Code = ApiResultCode.InputError;
                result.Message = "輸入的卡片等級有重複";
                return result;
            }

            var userId = MemberFacade.GetUniqueId(User.Identity.Name);

            if (inputObj.Cards.Where(x => x.CardId == null || x.CardId == 0).Any(item => BonusFacade.MembershipCardGet(inputObj.CardGroupId, item.CloseTime, item.Level).IsLoaded))
            {
                result.Code = ApiResultCode.InputError;
                result.Message = "輸入的卡片資料有重覆上架時間";
                return result;
            }

            int cardGroupId = inputObj.CardGroupId;
            int cardVersionId = inputObj.VersionId;

            #region 有傳卡片資料進來則進行卡片異動作業

            if (inputObj.Cards.Count > 0)
            {
                #region 驗證 card group id & version id, Insert or Update.

                var openTime = inputObj.Cards.OrderBy(x => x.OpenTime).First().OpenTime;
                DateTime closeTime, quarterTimeStart;
                Helper.GetQuarterDateRange(openTime.Year, (Quarter)Helper.GetQuarterByDate(openTime),
                    out quarterTimeStart, out closeTime);

                if (
                    !BonusFacade.CheckMembershipCardGroupIdAndVersionId(inputObj.SellerGuid, userId, inputObj.CardGroupId, inputObj.VersionId,
                        openTime, closeTime,
                        out cardGroupId, out cardVersionId))
                {
                    result.Code = ApiResultCode.InputError;
                    result.Message = "輸入的卡片群組或版本錯誤";
                    return result;
                }

                #endregion
                //AvailableDateType availableDateType = AvailableDateType.AllDay;
                foreach (var item in inputObj.Cards)
                {
                    var mc = (item.CardId != null && item.CardId != 0)
                        ? BonusFacade.MembershipCardGet((int)item.CardId)
                        : new MembershipCard();

                    if (mc.IsLoaded)
                    {
                        mc.ModifyId = userId;
                        mc.ModifyTime = DateTime.Now;
                    }
                    else
                    {
                        mc.CreateId = userId;
                        mc.CreateTime = DateTime.Now;
                    }
                    mc.CardGroupId = cardGroupId;
                    mc.VersionId = cardVersionId;
                    mc.Level = item.Level;
                    //狀態為作廢則enabled設為取消 TODO:須把enabled拿掉 改以status取代
                    mc.Enabled = item.Status != MembershipCardStatus.Cancel;
                    mc.PaymentPercent = item.PaymentPercent == 0 ? 1 : item.PaymentPercent;
                    mc.Others = item.OtherPremiums;
                    mc.OrderNeeded = item.OrderNeeded;
                    mc.AmountNeeded = (decimal?)item.AmountNeeded;
                    mc.ConditionalLogic = item.ConditionalLogic;
                    mc.OpenTime = item.OpenTime;
                    mc.CloseTime = item.CloseTime;
                    mc.Status = (byte)item.Status;
                    mc.Instruction = item.Instruction;
                    BonusFacade.MembershipCardSet(mc);
                    //修改所有卡片皆相同的資料
                    BonusFacade.MembershipCardUpdateSynchronizationData(cardGroupId, cardVersionId,
                        (AvailableDateType)inputObj.AvailableDateType, inputObj.CombineUse);

                    if (item.Level == (int)MembershipCardLevel.Level1 && DateTime.Now.IsBetween(item.OpenTime, item.CloseTime)
                        && item.Status == MembershipCardStatus.Open && mc.Enabled && mc.Id > 0)
                    {
                        //將有領取零級卡者升級一級卡片
                        pcp.UserMembershipCardUpdateLevelZeroToOne(cardGroupId, mc.Id);
                    }
                }
            }
            #endregion 有傳卡片資料進來則進行卡片異動作業

            if (BonusFacade.MembershipCardGroupStoreReset(cardGroupId, inputObj.StoreGuids))
            {
                result.Code = ApiResultCode.Success;
            }
            else
            {
                result.Code = ApiResultCode.MembershipCardStoreSaveError;
                result.Message = I18N.Phrase.ApiResultCodeMembershipCardStoreSaveError;
            }
            return result;
        }

        /// <summary>
        /// 發行卡片
        /// </summary>
        /// <param name="inputObj">需輸入CardId參數，為int型別</param>
        /// <returns></returns>
        //[VbsRoleValidate(VbsRoleEnum.PcpAdmin)]
        [HttpPost]
        [Authorize]
        public ApiResult IssueCard([FromBody]JObject inputObj)
        {
            var result = new ApiResult { Code = ApiResultCode.Success };

            if (inputObj == null)
            {
                result.Code = ApiResultCode.InputError;
                result.Message = I18N.Phrase.ApiResultCodeInputError;
                return result;
            }
            int cardId;
            try
            {
                cardId = (int)inputObj["CardId"];
            }
            catch
            {
                result.Code = ApiResultCode.InputError;
                result.Message = I18N.Phrase.ApiReturnCodeInputError;
                return result;
            }


            var userId = MemberFacade.GetUniqueId(User.Identity.Name);
            var card = BonusFacade.ViewMembershipCatdGet(cardId);

            if (card == null || card.SellerUserId != userId)
            {
                result.Code = ApiResultCode.MembershipCardNotFound;
                result.Message = "查無此會員卡";
                return result;
            }
            if (DateTime.Now >= card.CloseTime)
            {
                result.Code = ApiResultCode.MembershipCardExpire;
                result.Message = "卡片已超過有效時間，請修改後重新嘗試。";
                return result;
            }
            switch (card.Status)
            {
                case (int)MembershipCardStatus.Open:
                    result.Code = ApiResultCode.MembershipCardWasIssued;
                    result.Message = "熟客卡已發行";
                    return result;
                case (int)MembershipCardStatus.Cancel:
                    result.Code = ApiResultCode.MembershipCardWasCancelled;
                    result.Message = "熟客卡已作廢";
                    return result;
            }

            if (result.Code != ApiResultCode.Success) return result;

            var issueCard = BonusFacade.MembershipCardGet(cardId);
            issueCard.Status = (int)MembershipCardStatus.Open;
            if (issueCard.ReleaseTime == null)
            {
                issueCard.ReleaseTime = DateTime.Now;
            }

            BonusFacade.MembershipCardSet(issueCard);

            return result;
        }

        /// <summary>
        /// 設定卡片風格
        /// </summary>
        /// <param name="inputObj"></param>
        /// <returns></returns>
        //[VbsRoleValidate(VbsRoleEnum.PcpAdmin)]
        [HttpPost]
        [Authorize]
        public ApiResult SetCardStyle([FromBody]PCPSetCardStyleModel inputObj)
        {
            var result = new ApiResult();

            if (inputObj == null)
            {
                result.Code = ApiResultCode.InputError;
                result.Message = I18N.Phrase.ApiResultCodeInputError;
                return result;
            }
            if (BonusFacade.MembershipCardGroupVersionUpdateStyle(inputObj.CardVersionId, inputObj.BackgroundColorId, inputObj.BackgroundImageId, inputObj.IconImageId))
            {
                result.Code = ApiResultCode.Success;
            }
            else
            {
                result.Code = ApiResultCode.DataNotFound;
                result.Message = "查無會員卡版本資料";
            }

            return result;
        }

        #endregion 熟客卡

        #region 熟客卡會員

        /// <summary>
        /// virtual Token
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public ApiResult GetUserToken(MembershipCardCheckModel model)
        {
            int userId = User.Identity.Id;
            string cardQrCodeUrl;
            string cardQrCodeImage = string.Empty;

            var puk = PcpFacade.GetPcpUserToken(userId);
            var imageData = VBSFacade.GetSellerImage(model.GroupId);
            var mcg = pcp.MembershipCardGroupGet(model.GroupId);

            if (mcg.IsLoaded && mcg.IsPos) //ex:錢都
            {
                var userMembershipCard = pcp.UserMembershipCardGetByCardGroupIdAndUserId(model.GroupId, userId);
                cardQrCodeUrl = ApiPCPManager.CreateIdentityCodeUrl(userMembershipCard.Id, 0, userId);
                cardQrCodeImage =
                    Helper.GetPropValue(VBSFacade.GetImagePathByType(imageData, PcpImageType.Logo, true), "result")
                        .ToString();
            }
            else
            {
                cardQrCodeUrl = puk.RefreshToken ?? puk.AccessToken;
            }

            var result = new ApiResult
            {
                Data = new
                {
                    Token = puk.RefreshToken ?? puk.AccessToken,
                    ExpiredTime = ApiSystemManager.DateTimeToDateTimeString(puk.ExpiredTime),
                    CardQRCodeUrl = cardQrCodeUrl,
                    CardQRCodeImage = cardQrCodeImage,
                },
                Code = ApiResultCode.Success
            };
            return result;
        }

        /// <summary>
        /// check user status
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public ApiResult PollUserStatus(MembershipCardCheckModel model)
        {
            int userId = User.Identity.Id;
            var puk = PcpFacade.GetPcpUserTokenByToken(model.Token);
            var result = new ApiResult();

            //查無此token
            if (!puk.IsLoaded)
            {
                result.Code = ApiResultCode.OAuthTokerNotFound;
                result.Message = "查無此Token";
                return result;
            }

            if (puk.UserId != userId)
            {
                result.Code = ApiResultCode.OAuthTokerNoAuth;
                result.Message = "此Token非本人持有權限";
                return result;
            }

            var ppus = PcpFacade.GetPollUserStatusByToken(model.Token, true);
            if (ppus.ExpiredTime < DateTime.Now)
            {
                ppus.Alive = (int)PcpPollingUserAlive.Dead;
                pcp.PcpPollingUserStatusSet(ppus);
                result.Code = ApiResultCode.OAuthTokenExpired;
                result.Message = "QRCode已失效，請重新取得";
                return result;
            }


            StringBuilder sb = new StringBuilder();
            string storeName = string.Empty;
            var userMembershipCard = pcp.ViewUserMembershipCardGet(ppus.UserMembershipCardId ?? 0);


            if (userMembershipCard.IsLoaded)
            {
                #region 顯示使用者資訊(處理中)

                storeName = userMembershipCard.SellerName;

                //熟客優惠
                if (userMembershipCard.Level != (int)MembershipCardLevel.Level0)
                {
                    string cardlevel =
                    Helper.GetEnumDescription(
                        (MembershipCardLevel)Enum.ToObject(typeof(MembershipCardLevel), userMembershipCard.Level));
                    sb.AppendLine(string.Format("優惠:{0}", cardlevel));
                }

                //寄杯
                var userDeposit = PcpFacade.GetViewPcpUserDepositListByUserIdAndCardGroupId(ppus.UserId, userMembershipCard.CardGroupId).Where(x => x.RemainAmount > 0);
                sb.AppendLine(string.Format("寄杯:{0}", userDeposit.Any() ? userDeposit.Sum(x => x.RemainAmount) : 0));

                //集點
                var userPoint = pcp.PcpUserPointListGetByCardGroupId(userMembershipCard.CardGroupId, ppus.UserId);
                sb.Append(string.Format("集點:{0}", userPoint.Any() ? userPoint.Sum(x => x.RemainPoint) : 0));

                #endregion

            }

            var trans = new PcpTransResultModel();
            if (ppus.Action == (int)PcpPollingUserActionType.Complete)//交易完成後
            {
                trans = PcpFacade.GetPcpTransResult(ppus.UserId);
                //complete status
                ppus.Alive = (int)PcpPollingUserAlive.Dead;
                pcp.PcpPollingUserStatusSet(ppus);
            }

            result.Data = new PollingUserStatusModel()
            {
                PollingUserData = new PollingUserData()
                {
                    PollingId = ppus.Id,
                    ExpiredTime = ApiSystemManager.DateTimeToDateTimeString(ppus.ExpiredTime),
                    Action = ppus.Action,
                    UserCardId = ppus.UserMembershipCardId ?? 0
                },
                TransType = (int)trans.Type,
                TransMessage = trans.ResultMessage,
                TransTitle = trans.Title,
                GroupId = trans.GroupId,
                UserServiceInfo = sb.ToString(),
                StoreName = storeName
            };
            result.Code = ApiResultCode.Success;
            return result;
        }

        /// <summary>
        /// 新版一次性熟客卡條款
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ApiResult GetMembershipCardAgreement()
        {
            var result = new ApiResult();

            SystemData systemdata = SystemCodeManager.GetSystemDataByName("PersonalMemberShipCardAgree");

            result.Data = new
            {
                Version = ApiPCPManager.PersonalDataCollectMatterFormatVersion,
                MatterContent = systemdata.Data
            };
            result.Code = ApiResultCode.Success;
            return result;
        }

        /// <summary>
        /// 同意條款，取得熟客卡
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public ApiResult AcceptMembershipCard(MembershipCardCheckModel model)
        {
            var result = new ApiResult();
            var ppus = PcpFacade.GetPollUserStatusByToken(model.Token, true);
            ////查無此token
            if (!ppus.IsLoaded)
            {
                result.Code = ApiResultCode.OAuthTokerNotFound;
                result.Message = "查無此Token";
                return result;
            }

            if (ppus.ExpiredTime < DateTime.Now)
            {
                ppus.Alive = (int)PcpPollingUserAlive.Dead;
                pcp.PcpPollingUserStatusSet(ppus);
                result.Code = ApiResultCode.OAuthTokenExpired;
                result.Message = "QRCode已失效，請重新取得";
                return result;
            }

            //set argee status
            PcpUserAgreeStatus puas = new PcpUserAgreeStatus()
            {
                UserId = ppus.UserId,
                AgreeTime = DateTime.Now,
                SystemDataName = "PersonalMembershipCardAgree"
            };
            pcp.PcpUserAgreeStatusSet(puas);

            //同意完，polling status 改成start
            ppus.Action = (int)PcpPollingUserActionType.Start;
            ppus.ModifiedTime = DateTime.Now;
            pcp.PcpPollingUserStatusSet(ppus);

            result.Code = ApiResultCode.Success;
            result.Message = "熟客卡條款同意完成";
            return result;
        }

        /// <summary>
        /// 取得User 於熟客卡的服務內容，寄杯數、集點數
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public ApiResult GetUserServiceInfo(MembershipCardCheckModel model)
        {
            var result = new ApiResult();
            var ppus = PcpFacade.GetPollUserStatusByToken(model.Token, true);
            ////查無此token
            if (!ppus.IsLoaded)
            {
                result.Code = ApiResultCode.OAuthTokerNotFound;
                result.Message = "查無此Token";
                return result;
            }

            if (ppus.ExpiredTime < DateTime.Now)
            {
                ppus.Alive = (int)PcpPollingUserAlive.Dead;
                pcp.PcpPollingUserStatusSet(ppus);
                result.Code = ApiResultCode.OAuthTokenExpired;
                result.Message = "QRCode已失效，請重新取得";
                return result;
            }

            var mcg = PcpFacade.GetMembershipCardGroupBySellerGuid(model.StoreGuid);

            if (mcg.IsLoaded)
            {

                MembershipCardGroupPermissionCollection permissions = pcp.MembershipCardGroupPermissionGetList(mcg.Id);

                var data = new PcpUserServiceInfo()
                {
                    IsVipOn = PcpFacade.IsRegularsServiceOn(mcg.Id, permissions, MembershipService.Vip),
                    IsPointOn = PcpFacade.IsRegularsServiceOn(mcg.Id, permissions, MembershipService.Point),
                    IsDepositOn = PcpFacade.IsRegularsServiceOn(mcg.Id, permissions, MembershipService.Deposit),
                    IsVipSet = PcpFacade.IsRegularsServiceSet(mcg.Id, MembershipService.Vip),
                    IsPointSet = PcpFacade.IsRegularsServiceSet(mcg.Id, MembershipService.Point),
                    IsDepositSet = PcpFacade.IsRegularsServiceSet(mcg.Id, MembershipService.Deposit)
                };

                //集點
                var userPoint = pcp.PcpUserPointListGetByCardGroupId(mcg.Id, ppus.UserId);
                if (userPoint.Any())
                {
                    data.UserPoint = userPoint.Sum(x => x.RemainPoint);
                }

                //寄杯
                var userDeposit = PcpFacade.GetViewPcpUserDepositListByUserIdAndCardGroupId(ppus.UserId, mcg.Id);
                if (userDeposit.Any())
                {
                    data.UserDeposit = userDeposit.Sum(x => x.RemainAmount);
                }

                data.IsHistoryOn = userPoint.Any() || userPoint.Any();
                result.Data = data;
                result.Code = ApiResultCode.Success;
            }
            else
            {
                result.Code = ApiResultCode.PcpVbsSellerMemberNotFound;
            }

            return result;
        }

        #endregion

        #region 商家會員

        /// <summary>
        /// 確認使用者是否有此商家熟客卡
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public ApiResult CheckMembershipCardOwn(MembershipCardCheckModel model)
        {
            //掃描熟客卡的廠商
            var userId = MemberFacade.GetUniqueId(User.Identity.Name);
            var result = new ApiResult();
            var ppusForAll= PcpFacade.GetPollUserStatusByTokenWithLog(model.Token, false,userId);
            var ppus = PcpFacade.GetPollUserStatusByTokenWithLog(model.Token, true, userId);
            //var ppus = PcpFacade.GetPollUserStatusByToken(model.Token, true);

            #region check user
            //查無此token(不管是否Alive都沒有撈到此Token)
            if (ppusForAll==null)
            {
                result.Code = ApiResultCode.OAuthTokerNotFound;
                result.Message = "查無此Token";
                return result;
            }

            //查無此token(因為有判別是否Alive導致沒有撈到)
            if (ppus==null)
            {
                result.Code = ApiResultCode.OAuthTokenExpired;
                result.Message = "QRCode已失效，請重新取得";
                return result;
            }

            if (ppus.ExpiredTime < DateTime.Now)
            {
                ppus.Alive = (int)PcpPollingUserAlive.Dead;
                pcp.PcpPollingUserStatusSet(ppus);
                result.Code = ApiResultCode.OAuthTokenExpired;
                result.Message = "QRCode已失效，請重新取得";
                return result;
            }

            //確認是否有同意條款
            var paus = PcpFacade.GetPcpUserAgreeStatusById(ppus.UserId);
            if (!paus.IsLoaded)
            {
                ppus.Action = (int)PcpPollingUserActionType.Agree;
                pcp.PcpPollingUserStatusSet(ppus);
                result.Code = ApiResultCode.PcpAgreementNotAccept;
                result.Message = "使用者尚未同意條款";
                return result;
            }
            #endregion

            #region check seller
            var checkExist = pcp.UserMembershipCardGetByUserId(ppus.UserId);
            var mcg = PcpFacade.GetMembershipCardGroupBySellerGuid(model.StoreGuid);

            if (mcg.Status != (int)MembershipCardStatus.Open)
            {
                result.Code = ApiResultCode.MembershipCardCanNotUsingThisStore;
                result.Message = "目前此店家尚未發布熟客卡";
                return result;
            }

            if (!PcpFacade.MembershipCardGroupStoreGuidIsExists(mcg.Id, model.StoreGuid))
            {
                result.Code = ApiResultCode.MembershipCardCanNotUsingThisStore;
                result.Message = "目前此店家不適用熟客系統";
                return result;
            }

            //沒有則直接領 
            if (!checkExist.Any(x => x.CardGroupId == mcg.Id && x.Enabled == true))
            {
                //領取熟客卡
                var pcpApp = ApiPCPManager.CreatePcpService(ppus.UserId, mcg.Id, PcpType.App);
                if (!pcpApp.ApplyCard())
                {
                    result.Code = ApiResultCode.PcpVbsSellerMemberNotFound;
                    result.Message = "目前此店家不適用熟客系統";
                    return result;
                }
            }
            #endregion

            var viewVbsSellerMember = sp.ViewVbsSellerMemberGet(ppus.UserId, mcg.Id);

            //與商家連結交易開始，增加交易ExpiredTime30分
            var addMinutes = ppus.ExpiredTime.AddMinutes(30);
            ppus.ExpiredTime = addMinutes;
            ppus.Action = (int)PcpPollingUserActionType.Start;
            ppus.ModifiedTime = DateTime.Now;
            ppus.UserMembershipCardId = viewVbsSellerMember.UserMembershipCardId;
            pcp.PcpPollingUserStatusSet(ppus);

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = new MembershipCardCheckData
                {
                    MemberInfo = new MemberInfo
                    {
                        Name = viewVbsSellerMember.SellerMemberLastName + viewVbsSellerMember.SellerMemberFirstName,
                        Gender = viewVbsSellerMember.SellerMemberGender ?? 0,
                        Mobile = viewVbsSellerMember.SellerMemberMobile ?? string.Empty,
                        Birthday = viewVbsSellerMember.SellerMemberBirthday ?? DateTime.MinValue
                    },
                    Card = new MembershipCardData
                    {
                        UserMemberShipCardId = viewVbsSellerMember.UserMembershipCardId,
                        CardLevel = viewVbsSellerMember.Level,
                        PaymentPercent = viewVbsSellerMember.PaymentPercent,
                        Instruction = viewVbsSellerMember.Instruction,
                        OtherPremiums = viewVbsSellerMember.OtherPremiums,
                        AvailableDateType = viewVbsSellerMember.AvailableDateType,
                        AvailableDateDesc = Helper.GetEnumDescription((AvailableDateType)viewVbsSellerMember.AvailableDateType),
                        OrderCount = viewVbsSellerMember.OrderCount,
                        AmountTotal = viewVbsSellerMember.AmountTotal,
                        LastUseTime = viewVbsSellerMember.LastUseTime ?? DateTime.Now
                    }
                }
            };
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public ApiResult MembersCount([FromBody]MembersCountInputModel model)
        {
            var result = new ApiResult();

            if (model == null || model.IsValid == false)
            {
                result.Code = ApiResultCode.InputError;
                result.Message = "參數錯誤";
                return result;
            }

            var sellerUserId = MemberFacade.GetUniqueId(User.Identity.Name);
            var members = SellerFacade.GetViewVbsSellerMemberCount(sellerUserId, (SellerMemberType)model.Type);
            result.Data = members;
            result.Code = ApiResultCode.Success;
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public ApiResult MemberDetail([FromBody]MemberDetailInputModel model)
        {
            var result = new ApiResult();

            if (model == null || model.IsValid == false)
            {
                result.Code = ApiResultCode.InputError;
                result.Message = "請輸入 user_id 或 seller_member_id";
                return result;
            }

            var sellerUserId = MemberFacade.GetUniqueId(User.Identity.Name);
            var member = SellerFacade.GetViewVbsSellerMember(sellerUserId, model.UserId, model.SellerMemberId);
            if (member.IsLoaded)
            {
                result.Data = new SellerMemberDetailModel(member);
                result.Code = ApiResultCode.Success;
            }
            else
            {
                result.Code = ApiResultCode.DataNotFound;
            }

            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Authorize]
        public ApiResult SetMemberDetail([FromBody] SetMemberDetailInputModel model)
        {
            var result = new ApiResult();

            if (pcp.SellerMemberCheck(model.SellerMemberId ?? 0, model.Mobile))
            {
                result.Code = ApiResultCode.DataNotFound;
                result.Message = "發現重複的手機號碼。";
                return result;
            }

            var sellerUserId = MemberFacade.GetUniqueId(User.Identity.Name);
            var sMember = BonusFacade.SellerMemberGet(model.SellerMemberId);

            if (sMember.IsLoaded)
            {
                if (sMember.SellerUserId != sellerUserId)
                {
                    result.Code = ApiResultCode.DataNotFound;
                    result.Message = "找不到會員資料";
                    return result;
                }
            }
            else
            {
                sMember = pcp.SellerMemberGet(sellerUserId, model.Mobile);
            }

            sMember.SellerUserId = sellerUserId;
            sMember.LastName = model.LastName;
            sMember.FirstName = model.FirstName;
            sMember.Mobile = model.Mobile;
            sMember.Gender = model.Gender;
            sMember.Birthday = model.Birthday;
            sMember.ModifyId = sellerUserId;
            sMember.ModifyTime = DateTime.Now;
            sMember.Remarks = model.Remarks;

            pcp.SellerMemberSet(sMember);
            result.Code = ApiResultCode.Success;
            //整理回傳用的資料
            var member = SellerFacade.GetViewVbsSellerMember(sellerUserId, null, sMember.Id);
            if (member.IsLoaded)
            {
                //後來APP需求，要判斷新增或是修改，先用時間差來判斷
                var checkModify = (sMember.ModifyTime.Value - sMember.CreateTime).TotalSeconds > 3;
                var countMem = SellerFacade.GetViewVbsSellerMemberCount(sellerUserId, SellerMemberType.SellerMember);
                result.Data = new SellerMemberDetailModel(member, checkModify, countMem);
            }
            else
            {
                result.Code = ApiResultCode.DataNotFound;
                result.Message = I18N.Phrase.ApiResultCodeDataNotFound;
            }
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public ApiResult Members([FromBody]MembersInputModel model)
        {
            var result = new ApiResult();

            if (model == null || model.IsValid == false)
            {
                result.Code = ApiResultCode.InputError;
                result.Message = "參數錯誤";
                return result;
            }

            var sellerUserId = MemberFacade.GetUniqueId(User.Identity.Name);

            bool newMember = false;
            MembershipCardLevel? level = null;
            switch (model.Filter)
            {
                case MembersInputModelFilter.MembershipCardLevel1:
                    level = MembershipCardLevel.Level1;
                    break;
                case MembersInputModelFilter.MembershipCardLevel2:
                    level = MembershipCardLevel.Level2;
                    break;
                case MembersInputModelFilter.MembershipCardLevel3:
                    level = MembershipCardLevel.Level3;
                    break;
                case MembersInputModelFilter.MembershipCardLevel4:
                    level = MembershipCardLevel.Level4;
                    break;
                case MembersInputModelFilter.NewMember:
                    newMember = true;
                    break;
            }

            var members = SellerFacade.GetViewVbsSellerMember(sellerUserId, (SellerMemberType)model.Type,
                model.Criteria, model.StartIndex, model.Size, level, newMember);
            var memberList = new List<SellerMemberModel>();
            if (members.Any())
            {
                foreach (var m in members)
                {
                    if (string.IsNullOrEmpty(m.SellerMemberFirstName) && m.UserId != null)
                    {
                        var memberLink = mp.MemberLinkGetList((int)m.UserId);
                        if (memberLink.Any(x => x.ExternalOrg == (int)SingleSignOnSource.Facebook))
                        {
                            m.SellerMemberFirstName = "FB註冊";
                        }
                        else
                        {
                            m.SellerMemberFirstName = "未填寫";
                        }
                    }
                    memberList.Add(new SellerMemberModel(m));
                }
                result.Code = ApiResultCode.Success;
            }
            else
            {
                result.Code = ApiResultCode.DataNotFound;
            }

            //依據條件查詢商家會員或熟客卡會員的人數
            int totalMemberCount = SellerFacade.GetViewVbsSellerMemberCount(sellerUserId, (SellerMemberType)model.Type, model.Criteria, level, newMember);

            result.Data = new { Members = memberList, TotalCount = totalMemberCount };
            return result;
        }

#endregion 商家會員

        #region 策展推薦

        /// <summary>
        /// 取得首頁活動策展2.0
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Deal)]
        public ApiResult GetRecommendCurations(GetMainPageCurationsModel model)
        {
            //目前先隨機取2筆
            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = MarketingFacade.GetRandomDefaultCurationTwo(2)
            };
        }



        #endregion

        #region 訊息推播任務
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Authorize]
        public ApiResult MessageAssignments([FromBody] MessageAssignmentsInputModel model)
        {
            var result = new ApiResult();

            if (model == null || model.IsValid == false)
            {
                result.Code = ApiResultCode.InputError;
                result.Message = "參數錯誤";
                return result;
            }

            var userId = MemberFacade.GetUniqueId(User.Identity.Name);
            var assignments = np.PcpAssignmentGetList(userId, model.BeginTime, model.EndTime);

            var data = new List<MessageAssignmentsModel>(assignments.Select(x => new MessageAssignmentsModel
            {
                AssignmentId = x.Id,
                AssignmentType = x.AssignmentType,
                Status = x.Status,
                Subject = x.Subject,
                CreateTime = x.CreateTime
            }));

            result.Code = ApiResultCode.Success;
            result.Data = data;
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Authorize]
        public ApiResult MessageAssignmentDetail([FromBody]MessageAssignmentDetailInputModel model)
        {
            var result = new ApiResult();

            if (model == null || model.IsValid == false)
            {
                result.Code = ApiResultCode.InputError;
                result.Message = "參數錯誤";
                return result;
            }

            var assignment = np.PcpAssignmentGet(model.AssignmentId);
            if (!assignment.IsLoaded)
            {
                result.Code = ApiResultCode.DataNotFound;
                result.Message = "找不到資料";
                return result;
            }

            var filterData = np.PcpAssignmentFilterGetList(assignment.Id);

            var data = new MessageAssignmentDetailModel
            {
                AssignmentId = assignment.Id,
                AssignmentType = assignment.AssignmentType,
                Status = assignment.Status,
                ExecutionTime = assignment.ExecutionTime,
                MessageType = assignment.MessageType,
                Subject = assignment.Subject,
                Message = assignment.Message,
                SendCount = assignment.SendCount,
                FilterData = filterData.Select(x => new AssignmentFilterModel
                {
                    FilterType = (AssignmentFilterType)x.FilterType,
                    Operator = (Operators)x.OperatorX,
                    ParameterFirst = x.ParameterFirst,
                    ParameterSecond = x.ParameterSecond
                }).ToList()
            };

            result.Code = ApiResultCode.Success;
            result.Data = data;
            return result;
        }

        #endregion 訊息推播任務

        #region 上傳圖檔
        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public ApiResult ImageUpload([FromBody]PCPImageUpload input)
        {
            var result = new ApiResult();

            byte[] imageData = null;
            try
            {
                imageData = System.Convert.FromBase64String(input.ImageData);
            }
            catch (System.ArgumentNullException)
            {
                result.Code = ApiResultCode.InputError;
                result.Message = "Base 64 string is null.";
                return result;
            }
            catch (System.FormatException)
            {
                result.Code = ApiResultCode.InputError;
                result.Message = "Base 64 string length is not 4 or is not an even multiple of 4.";
                return result;
            }

            var success = false;

            using (MemoryStream ms = new MemoryStream(imageData))
            {
                using (Image img = Image.FromStream(ms))
                {
                    success = BackendUtility
                        .UploadPcpImage(input.GroupId, input.UserId, img, input.ImageSequence, input.Type, input.IsInsert);
                }
            }

            if (success)
            {
                result.Code = ApiResultCode.Success;
                var id = BackendUtility.GetPcpImageId(input.GroupId, input.ImageSequence, input.Type);
                result.Data = new { ImageId = id };
            }
            else
            {
                result.Code = ApiResultCode.SaveFail;
            }

            return result;
        }

        /// <summary>
        /// 圖檔刪除
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public ApiResult ImageDelete([FromBody]PCPImageInfo input)
        {
            var result = new ApiResult();

            var mcg = pcp.MembershipCardGroupGet(input.GroupId);

            if (mcg.IsLoaded)
            {
                if (VBSFacade.DeleteSellerImageBySeq(input.GroupId, input.UserId, input.Seq, input.ImgType))
                {
                    result.Code = ApiResultCode.Success;
                }
                else
                {
                    result.Code = ApiResultCode.Error;
                    result.Message = "刪除失敗";
                }
            }
            else
            {
                result.Code = ApiResultCode.InputError;
            }
            return result;
        }

        /// <summary>
        /// 多圖檔刪除
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public ApiResult ImageDeleteV2([FromBody]PCPImageDelete input)
        {
            var result = new ApiResult();

            var mcg = pcp.MembershipCardGroupGet(input.GroupId);

            if (mcg.IsLoaded)
            {
                if (VBSFacade.DeleteSellerImageById(input.GroupId, input.UserId, input.ImageId, input.ImgType))
                {
                    result.Code = ApiResultCode.Success;
                }
                else
                {
                    result.Code = ApiResultCode.Error;
                    result.Message = "刪除失敗";
                }
            }
            else
            {
                result.Code = ApiResultCode.InputError;
            }
            return result;
        }

        /// <summary>
        /// 取得圖片列表
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public ApiResult ImageListGet([FromBody]PCPImageInfo input)
        {
            var result = new ApiResult();
            List<PCPImageInfo> datas = new List<PCPImageInfo>();
            datas = pcp.PcpImageGetByCardGroupId(input.GroupId, input.ImgType).Select(x => new PCPImageInfo()
            {
                GroupId = x.GroupId,
                ImgType = (PcpImageType)x.ImageType,
                ImageId = x.Id,
                Seq = x.Sequence,
                UserId = x.SellerUserId,
                Url = ImageFacade.GetMediaPath(x.ImageUrl, MediaType.PCPImage)
            }).ToList();

            result.Code = ApiResultCode.Success;
            result.Data = datas;
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public ApiResult ImageSeqSet([FromBody]PCPImageSeq input)
        {
            var result = new ApiResult();
            var images = pcp.PcpImageGetByCardGroupId(input.GroupId, input.ImgType);

            if (images.Count != input.ImgSeq.Count)
            {
                result.Code = ApiResultCode.Error;
                result.Message = "ImgSeq排序Id筆數不一致";
                return result;
            }

            try
            {
                int index = 1;
                foreach (var imageId in input.ImgSeq)
                {
                    images.FirstOrDefault(x => x.Id == imageId).Sequence = index;
                    index++;
                }

                pcp.PcpImageSet(images);
                result.Code = ApiResultCode.Success;
            }
            catch (Exception)
            {

                result.Code = ApiResultCode.Error;
                result.Message = "排序失敗";
            }

            return result;
        }

        #endregion

    }
}

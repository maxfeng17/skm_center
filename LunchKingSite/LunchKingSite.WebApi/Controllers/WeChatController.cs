﻿using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;


namespace LunchKingSite.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class WeChatController : BaseController
    {
        //private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        //private ICmsProvider cp = ProviderFactory.Instance().GetDefaultProvider<ICmsProvider>();
        //private IPponProvider pp = ProviderFactory.Instance().GetDefaultProvider<IPponProvider>();
        //private ILocationProvider lp = ProviderFactory.Instance().GetDefaultProvider<ILocationProvider>();
        //private DateTime init_date = new DateTime(1970, 1, 1);

        ///// <summary>
        ///// 驗證提交給wechat的url(此api網址)有效性
        ///// </summary>
        ///// <param name="echostr">隨機字串</param>
        ///// <param name="signature">加密文字(token和timestamp及nonce)</param>
        ///// <param name="timestamp">時間戳記</param>
        ///// <param name="nonce">亂數</param>
        ///// <returns></returns>
        //public HttpResponseMessage Get(string echostr, string signature, string timestamp, string nonce)
        //{
        //    if (!string.IsNullOrEmpty(echostr) && !string.IsNullOrEmpty(signature)
        //                && !string.IsNullOrEmpty(timestamp) && !string.IsNullOrEmpty(nonce))
        //    {
        //        //組成陣列並排序組成字串，以SHA1加密
        //        string[] array_list = { config.WeChatToken, timestamp, nonce };
        //        Array.Sort(array_list);
        //        string array_string = string.Join(string.Empty, array_list);
        //        array_string = FormsAuthentication.HashPasswordForStoringInConfigFile(array_string, "SHA1");
        //        //比較加密文字
        //        if (array_string.Equals(signature, StringComparison.CurrentCultureIgnoreCase))
        //        {
        //            //相同則直接回傳參數eonce
        //            return new HttpResponseMessage() { Content = new StringContent(echostr, Encoding.UTF8, "text/html") };
        //        }
        //        else
        //        {
        //            return Request.CreateResponse(HttpStatusCode.NoContent);
        //        }
        //    }
        //    else
        //    {
        //        return Request.CreateResponse(HttpStatusCode.NoContent);
        //    }
        //}

        ///// <summary>
        ///// 接收WeChat Post
        ///// </summary>
        ///// <param name="request"></param>
        ///// <returns></returns>
        //public HttpResponseMessage Post(HttpRequestMessage request)
        //{
        //    try
        //    {
        //        //WeChat固定回傳xml格式
        //        XDocument doc = XDocument.Load(request.Content.ReadAsStreamAsync().Result);
        //        //接收到WeChat Post資料後，可在5秒內回傳訊息，超過5秒WeChat會自動斷掉連線 (!!注意回傳資料是否超過5秒)
        //        XDocument response_doc = new XDocument();
        //        List<PponDealSynopsis> deals = new List<PponDealSynopsis>();
        //        List<ApiVourcherStore> vourchers = new List<ApiVourcherStore>();
        //        List<ApiVourcherPromoData> vourcher_promo = new List<ApiVourcherPromoData>();
        //        WeChatEventPromoMessageClass wechat_promo = null;
        //        int city_id = PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId;
        //        ApiCoordinates coordinates;
        //        string response_string = "完整功能、更多好康優惠盡在17Life，<a href=\"" + config.SiteUrl +
        //            "/User/coupon_List.aspx\">立刻進入</a>";
        //        IEnumerable<XElement> elements = doc.Descendants();
        //        //訊息類別
        //        WeChatMsgType msg_type = XElementToProperty_WeChatMsgType(elements);
        //        //事件類別
        //        WeChatEventType event_type = XElementToPropertyWeChatEventType(elements);
        //        //事件回傳key值
        //        WeChatEventKey event_key = XElementToPropertyWeChatEventKey(elements);
        //        //接收者(開發者OpenId)
        //        string to_user_name = XElementToProperty(elements, "ToUserName");
        //        //發送者(使用者OpenId)
        //        string from_user_name = XElementToProperty(elements, "FromUserName");
        //        WechatLocation wechat_location = new WechatLocation();
        //        switch (msg_type)
        //        {
        //            case WeChatMsgType.@event:
        //                {
        //                    switch (event_type)
        //                    {
        //                        //點選Menu中的選項
        //                        case WeChatEventType.click:
        //                            {
        //                                switch (event_key)
        //                                {
        //                                    // 在地好康
        //                                    case WeChatEventKey.todaydeals_local:
        //                                        wechat_location = lp.WeChatLocationGet(from_user_name);
        //                                        deals = PponDealApiManager.PponDealSynopsesGetList(city_id = wechat_location.IsLoaded ? wechat_location.AreaCityId : PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId, 1, config.WeChatArticleCount, ApiImageSize.S, true, true);
        //                                        break;
        //                                    //宅配好康
        //                                    case WeChatEventKey.todaydeals_delivery:
        //                                        deals = PponDealApiManager.PponDealSynopsesGetList(city_id = PponCityGroup.DefaultPponCityGroup.AllCountry.CityId, 1, config.WeChatArticleCount, ApiImageSize.S, true, true);
        //                                        break;
        //                                    //旅遊度假
        //                                    case WeChatEventKey.todaydeals_travel:
        //                                        deals = PponDealApiManager.PponDealSynopsesGetList(city_id = PponCityGroup.DefaultPponCityGroup.Travel.CityId, 1, config.WeChatArticleCount, ApiImageSize.S, true, true);
        //                                        break;
        //                                    //女性專區
        //                                    case WeChatEventKey.todaydeals_peauty:
        //                                        deals = PponDealApiManager.PponDealSynopsesGetList(city_id = PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityId, 1, config.WeChatArticleCount, ApiImageSize.S, true, true);
        //                                        break;
        //                                    //全家專區
        //                                    case WeChatEventKey.todaydeals_family:
        //                                        deals = PponDealApiManager.PponDealSynopsesGetList(city_id = PponCityGroup.DefaultPponCityGroup.Family.CityId, 1, config.WeChatArticleCount, ApiImageSize.S, true, true);
        //                                        break;
        //                                    //我的17Life
        //                                    case WeChatEventKey.enjoy_mine:
        //                                        break;
        //                                    //好康活動
        //                                    case WeChatEventKey.enjoy_promo:
        //                                        wechat_promo = GetPromoMessage();
        //                                        break;
        //                                    //優惠券
        //                                    case WeChatEventKey.vourcher:
        //                                        wechat_location = lp.WeChatLocationGet(from_user_name);
        //                                        if (wechat_location.IsLoaded)
        //                                        {
        //                                            KeyValuePair<double, double> lat_longitude = LocationFacade.GetLatLongitudeByGeography(wechat_location.Coordinate);
        //                                            if (ApiCoordinates.TryParse(lat_longitude.Value.ToString(), lat_longitude.Key.ToString(), out coordinates))
        //                                            {
        //                                                vourchers = ApiVourcherManager.ApiVourcherStoreGetList(1, config.WeChatArticleCount, wechat_location.AreaCityId,
        //                                                                                              null, VourcherSortType.Popular, coordinates, null);
        //                                            }
        //                                            else
        //                                            {
        //                                                vourcher_promo = ApiVourcherManager.ApiVourcherPromoDataGetList();
        //                                            }
        //                                        }
        //                                        else
        //                                        {
        //                                            vourcher_promo = ApiVourcherManager.ApiVourcherPromoDataGetList();
        //                                        }
        //                                        break;
        //                                    default:
        //                                        return new HttpResponseMessage(HttpStatusCode.NoContent);
        //                                }
        //                            }
        //                            break;
        //                        //使用者回傳位置座標
        //                        case WeChatEventType.LOCATION:
        //                            wechat_location = GetAreaCityId(from_user_name, XElementToProperty(elements, "Latitude"), XElementToProperty(elements, "Longitude"), false);
        //                            return new HttpResponseMessage(HttpStatusCode.NoContent);
        //                        //訂閱
        //                        case WeChatEventType.subscribe:
        //                            response_string = "哈囉，歡迎關注17Life！" + Environment.NewLine + Environment.NewLine + "天天提供您全台灣各地團購好康" + Environment.NewLine + "超過千張餐廳優惠券免費索取"
        //                                + Environment.NewLine + "陪您一起快樂購物、省錢過生活" + Environment.NewLine + "<a href=\"https://www.17life.com/ppon/default.aspx?rsrc=WeChat_po\">快來17Life逛逛吧</a>";
        //                            break;
        //                        //取消訂閱
        //                        case WeChatEventType.unsubscribe:
        //                        default:
        //                            return new HttpResponseMessage(HttpStatusCode.NoContent);
        //                    }
        //                }
        //                break;
        //            case WeChatMsgType.location:
        //                wechat_location = GetAreaCityId(from_user_name, XElementToProperty(elements, "Location_X"), XElementToProperty(elements, "Location_Y"), true);
        //                response_string = "您目前的位置在" + wechat_location.CityName + "，" + response_string;
        //                break;
        //            default:
        //            //使用者回傳文字訊息
        //            case WeChatMsgType.text:
        //                string content = XElementToProperty(elements, "Content");
        //                if (content.Equals("i like move it move it", StringComparison.CurrentCultureIgnoreCase))
        //                {
        //                    response_string = from_user_name;
        //                }
        //                //else if (content.Equals("17life加菜金", StringComparison.CurrentCultureIgnoreCase))
        //                //{
        //                //    DiscountCode discount_code;
        //                //    InstantGenerateDiscountCampaignResult campaign_result = PromotionFacade.InstantGenerateDiscountCode(259, out discount_code);
        //                //    if (campaign_result == InstantGenerateDiscountCampaignResult.Success)
        //                //    {
        //                //        response_string = string.Format("恭喜得到折價券:{0}{1}{2}", discount_code.Code, Environment.NewLine, response_string);
        //                //    }
        //                //    else if (campaign_result == InstantGenerateDiscountCampaignResult.Exceed)
        //                //    {
        //                //        response_string = "折價券已全數索取完畢~";
        //                //    }
        //                //    else
        //                //    {
        //                //        response_string = "活動已結束~";
        //                //    }
        //                //}
        //                else
        //                {
        //                    response_string = "感謝您支持17Life" + Environment.NewLine + Environment.NewLine + "下載17Life app" + Environment.NewLine + "隨時隨地抓住每一個超殺優惠"
        //                        + Environment.NewLine + "還有全家便利商店、知名連鎖品牌等" + Environment.NewLine + "全台超過千張吃喝玩樂優惠券免費使用"
        //                        + Environment.NewLine + "<a href=\"https://www.17life.com/ppon/default.aspx?rsrc=WeChat_po\">快點此擁有</a>";
        //                }
        //                break;
        //        }

        //        if (deals.Count() > 0)
        //        {
        //            response_doc = ReturnNewsXmlDoc(from_user_name, to_user_name, deals, city_id);
        //        }
        //        else if (vourchers.Count > 0)
        //        {
        //            response_doc = ReturnNewsXmlDoc(from_user_name, to_user_name, vourchers);
        //        }
        //        else if (vourcher_promo.Count > 0)
        //        {
        //            response_doc = ReturnNewsXmlDoc(from_user_name, to_user_name, vourcher_promo);
        //        }
        //        else if (wechat_promo != null)
        //        {
        //            if (wechat_promo.PromoType == EventPromoType.WeChatPromoMessage)
        //            {
        //                response_doc = ReturnTextXmlDoc(from_user_name, to_user_name, wechat_promo.PromoMessage);
        //            }
        //            else
        //            {
        //                response_doc = ReturnNewsXmlDoc(from_user_name, to_user_name, wechat_promo.PromoItems);
        //            }
        //        }
        //        else
        //        {
        //            response_doc = ReturnTextXmlDoc(from_user_name, to_user_name, response_string);
        //        }
        //        return new HttpResponseMessage() { Content = new StringContent(response_doc.ToString()) };
        //    }
        //    catch (Exception error)
        //    {
        //        return request.CreateErrorResponse(HttpStatusCode.ExpectationFailed, error.Message);
        //    }

        //}
        ///// <summary>
        ///// 取得好康活動訊息
        ///// </summary>
        ///// <returns></returns>
        //private WeChatEventPromoMessageClass GetPromoMessage()
        //{
        //    WeChatEventPromoMessageClass wechat_promo = new WeChatEventPromoMessageClass(EventPromoType.WeChatPromoMessage, "更多優惠請上<a href=\"" + config.SiteUrl + "\">17Life</a>");
        //    DateTime sysTime = DateTime.Now;
        //    EventPromoCollection eventPromos = pp.EventPromoGetList(
        //                                   EventPromo.Columns.Status + " = " + true,
        //                                   EventPromo.Columns.StartDate + " <= " + sysTime.Date,
        //                                   EventPromo.Columns.EndDate + " >= " + sysTime.Date, EventPromo.Columns.Type + " in (" + (int)EventPromoType.WeChatPromoMessage + "," + (int)EventPromoType.WeChatPromoArticle + ")");
        //    int total;
        //    if ((total = eventPromos.Count) > 0)
        //    {
        //        //亂數取得訊息
        //        Random ran = new Random();
        //        EventPromo eventpromo = eventPromos[ran.Next(0, total)];
        //        switch (eventpromo.Type)
        //        {
        //            //單純文字訊息內容
        //            case (int)EventPromoType.WeChatPromoMessage:
        //                wechat_promo.PromoMessage = eventpromo.Description;
        //                break;
        //            //圖文訊息
        //            case (int)EventPromoType.WeChatPromoArticle:
        //                {
        //                    wechat_promo.PromoType = EventPromoType.WeChatPromoArticle;
        //                    //最多10則(要扣掉最後一個連結至網站)
        //                    wechat_promo.PromoItems = pp.GetEventPromoItemList(eventpromo.Id).Where(x => x.Status).Take(9);
        //                }
        //                break;
        //        }
        //    }
        //    return wechat_promo;
        //}

        ///// <summary>
        ///// 從XML按名稱抓取key值
        ///// </summary>
        ///// <param name="elements">xml 項目</param>
        ///// <param name="name">名稱</param>
        ///// <returns></returns>
        //private string XElementToProperty(IEnumerable<XElement> elements, string name)
        //{
        //    XElement key_element = elements.FirstOrDefault(x => x.Name.LocalName.Equals(name, StringComparison.CurrentCultureIgnoreCase));
        //    if (key_element != null)
        //    {
        //        return key_element.Value;
        //    }
        //    else
        //    {
        //        return string.Empty;
        //    }
        //}
        ///// <summary>
        ///// 抓取XML中的訊息類別
        ///// </summary>
        ///// <param name="elements">XML項目</param>
        ///// <returns></returns>
        //private WeChatMsgType XElementToProperty_WeChatMsgType(IEnumerable<XElement> elements)
        //{
        //    string value = XElementToProperty(elements, "MsgType");
        //    WeChatMsgType type;
        //    if (Enum.TryParse<WeChatMsgType>(value, out type))
        //    {
        //        return type;
        //    }
        //    else
        //    {
        //        return WeChatMsgType.text;
        //    }
        //}
        ///// <summary>
        ///// 抓取XML中的事件類別
        ///// </summary>
        ///// <param name="elements">XML項目</param>
        ///// <returns></returns>
        //private WeChatEventType XElementToPropertyWeChatEventType(IEnumerable<XElement> elements)
        //{
        //    string value = XElementToProperty(elements, "Event");
        //    WeChatEventType type;
        //    if (Enum.TryParse<WeChatEventType>(value, out type))
        //    {
        //        return type;
        //    }
        //    else
        //    {
        //        return WeChatEventType.click;
        //    }
        //}
        ///// <summary>
        ///// 抓取XML中的事件key值
        ///// </summary>
        ///// <param name="elements">XML項目</param>
        ///// <returns></returns>
        //private WeChatEventKey XElementToPropertyWeChatEventKey(IEnumerable<XElement> elements)
        //{
        //    string value = XElementToProperty(elements, "EventKey");
        //    WeChatEventKey key;
        //    if (Enum.TryParse<WeChatEventKey>(value, out key))
        //    {
        //        return key;
        //    }
        //    else
        //    {
        //        return WeChatEventKey.todaydeals_local;
        //    }
        //}
        ///// <summary>
        ///// WeChat中建檔時間用Int規格，文件中沒說明計算方法，網路上是減去1970年
        ///// </summary>
        ///// <returns></returns>
        //private Int32 GetCreateTimeInt()
        //{
        //    return (Int32)(DateTime.Now - init_date).TotalSeconds;
        //}
        ///// <summary>
        ///// 回傳給消費者文字訊息XML結構
        ///// </summary>
        ///// <param name="to_user_name">接收者</param>
        ///// <param name="from_user_name">發送者</param>
        ///// <param name="content">文字內容</param>
        ///// <returns></returns>
        //private XDocument ReturnTextXmlDoc(string to_user_name, string from_user_name, string content)
        //{
        //    XDocument x = new XDocument();
        //    x.Add(new XElement("xml",
        //        new XElement("ToUserName", new XCData(to_user_name)),
        //        new XElement("FromUserName", new XCData(from_user_name)),
        //        new XElement("CreateTime", GetCreateTimeInt()),
        //        new XElement("MsgType", new XCData(WeChatMsgType.text.ToString())),
        //        new XElement("Content", new XCData(content))));
        //    return x;
        //}
        ///// <summary>
        ///// 回傳多媒體訊息(圖片、聲音和影音)
        ///// </summary>
        ///// <param name="to_user_name">接收者</param>
        ///// <param name="from_user_name">發送者</param>
        ///// <param name="msg_type">訊息類別</param>
        ///// <param name="media_id">多媒體Id</param>
        ///// <param name="thumb_media_id">影音多媒體縮圖的Id</param>
        ///// <returns></returns>
        //private XDocument ReturnMediaXmlDoc(string to_user_name, string from_user_name, WeChatMsgType msg_type, string media_id, string thumb_media_id)
        //{
        //    XDocument x = new XDocument();
        //    XElement media_element;
        //    switch (msg_type)
        //    {
        //        case WeChatMsgType.image:
        //        case WeChatMsgType.voice:
        //            media_element = new XElement(new CultureInfo("en-US").TextInfo.ToTitleCase(msg_type.ToString()), new XElement("MediaId", new XCData(media_id)));
        //            break;
        //        case WeChatMsgType.video:
        //            media_element = new XElement(new CultureInfo("en-US").TextInfo.ToTitleCase(msg_type.ToString()), new XElement("MediaId", new XCData(media_id)), new XElement("ThumbMediaId", new XCData(thumb_media_id)));
        //            break;
        //    }
        //    x.Add(new XElement("xml",
        //        new XElement("ToUserName", new XCData(to_user_name)),
        //        new XElement("FromUserName", new XCData(from_user_name)),
        //        new XElement("CreateTime", GetCreateTimeInt()),
        //        new XElement("MsgType", new XCData(msg_type.ToString())), media_id));
        //    return x;
        //}
        ///// <summary>
        ///// 回傳圖文訊息
        ///// </summary>
        ///// <param name="to_user_name">接收者</param>
        ///// <param name="from_user_name">發送者</param>
        ///// <param name="article_count">文章數量(限制10)</param>
        ///// <param name="deals">好康檔次內容</param>
        ///// <returns></returns>
        //private XDocument ReturnNewsXmlDoc(string to_user_name, string from_user_name, List<PponDealSynopsis> deals, int city_id)
        //{
        //    XDocument x = new XDocument();
        //    XElement item_element = new XElement("Articles");
        //    foreach (var deal in deals)
        //    {
        //        item_element.Add(new XElement("item",
        //            new XElement("Title", new XCData(deal.DealName)),
        //            new XElement("Description", new XCData(deal.DealEventName)),
        //            new XElement("PicUrl", new XCData(deal.ImagePath)),
        //            new XElement("Url", new XCData(string.Format("{0}/{1}/{2}", config.SiteUrl, city_id, deal.Bid)))
        //            //new XElement("Url", new XCData(string.Format("{0}/{1}", conf.SiteUrl, deal.Bid)))
        //            ));
        //    }
        //    item_element.Add(new XElement("item",
        //            new XElement("Title", new XCData("看更多好康 >>")),
        //            new XElement("Description", new XCData("看更多好康 >>")),
        //            new XElement("PicUrl", new XCData(string.Empty)),
        //        new XElement("Url", new XCData(string.Format("{0}/{1}", config.SiteUrl, city_id)))
        //        //    new XElement("Url", new XCData(string.Format("{0}", conf.SiteUrl)))
        //            ));
        //    x.Add(new XElement("xml",
        //        new XElement("ToUserName", new XCData(to_user_name)),
        //        new XElement("FromUserName", new XCData(from_user_name)),
        //        new XElement("CreateTime", GetCreateTimeInt()),
        //        new XElement("MsgType", new XCData(WeChatMsgType.news.ToString())),
        //        new XElement("ArticleCount", deals.Count + 1),
        //        item_element));
        //    return x;
        //}
        //private XDocument ReturnNewsXmlDoc(string to_user_name, string from_user_name, List<ApiVourcherStore> vourchers)
        //{
        //    XDocument x = new XDocument();
        //    XElement item_element = new XElement("Articles");
        //    foreach (var vourcher in vourchers)
        //    {
        //        item_element.Add(new XElement("item",
        //            new XElement("Title", new XCData(vourcher.SellerName)),
        //            new XElement("Description", new XCData(vourcher.VourcherContent)),
        //            new XElement("PicUrl", new XCData(vourcher.SellerLogoimgPath)),
        //            new XElement("Url", new XCData(string.Format("{0}/vourcher/wechat_coupon.aspx?eid={1}", config.SiteUrl, vourcher.EventId)))
        //            ));
        //    }
        //    item_element.Add(new XElement("item",
        //           new XElement("Title", new XCData("千張吃喝玩樂優惠券，完整收藏盡在17Life App  >>")),
        //           new XElement("Description", new XCData("千張吃喝玩樂優惠券，完整收藏盡在17Life App  >>")),
        //           new XElement("PicUrl", new XCData(string.Empty)),
        //        new XElement("Url", new XCData(string.Format("{0}/ppon/promo.aspx?cn=app", config.SiteUrl)))
        //        // new XElement("Url", new XCData(string.Format("{0}", conf.SiteUrl)))
        //           ));
        //    x.Add(new XElement("xml",
        //        new XElement("ToUserName", new XCData(to_user_name)),
        //        new XElement("FromUserName", new XCData(from_user_name)),
        //        new XElement("CreateTime", GetCreateTimeInt()),
        //        new XElement("MsgType", new XCData(WeChatMsgType.news.ToString())),
        //        new XElement("ArticleCount", vourchers.Count + 1),
        //        item_element));
        //    return x;
        //}

        //private XDocument ReturnNewsXmlDoc(string to_user_name, string from_user_name, List<ApiVourcherPromoData> vourcher_promos)
        //{
        //    XDocument x = new XDocument();
        //    XElement item_element = new XElement("Articles");
        //    foreach (var vourcher in vourcher_promos)
        //    {
        //        item_element.Add(new XElement("item",
        //            new XElement("Title", new XCData(vourcher.SellerName)),
        //            new XElement("Description", new XCData(vourcher.VourcherContent)),
        //            new XElement("PicUrl", new XCData(vourcher.LogoPic)),
        //            new XElement("Url", new XCData(string.Format("{0}/vourcher/wechat_coupon.aspx?eid={1}", config.SiteUrl, vourcher.EventId)))
        //            ));
        //    }
        //    item_element.Add(new XElement("item",
        //           new XElement("Title", new XCData("千張吃喝玩樂優惠券，完整收藏盡在17Life App  >>")),
        //           new XElement("Description", new XCData("千張吃喝玩樂優惠券，完整收藏盡在17Life App  >>")),
        //           new XElement("PicUrl", new XCData(string.Empty)),
        //        new XElement("Url", new XCData(string.Format("{0}/ppon/promo.aspx?cn=app", config.SiteUrl)))
        //        // new XElement("Url", new XCData(string.Format("{0}", conf.SiteUrl)))
        //           ));
        //    x.Add(new XElement("xml",
        //        new XElement("ToUserName", new XCData(to_user_name)),
        //        new XElement("FromUserName", new XCData(from_user_name)),
        //        new XElement("CreateTime", GetCreateTimeInt()),
        //        new XElement("MsgType", new XCData(WeChatMsgType.news.ToString())),
        //        new XElement("ArticleCount", vourcher_promos.Count + 1),
        //        item_element));
        //    return x;
        //}

        //private XDocument ReturnNewsXmlDoc(string to_user_name, string from_user_name, IEnumerable<EventPromoItem> eventpromoitems)
        //{
        //    XDocument x = new XDocument();
        //    XElement item_element = new XElement("Articles");
        //    foreach (var eventpromoitem in eventpromoitems)
        //    {
        //        item_element.Add(new XElement("item",
        //            new XElement("Title", new XCData(eventpromoitem.Title)),
        //            new XElement("Description", new XCData(eventpromoitem.Description)),
        //            new XElement("PicUrl", new XCData(eventpromoitem.ItemPicUrl)),
        //            new XElement("Url", new XCData(eventpromoitem.ItemUrl))
        //            ));
        //    }
        //    item_element.Add(new XElement("item",
        //           new XElement("Title", new XCData("千張吃喝玩樂優惠券，完整收藏盡在17Life App  >>")),
        //           new XElement("Description", new XCData("千張吃喝玩樂優惠券，完整收藏盡在17Life App  >>")),
        //           new XElement("PicUrl", new XCData(string.Empty)),
        //        new XElement("Url", new XCData(config.SiteUrl))
        //        //   new XElement("Url", new XCData(string.Format("{0}", conf.SiteUrl)))
        //           ));
        //    x.Add(new XElement("xml",
        //        new XElement("ToUserName", new XCData(to_user_name)),
        //        new XElement("FromUserName", new XCData(from_user_name)),
        //        new XElement("CreateTime", GetCreateTimeInt()),
        //        new XElement("MsgType", new XCData(WeChatMsgType.news.ToString())),
        //        new XElement("ArticleCount", eventpromoitems.Count() + 1),
        //        item_element));
        //    return x;
        //}
        ///// <summary>
        ///// 依使用者wechat openid判斷是否要更新DB地理位置
        ///// </summary>
        ///// <param name="open_id">wechat openid</param>
        ///// <param name="latitude_string">回傳的座標</param>
        ///// <param name="longitude_string">回傳的座標</param>
        //private WechatLocation GetAreaCityId(string open_id, string latitude_string, string longitude_string, bool update_immediate)
        //{
        //    City actual_city;
        //    WechatLocation wechat_location = lp.WeChatLocationGet(open_id);

        //    //5分鐘內不更新
        //    if (!wechat_location.IsLoaded || (wechat_location.IsLoaded && (update_immediate ? true : (DateTime.Now - wechat_location.ModifyTime).TotalMinutes > 5)))
        //    {
        //        double longitude, latitude;
        //        if (double.TryParse(longitude_string, out longitude) && double.TryParse(latitude_string, out latitude))
        //        {
        //            string coordinate = LocationFacade.GetGeographyWKT(latitude.ToString(), longitude.ToString());
        //            PponCity area_city = LocationFacade.GetAreaCityWithBingMap(latitude, longitude, out actual_city);
        //            wechat_location.OpenId = open_id;
        //            wechat_location.ModifyTime = DateTime.Now;
        //            wechat_location.CityId = actual_city.Id;
        //            wechat_location.CityName = actual_city.CityName;
        //            wechat_location.AreaCityId = area_city.CityId;
        //            wechat_location.AreaCityName = area_city.CityName;
        //            wechat_location.Coordinate = coordinate;
        //            lp.WeChatLocationSet(wechat_location);
        //        }
        //    }
        //    return wechat_location;
        //}
    }
    /// <summary>
    /// 
    /// </summary>
    public class WeChatEventPromoMessageClass
    {
        /// <summary>
        /// 
        /// </summary>
        public EventPromoType PromoType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<EventPromoItem> PromoItems { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string PromoMessage { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="message"></param>
        public WeChatEventPromoMessageClass(EventPromoType type, string message)
        {
            PromoType = type;
            PromoMessage = message;
        }
        /// <summary>
        /// 
        /// </summary>
        public WeChatEventPromoMessageClass() { }
    }
}

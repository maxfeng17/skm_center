﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using log4net;
using LunchKingSite.Core.Component;
using LunchKingSite.WebApi.Core;
using Helper = LunchKingSite.Core.Helper;

namespace LunchKingSite.WebApi.Controllers
{
    /// <summary>
    /// 2018雙11 遊戲
    /// </summary>
    public class GameController : BaseController
    {
        ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        private ILog logger = LogManager.GetLogger(typeof(GameController));

        [HttpGet]
        public HttpResponseMessage LineAuth(string code, string state, string error= "")
        {
            if (!string.IsNullOrEmpty(error) || string.IsNullOrEmpty(code))
            {
                throw new HttpErrorResponseException(ApiResultCode.Error, "未授權");
            }
            try
            {
                string accessToken = LineUtility.GetGameOAuhToken(code);
                string responseUrl = "/Game/GuestEvent?atk=" + accessToken;
                responseUrl += "&roundGuid=" + Uri.UnescapeDataString(state);
                responseUrl += "&source=" + (int)SingleSignOnSource.Line;

                var response = Request.CreateResponse(HttpStatusCode.Redirect);
                response.Headers.Location = new Uri(Helper.CombineUrl(config.SiteUrl, responseUrl));
                return response;
            }
            catch (Exception ex)
            {
                logger.Info("LineAuth fail,", ex);
                throw new HttpErrorResponseException(ApiResultCode.Error, "執行階段發生錯誤");
            }
        }

        [HttpGet]
        public HttpResponseMessage FBAuth(string code, string state)
        {
            try
            {
                if (string.IsNullOrEmpty(code))
                {
                    throw new HttpErrorResponseException(ApiResultCode.Error, "未授權");
                }
                string url = string.Format("https://graph.facebook.com/oauth/access_token?client_id={0}&redirect_uri={1}&client_secret={2}&code={3}",
                    config.GameFacebookApplicationId,
                    Helper.CombineUrl(config.SiteUrl, config.GameFbAuthRedirectUri),
                    config.GameFacebookApplicationSecret,
                    code);
                string accessToken = FacebookUtility.GetOAuhToken(url);
                string responseUrl = "/Game/GuestEvent?atk=" + accessToken;
                responseUrl += "&roundGuid=" + Uri.UnescapeDataString(state);
                responseUrl += "&source=" + (int)SingleSignOnSource.Facebook;

                var response = Request.CreateResponse(HttpStatusCode.Redirect);
                response.Headers.Location = new Uri(Helper.CombineUrl(config.SiteUrl, responseUrl));
                return response;
            }
            catch (Exception ex)
            {
                logger.Info("FBAuth fail,", ex);
                throw new HttpErrorResponseException(ApiResultCode.Error, "執行階段發生錯誤");
            }
        }
    }
}

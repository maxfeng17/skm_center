﻿using System;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.WebApi.Controllers.Base;
using LunchKingSite.WebApi.Core.Attributes;
using LunchKingSite.WebApi.Core.OAuth;
using LunchKingSite.WebApi.Models;

namespace LunchKingSite.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [RequireHttps]
    [ForceCamelCase]
    public class MasterPassController : ApiControllerBase
    {
        private static IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();

        #region BindingCancel

        /// <summary>
        /// 取消綁定
        /// </summary>
        /// <param></param>
        [HttpPost]
        [OAuthScope(TokenScope.Deal)]
        public ApiResult BindingCancel()
        {
            string userName = HttpContext.Current.User.Identity.Name;
            int memberUniqueId = MemberFacade.GetUniqueId(userName);

            //取消綁定結果
            if (!op.MasterPassPreCheckoutTokenUpdateUsed(memberUniqueId))
            {
                return new ApiResult {Code = ApiResultCode.SaveFail};
            }

            return new ApiResult {Code = ApiResultCode.Success};
          
        }

        #endregion BindingCancel

        /// <summary>
        /// MasterPass Pre checkout card data
        /// </summary>
        /// <param></param>
        /// <param name="model"></param>
        [HttpPost]
        [OAuthScope(TokenScope.Deal)]
        public HttpResponseMessage MasterpassPrecheckout(MasterpassPrecheckout model)
        {
            var rtnObject = new ApiReturnObject();
            //驗證使用者
            var apiUser = ApiUserManager.ApiUserGetByUserId(model.UserId);
            if (apiUser == null)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeApiUserIdError;
                return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
            }

            var userName = HttpContext.Current.User.Identity.Name;
            if (string.IsNullOrWhiteSpace(userName))
            {
                rtnObject.Code = ApiReturnCode.UserNoSignIn;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeUserNoSignIn;
                return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
                
            }

            var mem = MemberFacade.GetMember(userName);
            if (!mem.IsLoaded)
            {
                rtnObject.Code = ApiReturnCode.UserNoSignIn;
                rtnObject.Message = I18N.Phrase.ApiReturnCodeUserNoSignIn;
                return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
            }
            
            var preCheckout = op.MasterPassPreCheckoutTokenGet(mem.UniqueId);
            if (preCheckout.IsLoaded && preCheckout.IsUsed == false)
            {
                try
                {
                    var masterPass = new MasterPass();
                    var checkResult = masterPass.PreCheckout(preCheckout, mem.UniqueId);
                    if (checkResult.IsCheckoutPass)
                    {
                        MasterPassPreCheckoutData masterPassPreCheckout = checkResult;
                        rtnObject.Code = ApiReturnCode.Success;
                        rtnObject.Message = I18N.Phrase.ApiResultCodeSuccess;
                        rtnObject.Data = masterPassPreCheckout;
                        return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
                    }
                    rtnObject.Code = ApiReturnCode.Error;
                    rtnObject.Message = "Masterpass驗證失敗";
                    return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
                }
                catch (Exception)
                {
                    rtnObject.Code = ApiReturnCode.Error;
                    rtnObject.Message = "Masterpass無回應";
                    return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
                }
            }
            rtnObject.Code = ApiReturnCode.DataNotExist;
            rtnObject.Message = "找不到未使用的PreCheckoutToken。";
            return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
        }
    }
}

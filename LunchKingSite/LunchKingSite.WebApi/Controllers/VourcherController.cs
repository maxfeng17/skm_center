﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Http;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using System.Net.Http;
using LunchKingSite.Core.Component;
using Newtonsoft.Json;
using LunchKingSite.BizLogic.Model.Vourcher;
using Newtonsoft.Json.Linq;
using System.Net;
using LunchKingSite.WebApi.Core.OAuth;

namespace LunchKingSite.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class VourcherController : BaseController
    {
        private IEventProvider ep = ProviderFactory.Instance().GetProvider<IEventProvider>();

        /// <summary>
        /// 回傳優惠券的分類項目
        /// </summary>
        /// <param name="cityId"></param>
        /// <returns></returns>
        [HttpGet]
        [OAuthScope(TokenScope.Vourcher)]
        public ApiVersionResult VourcherQueryData(int cityId)
        {
            ApiVersionResult rtnObject = new ApiVersionResult();

            var tmpCategories = ApiVourcherManager.ApiSellerSampleCategoryGetListByCityId(cityId);
            List<ApiSellerSampleCategory> categories = new List<ApiSellerSampleCategory>(tmpCategories);
            //補上全部的項目
            ApiSellerSampleCategory allCategory = new ApiSellerSampleCategory();
            allCategory.Name = "全部類別";
            allCategory.Value = -1;
            allCategory.ShortName = "全部";
            allCategory.Count = categories.Sum(x => x.Count);
            categories.Insert(0, allCategory);

            var regions = ApiVourcherManager.ApiVourcherRegionGetList();
            var sortTypes = ApiVourcherManager.ApiVourcherSortTypeGetList();

            rtnObject.Data = new { Categories = categories, Regions = regions, SortTypes = sortTypes };
            //只有vourcherRegion的值會動態調整，以此值為版本控制的條件
            rtnObject.Version = ApiVourcherManager.ApiVourcherRegionVersion;

            return rtnObject;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        [OAuthScope(TokenScope.Vourcher)]
        public HttpResponseMessage GetYahooVoucherSeller(string accessToken)
        {
            return new HttpResponseMessage()
            {
                Content = new StringContent(JsonConvert.SerializeObject(ep.ViewYahooVoucherSellerGetAvailableList().Select(x => new ViewYahooVoucherSellerModel(x))), Encoding.UTF8, "application/json")
            };
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public HttpResponseMessage GetYahooVoucherSellerById(int id)
        {
            return new HttpResponseMessage()
            {
                Content = new StringContent(JsonConvert.SerializeObject(ep.ViewYahooVoucherSellerGetById(id).Select(x => new ViewYahooVoucherSellerModel(x))), Encoding.UTF8, "application/json")
            };
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        [OAuthScope(TokenScope.Vourcher)]
        public HttpResponseMessage GetYahooVoucherCategory(string accessToken)
        {
            return new HttpResponseMessage()
            {
                Content = new StringContent(JsonConvert.SerializeObject(ep.ViewYahooVoucherCategoryGetAvailableList().Select(x => new ViewYahooVoucherCategoryModel(x))), Encoding.UTF8, "application/json")
            };
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        [OAuthScope(TokenScope.Vourcher)]
        public HttpResponseMessage GetYahooVoucherStore(string accessToken)
        {
            return new HttpResponseMessage()
            {
                Content = new StringContent(JsonConvert.SerializeObject(ep.ViewYahooVoucherStoreGetAvailableList().Select(x => new ViewYahooVoucherStoreModel(x))), Encoding.UTF8, "application/json")
            };
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="eventid"></param>
        /// <returns></returns>
        public HttpResponseMessage GetYahooVoucherStoreByEventId(int eventid)
        {
            return new HttpResponseMessage()
            {
                Content = new StringContent(JsonConvert.SerializeObject(ep.ViewYahooVoucherStoreGetByEventId(eventid).Select(x => new ViewYahooVoucherStoreModel(x))), Encoding.UTF8, "application/json")
            };
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public HttpResponseMessage GetYahooVoucherPromo()
        {
            return new HttpResponseMessage()
           {
               Content = new StringContent(JsonConvert.SerializeObject(ep.ViewYahooVoucherPromoGetAvailableList().Select(x => new ViewYahooVoucherPromoModel(x))), Encoding.UTF8, "application/json")
           };
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public HttpResponseMessage GetYahooVoucherPageCountSum()
        { 
             return new HttpResponseMessage()
           {
               Content = new StringContent(JsonConvert.SerializeObject(ep.VourcherEventGetPageCountSum()), Encoding.UTF8, "application/json")
           };
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ob"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage SetVoucherPageCount(JObject ob)
        {
            KeyValuePair<int, int> event_pagecount = ob.ToObject<KeyValuePair<int, int>>();
            ep.VourcherEventAddPageCount(event_pagecount.Key, event_pagecount.Value);
            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ob"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage SetVoucherPromoPageCount(JObject ob)
        {
            KeyValuePair<int, int> event_pagecount = ob.ToObject<KeyValuePair<int, int>>();
            ep.VoucherPromoAddPageCount(event_pagecount.Key, event_pagecount.Value);
            return new HttpResponseMessage(HttpStatusCode.NoContent);
        }
    }
}

﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using log4net;
using LunchKingSite.Core.Component;
using LunchKingSite.WebApi.Core;
using Helper = LunchKingSite.Core.Helper;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.WebLib.Component;
using System.Web;
using System.Collections.Generic;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Component.MemberActions;
using LunchKingSite.I18N;
using System.Web.Security;

namespace LunchKingSite.WebApi.Controllers
{
    /// <summary>
    /// 新會員登入綁定 
    /// </summary>
    public class NewMemberController : BaseController
    {
        ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        private ILog logger = LogManager.GetLogger(typeof(GameController));
        IMemberProvider mp = ProviderFactory.Instance().GetDefaultProvider<IMemberProvider>();
        

        [HttpGet]
        public HttpResponseMessage LineAuth(string code, string state, string error= "")
        {
            if (!string.IsNullOrEmpty(error) || string.IsNullOrEmpty(code))
            {
                throw new HttpErrorResponseException(ApiResultCode.Error, "未授權");
            }
            try
            {
                string accessToken = LineUtility.GetLoginOAuhToken(code);
                //轉到此頁取得sso
                string responseUrl = "/user/sso.aspx?atk=" + accessToken;
                responseUrl += "&roundGuid=" + Uri.UnescapeDataString(state);
                responseUrl += "&source=" + (int)SingleSignOnSource.Line;

                var response = Request.CreateResponse(HttpStatusCode.Redirect);
                response.Headers.Location = new Uri(Helper.CombineUrl(config.SiteUrl, responseUrl));
                return response;
            }
            catch (Exception ex)
            {
                logger.Info("LineAuth fail,", ex);
                throw new HttpErrorResponseException(ApiResultCode.Error, "執行階段發生錯誤");
            }
        }
        [HttpPost]
        public ApiResult RegisterNewMember(NewEmailMemberModel model)
        {
            string appId = this.AppId;
            string message = string.Empty;
            if (model == null)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError,
                    Message = message
                };
            }

            List<string> errors = model.CheckValid();

            if (errors.Count > 0)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError,
                    Data = new
                    {
                        errors
                    }
                };
            }
            //修改未開通會員的mail
            if (!string.IsNullOrWhiteSpace(model.key))
            {
                return ChangeEmail(model);
            }
                                   
            var replyData = MemberUtility.RegisterContactMember(
                model.email, model.password, model.city, model.edm);

            Member m = mp.MemberGet(model.email);
            MemberAuthInfo mai = mp.MemberAuthInfoGet(m.UniqueId);
            string authKey = mai.AuthKey;

            if (replyData.Reply == MemberRegisterReplyType.RegisterSuccess || 
                replyData.Reply == MemberRegisterReplyType.RegisterContinue )
            {

                return new ApiResult
                {
                    Code = ApiResultCode.Success,
                    Data = new
                    {
                        authKey = authKey
                    }
                };
            }

            if (replyData.Reply == MemberRegisterReplyType.MailRegisterInactive)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.MailRegisterInactive,
                    Data = new
                    {
                        authKey = authKey
                    },
                    Message = replyData.ReplyMessage
                };
            }
            return new ApiResult
            {
                Code = ApiResultCode.InputError,
                Message = replyData.ReplyMessage
            };
        }

        private ApiResult ChangeEmail(NewEmailMemberModel model)
        {
            string message = string.Empty;
            var mai = mp.MemberAuthInfoGetByKey(model.key);
            if (mai.IsLoaded == false)
            {
                //資料不存在
                return new ApiResult
                {
                    Code = ApiResultCode.InputError,
                    Message = message
                };
            }

            Member mem = MemberFacade.GetMember(mai.UniqueId);
            //檢查是否已開通
            var checkOriginallyMail = MemberUtilityCore.CheckMailAvailable(mem.UserEmail, false);
            if (checkOriginallyMail != EmailAvailableStatus.Authorizing)
            {
                //非未開通
                return new ApiResult
                {
                    Code = ApiResultCode.InputError,
                    Message = message
                };
            }

            //check register data correct
            var checkMail = MemberUtilityCore.CheckMailAvailable(model.email, true);
            if (checkMail == EmailAvailableStatus.Available)
            {
                mem.CityId = model.city;
                mem.Password = MembershipProviderCore.EncodePassword(model.password, Membership.Provider.PasswordFormat, mem.PasswordSalt);
                MemberUtility.UpdateMemberInfo(mem, model.email);
                MemberFacade.ChangeMemberAuthInfoMail(mem.UniqueId, model.email);
                MemberFacade.ResetMemberAuthInfo(mai);

                if (model.edm)
                {
                    MemberFacade.SubscribeForNewMember(model.email, model.city);
                }

                return new ApiResult
                {
                    Code = ApiResultCode.Success,
                    Data = new
                    {
                        authKey = model.key
                    }
                };
            }
            else
            {
                if (checkMail == EmailAvailableStatus.IllegalFormat)
                {
                    return new ApiResult
                    {
                        Code = ApiResultCode.InputError,
                        Message = Phrase.RegisterReturnMessageEmailError,
                    };
                }
                if (checkMail == EmailAvailableStatus.DisposableEmailAddress)
                {
                    return new ApiResult
                    {
                        Code = ApiResultCode.InputError,
                        Message = Phrase.RegisterReturnMessageDisposableEmailAddress,
                    };
                }
                if (checkMail == EmailAvailableStatus.WasUsed)
                {
                    return new ApiResult
                    {
                        Code = ApiResultCode.InputError,
                        Message = Phrase.RegisterReturnMessageEmailIsUser,
                    };
                }
                if (checkMail == EmailAvailableStatus.Authorizing)
                {
                    return new ApiResult
                    {
                        Code = ApiResultCode.InputError,
                        Message = Phrase.RegisterReturnMessageRegisterNoOpen,
                    };
                }

                return new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = Phrase.Error,
                };
            }
        }

        [HttpPost]
        public ApiResult GetCityDataSource( )
        {
            List<CityDataItem> cityItems = CityManager.Citys.Where(x => x.Code != "com" && x.Code != "SYS")
                .Select(t => new CityDataItem { id = t.Id, cityName = t.CityName }).ToList();

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = cityItems
            };
        }

        [HttpPost]
        public ApiResult GetExternalLoginDataSource()
        {
            List<ExternalLoginDataItem> data = new List<ExternalLoginDataItem>();
            data.Add(new ExternalLoginDataItem
            {
                name = "17Life",
                imgurl = "/themes/PCweb/images/icons/entry_17.png",
                url = LunchKingSite.WebLib.WebUtility.GetLoginPath()
            });
            data.Add(new ExternalLoginDataItem
            {
                name = "Facebook",
                imgurl = "/themes/PCweb/images/icons/entry_fb.png",
                url = LunchKingSite.WebLib.WebUtility.GetFacebookLoginUrl()
            });
            data.Add(new ExternalLoginDataItem
            {
                name = "Line",
                imgurl = "/themes/PCweb/images/icons/entry_line.png",
                url = LunchKingSite.WebLib.WebUtility.GetLineLoginUrl()
            });
            data.Add(new ExternalLoginDataItem
            {
                name = "PayEasy",
                imgurl = "/themes/PCweb/images/icons/entry_payez.png",
                url = LunchKingSite.WebLib.WebUtility.GetPayEasySsoPath()
            });

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = data
            };
        }

        [HttpPost]
        public ApiResult GetEmailByAuthKey(GetEmailByAuthKeyInputModel model)
        {
            if (model == null || string.IsNullOrEmpty(model.key))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError,
                    Data = "參數不正確"
                };
            }
            IMemberProvider mp = ProviderFactory.Instance().GetDefaultProvider<IMemberProvider>();
            var mai = mp.MemberAuthInfoGetByKey(model.key);
            if (mai.IsLoaded == false)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.DataNotFound,
                    Data = "資料不存在"
                };
            }
            Member mem = MemberFacade.GetMember(mai.UniqueId);
            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = mem.UserEmail
            };
        }

        #region models 


        public class CityDataItem
        {
            public int id { get; set; }
            public string cityName { get; set; }
        }

        public class ExternalLoginDataItem
        {
            public string name { get; set; }
            public string imgurl { get; set; }
            public string url { get; set; }
        }

        public class NewEmailMemberModel
        {
            public string email { get; set; }
            public string password { get; set; }
            public string repassword { get; set; }
            public int city { get; set; }
            public bool edm { get; set; }
            public string verificationCode { get; set; }
            public string captchaKey { get; set; }
            public string key { get; set; }

            public List<string> CheckValid()
            {
                List<string> result = new List<string>();

                if (RegExRules.CheckPassword(password) == false)
                {
                    result.Add("wrongPassword");
                }

                if (password == null || password.Equals(repassword) == false)
                {
                    result.Add("passwordNotMatch");
                }

                if (city == 0)
                {
                    result.Add("noCity");
                }

                if (string.IsNullOrEmpty(this.captchaKey) || string.IsNullOrEmpty(this.verificationCode) ||
                    NewMemberUtility.ValidateCaptcha(captchaKey, verificationCode)==false)
                {
                    result.Add("wrongCaptcha");
                }

                if (string.IsNullOrWhiteSpace(email))
                {
                    result.Add("wrongEmail");
                }

                if (RegExRules.CheckEmail(email) == false)
                {
                    result.Add("wrongEmail");
                }

                return result;
            }
        }

        public class GetEmailByAuthKeyInputModel
        {
            public string key { get; set; }
        }

        #endregion

    }
}

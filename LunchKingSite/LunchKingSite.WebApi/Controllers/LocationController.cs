﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Http;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.WebApi.Core;
using LunchKingSite.WebApi.Models;
using System;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;
using LunchKingSite.BizLogic.Model;
using log4net;

namespace LunchKingSite.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class LocationController : BaseController//ApiController
    {
        protected static ISysConfProvider config = LunchKingSite.Core.Component.ProviderFactory.Instance().GetConfig();
        protected static ILog logger = LogManager.GetLogger(typeof(LocationController));
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        //[AllowCrossSiteJson]
        public dynamic GetCityTownships()
        {
            List<CityTownship> result = new List<CityTownship>();
            List<City> cities = CityManager.Citys.Where(x => x.Code != "SYS").ToList();
            CityCollection towns = CityManager.TownShips;
            Dictionary<int, CityTownship> cityMap = new Dictionary<int, CityTownship>();
            foreach (City city in cities)
            {
                CityTownship item = new CityTownship
                {
                    Id = city.Id,
                    Name = city.CityName,
                    Townships = new List<CityTownship>(),
                    Visible = city.Visible
                };
                result.Add(item);
                cityMap.Add(item.Id, item);
            }
            foreach (City town in towns)
            {
                if (town.ParentId != null && cityMap.ContainsKey(town.ParentId.Value))
                {
                    CityTownship item = new CityTownship
                    {
                        Id = town.Id,
                        Name = town.CityName,
                        Visible = town.Visible
                    };
                    cityMap[town.ParentId.Value].Townships.Add(item);
                }
            }
            return result;
        }
    
        /// <summary>
        /// 取得城市及其下的鄉鎮市區資料，供地址輸入與維護相關功能使用
        /// </summary>
        /// <returns></returns>
        [HttpGet,HttpPost]
        public ApiVersionResult CityListWithTownship()
        {
            ApiVersionResult result = new ApiVersionResult();
            result.Data = LocationApiManager.GetApiCityList();
            result.Version = LocationApiManager.CityItemVersion;

            return result;
        }

        /// <summary>
        /// 檢查地址合法性
        /// </summary>
        /// <param name="address"></param>
        /// <param name="townId"></param>
        /// <returns></returns>
        [HttpGet]
        public string ValidateAddress(string address,int townId)
        {
            if (config.EnabledValidateBuyerAddress)
            {
                try
                {
                    //轉為半形
                    address = Helper.ConvertToHalf(address);
                    //地址含有罕見字就跳過檢查
                    string[] exStr = new string[] { "坔", "仔", "邨" };

                    #region 檢查簡體字
                    Regex wordRegex = new Regex(@"[a-zA-Z\-]+");

                    if (address.Any(x => !Helper.IsBig5Code(x) && !Helper.IsNumeric(x) && !exStr.Any(x.ToString().Contains) && !wordRegex.Match(x.ToString()).Success))
                    {
                        return "地址疑似含有簡體字或特殊字元，但不會影響您繼續購買，請再三確保地址的正確性。";
                    }

                    #endregion

                    string[] patternGroup = new string[] { @"(?<city>\D+?[縣市])", @"(?<district>\D+?(市區|鎮區|鎮市|[鄉鎮市區]))", @"(?<village1>\D+?[里])", @"(?<village2>\D+?[村])", @"(?<neighbor>\d+?[鄰])", @"(?<street>.*街)", @"(?<road>\D+?路)", @"(?<section>.*段)", @"(?<lane>.*巷)", @"(?<alley>\d+?弄)", @"(?<no>\d+?[-之~附]\d+號)", @"(?<no>\d+?號)", @"(?<building>[A-Za-z0-9]+棟)", @"(?<floor>[A-Za-z0-9]+樓)", @"(?<others>.+)?", "" };
                    //string p = @"(?<city>\D+?[縣市])(?<district>\D+?(市區|鎮區|鎮市|[鄉鎮市區]))?(?<village1>\D+?[里])?(?<village2>\D+?[村])?(?<neighbor>\d+?[鄰])?(?<section>.*段)?(?<street>.*街)?(?<road>\D+?路)?(?<lane>.*巷)?(?<alley>\d+?弄)?(?<no>\d+?[-之~附]\d+號)?(?<no>\d+?號)?";

                    string p = string.Join("?", patternGroup);

                    Regex regex = new Regex(p);
                    TaiwanAddress obj = new TaiwanAddress("");

                    //排除括號內的文字
                    if (address.Contains('(') && address.Contains(')'))
                    {
                        address = address.Split('(')[0] + address.Split(')')[1];
                    }
                    string oriAddress = address;

                    foreach (Match m in regex.Matches(address))
                    {
                        obj.City = !string.IsNullOrEmpty(m.Groups["city"].Value) && string.IsNullOrEmpty(obj.City) ? m.Groups["city"].Value : obj.City;
                        obj.Town = !string.IsNullOrEmpty(m.Groups["district"].Value) && string.IsNullOrEmpty(obj.Town) ? m.Groups["district"].Value : obj.Town;
                        obj.Village = !string.IsNullOrEmpty(m.Groups["village1"].Value + m.Groups["village2"].Value) && string.IsNullOrEmpty(obj.Village) ? m.Groups["village1"].Value + m.Groups["village2"].Value : obj.Village;
                        obj.Lin = !string.IsNullOrEmpty(m.Groups["neighbor"].Value) && string.IsNullOrEmpty(obj.Lin) ? m.Groups["neighbor"].Value : obj.Lin;
                        obj.Sec = !string.IsNullOrEmpty(m.Groups["section"].Value) && string.IsNullOrEmpty(obj.Sec) ? m.Groups["section"].Value : obj.Sec;
                        //obj.Street = !string.IsNullOrEmpty(m.Groups["street"].Value) && string.IsNullOrEmpty(obj.Street) ? m.Groups["street"].Value : obj.Street;
                        obj.Road = !string.IsNullOrEmpty(m.Groups["street"].Value + m.Groups["road"].Value) && string.IsNullOrEmpty(obj.Road) ? m.Groups["street"].Value + m.Groups["road"].Value : obj.Road;
                        obj.Lane = !string.IsNullOrEmpty(m.Groups["lane"].Value) && string.IsNullOrEmpty(obj.Lane) ? m.Groups["lane"].Value : obj.Lane;
                        obj.Alley = !string.IsNullOrEmpty(m.Groups["alley"].Value) && string.IsNullOrEmpty(obj.Alley) ? m.Groups["alley"].Value : obj.Alley;
                        obj.No = !string.IsNullOrEmpty(m.Groups["no"].Value) && string.IsNullOrEmpty(obj.No) ? m.Groups["no"].Value.Replace("之", "-") : obj.No;

                        address = !string.IsNullOrEmpty(m.Value) ? address.Replace(m.Value, string.Empty) : address;
                    }

                    string roadName = !string.IsNullOrEmpty(obj.Road) ? obj.Road + obj.Sec : string.Empty;

                    //如果沒有找到路名 嘗試用剩下的去找
                    if (string.IsNullOrEmpty(roadName))
                    {
                        string[] others;
                        address = Helper.mapCnumLettersString(address);
                        foreach (var words in patternGroup)
                        {
                            others = Regex.Split(address, words);
                            if (others.Count() > 1 && others.Any(x => !string.IsNullOrEmpty(x)))
                            {
                                roadName = others.FirstOrDefault(x => !string.IsNullOrEmpty(x)).Trim();
                                break;
                            }
                        }

                    }

                    roadName = Helper.mapCnumLettersString(roadName);
                    string building = string.Empty;

                    //從DB檢查路名是否存在
                    if (!string.IsNullOrEmpty(roadName))
                    {
                        foreach (var item in CityManager.BuildingListGetByTownshipId(townId))
                        {
                            building = Helper.mapCnumLettersString(Helper.ConvertToHalf(item.BuildingName));
                            if (building == roadName)
                            {
                                return string.Empty;
                            }
                            else if (building.Contains(roadName) || roadName.Contains(building))
                            {
                                return string.Empty;
                            }
                        }
                    }

                    //用gov API檢查路名是否存在
                    try
                    {
                        Uri uri = new Uri(string.Format("http://210.69.35.216/od/data/api/9F468A11-90C2-4747-B44F-3B9BCFDAEBC0?$format=json&$top=32326&$filter=site_id%20eq%20{0}", (obj.City + obj.Town).Replace("台", "臺")));
                        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
                        request.UseDefaultCredentials = true;
                        request.Timeout = 10000;
                        // 取得回應  
                        HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                        // 讀取結果 
                        StreamReader sr = new StreamReader(response.GetResponseStream());
                        //解析
                        var result = new JsonSerializer().Deserialize<List<govCity>>(sr.ReadToEnd());
                        if (result.Count > 0 && !string.IsNullOrEmpty(roadName))
                        {
                            if (result.Any(x => Helper.mapCnumLettersString(x.road).Contains(roadName.Replace("台灣", "臺灣")) || roadName.Replace("台灣", "臺灣").Contains(Helper.mapCnumLettersString(x.road))))
                            {
                                return string.Empty;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex.Message);
                    }


                    var cityId = CityManager.TownShipGetById(townId).ParentId.GetValueOrDefault();
                    if (!string.IsNullOrEmpty(roadName.Trim()))
                    {
                        //改從db檢查
                        foreach (var item in CityManager.BuildingListGetByTownshipId(cityId))
                        {
                            building = Helper.mapCnumLettersString(item.BuildingName);
                            if (building == roadName)
                            {
                                return string.Empty;
                            }
                            else if (building.Contains(roadName) || roadName.Contains(building))
                            {
                                break;
                            }
                        }
                    }

                    //Init
                    LevenshteinDistance distance = new LevenshteinDistance();
                    decimal matchRate = 0.5M;
                    string sgName = string.Empty;

                    //用路名來推估縣市
                    foreach (var item in CityManager.BuildingListGetByName(obj.Road))
                    {
                        if (Helper.mapCnumLettersString(Helper.ConvertToHalf(item.BuildingName)).Contains(roadName) || roadName.Contains(Helper.mapCnumLettersString(Helper.ConvertToHalf(item.BuildingName))))
                        {
                            var townShip = CityManager.TownShipGetById(item.CityId);
                            var city = CityManager.CityGetById(townShip.ParentId.GetValueOrDefault());
                            if (townShip.CityName == obj.Town)
                            {
                                sgName = city.CityName;
                                if (distance.LevenshteinDistancePercent(city.CityName, obj.City) >= matchRate)
                                {
                                    sgName = city.CityName;
                                    break;
                                }
                            }
                            if (city.CityName == obj.City)
                            {
                                sgName = townShip.CityName;
                                if (distance.LevenshteinDistancePercent(townShip.CityName, obj.Town) >= matchRate)
                                {
                                    sgName = townShip.CityName;
                                    break;
                                }
                            }
                        }
                    }

                    //用縣市推算可能路名
                    if (string.IsNullOrEmpty(sgName))
                    {
                        foreach (var item in CityManager.BuildingListGetByTownshipId(townId).OrderByDescending(x => x.BuildingName))
                        {
                            building = Helper.mapCnumLettersString(Helper.ConvertToHalf(item.BuildingName));
                            if (distance.LevenshteinDistancePercent(building, roadName) >= matchRate)
                            {
                                matchRate = distance.LevenshteinDistancePercent(building, roadName);
                                sgName = item.BuildingName;
                            }
                        }
                    }

                    if (new string[] { "郵政", "郵局" }.Any(oriAddress.Contains))
                    {
                        return string.Empty;
                    }
                    if (!string.IsNullOrEmpty(sgName))
                    {
                        if (exStr.Any(oriAddress.Contains))
                        {
                            return string.Empty;
                        }
                        return string.Format("您想輸入的是否為：<font style='font-style:italic;'>{0}</font>", sgName);
                    }
                    return "疑似未知的地址，但不會影響您繼續購買，請再三確保地址的正確性。";
                }
                catch(Exception ex)
                {
                    logger.Error(ex.Message);
                }
                
            }
            return string.Empty;
        }

        private class govCity
        {
            public string city { get; set; }
            public string site_id { get; set; }
            public string road { get; set; }
        }
    }
}



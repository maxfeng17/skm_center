﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.BizLogic.Models;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Interface;
using LunchKingSite.WebApi.Core.OAuth;
using LunchKingSite.Core.Models.Entities;

namespace LunchKingSite.WebApi.Controllers
{
    /// <summary>
    /// 問卷調查
    /// </summary>
    public class QuestionController : BaseController
    {
        private static ISysConfProvider _cp = ProviderFactory.Instance().GetConfig();
        private static IQuestionProvider _qp = ProviderFactory.Instance().GetProvider<IQuestionProvider>();

        /// <summary>
        /// New Short Guid
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [OAuthScope(TokenScope.SystemManagement)]
        public ApiResult GetNewShortGuid()
        {
            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = ShortGuid.NewGuid()
            };
        }

        /// <summary>
        /// 取得活動
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet]
        [OAuthScope(TokenScope.QuestAccess)]
        public ApiResult GetEvent(string token)
        {
            var ev = _qp.GetQuestionEvent(token);
            return new ApiResult
            {
                Code = ev != null ? ApiResultCode.Success : ApiResultCode.DataNotFound,
                Data = ev ?? new QuestionEvent()
            };
        }

        /// <summary>
        /// 取得問題分類
        /// </summary>
        /// <param name="eventId"></param>
        /// <returns></returns>
        [HttpGet]
        [OAuthScope(TokenScope.QuestAccess)]
        public ApiResult GetCategory(int eventId)
        {
            var categoryList = _qp.GetQuestionItemCategory(eventId);

            return new ApiResult
            {
                Code = categoryList != null && categoryList.Any() ? ApiResultCode.Success : ApiResultCode.DataNotFound,
                Data = categoryList ?? new List<QuestionItemCategory>()
            };
        }

        /// <summary>
        /// 取得問題
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        [HttpGet]
        [OAuthScope(TokenScope.QuestAccess)]
        public ApiResult GetItem(int eventId, int categoryId)
        {
            var items = _qp.GetViewQuestionItem(eventId, categoryId);
            return new ApiResult
            {
                Code = items != null && items.Any() ? ApiResultCode.Success : ApiResultCode.DataNotFound,
                Data = items ?? new List<ViewQuestionItem>()
            };
        }

        /// <summary>
        /// 取得問題的答案
        /// </summary>
        /// <param name="itemId"></param>
        /// <returns></returns>
        [HttpGet]
        [OAuthScope(TokenScope.QuestAccess)]
        public ApiResult GetItemOptions(int itemId)
        {
            var options = _qp.GetQuestionItemOptions(itemId);
            return new ApiResult
            {
                Code = options != null && options.Any() ? ApiResultCode.Success : ApiResultCode.DataNotFound,
                Data = options ?? new List<QuestionItemOption>()
            };
        }

        /// <summary>
        /// 取得是否已進行過問卷調查
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [OAuthScope(TokenScope.SystemManagement)]
        public ApiResult CheckQuestionAnswered(int userId, int eventId)
        {
            var checkAnswer = _qp.GetQuestionAnswerByEventId(userId, eventId);            
            return new ApiResult
            {
                Code = checkAnswer.Any() ? ApiResultCode.DataIsExist : ApiResultCode.DataNotFound
            };
        }

        /// <summary>
        /// 答題
        /// </summary>
        /// <param name="answer"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.SystemManagement)]
        public ApiResult AnswerQuestion(QuestionAnswerModel answer)
        {
            return new ApiResult
            {
                Code = PromotionFacade.AnswerQuestion(answer)
            };
        }
    }
}

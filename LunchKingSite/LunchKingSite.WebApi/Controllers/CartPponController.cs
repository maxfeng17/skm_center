﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace LunchKingSite.WebApi.Controllers
{
    /// <summary>
    /// 購物車相關資料
    /// </summary>
    public class CartPponController : BaseController
    {
        #region Property

        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private static IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        private static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private static IItemProvider ip = ProviderFactory.Instance().GetProvider<IItemProvider>();
        
        /// <summary>
        /// 使用者名稱
        /// </summary>
        public string UserName
        {
            get { return HttpContext.Current.User.Identity.Name; }
        }
        
        /// <summary>
        /// 使用者UniqueId
        /// </summary>
        public int UserId
        {
            get
            {
                WebLib.Component.PponIdentity pponUser = HttpContext.Current.User.Identity as WebLib.Component.PponIdentity;
                if (pponUser == null)
                {
                    return 0;
                }
                else
                {
                    return pponUser.Id;
                }
            }
        }
        #endregion

        /// <summary>
        /// 取得檔次明細資料
        /// </summary>
        /// <param name="cd"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult GetDealDetail(CartDeal cd)
        {
            ApiResult result = new ApiResult();

            Guid bid = cd.BusinessHourGuid;

            DealDetail dealDetail = new DealDetail();

            IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid);

            ViewComboDealCollection combodeals = pp.GetViewComboDealAllByBid(vpd.BusinessHourGuid);
            if (combodeals.Count > 0)
            {
                vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(combodeals.FirstOrDefault().MainBusinessHourGuid.Value);
            }

            if (vpd.BusinessHourGuid != Guid.Empty)
            {
                //品生活頁籤
                if (vpd.CategoryIds.Contains(CategoryManager.Default.Piinlife.CategoryId))
                {
                    //品生活
                    dealDetail.isPiinLife = true;
                    dealDetail.PiinLifeTabList = CartFacade.GetDealPiinLifeTabList(vpd).Skip(1).ToDictionary(x => x.Key, y => y.Value);
                }
                //詳細介紹
                string dealDesc = ViewPponDealManager.DefaultManager.GetDealDescription(vpd);
                dealDetail.Description = string.Format("{0}<br />{1}", dealDesc, vpd.DealPromoDescription);
                //權益說明
                var contentLite = ViewPponDealManager.DefaultManager.GetPponContentLite(vpd);
                dealDetail.Restrictions = contentLite.Restrictions;
                //IconTags
                dealDetail.IconTags = vpd.IconTags;
                //售價
                dealDetail.ItemPrice = vpd.ItemPrice;
                //多檔次
                if (combodeals.Count > 0)
                {
                    //多檔次
                    dealDetail.ComboDeals = combodeals.Where(x => x.MainBusinessHourGuid != x.BusinessHourGuid)
                        .ToDictionary(x => x.BusinessHourGuid.ToString(), y => y.Title);
                }
                else
                {
                    DealBase deal = new DealBase()
                    {
                        BusinessHourGuid = vpd.BusinessHourGuid,
                        DealName = vpd.ItemName
                    };
                    dealDetail.Deal = deal;
                }
                //多分店
                ViewPponStoreCollection stores = pp.ViewPponStoreGetListByBidWithNoLock(bid, VbsRightFlag.VerifyShop);
                List<CartPponSotres> cartPponSotres = new List<CartPponSotres>();
                if (stores.Count > 0)
                {
                    foreach (var store in stores.OrderBy(x => x.SortOrder))
                    {
                        CartPponSotres cartPponSotre = new CartPponSotres()
                        {
                            StoreDescription = GetStoreOptionTitle(store),
                            StoreGuid = store.StoreGuid,
                            IsDisabled = IsStoreSellOut(store)
                        };
                        cartPponSotres.Add(cartPponSotre);
                    }
                    dealDetail.PponStores = cartPponSotres;
                }
                //多重選項                
                dealDetail.MultiOptions = GetMultiOptions(vpd.ItemGuid);

                //是否為成套票券
                dealDetail.isGroupCoupon = Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.GroupCoupon);

                //最大購買數量
                dealDetail.MaxItemCount = vpd.MaxItemCount ?? 0;
            }

            result.Code = ApiResultCode.Success;
            result.Data = dealDetail;
            return result;
        }

        /// <summary>
        /// 取得SessionId
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ApiResult GetTicketId()
        {
            ApiResult result = new ApiResult();

            string TicketId = "";

            if (!string.IsNullOrEmpty(UserName))
            {
                //使用者已登入
                TicketId = CartFacade.GetTicketId(UserName);
            }
            else
            {
                //使用者未登入
                TicketId = CartFacade.GetTicketId(null);
            }

            result.Code = ApiResultCode.Success;
            result.Data = TicketId;

            return result;
        }

        /// <summary>
        /// 將商品放入購物車
        /// </summary>
        /// <param name="cartItemDTO"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult PutItemToCart(CartItemDTO cartItemDTO)
        {
            ApiResult result = new ApiResult();

            LiteMainCart mainCartDTO = CartFacade.LiteCartListGet(cartItemDTO, UserName, cartItemDTO.TicketId);

            result.Code = ApiResultCode.Success;
            result.Data = mainCartDTO;

            return result;
        }
        /// <summary>
        /// 購物結帳頁
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ApiResult GetCartItem(paramCartTicketId param)
        {
            ApiResult result = new ApiResult();

            string newTicketId = param.TicketId;

            LiteMainCart mainCartDTO = new LiteMainCart();

            if (!string.IsNullOrEmpty(newTicketId))
            {
                mainCartDTO = CartFacade.LiteCartListGet(null, UserName, newTicketId);
            }

            result.Code = ApiResultCode.Success;
            result.Data = mainCartDTO;

            return result;
        }

        /// <summary>
        /// 取得小圖示
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult GetAppPic(paramCartBid param)
        {
            ApiResult result = new ApiResult();

            var _bid = param.BusinessHourGuid;

            string AppDealPic = CartFacade.GetAppPic(param.BusinessHourGuid);

            result.Code = ApiResultCode.Success;
            result.Data = AppDealPic;
            return result;
        }

        /// <summary>
        /// 取得檔次售價
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult GetDealItemPrice(paramCartBid param)
        {
            ApiResult result = new ApiResult();

            var _bid = param.BusinessHourGuid;
            Guid BusinessHourGuid = Guid.Empty;
            Guid.TryParse(_bid, out BusinessHourGuid);

            IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(BusinessHourGuid);

            result.Code = ApiResultCode.Success;
            result.Data = vpd.ItemPrice;
            return result;
        }

        /// <summary>
        /// 取得檔次資料
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult GetDealBase(paramCartBid param)
        {
            ApiResult result = new ApiResult();

            var _bid = param.BusinessHourGuid;
            Guid BusinessHourGuid = Guid.Empty;
            Guid.TryParse(_bid, out BusinessHourGuid);

            IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(BusinessHourGuid);

            dynamic obj = new
            {
                MultiOptions = GetMultiOptions(vpd.ItemGuid),
                isGroupCoupon = Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.GroupCoupon),
                MaxItemCount = vpd.MaxItemCount
            };

            result.Code = ApiResultCode.Success;
            result.Data = obj;
            return result;
        }

        /// <summary>
        /// 取得檔次成套倍數
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult GetDealComboPackCount(paramCartBid param)
        {
            ApiResult result = new ApiResult();

            var _bid = param.BusinessHourGuid;
            Guid BusinessHourGuid = Guid.Empty;
            Guid.TryParse(_bid, out BusinessHourGuid);

            IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(BusinessHourGuid);

            result.Code = ApiResultCode.Success;
            result.Data = vpd.ComboPackCount.HasValue ? vpd.ComboPackCount.Value.ToString() : "1";
            return result;
        }

        /// <summary>
        /// 取得會員愛心收藏
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ApiResult GetMemberCollectDeal()
        {
            ApiResult result = new ApiResult();
            List<Guid> vmcs = new List<Guid>();

            if (!string.IsNullOrEmpty(UserName))
            {
                //使用者已登入
                vmcs = mp.ViewMemberCollectionDealGetListByPager(UserId, 1, 999, "collect_time")
                    .Select(x=>x.BusinessHourGuid).ToList();           
            }
            result.Code = ApiResultCode.Success;
            result.Data = vmcs;
            return result;
        }


        private bool IsStoreSellOut(ViewPponStore store)
        {
            if (store.TotalQuantity <= 0 || store.OrderedQuantity >= store.TotalQuantity)
            {
                return true;
            }
            return false;
        }

        private string GetStoreOptionTitle(ViewPponStore store)
        {
            return string.Format("{0} {1}{2}{3}",
                store.StoreName, store.CityName, store.TownshipName, store.AddressString);
        }
        private List<CartMultiOptions> GetMultiOptions(Guid ItemGuid)
        {
            AccessoryGroupCollection viagc = ip.AccessoryGroupGetListByItem(ItemGuid);
            List<CartMultiOptions> cartMultiOptions = new List<CartMultiOptions>();

            int itemIdx = 1;
            foreach (var ag in viagc)
            {
                ViewItemAccessoryGroupCollection vigc = ip.ViewItemAccessoryGroupGetList(ItemGuid, ag.Guid);

                List<ViewItemAccessoryGroup> members = vigc.ToList();
                List<CartAccessoryGroupMember> cartAccessoryGroupMembers = new List<CartAccessoryGroupMember>();
                foreach (ViewItemAccessoryGroup member in members)
                {
                    CartAccessoryGroupMember cartAccessoryGroupMember = new CartAccessoryGroupMember()
                    {
                        AccessoryGroupMemberSequence = member.AccessoryGroupMemberSequence ?? 1,
                        AccessoryGroupMemberGUID = member.AccessoryGroupMemberGuid,
                        AccessoryName = member.AccessoryName,
                        Quantity = member.Quantity
                    };
                    cartAccessoryGroupMembers.Add(cartAccessoryGroupMember);
                }

                CartMultiOptions cartMultiOption = new CartMultiOptions()
                {
                    ItemAccessoryGroupListSequence = itemIdx,
                    ItemAccessoryGroupListName = ag.AccessoryGroupName,
                    GroupMembers = cartAccessoryGroupMembers
                };
                cartMultiOptions.Add(cartMultiOption);

                itemIdx++;
            }
            return cartMultiOptions;
        }
    }
}

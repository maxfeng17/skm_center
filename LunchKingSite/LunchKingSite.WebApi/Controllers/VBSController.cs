﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.WebApi.Models;
using LunchKingSite.WebLib.Component;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.Http;
using LunchKingSite.Core.Component;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.WebApi.Core.Attributes;
using Newtonsoft.Json.Linq;
using OAuth = LunchKingSite.WebApi.Core.OAuth;
using VbsModel = LunchKingSite.BizLogic.Model.VBS;

namespace LunchKingSite.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [Authorize]
    public class VBSController : BaseController
    {
        /// <summary>
        /// 
        /// </summary>
        protected static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        /// <summary>
        /// 
        /// </summary>
        protected const string vbsPost = "@vbs";
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [RequireHttps]
        [HttpPost]
        public ApiResult ChangePassword([FromBody]JObject data)
        {
            string userName;
            string password;
            string newpassword;

            ApiResult result = new ApiResult();

            try
            {
                userName = User.Identity.Name;
                userName = userName.IndexOf(vbsPost) > 0 ? userName.Replace(vbsPost, "") : userName;
                password = (string)data.Property("Password").Value;
                newpassword = (string)data.Property("Newpassword").Value;
            }
            catch (Exception)
            {
                result.Code = ApiResultCode.InputError;
                result.Message = "參數錯誤";

                SetApiLog(GetMethodName("ChangePassword"), AppId, new { }, result, Helper.GetClientIP());
                return result;
            }

            string retypedNewPassword = newpassword;
            bool checkPwd = true;
            if (string.Compare(userName, "DEMONO0001", true) == 0)
            {
                #region DemoUser password check
                if (string.Compare(password, "123456", false) != 0)
                {
                    checkPwd = false;
                    result.Message = I18N.Message.PasswordVerificationFailure;
                }

                if (checkPwd && !string.Equals(newpassword, retypedNewPassword))
                {
                    checkPwd = false;
                    result.Message = I18N.Message.PasswordInconsistent;
                }

                if (checkPwd && !VbsMemberUtility.CheckPassword(newpassword))
                {
                    checkPwd = false;
                    result.Message = I18N.Message.PasswordFormatRule;
                }

                #endregion DemoUser password check
            }
            else
            {
                #region  IsOriginalPasswordCorrect

                if (!VbsMemberUtility.VerifyPassword(userName, password))
                {
                    checkPwd = false;
                    result.Message = I18N.Message.PasswordVerificationFailure;
                }
                #endregion IsOriginalPasswordCorrect

                #region  IsNewPasswodAndRetypedNewPasswordTheSame

                if (checkPwd && !string.Equals(newpassword, retypedNewPassword))
                {
                    checkPwd = false;
                    result.Message = I18N.Message.PasswordInconsistent;
                }
                #endregion IsNewPasswodAndRetypedNewPasswordTheSame

                #region  IsPasswordFormatCorrect
                if (checkPwd && !VbsMemberUtility.CheckPassword(newpassword))
                {
                    checkPwd = false;
                    result.Message = I18N.Message.PasswordFormatRule;
                }
                #endregion IsPasswordFormatCorrect

                #region IsChagnePassword
                if (checkPwd && !VbsMemberUtility.ChangePassword(userName, password, newpassword))
                {
                    checkPwd = false;
                    result.Message = I18N.Message.PasswordChangeFailure;
                }
                #endregion IsChagnePassword
            }

            if (checkPwd)
            {
                result.Code = ApiResultCode.Success;
                result.Data = ApiReturnCode.Success.ToString();
            }
            else
            {
                result.Code = ApiResultCode.BadUserInfo;
            }

            SetApiLog(GetMethodName("ChangePassword"), AppId, new { userName }, result, Helper.GetClientIP());

            return result;
        }        
        
        /// <summary>
        /// 單筆核銷 VerifyCheck
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [RequireHttps]
        [HttpPost]
        public ApiResult VerifyCheck([FromBody]JObject data)
        {
            string couponSerial;
            string couponCode;
            
            ApiResult result = new ApiResult();
            var verifyData = new List<VbsModel.VerifyCode>();
            try
            {
                couponSerial = (string)data.Property("CouponSerial").Value;
                couponCode = (string)data.Property("CouponCode").Value;
                verifyData.Add(new VbsModel.VerifyCode
                {
                    CouponSerial = couponSerial,
                    CouponCode = couponCode,
                    TrustId = Guid.Empty
                });
            }
            catch (Exception)
            {
                result.Code = ApiResultCode.InputError;
                result.Message = "參數錯誤";
                SetApiLog(GetMethodName("VerifyCheck"), AppId, new { }, result, Helper.GetClientIP());
                return result;
            }

            var userId = MemberFacade.GetUniqueId(User.Identity.Name);

            VbsMembership memOriginal = VbsMemberUtility.GetVbsMemberShipByUserId(userId);

            var resultInfo = VBSFacade.VerifyCheckInfo(memOriginal, verifyData);//單筆核銷

            result.Code = resultInfo.Item1;
            result.Data = resultInfo.Item2;
            result.Message = resultInfo.Item3;

            return result;
        }
        
        /// <summary>
        /// 多筆核銷 VerifyCheck
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [RequireHttps]
        [HttpPost]
        public ApiResult VerifyCheckList([FromBody]JObject data)
        {
            ApiResult result = new ApiResult();
            var verifyData = new List<VbsModel.VerifyCode>();
            try
            {
                var ary = ((JArray)data["VerifyData"]).Cast<dynamic>().ToArray();

                foreach (var item in ary)
                {
                    verifyData.Add(new VbsModel.VerifyCode
                    {
                        CouponSerial = item.CouponSerial,
                        CouponCode = item.CouponCode,
                        TrustId = Guid.Empty
                    });
                }
            }
            catch (Exception)
            {
                result.Code = ApiResultCode.InputError;
                result.Message = "參數錯誤";
                SetApiLog(GetMethodName("VerifyCheckList"), AppId, new { }, result, Helper.GetClientIP());
                return result;
            }

            var userId = MemberFacade.GetUniqueId(User.Identity.Name);

            VbsMembership memOriginal = VbsMemberUtility.GetVbsMemberShipByUserId(userId);

            var resultInfo = VBSFacade.VerifyCheckInfo(memOriginal,verifyData); //多筆核銷

            result.Code = resultInfo.Item1;
            result.Data = resultInfo.Item2;
            result.Message = resultInfo.Item3;

            return result;
        }
        
        /// <summary>
        /// 針對 trustId 核銷 (cash_trust_log)
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [RequireHttps]
        [HttpPost]
        public ApiResult VerifyCoupon([FromBody]JObject data)
        {
            //憑證ID
            string trustId;

            VerificationStatus status;
            ApiResult result = new ApiResult();

            try
            {
                trustId = (string)data.Property("TrustId").Value;
            }
            catch (Exception)
            {
                result.Code = ApiResultCode.InputError;
                result.Message = "參數錯誤";

                SetApiLog(GetMethodName("VerifyCoupon"), AppId, new { }, result, Helper.GetClientIP());
                return result;
            }
            //API UserID
            var userId = MemberFacade.GetUniqueId(User.Identity.Name);

            VbsMembership memOriginal = VbsMemberUtility.GetVbsMemberShipByUserId(userId);
            status = OrderFacade.VerifyCoupon(new Guid(trustId), false, memOriginal.AccountId, Helper.GetClientIP(), "");
            
            CashTrustLog cashTrustLog;
            switch (status)
            {
                case VerificationStatus.CouponOk:
                    result.Code = ApiResultCode.Success;
                    result.Data = "已核銷成功，請繼續下一筆核銷。";
                    break;
                case VerificationStatus.CouponUsed:
                    cashTrustLog = VerificationFacade.GetCashTrustLog(new Guid(trustId));
                    result.Code = ApiResultCode.CouponUsed;
                    result.Message = "此憑證已使用過了，\n核銷時間：" + cashTrustLog.ModifyTime;
                    break;
                case VerificationStatus.CouponReted:
                    cashTrustLog = VerificationFacade.GetCashTrustLog(new Guid(trustId));
                    result.Code = ApiResultCode.CouponReturned;
                    result.Message = "此憑證已退貨\n退貨時間：" + cashTrustLog.ModifyTime;
                    break;
                case VerificationStatus.CouponLocked:
                    result.Code = ApiResultCode.SystemBusy;
                    result.Message = "您欲核銷的憑證，目前處於【送禮中】的狀態，所以無法完成核銷，請您協助消費者再次確認憑證狀態後，再進行核銷。";
                    break;
                case VerificationStatus.CouponError:
                case VerificationStatus.CashTrustLogErr:
                case VerificationStatus.CashTrustStatusLogErr:
                default:
                    result.Code = ApiResultCode.SystemBusy;
                    result.Message = "系統繁忙中，請30秒後再試一次。";
                    break;
            }

            SetApiLog(GetMethodName("VerifyCoupon"), AppId, new { userId , trustId, DateTime.Now, status }
                , result, Helper.GetClientIP());

            return result;
        }

        /// <summary>
        /// 成套商品核銷專用
        /// </summary>
        [RequireHttps]
        [HttpPost]
        public ApiResult VerifyCheckCouponGroup([FromBody] JObject data)
        {
            var result = new ApiResult();
            Guid orderGuid;  //核銷訂單
            int couponUserId; //憑證擁有者UserId
            int verifyCount; //欲核銷數量
            List<Guid> tids; //準備核銷憑證 trust id
            string timestamp;

            try
            {
                orderGuid = (Guid)data.Property("OrderGuid").Value;
                couponUserId = (int)data.Property("OrderUserId").Value;
                verifyCount = (int)data.Property("VerifyCount").Value;
                timestamp = (string)data.Property("Timestamp").Value;
            }
            catch (Exception)
            {
                result.Code = ApiResultCode.InputError;
                result.Message = "參數錯誤";
                SetApiLog(GetMethodName("VerifyCouponGroup"), AppId, new { }, result, Helper.GetClientIP());
                return result;
            }

            if (VBSFacade.GroupCouponIsExist(orderGuid, timestamp))
            {
                result.Code = ApiResultCode.CouponUsed;
                result.Message = "QR code 已使用過，請消費者重新選擇兌換數量，再生成 QR code";
                return result;
            }
           
            var verifyResult = VBSFacade.CheckCouponGroupTrustIdQuantity(couponUserId, orderGuid, verifyCount, out tids);

            if (verifyResult != GroupCouponVerifyCheckType.Success)
            {
                switch (verifyResult)
                {
                    case GroupCouponVerifyCheckType.QtyNotEnough:                
                        result.Message = "可核銷數量不足";
                        break;
                    case GroupCouponVerifyCheckType.CouponNotEnough:
                        result.Message = "需使用完一般憑證才可核銷贈品憑證";
                        break;
                    case GroupCouponVerifyCheckType.GiveawayCouponNotEnouth:
                        result.Message = "可核銷數量不足";
                        break;
                }

                result.Code = ApiResultCode.CouponQtyNotEnough;
                SetApiLog(GetMethodName("VerifyCouponGroup"), AppId, new { }, result, Helper.GetClientIP());
                return result;
            }

            var userId = MemberFacade.GetUniqueId(User.Identity.Name);
            VbsMembership memOriginal = VbsMemberUtility.GetVbsMemberShipByUserId(userId);
            var verifyData = tids.Select(x => new VbsModel.VerifyCode
                                                {
                                                    CouponCode = string.Empty,
                                                    CouponSerial = string.Empty,
                                                    TrustId = x
                                                }).ToList();

            var resultInfo = VBSFacade.VerifyCheckInfo(memOriginal, verifyData); //成套核銷

            result.Code = resultInfo.Item1;
            result.Data = resultInfo.Item2;
            result.Message = resultInfo.Item3;

            return result;
        }

        /// <summary>
        /// 針對 trustId 核銷 (cash_trust_log)
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [RequireHttps]
        [HttpPost]
        public ApiResult VerifyCouponList([FromBody]JObject data)
        {
            ApiResult result = new ApiResult();

            var tids = new List<Guid>();
            Guid orderGuid;  //核銷訂單
            string timestamp;

            try
            {
                var ary = ((JArray) data["VerifyTid"]).Cast<dynamic>().ToArray();
                tids.AddRange(ary.Select(item => new Guid(item.Value)));
                orderGuid = (Guid)data.Property("OrderGuid").Value;
                timestamp = (string)data.Property("Timestamp").Value;
            }
            catch (Exception)
            {
                result.Code = ApiResultCode.InputError;
                result.Message = "參數錯誤";

                SetApiLog(GetMethodName("VerifyCoupon"), AppId, new { }, result, Helper.GetClientIP());
                return result;
            }
            var userId = MemberFacade.GetUniqueId(User.Identity.Name);

            VbsMembership memOriginal = VbsMemberUtility.GetVbsMemberShipByUserId(userId);
            
            VbsModel.MultiCouponVerifyResult resultData;
            var groupCode = VBSFacade.MultiCouponVerify(memOriginal, tids, orderGuid, timestamp, Helper.GetClientIP(), out resultData);

            result.Data = resultData;

            switch (groupCode) 
            {
                case ApiResultCode.CouponPartialFail:
                    result.Code = ApiResultCode.CouponPartialFail;
                    result.Message = "此批憑證部份核銷失敗";
                    break;
                case ApiResultCode.CouponUsed:
                    result.Code = ApiResultCode.CouponUsed;
                    result.Message = "code 已使用過，請消費者重新選擇兌換數量，再生成 QR code";
                    break;
                default:
                    result.Code = ApiResultCode.Success;
                    result.Message = "核銷成功，是否繼續核銷?";
                    break;
            }

            SetApiLog(GetMethodName("VerifyCoupon"), AppId, new { userId, tids, DateTime.Now, groupCode }
                , result, Helper.GetClientIP());

            return result;
        }

        /// <summary>
        /// 商家帳號的詳細資料 Insert Update
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [RequireHttps]
        [HttpPost]
        public ApiResult SetMemberDetail([FromBody]MemberDetail memberDetail)
        {
            var result = new ApiResult();

            if (VbsMemberUtility.VbsCompanyDetailSet(memberDetail))
            {
                result.Code = ApiResultCode.Success;
            }
            else
            {
                result.Code = ApiResultCode.SaveFail;
                result.Message = "更新失敗";
            }

            return result;
        }

        /// <summary>
        /// 商家帳號的詳細資料
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [RequireHttps]
        [HttpPost]
        public ApiResult MemberDetail()
        {
            var result = new ApiResult();

            var userId = MemberFacade.GetUniqueId(User.Identity.Name);
            var memberDetail = VbsMemberUtility.ViewVbsMemberDetailGet(userId);
            if (!string.IsNullOrWhiteSpace(memberDetail.LoginId))
            {
                result.Code = ApiResultCode.Success;
                result.Data = memberDetail;
            }
            else
            {
                result.Code = ApiResultCode.DataNotFound;
                result.Message = I18N.Phrase.ApiReturnCodeDataNotFound;
            }

            return result;
        }
        
        /// <summary>
        /// 回傳商家帳號取得的總覽資訊
        /// </summary>
        /// <remarks>
        /// 修改自VBSHome，為防止User.Identity.Name與主站登入權限混用，應用accessToken反查使用者
        /// </remarks>
        /// <returns></returns>
        [RequireHttps, HttpPost]
        public ApiResult VBSHome([FromBody] JObject inputParameter)
        {
            var result = new ApiResult() { Code = ApiResultCode.Error };
            try
            {
                int sellerUserId = 0;
                dynamic input = inputParameter as dynamic;
                var cardGroupId = (input.GroupId != 0) ? Convert.ToInt32(input.GroupId) : 0;

                string accessToken = Request.Headers.Authorization.Parameter;
                if (!string.IsNullOrWhiteSpace(accessToken))
                {
                    Member member = MemberFacade.GetMemberByAccessToken(accessToken);
                    sellerUserId = member.UniqueId;
                }
                else
                {
                    //※User.Identity.Name取得到表示已經用商家帳號登入17Life主站，此為不正確的！
                    //待核銷App的登入修改正確後，else裡面應整個移除，目前先保留也是為了相容核銷App較舊的版本
                    sellerUserId = MemberFacade.GetUniqueId(User.Identity.Name);
                }

                var query = new VbsModel.VBSQueryModel
                {
                    SellerUserId = sellerUserId,
                    GroupId = cardGroupId
                };

                var data = VBSFacade.GetVbsHomeInfo(query);
                result.Data = data;

                result.Code = ApiResultCode.Success;
                return result;
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
                return result;
            }
        }

        /// <summary>
        /// 取得vbs帳號管理的所有分店資料列表
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [RequireHttps]
        [HttpPost]
        public ApiVersionResult VbsStores()
        {
            var result = new ApiVersionResult();

            var allowStores = VBSFacade.GetVBSMemberAllowStoreList(User.Identity.Id);
            List<StoreModel> rtnList = new List<StoreModel>();
            foreach (var allowStore in allowStores)
            {
                rtnList.Add(new StoreModel(allowStore));
            }
            result.Code = ApiResultCode.Success;
            //todo:現階段核銷帳號權限修改時沒有任何可作為依據的值，之後需要補上
            result.Version = new ApiVersionItem("VbsStoreVersion","1");
            result.Data = rtnList;

            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [RequireHttps(false)]
        [HttpPost,HttpGet]
        public ApiVersionResult BankNoList()
        {
            var result = new ApiVersionResult();
            result.Version = BankManager.BankNoDataVersion;

            List<ApiBankNo> rtnList = VourcherFacade.ApiBankNoGetList();
            if (rtnList!=null && rtnList.Count > 0)
            {
                result.Code = ApiResultCode.Success;
                result.Data = rtnList;
            }
            else
            {
                result.Code = ApiResultCode.DataNotFound;
                result.Message = I18N.Phrase.ApiReturnCodeDataNotFound;
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [RequireHttps(false)]
        [HttpPost]
        public ApiVersionResult BranchNoList([FromBody]JObject data)
        {
            var result = new ApiVersionResult();
            string bankNo = string.Empty;
            if (data != null && data["BankNo"] != null)
            {
                bankNo = data["BankNo"].ToString();
            }
            else
            {
                result.Code = ApiResultCode.InputError;
                result.Message = I18N.Phrase.ApiReturnCodeInputError;
                return result;
            }

            result.Version = BankManager.BankNoDataVersion;

            List<ApiBranchNo> rtnList = VourcherFacade.ApiBranchNoGetList(bankNo);
            if (rtnList != null && rtnList.Count > 0)
            {
                result.Code = ApiResultCode.Success;
                result.Data = rtnList;
            }
            else
            {
                result.Code = ApiResultCode.DataNotFound;
                result.Message = I18N.Phrase.ApiReturnCodeDataNotFound;
            }

            return result;
        }
        
        /// <summary>
        /// 回傳所有銀行與分行的資料
        /// </summary>
        /// <returns></returns>
        [RequireHttps(false)]
        [HttpPost]
        public ApiVersionResult BankAndBranchNo()
        {
            var result = new ApiVersionResult();
            result.Version = BankManager.BankNoDataVersion;

            List<ApiBankNo> rtnList = BankManager.BankNoList;
            if (rtnList != null && rtnList.Count > 0)
            {
                result.Code = ApiResultCode.Success;
                result.Data = rtnList;
            }
            else
            {
                result.Code = ApiResultCode.DataNotFound;
                result.Message = I18N.Phrase.ApiReturnCodeDataNotFound;
            }

            return result;
        }

    }
}

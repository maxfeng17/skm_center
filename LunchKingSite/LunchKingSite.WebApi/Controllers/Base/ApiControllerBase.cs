﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web.Http;
using System.Web.Http.Description;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.WebApi.Core;
using LunchKingSite.WebApi.Core.Attributes;
using LunchKingSite.WebApi.Models;
using LunchKingSite.WebLib.Component;

namespace LunchKingSite.WebApi.Controllers.Base
{
    /// <summary>
    /// 提供自動說明文件 /controller/help
    /// </summary>
    public class ApiControllerBase : ApiController
    {
        /// <summary>
        /// 
        /// </summary>
        public new PponPrincipal User
        {
            get
            {
                if (base.User.Identity.IsAuthenticated == false)
                {
                    return PponPrincipal.Guest;
                }
                PponPrincipal user = base.User as PponPrincipal;
                if (user == null)
                {
                    PponIdentity pponUser = PponIdentity.Get(base.User.Identity.Name);
                    user = new PponPrincipal(pponUser);
                }
                return user;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpGet]
        public HttpResponseMessage Help()
        {
            XmlCommentDocumentationProvider docProvider = GlobalConfiguration.Configuration.Services.GetDocumentationProvider()
                as XmlCommentDocumentationProvider;
            if (docProvider == null)
            {                
                throw new HttpErrorResponseException(ApiResultCode.Error, "說明文件需要的描述檔不存在 LunchKingSite.WebAPI.xml");
            }
            ApiHelperBuilder sb = new ApiHelperBuilder(docProvider);

            List<Type> relatedTypes = new List<Type>();
            List<Type> relatedEnums = new List<Type>();
            IApiExplorer apiExplorer = GlobalConfiguration.Configuration.Services.GetApiExplorer();

            string headBootstrapStyleAndScript = @"
<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css'>
<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css'>
<link rel='stylesheet' href='http://getbootstrap.com/assets/css/docs.min.css'>
<style>
.bs-callout {
    margin: 20px 0 5px 0;
}
.code-comment {
    color: #008000
}
</style>
<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js'></script>
";
            sb.AppendLine("<!DOCTYPE html>");
            sb.AppendLine("<html>");
            sb.AppendLine("<head>");
            sb.AppendLine("<meta charset='utf-8'>");
            sb.Append(headBootstrapStyleAndScript);
            sb.AppendLine("</head>");
            sb.AppendLine("<body style='margin:10px 20px 10px 20px'>");
            sb.AppendFormat(@"
<div class='page-header'>
  <h1>{0} <small>{1}</small></h1>
</div>
", DocumentSetting.Title, DocumentSetting.Version);
            
            bool isCamcelCase = GetType().HasAttribute(typeof(ForceCamelCaseAttribute));
            
            foreach (ApiDescription t in apiExplorer.ApiDescriptions)
            {
                ApiDescriptionAdapter api = new ApiDescriptionAdapter(t, isCamcelCase);
                if (api.ActionDescriptor.ControllerDescriptor.ControllerType != this.GetType())
                {
                    continue;
                }
                if (DocumentSetting.UrlRegex.IsMatch(api.RelativePath) == false)
                {
                    continue;
                }

                sb.AppendFormat(@"
    <div class='bs-callout bs-callout-info'>
        <h4>{0}</h4>
        <p>{1} <code>{2}</code></p>
    </div>
", api.Documentation, api.HttpMethod, api.RelativePath);

                sb.AppendLine("<div class='highlight'>");

                if (api.ResponseDescription.DeclaredType != null)
                {
                    if (api.ResponseDescription.DeclaredType.IsEnum)
                    {
                        if (relatedEnums.Contains(api.ResponseDescription.DeclaredType) == false)
                        {
                            relatedEnums.Add(api.ResponseDescription.DeclaredType);
                        }
                    }
                    else if (api.ResponseDescription.DeclaredType.IsClass && api.ResponseDescription.DeclaredType != typeof(string))
                    {
                        if (relatedTypes.Contains(api.ResponseDescription.DeclaredType) == false)
                        {
                            relatedTypes.Add(api.ResponseDescription.DeclaredType);
                        }
                        foreach (var propInfo in api.ResponseDescription.DeclaredType.GetProperties())
                        {
                            if (propInfo.PropertyType.IsEnum)
                            {
                                if (relatedEnums.Contains(propInfo.PropertyType) == false)
                                {
                                    relatedEnums.Add(propInfo.PropertyType);
                                }
                            }
                        }
                    }

                    sb.Append("<h6>Return: ");
                    sb.RenderAnchor(api.ResponseDescription.DeclaredType, relatedTypes);
                    sb.Append("</h6>");
                }

                if (api.ParameterDescriptions.Count > 0)
                {
                    sb.AppendLine("<h6>Parameters</h6>");
                    sb.AppendLine("<ul>");
                    foreach (var parameter in api.ParameterDescriptions)
                    {
                        if (parameter.ParameterDescriptor.ParameterType.IsEnum)
                        {
                            if (relatedEnums.Contains(parameter.ParameterDescriptor.ParameterType) == false)
                            {
                                relatedEnums.Add(parameter.ParameterDescriptor.ParameterType);
                            }
                        } 
                        else if (parameter.ParameterDescriptor.ParameterType.IsClass && parameter.ParameterDescriptor.ParameterType != typeof(string))
                        {
                            if (relatedTypes.Contains(parameter.ParameterDescriptor.ParameterType) == false)
                            {
                                relatedTypes.Add(parameter.ParameterDescriptor.ParameterType);
                            }
                            foreach (var propInfo in parameter.ParameterDescriptor.ParameterType.GetProperties())
                            {
                                if (propInfo.PropertyType.IsEnum)
                                {
                                    if (relatedEnums.Contains(propInfo.PropertyType) == false)
                                    {
                                        relatedEnums.Add(propInfo.PropertyType);
                                    }
                                }
                            }
                        }

                        sb.AppendLine("<li>");
                        sb.Append(parameter.Name);
                        sb.Append(" ");
                        sb.RenderAnchor(parameter.ParameterDescriptor.ParameterType, relatedTypes);
                        if (string.IsNullOrEmpty(parameter.Documentation) == false)
                        {
                            sb.Append(": ");
                            sb.Append(parameter.Documentation);
                        }
                        sb.AppendLine("</li>");
                    }
                    sb.AppendLine("</ul>");
                }
                sb.AppendLine("</div>");
            }
            //relatedTypes.Add(typeof(TrustCodeModel));
            //relatedTypes.Add(typeof(CancelInputModel));
            //relatedTypes.Add(typeof(ApiResult));
            //relatedEnums.Add(typeof(ApiResultCode));


            if (relatedTypes.Count > 0)
            {
                sb.Append("<h1>相關物件</h1>");
                foreach (Type type in relatedTypes)
                {
                    string desc = docProvider.GetClassOrEnumDocumentation(type);
                    sb.RenderClass(type, desc);
                }
            }      

            if (relatedEnums.Count > 0)
            {
                sb.Append("<h1>相關列舉</h1>");
                foreach (Type type in relatedEnums)
                {
                    string desc = docProvider.GetClassOrEnumDocumentation(type);
                    sb.RenderEnum(type, desc);
                }
            }

            sb.Append("</body></html>");
            
            var response = new HttpResponseMessage();
            response.Content = new StringContent(sb.ToString());
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
            return response;
        }
        /// <summary>
        /// 
        /// </summary>
        public virtual HelperDocumentSetting DocumentSetting {
            get
            {
                return new HelperDocumentSetting
                {
                    Title = "WebApi 規格參考",
                    Version = Assembly.GetExecutingAssembly().GetName().Version.ToString(),
                    UrlRegex = new Regex(@"api\/[\S]*", RegexOptions.IgnoreCase | RegexOptions.Compiled)
                };
            }
        }

        /// <summary>
        /// 取得 ApiResultCode List
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ApiResult ApiResultCodeList()
        {
            var codeList = (from ApiResultCode code in Enum.GetValues(typeof(ApiResultCode))
                            select new ApiResultCodeModel
                            {
                                ResultCode = code.ToString(),
                                ResultNo = (int)code,
                                ResultDescript = LunchKingSite.Core.Helper.GetLocalizedEnum(code)
                            }).ToList();

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = codeList
            };
        }
    }
}
using System.Text.RegularExpressions;

namespace LunchKingSite.WebApi.Controllers.Base
{
    /// <summary>
    /// 
    /// </summary>
    public class HelperDocumentSetting
    {
        /// <summary>
        /// 
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Version { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Regex UrlRegex { get; set; }
    }
}
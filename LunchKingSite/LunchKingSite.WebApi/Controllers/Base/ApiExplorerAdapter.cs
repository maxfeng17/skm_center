﻿using System.Collections.ObjectModel;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Description;
using System.Web.Http.Routing;
using LunchKingSite.Core;

namespace LunchKingSite.WebApi.Controllers.Base
{
    /// <summary>
    /// 
    /// </summary>
    public class ApiDescriptionAdapter
    {
        private readonly bool camelCase;
        private ApiDescription apiDesc;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="apiDesc"></param>
        /// <param name="camelCase"></param>
        public ApiDescriptionAdapter(ApiDescription apiDesc, bool camelCase)
        {
            this.camelCase = camelCase;
            this.apiDesc = apiDesc;
        }
        /// <summary>
        /// 
        /// </summary>
        public HttpActionDescriptor ActionDescriptor {
            get { return apiDesc.ActionDescriptor; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Documentation {
            get { return apiDesc.Documentation; }
        }
        /// <summary>
        /// 
        /// </summary>
        public HttpMethod HttpMethod {
            get { return apiDesc.HttpMethod; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Id {
            get { return apiDesc.ID; }
        }
        /// <summary>
        /// 
        /// </summary>
        public Collection<ApiParameterDescription> ParameterDescriptions {
            get { return apiDesc.ParameterDescriptions; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string RelativePath
        {
            get
            {
                if (camelCase)
                {
                    return apiDesc.RelativePath.ToCamelCase();
                }
                return apiDesc.RelativePath;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public ResponseDescription ResponseDescription {
            get { return apiDesc.ResponseDescription; }
        }
        /// <summary>
        /// 
        /// </summary>
        public IHttpRoute Route {
            get { return apiDesc.Route; }
        }
    }
}

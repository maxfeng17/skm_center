﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace LunchKingSite.WebApi.Controllers.Base
{
    /// <summary>
    /// 
    /// </summary>
    public class ApiHelperBuilder
    {
        private StringBuilder sb = new StringBuilder();

        XmlCommentDocumentationProvider docProvider;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="docProvider"></param>
        public ApiHelperBuilder(XmlCommentDocumentationProvider docProvider)
        {
            this.docProvider = docProvider;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        public void Append(string value)
        {
            sb.Append(value);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        public void AppendLine(string value)
        {
            sb.AppendLine(value);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="format"></param>
        /// <param name="args"></param>
        public void AppendFormat(string format, params object[] args)
        {
            sb.AppendFormat(format, args);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return sb.ToString();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="relatedTypes"></param>
        public void RenderAnchor(Type type, List<Type> relatedTypes)
        {
            if (relatedTypes.Contains(type))
            {
                sb.AppendFormat("<a href='#{0}'>{1}</a>", type.FullName, type.Name);
            }
            else
            {
                sb.Append(type.Name);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="desc"></param>
        public void RenderClass(Type type, string desc = "")
        {
            sb.Append("<div class='panel panel-default'>");
            sb.AppendFormat("<a name='{0}'></a>", type.FullName);
            sb.Append("<pre>");
            if (string.IsNullOrEmpty(desc) == false)
            {
                sb.AppendLine("/// &lt;summary&gt;");
                sb.AppendFormat("/// <span class='code-comment'>{0}</span>\r\n", desc);
                sb.AppendLine("/// &lt;/summary&gt;");
            }
            sb.AppendLine("public class " + type.Name);
            sb.AppendLine("{");
            foreach(var prop in type.GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.GetProperty))
            {
                string propDesc = docProvider.GetPropertyDocumentation(type, prop.Name);
                if (string.IsNullOrEmpty(propDesc) == false)
                {
                    sb.AppendLine("\t/// &lt;summary&gt;");
                    sb.AppendFormat("\t/// <span class='code-comment'>{0}</span>\r\n", propDesc);
                    sb.AppendLine("\t/// &lt;/summary&gt;");
                }
                sb.AppendLine("\tpublic " + prop.PropertyType.Name + " " + prop.Name +" { get; set; }");
            }
            sb.Append("}");
            sb.Append("</pre>");
            sb.Append("</div>");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="desc"></param>
        public void RenderEnum(Type type, string desc = "")
        {
            sb.Append("<div class='panel panel-default'>");
            sb.AppendFormat("<a name='{0}'></a>", type.FullName);
            sb.Append("<pre>");
            if (string.IsNullOrEmpty(desc) == false)
            {
                sb.AppendLine("/// &lt;summary&gt;");
                sb.AppendFormat("/// <span class='code-comment'>{0}</span>\r\n", desc);
                sb.AppendLine("/// &lt;/summary&gt;");
            }
            sb.AppendLine("public enum " + type.Name);
            sb.AppendLine("{");



            List<EnumPair> pairs = new List<EnumPair>();
            
            string[] enumNames = type.GetEnumNames();
            Array enumValues = type.GetEnumValues();
            for (int i = 0; i < enumNames.Length; i++)
            {
                pairs.Add(new EnumPair {Name = enumNames[i], Value = (int)enumValues.GetValue(i)});
            }
            pairs = pairs.OrderByDescending(t => t.Value).ToList();

            foreach (EnumPair pair in pairs)
            {
                string propDesc = docProvider.GetEnumNameDocumentation(type, pair.Name);
                if (string.IsNullOrEmpty(propDesc) == false)
                {
                    sb.AppendLine("\t/// &lt;summary&gt;");
                    sb.AppendFormat("\t/// <span class='code-comment'>{0}</span>\r\n", propDesc);
                    sb.AppendLine("\t/// &lt;/summary&gt;");
                }
                sb.AppendLine("\t" + pair.Name + " = "  +  pair.Value  +  ",");
            }
            sb.AppendLine("}");
            sb.Append("</pre>");
            sb.Append("</div>");
        }
        /// <summary>
        /// 
        /// </summary>
        public class EnumPair
        {
            /// <summary>
            /// 
            /// </summary>
            public string Name { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public int Value { get; set; }
        }
    }
}

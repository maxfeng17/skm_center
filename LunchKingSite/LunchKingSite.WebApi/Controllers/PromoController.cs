﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Security;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebApi.Core.OAuth;

namespace LunchKingSite.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class PromoController : BaseController
    {
        private static readonly ICmsProvider Cms = ProviderFactory.Instance().GetProvider<ICmsProvider>();
        private static readonly ConcurrentDictionary<string, ParagraphModel> ParagraphCache = new ConcurrentDictionary<string, ParagraphModel>();

        /// <summary>
        /// 取得Paragraph快取內容
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="contentName"></param>
        /// <returns></returns>
        [HttpGet]
        public ApiResult GetParagraph( string userName, string contentName)
        {
            ApiResult result;
            try
            {
                result = new ApiResult
                {
                    Data = GetParagraphModel(userName, contentName),
                    Code = ApiResultCode.Success
                };
            }
            catch(Exception ex)
            {
                result = new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = ex.Message
                };
            }
            return result;
        }

        /// <summary>
        /// 移除快取內容
        /// </summary>
        /// <param name="contentName"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        [OAuthScope(TokenScope.SystemManagement)]
        [HttpGet]
        public ApiResult RemoveParagraphCache(string userName, string contentName)
        {
            ParagraphModel removeItem;
            ParagraphCache.TryRemove(contentName, out removeItem);
            return new ApiResult
            {
                Code = ApiResultCode.Success
            };
        }

        #region Private Medhod

        private static ParagraphModel GetParagraphModel(string userName, string contentName)
        {
            var data = ParagraphCache.GetOrAdd(contentName, delegate
            {
                ParagraphModel model = new ParagraphModel();
                CmsContent article = Cms.CmsContentGet(contentName);
                if (!article.IsLoaded)
                {
                    if (CheckUserIsEditor(userName))
                    {
                        article = new CmsContent
                        {
                            ContentName = contentName,
                            CreatedBy = userName,
                            CreatedOn = DateTime.Now,
                            Locale = "zh_TW"
                        };
                        Cms.CmsContentSet(article);
                        model.ContentBody = string.Empty;
                    }
                }
                else
                {
                    model.ContentBody = article.Body;    
                }

                var seo = PromotionFacade.GetPromoSeoKeyword((int)PromoSEOType.PromoActivit, article.ContentId);
                if (!seo.IsLoaded) return model;
                model.SeoDescription = seo.Description;
                model.SeoKeyword = seo.Keyword;
                return model;
            });
            return data;
        }

        private static bool CheckUserIsEditor(string userName)
        {
            if (string.IsNullOrEmpty(userName)) return false;
            var roles = Roles.GetRolesForUser(userName);
            if (roles == null) return false;

            var passRoles = new List<MemberRoles>
            {
                MemberRoles.Administrator,
                MemberRoles.Planning,
                MemberRoles.MemberAdmin,
                MemberRoles.Editor
            };

            return roles.Intersect(passRoles.Select(x => x.ToString())).Any();
        }

        #endregion Private Medhod

        #region Model Inner Class
        /// <summary>
        /// 
        /// </summary>
        public class ParagraphModel
        {
            /// <summary>
            /// 
            /// </summary>
            public string ContentBody { set; get; }
            /// <summary>
            /// 
            /// </summary>
            public string SeoDescription { set; get; }
            /// <summary>
            /// 
            /// </summary>
            public string SeoKeyword { set; get; }
        }

        #endregion Model Inner Class
    }
}

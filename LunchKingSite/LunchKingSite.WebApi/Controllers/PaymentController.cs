﻿using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.BizLogic.Model.BookingSystem;
using LunchKingSite.BizLogic.Model.OAuth;
using LunchKingSite.BizLogic.Model.Refund;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Transactions;
using System.Web;
using System.Web.Http;
using System.Web.SessionState;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.WebApi.Core.OAuth;
using LunchKingSite.WebApi.Models.Channel;
using Helper = LunchKingSite.Core.Helper;
using System.Text;
using System.Net.Http.Headers;
using System.Net;

namespace LunchKingSite.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class PaymentController : BaseController
    {
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private static IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        private static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private static ISellerProvider sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        private static IItemProvider ip = ProviderFactory.Instance().GetProvider<IItemProvider>();
        private static ILocationProvider lp = ProviderFactory.Instance().GetProvider<ILocationProvider>();
        private static IBookingSystemProvider bp = ProviderFactory.Instance().GetProvider<IBookingSystemProvider>();
        private static ILog logger = LogManager.GetLogger("PaymentController");
        private static LunchKingSite.Core.JsonSerializer jsonSerializer = new LunchKingSite.Core.JsonSerializer();

        #region model

        /// <summary>
        /// 
        /// </summary>
        public class LionTravelOrderCheckModel
        {
            /// <summary>
            /// 
            /// </summary>
            public string RelatedOrderId { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string SmsMobile { get; set; }
        }
        /// <summary>
        /// 
        /// </summary>
        public class RefundLionTravelOrderModel
        {
            /// <summary>
            /// 
            /// </summary>
            public Guid OrderGuid { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string RelatedOrderId { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public List<int> CouponId { get; set; }
        }
        /// <summary>
        /// 
        /// </summary>
        public class CouponStatusModel
        {
            /// <summary>
            /// 
            /// </summary>
            public Guid OrderGuid { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public int Id { get; set; }
        }
        /// <summary>
        /// 
        /// </summary>
        public class CouponSmsModel
        {
            /// <summary>
            /// 
            /// </summary>
            public Guid OrderGuid { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public int Id { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string SmsMobile { get; set; }
        }

        public class SendAuthCodeSmsModel
        {
            public Guid TaskId { get; set; }
            public string SmsMobile { get; set; }
            public long Ticks { get; set; }
        }
        #endregion

        #region LionTravel



        /// <summary>
        /// 
        /// </summary>
        /// <param name="ob"></param>
        /// <param name="accesstoken"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.LionTravel)]
        public HttpResponseMessage MakeLionTravelOrder(JObject ob, string accesstoken)
        {
            return MakeOrder(ob, accesstoken);
        }

        private HttpResponseMessage MakeOrder(JObject ob, string accesstoken)
        {
            AuditLog(accesstoken, ob.ToString(), "MakeLionTravelOrder");
            int source_id = 0;
            OauthToken oauth_token = CommonFacade.OauthTokenGet(accesstoken);
            OAuthClientModel client = OAuthFacade.GetClient(oauth_token.AppId);
            var channel = OAuthFacade.GetChannelClientProperty(client.AppId);
            AgentChannel orderCorrespondingType = AgentChannel.NONE;

            if (client != null)
            {
                source_id = client.Id;
                orderCorrespondingType = ChannelFacade.GetOrderClassificationByName(client.Name);
            }

            ChannelMakeOrderModel channelOrder;
            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };

            if (orderCorrespondingType == AgentChannel.NONE)
            {
                rtnObject.Code = ApiReturnCode.InputError;
                rtnObject.Message = "傳入參數格式不符";
                return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
            }

            try
            {
                channelOrder = ob.ToObject<ChannelMakeOrderModel>();
            }
            catch
            {
                rtnObject.Code = ApiReturnCode.InputError;
                rtnObject.Message = "傳入參數格式不符";
                return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
            }

            //固定使用者名稱
            string username = config.TmallDefaultUserName;
            int userId = MemberFacade.GetUniqueId(username);

            //使用Token建立對應零級會員
            if (channel.UsingTokenToGuestId)
            {
                if (string.IsNullOrEmpty(channelOrder.MemberToken) || client.Id == 0)
                {
                    rtnObject.Code = ApiReturnCode.InputError;
                    rtnObject.Message = "傳入參數格式不符(member token is null)";
                    return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
                }

                //get or add entrepot_member
                var entrepotMember = EntrepotFacade.MemberGetByToken(channelOrder.MemberToken, client.Id);
                if (entrepotMember == null || entrepotMember.Id <= 0)
                {
                    rtnObject.Code = ApiReturnCode.Error;
                    rtnObject.Message = "虛擬帳號建立失敗";
                    return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
                }

                username = MemberFacade.GetUserName(entrepotMember.UserId);
                userId = entrepotMember.UserId;
            }

            IViewPponDeal theDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(channelOrder.DealGuid);

            #region Check Order

            if (!theDeal.IsLoaded)
            {
                rtnObject.Code = ApiReturnCode.InputError;
                rtnObject.Message = "查無檔次";
                return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
            }

            if ((theDeal.BusinessHourStatus & (int)BusinessHourStatus.ComboDealMain) > 0)
            {
                rtnObject.Code = ApiReturnCode.InputError;
                rtnObject.Message = "無法購買多檔次代表檔";
                return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
            }

            if (channelOrder.DealGuid == Guid.Empty || (!channelOrder.IsDelivery && channelOrder.StoreGuid == Guid.Empty) || channelOrder.Quantity <= 0 || string.IsNullOrEmpty(channelOrder.RelatedOrderId) || (!channelOrder.IsDelivery && string.IsNullOrEmpty(channelOrder.SmsMobile)) || (channelOrder.IsDelivery && channelOrder.DeliveryInfo == null))
            {
                rtnObject.Code = ApiReturnCode.InputError;
                rtnObject.Message = "傳入參數格式不符";
                return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
            }

            //檢查使用者
            if (userId == 0)
            {
                rtnObject.Code = ApiReturnCode.ApiUserIdError;
                rtnObject.Message = string.Format("{0}，查無會員", username);
                return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
            }

            if (orderCorrespondingType != AgentChannel.PChome && theDeal.ItemPrice == 0)
            {
                rtnObject.Code = ApiReturnCode.DealTypeError;
                rtnObject.Message = "無法購買零元商品";
                return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
            }

            if (theDeal.DeliveryType == (int)DeliveryType.ToShop)
            {
                //通用券檔次不檢查分店
                if (!Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.NoRestrictedStore))
                {
                    //非商品檔，分店編號錯誤回傳false
                    if (Guid.Empty == channelOrder.StoreGuid)
                    {
                        rtnObject.Code = ApiReturnCode.PaymentError;
                        rtnObject.Message = "請輸入正確的StoreGuid";
                        return new HttpResponseMessage()
                        {
                            Content =
                                new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(),
                                    "application/json")
                        };
                    }
                    //有分店編號，檢查是否還有數量 and vbsright =VerifyShop
                    PponStore pponStore = pp.PponStoreGet(channelOrder.DealGuid, channelOrder.StoreGuid, VbsRightFlag.VerifyShop);
                    //查無分店設定，回傳false
                    if (!pponStore.IsLoaded)
                    {
                        rtnObject.Code = ApiReturnCode.PaymentError;
                        rtnObject.Message = "此檔次無此分店";
                        return new HttpResponseMessage()
                        {
                            Content =
                                new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(),
                                    "application/json")
                        };
                    }

                    //檢查分店編號是否正確 參數false:非宅配商品
                    if (!CheckBranch(pponStore, channelOrder.Quantity, theDeal.SaleMultipleBase))
                    {
                        rtnObject.Code = ApiReturnCode.PaymentError;
                        rtnObject.Message = "請選擇還有數量的分店";
                        return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
                    }
                }
                else
                {
                    if (theDeal.SellerGuid != channelOrder.StoreGuid)
                    {
                        rtnObject.Code = ApiReturnCode.PaymentError;
                        rtnObject.Message = "請輸入正確的StoreGuid";
                        return new HttpResponseMessage()
                        {
                            Content =
                                new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(),
                                    "application/json")
                        };
                    }
                    if (theDeal.OrderedQuantity >= theDeal.OrderTotalLimit)
                    {
                        rtnObject.Code = ApiReturnCode.PaymentError;
                        rtnObject.Message = "請選擇還有數量的分店";
                        return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
                    }
                    channelOrder.StoreGuid = Guid.Empty;
                }
            }

            if (!Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.GroupCoupon) 
                && orderCorrespondingType != AgentChannel.PayEasyOrder && orderCorrespondingType != AgentChannel.PChome 
                && Helper.IsFlagSet(theDeal.GroupOrderStatus ?? 0, GroupOrderStatus.PEZevent))
            {
                rtnObject.Code = ApiReturnCode.InputError;
                rtnObject.Message = "檔次不存在無法購買";
                return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
            }

            if (!Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.GroupCoupon) 
                && Helper.IsFlagSet(theDeal.GroupOrderStatus ?? 0, GroupOrderStatus.FamiDeal))
            {
                rtnObject.Code = ApiReturnCode.InputError;
                rtnObject.Message = "檔次不存在無法購買";
                return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
            }

            //API串接不賣成套禮券
            if (Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.GroupCoupon)
                && channel.IsEnableGroupCoupon && ChannelFacade.IsEnableChannelGroupCoupon(theDeal, channel))
            {
                if (theDeal.SaleMultipleBase != channelOrder.Quantity)
                {
                    rtnObject.Code = ApiReturnCode.InputError;
                    rtnObject.Message = "成套商品購買數量不正確";
                    return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
                }
            }
            else
            {
                if (Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.GroupCoupon))
                {
                    rtnObject.Code = ApiReturnCode.InputError;
                    rtnObject.Message = "無法購買成套商品";
                    return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
                }
            }

            //check app 限定檔
            if (PponDealPreviewManager.GetDealCategoryIdList(theDeal.BusinessHourGuid)
            .IndexOf(CategoryManager.Default.AppLimitedEdition.CategoryId) > -1)
            {
                rtnObject.Code = ApiReturnCode.InputError;
                rtnObject.Message = "無法購買APP限定檔次";
                return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
            }

            //check 檔次毛利
            //若有在白名單則不需檢查毛利
            if (!ChannelFacade.IsChannleForcePass(orderCorrespondingType, theDeal))
            {
                BaseCostGrossMargin cost = ViewPponDealManager.DefaultManager.BaseCostGrossMarginGet(theDeal.BusinessHourGuid);
                if ((channel.MinGrossMargin ?? 0) == 0)
                {
                    if (Math.Round(cost.BaseGrossMargin, 4) < 0.1m)
                    {
                        rtnObject.Code = ApiReturnCode.InputError;
                        rtnObject.Message = "檔次不存在無法購買";
                        return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
                    }
                }
                else if (Math.Round(cost.MinGrossMargin, 4) <= channel.MinGrossMargin)
                {
                    rtnObject.Code = ApiReturnCode.InputError;
                    rtnObject.Message = "檔次不存在無法購買";
                    return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
                }
            }
            

            //排除憑證消費多重選項
            if (theDeal.DeliveryType.HasValue && int.Equals((int)DeliveryType.ToShop, theDeal.DeliveryType.Value) && theDeal.ShoppingCart == true)
            {
                rtnObject.Code = ApiReturnCode.InputError;
                rtnObject.Message = "無法購買此類型商品";
                return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
            }

            var deliveryCharge = 0;

            //宅配 Check
            if (channelOrder.IsDelivery)
            {
                //檢查DeliveryInfo郵遞資訊
                if (string.IsNullOrWhiteSpace(channelOrder.DeliveryInfo.WriteAddress) ||    //收件地址(NVarchar)
                    string.IsNullOrWhiteSpace(channelOrder.DeliveryInfo.WriteZipCode) ||    //收件郵遞區號(NVarchar)
                    string.IsNullOrWhiteSpace(channelOrder.DeliveryInfo.WriteName) ||       //收件人姓名(NVarchar)
                    string.IsNullOrWhiteSpace(channelOrder.DeliveryInfo.Phone))             //收件人聯絡電話(NVarchar)
                {
                    rtnObject.Code = ApiReturnCode.InputError;
                    rtnObject.Message = "請檢查宅配寄送資訊是否不正確";
                    return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
                }

                //此訂單的購買選項
                List<Guid> optionGuidCol = (from itemOption in channelOrder.ItemOption from itemSpec in itemOption.ItemSpec select itemSpec.OptionGuid).ToList();
                bool isHouseNewVersion = false;//判斷是否為新版提案單
                AccessoryGroupCollection viagc = new AccessoryGroupCollection();
                List<Guid> allOptionGuid = new List<Guid>();
                if (ProposalFacade.IsVbsProposalNewVersion() && theDeal.IsHouseDealNewVersion())
                {
                    isHouseNewVersion = true;
                }
                if (isHouseNewVersion)
                {
                    //新版宅配提案單
                    viagc = PponFacade.AccessoryGroupGetByHouseDeal(theDeal);
                    allOptionGuid.AddRange(viagc.SelectMany(x => x.members).Select(y => y.AccessoryGroupMemberGuid));
                }
                else
                {
                    //舊有檔次
                    viagc = ip.AccessoryGroupGetListByItem(theDeal.ItemGuid);
                    //此檔次的所有選項
                    allOptionGuid = (from ag in viagc from ig in ip.ViewItemAccessoryGroupGetList(theDeal.ItemGuid, ag.Guid) select ig.AccessoryGroupMemberGuid).ToList();
                }
                if (optionGuidCol.Any(optionGuid => !allOptionGuid.Contains(optionGuid)))
                {
                    rtnObject.Code = ApiReturnCode.InputError;
                    rtnObject.Message = "錯誤的商品選項";
                    return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
                }

                if (channelOrder.Amount == 0)
                {
                    rtnObject.Code = ApiReturnCode.InputError;
                    rtnObject.Message = "宅配需傳入訂單金額";
                    return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
                }

                var comboPackCount = theDeal.ComboPackCount ?? 1;
                if (comboPackCount > channelOrder.Quantity)
                {
                    rtnObject.Code = ApiReturnCode.InputError;
                    rtnObject.Message = "商品數量與條件不符";
                    return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
                }

                var maxOrderCount = theDeal.MaxItemCount * comboPackCount;
                if (channelOrder.Quantity > maxOrderCount)
                {
                    rtnObject.Code = ApiReturnCode.InputError;
                    rtnObject.Message = "超過單次可購買數量上限";
                    return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
                }

                //check數量倍數
                if (channelOrder.Quantity % comboPackCount > 0)
                {
                    rtnObject.Code = ApiReturnCode.InputError;
                    rtnObject.Message = "成套商品數量倍數不符";
                    return new HttpResponseMessage()
                    {
                        Content =
                            new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(),
                                "application/json")
                    };
                }

                if (theDeal.ShoppingCart.HasValue && !theDeal.ShoppingCart.Value && channelOrder.ItemOption.Count() > 1)
                {
                    rtnObject.Code = ApiReturnCode.InputError;
                    rtnObject.Message = "此檔次非購物車檔次";
                    return new HttpResponseMessage()
                    {
                        Content =
                            new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(),
                                "application/json")
                    };
                }

                deliveryCharge = SetDeliveryCharge((channelOrder.Amount - channelOrder.DeliveryInfo.ShippingFee), theDeal);
                //check運費
                if (deliveryCharge != channelOrder.DeliveryInfo.ShippingFee)
                {
                    rtnObject.Code = ApiReturnCode.InputError;
                    rtnObject.Message = "運費輸入錯誤";
                    return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
                }
            }

            #endregion Check Order

            #region CheckOut
            SessionIDManager session_manager = new SessionIDManager();
            string sessionid = session_manager.CreateSessionID(HttpContext.Current);
            string ticketid = OrderFacade.MakeRegularTicketId(sessionid, channelOrder.DealGuid) + "_" + Path.GetRandomFileName().Replace(".", "");
            PaymentAPIProvider APIProvider = (PaymentAPIProvider)Enum.Parse(typeof(PaymentAPIProvider), config.CreditCardAPIProvider);
            //檢查數量和檔次是否結束或售罄
            string strResult = CheckLionTravelData(theDeal, (int)(Math.Ceiling((double)(channelOrder.Quantity) / (theDeal.ComboPackCount ?? 1))));
            if (string.IsNullOrWhiteSpace(strResult))
            {
                string infoStr = channelOrder.IsDelivery ? jsonSerializer.Serialize(channelOrder.GetPponDeliveryInfo()) : string.Empty;
                string spec = channelOrder.IsDelivery
                    ? channelOrder.GetItemOptionString()
                    : string.Format("|#|{0}", channelOrder.Quantity);

                List<ItemEntry> ies = PaymentFacade.ProcessSubItemsAndAccessoriesWithComboPack(spec, ticketid, theDeal, channelOrder.StoreGuid, infoStr);

                try
                {
                    OrderFacade.PutItemToCart(ticketid, ies);

                    #region 把資料放到暫存Session中

                    TempSession ts = new TempSession();
                    ts.SessionId = ticketid;
                    ts.Name = "pponContent";
                    ts.ValueX = string.Empty;
                    pp.TempSessionSetForUpdate(ts);

                    #endregion 把資料放到暫存Session中
                }
                catch (Exception ex)
                {
                    rtnObject.Code = ApiReturnCode.PaymentError;
                    rtnObject.Message = "交易失敗！";
                    logger.Error("Something wrong while put items to cart : " + ex.Message);
                    return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
                }
            }
            else
            {
                rtnObject.Code = ApiReturnCode.PaymentError;
                rtnObject.Message = strResult;
                return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
            }

            #endregion CheckOut

            TempSessionCollection tsc = pp.TempSessionGetList(ticketid);

            if (tsc.Count <= 0)
            {
                rtnObject.Code = ApiReturnCode.PaymentError;
                rtnObject.Message = "交易失敗！";
                logger.Error("Temp Session Missing.");
                return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
            }

            string transactionid = PaymentFacade.GetTransactionId();

            #region order amount
            //訂單總金額
            ShoppingCartCollection carts = new ShoppingCartCollection();
            int amount = 0;
            if (theDeal.ShoppingCart.HasValue && theDeal.ShoppingCart.Value)
            {
                ShoppingCartCollection scc = op.ShoppingCartGetList(ShoppingCart.Columns.TicketId, ticketid);
                foreach (ShoppingCart sc in scc)
                {
                    carts.Add(sc);
                    int x = (int)Math.Round(sc.ItemUnitPrice * sc.ItemQuantity);
                    amount += x;
                }
            }
            else
            {
                //前端Deal以憑證宅配撈取，所以結帳時不以館別判斷
                carts.Add(op.ShoppingCartGet(ShoppingCart.Columns.TicketId, ticketid));
                amount = (int)Math.Round(carts[0].ItemUnitPrice * carts[0].ItemQuantity);
            }

            //宅配加運費
            if (channelOrder.IsDelivery)
            {
                amount = amount + (int)channelOrder.DeliveryInfo.ShippingFee;
            }

            if ((channelOrder.IsDelivery && amount != channelOrder.Amount) || (channelOrder.IsConfirmAmount && amount != channelOrder.Amount))
            {
                rtnObject.Code = ApiReturnCode.PaymentError;
                rtnObject.Message = "交易失敗，金額不符！";
                logger.Error(string.Format("Amount not match:RelatedOrderId={0},DealGuid={1},InputAmount={2},amount={3} ", channelOrder.RelatedOrderId, channelOrder.DealGuid, channelOrder.Amount, amount));
                return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
            }

            #endregion order amount

            #region New Version Order Start

            DeliveryInfo di = new DeliveryInfo();
            di.DeliveryCharge = deliveryCharge;
            OrderFromType orderfromtype = OrderFromType.ByWebService;
            di.DeliveryAddress = channelOrder.IsDelivery ? (channelOrder.DeliveryInfo.WriteZipCode + " " + channelOrder.DeliveryInfo.WriteAddress) : "LionTravelOrder";
            if (channelOrder.IsDelivery)
            {
                di.CustomerName = channelOrder.DeliveryInfo.WriteName;
                di.CustomerMobile = channelOrder.DeliveryInfo.Phone;
            }

            // make order
            Guid orderguid = OrderFacade.MakeOrder(username, ticketid, DateTime.Now, ob.ToString(), di,
                theDeal, orderfromtype);

            //save wms (pchome)
            if (theDeal.IsWms)
            {
                PponBuyFacade.SaveWmsOrder(orderguid);
            }
            #endregion New Version Order Start

            //代銷全額用LCash付款
            OrderFacade.MakeTransaction(transactionid, LunchKingSite.Core.PaymentType.LCash, amount, (DepartmentTypes)theDeal.Department,
                username, orderguid, string.Empty);


            bool orderOK = false;
            using (TransactionScope transScope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted }))
            {
                #region 扣除選項
                pp.TempSessionDelete(ticketid);
                tsc = pp.TempSessionGetList("Accessory" + ticketid);
                if (tsc.Count > 0)
                {
                    //多重選項
                    foreach (TempSession ts in tsc)
                    {
                        if (ProposalFacade.IsVbsProposalNewVersion() && theDeal.IsHouseDealNewVersion())
                        {
                            Guid option_guid = Guid.Empty;
                            Guid.TryParse(ts.Name, out option_guid);
                            PponOption opt1 = pp.PponOptionGetByGuid(option_guid);
                            int forOut = 0;
                            int.TryParse(ts.ValueX, out forOut);
                            if (opt1 != null && opt1.IsLoaded)
                            {
                                if (opt1.ItemGuid != null)
                                {
                                    int quantity = 0;
                                    int.TryParse(ts.ValueX, out quantity);
                                    ProductItem pdi = pp.ProductItemGet(opt1.ItemGuid.Value);
                                    if (pdi != null && pdi.IsLoaded)
                                    {
                                        int stock = pdi.Stock - forOut;
                                        if (stock >= 0)
                                        {
                                            pdi.Stock = pdi.Stock - forOut;
                                            pdi.Sales = pdi.Sales + forOut;
                                            pp.ProductItemSet(pdi);

                                            PponFacade.UpdateDealMaxItemCount(theDeal, opt1.ItemGuid.Value); //將相關檔次的最大購買數量改為0，避免前台繼續銷售
                                        }
                                        else
                                        {
                                            PponFacade.UpdateDealMaxItemCount(theDeal, opt1.ItemGuid.Value); //將相關檔次的最大購買數量改為0，避免前台繼續銷售
                                            rtnObject.Code = ApiReturnCode.PaymentError;
                                            rtnObject.Message = string.Format("交易失敗，{0} 庫存不足！{1}{2}", pdi.SpecName, theDeal.BusinessHourGuid, theDeal.ItemName);
                                            logger.Error(string.Format("Stock not enough:RelatedOrderId={0},DealGuid={1},InputAmount={2},amount={3} ", channelOrder.RelatedOrderId, channelOrder.DealGuid, channelOrder.Amount, amount));
                                            return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            int forOut = 0;
                            AccessoryGroupMember agm = ip.AccessoryGroupMemberGet(new Guid(ts.Name));
                            if (!agm.IsLoaded)
                                continue;

                            if (agm.Quantity != null && int.TryParse(ts.ValueX, out forOut))
                            {
                                agm.Quantity = agm.Quantity - int.Parse(ts.ValueX);

                                int? optId = PponOption.FindId(agm.Guid, theDeal.BusinessHourGuid);
                                if (optId.HasValue)
                                {
                                    PponOption opt = pp.PponOptionGet(optId.Value);
                                    if (opt != null && opt.IsLoaded)
                                    {
                                        opt.Quantity = agm.Quantity;
                                        pp.PponOptionSet(opt);
                                    }
                                }
                            }

                            ip.AccessoryGroupMemberSet(agm);
                        }
                    }
                }
                else
                {
                    if (ProposalFacade.IsVbsProposalNewVersion() && theDeal.IsHouseDealNewVersion())
                    {
                        int quantity = channelOrder.Quantity;
                        ProposalMultiDeal pmd = sp.ProposalMultiDealGetByBid(theDeal.BusinessHourGuid);
                        if (pmd.IsLoaded)
                        {
                            ProposalMultiDealsSpec spec = new LunchKingSite.Core.JsonSerializer().Deserialize<List<ProposalMultiDealsSpec>>(pmd.Options).FirstOrDefault();
                            if (spec != null)
                            {
                                ProductItem pdi = pp.ProductItemGet(spec.Items.FirstOrDefault().item_guid);
                                if (pdi != null && pdi.IsLoaded)
                                {
                                    int stock = pdi.Stock - (quantity * theDeal.QuantityMultiplier.GetValueOrDefault(1));
                                    if (stock >= 0)
                                    {
                                        pdi.Stock = pdi.Stock - (quantity * theDeal.QuantityMultiplier.GetValueOrDefault(1));
                                        pdi.Sales = pdi.Sales + (quantity * theDeal.QuantityMultiplier.GetValueOrDefault(1));
                                        pp.ProductItemSet(pdi);

                                        PponFacade.UpdateDealMaxItemCount(theDeal, spec.Items.FirstOrDefault().item_guid); //將相關檔次的最大購買數量改為0，避免前台繼續銷售
                                    }
                                    else
                                    {
                                        try
                                        {
                                            PponFacade.UpdateDealMaxItemCount(theDeal, spec.Items.FirstOrDefault().item_guid); //將相關檔次的最大購買數量改為0，避免前台繼續銷售
                                        }
                                        catch { }
                                        rtnObject.Code = ApiReturnCode.PaymentError;
                                        rtnObject.Message = string.Format("交易失敗，{0} 庫存不足！{1}{2}", pdi.SpecName, theDeal.BusinessHourGuid, theDeal.ItemName);
                                        logger.Error(string.Format("Stock not enough:RelatedOrderId={0},DealGuid={1},InputAmount={2},amount={3} ", channelOrder.RelatedOrderId, channelOrder.DealGuid, channelOrder.Amount, amount));
                                        return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
                                    }
                                }
                            }
                        }
                    }
                }

                pp.TempSessionDelete("Accessory" + ticketid);

                #endregion 扣除選項

                #region 分店銷售數量

                if (channelOrder.StoreGuid != Guid.Empty)
                {
                    int k = 0;
                    if (theDeal.ShoppingCart == true)
                    {
                        k = (int)(Math.Ceiling((double)channelOrder.Quantity / (theDeal.ComboPackCount ?? 1)));
                    }
                    else
                    {
                        k = channelOrder.Quantity;
                    }
                    k = (theDeal.SaleMultipleBase ?? 0) > 0 ? theDeal.SaleMultipleBase.Value : k;
                    pp.PponStoreUpdateOrderQuantity(theDeal.BusinessHourGuid, channelOrder.StoreGuid, k);
                }
                #endregion 分店銷售數量

                transScope.Complete();
                orderOK = true;
            }
            if (!orderOK)
            {
                CreditCardUtility.AuthenticationReverse(transactionid);
                rtnObject.Code = ApiReturnCode.PaymentError;
                rtnObject.Message = "交易失敗！";
                logger.Error("Order update failed.");
                return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
            }
            else
            {
                int trust_checkout_type = 0;
                if (((theDeal.BusinessHourStatus) & (int)BusinessHourStatus.WeeklyPay) > 0)
                {
                    trust_checkout_type = (int)TrustCheckOutType.WeeklyPay;
                }
                int quantity = (int)(Math.Ceiling((double)(channelOrder.Quantity) / (theDeal.ComboPackCount ?? 1)));
                OrderDetailCollection odCol = op.OrderDetailGetList(1, theDeal.MaxItemCount ?? 10, string.Empty, orderguid, OrderDetailTypes.Regular);
                Dictionary<LunchKingSite.Core.PaymentType, int> payments = new Dictionary<LunchKingSite.Core.PaymentType, int>();
                payments.Add(LunchKingSite.Core.PaymentType.LCash, amount);
                //判斷成套數量
                int saleMultipleBase = Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.GroupCoupon) ? theDeal.SaleMultipleBase ?? 0 : 0;
                CashTrustInfo cti = new CashTrustInfo
                {
                    OrderGuid = orderguid,
                    OrderDetails = odCol,
                    CreditCardAmount = 0,//如果用刷卡 此值設定為amount
                    SCashAmount = 0,
                    PCashAmount = 0,
                    BCashAmount = 0,
                    Quantity = saleMultipleBase > 0 ? saleMultipleBase : quantity,
                    DeliveryCharge = deliveryCharge,
                    DeliveryType = theDeal.DeliveryType == null ? DeliveryType.ToShop : (DeliveryType)theDeal.DeliveryType.Value,
                    DiscountAmount = 0,
                    AtmAmount = 0,
                    BusinessHourGuid = theDeal.BusinessHourGuid,
                    CheckoutType = trust_checkout_type,
                    TrustProvider = Helper.GetBusinessHourTrustProvider(theDeal.BusinessHourStatus),
                    ItemName = theDeal.ItemName,
                    ItemPrice = (int)theDeal.ItemPrice,
                    ItemOriPrice = (int)theDeal.ItemOrigPrice,
                    User = mp.MemberGet(username),
                    Payments = payments,
                    LCashAmount = amount,
                    PresentQuantity = theDeal.PresentQuantity ?? 0,
                    IsGroupCoupon = Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.GroupCoupon),
                    GroupCouponType = (GroupCouponDealType)(theDeal.GroupCouponDealType ?? 0)
                };

                CashTrustLogCollection ctCol = OrderFacade.MakeCashTrust(cti);

                Order o = op.OrderGet(orderguid);
                try
                {
                    var phoneNum = channelOrder.IsDelivery ? channelOrder.DeliveryInfo.Phone : channelOrder.SmsMobile ?? string.Empty;
                    OrderCorresponding oc = new OrderCorresponding()
                    {
                        CreatedTime = DateTime.Now,
                        UserId = userId,
                        Mobile = phoneNum,
                        OrderGuid = o.Guid,
                        RelatedOrderId = channelOrder.RelatedOrderId,
                        OrderId = o.OrderId,
                        BusinessHourGuid = theDeal.BusinessHourGuid,
                        UniqueId = theDeal.UniqueId.Value,
                        Memo = channelOrder.IsDelivery ? "宅配" : string.Empty,
                        Type = (int)orderCorrespondingType,
                        TokenClientId = source_id,
                        OrderCheck = (channelOrder.IsDelivery && orderCorrespondingType == AgentChannel.PayEasyOrder) ? (int?)ChannelOrderCheck.Init : null
                    };

                    op.OrderCorrespondingSet(oc);


                    //簡訊另外處理，宅配不發簡訊
                    List<Coupon> coupons = CouponFacade.GenCouponWithoutSms(o, theDeal);

                    //萊爾富咖啡寄杯
                    if (config.EnableHiLifeDealSetup && (theDeal.GroupOrderStatus & (int)GroupOrderStatus.HiLifeDeal) > 0 &&
                        Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.GroupCoupon) &&
                        (theDeal.GroupOrderStatus & (int)GroupOrderStatus.PEZevent) > 0)
                    {
                        if (!HiLifeFacade.MakeHiLifePincode(o, saleMultipleBase, userId))
                        {
                            //throw new System.AggregateException("HiLife Insert Pincode Fail.");
                        }
                    }

                    if (!channelOrder.IsDelivery && channelOrder.IsSendSms)
                    {
                        coupons.ForEach(c =>
                        {
                            SendSmsProcess(theDeal.BusinessHourGuid, theDeal.BusinessHourDeliverTimeS.Value,
                                theDeal.ChangedExpireDate ?? theDeal.BusinessHourDeliverTimeE.Value, c.Id,
                                c.SequenceNumber, c.Code,
                                c.StoreSequence.HasValue
                                    ? ("，序號" + c.StoreSequence.Value.ToString("0000"))
                                    : string.Empty,
                                channelOrder.SmsMobile, c.OrderDetailId, string.Empty, config.TmallDefaultUserName, true);
                        });
                    }

                    OrderFacade.SetOrderStatus(orderguid, username, OrderStatus.Complete, true,
                                              new SalesInfoArg
                                              {
                                                  BusinessHourGuid = theDeal.BusinessHourGuid,
                                                  Quantity = odCol.OrderedQuantity,
                                                  Total = odCol.OrderedTotal
                                              });

                    #region 建立pchome訂單
                    if (PponBuyFacade.AddPchomeOrder(theDeal, o.Guid, o) == false)
                    {
                        OrderFacade.DeleteItemInCart(ticketid);
                        PponBuyFacade.DeletePponDeliveryInfoAtSession(ticketid);

                        PaymentFacade.CancelPaymentTransactionByOrderForIncompleteOrder(o, "sys"); //自動退貨


                        rtnObject.Code = ApiReturnCode.PaymentError;
                        rtnObject.Message = "交易失敗！";
                        logger.Error("Create Pchome Order failed.");
                        return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
                    }
                    #endregion 建立pchome訂單


                    ApiMakeOrderReturnObject rtnApiMakeOrderReturnObject = new ApiMakeOrderReturnObject
                    {
                        OrderGuid = orderguid.ToString(),
                        OrderId = o.OrderId,
                        Coupon = coupons.Select(x => new ApiCouponReturnObject(x.Id, x.SequenceNumber, x.Code, theDeal)).ToList(),
                        UseTimeS = theDeal.BusinessHourDeliverTimeS.Value,
                        UseTimeE = theDeal.ChangedExpireDate ?? theDeal.BusinessHourDeliverTimeE.Value,
                        TimeTicks = odCol.CreateTimeTicks
                    };
                    return new HttpResponseMessage() { Content = new ObjectContent<ApiMakeOrderReturnObject>(rtnApiMakeOrderReturnObject, new JsonMediaTypeFormatter(), "application/json") };
                }
                catch (Exception error)
                {
                    rtnObject.Code = ApiReturnCode.PaymentError;
                    rtnObject.Message = "交易失敗！";
                    logger.Error(error.Message);
                    return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ob"></param>
        /// <param name="accesstoken"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.LionTravel)]
        public HttpResponseMessage SendSms(JObject ob, string accesstoken)
        {
            AuditLog(accesstoken, ob.ToString(), "SendSms");
            CouponSmsModel sms = new CouponSmsModel();
            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };
            try
            {
                sms = ob.ToObject<CouponSmsModel>();
            }
            catch
            {
                rtnObject.Code = ApiReturnCode.InputError;
                rtnObject.Message = "傳入參數格式不符";
                return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
            }
            int smsCount = pp.SMSLogGetCountByPpon(config.TmallDefaultUserName, sms.Id.ToString());
            if (smsCount >= 3)
            {
                rtnObject.Code = ApiReturnCode.InputInvalid;
                rtnObject.Message = "發送次數已超過3次";
                return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
            }
            else
            {
                //Order order = op.OrderGet(sms.OrderGuid);
                CashTrustLogCollection cashTrustLogs = mp.CashTrustLogGetListByOrderGuid(sms.OrderGuid, OrderClassification.LkSite);

                if (cashTrustLogs.Any(x => x.DeliveryType == (int)DeliveryType.ToHouse))
                {
                    rtnObject.Code = ApiReturnCode.InputError;
                    rtnObject.Message = "宅配訂單無法發送簡訊";
                    return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
                }

                CashTrustLogCollection returnCashTrustLogs = mp.PponRefundingCashTrustLogGetListByOrderGuid(sms.OrderGuid);

                if (cashTrustLogs.Any(x => x.CouponId == sms.Id))
                {
                    CashTrustLog cashTrustLog = cashTrustLogs.FirstOrDefault(x => x.CouponId == sms.Id);
                    switch ((TrustStatus)cashTrustLog.Status)
                    {
                        case TrustStatus.Initial:
                        case TrustStatus.Trusted:
                            if (returnCashTrustLogs.Any(x => x.CouponId == sms.Id))
                            {
                                rtnObject.Code = ApiReturnCode.InputInvalid;
                                rtnObject.Message = "訂單已申請退貨";
                            }
                            else
                            {
                                ExpirationDateSelector selector = new ExpirationDateSelector();
                                selector.ExpirationDates = new PponUsageExpiration(sms.OrderGuid);
                                if (selector.IsPastExpirationDate(DateTime.Now))
                                {
                                    rtnObject.Code = ApiReturnCode.InputInvalid;
                                    rtnObject.Message = "憑證已過期";
                                }
                                else
                                {
                                    try
                                    {
                                        ViewPponCoupon view_ppon_coupon = pp.ViewPponCouponGet(sms.Id);
                                        SendSmsProcess(view_ppon_coupon.BusinessHourGuid, view_ppon_coupon.BusinessHourDeliverTimeS.Value, view_ppon_coupon.BhChangedExpireDate ?? view_ppon_coupon.BusinessHourDeliverTimeE.Value, view_ppon_coupon.CouponId.Value, view_ppon_coupon.SequenceNumber,
                                             view_ppon_coupon.CouponCode, view_ppon_coupon.CouponStoreSequence.HasValue ? "，序號" + view_ppon_coupon.CouponStoreSequence.Value.ToString("0000") : string.Empty,
                                             sms.SmsMobile, view_ppon_coupon.OrderDetailGuid, string.Empty, config.TmallDefaultUserName, true);
                                        rtnObject.Code = ApiReturnCode.Success;
                                        return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
                                    }
                                    catch
                                    {
                                        rtnObject.Code = ApiReturnCode.Error;
                                        rtnObject.Message = "簡訊發送失敗";
                                        return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
                                    }
                                }
                            }
                            break;
                        case TrustStatus.Verified:
                            DateTime modifyDT = cashTrustLog.ModifyTime;
                            string modifyTime = string.Format("{0:yyyy/MM/dd HH:mm} ", modifyDT);

                            if (Helper.IsFlagSet(cashTrustLog.SpecialStatus, TrustSpecialStatus.VerificationForced) || Helper.IsFlagSet(cashTrustLog.SpecialStatus, TrustSpecialStatus.VerificationLost))
                            {
                                rtnObject.Code = ApiReturnCode.InputInvalid;
                                rtnObject.Message = "已強制核銷";
                            }
                            else
                            {
                                rtnObject.Code = ApiReturnCode.InputInvalid;
                                rtnObject.Message = "已核銷";
                            }
                            break;
                        case TrustStatus.Returned:
                            if (Helper.IsFlagSet(cashTrustLog.SpecialStatus, TrustSpecialStatus.ReturnForced))
                            {
                                rtnObject.Code = ApiReturnCode.InputInvalid;
                                rtnObject.Message = "已強制退貨";
                            }
                            else
                            {
                                rtnObject.Code = ApiReturnCode.InputInvalid;
                                rtnObject.Message = "已退貨";
                            }
                            break;
                        case TrustStatus.Refunded:
                            if (Helper.IsFlagSet(cashTrustLog.SpecialStatus, TrustSpecialStatus.ReturnForced))
                            {
                                rtnObject.Code = ApiReturnCode.InputInvalid;
                                rtnObject.Message = "已強制退貨";
                            }
                            else
                            {
                                rtnObject.Code = ApiReturnCode.InputInvalid;
                                rtnObject.Message = "已退貨";
                            }
                            break;
                        case TrustStatus.ATM:
                        default:
                            rtnObject.Code = ApiReturnCode.InputInvalid;
                            rtnObject.Message = "憑證狀態錯誤";
                            break;
                    }
                }
                else
                {
                    rtnObject.Code = ApiReturnCode.InputInvalid;
                    rtnObject.Message = "查無憑證資料";
                }
                return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
            }
        }

        /// <summary>
        /// 發送中台結清帳號驗證碼
        /// </summary>
        /// <param name="ob"></param>
        /// <param name="accesstoken"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.LionTravel)]
        public HttpResponseMessage SendAuthCodeSms(JObject ob, string accesstoken)
        {
            AuditLog(accesstoken, ob.ToString(), "SendAuthCodeSms");
            SendAuthCodeSmsModel sms = new SendAuthCodeSmsModel();
            ApiReturnObject rtnObject = new ApiReturnObject() { Code = ApiReturnCode.Success };
            try
            {
                sms = ob.ToObject<SendAuthCodeSmsModel>();
            }
            catch
            {
                rtnObject.Code = ApiReturnCode.InputError;
                rtnObject.Message = "傳入參數格式不符";
                return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
            }

            string decryptMobile = GetDecryptAuthCodeData(sms.Ticks, sms.SmsMobile, sms.TaskId);

            if (!RegExRules.CheckMobile(decryptMobile))
            {
                rtnObject.Code = ApiReturnCode.InputError;
                rtnObject.Message = "手機格式異常";
                return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
            }
            Random random = new Random();
            string code = random.Next(100000, 999999).ToString();

            SendAuthCodeSmsProcess(code, decryptMobile, config.TmallDefaultUserName, sms.TaskId);
            rtnObject.Code = ApiReturnCode.Success;
            rtnObject.Data = new
            {
                TaskId = sms.TaskId,
                Code = GetEncryptAuthCodeData(sms.Ticks, code, sms.TaskId)
            };
            return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
        }

        /// <summary>
        /// 訂單憑證資訊查詢
        /// </summary>
        /// <param name="ob"></param>
        /// <param name="accesstoken"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.LionTravel)]
        public HttpResponseMessage CheckLionTravelOrder(JObject ob, string accesstoken)
        {
            AuditLog(accesstoken, ob.ToString(), "CheckLionTravelOrder");

            ApiRefundReturnObject rtnObject = new ApiRefundReturnObject();
            ApiOrderInfoReturnObject rtnApiOrderInfoReturnObject = new ApiOrderInfoReturnObject();
            LionTravelOrderCheckModel orderinfo = new LionTravelOrderCheckModel();
            try
            {
                orderinfo = ob.ToObject<LionTravelOrderCheckModel>();
            }
            catch
            {
                rtnObject.Code = PaymentUtilityRefundType.OrderGuidInvalid;
                rtnObject.Message = "傳入參數格式不符";
                return new HttpResponseMessage() { Content = new ObjectContent<ApiRefundReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
            }

            OauthToken oauthToken = CommonFacade.OauthTokenGet(accesstoken);
            OAuthClientModel client = OAuthFacade.GetClient(oauthToken.AppId);
            AgentChannel orderClassType = ChannelFacade.GetOrderClassificationByName(client.Name);

            if (orderClassType.Equals(AgentChannel.NONE))
            {
                rtnObject.Code = PaymentUtilityRefundType.OrderGuidInvalid;
                rtnObject.Message = "查無訂單類型";
                return new HttpResponseMessage() { Content = new ObjectContent<ApiRefundReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
            }

            OrderCorrespondingCollection occ = op.OrderCorrespondingListGet(new string[] { OrderCorresponding.Columns.RelatedOrderId + "=" + orderinfo.RelatedOrderId, OrderCorresponding.Columns.Mobile + "=" + orderinfo.SmsMobile, OrderCorresponding.Columns.Type + "=" + (int)orderClassType });
            if (occ.Count == 0)
            {
                rtnObject.Code = PaymentUtilityRefundType.OrderGuidInvalid;
                rtnObject.Message = "查無訂單";
                return new HttpResponseMessage() { Content = new ObjectContent<ApiRefundReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
            }
            else
            {

                Order o = op.OrderGet(occ.OrderByDescending(x => x.Id).FirstOrDefault().OrderGuid);
                if ((o.OrderStatus & (int)OrderStatus.Complete) > 0)
                {
                    ViewCouponListSequenceCollection coupons = mp.GetCouponListSequenceListByOid(o.Guid, OrderClassification.LkSite);
                    rtnApiOrderInfoReturnObject.OrderGuid = o.Guid.ToString();

                    if (coupons.Any())
                    {
                        var vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(occ.First().BusinessHourGuid);
                        rtnApiOrderInfoReturnObject.Coupon.AddRange(coupons.Select(x => new ApiCouponReturnObject(x.Id, x.SequenceNumber, x.Code, vpd)).ToList());
                    }
                    return new HttpResponseMessage() { Content = new ObjectContent<ApiOrderInfoReturnObject>(rtnApiOrderInfoReturnObject, new JsonMediaTypeFormatter(), "application/json") };

                }
                else
                {
                    rtnObject.Code = PaymentUtilityRefundType.OrderGuidInvalid;
                    rtnObject.Message = "查無訂單";
                    return new HttpResponseMessage() { Content = new ObjectContent<ApiRefundReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
                }
            }

        }

        /// <summary>
        /// 多筆訂單憑證資訊查詢
        /// </summary>
        /// <param name="ob"></param>
        /// <param name="accesstoken"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.LionTravel)]
        public HttpResponseMessage CheckLionTravelMultiOrder(JArray ob, string accesstoken)
        {
            var rtnObjectList = new List<ApiOrderInfoMultiReturnObject>();
            foreach (var item in ob)
            {
                ApiRefundReturnObject rtnErrorObject = new ApiRefundReturnObject();
                ApiOrderInfoReturnObject couponInfo = new ApiOrderInfoReturnObject();
                ApiOrderInfoMultiReturnObject rtnObject = new ApiOrderInfoMultiReturnObject();

                AuditLog(accesstoken, item.ToString(), "CheckLionTravelOrder");
                LionTravelOrderCheckModel orderinfo = new LionTravelOrderCheckModel();
                try
                {
                    orderinfo = item.ToObject<LionTravelOrderCheckModel>();
                    rtnObject.RelatedOrderId = orderinfo.RelatedOrderId;
                    rtnObject.SmsMobile = orderinfo.SmsMobile;

                }
                catch
                {
                    rtnErrorObject.Code = PaymentUtilityRefundType.OrderGuidInvalid;
                    rtnErrorObject.Message = "傳入參數格式不符";
                    return new HttpResponseMessage()
                    {
                        Content =
                            new ObjectContent<ApiRefundReturnObject>(rtnErrorObject, new JsonMediaTypeFormatter(),
                                "application/json")
                    };
                }

                OauthToken oauthToken = CommonFacade.OauthTokenGet(accesstoken);
                OAuthClientModel client = OAuthFacade.GetClient(oauthToken.AppId);
                AgentChannel orderClassType = ChannelFacade.GetOrderClassificationByName(client.Name);

                if (orderClassType.Equals(AgentChannel.NONE))
                {
                    rtnErrorObject.Code = PaymentUtilityRefundType.OrderGuidInvalid;
                    rtnErrorObject.Message = "傳入參數格式不符";
                    return new HttpResponseMessage()
                    {
                        Content =
                            new ObjectContent<ApiRefundReturnObject>(rtnErrorObject, new JsonMediaTypeFormatter(),
                                "application/json")
                    };
                }

                OrderCorrespondingCollection occ =
                    op.OrderCorrespondingListGet(new string[]
                    {
                        OrderCorresponding.Columns.RelatedOrderId + "=" + orderinfo.RelatedOrderId,
                        OrderCorresponding.Columns.Mobile + "=" + orderinfo.SmsMobile,
                        OrderCorresponding.Columns.Type + "=" + (int) orderClassType
                    });
                if (occ.Count == 0)
                {
                    rtnObject.HasValue = false;
                }
                else
                {
                    Order o = op.OrderGet(occ.OrderByDescending(x => x.Id).FirstOrDefault().OrderGuid);
                    if ((o.OrderStatus & (int)OrderStatus.Complete) > 0)
                    {
                        ViewCouponListSequenceCollection coupons = mp.GetCouponListSequenceListByOid(o.Guid, OrderClassification.LkSite);
                        couponInfo.OrderGuid = o.Guid.ToString();
                        if (coupons.Any())
                        {
                            var vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(occ.First().BusinessHourGuid);
                            rtnObject.HasValue = true;
                            couponInfo.Coupon.AddRange(coupons.Select(y => new ApiCouponReturnObject(y.Id, y.SequenceNumber, y.Code, vpd)));
                        }

                        rtnObject.ReturnObject = couponInfo;
                    }
                }
                rtnObjectList.Add(rtnObject);
            }

            return new HttpResponseMessage() { Content = new StringContent(JsonConvert.SerializeObject(rtnObjectList, Formatting.Indented)) };

        }

        /// <summary>
        /// 憑證狀態查詢
        /// </summary>
        /// <param name="ob"></param>
        /// <param name="accesstoken"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.LionTravel)]
        public HttpResponseMessage CheckLionTravelCouponStatus(JObject ob, string accesstoken)
        {
            AuditLog(accesstoken, ob.ToString(), "CheckLionTravelCouponStatus");
            ApiRefundReturnObject rtnObject = new ApiRefundReturnObject();
            CouponStatusModel couponstatus = new CouponStatusModel();
            try
            {
                couponstatus = ob.ToObject<CouponStatusModel>();
            }
            catch
            {
                rtnObject.Code = PaymentUtilityRefundType.OrderGuidInvalid;
                rtnObject.Message = "傳入參數格式不符";
                return new HttpResponseMessage() { Content = new ObjectContent<ApiRefundReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
            }
            Order o = op.OrderGet(couponstatus.OrderGuid);
            if (o.Guid == Guid.Empty)
            {
                rtnObject.Code = PaymentUtilityRefundType.OrderGuidInvalid;
                rtnObject.Message = "查無訂單";
            }
            else
            {
                ViewCouponListSequenceCollection coupons = mp.GetCouponListSequenceListByOid(couponstatus.OrderGuid, OrderClassification.LkSite);
                ViewCouponListSequence coupon = null;
                if (coupons.Any(x => x.Id == couponstatus.Id))
                {
                    coupon = coupons.First(x => x.Id == couponstatus.Id);
                    rtnObject.Data = coupon.ModifyTime;

                    CouponStatusMessage rtnMsg = new CouponStatusMessage();

                    rtnMsg = GetCouponStatusMessage(coupon);
                    rtnObject.Code = rtnMsg.Code;
                    rtnObject.Message = rtnMsg.Message;
                }
                else
                {
                    rtnObject.Code = PaymentUtilityRefundType.CouponIdInvalid;
                    rtnObject.Message = "查無憑證號碼";
                    return new HttpResponseMessage() { Content = new ObjectContent<ApiRefundReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
                }



            }
            return new HttpResponseMessage() { Content = new ObjectContent<ApiRefundReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
        }

        /// <summary>
        /// 多筆憑證狀態查詢
        /// </summary>
        /// <param name="ob"></param>
        /// <param name="accesstoken"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.LionTravel)]
        public HttpResponseMessage CheckLionTravelMultiCouponStatus(JArray ob, string accesstoken)
        {
            return MultiCouponStatus(ob, accesstoken);
        }

        private HttpResponseMessage MultiCouponStatus(JArray ob, string accesstoken)
        {
            var rtnObjectList = new List<ApiRefundMultiReturnObject>();
            CouponStatusModel couponstatus = new CouponStatusModel();

            for (int i = 0; i < ob.Count; i++)
            {
                var rtnObject = new ApiRefundMultiReturnObject();
                AuditLog(accesstoken, ob[i].ToString(), "CheckLionTravelOrder");
                rtnObject.OrderGuid = ob[i]["OrderGuid"].ToString();
                rtnObject.Id = ob[i]["Id"].ToString();
                try
                {
                    couponstatus = ob[i].ToObject<CouponStatusModel>();
                }
                catch
                {
                    rtnObject.ReturnObject.Code = PaymentUtilityRefundType.OrderGuidInvalid;
                    rtnObject.ReturnObject.Message = "傳入參數格式不符";
                    return new HttpResponseMessage()
                    {
                        Content =
                            new ObjectContent<ApiRefundMultiReturnObject>(rtnObject, new JsonMediaTypeFormatter(),
                                "application/json")
                    };
                }
                Order o = op.OrderGet(couponstatus.OrderGuid);
                if (o.Guid == Guid.Empty)
                {
                    rtnObject.ReturnObject.Code = PaymentUtilityRefundType.OrderGuidInvalid;
                    rtnObject.ReturnObject.Message = "查無訂單";
                }
                else
                {
                    ViewCouponListSequenceCollection coupons =
                        mp.GetCouponListSequenceListByOid(couponstatus.OrderGuid, OrderClassification.LkSite);
                    ViewCouponListSequence coupon = null;
                    if (coupons.Any(x => x.Id == couponstatus.Id))
                    {
                        coupon = coupons.First(x => x.Id == couponstatus.Id);
                        rtnObject.ReturnObject.Data = coupon.ModifyTime;

                        CouponStatusMessage rtnMsg = new CouponStatusMessage();

                        rtnMsg = GetCouponStatusMessage(coupon);
                        rtnObject.ReturnObject.Code = rtnMsg.Code;
                        rtnObject.ReturnObject.ReturnStatus = rtnMsg.RefundStatus;
                        rtnObject.ReturnObject.ReturnModifyTime = rtnMsg.ReturnModifyTime;
                        rtnObject.ReturnObject.Message = rtnMsg.Message;
                    }
                    else
                    {
                        rtnObject.ReturnObject.Code = PaymentUtilityRefundType.CouponIdInvalid;
                        rtnObject.ReturnObject.Message = "查無憑證號碼";
                    }
                }
                rtnObjectList.Add(rtnObject);
            }
            return new HttpResponseMessage() { Content = new StringContent(JsonConvert.SerializeObject(rtnObjectList, Formatting.Indented)) };
        }

        /// <summary>
        /// 用時間查詢憑證狀態
        /// </summary>
        /// <param name="ob"></param>
        /// <param name="accesstoken"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.LionTravel)]
        public HttpResponseMessage CheckLionTravelCouponStatusBySearchTime(JObject ob, string accesstoken)
        {
            CouponStatusBySearchTimeObject getObject = new CouponStatusBySearchTimeObject();
            List<CouponStatusBySearchTimeReturnObject> rtnObjectList = new List<CouponStatusBySearchTimeReturnObject>();
            OauthToken oauthToken = CommonFacade.OauthTokenGet(accesstoken);
            OAuthClientModel client = OAuthFacade.GetClient(oauthToken.AppId);
            getObject = ob.ToObject<CouponStatusBySearchTimeObject>();
            AuditLog(accesstoken, ob.ToString(), "CheckLionTravelOrder");

            //檢查時間區間
            TimeSpan totalDay = getObject.EndTime.Subtract(getObject.StartTime); //日期相減
            TimeSpan MaxDay = new TimeSpan(30, 0, 0, 0);    //最大天數
            if (totalDay > MaxDay)
            {
                CouponStatusBySearchTimeReturnObject rtnObject = new CouponStatusBySearchTimeReturnObject();

                rtnObject.Code = PaymentUtilityRefundType.OrderGuidInvalid;
                rtnObject.Message = "參數錯誤，時間範圍超出一個月";
                rtnObjectList.Add(rtnObject);
                return new HttpResponseMessage() { Content = new StringContent(JsonConvert.SerializeObject(rtnObjectList, Formatting.Indented)) };
            }

            ViewCouponListSequenceCollection coupons = mp.GetCouponListSequenceListByBetweenTime(client.Id, getObject.StartTime, getObject.EndTime);

            foreach (var coupon in coupons)
            {
                CouponStatusBySearchTimeReturnObject rtnObject = new CouponStatusBySearchTimeReturnObject();
                CouponStatusMessage rtnMsg = new CouponStatusMessage();

                rtnMsg = GetCouponStatusMessage(coupon);

                rtnObject.Id = coupon.Id;
                rtnObject.SequenceNumber = coupon.SequenceNumber;
                rtnObject.Data = coupon.ModifyTime;
                rtnObject.Code = rtnMsg.Code;
                rtnObject.Message = rtnMsg.Message;

                rtnObjectList.Add(rtnObject);
            }
            return new HttpResponseMessage() { Content = new StringContent(JsonConvert.SerializeObject(rtnObjectList, Formatting.Indented)) };
        }

        /// <summary>
        /// 代銷模式的退貨申請
        /// </summary>
        /// <param name="ob"></param>
        /// <param name="accesstoken"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.LionTravel)]
        public HttpResponseMessage RefundLionTravelOrder(JObject ob, string accesstoken)
        {
            return RefundOrder(ob, accesstoken);
        }

        private HttpResponseMessage RefundOrder(JObject ob, string accesstoken)
        {
            AuditLog(accesstoken, ob.ToString(), "RefundLionTravelOrder");
            ApiRefundReturnObject rtnObject = new ApiRefundReturnObject();
            RefundLionTravelOrderModel orderguid = new RefundLionTravelOrderModel();
            try
            {
                orderguid = ob.ToObject<RefundLionTravelOrderModel>();
            }
            catch
            {
                rtnObject.Code = PaymentUtilityRefundType.OrderGuidInvalid;
                rtnObject.Message = "傳入參數格式不符";
                return new HttpResponseMessage() { Content = new ObjectContent<ApiRefundReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
            }

            OauthToken oauthToken = CommonFacade.OauthTokenGet(accesstoken);
            OAuthClientModel client = OAuthFacade.GetClient(oauthToken.AppId);
            AgentChannel orderClassType = ChannelFacade.GetOrderClassificationByName(client.Name);

            if (orderClassType.Equals(AgentChannel.NONE))
            {
                rtnObject.Code = PaymentUtilityRefundType.OrderGuidInvalid;
                rtnObject.Message = "查無訂單類型";
            }
            else
            {
                Order o = op.OrderGet(orderguid.OrderGuid);
                OrderCorrespondingCollection occ = op.OrderCorrespondingListGet(new string[] { OrderCorresponding.Columns.RelatedOrderId + "=" + orderguid.RelatedOrderId, OrderCorresponding.Columns.OrderGuid + "=" + orderguid.OrderGuid, OrderCorresponding.Columns.Type + "=" + (int)orderClassType });
                if (o.Guid == Guid.Empty || occ.Count == 0)
                {
                    rtnObject.Code = PaymentUtilityRefundType.OrderGuidInvalid;
                    rtnObject.Message = "查無訂單";
                }
                else
                {
                    //檢核成套檔次退貨
                    var vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(occ.First().BusinessHourGuid);
                    if (Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.GroupCoupon) && orderguid.CouponId.Any()) 
                    {
                        rtnObject.Code = PaymentUtilityRefundType.NoPartialReturns;
                        rtnObject.Message = "成套商品不接受部分退貨";
                        return new HttpResponseMessage() { Content = new ObjectContent<ApiRefundReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
                    }

                    RefundOrderInfo orderinfo = UserRefundFacade.GetOrderInfo(o.UserId, o.OrderId);
                    int slug;
                    DateTime now = DateTime.Now;
                    ViewCouponListMain main = orderinfo.Coupons.First();
                    //if (orderinfo.HasProcessingReturnForm)
                    //{
                    //    rtnObject.Code = PaymentUtilityRefundType.ProcessingReturnForm;
                    //    rtnObject.Message = "已申請退貨，無法再申請";
                    //}
                    //else 
                    if (!orderinfo.IsPpon && orderinfo.IsExchangeProcessing)
                    {
                        rtnObject.Code = PaymentUtilityRefundType.ExchangeProcessing;
                        rtnObject.Message = "已申請換貨處理中，無法再申請";
                    }
                    else if (!orderinfo.IsReturnable)
                    {
                        rtnObject.Code = PaymentUtilityRefundType.NotReturnable;
                        rtnObject.Message = "訂單已全數退貨，無法再申請";
                    }
                    else if (main.BusinessHourOrderTimeE <= now && int.TryParse(main.Slug, out slug) &&
                             slug < main.BusinessHourOrderMinimum)
                    {
                        rtnObject.Code = PaymentUtilityRefundType.DealStatusError;
                        rtnObject.Message = "未達門檻不需退貨";
                    }
                    else if (orderinfo.IsInvoice2To3)
                    {
                        rtnObject.Code = PaymentUtilityRefundType.Invoice2To3;
                        rtnObject.Message = "發票二聯改三聯處理中，無法申請退貨";
                    }
                    else
                    {
                        if (orderinfo.IsPpon)
                        {
                            if (main.TotalCount != 0 && main.RemainCount == 0)
                            {
                                rtnObject.Code = PaymentUtilityRefundType.CouponRemainError;
                                rtnObject.Message = "憑證已使用完畢，無法退貨";
                            }
                        }
                        else
                        {
                            if (PponOrderManager.CheckIsOverTrialPeriod(main.Guid))
                            {
                                rtnObject.Code = PaymentUtilityRefundType.OverTrialPeriod;
                                rtnObject.Message = "商品已過鑑賞期，無法退貨";
                            }
                        }
                    }

                    if ((main.OrderStatus & (int)OrderStatus.Complete) == 0)
                    {
                        if ((main.OrderStatus & (int)OrderStatus.ATMOrder) > 0)
                        {
                            rtnObject.Code = PaymentUtilityRefundType.ATMStatusError;
                            rtnObject.Message = main.CreateTime.Date.Equals(DateTime.Now.Date)
                                                            ? "尚未完成ATM付款，無需退貨。"
                                                            : "ATM 逾期未付款，無法退貨。";
                        }
                        else
                        {
                            rtnObject.Code = PaymentUtilityRefundType.OrderGuidInvalid;
                            rtnObject.Message = "未完成付款的訂單";
                        }
                    }
                    if ((main.Status & (int)GroupOrderStatus.NoRefund) > 0)
                    {
                        rtnObject.Code = PaymentUtilityRefundType.NoRefund;
                        rtnObject.Message = "此訂單不接受退貨申請";
                    }
                    if ((main.Status & (int)GroupOrderStatus.DaysNoRefund) > 0 && main.BusinessHourOrderTimeE.AddDays(config.ProductRefundDays) < DateTime.Now)
                    {
                        rtnObject.Code = PaymentUtilityRefundType.NoRefund;
                        rtnObject.Message = "此訂單不接受退貨申請";
                    }
                    if ((main.Status & (int)GroupOrderStatus.ExpireNoRefund) > 0)
                    {
                        if (main.BusinessHourDeliverTimeE != null && main.BusinessHourDeliverTimeE.Value.AddDays(1) < now)
                        {
                            rtnObject.Code = PaymentUtilityRefundType.NoRefund;
                            rtnObject.Message = "此訂單不接受退貨申請";
                        }
                    }
                    if ((main.Status & (int)GroupOrderStatus.NoRefundBeforeDays) > 0 && now.Date >= ((DateTime)main.BusinessHourDeliverTimeS).AddDays(-config.PponNoRefundBeforeDays).Date)
                    {
                        rtnObject.Code = PaymentUtilityRefundType.NoRefund;
                        rtnObject.Message = "此訂單不接受退貨申請";
                    }
                    if (main.ItemPrice.Equals(0) || (main.Status & (int)GroupOrderStatus.KindDeal) > 0)
                    {
                        rtnObject.Code = PaymentUtilityRefundType.NoRefund;
                        rtnObject.Message = "此訂單不接受退貨申請";
                    }
                    if (main.BusinessHourOrderTimeS < new DateTime(2011, 4, 1))
                    {
                        rtnObject.Code = PaymentUtilityRefundType.NoRefund;
                        rtnObject.Message = "此訂單不接受退貨申請";
                    }
                    string booking_lock_datetime = string.Empty;
                    List<int> locked_coupons = new List<int>();
                    if (orderinfo.IsReservationLock)
                    {
                        foreach (var coupon_id in orderguid.CouponId)
                        {
                            BookingSystemReserveLockStatusLogCollection booking_logs = bp.BookingSystemReserveLockStatusLogGetByCouponId(coupon_id, (int)ReserveCouponStatus.Lock);
                            if (booking_logs.Count() > 0)
                            {
                                booking_lock_datetime += string.Format("，CouponId:{0}，預約時間:{1}", coupon_id, booking_logs.OrderByDescending(x => x.ModifyTime).First().ModifyTime.ToString("yyyy/MM/dd HH:mm"));
                                locked_coupons.Add(coupon_id);
                            }
                        }

                    }
                    if (orderguid.CouponId != null && orderguid.CouponId.Count > 0 && orderguid.CouponId.Count == locked_coupons.Count)
                    {
                        rtnObject.Code = PaymentUtilityRefundType.ReservationLock;
                        rtnObject.Message = "訂單已預約，無法申請退貨" + booking_lock_datetime;
                    }
                    else
                    {
                        orderguid.CouponId = orderguid.CouponId.Except(locked_coupons).ToList();
                    }

                    if (string.IsNullOrEmpty(rtnObject.Message))
                    {
                        bool isRefundCash = false;//(e.Data.Value == RefundType.Atm || e.Data.Value == RefundType.Cash) ? true : false; //是否刷退 或 退款至帳戶

                        //bool requireCreditNote = false;
                        CreateReturnFormResult result;
                        int? dummy;
                        if (orderinfo.IsPpon)
                        {
                            IEnumerable<int> refundableCouponIds = ReturnService.GetRefundableCouponCtlogs(orderguid.OrderGuid, BusinessModel.Ppon).Select(x => (int)x.CouponId).ToList();
                            if (orderguid.CouponId != null && orderguid.CouponId.Count > 0)
                            {
                                IEnumerable<int> intersect_coupons = orderguid.CouponId.Intersect(refundableCouponIds);
                                if (intersect_coupons.Any())
                                {
                                    result = ReturnService.CreateCouponReturnForm(orderguid.OrderGuid, intersect_coupons, isRefundCash, config.TmallDefaultUserName, "透過API退貨", out dummy, false);
                                }
                                else
                                {
                                    result = CreateReturnFormResult.ProductsUnreturnable;
                                }
                            }
                            else if (refundableCouponIds.Any())
                            {
                                result = ReturnService.CreateCouponReturnForm(orderguid.OrderGuid, refundableCouponIds, isRefundCash, config.TmallDefaultUserName, "透過API退貨", out dummy, false);

                                //檢查是否需折讓單:已開立或準備開立發票、有使用現金購買及申請退現金
                                //requireCreditNote = einvoice.Any(x => x.CouponId.HasValue &&
                                //                                      refundableCouponIds.Contains(x.CouponId.Value) &&
                                //                                      x.VerifiedTime.HasValue)
                                //                    && orderinfo.IsPaidByCash
                                //                    && isRefundCash;
                            }
                            else
                            {
                                result = CreateReturnFormResult.ProductsUnreturnable;
                            }
                        }
                        else
                        {
                            List<int> orderProductIds = op.OrderProductGetListByOrderGuid(orderguid.OrderGuid).Where(x => x.IsCurrent && !x.IsReturning && !x.IsReturned && !x.IsExchanging).Select(x => x.Id).ToList();
                            if (orderProductIds.Any())
                            {
                                result = ReturnService.CreateRefundForm(orderguid.OrderGuid, orderProductIds, isRefundCash, config.TmallDefaultUserName,
                                    string.Empty, null, null, null, null, out dummy, false);
                            }
                            else
                            {
                                result = CreateReturnFormResult.ProductsUnreturnable;
                            }
                        }

                        if (result == CreateReturnFormResult.Created)
                        {
                            int returnFormId = op.ReturnFormGetListByOrderGuid(orderguid.OrderGuid)
                                  .OrderByDescending(x => x.Id)
                                  .First().Id;

                            string auditMessage = string.Format("退貨單({0})建立. 原因:{1}", returnFormId, string.Empty);
                            CommonFacade.AddAudit(orderguid.OrderGuid, AuditType.Refund, auditMessage, config.TmallDefaultUserName, false);

                            OrderFacade.SendCustomerReturnFormApplicationMail(returnFormId);
                            if (!orderinfo.IsPpon)
                            {
                                EmailFacade.SendProductRefundOrExchangeToSeller(OrderReturnType.Refund, returnFormId);
                            }
                            rtnObject.Code = PaymentUtilityRefundType.Success;

                        }
                        else
                        {
                            rtnObject.Code = PaymentUtilityRefundType.RefundProcessError;
                            rtnObject.Message = "退貨過程錯誤";
                        }
                    }
                }
            }


            return new HttpResponseMessage() { Content = new ObjectContent<ApiRefundReturnObject>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
        }

        /// <summary>
        /// 查詢宅配訂單
        /// </summary>
        /// <param name="ob"></param>
        /// <param name="accesstoken"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.LionTravel)]
        public HttpResponseMessage CheckDeliveryStatus(JArray ob, string accesstoken)
        {
            StringBuilder builder = new StringBuilder();
            AuditLog(accesstoken, ob.ToString(), "CheckLionTravelOrder");

            OauthToken oauthToken = CommonFacade.OauthTokenGet(accesstoken);
            OAuthClientModel client = OAuthFacade.GetClient(oauthToken.AppId);
            int userId = MemberFacade.GetUniqueId(config.TmallDefaultUserName);

            ApiReturnObject resultObject = new ApiReturnObject();
            Dictionary<string, List<Guid>> orderQuery = new Dictionary<string, List<Guid>>();

            int revenueCode = 0;
            #region 取得所有 OrderGuid

            foreach (var item in ob)
            {
                builder.AppendLine("1" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff"));
                CheckDelivery delivery = new CheckDelivery();
                try
                {
                    delivery = item.ToObject<CheckDelivery>();
                    delivery.IsLoaded = true;
                }
                catch
                {
                    resultObject = new ApiReturnObject
                    {
                        Code = ApiReturnCode.InputError,
                        Message = "輸入的參數錯誤",
                        Data = item
                    };
                    return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(resultObject, new JsonMediaTypeFormatter(), "application/json") };
                }
                builder.AppendLine("2" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff"));
                if (!delivery.IsLoaded) continue;

                var ocCol = op.OrderCorrespondingGet(delivery.RelatedOrderId, client.Id);
                builder.AppendLine("3" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff"));
                if (ocCol.Any())
                {
                    orderQuery.Add(delivery.RelatedOrderId, ocCol.Select(x => x.OrderGuid).ToList());
                    builder.AppendLine("4" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff"));
                    revenueCode = ocCol.First().Type;
                }
                builder.AppendLine("5" + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff"));
            }

            #endregion 取得所有 OrderGuid

            if (!orderQuery.Any())
            {
                resultObject = new ApiReturnObject
                {
                    Code = ApiReturnCode.DataNotExist,
                    Message = "查無宅配訂單資料"
                };
                return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(resultObject, new JsonMediaTypeFormatter(), "application/json") };
            }

            List<CheckDeliveryQuery> queryCol = new List<CheckDeliveryQuery>();

            foreach (var item in orderQuery)
            {
                builder.AppendLine("6" + item.Key + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff"));
                CheckDeliveryQuery query = new CheckDeliveryQuery
                {
                    RelatedOrderId = item.Key,
                    DeliveryData = new List<DeliveryDetail>()
                };

                foreach (Guid orderGuid in item.Value)
                {
                    builder.AppendLine("7" + orderGuid.ToString() + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff"));
                    string innerLog = string.Empty;
                    var order = PponDealApiManager.ApiUserOrderGetByOrderGuid(orderGuid, userId);
                    if (order == null)
                    {
                        continue;
                    }
                    builder.AppendLine("8" + orderGuid.ToString() + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff"));
                    query.DeliveryData.Add(new DeliveryDetail(order));
                    DateTime buyDate = DateTime.ParseExact(order.BuyDate.Substring(0, 15), "yyyyMMdd HHmmss", System.Globalization.CultureInfo.InvariantCulture);
                    builder.AppendLine("9" + orderGuid.ToString() + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff"));
                    query.RevenueCode = OrderFacade.GetCommissionRuleReturnCode((ChannelSource)revenueCode, DeliveryType.ToHouse, order.BusinessHourGuid, buyDate);
                    builder.AppendLine("10" + orderGuid.ToString() + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff"));
                }
                queryCol.Add(query);

            }

            resultObject = new ApiReturnObject
            {
                Code = ApiReturnCode.Success,
                Data = queryCol
            };

            AuditLog(accesstoken, queryCol.Count.ToString() + "筆" + builder.ToString(), "CheckLionTravelOrder");
            return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(resultObject, new JsonMediaTypeFormatter(), "application/json") };
        }

        /// <summary>
        /// 宅配退貨前查詢
        /// </summary>
        /// <param name="ob"></param>
        /// <param name="accesstoken"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.LionTravel)]
        public HttpResponseMessage CheckDeliveryRefundOrder(JObject ob, string accesstoken)
        {
            //Source
            AuditLog(accesstoken, ob.ToString(), "CheckDeliveryRefundOrder");

            OauthToken oauthToken = CommonFacade.OauthTokenGet(accesstoken);
            OAuthClientModel client = OAuthFacade.GetClient(oauthToken.AppId);
            ApiReturnObject resultObject;

            #region 取得 OrderGuid

            DeliveryRefundOrder refund;
            try
            {
                refund = ob.ToObject<DeliveryRefundOrder>();
                refund.IsLoaded = true;
            }
            catch
            {
                resultObject = new ApiReturnObject
                {
                    Code = ApiReturnCode.InputError,
                    Message = "輸入的參數錯誤"
                };
                return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(resultObject, new JsonMediaTypeFormatter(), "application/json") };
            }

            var ocCol = op.OrderCorrespondingGet(refund.RelatedOrderId, client.Id);
            if (!ocCol.Any())
            {
                resultObject = new ApiReturnObject
                {
                    Code = ApiReturnCode.DataNotExist,
                    Message = "查無宅配訂單資料"
                };
                return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(resultObject, new JsonMediaTypeFormatter(), "application/json") };

            }
            var orderGuid = ocCol.Select(x => x.OrderGuid).First();

            #endregion 取得 OrderGuid

            //check 能不能退貨
            var returnable = true;
            var message = string.Empty;
            string dummy;
            if (!ReturnService.AllowReturn(orderGuid, out dummy))
            {
                returnable = false;
                message = dummy;
            }
            if (ReturnService.HasProcessingReturnForm(orderGuid))
            {
                returnable = false;
                message = "有退貨中之商品";
            }
            if (ReturnService.GetReturnableProducts(orderGuid).Count < 1)
            {
                returnable = false;
                message = "無可退貨之商品";
            }

            PponOrder odr = new PponOrder(orderGuid);
            IEnumerable<SummarizedProductSpec> source = odr.GetToHouseProductSummary();
            if (source.Any())
            {
                DeliveryRefundOrderQuery refundOrder = new DeliveryRefundOrderQuery
                {
                    RefundInfo = source,
                    IsReturnable = returnable,
                    Message = message
                };

                resultObject = new ApiReturnObject
                {
                    Code = ApiReturnCode.Success,
                    Data = refundOrder,
                    Message = "查詢成功"
                };
            }
            else
            {
                resultObject = new ApiReturnObject
                {
                    Code = ApiReturnCode.InputError,
                    Message = "查詢失敗"
                };
            }

            return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(resultObject, new JsonMediaTypeFormatter(), "application/json") };
        }

        /// <summary>
        /// 宅配退貨
        /// </summary>
        /// <param name = "ob" ></param >
        /// <param name="accesstoken"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.LionTravel)]
        public HttpResponseMessage DeliveryRefund(JObject ob, string accesstoken)
        {
            //Source
            AuditLog(accesstoken, ob.ToString(), "DeliveryRefund");

            OauthToken oauthToken = CommonFacade.OauthTokenGet(accesstoken);
            OAuthClientModel client = OAuthFacade.GetClient(oauthToken.AppId);
            ApiReturnObject resultObject;

            #region 取得 OrderGuid

            DeliveryRefund refund;
            int? isNotifyChannel = null;
            try
            {
                refund = ob.ToObject<DeliveryRefund>();
                refund.IsLoaded = true;
            }
            catch
            {
                resultObject = new ApiReturnObject
                {
                    Code = ApiReturnCode.InputError,
                    Message = "輸入的參數錯誤"
                };
                return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(resultObject, new JsonMediaTypeFormatter(), "application/json") };
            }

            var ocCol = op.OrderCorrespondingGet(refund.RelatedOrderId, client.Id);
            if (!ocCol.Any())
            {
                resultObject = new ApiReturnObject
                {
                    Code = ApiReturnCode.DataNotExist,
                    Message = "查無宅配訂單資料"
                };
                return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(resultObject, new JsonMediaTypeFormatter(), "application/json") };

            }
            var orderGuid = ocCol.Select(x => x.OrderGuid).First();

            #endregion 取得 OrderGuid

            #region 選取退貨數量

            List<int> orderProductIds = new List<int>();
            foreach (var item in refund.RefundData)
            {
                var prodIds = item.ProductIds;
                var count = item.RefundCount;
                IEnumerable<string> prods = prodIds.Split(',');
                prods
                    .Take(count)
                    .ForEach(id => orderProductIds.Add(int.Parse(id)));
            }
            #endregion

            if (orderProductIds.Count > 0)
            {
                //CreateReturnForm
                var reason = string.Format("{0}, From:代銷API[{1}]", refund.Reason, client.Name);
                var isRefundCash = false;
                int? dummy;

                if (ocCol.First() != null && ocCol.First().Type == (int)AgentChannel.PayEasyOrder)
                {
                    //目前先pez的退貨需要通知
                    isNotifyChannel = (int)NotifyChannel.Init;
                }

                CreateReturnFormResult result = ReturnService.CreateRefundForm(orderGuid, orderProductIds, isRefundCash, config.TmallDefaultUserName,
                    reason, refund.ReceiverName, refund.ReceiverAddress, refund.IsReceive, isNotifyChannel, out dummy, false);
                if (result != CreateReturnFormResult.Created)
                {
                    string refundResultMessage;
                    switch (result)
                    {
                        case CreateReturnFormResult.ProductsUnreturnable:
                            refundResultMessage = "欲退項目的狀態無法退貨";
                            break;
                        case CreateReturnFormResult.AtmOrderNeedAtmRefundAccount:
                            refundResultMessage = "ATM訂單, 請先設定ATM退款帳號";
                            break;
                        case CreateReturnFormResult.OldOrderNeedAtmRefundAccount:
                            refundResultMessage = "舊訂單, 請先設定ATM退款帳號";
                            break;
                        case CreateReturnFormResult.InvalidArguments:
                            var comboCount = ReturnService.QueryComboPackCount(orderGuid);
                            refundResultMessage = !Equals(0, orderProductIds.Count() % comboCount) ? string.Format("此訂單為成套售出，退貨需成套 [({0})的倍數] 申請。", comboCount.ToString()) : "無法建立退貨單, 請洽技術部";
                            break;
                        case CreateReturnFormResult.OrderNotCreate:
                            refundResultMessage = "訂單未完成付款, 無法建立退貨單";
                            break;
                        case CreateReturnFormResult.InstallmentOrderNeedAtmRefundAccount:
                            refundResultMessage = "分期且部分退, 請先設定ATM退款帳號";
                            break;
                        case CreateReturnFormResult.ProductsIsReturning:
                            refundResultMessage = "有退貨中之商品";
                            break;
                        case CreateReturnFormResult.DealDeniesRefund:
                            refundResultMessage = "檔次不允許退貨";
                            break;
                        default:
                            refundResultMessage = "不明錯誤, 請洽技術部";
                            break;
                    }
                    resultObject = new ApiReturnObject
                    {
                        Code = ApiReturnCode.DataNotExist,
                        Message = refundResultMessage
                    };
                }
                else
                {
                    var returnFormCol = op.ReturnFormGetListByOrderGuid(orderGuid);
                    var returnFormId = returnFormCol.OrderByDescending(x => x.Id)
                                  .First().Id;
                    var auditMessage = string.Format("退貨單({0})建立. 原因:{1}, From:代銷API[{2}]", returnFormId, reason,
                        client.Name);
                    CommonFacade.AddAudit(orderGuid, AuditType.Refund, auditMessage, config.TmallDefaultUserName, false);
                    OrderFacade.SendCustomerReturnFormApplicationMail(returnFormId);
                    EmailFacade.SendProductRefundOrExchangeToSeller(OrderReturnType.Refund, returnFormId);

                    DeliveryRefundQuery refundResult = new DeliveryRefundQuery
                    {
                        ReturnFormId = returnFormId
                    };

                    resultObject = new ApiReturnObject
                    {
                        Code = ApiReturnCode.Success,
                        Data = refundResult,
                        Message = "退貨單建立成功"
                    };
                }
            }
            else
            {
                resultObject = new ApiReturnObject
                {
                    Code = ApiReturnCode.InputError,
                    Message = "退貨數量有誤"
                };
            }
            return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(resultObject, new JsonMediaTypeFormatter(), "application/json") };
        }

        /// <summary>
        /// 查詢宅配退貨狀態
        /// </summary>
        /// <param name = "ob" ></param >
        /// <param name="accesstoken"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.LionTravel)]
        public HttpResponseMessage CheckDeliveryRefundStatus(JArray ob, string accesstoken)
        {
            //Source
            AuditLog(accesstoken, ob.ToString(), "CheckDeliveryRefundStatus");

            OauthToken oauthToken = CommonFacade.OauthTokenGet(accesstoken);
            OAuthClientModel client = OAuthFacade.GetClient(oauthToken.AppId);
            ApiReturnObject resultObject;

            Dictionary<string, List<Guid>> refundQuery = new Dictionary<string, List<Guid>>();

            #region 取得所有 OrderGuid

            foreach (var item in ob)
            {
                CheckDeliveryRefundStatus queryRequest;
                try
                {
                    queryRequest = item.ToObject<CheckDeliveryRefundStatus>();
                    queryRequest.IsLoaded = true;
                }
                catch
                {
                    resultObject = new ApiReturnObject
                    {
                        Code = ApiReturnCode.InputError,
                        Message = "輸入的參數錯誤",
                        Data = item
                    };
                    return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(resultObject, new JsonMediaTypeFormatter(), "application/json") };
                }

                var ocCol = op.OrderCorrespondingGet(queryRequest.RelatedOrderId, client.Id);
                if (ocCol.Any())
                {
                    refundQuery.Add(queryRequest.RelatedOrderId, ocCol.Select(x => x.OrderGuid).ToList());
                }
            }

            #endregion 取得所有 OrderGuid

            List<DeliveryRefundStatusOutput> resultDataCol = new List<DeliveryRefundStatusOutput>();

            foreach (var refund in refundQuery)
            {
                DeliveryRefundStatusOutput resultData = new DeliveryRefundStatusOutput
                {
                    RelatedOrderId = refund.Key,
                    RefundData = new RefundStatusOrder()
                };


                foreach (var orderGuid in refund.Value)
                {
                    resultData.RefundData.OrderGuid = orderGuid;
                    IList<ReturnFormEntity> returnForms = ReturnFormRepository.FindAllByOrder(orderGuid);
                    if (!returnForms.Any())
                    {
                        resultData.RefundData.Remark = "查無退貨記錄";
                        continue;
                    }

                    List<CheckDeliveryRefundStatusQuery> queryCol = returnForms.Select(item => new CheckDeliveryRefundStatusQuery(item)).ToList();
                    resultData.RefundData.RefundLog = queryCol;
                }

                resultDataCol.Add(resultData);
            }

            resultObject = new ApiReturnObject
            {
                Code = ApiReturnCode.Success,
                Data = resultDataCol,
                Message = "退貨記錄查詢成功"
            };

            return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(resultObject, new JsonMediaTypeFormatter(), "application/json") };
        }

        /// <summary>
        /// 查詢退貨單
        /// </summary>
        /// <param name="ob"></param>
        /// <param name="accesstoken"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.LionTravel)]
        public HttpResponseMessage CheckRefundFormStatus(JArray ob, string accesstoken)
        {
            ApiReturnObject resultObject;
            AuditLog(accesstoken, ob.ToString(), "CheckRefundFormStatus");
            OauthToken oauthToken = CommonFacade.OauthTokenGet(accesstoken);
            OAuthClientModel client = OAuthFacade.GetClient(oauthToken.AppId);
            AgentChannel orderCorrespondingType = AgentChannel.NONE;

            if (client != null)
            {
                orderCorrespondingType = ChannelFacade.GetOrderClassificationByName(client.Name);
            }

            if (orderCorrespondingType == AgentChannel.NONE)
            {
                resultObject = new ApiReturnObject
                {
                    Code = ApiReturnCode.InputError,
                    Message = "輸入的參數錯誤"
                };
                return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(resultObject, new JsonMediaTypeFormatter(), "application/json") };
            }

            List<int> queryRefundFormIds = new List<int>();
            foreach (var item in ob)
            {
                CheckRefundFormId formId;
                try
                {
                    formId = item.ToObject<CheckRefundFormId>();
                    formId.IsLoaded = true;
                }
                catch
                {
                    resultObject = new ApiReturnObject
                    {
                        Code = ApiReturnCode.InputError,
                        Message = "輸入的參數錯誤",
                        Data = item
                    };
                    return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(resultObject, new JsonMediaTypeFormatter(), "application/json") };
                }
                queryRefundFormIds.Add(formId.RefundFormId);
            }

            if (!queryRefundFormIds.Any())
            {
                resultObject = new ApiReturnObject
                {
                    Code = ApiReturnCode.InputError,
                    Message = "輸入的參數錯誤"
                };
                return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(resultObject, new JsonMediaTypeFormatter(), "application/json") };
            }

            List<CheckDeliveryRefundStatusQuery> queryCol = new List<CheckDeliveryRefundStatusQuery>();

            foreach (var formId in queryRefundFormIds)
            {
                ReturnFormEntity returnForm = ReturnFormRepository.FindById(formId);
                //確認退貨單是否屬於該代銷商
                var ocCol = op.OrderCorrespondingGetByOrderGuid(returnForm.OrderGuid, orderCorrespondingType);
                if (!ocCol.Any()) continue; //此Order Guid不屬於此代銷商
                CheckDeliveryRefundStatusQuery query = new CheckDeliveryRefundStatusQuery(returnForm);
                queryCol.Add(query);
            }

            resultObject = new ApiReturnObject
            {
                Code = ApiReturnCode.Success,
                Data = queryCol
            };

            return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(resultObject, new JsonMediaTypeFormatter(), "application/json") };
        }


        /// <summary>
        /// 查詢中台憑證退貨狀態
        /// </summary>
        /// <param name="ob"></param>
        /// <param name="accesstoken"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.LionTravel)]
        public HttpResponseMessage CheckCouponRefundForm(JArray ob, string accesstoken)
        {
            ApiReturnObject resultObject;
            AuditLog(accesstoken, ob.ToString(), "CheckCouponRefundForm");
            OauthToken oauthToken = CommonFacade.OauthTokenGet(accesstoken);
            OAuthClientModel client = OAuthFacade.GetClient(oauthToken.AppId);
            AgentChannel orderCorrespondingType = AgentChannel.NONE;

            if (client != null)
            {
                orderCorrespondingType = ChannelFacade.GetOrderClassificationByName(client.Name);
            }

            if (orderCorrespondingType == AgentChannel.NONE)
            {
                resultObject = new ApiReturnObject
                {
                    Code = ApiReturnCode.InputError,
                    Message = "輸入的參數錯誤"
                };
                return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(resultObject, new JsonMediaTypeFormatter(), "application/json") };
            }

            List<Guid> queryRefundFormIds = new List<Guid>();
            foreach (var item in ob)
            {
                CheckDeliveryRefundStatus formId;
                try
                {
                    formId = item.ToObject<CheckDeliveryRefundStatus>();
                    formId.IsLoaded = true;
                }
                catch
                {
                    resultObject = new ApiReturnObject
                    {
                        Code = ApiReturnCode.InputError,
                        Message = "輸入的參數錯誤",
                        Data = item
                    };
                    return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(resultObject, new JsonMediaTypeFormatter(), "application/json") };
                }

                var ocCol = op.OrderCorrespondingGet(formId.RelatedOrderId, client.Id);
                if (ocCol.Any())
                {
                    queryRefundFormIds = ocCol.Select(x => x.OrderGuid).ToList();
                }
            }

            if (!queryRefundFormIds.Any())
            {
                resultObject = new ApiReturnObject
                {
                    Code = ApiReturnCode.InputError,
                    Message = "輸入的參數錯誤"
                };
                return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(resultObject, new JsonMediaTypeFormatter(), "application/json") };
            }

            List<CheckDeliveryRefundStatusQuery> queryCol = new List<CheckDeliveryRefundStatusQuery>();

            foreach (var formId in queryRefundFormIds)
            {
                ReturnFormEntity returnForm = ReturnFormRepository.FindAllByOrder(formId).FirstOrDefault(x => x.ProgressStatus == ProgressStatus.Completed);
                //確認退貨單是否屬於該代銷商
                CheckDeliveryRefundStatusQuery query = new CheckDeliveryRefundStatusQuery(returnForm);
                queryCol.Add(query);
            }

            resultObject = new ApiReturnObject
            {
                Code = ApiReturnCode.Success,
                Data = queryCol
            };

            return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(resultObject, new JsonMediaTypeFormatter(), "application/json") };
        }

        [HttpPost]
        [OAuthScope(TokenScope.LionTravel)]
        public HttpResponseMessage GroupCouponTrustInfo(JObject ob, string accessToken) 
        {
            ApiReturnObject resultObject;
            AuditLog(accessToken, ob.ToString(), "GroupCouponTrustInfo");
            OauthToken oauthToken = CommonFacade.OauthTokenGet(accessToken);
            OAuthClientModel client = OAuthFacade.GetClient(oauthToken.AppId);
            AgentChannel orderCorrespondingType = AgentChannel.NONE;
            GCTrustInfoInput input = new GCTrustInfoInput();

            if (client != null)
            {
                orderCorrespondingType = ChannelFacade.GetOrderClassificationByName(client.Name);
            }

            if (orderCorrespondingType == AgentChannel.NONE)
            {
                resultObject = new ApiReturnObject
                {
                    Code = ApiReturnCode.InputError,
                    Message = "輸入的參數錯誤"
                };
                return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(resultObject, new JsonMediaTypeFormatter(), "application/json") };
            }

            try
            {
                input = ob.ToObject<GCTrustInfoInput>();
            }
            catch
            {
                resultObject = new ApiReturnObject
                {
                    Code = ApiReturnCode.InputError,
                    Message = "輸入的參數錯誤"
                };
                return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(resultObject, new JsonMediaTypeFormatter(), "application/json") };
            }

            Guid orderGuid = Guid.Empty;
            if (string.IsNullOrEmpty(input.OrderId))
            {
                resultObject = new ApiReturnObject
                {
                    Code = ApiReturnCode.InputError,
                    Message = "輸入的參數錯誤"
                };
                return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(resultObject, new JsonMediaTypeFormatter(), "application/json") };
            }

            if (Guid.TryParse(input.OrderId, out orderGuid)) 
            {
                var od = op.OrderGet(orderGuid);
                if (!od.IsLoaded) 
                {
                    orderGuid = Guid.Empty;
                }

            }

            if (orderGuid == Guid.Empty) 
            {
                var oc = op.OrderCorrespondingGet(input.OrderId, client.Id);
                if (oc.Any()) 
                {
                    orderGuid = oc.First().OrderGuid;
                }
            }

            if (orderGuid == Guid.Empty) 
            {
                resultObject = new ApiReturnObject
                {
                    Code = ApiReturnCode.DataNotExist,
                    Message = "查無訂單資料"
                };
                return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(resultObject, new JsonMediaTypeFormatter(), "application/json") };
            }

            CashTrustLogCollection ctCol = mp.CashTrustLogGetListByOrderGuid(orderGuid, OrderClassification.LkSite);
            if (!ctCol.Any()) 
            {
                resultObject = new ApiReturnObject
                {
                    Code = ApiReturnCode.DataNotExist,
                    Message = "查無訂單資料"
                };
                return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(resultObject, new JsonMediaTypeFormatter(), "application/json") };
            }

            if (!Helper.IsFlagSet(ViewPponDealManager.DefaultManager.ViewPponDealGetByBid((Guid)ctCol.First().BusinessHourGuid).BusinessHourStatus
                    , BusinessHourStatus.GroupCoupon)) 
            {
                resultObject = new ApiReturnObject
                {
                    Code = ApiReturnCode.DataNotExist,
                    Message = "查無訂單資料"
                };
                return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(resultObject, new JsonMediaTypeFormatter(), "application/json") };
            }

            GroupCouponInfoResult data = new GroupCouponInfoResult 
            {
                OrderGuid = orderGuid
            };

            CashTrustLogCollection returnFormCtlogs = mp.PponRefundingCashTrustLogGetListByOrderGuid(orderGuid);
            data.TrustInfoData = ctCol.Select(c => new TrustInfo(c, returnFormCtlogs.FirstOrDefault(x=>x.CouponId == c.CouponId)))
                .OrderByDescending(a=>a.CostAmount).ToList();
            resultObject = new ApiReturnObject
            {
                Code = ApiReturnCode.Success,
                Data = data
            };
            return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(resultObject, new JsonMediaTypeFormatter(), "application/json") };
        }

        #endregion

        #region Private Method

        private void SendSmsProcess(Guid business_hour_guid, DateTime business_hour_delivertime_s, DateTime business_hour_deliver_e,
            int coupon_id, string sequence_number, string code, string store_sequence, string mobile, Guid order_detail_guid, string sms_memo, string created_id, bool remove_17life_title = false)
        {
            #region send SMS 不走workflow
            SMS sms = new SMS();
            SmsContent sc = OrderFacade.SMSContentGet(business_hour_guid);
            string[] msgs = sc.Content.Split('|');
            string branch = OrderFacade.SmsGetPhoneFromWhere(order_detail_guid, business_hour_guid);  //分店資訊 & 多重選項
            string msg = string.Empty;
            if (remove_17life_title)
            {
                //todo 緊急處理，之後改成更好的取代方式
                msgs[2] = msgs[2].Replace(@"，下載APP看憑證：http://x.co/4r1JG", string.Empty);
                msgs[2] = msgs[2].Replace(@"，App載憑證：http://x.co/4r1JG", string.Empty);
                //透過我們發送簡訊的代銷商抬頭仍保留17life
                msg = string.Format(msgs[0] + msgs[1] + branch + msgs[2] + (string.IsNullOrEmpty(sms_memo) ? string.Empty : ("，" + sms_memo)),
                    "編號" + sequence_number,
                    "確認碼" + code,
                    business_hour_delivertime_s.ToString("yyyyMMdd"),
                    business_hour_deliver_e.ToString("yyyyMMdd"),
                    store_sequence);
            }
            else
            {
                msg = string.Format(msgs[0] + msgs[1] + branch + msgs[2] + (string.IsNullOrEmpty(sms_memo) ? string.Empty : ("，" + sms_memo)),
                        sequence_number,
                        code,
                        business_hour_delivertime_s.ToString("yyyyMMdd"),
                        business_hour_deliver_e.ToString("yyyyMMdd"),
                        store_sequence);
            }
            sms.QueueMessage(msg, SmsType.Ppon, created_id, mobile, business_hour_guid, coupon_id.ToString(), SmsSystem.iTe2);

            #endregion
        }


        private void SendAuthCodeSmsProcess(string code, string phoneNumber, string createId, Guid taskId)
        {
            #region send SMS
            SMS sms = new SMS();
            string content = string.Format("康太商城手機驗證碼為【{0}】，請您於3分鐘內輸入此驗證碼，以進行帳號清算身分驗證，謝謝", code);

            SmsSendReply smsReply;
            ISmsSender sender = new iTe2Sender(); ;
            sender.Send(content, phoneNumber, new SmsExtraInfo
            {
            }, out smsReply);

            if (smsReply.Equals(SmsSendReply.Empty) == true)
            {
                //發送失敗，放到Queue
                sms.QueueMessage(content, SmsType.Ppon, createId, phoneNumber, Guid.Empty, code, SmsSystem.iTe2, taskId);
            }
            else
            {
                SmsLog slog = new SmsLog();
                slog.Message = content;
                slog.Type = (int)SmsType.Ppon;
                slog.CreateTime = DateTime.Now;
                slog.CreateId = createId;
                slog.Mobile = phoneNumber;
                slog.Status = (int)SmsStatus.Success;
                slog.FailReason = (int)SmsFailReason.None;
                slog.AcceptableStartHour = 0;
                slog.AcceptableEndHour = 0;

                slog.SmsOrderCount = 0;
                slog.SmsOrderId = smsReply.OrderId;

                slog.CouponId = code;
                slog.TaskId = taskId;
                slog.Provider = (int)SmsSystem.iTe2;
                pp.SMSLogSet(slog);
            }
            #endregion
        }

        private string GetDecryptAuthCodeData(long ticks, string input, Guid taskId)
        {
            string result = string.Empty;
            string code = taskId.ToString().Replace("-", string.Empty);
            int startIdx = Convert.ToInt16(ticks.ToString().Substring(4, 1));
            byte[] key = Encoding.UTF8.GetBytes(code.Substring(startIdx, 16));
            byte[] iv = Encoding.UTF8.GetBytes(code.Substring(startIdx, 16));

            Security security = new Security(SymmetricCryptoServiceProvider.AES, key, iv);
            result = security.Decrypt(input);

            return result;
        }

        private string GetEncryptAuthCodeData(long ticks, string input, Guid taskId)
        {
            string result = string.Empty;
            string code = taskId.ToString().Replace("-", string.Empty);
            int startIdx = Convert.ToInt16(ticks.ToString().Substring(4, 1));
            byte[] key = Encoding.UTF8.GetBytes(code.Substring(startIdx, 16));
            byte[] iv = Encoding.UTF8.GetBytes(code.Substring(startIdx, 16));

            Security security = new Security(SymmetricCryptoServiceProvider.AES, key, iv);
            result = security.Encrypt(input);

            return result;
        }

        private bool CheckBranch(PponStore pponStore, int itemQuantity, int? saleMultipleBase)
        {
            //分店設定有設定數量控制，檢查是否超過上限，若是則不允許購買
            itemQuantity = (saleMultipleBase ?? 0) > 0 ? saleMultipleBase.Value : itemQuantity;
            if ((pponStore.TotalQuantity != null) &&
                (pponStore.TotalQuantity.Value < pponStore.OrderedQuantity + itemQuantity))
            {
                return false;
            }
            return true;
        }

        private string CheckLionTravelData(IViewPponDeal viewPponDeal, int quantity)
        {
            if (quantity > (viewPponDeal.MaxItemCount ?? int.MaxValue))
            {
                return "每單訂購數量最多" + viewPponDeal.MaxItemCount + "份，請重新輸入數量";
            }
            if (PponFacade.CheckExceedOrderedQuantityLimit(viewPponDeal, quantity))
            {
                return "很抱歉!已超過最大訂購數量";
            }
            if (DateTime.Now > viewPponDeal.BusinessHourOrderTimeE)
            {
                return "很抱歉~此好康已結束";
            }
            return string.Empty;
        }

        private decimal CheckDeliveryCharge(decimal orderedAmount, IViewPponDeal deal)
        {
            return deal.BusinessHourDeliveryCharge;
        }

        private int SetDeliveryCharge(decimal orderedAmount, IViewPponDeal deal)
        {
            CouponFreightCollection cfc = pp.CouponFreightGetList(deal.BusinessHourGuid, CouponFreightType.Income);

            if (null != cfc && cfc.Count > 0)
            {
                foreach (CouponFreight cf in cfc)
                {
                    if (orderedAmount >= cf.StartAmount && cf.EndAmount > orderedAmount)
                    {
                        return (int)(cf.FreightAmount);
                    }
                }
            }
            else
            {
                return (int)CheckDeliveryCharge(orderedAmount, deal);
            }

            return 0;
        }

        private void AuditLog(string accesstoken, string ob, string actionname)
        {
            CommonFacade.AddAudit(accesstoken, AuditType.Api, string.Format("{0};{1};{2}", ob, actionname, HttpContext.Current.Request.UserHostAddress), config.TmallDefaultUserName, false);
        }

        /// <summary>
        /// CouponStatus處理、判斷狀態並回傳Message與Code
        /// P.S:將原本CouponStatus、MultiCouponStatus做的事情整合到這邊來
        /// </summary>
        /// <param name="coupon"></param>
        /// <returns></returns>
        private CouponStatusMessage GetCouponStatusMessage(ViewCouponListSequence coupon)
        {
            CouponStatusMessage rtnMsg = new CouponStatusMessage();
            int slug;
            Order o = op.OrderGet(coupon.Guid); //還是需要呼叫OrderGet 因為orderinfo需要用到OrderId
            RefundOrderInfo orderinfo = UserRefundFacade.GetOrderInfo(MemberFacade.GetUniqueId(config.TmallDefaultUserName), o.OrderId);
            DateTime now = DateTime.Now;
            ViewCouponListMain main = orderinfo.Coupons.First();
            CashTrustLogCollection cash_trust_logs = mp.CashTrustLogGetListByOrderGuid(o.Guid, OrderClassification.LkSite);
            CashTrustLogCollection return_cash_trust_logs = mp.PponRefundingCashTrustLogGetListByOrderGuid(o.Guid);

            if (cash_trust_logs.Any(x => x.CouponId == coupon.Id))
            {
                CashTrustLog cash_trust_log = cash_trust_logs.FirstOrDefault(x => x.CouponId == coupon.Id);
                switch ((TrustStatus)cash_trust_log.Status)
                {
                    case TrustStatus.Initial:
                    case TrustStatus.Trusted:
                        if (return_cash_trust_logs.Any(x => x.CouponId == coupon.Id))
                        {
                            rtnMsg.Code = PaymentUtilityRefundType.ProcessingReturnForm;
                            rtnMsg.Message = "訂單已申請退貨";
                        }
                        else
                        {
                            ExpirationDateSelector selector = new ExpirationDateSelector();
                            selector.ExpirationDates = new PponUsageExpiration(coupon.Guid);
                            if (selector.IsPastExpirationDate(DateTime.Now))
                            {
                                if (orderinfo.IsReservationLock &&
                                    BookingSystemFacade.IsReservationLock(coupon.Id))
                                {
                                    rtnMsg.Code = PaymentUtilityRefundType.ExpiredAndReservationLock;
                                    rtnMsg.Message = "憑證有預約但已過期";
                                }
                                else
                                {
                                    rtnMsg.Code = PaymentUtilityRefundType.CouponExpired;
                                    rtnMsg.Message = "憑證已過期";
                                }
                            }
                            else
                            {
                                rtnMsg.Code = PaymentUtilityRefundType.CouponUnUsed;
                                rtnMsg.Message = "憑證未使用";
                            }
                        }
                        break;
                    case TrustStatus.Verified:
                        DateTime modifyDT = cash_trust_log.ModifyTime;
                        string modifyTime = string.Format("{0:yyyy/MM/dd HH:mm} ", modifyDT);

                        if (
                            Helper.IsFlagSet(cash_trust_log.SpecialStatus,
                                TrustSpecialStatus.VerificationForced) ||
                            Helper.IsFlagSet(cash_trust_log.SpecialStatus,
                                TrustSpecialStatus.VerificationLost))
                        {
                            rtnMsg.Code = PaymentUtilityRefundType.CouponUsed;
                            rtnMsg.Message = "已強制核銷";
                        }
                        else
                        {
                            rtnMsg.Code = PaymentUtilityRefundType.CouponUsed;
                            rtnMsg.Message = "已核銷";
                        }
                        break;
                    case TrustStatus.Returned:
                        if (Helper.IsFlagSet(cash_trust_log.SpecialStatus, TrustSpecialStatus.ReturnForced))
                        {
                            rtnMsg.Code = PaymentUtilityRefundType.RefundSuccess;
                            rtnMsg.Message = "已強制退貨";
                        }
                        else
                        {
                            rtnMsg.Code = PaymentUtilityRefundType.RefundSuccess;
                            rtnMsg.Message = "已退貨";
                        }
                        break;
                    case TrustStatus.Refunded:
                        if (Helper.IsFlagSet(cash_trust_log.SpecialStatus, TrustSpecialStatus.ReturnForced))
                        {
                            rtnMsg.Code = PaymentUtilityRefundType.RefundSuccess;
                            rtnMsg.Message = "已強制退貨";
                        }
                        else
                        {
                            rtnMsg.Code = PaymentUtilityRefundType.RefundSuccess;
                            rtnMsg.Message = "已退貨";
                        }
                        break;
                    case TrustStatus.ATM:
                    default:
                        rtnMsg.Code = PaymentUtilityRefundType.CouponStatusError;
                        rtnMsg.Message = "憑證狀態錯誤";
                        break;
                }


                #region 退貨狀態獨立出來顯示
                ReturnFormRefund rfr = op.ReturnFormRefundGetList(cash_trust_log.TrustId).OrderByDescending(x => x.ReturnFormId).FirstOrDefault();
                if (rfr != null)
                {
                    ReturnForm rf = op.ReturnFormGet(rfr.ReturnFormId);

                    rtnMsg.RefundStatus = (ProgressStatus)rf.ProgressStatus;
                    rtnMsg.ReturnModifyTime = rf.ModifyTime;
                    if (rtnMsg.RefundStatus == ProgressStatus.Completed && cash_trust_log.Status == (int)TrustStatus.Verified)
                    {
                        //一張訂單多張憑證,部份成功部分失敗
                        rtnMsg.RefundStatus = ProgressStatus.Unreturnable;
                        rtnMsg.ReturnModifyTime = null;
                    }
                }
                else
                {
                    rtnMsg.RefundStatus = ProgressStatus.NotRefound;
                    rtnMsg.ReturnModifyTime = null;
                }
                #endregion
            }
            else
            {
                if (orderinfo.HasProcessingReturnForm)
                {
                    rtnMsg.Code = PaymentUtilityRefundType.ProcessingReturnForm;
                    rtnMsg.Message = "訂單已申請退貨";
                }
                else if (!orderinfo.IsPpon && orderinfo.IsExchangeProcessing)
                {
                    rtnMsg.Code = PaymentUtilityRefundType.ExchangeProcessing;
                    rtnMsg.Message = "訂單已申請換貨處理中";
                }
                else if (!orderinfo.IsReturnable)
                {
                    rtnMsg.Code = PaymentUtilityRefundType.NotReturnable;
                    rtnMsg.Message = "訂單訂單已全數退貨";
                }
                else if (main.BusinessHourOrderTimeE <= now && int.TryParse(main.Slug, out slug) &&
                         slug < main.BusinessHourOrderMinimum)
                {
                    rtnMsg.Code = PaymentUtilityRefundType.DealStatusError;
                    rtnMsg.Message = "訂單未達門檻";
                }
                else
                {
                    if (orderinfo.IsPpon)
                    {
                        if (main.TotalCount != 0 && main.RemainCount == 0)
                        {
                            rtnMsg.Code = PaymentUtilityRefundType.CouponUsed;
                            rtnMsg.Message = "憑證已使用完畢";
                        }
                        else
                        {
                            if (coupon == null)
                            {
                                rtnMsg.Code = PaymentUtilityRefundType.OrderGuidInvalid;
                                rtnMsg.Message = "查無憑證";
                            }
                            else
                            {
                                //非憑證檔次
                                if (coupon.Status < 0)
                                {
                                    rtnMsg.Code = PaymentUtilityRefundType.OrderGuidInvalid;
                                    rtnMsg.Message = "查無憑證";

                                }
                                else
                                {
                                    switch ((TrustStatus)coupon.Status)
                                    {
                                        case TrustStatus.Initial:
                                        case TrustStatus.Trusted:
                                            ExpirationDateSelector selector = new ExpirationDateSelector();
                                            selector.ExpirationDates =
                                                new PponUsageExpiration(coupon.Guid);
                                            if (selector.IsPastExpirationDate(DateTime.Now))
                                            {
                                                if (orderinfo.IsReservationLock &&
                                                    BookingSystemFacade.IsReservationLock(coupon.Id))
                                                {
                                                    rtnMsg.Code =
                                                        PaymentUtilityRefundType.ExpiredAndReservationLock;
                                                    rtnMsg.Message = "憑證有預約但已過期";
                                                }
                                                else
                                                {
                                                    rtnMsg.Code = PaymentUtilityRefundType.CouponExpired;
                                                    rtnMsg.Message = "憑證已過期";
                                                }
                                            }
                                            else
                                            {
                                                rtnMsg.Code = PaymentUtilityRefundType.CouponUnUsed;
                                                rtnMsg.Message = "憑證未使用";
                                            }
                                            break;
                                        case TrustStatus.Verified:
                                            rtnMsg.Code = PaymentUtilityRefundType.CouponUsed;
                                            rtnMsg.Message = "憑證已使用";
                                            break;
                                        case TrustStatus.Returned:
                                        case TrustStatus.Refunded:
                                            rtnMsg.Code = PaymentUtilityRefundType.ProcessingReturnForm;
                                            rtnMsg.Message = "憑證已申請退貨";
                                            break;
                                        default:
                                            rtnMsg.Code = PaymentUtilityRefundType.CouponStatusError;
                                            rtnMsg.Message = "憑證狀態錯誤";
                                            break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return rtnMsg;
        }

        #endregion Private Method


        #region PChome


        /// <summary>
        /// PChome代銷訂購
        /// </summary>
        /// <param name="accesstoken"></param>
        /// <returns></returns>
        [OAuthScope(TokenScope.LionTravel)]
        [HttpPost]
        public HttpResponseMessage MakePChomeOrder(JObject ob, string accesstoken)
        {
            AuditLog(accesstoken, ob.ToString(), "MakePChomeOrder");
            PChomeMakeOrderModel input;
            ApiChannelPChomeResult rtnObject = new ApiChannelPChomeResult() { Code = "200-00" };
            try
            {

                #region 前置資料準備
                try
                {
                    input = ob.ToObject<PChomeMakeOrderModel>();
                }
                catch
                {
                    rtnObject.Code = "400-01";
                    rtnObject.Msg = "Invalid Input";
                    return new HttpResponseMessage() { Content = new ObjectContent<ApiChannelPChomeResult>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
                }

                //檢查壓碼
                string macCode = Security.GetSHA1Hash(input.ProductNo + input.Quantity + config.PChomeChannelVendorNo);
                if (macCode != input.MacCode)
                {
                    rtnObject.Code = "400-01";
                    rtnObject.Msg = "Invalid Input";
                    return new HttpResponseMessage() { Content = new ObjectContent<ApiChannelPChomeResult>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
                }

                int uniqueId = 0;
                int.TryParse(input.ProductNo, out uniqueId);
                //IViewPponDeal theDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(dealGuid);
                IViewPponDeal theDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByUniqueId(uniqueId);

                if (!theDeal.IsLoaded)
                {
                    rtnObject.Code = "400-02";
                    rtnObject.Msg = "Product Not Exist";
                    return new HttpResponseMessage() { Content = new ObjectContent<ApiChannelPChomeResult>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
                }


                Guid storeGuid = Guid.Empty;
                if (!Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.NoRestrictedStore))
                {
                    var stores = ViewPponStoreManager.DefaultManager.ViewPponStoreGetList(theDeal.BusinessHourGuid, VbsRightFlag.VerifyShop);
                    if (stores.Count() == 1)
                    {
                        storeGuid = stores[0].StoreGuid;
                    }
                    else
                    {
                        rtnObject.Code = "400-99";
                        rtnObject.Msg = "分店資訊有誤";
                        return new HttpResponseMessage() { Content = new ObjectContent<ApiChannelPChomeResult>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
                    }
                }
                else
                {
                    storeGuid = theDeal.SellerGuid;
                }

                ChannelMakeOrderModel channelOrder = new ChannelMakeOrderModel();
                channelOrder.DealGuid = theDeal.BusinessHourGuid;
                channelOrder.StoreGuid = storeGuid;
                channelOrder.Quantity = input.Quantity;
                channelOrder.RelatedOrderId = "12345678";
                channelOrder.SmsMobile = "0900000000";
                channelOrder.IsSendSms = false;
                channelOrder.IsConfirmAmount = false;
                channelOrder.Amount = Convert.ToInt32(theDeal.ItemPrice) * input.Quantity;
                #endregion

                var postData = JObject.FromObject(channelOrder).ToString();
                string content = MakeOrder(JObject.Parse(postData), accesstoken).Content.ReadAsStringAsync().Result;
                ApiMakeOrderReturnObject contentModel = JsonConvert.DeserializeObject<ApiMakeOrderReturnObject>(content);

                if (contentModel.OrderGuid != null)
                {
                    #region 轉成PChome需要的格式
                    ApiPChomeMakeOrderReturnObject returnModel = new ApiPChomeMakeOrderReturnObject();
                    returnModel.OrderNo = contentModel.OrderId;

                    //0:明碼, 1:QRCode, 2:BarCode, 3:Url //咖啡寄杯只有條碼
                    if (config.PChomeChannelCouponQRCODE == theDeal.BusinessHourGuid.ToString())
                    {
                        returnModel.AppearType = 1;
                    }
                    else if (config.PChomeChannelCouponBarCode == theDeal.BusinessHourGuid.ToString())
                    {
                        returnModel.AppearType = 2;
                    }
                    else
                    {
                        //一般憑證都預設QRCODE
                        returnModel.AppearType = 1;
                    }

                    returnModel.NoticeMsg = "票券應記載事項" + "<br />";
                    returnModel.NoticeMsg += @"發行單位：康太數位整合股份有限公司  負責人：李易騰 <br />
                                統一編號：24317014 <br />
                                實收資本額：180,000,000 <br />
                                地址：台北市中山區中山北路一段11號13樓 電話 :2521 - 9131 <br />
                                信託期間：發售日起一年內 <br />
                                本商品(服務)禮券所收取之金額，已存入發行人於台新銀行、陽信銀行開立之信託專戶，專款專用。所稱專用，係指供發行人履行交付商品或提供服務義務使用。<br />
                                在電子票券優惠期間內：尚未到店兌換的電子票券，可辦理全額無息退費，作業手續費$0。<br />
                                超過電子票券優惠期間：尚未到店兌換的電子票券，可辦理全額無息退費，作業手續費$0。<br />
                                兌換優惠期間內，與發行人合作之店家倒閉或因故無法提供商品或服務，可辦理全額無息退費，作業手續費$0。<br />
                                <br />
                                ※部分電子票券所表彰之內容為依據特定單一時間與特定場合所提供之服務(包括但不限於演唱會、 音樂會或展覽)，當電子票券持有人因故未能於該特定時間與特定場合兌換該服務，恕無法要求退費或另行補足差額重現該服務。<br />
                                ※本電子票券所表彰之商品或服務內容由與發行人合作之店家提供，PChome則為電子票券銷售服務提供者。請電子票券持有人務必於兌換優惠期間內使用，並確實了解本電子票券及商品頁面所載明之使用說明（包括但不限於：使用時段、事前預約規定、內用或外帶、每人使用限制）<br />
                                ※消費爭議處理申訴專線：2704 - 0999";


                    returnModel.Vouchers = new List<MakeOrderVouchers>();
                    foreach (ApiCouponReturnObject coupon in contentModel.Coupon)
                    {
                        MakeOrderVouchers voucher = new MakeOrderVouchers();
                        voucher.VoucherUId = coupon.Id.ToString();
                        voucher.VoucherNo = ChannelFacade.PChomeChannelEncryptStr(coupon.SequenceNumber);
                        voucher.AuthCode = ChannelFacade.PChomeChannelEncryptStr(coupon.Code);
                        voucher.VoucherUrl = ChannelFacade.PChomeChannelEncryptStr("");
                        voucher.StartTime = theDeal.BusinessHourDeliverTimeS.Value.ToString("yyyy-MM-dd HH:mm:ss");
                        voucher.ExpireTime = theDeal.ChangedExpireDate != null ? theDeal.ChangedExpireDate.Value.ToString("yyyy-MM-dd HH:mm:ss") : theDeal.BusinessHourDeliverTimeE.Value.ToString("yyyy-MM-dd HH:mm:ss");
                        voucher.Amount = Convert.ToInt32(theDeal.ItemPrice);
                        voucher.AmountUnit = "time";//time:次, point:點, ntd:元,
                        returnModel.Vouchers.Add(voucher);
                    }
                    #endregion

                    return new HttpResponseMessage() { Content = new ObjectContent<ApiPChomeMakeOrderReturnObject>(returnModel, new JsonMediaTypeFormatter(), "application/json") };
                }
                else
                {
                    ApiReturnObject errContentModel = JsonConvert.DeserializeObject<ApiReturnObject>(content);

                    rtnObject.Code = "400-99";
                    rtnObject.Msg = errContentModel.Message;
                    logger.Error("PChome MakePChomeOrder content=" + ob.ToString() + " error=" + errContentModel.Message);
                    return new HttpResponseMessage() { Content = new ObjectContent<ApiChannelPChomeResult>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
                }
            }
            catch (Exception ex)
            {
                rtnObject.Code = "400-99";
                rtnObject.Msg = ex.StackTrace;
                logger.Error("PChome MakePChomeOrder content=" + ob.ToString() + " error=" + ex.Message, ex);
                return new HttpResponseMessage() { Content = new ObjectContent<ApiChannelPChomeResult>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
            }

        }

        /// <summary>
        /// PChome代銷退貨
        /// </summary>
        /// <param name="accesstoken"></param>
        /// <returns></returns>
        [OAuthScope(TokenScope.LionTravel)]
        [HttpPost]
        public HttpResponseMessage RefundPChomeOrder(JObject ob, string accesstoken)
        {
            AuditLog(accesstoken, ob.ToString(), "RefundPChomeOrder");
            PChomeRefundLionTravelOrderModel input;
            ApiChannelPChomeResult rtnObject = new ApiChannelPChomeResult() { Code = "200-00" };
            try
            {
                #region 前置資料準備
                try
                {
                    input = ob.ToObject<PChomeRefundLionTravelOrderModel>();
                }
                catch
                {
                    rtnObject.Code = "400-01";
                    rtnObject.Msg = "Invalid Input";
                    return new HttpResponseMessage() { Content = new ObjectContent<ApiChannelPChomeResult>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
                }

                //檢查壓碼
                string macCode = Security.GetSHA1Hash(input.OrderNo + input.Quantity + string.Join("", input.VoucherUIds) + config.PChomeChannelVendorNo);
                if (macCode != input.MacCode)
                {
                    rtnObject.Code = "400-01";
                    rtnObject.Msg = "Invalid Input";
                    return new HttpResponseMessage() { Content = new ObjectContent<ApiChannelPChomeResult>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
                }

                if (input.Quantity != input.VoucherUIds.Count())
                {
                    rtnObject.Code = "400-99";
                    rtnObject.Msg = "退貨數量不一致";
                    return new HttpResponseMessage() { Content = new ObjectContent<ApiChannelPChomeResult>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
                }

                Order order = op.OrderGetByOrderId(input.OrderNo);
                OrderCorresponding oc = op.OrderCorrespondingGetByOrderGuid(order.Guid, AgentChannel.PChome).FirstOrDefault();
                IViewPponDeal theDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(oc.BusinessHourGuid);

                RefundLionTravelOrderModel channelredund = new RefundLionTravelOrderModel();
                channelredund.OrderGuid = order.Guid;
                channelredund.RelatedOrderId = oc.RelatedOrderId;
                channelredund.CouponId = input.VoucherUIds;
                #endregion

                var postData = JObject.FromObject(channelredund).ToString();
                string content = RefundOrder(JObject.Parse(postData), accesstoken).Content.ReadAsStringAsync().Result;
                ApiRefundReturnObject contentModel = JsonConvert.DeserializeObject<ApiRefundReturnObject>(content);

                if (contentModel.Code == PaymentUtilityRefundType.Success)
                {
                    #region 轉成PChome需要的格式
                    ApiPChomeRefundLionTravelOrderModel returnModel = new ApiPChomeRefundLionTravelOrderModel();
                    returnModel.OrderNo = input.OrderNo;
                    returnModel.Status = (int)PChomeRefund.Processing;
                    returnModel.Vouchers = new List<ReturnVouchers>();
                    foreach (int couponId in input.VoucherUIds)
                    {
                        ReturnVouchers voucher = new ReturnVouchers();
                        voucher.VoucherUId = couponId;
                        voucher.RefundAmount = 0;
                        returnModel.Vouchers.Add(voucher);
                    }
                    #endregion

                    return new HttpResponseMessage() { Content = new ObjectContent<ApiPChomeRefundLionTravelOrderModel>(returnModel, new JsonMediaTypeFormatter(), "application/json") };
                }
                else
                {
                    ApiRefundReturnObject errContentModel = JsonConvert.DeserializeObject<ApiRefundReturnObject>(content);

                    rtnObject.Code = "400-99";
                    rtnObject.Msg = errContentModel.Message;
                    return new HttpResponseMessage() { Content = new ObjectContent<ApiChannelPChomeResult>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
                }
            }
            catch (Exception ex)
            {
                rtnObject.Code = "400-99";
                rtnObject.Msg = ex.StackTrace;
                return new HttpResponseMessage() { Content = new ObjectContent<ApiChannelPChomeResult>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
            }
        }

        /// <summary>
        /// PChome代銷多筆憑證狀態查詢
        /// </summary>
        /// <param name="accesstoken"></param>
        /// <returns></returns>
        [OAuthScope(TokenScope.LionTravel)]
        [HttpPost]
        public HttpResponseMessage CheckPChomeMultiCouponStatus(JObject ob, string accesstoken)
        {
            AuditLog(accesstoken, ob.ToString(), "CheckPChomeMultiCouponStatus");
            PChomeMultiCouponStatusModel input;
            ApiChannelPChomeResult rtnObject = new ApiChannelPChomeResult() { Code = "200-00" };
            try
            {
                #region 前置資料準備
                try
                {
                    input = ob.ToObject<PChomeMultiCouponStatusModel>();
                }
                catch
                {
                    rtnObject.Code = "400-01";
                    rtnObject.Msg = "Invalid Input";
                    return new HttpResponseMessage() { Content = new ObjectContent<ApiChannelPChomeResult>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
                }

                Order order = op.OrderGetByOrderId(input.OrderNo);
                if (!order.IsLoaded)
                {
                    rtnObject.Code = "400-04";
                    rtnObject.Msg = "錯誤的訂單";
                    return new HttpResponseMessage() { Content = new ObjectContent<ApiChannelPChomeResult>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
                }
                OrderCorresponding oc = op.OrderCorrespondingGetByOrderGuid(order.Guid, AgentChannel.PChome).FirstOrDefault();
                IViewPponDeal theDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(oc.BusinessHourGuid);

                List<CouponStatusModel> channelcouponstatuss = new List<CouponStatusModel>();
                foreach (int couponId in input.VoucherUIds)
                {
                    CouponStatusModel channelcouponstatus = new CouponStatusModel();
                    channelcouponstatus.OrderGuid = order.Guid;
                    channelcouponstatus.Id = couponId;
                    channelcouponstatuss.Add(channelcouponstatus);
                }
                #endregion

                var postData = JsonConvert.SerializeObject(channelcouponstatuss);
                string content = MultiCouponStatus(JArray.Parse(postData), accesstoken).Content.ReadAsStringAsync().Result;
                List<ApiRefundMultiReturnObject> contentModel = JsonConvert.DeserializeObject<List<ApiRefundMultiReturnObject>>(content);

                if (contentModel.Any())
                {
                    #region 轉成PChome需要的格式
                    ApiPChomeMultiCouponStatusModel couponStatusModel = new ApiPChomeMultiCouponStatusModel();
                    couponStatusModel.OrderNo = input.OrderNo;
                    couponStatusModel.Vouchers = new List<CouponStatusVouchers>();

                    foreach (ApiRefundMultiReturnObject m in contentModel)
                    {
                        CouponStatusVouchers voucher = new CouponStatusVouchers();
                        voucher.VoucherUId = m.Id;
                        if (m.ReturnObject.Code == PaymentUtilityRefundType.CouponUnUsed)
                        {
                            voucher.VoucherStatus = (int)PChomeVoucherStatus.None;
                        }
                        else if (m.ReturnObject.Code == PaymentUtilityRefundType.CouponUsed)
                        {
                            voucher.VoucherStatus = (int)PChomeVoucherStatus.Used;
                        }
                        else if (m.ReturnObject.Code == PaymentUtilityRefundType.CouponExpired)
                        {
                            voucher.VoucherStatus = (int)PChomeVoucherStatus.Expired;
                        }

                        else if (m.ReturnObject.Code == PaymentUtilityRefundType.ProcessingReturnForm)
                        {
                            //退貨中顯示未使用
                            voucher.VoucherStatus = (int)PChomeVoucherStatus.None;
                        }
                        else if (m.ReturnObject.Code == PaymentUtilityRefundType.RefundSuccess)
                        {
                            //退貨完成才顯示已退券
                            voucher.VoucherStatus = (int)PChomeVoucherStatus.Refund;
                        }
                        else if (m.ReturnObject.Code == PaymentUtilityRefundType.CouponIdInvalid)
                        {
                            rtnObject.Code = "400-05";
                            rtnObject.Msg = "錯誤的票券";
                            return new HttpResponseMessage() { Content = new ObjectContent<ApiChannelPChomeResult>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
                        }

                        voucher.RefundTime = "";
                        if (m.ReturnObject.ReturnStatus == ProgressStatus.NotRefound)
                        {
                            voucher.RefundStatus = (int)PChomeRefundStatus.None;
                        }
                        else if (m.ReturnObject.ReturnStatus == ProgressStatus.Processing)
                        {
                            voucher.RefundStatus = (int)PChomeRefundStatus.RefundProcessing;
                        }
                        else if (m.ReturnObject.ReturnStatus == ProgressStatus.Completed)
                        {
                            voucher.RefundStatus = (int)PChomeRefundStatus.RefundSuccess;
                            voucher.RefundTime = m.ReturnObject.ReturnModifyTime.Value.ToString("yyyy-MM-dd HH:mm:ss");
                        }
                        else if (m.ReturnObject.ReturnStatus == ProgressStatus.Unreturnable)
                        {
                            voucher.RefundStatus = (int)PChomeRefundStatus.RefundFail;
                        }
                        else if (m.ReturnObject.ReturnStatus == ProgressStatus.Canceled)
                        {
                            voucher.RefundStatus = (int)PChomeRefundStatus.RefundCancel;
                        }

                        voucher.RemainAmount = Convert.ToInt32(theDeal.ItemPrice);
                        voucher.UpdateTime = m.ReturnObject.Data.ToString();
                        couponStatusModel.Vouchers.Add(voucher);
                    }
                    #endregion

                    return new HttpResponseMessage() { Content = new ObjectContent<ApiPChomeMultiCouponStatusModel>(couponStatusModel, new JsonMediaTypeFormatter(), "application/json") };
                }
                else
                {
                    //string errContent = response.Content.ReadAsStringAsync().Result;
                    //List<ApiRefundMultiReturnObject> errContentModel = JsonConvert.DeserializeObject<List<ApiRefundMultiReturnObject>>(content);

                    rtnObject.Code = "400-99";
                    rtnObject.Msg = "No contentModel";
                    return new HttpResponseMessage() { Content = new ObjectContent<ApiChannelPChomeResult>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
                }
            }
            catch (Exception ex)
            {
                rtnObject.Code = "400-99";
                rtnObject.Msg = ex.StackTrace;
                return new HttpResponseMessage() { Content = new ObjectContent<ApiChannelPChomeResult>(rtnObject, new JsonMediaTypeFormatter(), "application/json") };
            }
        }
        #endregion
    }
}

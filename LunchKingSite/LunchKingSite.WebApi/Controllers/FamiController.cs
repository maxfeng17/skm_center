﻿using System;
using System.Linq;
using System.Web.Http;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Interface;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System.Net.Http;
using System.Net;
using System.Text;
using System.Web;
using LunchKingSite.BizLogic.Models.FamiPort;
using LunchKingSite.WebApi.Core;
using LunchKingSite.WebApi.Models;
using System.Collections.Generic;
using System.Net.Http.Formatting;
using System.Reflection;
using log4net;
using LunchKingSite.BizLogic.Models.QueueModels;
using LunchKingSite.Core.Models.Entities;
using LunchKingSite.WebApi.Core.Attributes;
using LunchKingSite.WebApi.Core.OAuth;
using Newtonsoft.Json.Linq;
using Helper = LunchKingSite.Core.Helper;
using Peztemp = LunchKingSite.DataOrm.Peztemp;

namespace LunchKingSite.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class FamiController : BaseController
    {
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static IFamiportProvider fami = ProviderFactory.Instance().GetProvider<IFamiportProvider>();
        private static IMemberProvider member = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private static IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        private static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private static IMGMProvider mgmp= ProviderFactory.Instance().GetProvider<IMGMProvider>();
        private static readonly ILog logger = LogManager.GetLogger("Fami");

        private void FamiLog(string tranNo, FamiChannel channel, string methodName, FamiApiLogModel model, string returnCode, string message)
        {
            OrderFacade.FamiApiLogSet(new FamiApiLog
            {
                TranNo = tranNo,
                Channel = (byte)channel,
                Type = methodName,
                LogMsg = new JsonSerializer().Serialize(model),
                ReturnCode = returnCode,
                Message = message,
                CreateTime = DateTime.Now
            });
        }

        private void FamiLog(string tranNo, FamiChannel channel, string methodName, string logMsg, string returnCode, string message)
        {
            OrderFacade.FamiApiLogSet(new FamiApiLog
            {
                TranNo = tranNo,
                Channel = (byte)channel,
                Type = methodName,
                LogMsg = logMsg,
                ReturnCode = returnCode,
                Message = message,
                CreateTime = DateTime.Now
            });
        }

        /// <summary>
        /// 確認小白單是否已列印 (Peztemp.IsUsed)
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        [HttpGet]
        public dynamic CheckFamiCode(string code)
        {
            try
            {
                ApiReturnCode returnCode;
                string message;

                const int serviceCode = (int)PeztempServiceCode.Famiport;
                var pez = OrderFacade.PeztempCheck(code, serviceCode);
                if (pez.IsLoaded && pez.Bid.HasValue)
                {
                    if (pez.IsUsed)
                    {
                        message = "序號已使用";
                        returnCode = ApiReturnCode.InputInvalid;
                    }
                    else
                    {
                        var deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(pez.Bid.Value);
                        if (deal.IsLoaded)
                        {
                            var fami = OrderFacade.FamiportGet(deal.BusinessHourGuid);
                            if (fami.IsLoaded)
                            {
                                returnCode = ApiReturnCode.Success;

                                FamiLog(string.Empty, FamiChannel.Famiport, System.Reflection.MethodBase.GetCurrentMethod().Name
                                                    , new FamiApiLogModel
                                                    {
                                                        PinCode = code,
                                                        Bid = deal.BusinessHourGuid
                                                    }, returnCode.ToString(), "Success");

                                string defaultImage = config.SiteUrl +
                                                        "/Themes/PCweb/images/ppon-M1_pic.jpg";

                                return new
                                {
                                    StatusCode = returnCode,
                                    ImgUrl = ImageFacade.GetMediaPathsFromRawData(deal.EventImagePath, MediaType.PponDealPhoto).DefaultIfEmpty(defaultImage).First(),
                                    ItemName = deal.ItemName,
                                    FamiItemName = fami.ItemName,
                                    ItemPrice = deal.ExchangePrice,
                                    Bid = deal.BusinessHourGuid,
                                    ItemCode = fami.ItemCode,
                                    EndDate = (deal.BusinessHourDeliverTimeE.HasValue) ? deal.BusinessHourDeliverTimeE.Value.ToString("yyyyMMdd") : "",
                                    VendorId = fami.VendorId,
                                    EventId = fami.EventId,
                                    EventAttention1 = (string.IsNullOrEmpty(fami.EventAttention1)) ? "" : fami.EventAttention1.Trim(),
                                    EventAttention2 = (string.IsNullOrEmpty(fami.EventAttention2)) ? "" : fami.EventAttention2.Trim(),
                                    EventAttention3 = (string.IsNullOrEmpty(fami.EventAttention3)) ? "" : fami.EventAttention3.Trim(),
                                    EventAttention4 = (string.IsNullOrEmpty(fami.EventAttention4)) ? "" : fami.EventAttention4.Trim(),
                                    ItemAttention1 = (string.IsNullOrEmpty(fami.ItemAttention1)) ? "" : fami.ItemAttention1.Trim(),
                                    ItemAttention2 = (string.IsNullOrEmpty(fami.ItemAttention2)) ? "" : fami.ItemAttention2.Trim(),
                                    ItemAttention3 = (string.IsNullOrEmpty(fami.ItemAttention3)) ? "" : fami.ItemAttention3.Trim(),
                                    ItemAttention4 = (string.IsNullOrEmpty(fami.ItemAttention4)) ? "" : fami.ItemAttention4.Trim(),
                                    ItemRemark = (string.IsNullOrEmpty(fami.ItemRemark)) ? "" : fami.ItemRemark.Trim()
                                };
                            }
                            message = "Famiport檔次不存在";
                            returnCode = ApiReturnCode.DataNotExist;
                        }
                        else
                        {
                            message = "ViewPponDeal檔次不存在";
                            returnCode = ApiReturnCode.DataNotExist;
                        }
                    }
                }
                else
                {
                    message = "序號於期間內查無資料";
                    returnCode = ApiReturnCode.DataNotExist;
                }
                FamiLog(string.Empty, FamiChannel.Famiport, System.Reflection.MethodBase.GetCurrentMethod().Name
                    , new FamiApiLogModel
                    {
                        PinCode = code
                    }, returnCode.ToString(), message);
                return new { StatusCode = returnCode, Message = message };
            }
            catch (Exception ex)
            {
                var rtnObject = new ApiReturnObject
                {
                    Code = ApiReturnCode.InputError,
                    Data = ex,
                    Message = "執行階段發生錯誤"
                };
                FamiLog(string.Empty, FamiChannel.Famiport, System.Reflection.MethodBase.GetCurrentMethod().Name
                    , new FamiApiLogModel
                    {
                        PinCode = code,
                        ExMsg = ex.Message,
                        StackTrace = ex.StackTrace
                    }, "Exception", ex.Message);
                throw new HttpErrorResponseException("B001", "執行階段發生錯誤");
            }
        }

        /// <summary>
        /// 更新POS機付款資料 (分Famiport(MMK Id=153)流程 / Mobile(MMK Id=154)流程)
        /// </summary>
        /// <param name="tenCode"></param>
        /// <param name="tranNo"></param>
        /// <param name="mmkId"></param>
        /// <param name="serialNo"></param>
        /// <param name="cdFmcode"></param>
        /// <param name="firmNo"></param>
        /// <param name="eventNo"></param>
        /// <param name="payDate"></param>
        /// <param name="payTime"></param>
        /// <param name="rtnUrl"></param>
        /// <param name="traceUrl"></param>
        /// <param name="errUrl"></param>
        /// <param name="data1"></param>
        /// <param name="data2"></param>
        /// <param name="data3"></param>
        /// <returns></returns>
        [HttpGet]
        public dynamic UpdateFamiCodePayData(string tenCode, string tranNo, string mmkId, string serialNo, string cdFmcode, string firmNo, string eventNo, string payDate, string payTime, string rtnUrl, string traceUrl, string errUrl, string data1, string data2, string data3)
        {
            FamiChannel channel = FamiChannel.Famiport;
            try
            {
                var peztempId = 0;
                var rtnObject = new ApiReturnObject();
                var returnCode = ApiReturnCode.DataNotExist;
                string message;

                if (mmkId == config.FamiPosMMKId)
                {
                    channel = FamiChannel.FamiPos;
                    FamiposBarcode fami = OrderFacade.FamiposBarcodeGet(tranNo);
                    if (!fami.IsLoaded)
                    {
                        message = string.Format("查無TranNo:{0}的Famipos Barcode資料。", tranNo);
                    }
                    else if (fami.Status == (byte)FamiposBarcodeStatus.TranComplete)
                    {
                        returnCode = ApiReturnCode.InputInvalid;
                        message = string.Format("此筆資料已兌換過({0},{1},{2})", fami.TmTenName, fami.MachineNo, fami.TmTranNo);
                    }
                    else
                    {
                        peztempId = fami.PeztempId;
                        fami.SerialNo = serialNo;
                        fami.CdFmcode = cdFmcode;
                        fami.FirmNo = firmNo;
                        fami.EventNo = eventNo;
                        fami.PayDate = payDate;
                        fami.PayTime = payTime;
                        fami.Status = (byte)FamiposBarcodeStatus.TranComplete;
                        fami.UpdatePayTime = DateTime.Now;

                        //Cnvert 付款時間為 DateTime 格式
                        if (payDate.Length >= 8 && payTime.Length >= 6)
                        {
                            DateTime payDateTime;
                            if (DateTime.TryParse(string.Format("{0}/{1}/{2} {3}:{4}:{5}", payDate.Substring(0, 4)
                                , payDate.Substring(4, 2), payDate.Substring(6, 2), payTime.Substring(0, 2)
                                , payTime.Substring(2, 2), payTime.Substring(4, 2)), out payDateTime))
                            {
                                fami.PayDatetime = payDateTime;
                            }
                        }

                        if (!OrderFacade.FamiposBarcodeSet(fami, true))
                        {
                            returnCode = ApiReturnCode.InputError;
                            message = string.Format("Update FamiposBarcode Error. tranNo:{0}, {1} {2}", tranNo, payDate, payTime);
                        }
                        else
                        {
                            returnCode = ApiReturnCode.Success;
                            message = "Update FamiposBarcode Success.";
                        }
                    }
                }
                else if (mmkId == config.FamiportMMKId)
                {
                    var fpl = OrderFacade.GetFamiportPeztempLink(tranNo);
                    if (!fpl.IsLoaded)
                    {
                        message = string.Format("查無TranNo:{0}的FamiportPeztempLink資料。", tranNo);
                    }
                    else
                    {
                        //檢查是否已由 famiport 流程兌換過了
                        if (fpl.PayDatetime != null)
                        {
                            returnCode = ApiReturnCode.InputInvalid;
                            message = string.Format("此筆資料已兌換過({0},{1})", fpl.TenCode, ((DateTime)fpl.PayDatetime).ToString("yyyy/MM/dd HH:mm:ss"));
                        }
                        else
                        {
                            //檢查是否已由 手機->POS 流程兌換過了
                            var pos = OrderFacade.FamiposBarcodeGet(fpl.PeztempId);

                            if (pos.IsLoaded && pos.Status == (byte)FamiposBarcodeStatus.TranComplete)
                            {
                                returnCode = ApiReturnCode.InputInvalid;
                                message = string.Format("此筆資料已兌換過({0},{1},{2})", pos.TmTenName, pos.MachineNo, pos.TmTranNo);
                            }
                            else
                            {
                                peztempId = fpl.PeztempId;
                                fpl.PayTenCode = tenCode;
                                fpl.MmkId = mmkId;
                                fpl.SerialNo = serialNo;
                                fpl.CdFmcode = cdFmcode;
                                fpl.FirmNo = firmNo;
                                fpl.EventNo = eventNo;
                                fpl.PayDate = payDate;
                                fpl.PayTime = payTime;
                                fpl.RtnUrl = rtnUrl;
                                fpl.TraceUrl = traceUrl;
                                fpl.ErrUrl = errUrl;
                                fpl.Data1 = data1;
                                fpl.Data2 = data2;
                                fpl.Data3 = data3;

                                //Cnvert 付款時間為 DateTime 格式
                                if (payDate.Length >= 8 && payTime.Length >= 6)
                                {
                                    DateTime payDateTime;
                                    if (DateTime.TryParse(string.Format("{0}/{1}/{2} {3}:{4}:{5}", payDate.Substring(0, 4)
                                        , payDate.Substring(4, 2), payDate.Substring(6, 2), payTime.Substring(0, 2)
                                        , payTime.Substring(2, 2), payTime.Substring(4, 2)), out payDateTime))
                                    {
                                        fpl.PayDatetime = payDateTime;
                                    }
                                }

                                if (OrderFacade.SetFamiportPeztempLink(fpl, true) != 1)
                                {
                                    returnCode = ApiReturnCode.InputError;
                                    message = string.Format("Update FamiportPeztempLink Error. tranNo:{0}", tranNo);
                                }
                                else if (fpl.PayDatetime == null)
                                {
                                    returnCode = ApiReturnCode.InputInvalid;
                                    message = string.Format("PayDate:{0}, PayTime:{1}", payDate, payTime);
                                }
                                else
                                {
                                    returnCode = ApiReturnCode.Success;
                                    message = "Update FamiportPeztempLink Success.";
                                }
                            }
                        }
                    }
                }
                else
                {
                    returnCode = ApiReturnCode.InputError;
                    message = string.Format("Wrong MMK_ID:{0}, tranNo:{1}", mmkId, tranNo);
                }

                rtnObject.Code = returnCode;
                rtnObject.Message = message;

                FamiLog(tranNo, channel, System.Reflection.MethodBase.GetCurrentMethod().Name
                    , new FamiApiLogModel
                    {
                        TenCode = tenCode,
                        MmkId = mmkId,
                        EventId = eventNo,
                        PayDate = payDate,
                        PayTime = payTime
                    }, returnCode.ToString(), message);

                if (config.FamiLogSendDiscountEvent)
                {
                    DiscountEventLog(tranNo, channel, MethodBase.GetCurrentMethod().Name, peztempId, true);
                    DiscountEventLog(tranNo, channel, MethodBase.GetCurrentMethod().Name, peztempId, false);
                }

                var sendDiscountAtVerified = CheckFamiSendDiscountCodeAtVerified();
                var sendDiscountAtAccumulatedVerified = IsSendDiscountAtAccumulatedVerifiedExist();

                if (rtnObject.Code == ApiReturnCode.Success && peztempId != 0 
                    && (sendDiscountAtVerified || sendDiscountAtAccumulatedVerified))
                {
                    Peztemp pt = OrderFacade.PeztempGet(peztempId);
                    if (!pt.IsLoaded) return new { StatusCode = returnCode, Message = message };
                    var userId = OrderFacade.PeztempGetUsingMemberUserId(pt);
                    if (userId == 0) return new { StatusCode = returnCode, Message = message };

                    if (sendDiscountAtVerified)
                    {
                        SendDiscount(userId, config.FamiVerifiedDiscountCampaignId, tranNo, channel
                            , MethodBase.GetCurrentMethod().Name);
                    }

                    if (sendDiscountAtAccumulatedVerified && CheckUserAccumulatedVerifiedCount(userId))
                    {
                        SendDiscount(userId, config.FamiAccumulatedVerifiedDiscountCampaignId, tranNo, channel
                            , MethodBase.GetCurrentMethod().Name);
                    }
                }
                
                return new { StatusCode = returnCode, Message = message };
            }
            catch (Exception ex)
            {
                FamiLog(tranNo, channel, System.Reflection.MethodBase.GetCurrentMethod().Name
                    , new FamiApiLogModel
                    {
                        ExMsg = ex.Message,
                        StackTrace = ex.StackTrace,
                        TenCode = tenCode,
                        MmkId = mmkId,
                        EventId = eventNo,
                        PayDate = payDate,
                        PayTime = payTime
                    }, "Exception", ex.Message);
                throw new HttpErrorResponseException("B001", "執行階段發生錯誤");
            }
        }

        /// <summary>
        /// 更新Peztemp為已列印 (當famiport列印小白單)
        /// </summary>
        /// <param name="pezCode">peztemp pezCode</param>
        /// <param name="tranNo">全家交易序號</param>
        /// <param name="tenCode">店別代號</param>
        /// <param name="mmkId">服務項目代號</param>
        /// <param name="mmkIdName">服務項目名稱</param>
        /// <returns></returns>
        [HttpGet]
        public dynamic UpdateFamiCodeStatus(string pezCode, string tranNo, string tenCode, string mmkId, string mmkIdName)
        {
            try
            {
                var rtnObject = new ApiReturnObject();
                ApiReturnCode returnCode;
                string message;

                const int serviceCode = (int)PeztempServiceCode.Famiport;
                var pez = OrderFacade.PeztempCheck(pezCode, serviceCode);

                if (pez.IsLoaded && pez.Bid.HasValue)
                {
                    if (pez.IsUsed)
                    {
                        returnCode = ApiReturnCode.InputInvalid;
                        message = "序號已使用";
                    }
                    else
                    {
                        //Update Peztemp
                        pez.IsUsed = true;
                        pez.Remark = tenCode;
                        pez.UsedTime = DateTime.Now;
                        OrderFacade.SetPezTemp(pez);

                        //Insert FamiportPeztempLink
                        var fpl = new FamiportPeztempLink
                        {
                            PeztempId = pez.Id,
                            TranNo = tranNo,
                            TenCode = tenCode,
                            MmkId = mmkId,
                            MmkIdName = mmkIdName,
                            CreateTime = DateTime.Now
                        };
                        OrderFacade.SetFamiportPeztempLink(fpl);

                        returnCode = ApiReturnCode.Success;
                        message = "Success";
                    }
                }
                else
                {
                    returnCode = ApiReturnCode.DataNotExist;
                    message = "序號於期間內查無資料";
                }

                rtnObject.Code = returnCode;
                rtnObject.Data = message;
                FamiLog(tranNo, FamiChannel.Famiport, System.Reflection.MethodBase.GetCurrentMethod().Name
                    , new FamiApiLogModel
                    {
                        PinCode = pezCode,
                        TenCode = tenCode,
                        MmkId = mmkId
                    }, returnCode.ToString(), message);
                return new { StatusCode = returnCode, Message = message };
            }
            catch (Exception ex)
            {
                FamiLog(tranNo, FamiChannel.Famiport, System.Reflection.MethodBase.GetCurrentMethod().Name
                    , new FamiApiLogModel
                    {
                        PinCode = pezCode,
                        TenCode = tenCode,
                        MmkId = mmkId,
                        ExMsg = ex.Message,
                        StackTrace = ex.StackTrace
                    }, "Exception", ex.Message);
                throw new HttpErrorResponseException("B001", "執行階段發生錯誤");
            }
        }

        /// <summary>
        /// 確認手機顯示的barcode交易序號是否存在
        /// </summary>
        /// <param name="tranNo">交易序號</param>
        /// <param name="pezcode">Peztemp.pezcode</param>
        /// <param name="famiportEventId">Famiport.eventid</param>
        /// <param name="barcode1"></param>
        /// <param name="barcode2"></param>
        /// <param name="barcode3"></param>
        /// <param name="barcode4"></param>
        /// <param name="barcode5"></param>
        /// <param name="mmkId"></param>
        /// <param name="tenCode"></param>
        /// <param name="payDate"></param>
        /// <param name="payDttm"></param>
        /// <param name="tmTenName"></param>
        /// <param name="machineNo"></param>
        /// <param name="tmTranNo"></param>
        /// <returns></returns>
        [HttpGet]
        public dynamic CheckFamiBarcode(string tranNo, string pezcode, string famiportEventId
            , string barcode1, string barcode2, string barcode3, string barcode4, string barcode5
            , string mmkId, string tenCode, string payDate, string payDttm, string tmTenName, string machineNo, string tmTranNo)
        {
            string peztempCode = pezcode;

            try
            {
                var rtnObject = new ApiReturnObject();
                ApiReturnCode returnCode;
                string message;
                var fami = OrderFacade.FamiposBarcodeGet(tranNo);

                if (!fami.IsLoaded)
                {
                    returnCode = ApiReturnCode.DataNotExist;
                    message = "交易序號資料不存在";
                }
                else
                {
                    if (fami.Status == (byte)FamiposBarcodeStatus.TranComplete)
                    {
                        returnCode = ApiReturnCode.InputInvalid;
                        message = string.Format("此筆資料已兌換過({0},{1},{2})", fami.TmTenName, fami.MachineNo, fami.TmTranNo);
                    }
                    else if (fami.Barcode1 != barcode1 || fami.Barcode2 != barcode2 || fami.Barcode3 != barcode3)
                    {
                        returnCode = ApiReturnCode.InputInvalid;
                        message = "輸入的barcode不存在";
                    }
                    else
                    {
                        var fpl = OrderFacade.GetFamiportPeztempLink(fami.PeztempId);
                        if (fpl.IsLoaded && fpl.PayDatetime != null)
                        {
                            returnCode = ApiReturnCode.InputInvalid;
                            message = string.Format("此筆資料已透過Famiport兌換過(店別:{0}, {1})", fpl.TenCode, ((DateTime)fpl.PayDatetime).ToString("yyyy/MM/dd HH:mm:ss"));
                        }
                        else
                        {

                            var peztemp = OrderFacade.PeztempGet(fami.PeztempId);
                            var famiport = OrderFacade.FamiportGet(fami.FamiportId);

                            if (!peztemp.IsLoaded || !famiport.IsLoaded)
                            {
                                returnCode = ApiReturnCode.DataNotExist;
                                message = string.Format("{0}不存在({1}, {2})", tranNo, (peztemp.IsLoaded) ? "活動Id" : "Pin碼Id", (peztemp.IsLoaded) ? fami.FamiportId : fami.PeztempId);
                            }
                            else
                            {
                                peztempCode = peztemp.PezCode;
                                if (famiport.EventId.Substring(famiport.EventId.Length - 10, 10) != Convert.ToString((int)PeztempServiceCode.Famiport).PadLeft(3, '0') + famiportEventId)
                                {
                                    returnCode = ApiReturnCode.DataNotExist;
                                    message = string.Format("{0}不存在({1}, {2})", tranNo, "活動編號", famiportEventId);
                                }
                                else
                                {
                                    fami.MmkId = mmkId;
                                    fami.TenCode = tenCode;
                                    fami.ConfirmPayDate = payDate;
                                    fami.ConfirmPayDttm = payDttm;
                                    fami.TmTenName = tmTenName;
                                    fami.MachineNo = machineNo;
                                    fami.TmTranNo = tmTranNo;
                                    fami.Barcode4 = barcode4;
                                    fami.Barcode5 = barcode5;
                                    fami.Status = (byte)FamiposBarcodeStatus.ConfirmDeal;
                                    fami.UpdateConfirmPayTime = DateTime.Now;

                                    //Cnvert 付款時間為 DateTime 格式
                                    if (payDate.Length >= 8 && payDttm.Length >= 6)
                                    {
                                        DateTime confirmDateTime;
                                        if (DateTime.TryParse(string.Format("{0}/{1}/{2} {3}:{4}:{5}", payDate.Substring(0, 4)
                                            , payDate.Substring(4, 2), payDate.Substring(6, 2), payDttm.Substring(0, 2)
                                            , payDttm.Substring(2, 2), payDttm.Substring(4, 2)), out confirmDateTime))
                                        {
                                            fami.ConfirmPayDatetime = confirmDateTime;
                                        }
                                    }

                                    if (OrderFacade.FamiposBarcodeSet(fami))
                                    {
                                        returnCode = ApiReturnCode.Success;
                                        message = "Success";
                                    }
                                    else
                                    {
                                        returnCode = ApiReturnCode.Error;
                                        message = "資料更新失敗";
                                    }
                                }
                            }
                        }
                    }
                }

                rtnObject.Code = returnCode;
                rtnObject.Data = message;
                FamiLog(tranNo, FamiChannel.FamiPos, System.Reflection.MethodBase.GetCurrentMethod().Name
                    , new FamiApiLogModel
                    {
                        PinCode = peztempCode,
                        TenCode = tenCode,
                        EventId = famiportEventId,
                        MmkId = mmkId,
                        PayDate = payDate,
                        PayTime = payDttm
                    }, returnCode.ToString(), message);
                return new { StatusCode = returnCode, Message = message };
            }
            catch (Exception ex)
            {
                FamiLog(tranNo, FamiChannel.FamiPos, System.Reflection.MethodBase.GetCurrentMethod().Name
                    , new FamiApiLogModel
                    {
                        PinCode = peztempCode,
                        TenCode = tenCode,
                        EventId = famiportEventId,
                        MmkId = mmkId,
                        PayDate = payDate,
                        PayTime = payDttm,
                        ExMsg = ex.Message,
                        StackTrace = ex.StackTrace
                    }, "Exception", ex.Message);
                throw new HttpErrorResponseException("B001", "執行階段發生錯誤");
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage FamiMap()
        {
            //querystring is encoded in big5
            var decode_collection = HttpUtility.ParseQueryString(HttpUtility.UrlDecode(Request.RequestUri.Query, Encoding.GetEncoding("big5")));

            string cvsspot = decode_collection.Get("cvsspot");
            string name = decode_collection.Get("name");
            string addr = decode_collection.Get("addr");
            string tel = decode_collection.Get("tel");
            string cvsname = decode_collection.Get("cvsname");
            //傳入的ticketid
            string cvstemp = decode_collection.Get("cvstemp");

            //log for test
            CommonFacade.AddAudit("familymap", AuditType.Api, string.Format("{0},{1},{2},{3},{4},{5}"
                , cvsspot, name, addr, tel, cvsname, cvstemp), config.TmallDefaultUserName, false);

            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Moved);
            //redirect to payment step
            response.Headers.Location = new Uri(config.SiteUrl.Replace("http", "https") + "/ppon/buy.aspx?TicketId=" + cvstemp);
            return response;
        }

        #region FamiGroupCoupon

        [HttpGet]
        [RequireHttps]
        public string GetPingcodeD(string code)
        {
            return FamiGroupCoupon.DecryptPincode(code);
        }

        [HttpGet]
        [RequireHttps]
        public string GetPingcodeEUrl(string code)
        {
            return HttpUtility.UrlEncode(FamiGroupCoupon.EncryptPincode(code));
        }

        [HttpGet]
        [RequireHttps]
        public string GetPingcodeE(string code)
        {
            return FamiGroupCoupon.EncryptPincode(code);
        }

        [HttpGet]
        [RequireHttps]
        public string GetVerify(string code)
        {
            var p = new PincodeVerifyInput
            {
                AuthId = FamiGroupCoupon.GetFamiportAuthId(),
                Status = Convert.ToString((int)FamilyNetTransStatus.Code20),
            };

            var pez = fami.GetPeztemp(code);
            var pin = fami.GetFamilyNetPincodeByPeztempId(pez.Id);
            var trans = fami.GetFamilyNetPincodeTrans(pin.PincodeTransId);
            var evt = fami.GetFamilyNetEventById(trans.FamilyNetEventId);

            PincodeInputDetail det = new PincodeInputDetail
            {
                TranNo = DateTime.Now.ToString("yyyyMMddHHmmssfff"),
                SerialNo = "1",
                CdActCode = evt.ActCode,
                CdPinCode = FamiGroupCoupon.EncryptPincode(code),
                ItemCode = evt.ItemCode,
                PaymentTime = DateTime.Now.ToString("yyyyMMddHHmmss"),
                StoreCode = "004539",
                TmTransNo = "",
                Price = "45"
            };

            p.Detail.Add(det);
            JsonSerializer js = new JsonSerializer();
            var result = js.Serialize(p);
            result = result.Replace("\\", string.Empty);
            
            return result;
        }

        /// <summary>
        /// 全家(全網)Pincode換Barcode
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [RequireHttps]
        [OAuthScope(TokenScope.SystemManagement)]
        public ApiResult ExecuteOrderExg(ExecuteOrderExgInput input)
        {
            ApiResult result = new ApiResult();
            try
            {
                var pincode = FamiGroupCoupon.DecryptPincode(input.PezCodeEncode);
                var pez = fami.GetPeztemp(pincode);
                FamiGroupCoupon.FamiportPincodeOrderExg(pez, input.OrderGuid, input.CouponId);
                result.Code = ApiResultCode.Success;
            }
            catch (Exception ex)
            {
                result.Code = ApiResultCode.Error;
                result.Message = ex.Message;
            }

            return result;
        }

        /// <summary>
        /// 全家(全網)退Pincode
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [RequireHttps]
        [OAuthScope(TokenScope.SystemManagement)]
        public ApiResult ReturnPincode(FamiReturnPincodeInput input)
        {
            ApiResult result = new ApiResult
            {
                Code = ApiResultCode.DataNotFound
            };
            try
            {
                string returnMsg;
                result.Code = OrderFacade.FamilyNetPincodeReturn(input.OrderGuid, input.Bid, out returnMsg, FamilyNetReturnType.DealClosed) ? ApiResultCode.Success : ApiResultCode.Error;
                result.Message = returnMsg;
            }
            catch (Exception ex)
            {
                result.Code = ApiResultCode.Error;
                result.Message = ex.Message;
            }

            return result;
        }

        /// <summary>
        /// Mq Call 核銷全網Pincode
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [RequireHttps]
        [OAuthScope(TokenScope.SystemManagement)]
        public ApiResult ExecuteVerifyPincode(FamiVerifyPincodeMq q)
        {
            ApiResult result = new ApiResult
            {
                Code = ApiResultCode.DataNotFound
            };

            var verifyLog = fami.GetFamilyNetVerifyLog(q.VerifyLogId);
            UpdateVerifyLog(verifyLog, string.Empty, FamilyNetVerifyLogStatus.Executing);

            var pincode = fami.GetFamilyNetPincode(q.FamilyNetPincodeOrderNo);
            if (pincode == null)
            {
                UpdateVerifyLog(verifyLog, "pincode is not found 2.", FamilyNetVerifyLogStatus.Fail);
                result.Code = ApiResultCode.InvalidCoupon;
                return result;
            }

            if (pincode.IsVerified)
            {
                UpdateVerifyLog(verifyLog, "pincode is Verified 2.", FamilyNetVerifyLogStatus.Completed);
                result.Code = ApiResultCode.Success;
                return result;
            }

            var detail = fami.GetFamilyNetPincodeDetail(pincode.OrderNo, int.Parse(verifyLog.SerialNo), verifyLog.ItemCode);
            if (detail == null)
            {
                UpdateVerifyLog(verifyLog, "barcode is not found.", FamilyNetVerifyLogStatus.Fail);
                result.Code = ApiResultCode.InvalidCoupon;
                return result;
            }

            var pez = fami.GetPeztemp(q.PeztempId);
            if (pez == null)
            {
                UpdateVerifyLog(verifyLog, "peztemp is not fund 2.", FamilyNetVerifyLogStatus.Fail);
                result.Code = ApiResultCode.InvalidCoupon;
                return result;
            }

            pincode.IsVerified = true;
            pincode.IsLock = false;
            pincode.StoreCode = verifyLog.StoreCode;
            pincode.IsVerifying = false;
            
            detail.PaymentTime = verifyLog.PaymentTime;
            detail.StoreCode = verifyLog.StoreCode;
            detail.TmTransNo = verifyLog.TmTransNo;
            detail.Price = verifyLog.Price;
            detail.TranNo = verifyLog.TranNo;

            string remark;
            if (OrderFacade.SetFamilyNetPincodeVerify(pincode, detail, pez, verifyLog.IpAddress, out remark))
            {
                UpdateVerifyLog(verifyLog, remark, FamilyNetVerifyLogStatus.Completed);
                result.Code = ApiResultCode.Success;
            }
            else
            {
                UpdateVerifyLog(verifyLog, remark, FamilyNetVerifyLogStatus.Fail);
                result.Code = ApiResultCode.CouponPartialFail;
            }

            return result;
        }

        /// <summary>
        /// 重取barcode api
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [RequireHttps]
        [OAuthScope(TokenScope.SystemManagement)]
        public ApiResult RegetBarcode()
        {
            ApiResult result = new ApiResult
            {
                Code = ApiResultCode.Success
            };
            int executeCnt = 0;

            IMessageQueueProvider mqp = ProviderFactory.Instance().GetProvider<IMessageQueueProvider>();
            var data = fami.GetFamilyNetPincodeRegetBarcode();
            foreach (var d in data)
            {
                d.BarcodeReget = false;
                d.BarcodeRegetTime = DateTime.Now;
                d.Status = (byte) FamilyNetPincodeStatus.Fail;
                d.ReplyCode = string.Empty;
                d.OrderTicket = fami.GetFamilyNetOrderTicket(FamilyNetApi.OrderExg);
                var pez = fami.GetPeztemp(d.PeztempId);
                var famiMq = new FamiGroupCouponMq
                {
                    PeztempCode = FamiGroupCoupon.EncryptPincode(pez.PezCode),
                    OrderGuid = d.OrderGuid,
                    CouponId = d.CouponId
                };

                if (fami.UpdateFamilyNetPincode(d))
                {
                    mqp.Send(FamiGroupCouponMq._QUEUE_NAME, famiMq);
                    executeCnt++;
                }
            }

            result.Data = executeCnt;
            return result;
        }

        /// <summary>
        /// 全網Call 17Life核銷
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        [HttpPost]
        [RequireHttps]
        public PincodeVerifyResult VerifyPincode(FormDataCollection form)
        {
            var js = new JsonSerializer();
            var jsonInfo = form["Json_Info"];
            logger.Info(jsonInfo);
            jsonInfo = jsonInfo.Replace("\\", string.Empty);
            PincodeVerifyInput input = js.Deserialize<PincodeVerifyInput>(jsonInfo);
            var checkCode = CheckPincodeVerify(input);
            if (checkCode  != FamilyNetReplyCode.Code00)
            {
                var r = new PincodeVerifyResult();
                foreach (var d in input.Detail)
                {
                    var verifyLog = AddVerifyLog(d);
                    verifyLog.Remark = "pre check fail.";
                    r.Detail.Add(GetVerifyResultDetail(checkCode, d, input, verifyLog));
                }
                return r;
            }

            PincodeVerifyResult result = new PincodeVerifyResult();

            foreach (var d in input.Detail)
            {
                var verifyLog = AddVerifyLog(d);
                if (verifyLog.Id > 0)
                {
                    string code = string.Empty;
                    try
                    {
                        code = FamiGroupCoupon.DecryptPincode(verifyLog.CdPinCode);
                    }
                    catch (Exception ex)
                    {
                        UpdateVerifyLog(verifyLog, string.Format("Pincode Decode Error:{0}", ex.Message), FamilyNetVerifyLogStatus.Fail);
                        result.Detail.Add(GetVerifyResultDetail(FamilyNetReplyCode.Code81, d, input, verifyLog));
                        continue;
                    }

                    var pincode = fami.GetFamilyNetPincodeByPezCode(code);
                    if (pincode == null)
                    {
                        UpdateVerifyLog(verifyLog, "pincode is not found 1.", FamilyNetVerifyLogStatus.Fail);
                        result.Detail.Add(GetVerifyResultDetail(FamilyNetReplyCode.Code81, d, input, verifyLog));
                        continue;
                    }

                    verifyLog.PincodeOrderNo = pincode.OrderNo;
                    verifyLog.PincodeTranNo = pincode.TranNo;

                    if (pincode.ReturnStatus == (byte)FamilyNetPincodeReturnStatus.Completed || pincode.ReturnStatus == (byte)FamilyNetPincodeReturnStatus.Returning)
                    {
                        UpdateVerifyLog(verifyLog, "pincode is refunded.", FamilyNetVerifyLogStatus.Fail);
                        result.Detail.Add(GetVerifyResultDetail(FamilyNetReplyCode.Code33, d, input, verifyLog));
                        continue;
                    }

                    if (pincode.IsVerified)
                    {
                        UpdateVerifyLog(verifyLog, "pincode is Verified 1.", FamilyNetVerifyLogStatus.Completed);
                        result.Detail.Add(GetVerifyResultDetail(FamilyNetReplyCode.Code00, d, input, verifyLog));
                        continue;
                    }

                    if (verifyLog.Flag == (byte) FamilyNetPincodeDetailFlag.ReissuePinCode)
                    {
                        pincode.ReissuePin = true;
                    }
                    else
                    {                        
                        pincode.IsLock = pincode.IsVerifying = true;
                        pincode.LockTime = pincode.VerifyingTime = DateTime.Now;
                    }

                    fami.UpdateFamilyNetPincode(pincode);
                    result.Detail.Add(GetVerifyResultDetail(FamilyNetReplyCode.Code00, d, input, verifyLog));

                    if (verifyLog.Flag == (byte)FamilyNetPincodeDetailFlag.ReissuePinCode)
                    {
                        continue;
                    }

                    IMessageQueueProvider mqp = ProviderFactory.Instance().GetProvider<IMessageQueueProvider>();
                    var famiMq = new FamiVerifyPincodeMq
                    {
                        VerifyLogId = verifyLog.Id,
                        PeztempId = pincode.PeztempId,
                        FamilyNetPincodeOrderNo = pincode.OrderNo
                    };
                    mqp.Send(FamiVerifyPincodeMq._QUEUE_NAME, famiMq);
                }
                else
                {
                    result.Detail.Add(GetVerifyResultDetail(FamilyNetReplyCode.Code81, d, input, verifyLog));
                }
            }

            return result;
        }

        /// <summary>
        /// 取得兌換條碼
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [RequireHttps]
        [OAuthScope(TokenScope.Deal)]
        public ApiResult GetFamiGroupBarcode(GetFamiGroupBarCodeInput input)
        {
            var result = new ApiResult();
            result.Code = ApiResultCode.DataNotFound;
            var groupBarcodeList = new List<FamiGroupBarcode>();
            List<int> excludeCouponId = new List<int>();

            //filter gift active:1 濾掉正在送禮的
            var gift = mgmp.GiftGetByOid(input.OrderGuid).Where(x=>x.IsActive==true && x.SenderId==User.Identity.Id).ToList(); 
            if (gift.Any())
            {
                excludeCouponId.AddRange(gift.Select(g => g.CouponId ?? 0));
            }

            var barcodeData = fami.GetFamilNetVerifyBarcode(input.OrderGuid, input.ExchangeCount, excludeCouponId);
            if (!barcodeData.Any()) return result;
            
            foreach (var barcode in barcodeData)
            {
                var b = new FamiGroupBarcode
                {
                    CouponId = barcode.CouponId,
                    Pincode = barcode.PezCode,
                    Barcode = new List<string>()
                };
                if(input.CouponType == (int)DealCouponType.FamiSingleBarcode)
                {
                    if (!string.IsNullOrEmpty(barcode.Barcode2))
                    {
                        b.Barcode.Add(barcode.Barcode2);
                        groupBarcodeList.Add(b);
                    }
                }
                else
                {
                    b.Barcode.Add(barcode.Barcode1);
                    b.Barcode.Add(barcode.Barcode2);
                    b.Barcode.Add(barcode.Barcode3);
                    b.Barcode.Add(barcode.Barcode4);
                    groupBarcodeList.Add(b);
                }
            }
            
            fami.AddFamilyNetGetBarcodeLog(barcodeData, User.Identity.Id, input.OrderGuid, input.ExchangeCount);            
            result.Code = ApiResultCode.Success;
            result.Data = groupBarcodeList;
            return result;
        }

        
        /// <summary>
        /// 確認兌換及鎖定
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [RequireHttps]
        [OAuthScope(TokenScope.Deal)]
        public ApiResult ConfirmExchangeAndLock(ConfirmExchangeAndLockInput input)
        {
            var result = new ApiResult
            {
                Code = ApiResultCode.Success
            };

            result.Data = FamiGroupCoupon.ConfirmExchangeAndLock(input.OrderGuid, input.CouponIdList);

            fami.AddFamilyNetLockLog(input.CouponIdList, User.Identity.Id, input.OrderGuid);
            SetApiLog(GetMethodName(MethodBase.GetCurrentMethod().Name), string.Empty, 
                new { input.CouponIdList }, new { result.Code, result.Data }, Helper.GetClientIP());

            return result;
        }

        /// <summary>
        /// 確認兌換及鎖定
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [RequireHttps]
        [OAuthScope(TokenScope.Deal)]
        public ApiResult MakeFamiECPayResult(ConfirmECPayInput input)
        {
            var result = new ApiResult();

            StringBuilder builder = new StringBuilder();
            builder.AppendLine(new JsonSerializer().Serialize(input));
            builder.AppendLine(input.TradeData.StoreName);

            ApiResultCode retCode = ApiResultCode.Ready;
            int userId = 0;

            TaishinECPayTradeResponse wptr = new TaishinECPayTradeResponse();
            try
            {
                Guid orderGuid = input.OrderGuid;
                builder.AppendLine(orderGuid.ToString());

                Order order = op.OrderGet(orderGuid);
                builder.AppendLine(order.IsLoaded.ToString());

                if (order != null && order.IsLoaded)
                {
                    builder.AppendLine("order > 0");
                    OrderDetailCollection orderDetails = op.OrderDetailGetList(order.Guid);
                    if (orderDetails.Count > 0)
                    {
                        builder.AppendLine("orderDetails > 0");
                        Peztemp pt = pp.PeztempGetByOrderDetailGuid(orderDetails.FirstOrDefault().Guid);
                        if (pt != null && pt.IsLoaded)
                        {
                            userId = OrderFacade.PeztempGetUsingMemberUserId(pt);

                            builder.AppendLine("pt > 0");
                            FamiposBarcode fami = OrderFacade.FamiposBarcodeGet(pt.Id);
                            if (fami != null && fami.IsLoaded)
                            {
                                builder.AppendLine("fami > 0");
                                decimal exchangePrice = 0;
                                //if (fami.Status == (byte)FamiposBarcodeStatus.TranComplete)
                                //{
                                    CashTrustLog ctl = member.CashTrustLogGetByOrderId(order.Guid);
                                    if (ctl != null && ctl.IsLoaded)
                                    {
                                        DealProperty dp = pp.DealPropertyGet(ctl.BusinessHourGuid ?? Guid.Empty);
                                        if (dp != null && dp.IsLoaded)
                                        {
                                            exchangePrice = dp.ExchangePrice;
                                        }
                                    }
                                    wptr.StoreName = fami.TmTenName;
                                    wptr.TradeAmount = exchangePrice;
                                    wptr.MerchantTradeDate = (fami.ConfirmPayDatetime ?? DateTime.MinValue).ToString("yyyyMMdd HHmmss") + " +0800";
                                    //wptr.TradeStatus = ((int)TaishinECPayBankRtnCode.Success).ToString();

                                    order.MobilePayType = (int)MobilePayType.E7LifePay;
                                    op.OrderSet(order);
                                //}
                            }
                        }
                    }
                }
                Guid _orderGuid = Guid.Empty;
                try
                {
                    Guid.TryParse(orderGuid.ToString(), out _orderGuid);
                }
                catch
                {

                }
                if(userId == 0)
                {
                    int.TryParse(input.MemberId, out userId);
                }
                #region 紀錄交易Log
                TaishinEcPayLog tsEcLog = new TaishinEcPayLog()
                {
                    StoreName = input.TradeData.StoreName,
                    StoreTel = input.TradeData.StoreTel,
                    StoreAddress = input.TradeData.StoreAddress,
                    MerchantTradeDate = input.TradeData.MerchantTradeDate,
                    TradeAmount = Convert.ToInt32(input.TradeData.TradeAmount),
                    CardNumber = input.TradeData.CardNumberShelter,
                    CardName = input.TradeData.CardName,
                    TradeStatus = input.TradeData.TradeStatus,
                    TradeStatusName = input.TradeData.TradeStatusName,
                    CreateTime = DateTime.Now,
                    UserId = userId,
                    OrderGuid = _orderGuid,
                };
                op.TaishinEcPayLogSet(tsEcLog);
                #endregion 紀錄交易Log

                retCode = ApiResultCode.Success;
            }
            catch 
            {
                if (retCode == ApiResultCode.Ready)
                {
                    retCode = ApiResultCode.Error;
                }
            }
            finally
            {
                //WpTradeLog wplog = new WpTradeLog();
                //wplog.Guid = Guid.NewGuid();
                //wplog.CardToken = Guid.NewGuid().ToString();
                //wplog.TradeType = "MakeFamiECPayResult";
                //wplog.ContentLog = builder.ToString();
                //wplog.CreateTime = DateTime.Now;

                //wpp.WpTradeLogSet(wplog);
            }

            result.Code = (ApiResultCode)retCode;
            result.Data = wptr;
            return result;
        }

        private void UpdateVerifyLog(FamilyNetVerifyLog verifyLog, string remark, FamilyNetVerifyLogStatus status)
        {
            if (status == FamilyNetVerifyLogStatus.Executing)
            {
                verifyLog.RetryCount++;
            }
            verifyLog.Remark = remark;
            verifyLog.Status = (byte)status;
            fami.UpdateFamilyNetVerifyLog(verifyLog);
        }

        private FamilyNetVerifyLog AddVerifyLog(PincodeInputDetail d)
        {
            int price;
            int.TryParse(d.Price, out price);

            var verifyLog = new FamilyNetVerifyLog
            {
                TranNo = d.TranNo,
                SerialNo = d.SerialNo,
                CdActCode = d.CdActCode,
                CdPinCode = d.CdPinCode,
                ItemCode = d.ItemCode,
                PaymentTime = d.PaymentTime,
                StoreCode = d.StoreCode,
                TmTransNo = d.TmTransNo,
                Price = price,
                CreateTime = DateTime.Now,
                Status = (byte)FamilyNetVerifyLogStatus.Init,
                IpAddress = Helper.GetClientIP(),
                Flag = Convert.ToByte(d.Flag)
            };

            if (verifyLog.Flag == (byte) FamilyNetPincodeDetailFlag.ReissuePinCode)
            {
                verifyLog.Remark = "全網卡紙補發通知(非核銷)";
                verifyLog.Status = (byte)FamilyNetVerifyLogStatus.ReissuePin;
            }

            return fami.AddFamilyNetVerifyLog(verifyLog);
        }

        private VerifyResultDetail GetVerifyResultDetail(FamilyNetReplyCode code, PincodeInputDetail detail, PincodeVerifyInput input, FamilyNetVerifyLog log)
        {
            log.ReplyCode = (byte)code;
            log.ReplyDesc = Helper.GetEnumDescription(code);
            log.ReplyTime = DateTime.Now;
            fami.UpdateFamilyNetVerifyLog(log);

            return new VerifyResultDetail
            {
                TranNo = detail.TranNo,
                SerialNo = detail.SerialNo,
                Status = input.Status,
                ReplyTime = ((DateTime)log.ReplyTime).ToString("yyyyMMddHHmmss"),
                ReplyCode = ((int)code).ToString().PadLeft(2, '0'),
                ReplyDesc = log.ReplyDesc
            };
        }

        private FamilyNetReplyCode CheckPincodeVerify(PincodeVerifyInput input)
        {
            if (input.AuthId != FamiGroupCoupon.GetFamiportAuthId())
            {
                return FamilyNetReplyCode.Code30;
            }
            if (int.Parse(input.Status) != (int) FamilyNetTransStatus.Code20)
            {
                return FamilyNetReplyCode.Code32;
            }
            if (!input.Detail.Any())
            {
                return FamilyNetReplyCode.Code81;
            }
            return FamilyNetReplyCode.Code00;
        }

        #endregion

        private void DiscountEventLog(string tranNo, FamiChannel channel, string methodName, int peztempId, bool isAccumulated)
        {
            string log;
            bool checkResult;

            if (isAccumulated)
            {
                checkResult = IsSendDiscountAtAccumulatedVerifiedExist();
                log = string.Format(
                    "pid:{0},CodeAtVerified:{1},VerifiedCount:{2},CampaignId:{3},sd:{4},ed:{5},checkD:{6},dns:{7}"
                    , peztempId, config.FamiSendDiscountCodeAtVerified, config.FamiAccumulatedVerifiedCount
                    , config.FamiAccumulatedVerifiedDiscountCampaignId
                    , config.FamiAccumulatedVerifiedDiscountStartDate.ToString("yyyy/MM/dd HH:mm:ss")
                    , config.FamiAccumulatedVerifiedDiscountEndDate.ToString("yyyy/MM/dd HH:mm:ss")
                    , NowIsBetween(config.FamiAccumulatedVerifiedDiscountStartDate, config.FamiAccumulatedVerifiedDiscountEndDate)
                    , Dns.GetHostName());
            }
            else
            {
                checkResult = CheckFamiSendDiscountCodeAtVerified();
                log = string.Format(
                    "pid:{0},CodeAtVerified:{1},CampaignId:{2},sd:{3},ed:{4},checkD:{5},dns:{6}"
                    , peztempId, config.FamiSendDiscountCodeAtVerified
                    , config.FamiVerifiedDiscountCampaignId
                    , config.FamiVerifiedDiscountStartDate.ToString("yyyy/MM/dd HH:mm:ss")
                    , config.FamiVerifiedDiscountEndDate.ToString("yyyy/MM/dd HH:mm:ss")
                    , NowIsBetween(config.FamiVerifiedDiscountStartDate, config.FamiVerifiedDiscountEndDate)
                    , Dns.GetHostName());
            }

            FamiLog(tranNo, channel, methodName, log, string.Format("CheckDiscountEvent({0})", checkResult)
                , tranNo);
        }

        /// <summary>
        /// 是否有每次核銷成功後送折價券活動
        /// </summary>
        /// <returns></returns>
        private bool CheckFamiSendDiscountCodeAtVerified()
        {
            if (!config.FamiSendDiscountCodeAtVerified) return false;
            if (config.FamiVerifiedDiscountCampaignId == 0) return false;
            if (!NowIsBetween(config.FamiVerifiedDiscountStartDate, config.FamiVerifiedDiscountEndDate)) return false;
            
            return true;
        }

        /// <summary>
        /// 是否有累積核銷次數後送折價券活動
        /// </summary>
        /// <returns></returns>
        private bool IsSendDiscountAtAccumulatedVerifiedExist()
        {
            if (!config.FamiSendDiscountCodeAtVerified) return false;
            if (config.FamiAccumulatedVerifiedCount <= 1) return false;
            if (config.FamiAccumulatedVerifiedDiscountCampaignId == 0) return false;
            if (!NowIsBetween(config.FamiAccumulatedVerifiedDiscountStartDate, config.FamiAccumulatedVerifiedDiscountEndDate)) return false;

            return true;
        }

        /// <summary>
        /// 計算累計核銷次數後判斷是否符合活動
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        private bool CheckUserAccumulatedVerifiedCount(int userId)
        {
            if (IsSendDiscountAtAccumulatedVerifiedExist())
            {
                var verifiedCount = member.CashTrustLogGetFamiDealVerifiedCount(userId, config.FamiAccumulatedVerifiedDiscountStartDate,
                    config.FamiAccumulatedVerifiedDiscountEndDate);

                return verifiedCount > 0 && verifiedCount % config.FamiAccumulatedVerifiedCount == 0;
            }

            return false;
        }

        /// <summary>
        /// 發送即時折價券
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="campaignId"></param>
        /// <param name="tranNo"></param>
        /// <param name="channel"></param>
        /// <param name="methodName"></param>
        private void SendDiscount(int userId, int campaignId, string tranNo, FamiChannel channel, string methodName)
        {
            //發送折價券
            int amount, minAmount, discountCodeId;
            string discountCode;
            DateTime? codeEndDate;

            var result =
                PromotionFacade.InstantGenerateDiscountCode(campaignId, userId, false
                    , out discountCode, out amount, out codeEndDate, out discountCodeId, out minAmount);

            FamiLog(tranNo, channel, methodName, string.Format("{0},{1}", userId, Dns.GetHostName()), string.Format("sendDiscount({0}):{1}", campaignId, result)
                , discountCodeId.ToString());
        }

        private bool NowIsBetween(DateTime start, DateTime end)
        {
            var now = DateTime.Now;
            if (start <= now && now <= end)
            {
                return true;
            }
            return false;
        }
    }
}

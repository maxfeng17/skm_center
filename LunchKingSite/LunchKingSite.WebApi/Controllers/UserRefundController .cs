﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.BizLogic.Model.Refund;
using LunchKingSite.BizLogic.Models.CustomerService;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebApi.Core.OAuth;
using LunchKingSite.WebApi.Core.Attributes;
using Helper = LunchKingSite.Core.Helper;

namespace LunchKingSite.WebApi.Controllers
{
    /// <summary>
    /// 退貨相關 API
    /// </summary>
    public class UserRefundController : BaseController
    {
        private static IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();

        /// <summary>
        /// 取得銀行資料
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [OAuthScope(TokenScope.Deal)]
        public ApiResult BankInfoGetMainList()
        {
            ApiResult result = new ApiResult();
            result.Code = ApiResultCode.Success;
            result.Data = UserRefundFacade.BankInfoGetMainList();
            return result;
        }

        /// <summary>
        /// 取得分行資料
        /// </summary>
        /// <param name="bankNo"></param>
        /// <returns></returns>
        [HttpGet]
        [OAuthScope(TokenScope.Deal)]
        public ApiResult BankInfoGetBranchList(string bankNo)
        {
            ApiResult result = new ApiResult();
            result.Code = ApiResultCode.Success;
            result.Data = UserRefundFacade.BankInfoGetBranchList(bankNo);
            return result;
        }

        /// <summary>
        /// 取得預設退貨原因
        /// </summary>
        /// <param name="isToShopOrder">憑證訂單才需顯示</param>
        /// <returns></returns>
        [HttpGet]
        [OAuthScope(TokenScope.Deal)]
        public ApiResult GetRefundReason(bool isToShopOrder)
        {
            ApiResult result = new ApiResult();
            result.Code = ApiResultCode.Success;
            result.Data = UserRefundFacade.RefundReason(isToShopOrder);
            return result;
        }

        /// <summary>
        /// 訂單明細是否顯示退貨鈕(Web邏輯)，已退購物金可轉退現金
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [RequireHttps]
        [OAuthScope(TokenScope.Deal)]
        public ApiResult IsShowRefundButton(OrderGuidModel model)
        {
            ApiResult result = new ApiResult();

            string userName = HttpContext.Current.User.Identity.Name;
            if (string.IsNullOrWhiteSpace(userName))
            {
                result.Code = ApiResultCode.Error;
                result.Message = I18N.Phrase.ApiReturnCodeUserNoSignIn;
                return result;
            }

            string failedReason;
            var isAllow = OrderFacade.CanApplyRefund(model.OrderGuid, userName, true, out failedReason);
            result.Code = ApiResultCode.Success;
            result.Data = new { isShow = isAllow, failedReason };

            return result;
        }

        /// <summary>
        /// 訂單明細是否顯示退貨鈕(App邏輯)，已退購物金不可再轉退現金
        /// </summary>
        /// <param name="model"></param>
        [HttpPost]
        [RequireHttps]
        [OAuthScope(TokenScope.Deal)]
        public ApiResult IsAllowRefund(OrderIdModel model)
        {
            ApiResult result = new ApiResult();
            result.Code = ApiResultCode.Success;

            #region 登入驗證

            string userName = HttpContext.Current.User.Identity.Name;
            if (string.IsNullOrWhiteSpace(userName))
            {
                result.Code = ApiResultCode.Error;
                result.Message = I18N.Phrase.ApiReturnCodeUserNoSignIn;
                return result;
            }

            #endregion

            var orderInfo = UserRefundFacade.GetOrderInfo(MemberFacade.GetUniqueId(userName), model.OrderId);

            string failedReason;
            var isAllowRefund = UserRefundFacade.IsAllowRefund(orderInfo, out failedReason);

            result.Data = new { isAllowRefund, failedReason };
            return result;
        }

        /// <summary>
        /// 取得訂單可退方式
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [RequireHttps]
        [OAuthScope(TokenScope.Deal)]
        public ApiResult GetApiRefundTypeList(OrderIdModel model)
        {
            ApiResult result = new ApiResult();

            #region 登入驗證

            string userName = HttpContext.Current.User.Identity.Name;
            if (string.IsNullOrWhiteSpace(userName))
            {
                result.Code = ApiResultCode.Error;
                result.Message = I18N.Phrase.ApiReturnCodeUserNoSignIn;
                return result;
            }

            #endregion

            var apiRefundTypeList = new List<ApiRefundType>();
            var orderInfo = UserRefundFacade.GetOrderInfo(MemberFacade.GetUniqueId(userName), model.OrderId);
            var viewCouponListMain = orderInfo.Coupons.FirstOrDefault();

            if (orderInfo.IsPaidByIsp)
            {
                //超取超付
                if (viewCouponListMain != null)
                {
                    if (Helper.IsFlagSet(viewCouponListMain.OrderStatus, (int)OrderStatus.Complete))
                    {
                        //超取超付已取貨
                        apiRefundTypeList.Add(ApiRefundType.Atm);
                    }
                    else if (!viewCouponListMain.IsCanceling && !Helper.IsFlagSet(viewCouponListMain.OrderStatus, (int)OrderStatus.Cancel))
                    {
                        if (config.EnableCancelPaidByIspOrder)
                        {
                            //未取消
                            apiRefundTypeList.Add(ApiRefundType.IspUnpaid);
                            apiRefundTypeList.Add(ApiRefundType.Scash);
                        }
                    }
                }
            }
            else
            {
                apiRefundTypeList.Add(ApiRefundType.Scash);
                if (orderInfo.IsPaidByCash)
                {
                    var isAtmRefund = ReturnService.IsAtmRefund(orderInfo);
                    apiRefundTypeList.Add(isAtmRefund ? ApiRefundType.Atm : ApiRefundType.Cash);
                }
            }

            result.Data = new { apiRefundTypeList };
            result.Code = ApiResultCode.Success;

            return result;
        }

        /// <summary>
        /// 申請退貨
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [RequireHttps]
        [OAuthScope(TokenScope.Deal)]
        public ApiResult ConfirmRefund(ApiRefundModel model)
        {
            ApiResult result = new ApiResult();
            result.Code = ApiResultCode.Success;

            #region 登入驗證

            string userName = HttpContext.Current.User.Identity.Name;
            if (string.IsNullOrWhiteSpace(userName))
            {
                result.Code = ApiResultCode.Error;
                result.Message = I18N.Phrase.ApiReturnCodeUserNoSignIn;
                return result;
            }

            #endregion
            var userId = MemberFacade.GetUniqueId(userName);
            var orderInfo = UserRefundFacade.GetOrderInfo(userId, model.OrderId);

            var maskedModel = new ApiRefundModel(model, true);

            #region Step1.驗證是否可退

            string failedReason;
            var isAllowRefund = UserRefundFacade.IsAllowRefund(orderInfo, out failedReason);
            if (!isAllowRefund)
            {
                result.Data = new ApiRefundResultModel { IsSuccess = false, IsShowCreditNote = false, FailedReason = failedReason };
                SetApiLog("ConfirmRefund", userName, maskedModel, result, Helper.GetClientIP());
                return result;
            }

            if (orderInfo.IsIsp && model.RefundReciever == null)
            {
                failedReason = "為了更快速地完成退貨流程，請將APP更新至最新版本。或者，可先至17life網站上申請退貨，謝謝。";
                result.Data = new ApiRefundResultModel { IsSuccess = false, IsShowCreditNote = false, FailedReason = failedReason };
                SetApiLog("ConfirmRefund", userName, maskedModel, result, Helper.GetClientIP());
                return result;
            }
            
            #endregion

            #region Step2.進行退貨

            #region 超商取貨未付款退貨

            if (model.ApiRefundType == (int)ApiRefundType.IspUnpaid)
            {
                var viewCouponListMain = orderInfo.Coupons.FirstOrDefault();
                if (viewCouponListMain != null)
                {
                    bool isSuccess = false;
                    Order order = op.OrderGet(viewCouponListMain.Guid);

                    //訂單非取消中狀態
                    if (string.IsNullOrEmpty(viewCouponListMain.PreShipNo))
                    {
                        //未出貨(未配編) 直接進行退紅利金、購物金
                        isSuccess = PaymentFacade.CancelPaymentTransactionByOrderForIncompleteOrder(order, "sys");
                        OrderFacade.SendOrderCancelMail(order.Guid, false, true);
                    }
                    else
                    {
                        //已出貨(已配編)
                        order.IsCanceling = true;
                        isSuccess = op.OrderSet(order);
                        OrderFacade.SendOrderCancelMail(order.Guid, true, false);
                    }
                    CommonFacade.AddAudit(order.Guid, AuditType.Order, "取消訂單申請(APP)", userName, false);

                    result.Data = new ApiRefundResultModel { IsSuccess = isSuccess, IsShowCreditNote = false, FailedReason = failedReason };
                    SetApiLog("ConfirmRefund", userName, maskedModel, result, Helper.GetClientIP());
                    return result;
                }
            }

            #endregion

            #region 申請退貨

            else if (orderInfo != null && orderInfo.Coupons.Count > 0)
            {
                ViewCouponListMain vclm = mp.GetCouponListMainByOid(orderInfo.OrderGuid);
                if (Helper.IsFlagSet(vclm.BusinessHourStatus, (int)BusinessHourStatus.GroupCoupon) &&
                    Helper.IsFlagSet(vclm.GroupOrderStatus.Value, (int)GroupOrderStatus.FamiDeal))
                {
                    //全家寄杯退貨
                    if (!OrderFacade.FamilyNetPincodeReturn(orderInfo.OrderGuid, vclm.BusinessHourGuid, out failedReason))
                    {
                        result.Data = new ApiRefundResultModel { IsSuccess = false, IsShowCreditNote = false, FailedReason = failedReason };
                        SetApiLog("ConfirmRefund", userName, maskedModel, result, Helper.GetClientIP());
                        return result;
                    }
                }
                else if (Helper.IsFlagSet(vclm.BusinessHourStatus, (int)BusinessHourStatus.GroupCoupon) &&
                         Helper.IsFlagSet(vclm.GroupOrderStatus.Value, (int)GroupOrderStatus.HiLifeDeal))
                {
                    //萊爾富寄杯
                    if (config.EnableHiLifeDealSetup && !OrderFacade.HiLifePincodeReturn(orderInfo.OrderGuid, out failedReason))
                    {
                        result.Data = new ApiRefundResultModel { IsSuccess = false, IsShowCreditNote = false, FailedReason = failedReason };
                        SetApiLog("ConfirmRefund", userName, maskedModel, result, Helper.GetClientIP());
                        return result;
                    }
                }
                
                OrderStatusLog log = new OrderStatusLog
                {
                    OrderGuid = orderInfo.OrderGuid,
                    Status = 0,
                    CreateId = userName,
                    CreateTime = DateTime.Now,
                    Reason = model.Reason,
                };
                if (model.RefundReciever != null)
                {
                    log.ReceiverName = model.RefundReciever.Name;
                    log.ReceiverAddress = model.RefundReciever.Address;
                }
                if (model.IsReceive != null)
                    log.IsReceive = Convert.ToBoolean(model.IsReceive);

                RefundType refundType = RefundType.Unknown;
                switch (model.ApiRefundType)
                {
                    case (int)ApiRefundType.Scash:
                        refundType = RefundType.Scash;
                        break;
                    case (int)ApiRefundType.Cash:
                        refundType = orderInfo.IsThridPartyPay ? RefundType.Tcash : RefundType.Cash;
                        break;
                    case (int)ApiRefundType.Atm:
                        refundType = RefundType.Atm;

                        #region 處理 ATM 帳戶資料

                        var ctAtm = model.CtAtm;
                        string errMsg;
                        if (ctAtm.IsAtmInfoValidity(out errMsg))
                        {
                            CtAtmRefund ct = new CtAtmRefund
                            {
                                UserId = userId,
                                OrderGuid = orderInfo.OrderGuid,
                                AccountName = ctAtm.AccountName,
                                Id = ctAtm.Id,
                                BankName = ctAtm.BankName,
                                BranchName = ctAtm.BranchName,
                                BankCode = ctAtm.BankNo + ctAtm.BranchNo,
                                AccountNumber = ctAtm.AccountNumber.Replace(" ", string.Empty).PadLeft(14, '0'),
                                Phone = ctAtm.Phone,
                                CreateTime = DateTime.Now,
                                Status = (int)AtmRefundStatus.Initial
                            };
                            UserRefundFacade.SetCtAtmRefund(ct, orderInfo.OrderGuid);
                        }
                        else
                        {
                            SetApiLog("ConfirmRefund", userName, maskedModel, result, Helper.GetClientIP());
                            result.Code = ApiResultCode.InputError;
                            result.Data = new ApiRefundResultModel
                            {
                                IsSuccess = false,
                                IsShowCreditNote = false,
                                FailedReason = errMsg
                            };
                            return result;
                        }

                        #endregion

                        break;
                    default:
                        result.Code = ApiResultCode.InputError;
                        result.Data = new ApiRefundResultModel
                        {
                            IsSuccess = false,
                            IsShowCreditNote = false,
                            FailedReason = I18N.Phrase.ApiReturnCodeUnknowApiRefundType
                        };
                        return result;
                }
                var kp = new KeyValuePair<OrderStatusLog, RefundType>(log, refundType);
                var refundResult = UserRefundFacade.RequestRefund(kp, userName, orderInfo);
                result.Data = new ApiRefundResultModel
                {
                    IsSuccess = CreateReturnFormResult.Created.Equals(refundResult.Key),
                    IsShowCreditNote = refundResult.Value,
                    FailedReason = CreateReturnFormResult.Created.Equals(refundResult.Key) ? string.Empty : "您的退貨申請發生異常，若您仍需退貨，請聯絡客服人員"
                };

                SetApiLog("ConfirmRefund", userName, maskedModel, result, Helper.GetClientIP());
                return result;
            }

            #endregion

            result.Data = new ApiRefundResultModel { IsSuccess = false, IsShowCreditNote = false, FailedReason = I18N.Phrase.NoOrder };

            SetApiLog("ConfirmRefund", userName, maskedModel, result, Helper.GetClientIP());
            return result;

            #endregion
        }

        /// <summary>
        /// 是否顯示折讓單
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [RequireHttps]
        [OAuthScope(TokenScope.Deal)]
        public ApiResult IsShowCreditNote(OrderGuidModel model)
        {
            ApiResult result = new ApiResult();

            #region 登入驗證

            string userName = HttpContext.Current.User.Identity.Name;
            if (string.IsNullOrWhiteSpace(userName))
            {
                result.Code = ApiResultCode.Error;
                result.Message = I18N.Phrase.ApiReturnCodeUserNoSignIn;
                return result;
            }

            #endregion
            
            var userId = MemberFacade.GetUniqueId(HttpContext.Current.User.Identity.Name);
            result.Data = UserRefundFacade.IsShowCreditNote(userId, model.OrderGuid);
            result.Code = ApiResultCode.Success;

            return result;
        }
    }

    #region model class

    /// <summary>
    /// 退貨Model
    /// </summary>
    public class ApiRefundModel
    {
        /// <summary>
        /// Api退款方式
        /// </summary>
        public int ApiRefundType { get; set; }
        /// <summary>
        /// 訂單編號
        /// </summary>
        public string OrderId { get; set; }
        /// <summary>
        /// 退貨原因
        /// </summary>
        public string Reason { get; set; }
        /// <summary>
        /// ATM 資訊 (非ATM退款可不填)
        /// </summary>
        public CtAtmRefundModel CtAtm { get; set;}
        /// <summary>
        /// 退貨收件資訊
        /// </summary>
        public RefundReciever RefundReciever { get; set; }
        /// <summary>
        /// 是否收到商品
        /// </summary>
        public bool? IsReceive { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public ApiRefundModel()
        {
        }

        /// <summary>
        /// copy constructor
        /// </summary>
        /// <param name="model"></param>
        /// <param name="ismask"></param>
        public ApiRefundModel(ApiRefundModel model, bool ismask = false)
        {
            ApiRefundType = model.ApiRefundType;
            OrderId = model.OrderId;
            Reason = model.Reason;
            CtAtm = new CtAtmRefundModel
            {
                Id = model.CtAtm.Id,
                AccountName = model.CtAtm.AccountName,
                AccountNumber = model.CtAtm.AccountNumber,
                BankName = model.CtAtm.BankName,
                BankNo = model.CtAtm.BankNo,
                BranchName = model.CtAtm.BranchName,
                BranchNo = model.CtAtm.BranchNo,
                Phone = model.CtAtm.Phone,
            };
            if (model.RefundReciever != null)
            {
                RefundReciever = new RefundReciever { Name = model.RefundReciever.Name, Address = model.RefundReciever.Address };
            }

            //MASK
            if (ismask)
            {
                if (model.CtAtm != null)
                {
                    if (!string.IsNullOrEmpty(model.CtAtm.Id))
                    {
                        CtAtm.Id = Helper.MaskAnyWords(model.CtAtm.Id);
                    }
                    if (!string.IsNullOrEmpty(model.CtAtm.AccountNumber))
                    {
                        CtAtm.AccountNumber = Helper.MaskAnyWords(model.CtAtm.AccountNumber);
                    }
                    if (!string.IsNullOrEmpty(model.CtAtm.Phone))
                    {
                        CtAtm.Phone = Helper.MaskAnyWords(model.CtAtm.Phone);
                    }
                }
                if (model.RefundReciever != null)
                {
                    if (!string.IsNullOrEmpty(RefundReciever.Name))
                    {
                        RefundReciever.Name = Helper.MaskAnyWords(model.RefundReciever.Name);
                    }
                    if (!string.IsNullOrEmpty(RefundReciever.Address))
                    {
                        RefundReciever.Address = Helper.MaskAnyWords(model.RefundReciever.Address);
                    }
                }
            }
            if (model.IsReceive != null)
            {
                IsReceive = model.IsReceive;
            }
                
        }
    }
    
    /// <summary>
    /// 訂單GuidModel
    /// </summary>
    public class OrderGuidModel
    {
        /// <summary>
        /// 訂單Guid
        /// </summary>
        public Guid OrderGuid { get; set; }
    }
    /// <summary>
    /// 訂單編號Model
    /// </summary>
    public class OrderIdModel
    {
        /// <summary>
        /// 訂單編號
        /// </summary>
        public string OrderId { get; set; }
    }

    /// <summary>
    /// Api退貨結果
    /// </summary>
    public class ApiRefundResultModel
    {
        /// <summary>
        /// 是否退貨成功
        /// </summary>
        public bool IsSuccess { get; set; }
        /// <summary>
        /// 是否顯示折讓單
        /// </summary>
        public bool IsShowCreditNote { get; set; }
        /// <summary>
        /// 失敗原因
        /// </summary>
        public string FailedReason { get; set; }
    }

    /// <summary>
    /// 退貨收件資訊
    /// </summary>
    public class RefundReciever
    {
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Address { get; set; }
    }


    #endregion
}

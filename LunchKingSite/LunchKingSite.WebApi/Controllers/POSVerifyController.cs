﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.WebApi.Core.OAuth;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.Interface;
using LunchKingSite.DataOrm;
using LunchKingSite.WebApi.Controllers.Base;
using LunchKingSite.WebApi.Core;
using LunchKingSite.WebApi.Models.POSVerifyModels;
using LunchKingSite.WebLib.Component;
using Helper = LunchKingSite.Core.Helper;
using JsonSerializer = LunchKingSite.Core.JsonSerializer;
using VbsModel = LunchKingSite.BizLogic.Model.VBS;
using System.Text.RegularExpressions;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core.Models.Entities;
using LunchKingSite.WebApi.Core.Attributes;
using Newtonsoft.Json.Linq;

namespace LunchKingSite.WebApi.Controllers
{
    /// <summary>
    /// 外部串接核銷API
    /// </summary>
    [RequireHttps]
    [ForceCamelCase]
    public class POSVerifyController : BaseController
    {
        private static readonly ISellerProvider Sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        private static readonly IPCPProvider Pcp = ProviderFactory.Instance().GetProvider<IPCPProvider>();
        private static readonly ISysConfProvider Config =ProviderFactory.Instance().GetConfig();
        private static readonly IPponProvider Pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private static readonly IPosProvider Pos = ProviderFactory.Instance().GetProvider<IPosProvider>();
        private static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();

        #region Pcp

        /// <summary>
        /// Read Pcp 熟客卡識別碼
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.PosVerify)]
        public ApiResult MembershipCardCheck(PosMembershipCardCheckModel model)
        {
            ApiResult result;

            var sellerUserId = User.Identity.Id;
            var viewIdentityCode = BonusFacade.GetViewIdentityCode(model.Code, sellerUserId, DateTime.Now);

            if (!BonusFacade.CheckViewIdentityCode(viewIdentityCode, out result, DateTime.Now))
            {
                return result;
            }

            #region 取得sellerguid

            var vbs = VBSFacade.GetVbsMembershipByUserId(sellerUserId);
            var resource = SellerFacade.GetResourceAcl(vbs.AccountId, (int)ResourceType.Seller);
            if (!resource.IsLoaded)
            {
                result.Code = ApiResultCode.PcpSellerNotFound;
                return result;
            }

            #endregion

            #region 取得storeguid

            //check seller_tree
            var rootSellerGuid = resource.ResourceGuid;
            var sellerTreeCol = SellerFacade.GetSellerTreeCol(rootSellerGuid).ToList();
            if (!sellerTreeCol.Any())
            {
                result.Code = ApiResultCode.PcpSellerTreeNotFound;
                return result;
            }

            var storeGuid = SellerFacade.GetSellerGuidByStoreCode(sellerTreeCol.Select(x => x.SellerGuid).ToList(),
                model.StoreCode);
            if (storeGuid == Guid.Empty)
            {
                result.Code = ApiResultCode.PcpStoreNotFound;
                return result;
            }

            #endregion

            #region 驗證會員卡是否可使用

            var mc = BonusFacade.MembershipCardGet(viewIdentityCode.CardId);
            if (!BonusFacade.CheckMembershipCardStatus(mc, out result, DateTime.Now))
            {
                return result;
            }

            if (!BonusFacade.MembershipCardGroupStoreGuidIsExists(mc.CardGroupId, storeGuid))
            {
                result.Code = ApiResultCode.MembershipCardCanNotUsingThisStore;
                result.Message = "會員卡無法在這間店使用";
                return result;
            }

            #endregion

            var viewVbsSellerMember = Sp.ViewVbsSellerMemberGet(viewIdentityCode.UserId, null, sellerUserId);
            if (!viewVbsSellerMember.IsLoaded)
            {
                result.Code = ApiResultCode.PcpVbsSellerMemberNotFound;
                return result;
            }

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = new MembershipCardCheckData
                {
                    IdentityCodeId = viewIdentityCode.Id,
                    MemberInfo = new MemberInfo
                    {
                        Name = viewVbsSellerMember.SellerMemberLastName + viewVbsSellerMember.SellerMemberFirstName,
                        Gender = viewVbsSellerMember.SellerMemberGender ?? 0,
                        Mobile = viewVbsSellerMember.SellerMemberMobile ?? string.Empty,
                        Birthday = viewVbsSellerMember.SellerMemberBirthday ?? DateTime.MinValue
                    },
                    Card = new MembershipCardData
                    {
                        CardLevel = viewVbsSellerMember.Level,
                        PaymentPercent = viewVbsSellerMember.PaymentPercent,
                        Instruction = viewVbsSellerMember.Instruction,
                        OtherPremiums = viewVbsSellerMember.OtherPremiums,
                        AvailableDateType = viewVbsSellerMember.AvailableDateType,
                        AvailableDateDesc = Helper.GetEnumDescription((AvailableDateType)viewVbsSellerMember.AvailableDateType),
                        OrderCount = viewVbsSellerMember.OrderCount,
                        AmountTotal = viewVbsSellerMember.AmountTotal,
                        LastUseTime = viewVbsSellerMember.LastUseTime ?? DateTime.Now
                    }
                }
            };
        }

        /// <summary>
        /// Pcp 會員卡核銷
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.PosVerify)]
        public ApiResult MembershipCardVerify(MembershipCardVerifyModel model)
        {
            ApiResult result;

            #region 取得身份識別資料

            var identityCode = BonusFacade.GetViewIdentityCode((int)model.IdentityCodeId);

            if (!BonusFacade.CheckViewIdentityCode(identityCode, out result, DateTime.Now))
            {
                return result;
            }

            #endregion 取得身份識別資料

            #region 驗證會員卡是否可使用

            var mc = BonusFacade.MembershipCardGet(identityCode.CardId);
            if (!BonusFacade.CheckMembershipCardStatus(mc, out result, DateTime.Now))
            {
                return result;
            }

            var umc = BonusFacade.UserMembershipCardGet(identityCode.UserMembershipCardId);
            if (!BonusFacade.CheckUserMembershipCardStatus(umc, out result, DateTime.Now))
            {
                return result;
            }

            #region 取得sellerguid

            var sellerUserId = User.Identity.Id;
            var vbs = VBSFacade.GetVbsMembershipByUserId(sellerUserId);
            var resource = SellerFacade.GetResourceAcl(vbs.AccountId, (int)ResourceType.Seller);
            if (!resource.IsLoaded)
            {
                result.Code = ApiResultCode.PcpSellerNotFound;
                return result;
            }
            var sellerGuid = resource.ResourceGuid;

            #endregion

            #region 取得oauth_client_appid

            var seller = VourcherFacade.SellerGetByGuid(sellerGuid);

            if (!seller.IsLoaded)
            {
                result.Code = ApiResultCode.PcpSellerNotFound;
                return result;
            }
            var oauthClientAppid = seller.OauthClientAppId;

            #endregion

            #region 取得storeguid

            //check seller_tree
            var rootSellerGuid = resource.ResourceGuid;
            var sellerTreeCol = SellerFacade.GetSellerTreeCol(rootSellerGuid).ToList();
            if (!sellerTreeCol.Any())
            {
                result.Code = ApiResultCode.PcpSellerTreeNotFound;
                return result;
            }

            var storeGuid = SellerFacade.GetSellerGuidByStoreCode(sellerTreeCol.Select(x => x.SellerGuid).ToList(),
                model.StoreCode);
            if (storeGuid == Guid.Empty)
            {
                result.Code = ApiResultCode.PcpStoreNotFound;
                return result;
            }

            if (!BonusFacade.MembershipCardGroupStoreGuidIsExists(mc.CardGroupId, storeGuid))
            {
                result.Code = ApiResultCode.MembershipCardCanNotUsingThisStore;
                result.Message = "會員卡無法在這間店使用";
                return result;
            }

            #endregion

            #endregion

            #region PCP交易

            Guid pcpOrderGuid;

            double transAmount = model.DiscountAmount ?? 0; //交易金額
            double discount = (model.Amount ?? 0) - transAmount; //折扣金額
            if (discount < 0) discount = 0;

            var check = BonusFacade.PcpTransCheckOut(model.IdentityCodeId, identityCode.UserMembershipCardId, transAmount, discount
                , null, 0, 0, identityCode.UserId, identityCode.SellerUserId
                , identityCode.CardId, storeGuid, out pcpOrderGuid);

            switch (check)
            {
                case PcpCheckoutResult.Success:
                    result.Code = ApiResultCode.Success;
                    break;
                case PcpCheckoutResult.UpdateDiscountCodeFail:
                    result.Code = ApiResultCode.DiscountCodeError;
                    result.Message = "抵用券交易失敗";
                    break;
                case PcpCheckoutResult.NotEnoughBonus:
                    result.Code = ApiResultCode.PcpNotEnoughBonus;
                    result.Message = "無足夠的紅利點數可扣除";
                    break;
                case PcpCheckoutResult.SuperBonusOverAmount:
                    result.Code = ApiResultCode.PcpSuperBonusOverAmount;
                    result.Message = "超級紅利點折抵不可大於結帳金額";
                    break;
                case PcpCheckoutResult.PcpCodeIsUsed:
                    result.Code = ApiResultCode.PcpCodeIsUsed;
                    result.Message = "交易碼已使用過";
                    break;
                case PcpCheckoutResult.PcpCodeIsCancel:
                    result.Code = ApiResultCode.PcpCodeIsCancel;
                    result.Message = "交易碼已作廢";
                    break;
                default:
                    result.Code = ApiResultCode.PcpTransactionFail;
                    result.Message = "交易失敗";
                    break;
            }

            if (check != PcpCheckoutResult.Success)
            {
                return result;
            }

            #endregion

            #region 會員卡升等

            var userMembershipCardLevel = BonusFacade.MembershipCardLevelUpCheck(mc, identityCode.UserMembershipCardId, sellerUserId);//
            var isLevelUp = false;
            var levelUpMessage = string.Empty;

            if (userMembershipCardLevel > mc.Level)
            {
                isLevelUp = true;
                levelUpMessage = Helper.GetEnumDescription((MembershipCardLevel)userMembershipCardLevel);
            }

            #endregion 

            var viewVbsSellerMember = SellerFacade.GetViewVbsSellerMember(identityCode.SellerUserId, identityCode.UserId, null);

            #region update polling status
            var ppus = PcpFacade.GetPollingStatusById(identityCode.UserId);
            if (ppus.IsLoaded && ppus.ExpiredTime > DateTime.Now)
            {
                ppus.Action = (int)PcpPollingUserActionType.Complete;
                ppus.ModifiedTime = DateTime.Now;
                ppus.UserMembershipCardId = identityCode.UserMembershipCardId;
                Pcp.PcpPollingUserStatusSet(ppus);
            }
            #endregion



            #region 寫log

            var pcpPosTransLog = new PcpPosTransLog
            {
                OauthClientAppId = oauthClientAppid,
                TransType = (byte)PcpPosTranType.Verify,
                SellerGuid = sellerGuid,
                StoreGuid = storeGuid,
                StoreCode = model.StoreCode,
                StoreName = model.StoreName,
                IndentityCode = (int)model.IdentityCodeId,
                OrderGuid = pcpOrderGuid,
                InputData = new JsonSerializer().Serialize(new { model.Amount, model.DiscountAmount }),
                Result = result.Message,
                CreateTime = DateTime.Now
            };
            Pcp.PcpPosTransLogSet(pcpPosTransLog);

            #endregion

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = new MembershipCardVerifyData
                {
                    OrderId = pcpOrderGuid,
                    IsLevelUp = isLevelUp,
                    LevelUpMsg = levelUpMessage,
                    OrderCount = viewVbsSellerMember.OrderCount,
                    AmountTotal = (int)(viewVbsSellerMember.AmountTotal)
                }
            };

        }

        /// <summary>
        /// Pcp 會員卡上傳交易明細資料
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.PosVerify)]
        public ApiResult MembershipCardOrderDetail(PcpOrderDetailInputModel model)
        {
            ApiResult result = new ApiResult();

            #region 驗證賣家是否資料正確

            #region 取得sellerguid
            var sellerUserId = User.Identity.Id;
            var vbs = VBSFacade.GetVbsMembershipByUserId(sellerUserId);
            var resource = SellerFacade.GetResourceAcl(vbs.AccountId, (int)ResourceType.Seller);
            if (!resource.IsLoaded)
            {
                result.Code = ApiResultCode.PcpSellerNotFound;
                return result;
            }
            var sellerGuid = resource.ResourceGuid;

            #endregion

            #region 取得oauth_client_appid

            var seller = VourcherFacade.SellerGetByGuid(sellerGuid);

            if (!seller.IsLoaded)
            {
                result.Code = ApiResultCode.PcpSellerNotFound;
                return result;
            }
            var oauthClientAppid = seller.OauthClientAppId; //寫log用

            #endregion

            #region 取得storeguid
            //check seller_tree 利用rootsellerguid找到分店
            var rootSellerGuid = resource.ResourceGuid;
            var sellerTreeCol = SellerFacade.GetSellerTreeCol(rootSellerGuid).ToList();
            if (!sellerTreeCol.Any())
            {
                result.Code = ApiResultCode.PcpSellerTreeNotFound;
                return result;
            }

            //利用輸入的店code比對確認是哪間分店
            var storeGuid = SellerFacade.GetSellerGuidByStoreCode(sellerTreeCol.Select(x => x.SellerGuid).ToList(),
                model.StoreCode);
            if (storeGuid == Guid.Empty)
            {
                result.Code = ApiResultCode.PcpStoreNotFound;
                return result;
            }

            #endregion

            #endregion

            var pcpOrderDetailCol = new PcpOrderDetailCollection();
            var diffPcpOrderDetail = new List<PcpOrderKeyInfo>();
            var failPcpOrderDetail = new List<PcpOrderResult>();

            #region 取得Detail

            foreach (var orderDetail in model.OrderDetail)
            {
                Guid pcpOrderGuid = new Guid();

                if (string.IsNullOrEmpty(orderDetail.IdentityCode))
                {
                    failPcpOrderDetail.Add(
                        new PcpOrderResult
                        {
                            OrderId = orderDetail.OrderId,
                            IdentityCode = orderDetail.IdentityCode,
                            Code = ApiResultCode.InputError,
                            Message = "IdentityCode參數格式錯誤"
                        });
                    continue;
                }

                if (!string.IsNullOrEmpty(orderDetail.OrderId))
                {
                    if (!Guid.TryParse(orderDetail.OrderId, out pcpOrderGuid))
                    {
                        failPcpOrderDetail.Add(
                            new PcpOrderResult
                            {
                                OrderId = orderDetail.OrderId,
                                IdentityCode = orderDetail.IdentityCode,
                                Code = ApiResultCode.InputError,
                                Message = "OrderId參數格式錯誤"
                            });
                        continue;
                    }
                }

                pcpOrderDetailCol.Add(
            new DataOrm.PcpOrderDetail
            {
                //這裡沒有pcpOrderPk，後面等guid確定都有了再全部一起塞進去
                RelatedStoreCode = orderDetail.RelatedStoreCode,
                RelatedStoreName = orderDetail.RelatedStoreName,
                RelatedOderId = orderDetail.RelatedOrderId,
                PcpOrderGuid = pcpOrderGuid,
                IdentityCode = orderDetail.IdentityCode,
                TradeTime = orderDetail.TradeTime,
                ItemId = orderDetail.ItemId,
                ItemName = orderDetail.ItemName,
                ItemSpec = orderDetail.ItemSpec,
                ItemNotes = orderDetail.ItemNotes,
                Price = (decimal)orderDetail.Price,
                Qty = (int)orderDetail.Qty,
                Unit = orderDetail.Unit
            });
            }

            #endregion

            #region 建立Detail的Guid

            //選出沒有guid的Collection
            var pcpOrderDetailWithoutGuidCol = pcpOrderDetailCol.Where(x => !string.IsNullOrEmpty(x.IdentityCode) && x.PcpOrderGuid == Guid.Empty).ToList();

            foreach (var pcpOrderDetailWithoutGuid in pcpOrderDetailWithoutGuidCol.GroupBy(x => x.IdentityCode))
            {
                var identityCode = BonusFacade.GetViewIdentityCode(pcpOrderDetailWithoutGuid.Key, sellerUserId, pcpOrderDetailWithoutGuidCol.Where(x => x.IdentityCode == pcpOrderDetailWithoutGuid.Key).Select(x => x.TradeTime).First());

                #region 取得身份識別資料

                if (!BonusFacade.CheckViewIdentityCode(identityCode, out result, pcpOrderDetailWithoutGuidCol.Where(x => x.IdentityCode == pcpOrderDetailWithoutGuid.Key).Select(x => x.TradeTime).First()))
                {
                    failPcpOrderDetail.Add(
                       new PcpOrderResult
                       {
                           OrderId = null,    //此時GUID還沒產生
                           IdentityCode = pcpOrderDetailWithoutGuid.Key,
                           Code = result.Code,
                           Message = result.Message
                       });
                    continue;
                }

                var mc = BonusFacade.MembershipCardGet(identityCode.CardId);
                if (!BonusFacade.CheckMembershipCardStatus(mc, out result, pcpOrderDetailWithoutGuidCol.Where(x => x.IdentityCode == pcpOrderDetailWithoutGuid.Key).Select(x => x.TradeTime).First()))
                {
                    failPcpOrderDetail.Add(
                      new PcpOrderResult
                      {
                          OrderId = null,    //此時GUID還沒產生
                          IdentityCode = pcpOrderDetailWithoutGuid.Key,
                          Code = result.Code,
                          Message = result.Message
                      });
                    continue;
                }

                var umc = BonusFacade.UserMembershipCardGet(identityCode.UserMembershipCardId);
                if (!BonusFacade.CheckUserMembershipCardStatus(umc, out result, pcpOrderDetailWithoutGuidCol.Where(x => x.IdentityCode == pcpOrderDetailWithoutGuid.Key).Select(x => x.TradeTime).First()))
                {
                    failPcpOrderDetail.Add(
                      new PcpOrderResult
                      {
                          OrderId = null,    //此時GUID還沒產生
                          IdentityCode = pcpOrderDetailWithoutGuid.Key,
                          Code = result.Code,
                          Message = result.Message
                      });
                    continue;
                }

                #endregion

                #region PCP交易
                //取得折扣率
                var viewVbsSellerMember = Sp.ViewVbsSellerMemberGet(identityCode.UserId, null, sellerUserId);
                double transAmount = (double)pcpOrderDetailWithoutGuidCol.Where(x => x.IdentityCode == pcpOrderDetailWithoutGuid.Key).Sum(x => x.Price);
                double discountRate = viewVbsSellerMember.PaymentPercent;
                double discount = transAmount - (transAmount * discountRate);
                if (discount < 0) discount = 0;

                Guid pcpOrderGuid;

                var check = BonusFacade.PcpTransCheckOut(identityCode.Id,
                    identityCode.UserMembershipCardId, transAmount, discount
                    , null, 0, 0, identityCode.UserId, identityCode.SellerUserId
                    , identityCode.CardId, storeGuid, out pcpOrderGuid);

                switch (check)
                {
                    case PcpCheckoutResult.Success:
                        result.Code = ApiResultCode.Success;
                        break;
                    case PcpCheckoutResult.UpdateDiscountCodeFail:
                        result.Code = ApiResultCode.DiscountCodeError;
                        result.Message = "抵用券交易失敗";
                        break;
                    case PcpCheckoutResult.NotEnoughBonus:
                        result.Code = ApiResultCode.PcpNotEnoughBonus;
                        result.Message = "無足夠的紅利點數可扣除";
                        break;
                    case PcpCheckoutResult.SuperBonusOverAmount:
                        result.Code = ApiResultCode.PcpSuperBonusOverAmount;
                        result.Message = "超級紅利點折抵不可大於結帳金額";
                        break;
                    case PcpCheckoutResult.PcpCodeIsUsed:
                        result.Code = ApiResultCode.PcpCodeIsUsed;
                        result.Message = "交易碼已使用過";
                        break;
                    case PcpCheckoutResult.PcpCodeIsCancel:
                        result.Code = ApiResultCode.PcpCodeIsCancel;
                        result.Message = "交易碼已作廢";
                        break;
                    default:
                        result.Code = ApiResultCode.PcpTransactionFail;
                        result.Message = "交易失敗";
                        break;
                }

                #region 寫log

                var pcpPosTransLog = new PcpPosTransLog
                {
                    OauthClientAppId = oauthClientAppid,
                    TransType = (byte)PcpPosTranType.Verify,
                    SellerGuid = sellerGuid,
                    StoreGuid = storeGuid,
                    StoreCode = model.StoreCode,
                    StoreName = model.StoreName,
                    IndentityCode = (int)long.Parse(pcpOrderDetailWithoutGuid.Key),
                    OrderGuid = pcpOrderGuid,
                    InputData = new JsonSerializer().Serialize(new { transAmount, discount }),
                    Result = result.Message,
                    CreateTime = DateTime.Now
                };

                Pcp.PcpPosTransLogSet(pcpPosTransLog);

                #endregion

                if (check != PcpCheckoutResult.Success)
                {
                    failPcpOrderDetail.Add(
                        new PcpOrderResult
                        {
                            OrderId = pcpOrderGuid.ToString(),
                            IdentityCode = pcpOrderDetailWithoutGuid.Key,
                            Code = result.Code,
                            Message = result.Message
                        });
                    continue;
                }

                //將guid回填原Collection
                foreach (var pcpOrderDetail in pcpOrderDetailCol.Where(x => x.IdentityCode == pcpOrderDetailWithoutGuid.Key))
                {
                    pcpOrderDetail.PcpOrderGuid = pcpOrderGuid;
                }

                diffPcpOrderDetail.Add(
                       new PcpOrderKeyInfo
                       {
                           OrderId = pcpOrderGuid.ToString(),
                           IdentityCode = pcpOrderDetailWithoutGuid.Key
                       });

                #endregion
            }

            #endregion

            #region 寫入Detail

            var newPcpOrderDetailCol = new PcpOrderDetailCollection();

            foreach (var pcpOrderDetail in pcpOrderDetailCol.Where(x => x.PcpOrderGuid != Guid.Empty))
            {

                //取得pcporder的pk當作pcp_order與pcp_order_detail的關聯
                PcpOrder pcpOrder = Pcp.PcpOrderGetByGuid(pcpOrderDetail.PcpOrderGuid);

                if (!pcpOrder.IsLoaded)
                {
                    failPcpOrderDetail.Add(
                        new PcpOrderResult
                        {
                            OrderId = pcpOrderDetail.PcpOrderGuid.ToString(),
                            IdentityCode = pcpOrderDetail.IdentityCode,
                            Code = ApiResultCode.DataNotFound,
                            Message = "找不到對應的OrderId"
                        });
                    continue;
                }

                //檢查IdentityCode與OrderId(Guid)是否吻合
                if (pcpOrder.IdentityCodeId != null)
                {
                    var identityCode = Pcp.IdentityCodeGet((long)pcpOrder.IdentityCodeId);

                    if (identityCode.Code != pcpOrderDetail.IdentityCode)
                    {
                        failPcpOrderDetail.Add(
                            new PcpOrderResult
                            {
                                OrderId = pcpOrderDetail.PcpOrderGuid.ToString(),
                                IdentityCode = pcpOrderDetail.IdentityCode,
                                Code = ApiResultCode.DataNotFound,
                                Message = "IdentityCode或OrderId不正確"
                            });
                        continue;
                    }

                }
                else
                {
                    failPcpOrderDetail.Add(
                        new PcpOrderResult
                        {
                            OrderId = pcpOrderDetail.PcpOrderGuid.ToString(),
                            IdentityCode = pcpOrderDetail.IdentityCode,
                            Code = ApiResultCode.DataNotFound,
                            Message = "找不到對應的IdentityCodeId"
                        });
                    continue;
                }

                //檢查pcp_order_detail是否資料已存在
                if (Pcp.PcpOrderDetialCheckDataIsExistByPk(pcpOrder.Pk))
                {
                    failPcpOrderDetail.Add(
                        new PcpOrderResult
                        {
                            OrderId = pcpOrderDetail.PcpOrderGuid.ToString(),
                            IdentityCode = pcpOrderDetail.IdentityCode,
                            Code = ApiResultCode.DataIsExist,
                            Message = "該筆交易明細資料已存在"
                        });
                    continue;
                }

                    newPcpOrderDetailCol.Add(
                new DataOrm.PcpOrderDetail
                {
                    RelatedStoreCode = pcpOrderDetail.RelatedStoreCode,
                    RelatedStoreName = pcpOrderDetail.RelatedStoreName,
                    RelatedOderId = pcpOrderDetail.RelatedOderId,
                    PcpOrderPk = pcpOrder.Pk,
                    PcpOrderGuid = pcpOrderDetail.PcpOrderGuid,
                    IdentityCode = pcpOrderDetail.IdentityCode,
                    TradeTime = pcpOrderDetail.TradeTime,
                    ItemId = pcpOrderDetail.ItemId,
                    ItemName = pcpOrderDetail.ItemName,
                    ItemSpec = pcpOrderDetail.ItemSpec,
                    ItemNotes = pcpOrderDetail.ItemNotes,
                    Price = pcpOrderDetail.Price,
                    Qty = pcpOrderDetail.Qty,
                    Unit = pcpOrderDetail.Unit,
                    CreateTime = DateTime.Now
                });
 
            }

            Pcp.PcpOrderDetialCollectionSet(newPcpOrderDetailCol);

            #endregion

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = new PcpOrderDetailInputModelData
                {
                    TotleDetail = model.OrderDetail.Count,
                    SuccessDetailCount = newPcpOrderDetailCol.Count,
                    DiffDetailCount = diffPcpOrderDetail.Count,
                    FailDetailCount = failPcpOrderDetail.Count,
                    DiffOrderDetail = diffPcpOrderDetail,
                    FailOrderDetail = failPcpOrderDetail
                }
            };
        }

        /// <summary>
        /// Pcp 會員卡取消核銷
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.PosVerify)]
        public ApiResult MembershipCardCancel(MembershipCardCancelModel model)
        {
            var result = new ApiResult();

            var pcpOrder = Pcp.PcpOrderGetByGuid(model.OrderId);
            if (!pcpOrder.IsLoaded)
            {
                result.Code = ApiResultCode.PcpOrderNotFound;
                return result;
            }
            if (pcpOrder.Status != (int)PcpOrderStatus.Normal)
            {
                result.Code = ApiResultCode.PcpVerifyCancelled;
                return result;
            }

            #region 驗證會員卡是否可使用

            var nowUserMembershipCard = Pcp.UserMembershipCardGet((int)pcpOrder.CardId);
            var mc = BonusFacade.MembershipCardGet(nowUserMembershipCard.CardId);
            if (!BonusFacade.CheckMembershipCardStatus(mc, out result, DateTime.Now))
            {
                return result;
            }

            #endregion

            #region 取得sellerguid

            var sellerUserId = User.Identity.Id;
            var vbs = VBSFacade.GetVbsMembershipByUserId(sellerUserId);
            var resource = SellerFacade.GetResourceAcl(vbs.AccountId, (int)ResourceType.Seller);
            if (!resource.IsLoaded)
            {
                result.Code = ApiResultCode.PcpSellerNotFound;
                return result;
            }
            var sellerGuid = resource.ResourceGuid;

            #endregion

            #region 取得oauth_client_appid

            var seller = VourcherFacade.SellerGetByGuid(sellerGuid);

            if (!seller.IsLoaded)
            {
                result.Code = ApiResultCode.PcpSellerNotFound;
                return result;
            }
            var oauthClientAppid = seller.OauthClientAppId;

            #endregion

            #region 取得storeguid

            //check seller_tree
            var rootSellerGuid = resource.ResourceGuid;
            var sellerTreeCol = SellerFacade.GetSellerTreeCol(rootSellerGuid).ToList();
            if (!sellerTreeCol.Any())
            {
                result.Code = ApiResultCode.PcpSellerTreeNotFound;
                return result;
            }
            var storeGuid = SellerFacade.GetSellerGuidByStoreCode(sellerTreeCol.Select(x => x.SellerGuid).ToList(),
                model.StoreCode);
            if (storeGuid == Guid.Empty)
            {
                result.Code = ApiResultCode.PcpStoreNotFound;
                return result;
            }
            if (pcpOrder.StoreGuid != storeGuid)
            {
                result.Code = ApiResultCode.PcpOrderNotFound;
                return result;
            }

            #endregion

            pcpOrder.Status = (int)PcpOrderStatus.Cancel;
            pcpOrder.OrderType = (int) PcpOrderType.MembershipCard;
            Pcp.PcpOrderSet(pcpOrder);

            #region 熟客卡(會員卡)更新

            Pcp.UserMembershipCardUpdateOrderCount((int)pcpOrder.CardId, (double)(pcpOrder.Amount ?? 0), PcpLevelChangeType.Down);
            var userMembershipCard = Pcp.UserMembershipCardGet((int)pcpOrder.CardId);
            var updatedMcOrderCount = userMembershipCard.OrderCount;
            var updatedMcCardAmount = userMembershipCard.AmountTotal;

            #endregion

            #region 會員卡降等

            var userMembershipCardLevel = BonusFacade.MembershipCardLevelDownCheck(mc, userMembershipCard.Id, sellerUserId, pcpOrder.Guid);
            var isLevelDown = false;
            var levelDownMessage = string.Empty;

            if (userMembershipCardLevel <= mc.Level)
            {
                isLevelDown = true;
                levelDownMessage = Helper.GetEnumDescription((MembershipCardLevel)userMembershipCardLevel);
            }

            #endregion

            #region 寫log

            var pcpPosTransLog = new PcpPosTransLog
            {
                OauthClientAppId = oauthClientAppid,
                TransType = (byte)PcpPosTranType.Cancel,
                SellerGuid = sellerGuid,
                StoreGuid = storeGuid,
                StoreCode = model.StoreCode,
                StoreName = model.StoreName,
                IndentityCode = (int)pcpOrder.IdentityCodeId,
                OrderGuid = model.OrderId,
                InputData = new JsonSerializer().Serialize(new { pcpOrder.Amount, pcpOrder.DiscountAmount }),
                Result = Helper.GetEnumDescription(ApiResultCode.Success),
                CreateTime = DateTime.Now
            };
            Pcp.PcpPosTransLogSet(pcpPosTransLog);

            #endregion

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = new MembershipCardCancelData
                {
                    IsLevelDown = isLevelDown,
                    LevelDownMsg = levelDownMessage,
                    OrderCount = updatedMcOrderCount,
                    AmountTotal = (double)updatedMcCardAmount
                }
            };
        }

        /// <summary>
        /// Pcp 產生測試碼
        /// </summary>
        [HttpPost]
        [OAuthScope(TokenScope.PosVerify)]
        public ApiResult GenerateTestCode()
        {
            var result = new ApiResult();

            var sellerUserId = User.Identity.Id;

            var client = ChannelFacade.GetOauthClientByHeaderToken();
            var seller = Sp.SellerGet(Seller.Columns.OauthClientAppId, client.AppId);
            if (!seller.IsLoaded)
            {
                result.Code = ApiResultCode.Error;
                return result;
            }

            var cardGroup = Pcp.MembershipCardGroupGetSet(seller.Guid, false);
            var userMembership = Pcp.UserMembershipCardGetByCardGroupIDAndUserId(cardGroup.Id, sellerUserId);
            if (!userMembership.IsLoaded)
            {
                result.Code = ApiResultCode.Error;
                return result;
            }

            var cardId = userMembership.CardId;

            var memeberUserId = Config.PcpGenTestIdentityCodeUserId;

            var identityCode = BonusFacade.GenIdentityCode(cardId, userMembership.CardGroupId, DateTime.Now, memeberUserId, 100);
            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = Config.SiteUrl + "/vbs_r/verifycode?code=" + identityCode.Code
            };
        }

        #endregion Pcp

        #region Coupon Verify
        /// <summary>
        /// Coupon 核銷前檢驗
        /// </summary>
        [HttpPost]
        [OAuthScope(TokenScope.PosVerify)]
        public ApiResult VerifyCheck(VerifyCheckModel model)
        {
            var result = new ApiResult();
            var log = Pos.SavePosApiLog(new ChannelPosApiLog
            {
                ApiName = PosApiType.VerifyCheck.ToString(),
                KeyVal = string.Format("{0}-{1}", model.CouponSerial, model.CouponCode),
                Memo = model.StoreCode,
                OauthToken = ChannelFacade.GetHeaderToken(),
                RequestType = (byte)PosApiType.VerifyCheck,
                RequestIp = Helper.GetClientIP(),
                RequestTime = DateTime.Now
            });

            if (model.CouponType == (int)CouponType.Serial)
            {
                var pez=Pp.PeztempPincodeGet(model.CouponSerial, PeztempServiceCode.None);
                //檢查是否有此序號
                if (pez !=null && pez.IsLoaded)
                {
                    var bid = pez.Bid;
                    var go=PponFacade.GroupOrderGetByGuid((Guid)bid);
                    //檢查是否為廠商提供序號檔次
                    if (!Helper.IsFlagSet((int)go.Status, GroupOrderStatus.PEZevent))
                    {
                        result.Code = ApiResultCode.PcpPezEventNotFound;
                        SavePosApiLog(log, result);
                        return result;
                    }
                }
                else
                {
                    result.Code = ApiResultCode.PcpSerialNotFound;
                    SavePosApiLog(log, result);
                    return result;
                }
                
            }
            else
            {
                if (!Regex.IsMatch(model.CouponSerial, @"\d{4}-\d{4}"))
                {
                    result.Code = ApiResultCode.InputError;
                    SavePosApiLog(log, result);
                    return result;
                }
            }
            
            var verifyData = new List<VbsModel.VerifyCode>
            {
                new VbsModel.VerifyCode
                {
                    CouponSerial = model.CouponSerial,
                    CouponCode = (model.CouponType==(int)CouponType.Serial)? model.CouponSerial:model.CouponCode
                }
            };

            var sellerUserId = User.Identity.Id;

            #region check storeCode碼並取得userId

            var userId = SellerFacade.GetUserIdBySellerUserId(sellerUserId, model.StoreCode);
            if (userId == null)
            {
                result.Code = ApiResultCode.PcpStoreNotFound;
                SavePosApiLog(log, result);
                return result;
            }

            #endregion

            var memOriginal = VbsMemberUtility.GetVbsMemberShipByUserId((int)userId);

            var resultInfo = VBSFacade.VerifyCheckInfo(memOriginal, verifyData, model.CouponType == (int)CouponType.Serial); //單筆核銷
            result.Code = resultInfo.Item1;
            result.Data = resultInfo.Item2;
            result.Message = resultInfo.Item3;
            SavePosApiLog(log, result);
            return result;
        }

        /// <summary>
        /// 成套商品 核銷前檢驗
        /// </summary>
        [HttpPost]
        [OAuthScope(TokenScope.PosVerify)]
        public ApiResult VerifyCheckGroupCoupon(VerifyGroupCouponCheckModel model)
        {
            var result = new ApiResult();
            var log = Pos.SavePosApiLog(new ChannelPosApiLog
            {
                ApiName = PosApiType.VerifyCheckGroupCoupon.ToString(),
                KeyVal = string.Format("{0}-{1}", model.QRCode, model.StoreCode),
                Memo = model.StoreCode,
                OauthToken = ChannelFacade.GetHeaderToken(),
                RequestType = (byte)PosApiType.VerifyCheckGroupCoupon,
                RequestIp = Helper.GetClientIP(),
                RequestTime = DateTime.Now
            });

            var code =TransQRCode(model.QRCode);
            if(code.Contains("轉換失敗"))
            {
                result.Code = ApiResultCode.InputError;
                result.Message = "QRCode"+code;
                SavePosApiLog(log, result);
                return result;
            }
            var data=new JsonSerializer().Deserialize<JObject>(code);
            
            Guid orderGuid;  //核銷訂單
            int couponUserId; //憑證擁有者UserId
            int verifyCount; //欲核銷數量
            List<Guid> tids; //準備核銷憑證 trust id
            string timestamp;

            try
            {
                orderGuid = (Guid)data.Property("G").Value;
                couponUserId = (int)data.Property("I").Value;
                verifyCount = (int)data.Property("N").Value;
                timestamp = (string)data.Property("T").Value;
            }
            catch (Exception)
            {
                result.Code = ApiResultCode.InputError;
                result.Message = "參數錯誤";
                SetApiLog(GetMethodName("VerifyCheckGroupCoupon"), AppId, new { }, result, Helper.GetClientIP());
                return result;
            }
            
            if (VBSFacade.GroupCouponIsExist(orderGuid, timestamp))
            {
                result.Code = ApiResultCode.CouponUsed;
                result.Message = "QR code 已使用過，請消費者重新選擇兌換數量，再生成 QR code";
                return result;
            }

            #region check storeCode碼
            var sellerUserId = User.Identity.Id;
            var checkUserId = SellerFacade.GetUserIdBySellerUserId(sellerUserId, model.StoreCode);
            if (checkUserId == null)
            {
                result.Code = ApiResultCode.PcpStoreNotFound;
                SavePosApiLog(log, result);
                return result;
            }
            #endregion

            var verifyResult = VBSFacade.CheckCouponGroupTrustIdQuantity(couponUserId, orderGuid, verifyCount,out tids);

            if (verifyResult != GroupCouponVerifyCheckType.Success)
            {
                switch (verifyResult)
                {
                    case GroupCouponVerifyCheckType.QtyNotEnough:
                        result.Message = "可核銷數量不足";
                        break;
                    case GroupCouponVerifyCheckType.CouponNotEnough:
                        result.Message = "需使用完一般憑證才可核銷贈品憑證";
                        break;
                    case GroupCouponVerifyCheckType.GiveawayCouponNotEnouth:
                        result.Message = "可核銷數量不足";
                        break;
                }

                result.Code = ApiResultCode.CouponQtyNotEnough;
                SetApiLog(GetMethodName("VerifyCheckGroupCoupon"), AppId, new { }, result, Helper.GetClientIP());
                return result;
            }
            // change userid by storeCode
            var userId = (int)checkUserId;
            VbsMembership memOriginal = VbsMemberUtility.GetVbsMemberShipByUserId(userId);
            var verifyData = tids.Select(x => new VbsModel.VerifyCode
            {
                CouponCode = string.Empty,
                CouponSerial = string.Empty,
                TrustId = x
            }).ToList();

            var resultInfo = VBSFacade.VerifyCheckInfo(memOriginal, verifyData); //成套核銷

            result.Code = resultInfo.Item1;
            result.Data = new VerifyCheckGroupCouponOutputModel
            {
                OrderGuid = orderGuid,
                Timestamp = timestamp,
                Detail = resultInfo.Item2
            };
            result.Message = resultInfo.Item3;

            return result;
        }

        /// <summary>
        /// Coupon 核銷
        /// </summary>
        [HttpPost]
        [OAuthScope(TokenScope.PosVerify)]
        public ApiResult VerifyCoupon(VerifyCouponModel model)
        {
            var result = new ApiResult();
            var log = Pos.SavePosApiLog(new ChannelPosApiLog
            {
                ApiName = PosApiType.VerifyCoupon.ToString(),
                KeyVal = string.Format("{0}", model.TrustId),
                Memo = model.StoreCode,
                OauthToken = ChannelFacade.GetHeaderToken(),
                RequestType = (byte)PosApiType.VerifyCoupon,
                RequestIp = Helper.GetClientIP(),
                RequestTime = DateTime.Now
            });

            if (model.TrustId == Guid.Empty)
            {
                result.Code = ApiResultCode.InputError;
                SavePosApiLog(log, result);
                SetApiLog(GetMethodName("VerifyCoupon"), AppId, new { }, result, Helper.GetClientIP());
                return result;
            }
            var trustId = model.TrustId;

            //API UserID
            var sellerUserId = User.Identity.Id;

            #region 取得check storeCode碼並取得userId

            var userId = SellerFacade.GetUserIdBySellerUserId(sellerUserId, model.StoreCode);
            if (userId == null)
            {
                result.Code = ApiResultCode.PcpStoreNotFound;
                SavePosApiLog(log, result);
                return result;
            }

            #endregion

            var memOriginal = VbsMemberUtility.GetVbsMemberShipByUserId((int)userId);
            var status = OrderFacade.VerifyCoupon(trustId, false, memOriginal.AccountId, Helper.GetClientIP(), "");

            CashTrustLog cashTrustLog;
            switch (status)
            {
                case VerificationStatus.CouponOk:
                    result.Code = ApiResultCode.Success;
                    result.Data = "已核銷成功，請繼續下一筆核銷。";
                    break;
                case VerificationStatus.CouponUsed:
                    cashTrustLog = VerificationFacade.GetCashTrustLog(trustId);
                    result.Code = ApiResultCode.CouponUsed;
                    result.Message = "此憑證已使用過了，\n核銷時間：" + cashTrustLog.ModifyTime;
                    break;
                case VerificationStatus.CouponReted:
                    cashTrustLog = VerificationFacade.GetCashTrustLog(trustId);
                    result.Code = ApiResultCode.CouponReturned;
                    result.Message = "此憑證已退貨\n退貨時間：" + cashTrustLog.ModifyTime;
                    break;
                default:
                    result.Code = ApiResultCode.SystemBusy;
                    result.Message = "系統繁忙中，請30秒後再試一次。";
                    break;
            }

            SetApiLog(GetMethodName("VerifyCoupon"), AppId, new { userId, trustId, DateTime.Now, status }
                , result, Helper.GetClientIP());
            SavePosApiLog(log, result);
            return result;
        }

        /// <summary>
        /// 成套商品 核銷
        /// </summary>
        [HttpPost]
        [OAuthScope(TokenScope.PosVerify)]
        public ApiResult VerifyGroupCoupon(VerifyGroupCouponModel model)
        {
            var result = new ApiResult();
            var log = Pos.SavePosApiLog(new ChannelPosApiLog
            {
                ApiName = PosApiType.VerifyGroupCoupon.ToString(),
                KeyVal = string.Format("{0}-{1}-{2}-{3}", model.OrderGuid,model.Timestamp,model.StoreCode,model.TrustId),
                Memo = model.StoreCode,
                OauthToken = ChannelFacade.GetHeaderToken(),
                RequestType = (byte)PosApiType.VerifyGroupCoupon,
                RequestIp = Helper.GetClientIP(),
                RequestTime = DateTime.Now
            });
            
            var tids = new List<Guid>();
            Guid orderGuid;  //核銷訂單
            string timestamp;

            try
            {
                tids.AddRange(model.TrustId);
                orderGuid = model.OrderGuid;
                timestamp = model.Timestamp;
            }
            catch (Exception)
            {
                result.Code = ApiResultCode.InputError;
                result.Message = "參數錯誤";

                SetApiLog(GetMethodName("VerifyGroupCoupon"), AppId, new { }, result, Helper.GetClientIP());
                return result;
            }

            #region check storeCode碼
            var sellerUserId = User.Identity.Id;
            var checkUserId = SellerFacade.GetUserIdBySellerUserId(sellerUserId, model.StoreCode);
            if (checkUserId == null)
            {
                result.Code = ApiResultCode.PcpStoreNotFound;
                SavePosApiLog(log, result);
                return result;
            }
            #endregion

            // change userid by storeCode
            var userId = (int)checkUserId;

            VbsMembership memOriginal = VbsMemberUtility.GetVbsMemberShipByUserId(userId);

            VbsModel.MultiCouponVerifyResult resultData;
            var groupCode = VBSFacade.MultiCouponVerify(memOriginal, tids, orderGuid, timestamp, Helper.GetClientIP(), out resultData);

            result.Data = resultData;

            switch (groupCode)
            {
                case ApiResultCode.CouponPartialFail:
                    result.Code = ApiResultCode.CouponPartialFail;
                    result.Message = "此批憑證部份核銷失敗";
                    break;
                case ApiResultCode.CouponUsed:
                    result.Code = ApiResultCode.CouponUsed;
                    result.Message = "code 已使用過，請消費者重新選擇兌換數量，再生成 QR code";
                    break;
                default:
                    result.Code = ApiResultCode.Success;
                    result.Message = "核銷成功";
                    break;
            }

            SetApiLog(GetMethodName("VerifyGroupCoupon"), AppId, new { userId, tids, DateTime.Now, groupCode }
                , result, Helper.GetClientIP());

            return result;
        }

        #endregion Coupon Verify

        #region Coupon UnVerify

        /// <summary>
        /// Coupon 核銷前檢驗
        /// </summary>
        [HttpPost]
        [OAuthScope(TokenScope.PosVerify)]
        public ApiResult UndoVerifiedCheck(VerifyCheckModel model)
        {
            var result = new ApiResult();
            var log = Pos.SavePosApiLog(new ChannelPosApiLog
            {
                ApiName = PosApiType.UndoVerifiedCheck.ToString(),
                KeyVal = string.Format("{0}-{1}", model.CouponSerial, model.CouponCode),
                Memo = model.StoreCode,
                OauthToken = ChannelFacade.GetHeaderToken(),
                RequestType = (byte)PosApiType.UndoVerifiedCheck,
                RequestIp = Helper.GetClientIP(),
                RequestTime = DateTime.Now
            });

            if (model.CouponType == (int)CouponType.Serial)
            {
                var pez = Pp.PeztempPincodeGet(model.CouponSerial, PeztempServiceCode.None);
                //檢查是否有此序號
                if (pez != null && pez.IsLoaded)
                {
                    var bid = pez.Bid;
                    var go = PponFacade.GroupOrderGetByGuid((Guid)bid);
                    //檢查是否為廠商提供序號檔次
                    if (!Helper.IsFlagSet((int)go.Status, GroupOrderStatus.PEZevent))
                    {
                        result.Code = ApiResultCode.PcpPezEventNotFound;
                        SavePosApiLog(log, result);
                        return result;
                    }
                }
                else
                {
                    result.Code = ApiResultCode.PcpSerialNotFound;
                    SavePosApiLog(log, result);
                    return result;
                }

            }
            else
            {
                if (!Regex.IsMatch(model.CouponSerial, @"\d{4}-\d{4}"))
                {
                    result.Code = ApiResultCode.InputError;
                    SavePosApiLog(log, result);
                    return result;
                }
            }

            var verifyData = new List<VbsModel.VerifyCode>
            {
                new VbsModel.VerifyCode
                {
                    CouponSerial = model.CouponSerial,
                    CouponCode = (model.CouponType==(int)CouponType.Serial)? model.CouponSerial:model.CouponCode
                }
            };

            var sellerUserId = User.Identity.Id;

            #region check storeCode碼並取得userId

            var userId = SellerFacade.GetUserIdBySellerUserId(sellerUserId, model.StoreCode);
            if (userId == null)
            {
                result.Code = ApiResultCode.PcpStoreNotFound;
                SavePosApiLog(log, result);
                return result;
            }

            #endregion

            var memOriginal = VbsMemberUtility.GetVbsMemberShipByUserId((int)userId);

            var resultInfo = VBSFacade.VerifyCheckInfo(memOriginal, verifyData, model.CouponType == (int)CouponType.Serial, VbsCouponVerifyState.Verified); //單筆核銷
            result.Code = resultInfo.Item1;
            result.Data = resultInfo.Item2;
            result.Message = resultInfo.Item3;
            SavePosApiLog(log, result);
            return result;
        }

        /// <summary>
        /// Coupon 反核銷
        /// </summary>
        [HttpPost]
        [OAuthScope(TokenScope.PosVerify)]
        public ApiResult UndoVerifiedCoupon(VerifyCouponModel model)
        {
            var result = new ApiResult();
            var log = Pos.SavePosApiLog(new ChannelPosApiLog
            {
                ApiName = PosApiType.UndoVerifiedCoupon.ToString(),
                KeyVal = string.Format("{0}", model.TrustId),
                Memo = model.StoreCode,
                OauthToken = ChannelFacade.GetHeaderToken(),
                RequestType = (byte)PosApiType.UndoVerifiedCoupon,
                RequestIp = Helper.GetClientIP(),
                RequestTime = DateTime.Now
            });

            try
            {
                var ct = mp.CashTrustLogGet(model.TrustId);
                if (!ct.IsLoaded)
                {
                    result.Code = ApiResultCode.InvalidCoupon;
                    SavePosApiLog(log, result);
                    return result;
                }

                if (ct.Status != (int)TrustStatus.Verified)
                {
                    result.Code = ApiResultCode.CouponUnused;
                    SavePosApiLog(log, result);
                    return result;
                }

                var vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(ct.BusinessHourGuid ?? Guid.Empty);
                if (!vpd.IsLoaded)
                {
                    result.Code = ApiResultCode.InvalidCoupon;
                    SavePosApiLog(log, result);
                    return result;
                }

                //API UserID
                var sellerUserId = User.Identity.Id;

                #region 取得check storeCode碼並取得userId

                var userId = SellerFacade.GetUserIdBySellerUserId(sellerUserId, model.StoreCode);
                if (userId == null)
                {
                    result.Code = ApiResultCode.PcpStoreNotFound;
                    SavePosApiLog(log, result);
                    return result;
                }

                #endregion

                #region check 檔次與核銷帳號間的關係

                var memOriginal = VbsMemberUtility.GetVbsMemberShipByUserId(sellerUserId);
                var acl = mp.ResourceAclGetListByAccountId(memOriginal.AccountId);                
                if (!acl.Any())
                {
                    result.Code = ApiResultCode.InvalidCoupon;
                    SavePosApiLog(log, result);
                    return result;
                }

                if (acl.All(x => x.ResourceGuid != vpd.SellerGuid))
                {
                    result.Code = ApiResultCode.InvalidCoupon;
                    SavePosApiLog(log, result);
                    return result;
                }

                #endregion

                if (OrderFacade.UndoVerifiedStatus(model.TrustId, userId.ToString()))
                {
                    result.Code = ApiResultCode.Success;
                    result.Message = "Undo Success.";
                }
                else
                {
                    result.Code = ApiResultCode.Error;
                    result.Message = string.Format("Fail:{0}", ApiResultCode.InvalidCoupon);
                }
            }
            catch (Exception ex)
            {
                result.Code = ApiResultCode.Error;
                result.Message = string.Format("Error:{0}", ex.Message);
            }
            log.ReturnCode = (int)result.Code;
            SavePosApiLog(log, result);
            return result;
        }

        /// <summary>
        /// 成套禮券 取消核銷[檢查]
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.PosVerify)]
        public ApiResult UndoVerifiedCheckGroupCoupon(UndoVerifiedGroupCoupon model)
        {
            var result = new ApiResult();
            var log = Pos.SavePosApiLog(new ChannelPosApiLog
            {
                ApiName = PosApiType.UndoVerifiedCoupon.ToString(),
                KeyVal = string.Format("{0}", string.Join(",", model.TrustIds)),
                Memo = model.StoreCode,
                OauthToken = ChannelFacade.GetHeaderToken(),
                RequestType = (byte)PosApiType.UndoVerifiedCoupon,
                RequestIp = Helper.GetClientIP(),
                RequestTime = DateTime.Now
            });

            //檢查格式
            if (model.TrustIds.Count==0 || model.TrustIds.Where(p=>p==null).Any())
            {
                result.Code = ApiResultCode.CouponPartialFail;
                SavePosApiLog(log, result);
                return result;
            }
           
            var sellerUserId = User.Identity.Id;
            Guid storeGuid = new Guid("00000000-0000-0000-0000-000000000000");

            #region check storeCode碼並取得userId & storeGuid

            var userId = SellerFacade.GetUserIdBySellerUserId(sellerUserId, model.StoreCode, ref storeGuid);
            if (userId == null)
            {
                result.Code = ApiResultCode.PcpStoreNotFound;
                SavePosApiLog(log, result);
                return result;
            }

            #endregion

            var d = mp.CashTrustLogByTrustId(model.TrustIds);
            if (mp.CashTrustLogByTrustId(model.TrustIds).Where(p=>p.Status != (int)TrustStatus.Verified).Any())
            {
                result.Code = ApiResultCode.CouponUnused;
                SavePosApiLog(log, result);
                return result;
            }

            //檢查是否為同一個店家所核銷
            if (!mp.CashTrustLogIsSameStoreGuidByTrustId(model.TrustIds, storeGuid))
            {
                result.Code = ApiResultCode.PcpSerialsNotSameStore;
                SavePosApiLog(log, result);
                return result;
            }

            //檢查是否同一筆訂單
            var couponCols = Pp.CouponGet(model.TrustIds);
            if (couponCols.ToList().GroupBy(p => p.OrderDetailId).Count() != 1)
            {
                result.Code = ApiResultCode.PcpSerialsNotSameOrder;
                SavePosApiLog(log, result);
                return result;
            }

            //用trust id 反查 couponSerial 
            var verifyData = new List<VbsModel.VerifyCode>() { };
            foreach (Coupon coupon in couponCols)
            {
                verifyData.Add(
                    new VbsModel.VerifyCode
                    {
                        CouponSerial = coupon.SequenceNumber,
                        CouponCode = coupon.Code
                    }
                );
            }

            //檢查是否可被[取消]核銷
            var memOriginal = VbsMemberUtility.GetVbsMemberShipByUserId((int)userId);
            var resultInfo = VBSFacade.VerifyCheckInfo(memOriginal, verifyData, false, VbsCouponVerifyState.Verified); 
            result.Code = resultInfo.Item1;
            result.Data = resultInfo.Item2;
            result.Message = resultInfo.Item3;
            SavePosApiLog(log, result);
            return result;
        }


        /// <summary>
        /// 成套禮券 [取消核銷]
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.PosVerify)]
        public ApiResult UndoVerifiedGroupCoupon(UndoVerifiedGroupCoupon model)
        {
            var result = new ApiResult();
            var log = Pos.SavePosApiLog(new ChannelPosApiLog
            {
                ApiName = PosApiType.UndoVerifiedCoupon.ToString(),
                KeyVal = string.Format("{0}", string.Join(",", model.TrustIds)),
                Memo = model.StoreCode,
                OauthToken = ChannelFacade.GetHeaderToken(),
                RequestType = (byte)PosApiType.UndoVerifiedCoupon,
                RequestIp = Helper.GetClientIP(),
                RequestTime = DateTime.Now
            });
            
            try
            {
                //檢查格式
                if (model.TrustIds.Count == 0 || model.TrustIds.Where(p => p == null).Any())
                {
                    result.Code = ApiResultCode.CouponPartialFail;
                    SavePosApiLog(log, result);
                    return result;
                }

                var sellerUserId = User.Identity.Id;
                var ctlogList = mp.CashTrustLogByTrustId(model.TrustIds).ToList();
                
                ApiResultCode resultCode = ApiResultCode.Error;
                //檢查狀態是否可取消核銷
                if (OrderFacade.UndoGroupCouponVerifiedStatusCheck(ctlogList, User.Identity.Id, model.TrustIds, model.StoreCode, ref resultCode))
                {
                    //取得check storeCode碼並取得userId
                    var userId = SellerFacade.GetUserIdBySellerUserId(sellerUserId, model.StoreCode);
                    string resultMessage = string.Empty;

                    //真的執行核銷取消
                    if (OrderFacade.UndoGroupCouponVerifiedStatus(model.TrustIds, userId.ToString(), ref resultMessage))
                    {
                        result.Code = ApiResultCode.Success;
                        result.Message = "Undo Success";
                    }
                    else
                    {
                        result.Code = ApiResultCode.Error;
                        result.Message = string.Format("Fail:{0}", resultMessage);
                    }
                }
                else
                {
                    result.Code = resultCode;
                }
            }
            catch (Exception ex)
            {
                result.Code = ApiResultCode.Error;
                result.Message = string.Format("Error:{0}", ex.Message);
            }

            log.ReturnCode = (int)result.Code;
            SavePosApiLog(log, result);
            return result;
        }

        #endregion

        #region private method

        private void SavePosApiLog(ChannelPosApiLog log, ApiResult result)
        {
            log.ReturnCode = (int)result.Code;
            log.ReturnMessage = result.Message;
            Pos.SavePosApiLog(log);
        }
        /// <summary>
        /// 成套票券的憑證，API以明碼方式輸出
        /// APP加密後以QrCode呈現
        /// 此為相對應的解密程式
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        private string TransQRCode(string code)
        {
            string removeString = "GC=";
            code=code.Replace(removeString, "");
            string key = "17LifeGood";
            string result = "";
            try
            {
                for (int i = 0; i < code.Length; i++)
                {
                    int urlAscii = code[i];

                    for (int k = 0; k < key.Length; k++)
                    {
                        int keyAscii = key[k];
                        urlAscii = urlAscii ^ keyAscii;
                    }
                    result += (char)urlAscii;
                }                
            }
            catch (Exception ex)
            {
                return "轉換失敗"+ex.Message;
            }
            return result;
        }

        #endregion
    }
}

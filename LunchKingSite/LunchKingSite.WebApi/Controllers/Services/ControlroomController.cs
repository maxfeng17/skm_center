﻿using System;
using System.Linq;
using System.Web.Http;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebApi.Core.Attributes;

namespace LunchKingSite.WebApi.Controllers.Services
{
    /// <summary>
    /// 
    /// </summary>
    [RequireHttps]
    public class ControlroomController : ApiController
    {
        private IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        /// <summary>
        /// 後臺會員管理，取得手機會員的綁定狀況
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "CustomerCare,CustomerCare1,CustomerCare2,Administrator")]
        public ApiResult GetdMobileMemberBindInfo(UserIdInputModel model)
        {
            if (model == null || model.UserId == 0)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = "錯誤"
                };
            }
            MobileMember mm = MemberFacade.GetMobileMember(model.UserId);

            string statusDesc;
            if (mm.IsLoaded)
            {
                if (mm.Status == (int)MobileMemberStatusType.Activated)
                {
                    statusDesc = "已認證";
                }
                else
                {
                    statusDesc = "未認證";
                }
            }
            else
            {
                return new ApiResult
                {
                    Code = ApiResultCode.DataNotFound,
                    Message = "資料不存在"
                };
            }

            bool bindEnabled = false;
            bool unbindEnabled = false;
            if (mm.Status == (int)MobileMemberStatusType.Activated)
            {
                unbindEnabled = MemberFacade.IsMobileMemberRemovable(mm);
            }
            else
            {
                bindEnabled = true;
            }
            var mais = mp.MobileAuthInfoGetList(mm.MobileNumber);
            int smsSentCount = mais.Sum(t => t.QueryTimes);

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = new
                {
                    mm.Status,
                    mm.MobileNumber,
                    StatusDesc = statusDesc,
                    BindEnabled = bindEnabled,
                    UnbindEnabled = unbindEnabled,
                    SmsSentCount = smsSentCount
                },
                Message = "成功"
            };
        }

        /// <summary>
        /// 重設發送簡訊次數，給web頁面使用
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "CustomerCare,CustomerCare1,CustomerCare2,Administrator")]
        public ApiResult ResetMobileSmsSentCount(MobileInputModel model)
        {
            var mais = mp.MobileAuthInfoGetList(model.Mobile);
            foreach (var mai in mais)
            {
                mai.QueryTimes = 0;
                MemberFacade.SetMobileAuthInfo(mai);
            }
            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = 0,
                Message = "成功"
            };
        }

        /// <summary>
        /// 解除手機綁定
        /// </summary>        
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "CustomerCare,CustomerCare1,CustomerCare2,Administrator")]
        public ApiResult UnbindMobile(MobileInputModel model)
        {
            if (model == null || string.IsNullOrEmpty(model.Mobile))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = "錯誤"
                };
            }
            MobileMember mm = MemberFacade.GetMobileMember(model.Mobile);
            if (mm.IsLoaded == false)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = "錯誤"
                };
            }
            if (MemberFacade.IsMobileMemberRemovable(mm))
            {
                if (MemberFacade.DeleteMobileMember(mm))
                {
                    return new ApiResult
                    {
                        Code = ApiResultCode.Success,
                        Message = "成功"
                    };
                }
            }
            return new ApiResult
            {
                Code = ApiResultCode.Error,
                Message = "解除綁定失敗"
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "CustomerCare,CustomerCare1,CustomerCare2,Administrator")]
        public ApiResult BindMobile(MobileInputModel model)
        {
            bool dataReady = false;
            MobileMember mm = MemberFacade.GetMobileMember(model.Mobile);
            Member mem = null;
            if (mm.IsLoaded)
            {
                mem = MemberFacade.GetMember(mm.UserId);
                if (mem.IsLoaded)
                {
                    dataReady = true;
                }
            }
            if (dataReady == false)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError,
                    Message = "錯誤"
                };
            }
            try
            {
                MemberFacade.SetMobileMemberActive(mem, mm, User.Identity.Name);

                return new ApiResult
                {
                    Code = ApiResultCode.Success,
                    Message = "成功"
                };
            }
            catch (Exception ex)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError,
                    Message = ex.Message
                };
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public class UserIdInputModel
        {
            /// <summary>
            /// 
            /// </summary>
            public int UserId { get; set; }
        }

        /// <summary>
        /// 
        /// </summary>
        public class MobileInputModel
        {
            /// <summary>
            ///
            /// </summary>
            public string Mobile { get; set; }
        }
        /// <summary>
        /// 
        /// </summary>
        public class AccountInputModel
        {
            /// <summary>
            ///
            /// </summary>
            public string Account { get; set; }
        }
    }
}

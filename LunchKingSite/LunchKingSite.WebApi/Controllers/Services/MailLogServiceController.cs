﻿using System;
using System.Web.Http;
using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;

namespace LunchKingSite.WebApi.Controllers.Services
{
    /// <summary>
    /// 這組bapi是給網站本身使用 ，權限以cookie為主
    /// </summary>
    [RoutePrefix("service/mailLog")]
    //[Authorize(Roles = "CustomerCare,CustomerCare1,CustomerCare2,ProductionManager,Itstaff")]
    public class MailLogServiceController : ApiController
    {
        private ILog logger = LogManager.GetLogger(typeof(MailLogServiceController));
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        
        /// <summary>
        /// 搜尋maillog
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("query")]        
        public ApiResult Query(MailLogFacade.QueryMailLogFilter model)
        {
            var mailLogs = MailLogFacade.GetMailLogList(model);
            return new ApiResult()
            {
                Code = ApiResultCode.Success,
                Data = mailLogs
            };
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("getContent")]
        public ApiResult GetContent(dynamic model)
        {
            int id = 0;
            if (model.Id != null)
            {
                id = (int)model.Id;
            }
            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = MailLogFacade.GetContent(id)
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("resend")]
        public ApiResult Resend(dynamic model)
        {
            int id = model.id;
            try
            {
                return new ApiResult
                {
                    Code = ApiResultCode.Success,
                    Data = MailLogFacade.Resend(id)
                };
            }
            catch (Exception ex)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Data = ex.Message
                };
            }            
        }

        
        
    }
}

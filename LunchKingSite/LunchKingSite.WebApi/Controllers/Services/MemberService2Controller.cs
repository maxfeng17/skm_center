﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.BizLogic.Models;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.I18N;
using LunchKingSite.WebApi.Core;
using LunchKingSite.WebApi.Core.Attributes;
using LunchKingSite.WebApi.Core.OAuth;
using LunchKingSite.WebApi.Models;
using LunchKingSite.WebLib.Component;

namespace LunchKingSite.WebApi.Controllers.Services
{
    /// <summary>
    /// 因受不了 MemberServiceController 的 MvcStyleBinding 造成的限制， 該限制僅能上傳簡單的keyvalue style json
    /// 所以再開一個 MemberService2Controller
    /// </summary>
    [RequireHttps]
    public class MemberService2Controller : BaseController
    {
        private static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static ILog logger = LogManager.GetLogger(typeof(MemberService2Controller));
        /// <summary>
        /// 取得會員資料
        /// </summary>
        [HttpPost]
        [Authorize]
        [ApiUserValidator]
        public ApiResult GetMemberDetailData(MemberDetailDataInputModel model)
        {
            string errorMessage;
            ApiMemberDetailV2 data = PponDealApiManager.ApiMemberDetailGet(
                User.Identity.Name, out errorMessage, ApiUserManager.IsOldAppUserId(model.UserId))
                as ApiMemberDetailV2;

            ApiResult result;
            if (string.IsNullOrEmpty(errorMessage))
            {
                result = new ApiResult
                {
                    Code = ApiResultCode.Success,
                    Data = data
                };
            }
            else
            {
                result = new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = errorMessage
                };
            }
            return result;
        }

        /// <summary>
        /// 修改會員資料
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [ApiUserValidator]
        public ApiResult SaveMemberDetailData(ApiMemberDetailInputModel model)
        {
            ApiResult result = new ApiResult();
            string message;
            if (PponDealApiManager.ApiMemberDetailSet(model, User.Identity.Name, out message))
            {
                string errorMessage;
                ApiMemberDetailV2 data = PponDealApiManager.ApiMemberDetailGet(
                    User.Identity.Name, out errorMessage, ApiUserManager.IsOldAppUserId(model.UserId))
                    as ApiMemberDetailV2;
                if (string.IsNullOrEmpty(errorMessage))
                {
                    result = new ApiResult
                    {
                        Code = ApiResultCode.Success,
                        Data = data
                    };
                }
                else
                {
                    result = new ApiResult
                    {
                        Code = ApiResultCode.Error,
                        Message = errorMessage
                    };
                }
            }
            else
            {
                result.Code = ApiResultCode.Error;
                result.Message = message;
            }
            return result;
        }

        /// <summary>
        /// 會員手機號碼註冊時，更新簡單的基本資料
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ApiUserValidator]
        public ApiResult SaveMobileMemberRegisterInfo(MobileMemberRegisterInfoInputModel model)
        {
            string message;
            bool success = PponDealApiManager.SaveMobileMemberRegisterInfo(model, User.Identity.Name, out message);
            if (success == false)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = message
                };
            }
            var data = PponDealApiManager.GetMobileMemberRegisterInfo(User.Identity.Name);
            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = data
            };
        }

        /// <summary>
        /// 取得會員忘記密碼的選項
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ApiUserValidator]
        public ApiResult GetMemberForgotPasswordOptions(MobileMemberPasswordOptionsInputModel model)
        {
            string mobile = model.UserName;

            bool emailEnabled;
            bool mobileEnabled;
            string emailDisplay;
            string mobileDisplay;

            Member mem;
            if (RegExRules.CheckMobile(model.UserName))
            {
                mem = MemberFacade.GetMemberByMobile(mobile);
            }
            else
            {
                mem = MemberFacade.GetMember(model.UserName);
            }
            if (mem.IsLoaded)
            {
                MemberFacade.ChecMemberForgetOptions(mem.UniqueId,
                    out emailEnabled, out mobileEnabled, out emailDisplay, out mobileDisplay);
                return new ApiResult
                {
                    Code = ApiResultCode.Success,
                    Data = new
                    {
                        EmailEnabled = emailEnabled,
                        EmailDisplay = emailDisplay,
                        MobileEnabled = mobileEnabled,
                        MobileDisplay = mobileDisplay
                    }
                };
            }
            return new ApiResult
            {
                Code = ApiResultCode.AccountNotFound,
                Message = Phrase.ApiResultCodeMemberNotFound
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ApiUserValidator]
        public ApiResult ForgotPassword(ForgotPasswordInputModel model)
        {
            Member mem = MemberFacade.GetMember(model.UserName);
            if (mem.IsLoaded == false)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.AccountNotFound,
                    Message = "會員資料不存在"
                };
            }
            if (string.IsNullOrEmpty(mem.UserEmail) || RegExRules.CheckEmail(mem.UserEmail) == false)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = "聯絡信箱未填寫或格式不正確"
                };
            }

            string display = RegExRules.CheckMobile(model.UserName) ? "mobile" : "email";

            MemberAuthInfo mai = MemberFacade.GetOrAddMemberAuthInfo(mem.UniqueId, mem.UserEmail);
            MemberFacade.ResetMemberAuthInfo(mai);

            MemberFacade.SendForgetPasswordMail(
                mem.UserEmail, mem.UniqueId.ToString(), mai.ForgetPasswordKey, mai.ForgetPasswordCode, display);
            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Message = "信件已寄出"
            };
        }

        #region 會員信用卡管理

        /// <summary>
        /// 會員查詢單張信用卡
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [ApiUserValidator]
        public ApiResult GetMemberCreditCardByGuid(GetOrDeleteMemberCreditCardInputModel input)
        {
            var userId = MemberFacade.GetUniqueId(User.Identity.Name);
            var cards = MemberFacade.GetMemberCreditCardList(userId);
            var card = cards.FirstOrDefault(x => x.CardGuid == input.CardGuid);
            if (card != null)
            {
                return new ApiResult { Code = ApiResultCode.Success, Data = card };
            }
            return new ApiResult { Code = ApiResultCode.DataNotFound };
        }

        /// <summary>
        /// 會員查詢有效信用卡列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [ApiUserValidator]
        public ApiResult GetMemberCreditCardList(DefaultInputModel input)
        {
            //因為信用卡有效年份只有西元後兩位，所以要加上2000
            int twoKYears = 2000;
            var userId = MemberFacade.GetUniqueId(User.Identity.Name);
            //只列沒過期的信用卡
            List<MemberCreditCardInfo> cards = MemberFacade.GetMemberCreditCardList(userId)
                .Where(card => int.Parse((int.Parse(card.ValidYear) + twoKYears).ToString() + int.Parse(card.ValidMonth).ToString().PadLeft(2, '0')) >= int.Parse(DateTime.Today.ToString("yyyyMM"))).ToList();
            if (cards.Count > 0)
            {
                return new ApiResult { Code = ApiResultCode.Success, Data = cards };
            }
            return new ApiResult { Code = ApiResultCode.DataNotFound };
        }

        /// <summary>
        /// 會員新增信用卡
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [ApiUserValidator]
        public ApiResult AddMemberCreditCard(AddMemberCreditCardInputModel input)
        {
            string errorMessage;
            var isSuccess = MemberFacade.AddOrUpdateMemberCreditCard(Guid.Empty,
                User.Identity.Name, input.CardNo, input.ValidYear, input.ValidMonth, input.CardName, out errorMessage);

            return isSuccess
                ? new ApiResult { Code = ApiResultCode.Success }
                : new ApiResult { Code = ApiResultCode.SaveFail, Message = errorMessage };
        }

        /// <summary>
        /// 會員編輯信用卡
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [ApiUserValidator]
        public ApiResult EditMemberCreditCard(EditMemberCreditCardInputModel input)
        {
            var userId = MemberFacade.GetUniqueId(User.Identity.Name);
            var card = mp.MemberCreditCardGetByGuidUserId(input.CardGuid, userId);
            if (!card.IsLoaded)
            {
                return new ApiResult { Code = ApiResultCode.DataNotFound };
            }

            //日期驗證
            var errorMessage = string.Empty;

            if (!string.IsNullOrEmpty(input.ValidYear) && !string.IsNullOrEmpty(input.ValidMonth))
            {
                if (RegExRules.CheckCreditCardValidDate(input.ValidYear, input.ValidMonth, out errorMessage))
                {
                    card.CreditCardYear = input.ValidYear;
                    card.CreditCardMonth = input.ValidMonth;
                }
            }

            //信用卡號驗證
            if (string.IsNullOrEmpty(errorMessage))
            {
                if (!string.IsNullOrEmpty(input.CardNo) && //卡號非空值，表示有異動
                    RegExRules.CheckCreditCardNumber(input.CardNo, config.CheckCreditCardEnabled, out errorMessage))
                {
                    //卡號加密
                    card.CreditCardInfo = MemberFacade.EncryptCreditCardInfo(input.CardNo, card.EncryptSalt);
                    card.CreditCardTail = input.CardNo.Substring(12, 4);

                    var bankInfo = CreditCardPremiumManager.GetCreditCardBankInfo(input.CardNo);
                    if (bankInfo.IsLoaded)
                    {
                        card.BankId = bankInfo.BankId;
                        card.CardType = bankInfo.CardType ?? (int)CreditCardType.None;
                    }
                }
            }

            if (string.IsNullOrEmpty(errorMessage))
            {
                if (!string.IsNullOrEmpty(input.CardName))
                {
                    card.CreditCardName = input.CardName;
                }
            }

            if (string.IsNullOrEmpty(errorMessage))
            {
                card.ModifyTime = DateTime.Now;
                int saveCnt = mp.MemberCreditCardSet(card);
                if (saveCnt > 0)
                {
                    return new ApiResult { Code = ApiResultCode.Success };
                }
            }
            return new ApiResult { Code = ApiResultCode.SaveFail, Message = errorMessage };
        }

        /// <summary>
        /// 會員刪除信用卡
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [ApiUserValidator]
        public ApiResult DeleteMemberCreditCard(GetOrDeleteMemberCreditCardInputModel input)
        {
            string errorMessage;
            var isSuccess = MemberFacade.DeleteMemberCreditCard(User.Identity.Name, input.CardGuid, out errorMessage);

            return isSuccess
                ? new ApiResult { Code = ApiResultCode.Success }
                : new ApiResult { Code = ApiResultCode.Error, Message = errorMessage };
        }

        #endregion

        /// <summary>
        /// 新增收件聯絡人
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ApiUserValidator]
        public ApiResult InsertMemberAddressee(InsertMemberAddresseeModel model)
        {
            var userName = HttpContext.Current.User.Identity.Name;
            if (string.IsNullOrWhiteSpace(userName))
            {
                return new ApiResult { Code = ApiResultCode.UserNoSignIn };
            }

            try
            {
                var memberDelivery = PponDealApiManager.InsertMemberAddressee(userName, model.MemberDelivery);
                return new ApiResult
                {
                    Code = ApiResultCode.Success,
                    Data = new { DeliveryId = memberDelivery.Id }
                };
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ", input: " + model);
                return new ApiResult { Code = ApiResultCode.InputError };
            }
        }

        /// <summary>
        /// 提供記錄訪問深度API
        /// </summary>
        /// <param name="model">記錄類型與次數</param>
        /// <returns></returns>
        [OAuthScope(TokenScope.Deal)]
        [RequireHttps]
        [HttpPost]
        public ApiResult TrackingViewDealCount(ViewDealCountInputModel model)
        {
            ApiResult result = new ApiResult();
            if (User.Identity.IsAuthenticated == false)
            {
                result.Code = ApiResultCode.UserNoSignIn;
                return result;
            }
            if (model == null || model.TrackingType != 1 || model.ViewCount <= 0)
            {
                result.Code = ApiResultCode.InputError;
                return result;
            }

            try
            {
                logger.Info("TrackingViewDealCount DeviceType=" + DeviceType.ToString("g"));

                PponDealApiManager.InsertFrontViewCount(model.TrackingType, model.ViewCount, User.Identity.Id, DeviceType);
                result.Code = ApiResultCode.Success;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ", input: " + model);
                result.Code = ApiResultCode.Error;
            }
            return result;
        }

        #region 會員訂單

        /// <summary>
        /// 會員 Profile 總覽
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost, HttpGet]
        [ApiUserValidator]
        public ApiResult GetMemberOverview(BasicInputModel model)
        {
            return new ApiResult { Code = ApiResultCode.Success, Data = null };
        }


        /// <summary>
        /// [v3.0] 取得會員訂單總覽
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost, HttpGet]
        [ApiUserValidator]
        public ApiResult GetMemberOrderOverview(BasicInputModel model)
        {
            int uniqueId = User.Identity.Id;
            if (uniqueId == 0)
            {
                return new ApiResult { Code = ApiResultCode.UserNoSignIn };
            }
            Member mem = MemberFacade.GetMember(User.Identity.Id);
            if (mem.UniqueId == 0)
            {
                return new ApiResult { Code = ApiResultCode.UserNoSignIn };
            }
            var data = MemberFacade.GetMemberOrderOverview(uniqueId);

            #region 防盜刷

            if (mem.IsFraudSuspect)
            {
                var allOrders = data.FirstOrDefault(t => t.MainFilterType == MemberOrderMainFilterType.All);
                var pponOrders = data.FirstOrDefault(t => t.MainFilterType == MemberOrderMainFilterType.Ppon);
                if (allOrders != null && pponOrders != null)
                {
                    allOrders.OrderCount = allOrders.OrderCount - pponOrders.OrderCount;
                    pponOrders.OrderCount = 0;
                }
            }

            #endregion

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = data
            };
        }

        /// <summary>
        /// [v3.0] 會員訂單列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost, HttpGet]
        [ApiUserValidator]
        public ApiResult GetMemberOrders(GetMemberOrdersModel model)
        {
            int uniqueId = User.Identity.Id;
            if (uniqueId == 0)
            {
                return new ApiResult { Code = ApiResultCode.UserNoSignIn };
            }
            Member mem = MemberFacade.GetMember(uniqueId);
            if (mem.UniqueId == 0)
            {
                return new ApiResult { Code = ApiResultCode.UserNoSignIn };
            }

            #region 防盜刷

            var data = MemberFacade.GetApiMemberOrdersByUser(uniqueId, model.SubFilterType, 0);
            //if (mem.IsFraudSuspect && model.SubFilterType == MemberOrderSubFilterType.PponCanUse)
            if (mem.IsFraudSuspect)
            {
                //把待用憑證過瀘掉
                data = data.Where(t => t.PponTotalCouponCount > 0 && t.PponCouponCount == 0).ToList();
            }

            #endregion

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = data
            };
        }

        /// <summary>
        /// [v3.0] 會員訂單分類 (APP未採用此版本)
        /// </summary>
        /// <returns></returns>
        [HttpPost, HttpGet]
        public ApiResult GetMemberOrderFilters()
        {
            int? uniqueId = null;
            if (User.Identity.Id > 0)
            {
                uniqueId = User.Identity.Id;
            }

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = MemberFacade.GetOrderFilters(uniqueId)
            };
        }

        /// <summary>
        /// 更新會員發票載具
        /// </summary>
        /// <param name="userId"></param>
        [HttpPost]
        [RequireHttps]
        [ApiUserValidator]
        public ApiResult SaveMemberCarrierInfo(MemberCarrierInputModel model)
        {
            string methodName = "UpdateMemberCarrierInfo";
            ApiResult result = new ApiResult();

            //取得會員名稱
            string userName = HttpContext.Current.User.Identity.Name;
            Member member = MemberFacade.GetMember(userName);
            int memberUniqueId = member.UniqueId;
            string userId = model.UserId;
            string carrierType = model.CarrierType;
            string carrierId = model.CarrierId;
            bool isSuccess = true;

            System.Text.StringBuilder builder = new System.Text.StringBuilder();

            try
            {
                if (string.IsNullOrEmpty(carrierType) || string.IsNullOrEmpty(carrierId))
                {
                    result.Code = ApiResultCode.Error;
                    result.Data = "載具資料異常";
                    isSuccess = false;
                }
                if (string.IsNullOrEmpty(userName) || memberUniqueId == 0)
                {
                    result.Code = ApiResultCode.Error;
                    result.Data = "尚未登入";
                    isSuccess = false;
                }
                if (isSuccess)
                {
                    int cType = default(int);
                    int.TryParse(carrierType, out cType);
                    switch (cType)
                    {
                        case (int)CarrierType.Phone:

                            if (carrierId.Length == 8 && carrierId.StartsWith("/"))
                            {
                                bool isValidPhoneCarrierId = MemberFacade.IsValidPhoneCarrierId(carrierId);

                                if (isValidPhoneCarrierId)
                                {
                                    member.CarrierType = (int)CarrierType.Phone;
                                    member.CarrierId = carrierId;
                                    mp.MemberSet(member);

                                    result.Code = ApiResultCode.Success;
                                    result.Data = string.Empty;
                                }
                                else
                                {
                                    result.Code = ApiResultCode.InputError;
                                    result.Data = "此手機條碼無效，請重新輸入";
                                }
                            }
                            else
                            {
                                result.Code = ApiResultCode.InputError;
                                result.Data = "此手機條碼無效，請重新輸入";
                            }

                            break;
                        case (int)CarrierType.PersonalCertificate:

                            string pattern = @"^[A-Z][A-Z]\d{14}$";
                            System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(pattern);
                            bool flag = regex.IsMatch(carrierId);

                            if (flag)
                            {
                                member.CarrierType = (int)CarrierType.PersonalCertificate;
                                member.CarrierId = carrierId;
                                mp.MemberSet(member);

                                result.Code = ApiResultCode.Success;
                                result.Data = string.Empty;
                            }
                            else
                            {
                                result.Code = ApiResultCode.InputError;
                                result.Data = "此自然人憑證條碼無效，請重新輸入";
                            }
                            break;
                        default:
                            result.Code = ApiResultCode.InputError;
                            result.Data = I18N.Phrase.ApiResultCodeError;
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                builder.AppendLine(ex.Message);
                result.Code = ApiResultCode.Error;
                result.Data = string.Empty;
            }
            finally
            {
                //ProposalFacade.ProposalPerformanceLogSet(GetMethodName(methodName), builder.ToString(), "sys");
                SetApiLog(GetMethodName(methodName), userId, model, result, LunchKingSite.Core.Helper.GetClientIP());
            }
            return result;
        }

        /// <summary>
        /// 取得會員發票載具
        /// </summary>
        /// <param name="userId"></param>
        [HttpPost]
        [RequireHttps]
        [ApiUserValidator]
        public ApiResult GetMemberCarrierInfo(MemberCarrierModel model)
        {
            string methodName = "GetMemberCarrierInfo";
            ApiResult result = new ApiResult();

            //取得會員名稱
            string userName = HttpContext.Current.User.Identity.Name;
            Member member = MemberFacade.GetMember(userName);
            int memberUniqueId = member.UniqueId;
            string userId = model.UserId;
            string carrierType = model.CarrierType;
            string carrierId = string.Empty;
            bool isSuccess = true;

            try
            {
                if (string.IsNullOrEmpty(carrierType))
                {
                    result.Code = ApiResultCode.Error;
                    result.Data = "載具資料異常";
                    isSuccess = false;
                }
                if (string.IsNullOrEmpty(userName) || memberUniqueId == 0)
                {
                    result.Code = ApiResultCode.Error;
                    result.Data = "尚未登入";
                    isSuccess = false;
                }
                if (carrierType != ((int)CarrierType.PersonalCertificate).ToString()
                    && carrierType != ((int)CarrierType.Phone).ToString())
                {
                    result.Code = ApiResultCode.Error;
                    result.Data = "載具資料異常";
                    isSuccess = false;
                }
                if (isSuccess)
                {
                    int cType = default(int);
                    int.TryParse(carrierType, out cType);
                    if (member.CarrierType == cType)
                    {
                        carrierId = member.CarrierId;

                        result.Code = ApiResultCode.Success;
                        result.Data = carrierId;
                    }
                    else
                    {
                        result.Code = ApiResultCode.Success;
                        result.Data = string.Empty;
                    }
                }
            }
            catch
            {
                result.Code = ApiResultCode.Error;
                result.Data = string.Empty;
            }
            finally
            {
                SetApiLog(GetMethodName(methodName), userId, model, result, LunchKingSite.Core.Helper.GetClientIP());
            }
            return result;
        }
        #endregion

        #region LINE 註冊

        [HttpPost]
        [ApiUserValidator]
        public ApiResult CheckMemberExists([FromBody]CheckMemberExistsInputModel model)
        {
            if (model.BindingSource == null)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError,
                    Message = "未填寫 BindingSource"
                };
            }
            CheckMemberExistsOutputModel result = new CheckMemberExistsOutputModel
            {
                Exists = false,
                Activiate = false
            };
            if (model != null && string.IsNullOrEmpty(model.Account) == false)
            { 
                var mm = MemberFacade.GetMobileMember(model.Account);
                var mem = MemberFacade.GetMember(model.Account);                
                if (mm.IsLoaded)
                {
                    bool linkExists = mp.MemberLinkGet(mm.UserId, model.BindingSource.Value).IsLoaded;
                    bool activiate = (MobileMemberStatusType)mm.Status == MobileMemberStatusType.Activated;
                    result = new CheckMemberExistsOutputModel
                    {
                        Exists = true,
                        Activiate = activiate,
                        SourceExists = linkExists
                    };
                }
                else if (mem.IsLoaded)
                {
                    bool linkExists = mp.MemberLinkGet(mem.UniqueId, model.BindingSource.Value).IsLoaded;
                    bool activiate = LunchKingSite.Core.Helper.IsFlagSet(mem.Status, MemberStatusFlag.Active17Life);
                    result = new CheckMemberExistsOutputModel
                    {
                        Exists = true,
                        Activiate = activiate,
                        SourceExists = linkExists
                    };
                }                
            }
            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = result
            };
        }

        [HttpPost]
        [ApiUserValidator]
        public ApiResult BindExternalMember([FromBody]BindExternalMemberInputModel model)
        {
            SingleSignOnSource signOnSource;
            if (model == null || SingleSignOnSource.TryParse(model.SignType, out signOnSource) == false)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError,
                    Message = I18N.Phrase.ApiReturnCodeInputError
                };
            }
            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.UserNoSignIn,
                    Message = "尚未登入"
                };
            }
            int memberId = MemberFacade.GetUniqueId(HttpContext.Current.User.Identity.Name);
            if (memberId == 0)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.AccountNotFound,
                    Message = "尚未登入"
                };
            }
            string errorMessage;

            bool result = MemberUtility.BindExternalMember(
                memberId, signOnSource, model.Token, model.PezMemId, model.PezPassword, out errorMessage);
            if (result == false)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = errorMessage
                };
            }
            return new ApiResult
            {
                Code = ApiResultCode.Success
            };
        }

        #endregion

        #region input models

        /// <summary>
        /// 
        /// </summary>
        public class BasicInputModel : IApiUserInputModel
        {
            /// <summary>
            /// App Id
            /// </summary>
            public string UserId { get; set; }
        }

        /// <summary>
        /// 訂單列表Input
        /// </summary>
        public class GetMemberOrdersModel : IApiUserInputModel
        {
            /// <summary>
            /// App Id
            /// </summary>
            public string UserId { get; set; }

            /// <summary>
            ///  訂單分類
            /// </summary>
            public MemberOrderSubFilterType SubFilterType { get; set; }
        }

        /// <summary>
        /// 
        /// </summary>
        public class MemberDetailDataInputModel : IApiUserInputModel
        {
            /// <summary>
            /// App Id
            /// </summary>
            public string UserId { get; set; }
        }
        /// <summary>
        /// 
        /// </summary>
        public class MobileMemberRegisterInfoInputModel : ApiMobileMemberRegisterInfo, IApiUserInputModel
        {
            /// <summary>
            /// App Id
            /// </summary>
            public string UserId { get; set; }
        }
        /// <summary>
        /// 
        /// </summary>
        public class ApiMemberDetailInputModel : ApiMemberDetailV2, IApiUserInputModel
        {
            /// <summary>
            /// App Id
            /// </summary>
            public string UserId { get; set; }
        }
        /// <summary>
        /// 
        /// </summary>
        public class MobileMemberPasswordOptionsInputModel : IApiUserInputModel
        {
            /// <summary>
            /// App Id
            /// </summary>
            public string UserId { get; set; }

            /// <summary>
            /// 
            /// </summary>
            public string UserName { get; set; }
        }
        /// <summary>
        /// 
        /// </summary>
        public class ForgotPasswordInputModel : IApiUserInputModel
        {
            /// <summary>
            /// 
            /// </summary>
            public string UserId { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string UserName { get; set; }
        }
        /// <summary>
        /// 
        /// </summary>
        public class InsertMemberAddresseeModel : IApiUserInputModel
        {
            /// <summary>
            /// 
            /// </summary>
            public string UserId { get; set; }
            /// <summary>
            /// 聯絡人
            /// </summary>
            public ApiMemberDelivery MemberDelivery { get; set; }
        }

        public class ViewDealCountInputModel
        {
            public int TrackingType { get; set; }
            public int ViewCount { get; set; }
        }
        public class MemberCarrierInputModel : IApiUserInputModel
        {
            /// <summary>
            /// App Id
            /// </summary>
            public string UserId { get; set; }

            /// <summary>
            /// CarrierType
            /// </summary>
            public string CarrierType { get; set; }

            /// <summary>
            ///  CarrierId
            /// </summary>
            public string CarrierId { get; set; }
        }
        public class MemberCarrierModel : IApiUserInputModel
        {
            /// <summary>
            /// App Id
            /// </summary>
            public string UserId { get; set; }

            /// <summary>
            /// CarrierType
            /// </summary>
            public string CarrierType { get; set; }
        }
        public class CheckMemberExistsInputModel : IApiUserInputModel
        {
            /// <summary>
            /// 
            /// </summary>
            public string UserId { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string Account { get; set; }

            public SingleSignOnSource? BindingSource { get; set; }
        }
        public class CheckMemberExistsOutputModel
        {
            public bool Exists { get; set; }
            public bool Activiate { get; set; }
            public bool SourceExists { get; set; }
        }


        public class BindExternalMemberInputModel : IApiUserInputModel
        {
            
            public string UserId { get; set; }
            public string Token { get; set; }
            public string PezMemId { get; set; }
            public string PezPassword { get; set; }
            public string SignType { get; set; }

        }

        #endregion
    }
}

﻿using System;
using System.Web;
using System.Web.Http;
using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebApi.Models;
using LunchKingSite.WebLib;
using LunchKingSite.WebLib.Component;
using System.Linq;
using Newtonsoft.Json.Linq;
using System.Web.Http.Results;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Text;
using LunchKingSite.BizLogic.Model;
using PaymentType = LunchKingSite.Core.PaymentType;
using System.Collections.Generic;
using LunchKingSite.Core.Interface;
using LunchKingSite.Core.Models.PponEntities;
using LunchKingSite.WebApi.Models.PPon;
using LunchKingSite.WebLib.Models.Mobile;
using System.Runtime.Caching;
using LunchKingSite.BizLogic.Models.API;

namespace LunchKingSite.WebApi.Controllers.Services
{
    /// <summary>
    /// 這組bapi是給網站本身使用 ，權限以cookie為主
    /// </summary>
    public class PponServiceController : BaseController
    {
        private IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private ISellerProvider sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        private IPponEntityProvider pep = ProviderFactory.Instance().GetProvider<IPponEntityProvider>();
        private IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private ILocationProvider lp = ProviderFactory.Instance().GetProvider<ILocationProvider>();
        private IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        private ILog logger = LogManager.GetLogger(typeof (PponController).Name);
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public dynamic AddEdm([FromBody]SubscribeModel model)
        {
            string msg;
            bool result;
            bool isShowPponSelect = false;
            bool isEmailValid = RegExRules.CheckEmail(model.Email);

            if (isEmailValid)
            {
                Subscription subscription = pp.SubscriptionGet(model.Email, model.CategoryId);

                if (subscription.IsLoaded == false)
                {
                    subscription.CategoryId = model.CategoryId;
                    subscription.CreateTime = DateTime.Now;
                    subscription.Email = model.Email;
                    subscription.Status = 1;
                    pp.SubscriptionSet(subscription);
                    msg = "訂閱成功！";
                    isShowPponSelect = IsCheckPponSelect(model.Email, model.CategoryId);
                }
                else if (subscription.Status >= (int)SubscribeType.Unsubscribe)
                {
                    pp.SubscriptionReSet(Convert.ToInt32(subscription.Id));
                    msg = "重新訂閱成功！";
                    isShowPponSelect = IsCheckPponSelect(model.Email, model.CategoryId);
                }
                else
                {
                    msg = "此電子郵件已訂閱！";
                }
                result = true;
            }
            else
            {
                result = false;
                msg = "Email格式錯誤";
            }

            return new
            {
                Result = result,
                Message = msg,
                ShowPponSelect = isShowPponSelect
            };
        }
        private bool IsCheckPponSelect(string email, int categoryId)
        {
            Subscription subscription = pp.SubscriptionGet(email, CategoryManager.Default.PponSelect.CategoryId);
            if (subscription.Status < (int)SubscribeType.Unsubscribe)
            {
                return false;
            }
            return (categoryId != CategoryManager.Default.Delivery.CategoryId && categoryId != CategoryManager.Default.PponSelect.CategoryId) ? true : false;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpPost]
        public dynamic GetReceiver([FromBody]dynamic value)
        {
            string userName = HttpContext.Current.User.Identity.Name;
            MemberDelivery md = mp.MemberDeliveryGet(int.Parse((string)value.id));
            Member mem = mp.MemberGet(userName);
            if (mem.IsLoaded == false || md.UserId != mem.UniqueId)
            {
                return null;
            }

            Building bu = lp.BuildingGet(md.BuildingGuid);
            City area = lp.CityGet(bu.CityId);
            City city = lp.CityGet(area.ParentId.Value);
            var address = md.Address == null 
                ? string.Empty
                : md.Address.Replace(city.CityName, string.Empty).Replace(area.CityName, string.Empty);
            return new
            {
                name = md.ContactName,
                mobile = md.Mobile,
                city = city.Id.ToString(),
                town = area.Id.ToString(),
                addr = address
            };
        }

        /// <summary>
        /// 
        /// 使用者是否存在於資料庫，是否為訪客會員
        /// 
        /// IsNew:會員資料不存在
        /// IsGuest:是訪客會員 或 會員未認證
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult GetMemberLevel([FromBody]dynamic value)
        {
            if (value == null)
            {
                return null;
            }
            string userEmail = value.userEmail;
            Member mem = mp.MemberGet(userEmail);
            bool isGuest = false;
            if (mem.IsGuest || MemberFacade.GetMemberActiveStatus(userEmail) == MemberActiveStatus.Inactive)
            {
                isGuest = true;
            }
            return new ApiResult()
            {
                Code = ApiResultCode.Success,
                Data = new
                {
                    IsNew = mem.IsNew,
                    IsGuest = isGuest
                }
            };
        }

        /// <summary>
        /// 
        /// </summary>
        public class ValidateMerchantSessionModel
        {
            /// <summary>
            /// 
            /// </summary>
            public string ValidationUrl { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string DomainName { get; set; }
        }

        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<JsonResult<JObject>> Validate(ValidateMerchantSessionModel model)
        {
            logger.Info("Validate Start.");
            Uri requestUri;
            string validationURL = model.ValidationUrl;
            //logger.Info("[validation model]" + JsonConvert.SerializeObject(model));
            if (!Uri.TryCreate(validationURL, UriKind.Absolute, out requestUri))
            {
                return null;
            }

            try
            {
                // Load the merchant certificate for two-way TLS authentication with the Apple Pay server.
                var certificate = ApplePayUtility.LoadMerchantCertificate(config.ApplePayMerchantCerThumbprint);
                logger.Info("certificate get");
                // Get the merchant identifier from the certificate to send in the validation payload.
                var merchantIdentifier = ApplePayUtility.GetMerchantIdentifier(certificate);
                logger.Info("merchantIdentifier get");
                // Create the JSON payload to POST to the Apple Pay merchant validation URL.
                var payload = new
                {
                    merchantIdentifier = merchantIdentifier, //"merchant.com.17Life.merchantname",
                    domainName = model.DomainName,//Request.GetTypedHeaders().Host.Value,
                    displayName = "17Life"
                };

                JObject merchantSession;

                // Create an HTTP client with the merchant certificate
                // for two-way TLS authentication over HTTPS.
                var handler = new WebRequestHandler();
                handler.ClientCertificates.Add(certificate);

                using (var httpClient = new HttpClient(handler, true))
                {
                    //全域屬性，若是綁定會影響到其他僅支援SSL的服務
                    //System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
                    var jsonData = JsonConvert.SerializeObject(payload);
                    
                    using (var content = new StringContent(jsonData, Encoding.UTF8, "application/json"))
                    {
                        using (var response = await httpClient.PostAsync(requestUri, content))
                        {
                            logger.Info("response get");
                            response.EnsureSuccessStatusCode();
                            //Build merchant session object from web service response
                            var merchantSessionJson = await response.Content.ReadAsStringAsync();
                            //logger.Info("response msg:" + merchantSessionJson);
                            logger.Info("MerchantSession get");
                            merchantSession = JObject.Parse(merchantSessionJson);
                            return Json(merchantSession);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("{0} {1}{2}", "ApplePay validate error.", ex, Environment.NewLine);
                if (ex.InnerException != null)
                {
                    sb.AppendFormat("{0} {1}{2}", "ApplePay validate inner error.", ex.InnerException, Environment.NewLine);
                    if (ex.InnerException.InnerException != null)
                    {
                        sb.AppendFormat("{0} {1}{2}", "ApplePay validate inner's inner error.", 
                            ex.InnerException.InnerException, Environment.NewLine);
                        if (ex.InnerException.InnerException.InnerException != null)
                        {
                            sb.AppendFormat("{0} {1}{2}", "ApplePay validate inner's inner's inner error.",
                                ex.InnerException.InnerException.InnerException, Environment.NewLine);
                        }
                    }
                }
                logger.Warn(sb.ToString());
            }

            return null;
        }

        /// <summary>
        /// try decrypt toen
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult GoPay([FromBody]ApplePayToken token)
        {
            try
            {
                logger.Info("token data:" + token.Data);
                logger.Info("token header ephemeralPublicKey:" + token.Header.EphemeralPublicKey);
                logger.Info("token header publicKeyHash:" + token.Header.PublicKeyHash);
                logger.Info("token header transactionId:" + token.Header.TransactionId);
                logger.Info("token signature:" + token.Signature);
                logger.Info("token version:" + token.Version);
                logger.Info("token bid:" + token.Bid);
                var payData = ApplePayUtility.DecryptToken(token);

                if (payData == null)
                {
                    return new ApiResult
                    {
                        Code = ApiResultCode.Error,
                        Data = string.Empty
                    };
                }

                Guid businessHourId;
                if (payData != null && Guid.TryParse(token.Bid, out businessHourId))
                {
                    #region mobile payment - ApplePay
                    //這邊new的orderGuid 會被後面轉成scash新的order_guid取代                    
                    string transactionId = PaymentFacade.GetTransactionId();
                    int transactionAmount = int.Parse(payData.TransactionAmount.ToString());
                    string userName = User.Identity.Name;
                    var theDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(businessHourId);
                    Member mem = mp.MemberGet(userName);
                    //Guid orderGuid = Helper.GetNewGuid(null);
                    Guid orderGuid = OrderFacade.MakeMainOrder(mem, theDeal).Guid;
                    PaymentTransaction pt = OrderFacade.MakeTransaction(
                        transactionId, PaymentType.Creditcard, transactionAmount / 100, (DepartmentTypes)theDeal.Department, userName, orderGuid);

                    CreditCardAuthResult result = CreditCardUtility.Authenticate(
                                        new ApplePayAuthObject()
                                        {
                                            TransactionId = transactionId,
                                            Amount = transactionAmount,
                                            CardNumber = payData.ApplicationPrimaryAccountNumber,
                                            ExpireDate = payData.ApplicationExpirationDate,
                                            Description = token.Bid,
                                            DeviceManufacturerIdentifier = payData.DeviceManufacturerIdentifier,
                                            PaymentDataType = payData.PaymentDataType,
                                            OnlinePaymentCryptogram = payData.PaymentData.OnlinePaymentCryptogram,
                                            EciIndicator = payData.PaymentData.EciIndicator,
                                        }, orderGuid, userName, OrderClassification.LkSite,
                                         CreditCardOrderSource.Ppon, false);

                    bool isPaySuccess = result.ReturnCode == "00";
                    PayTransPhase status = isPaySuccess ? PayTransPhase.Successful : PayTransPhase.Failed;
                    int type = isPaySuccess ? (int)PayTransResponseType.OK : (int)PayTransResponseType.CreditCardFail;
                    PaymentFacade.UpdateTransaction(transactionId, pt.OrderGuid, PaymentType.Creditcard, pt.Amount,
                                result.AuthenticationCode
                                , PayTransType.Authorization, DateTime.Now
                                , "ApplePay-授權"
                                , Helper.SetPaymentTransactionPhase(pt.Status, status)
                                , type, PaymentAPIProvider.ApplePay);

                    #endregion

                    //如果在這成功代表付款成功 JOB就要去檢查付款成功但訂單沒成功的狀況
                    //如果這裡失敗 就不用繼續下一步 pay status = 52

                    return new ApiResult
                    {
                        Code = ApiResultCode.Success,
                        Data = new {
                            PrebuiltOrderGuid = orderGuid
                        }
                    };
                }
            }
            catch (Exception ex)
            {
                logger.Error("error" + ex.ToString());
                logger.Error("error" + ex.Message);
            }

            return new ApiResult
            {
                Code = ApiResultCode.Error,
                Data = ""
            };
        }

        /// <summary>
        /// 檢查0級會員是否被允許登入
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public ApiResult IsApprovedMember([FromBody]dynamic value)
        {
            if (value == null)
            {
                return null;
            }
            string userEmail = value.userEmail;
            Member mem = mp.MemberGet(userEmail);
            return new ApiResult()
            {
                Code = mem.IsLoaded ? (mem.IsApproved ? ApiResultCode.Success : ApiResultCode.Error) : ApiResultCode.Success,
                Data = ""
            };
        }

        /// <summary>
        /// 依據檔次bid回傳檔次的推薦分享連結，有登入情況下回傳分享送紅利的連結。
        /// 
        /// 
        /// 這隻跟 其它2隻 GetDealPromoUrl 不大一概，其它2隻的參數 userId ，是 API 的 ApiUserID
        /// 而我這要的是可以指定 會員的 UserId
        /// </summary>
        [HttpPost]
        public ApiResult GetDealPromoUrl(DealPromoUrlModel model)
        {
            ApiResult result = new ApiResult() { Code = ApiResultCode.Success };

            Guid bid;
            int userId;

            if (Guid.TryParse(model.bid, out bid) == false || int.TryParse(model.userId, out userId) == false)
            {
                result.Code = ApiResultCode.InputError;
                return result;
            }
            Member mem = MemberFacade.GetMember(userId);
            if (mem.IsLoaded == false)
            {
                result.Code = ApiResultCode.DataNotFound;
                return result;
            }

            string rawUrl;            
            PponDealPromoUrlType urlType = PponDealApiManager.TryGetPponDealPromoUrl(bid, userId, out rawUrl);

            string url = rawUrl;
            if (urlType == PponDealPromoUrlType.BonusFeedback)
            {
                //分享送紅利連結，轉換為短網址
                url = WebUtility.RequestShortUrl(rawUrl);
                PromotionFacade.SetDiscountReferrerUrl(rawUrl, url, userId, mem.UserName, bid, 0);
            }
            if (string.IsNullOrEmpty(url) == false)
            {
                result.Data = url;
            }
            else
            {
                result.Data = rawUrl;
            }
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ApiResult SendAuthMail(AuthMailModel model)
        {
            ApiResult result = new ApiResult() { Code = ApiResultCode.Success };
            try
            {
                Member m = null;
                MemberAuthInfo mai = null;
                if (string.IsNullOrEmpty(model.key) == false)
                {
                    mai = mp.MemberAuthInfoGetByKey(model.key);
                    if (mai.IsLoaded == false)
                    {
                        return new ApiResult
                        {
                            Code = ApiResultCode.Error,
                            Message = "找不到信箱"
                        };
                    }
                    m = mp.MemberGet(mai.UniqueId);
                }
                else
                {
                    m = mp.MemberGet(model.email);
                    mai = mp.MemberAuthInfoGet(m.UniqueId);
                }

                if (m != null && m.IsLoaded)
                {
                    string[] authPair = NewMemberUtility.GenEmailAuthCode();
                    if (string.IsNullOrEmpty(mai.AuthKey))
                    {
                        mai.AuthKey = authPair[0];
                    }
                    mai.AuthCode = authPair[1];
                    if (mai.AuthDate.Value.Date == DateTime.Today)
                    {
                        mai.AuthQueryTime = mai.AuthQueryTime ?? 0;
                        mai.AuthQueryTime++;
                    }
                    else
                    {
                        mai.AuthQueryTime = 1;
                        mai.AuthDate = DateTime.Now;
                    }
                    if (NewMemberUtility.ReAuthIsSendLimitToday(mai))
                    {
                        return new ApiResult
                        {
                            Code = ApiResultCode.ReachingReSendAuthMailLimit,
                            Message = "到達每日重送的上限"
                        };
                    }

                    mp.MemberAuthInfoSet(mai);

                    // sent account confirm mail
                    MemberFacade.SendAccountConfirmMail(m.UserEmail, m.UniqueId.ToString(), mai.AuthKey, mai.AuthCode);
                }
                else
                {
                    result.Code = ApiResultCode.Error;
                    result.Message = "找不到信箱";
                }
            }
            catch (Exception)
            {
                result.Code = ApiResultCode.Error;
                result.Message = "發送錯誤";
            }
            return result;
        }

        /// <summary>
        /// 索取信用卡銀行名稱
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ApiResult GetCreditCardName(BankNameModel model)
        {
            string BankName = string.Empty;
            if (model.bankId!=0)
            {
                string[] CardBin = CreditCardPremiumManager.GetCreditcardBankBinsByBankId(model.bankId).Split(',');

                if (CardBin.Contains(model.cardno.Substring(0, 6)))
                {
                    BankName = MemberFacade.GetDefaultCarditCardName(model.cardno);
                    return new ApiResult()
                    {
                        Code = ApiResultCode.Success,
                        Data = new
                        {
                            BankName = BankName
                        }
                    };
                }
                else
                {
                    BankName = MemberFacade.GetDefaultCarditCardName(model.cardno);
                    return new ApiResult()
                    {
                        Code = ApiResultCode.Error,
                        Data = new
                        {
                            BankName = "此好康限定，台新銀行信用卡付款。"
                        }
                    };
                }
            }
            else
            {
                BankName = MemberFacade.GetDefaultCarditCardName(model.cardno);

                return new ApiResult()
                {
                    Code = ApiResultCode.Success,
                    Data = new
                    {
                        BankName = BankName
                    }
                };
            }
        }
        // <summary>
        // 2019M版首頁頻道banner及分類
        // </summary>
        // <returns></returns>
        [HttpPost]
        public ApiResult GetChannelCategoryBanner(ChannelCategoryBannerModel model)
        {
            ChannelCategoryBanner result = new ChannelCategoryBanner();
            if (model == null)
            {
                return new ApiResult()
                {
                    Code = ApiResultCode.InputError,
                    Data = result
                };
            }

            int currentCityId = MobileManager.GetCurrentCityId();
            if (currentCityId <= 0) //未取到current預設值
            {
                currentCityId = CategoryManager.Default.Taipei.CategoryId;
            }
            PponCity pponCity = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(currentCityId);


            string linkCity = "";
            List<int> citys = new List<int>();
            if (model.channelId != 0)
            {
                #region 一般頻道
                if (model.channelId == (int)ChannelCategory.Food)
                {
                    linkCity = PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId.ToString() + "/";
                    citys.Add(PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId);//199
                    citys.Add(PponCityGroup.DefaultPponCityGroup.Taoyuan.CityId);
                    citys.Add(PponCityGroup.DefaultPponCityGroup.Hsinchu.CityId);
                    citys.Add(PponCityGroup.DefaultPponCityGroup.Taichung.CityId);
                    citys.Add(PponCityGroup.DefaultPponCityGroup.Tainan.CityId);
                    citys.Add(PponCityGroup.DefaultPponCityGroup.Kaohsiung.CityId);
                }
                else if (model.channelId == (int)ChannelCategory.Delivery)
                {
                    linkCity = PponCityGroup.DefaultPponCityGroup.AllCountry.CityId.ToString() + "/";
                    citys.Add(PponCityGroup.DefaultPponCityGroup.AllCountry.CityId);
                }
                else if (model.channelId == (int)ChannelCategory.Travel)
                {
                    linkCity = PponCityGroup.DefaultPponCityGroup.Travel.CityId.ToString() + "/";
                    citys.Add(PponCityGroup.DefaultPponCityGroup.Travel.CityId);
                }
                else if (model.channelId == (int)ChannelCategory.Beauty)
                {
                    linkCity = PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityId.ToString() + "/";
                    citys.Add(PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityId);
                }
                else if (model.channelId == 148)
                {
                    //品生活指定頁面
                    result.link = config.SSLSiteUrl + "/piinlife/default.aspx";

                }
                else if (model.channelId == (int)ChannelCategory.Family)
                {
                    //全家指定頁面
                    result.link = config.SSLSiteUrl + "/m/" + PponCityGroup.DefaultPponCityGroup.Family.CityId.ToString();

                }
                //else if (model.channelId == (int)ChannelCategory.Immediately)
                //{
                    //即買即用,有需要再用
                    //result.link = config.SSLSiteUrl + "/m/FastDeals";

                //}
                else
                {
                    return new ApiResult()
                    {
                        Code = ApiResultCode.DataNotFound,
                        Data = result
                    };

                }

                if (string.IsNullOrEmpty(result.link))
                {
                    //有指定link就別撈了
                    ApiCategoryTypeNode typeNode = PponDealPreviewManager.GetPponChannelCategoryTree();
                    ApiCategoryNode channelCategory = typeNode.CategoryNodes.Where(x => x.CategoryId == model.channelId).First();
                    List<ApiCategoryNode> existDeal = channelCategory.GetSubCategoryTypeNode(CategoryType.DealCategory).Where(x => x.ExistsDeal && x.CategoryId != model.channelId).ToList();
                    List<ViewCategoryDependency> vcdc = sp.ViewCategoryDependencyGetByCategoryIdList(existDeal.Select(x => x.CategoryId).ToList()).Where(x => x.ParentId == (model.channelId == 3000 ? 88 : model.channelId)).ToList();


                    List<ChannelCategorys> category = vcdc.Select(x => new ChannelCategorys
                    {
                        id = x.CategoryId,
                        name = x.CategoryName,
                        imagePath = x.Image,
                        link = config.SSLSiteUrl + "/m/" + linkCity + x.CategoryId //美食city抓cookie,無實質作用

                    }).ToList();


                    ViewCmsRandomCollection classifyBanner1 = CmsRandomFacade.GetOrAddData("/ppon/default.classify_banner1", RandomCmsType.PponRandomCms);
                    ViewCmsRandomCollection classifyBanner2 = CmsRandomFacade.GetOrAddData("/ppon/default.classify_banner2", RandomCmsType.PponRandomCms);


                    DateTime nowDateTime = DateTime.Now;
                    List<ViewCmsRandom> cms1 = classifyBanner1.Where(x => citys.Contains(x.CityId)).ToList();
                    List<ViewCmsRandom> cms2 = classifyBanner2.Where(x => citys.Contains(x.CityId)).ToList();

                    ClassfyBanner banner1 = new ClassfyBanner();
                    ClassfyBanner banner2 = new ClassfyBanner();
                    Random r = new Random();

                    int total = cms1.Sum(x => x.Ratio);
                    int randomNumber = r.Next(1, total + 1);
                    foreach (var item in cms1)
                    {
                        randomNumber -= item.Ratio;
                        if (randomNumber <= 0)
                        {
                            banner1.id = item.Pid;
                            banner1.imageHtml = item.Body;
                            break;
                        }
                    }

                    total = cms2.Sum(x => x.Ratio);
                    randomNumber = r.Next(1, total + 1);
                    foreach (var item in cms2)
                    {
                        randomNumber -= item.Ratio;
                        if (randomNumber <= 0)
                        {
                            banner2.id = item.Pid;
                            banner2.imageHtml = item.Body;
                            break;
                        }
                    }


                    result.category = category;
                    result.classfyBanner1 = banner1;
                    result.classfyBanner2 = banner2;
                }
                
                #endregion
            }
            //else if (model.channelId != 0 && model.category != 0)
            //{
                //指定分類,有需要再用
                //if (model.channelId == (int)ChannelCategory.Food)
                //{
                //    result.link = config.SSLSiteUrl + "/m/" + PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId.ToString() + "/" + model.category;
                //}
                //else if (model.channelId == (int)ChannelCategory.Delivery)
                //{
                //    result.link = config.SSLSiteUrl + "/m/" + PponCityGroup.DefaultPponCityGroup.AllCountry.CityId.ToString() + "/" + model.category;
                //}
                //else if (model.channelId == (int)ChannelCategory.Travel)
                //{
                //    result.link = config.SSLSiteUrl + "/m/" + PponCityGroup.DefaultPponCityGroup.Travel.CityId.ToString() + "/" + model.category;
                //}
                //else if (model.channelId == (int)ChannelCategory.Beauty)
                //{
                //    result.link = config.SSLSiteUrl + "/m/" + PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityId.ToString() + "/" + model.category;
                //}
            //}
            //else if (model.Theme)
            //{
                //主題活動,有需要再用
                //result.link = config.SSLSiteUrl + "/Event/ThemeCurationChannelMobile";
            //}

            return new ApiResult()
            {
                Code = ApiResultCode.Success,
                Data = result
            };
        }

        // <summary>
        // 2019M版首頁頻道
        // </summary>
        // <returns></returns>
        [HttpGet]
        public ApiResult GetChannel()
        {
            PponChannelList result = new PponChannelList();
            result.categoryItem = new List<PponChannel>();
            result.linkItem = new List<PponChannel>();

            result.categoryItem.Add(new PponChannel
            {
                channelId = (int)ChannelCategory.Food,
                channelName = "美食"
              
            });

            result.categoryItem.Add(new PponChannel
            {
                channelId = (int)ChannelCategory.Delivery,
                channelName = "宅配"

            });

            result.categoryItem.Add(new PponChannel
            {
                channelId = (int)ChannelCategory.Travel,
                channelName = "旅遊"

            });

            result.categoryItem.Add(new PponChannel
            {
                channelId = (int)ChannelCategory.Beauty,
                channelName = "玩美"

            });

            result.linkItem.Add(new PponChannel
            {
                channelId = (int)ChannelCategory.Family,
                channelName = "全家"

            });

            
            result.linkItem.Add(new PponChannel
            {
                channelId = 148,
                channelName = "品生活"

            });

            

            return new ApiResult()
            {
                Code = ApiResultCode.Success,
                Data = result
            };
        }
        /// <summary>
        /// M版首頁: 取得首頁各區塊的資料
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ApiResult GetHomepage(GetHomepageInputModel model)
        {
            model = model ?? new GetHomepageInputModel();

            List<IHomepagePageBlockContent> contents = new List<IHomepagePageBlockContent>();
            string cacheKey = string.Format("api://GetHomepage/{0}", DateTime.Now.ToString("yyyyMMddHH"));
            List<HomepageBlock> blockRaws = MemoryCache.Default.Get(cacheKey) as List<HomepageBlock>;
            if (model.preview || blockRaws == null)
            {
                blockRaws = pep.GetLatestHomepageBlocks().Where(t => t.Enabled).ToList();
                MemoryCache.Default.Set(cacheKey, blockRaws, null);
            }

            foreach(var raw in blockRaws)
            {
                var content = HomepagePageBlockConverterFactory.Convert(raw.TypeId, raw.Title, raw.Content, 
                    this.User.Identity.Id, model.cityId);
                if (content != null)
                {
                    contents.Add(content);
                }
            }

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = contents
            };
        }

        /// <summary>
        /// 取得新M版蓋版廣告
        /// </summary>
        /// <returns></returns>
        public ApiResult GetMobileEventAd()
        {
            try
            {
                var eventAd = EventFacade.GetCurrentMobileEventAd();
                if (eventAd == null)
                {
                    return new ApiResult
                    {
                        Code = ApiResultCode.DataNotFound
                    };
                }
                return new ApiResult
                {
                    Code = ApiResultCode.Success,
                    Data = eventAd
                };
            }
            catch
            {
                return new ApiResult
                {
                    Code = ApiResultCode.Error
                };
            }
        }


        /// <summary>
        /// M版首頁: 猜你喜歡的分頁檔次列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult GetRecommendedDeals(GetRecommendedDealsInputModel model)
        {
            if (model == null)
            {
                model = new GetRecommendedDealsInputModel
                {
                    pageSize = 10,
                    startIndex = 0,
                };
            }
            if (model.pageSize == 0)
            {
                model.pageSize = 10;
            }
            
            int totalCount;
            var deals = new HomepagePageBlockYouLikeConverter(this.User.Identity.Id, model.cityId)
                .Convert(string.Empty, string.Empty, model.startIndex, model.pageSize, out totalCount);

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = new
                {
                    deals,
                    totalCount
                }
            };
        }
        /// <summary>
        /// 取得會員資料
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ApiResult GetMemberInfo()
        {
            PponIdentity pponUser = User.Identity as PponIdentity;
            if (pponUser == null)
            {
                pponUser = PponIdentity.Get(User.Identity.Name);
            }
            Member mem = MemberFacade.GetMember(pponUser.Id);
            string memberPic = MemberFacade.GetMemberPicUrl(mem);
            if (string.IsNullOrEmpty(memberPic))
            {
                memberPic = Helper.CombineUrl(config.SiteUrl, "Themes/mobile/images/login_17life.jpg");
            }
            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = new
                {
                    user = new
                    {
                        isAuthenticated = User.Identity.IsAuthenticated,
                        name = User.Identity.Name,
                        userId = mem.UniqueId,
                        displayName = pponUser.DisplayName,
                        pic = memberPic
                    }
                }
            };
        }
        /// <summary>
        /// 會員收藏檔次
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult SetMemberCollectDeal(SetMemberCollectDealInputModel model)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated == false)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.UserNoSignIn,
                    Message = Helper.GetEnumDescription(ApiResultCode.UserNoSignIn)
                };
            }
            if (model == null || model.IsValid == false)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }

            int userId = MemberFacade.GetUniqueId(HttpContext.Current.User.Identity.Name);
            bool result;
            if (model.collect)
            {
                MemberCollectDeal mcd = new MemberCollectDeal();
                mcd.MemberUniqueId = userId;
                mcd.BusinessHourGuid = model.bid;
                mcd.CollectStatus = (byte)MemberCollectDealStatus.Collected;
                mcd.CollectTime = DateTime.Now;
                mcd.LastAccessTime = DateTime.Now;
                mcd.CityId = 0;
                result = MemberFacade.MemberCollectDealSave(mcd);
            }
            else
            {
                result = MemberFacade.MemberCollectDealRemove(userId, model.bid);
            }
            if (result)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.Success
                };
            }
            else
            {
                return new ApiResult
                {
                    Code = ApiResultCode.Error
                };
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public class UserIdInputModel
        {
            /// <summary>
            /// 
            /// </summary>
            public int UserId { get; set; }
        }

        /// <summary>
        /// 
        /// </summary>
        public class AccountInputModel
        {
            /// <summary>
            ///
            /// </summary>
            public string Account { get; set; }
        }

        public class GetHomepageInputModel
        {
            public int? cityId { get; set; }
            public bool preview { get; set; }
        }

        public class GetRecommendedDealsInputModel
        {
            /// <summary>
            /// 199 台北, 356 桃園....
            /// </summary>
            public int? cityId { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public int startIndex { get; set; }
            public int pageSize { get; set; }
        }

        public class SetMemberCollectDealInputModel
        {
            public Guid bid { get; set; }
            public bool collect { get; set; }

            public bool IsValid
            {
                get
                {
                    if (bid == Guid.Empty)
                    {
                        return false;
                    }
                    return true;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public class AuthMailModel
        {
            /// <summary>
            /// 
            /// </summary>
            public string email { get; set; }

            public string key { get; set; }
        }
        /// <summary>
        /// 
        /// </summary>
        public class DealPromoUrlModel
        {
            /// <summary>
            /// 
            /// </summary>
            public string bid { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string userId { get; set; }
        }
        public class CityIdModel
        {
            /// <summary>
            /// 
            /// </summary>
            public int cid { get; set; }
            
        }

        /// <summary>
        /// 
        /// </summary>
        public class BankNameModel
        {
            /// <summary>
            /// 
            /// </summary>
            public string cardno { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public int bankId { get; set; }
        }

        /// <summary>
        /// 推薦檔次API取檔
        /// </summary>
        [HttpPost]
        public ApiResult GetRelatedDeals(RelatedDealModel model)
        {
            ApiResult result = new ApiResult() { Code = ApiResultCode.Success };

            try
            {
                model.workCityId = model.workCityId == PponCityGroup.DefaultPponCityGroup.NewTaipeiCity.CityId
                                               ? PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId
                                               : model.workCityId;
                Guid _mainbid = ViewPponDealManager.DefaultManager.MainBusinessHourGuidGetByBid(model.bid);
                var dealList = PponDealApiManager.GetPponDealRecommendDeal(_mainbid, 6);


                result.Data = new LunchKingSite.Core.JsonSerializer().Serialize(PponFacade.GenBaseRelateDealsJson(dealList));
                //result.Data = new LunchKingSite.Core.JsonSerializer().Serialize(
                // PponFacade.GetRelatedDealsParseJson(model.bid, model.categoryId, model.workCityId, model.travelCategoryId, model.femaleCategoryId, model.filterCategoryIdList));
            }
            catch (Exception ex)
            {
                WebUtility.LogExceptionAnyway(ex);
                result.Code = ApiResultCode.Error;
                result.Data = new LunchKingSite.Core.JsonSerializer().Serialize(string.Empty);
            }

            return result;
        }

        /// <summary>
        /// 取得地區
        /// </summary>
        [HttpPost]
        public ApiResult GetAreaByCity(CityIdModel model)
        {
            ApiResult result = new ApiResult() { Code = ApiResultCode.Success };
            var tmpData = lp.CityGetList(model.cid);
            Dictionary<int, string> cityList = new Dictionary<int, string>();
            foreach (var d in tmpData)
            {
                cityList.Add(d.Id, d.CityName);
            }
            result.Data = JsonConvert.SerializeObject(cityList); ;
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        public class RelatedDealModel
        {
            public Guid bid { get; set; }
            public int categoryId { get; set; }
            public int workCityId { get; set; }
            public int travelCategoryId { get; set; }
            public int femaleCategoryId { get; set; }
            public List<int> filterCategoryIdList { get; set; }
        }

        public class ChannelCategoryBannerModel
        {
            public int channelId { get; set; }
        }

    }
}

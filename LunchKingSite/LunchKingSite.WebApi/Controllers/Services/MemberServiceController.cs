﻿using System;
using System.Web;
using System.Web.Http;
using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Component.MemberActions;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebApi.Core;
using LunchKingSite.WebApi.Core.Attributes;
using LunchKingSite.WebApi.Services;
using LunchKingSite.WebLib.Component;

namespace LunchKingSite.WebApi.Controllers.Services
{
    /// <summary>
    /// 
    /// </summary>
    [RequireHttps(false)]
    [MvcStyleBinding]//不要再使用MvcStyleBinding
    public class MemberServiceController : BaseController
    {
        private static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private static ILog logger = LogManager.GetLogger(typeof(MemberServiceController));

        /// <summary>
        /// 取得會員收藏的檔次資料
        /// </summary>
        /// <param name="userId"></param>
        [HttpPost]
        [Authorize]
        [MemberValidate(true, "userId")]
        public ApiResult GetMemberCollectDealList(string userId)
        {
            const string methodName = "GetMemberCollectDealList";
            ApiResult result = new ApiResult();

            //取得會員名稱
            string userName = HttpContext.Current.User.Identity.Name;

            int memberUniqueId = MemberFacade.GetUniqueId(userName);

            try
            {
                result.Code = ApiResultCode.Success;
                result.Data = ApiMemberManager.GetApiMemberCollectDealList(memberUniqueId);
            }
            catch
            {
                result.Code = ApiResultCode.Error;
                result.Message = I18N.Phrase.ApiResultCodeError;
            }
            SetApiLog(GetMethodName(methodName), userId, new { userId }, result, Helper.GetClientIP());
            return result;
        }

        /// <summary>
        /// 是否已認證
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [MemberValidate(true, "userId")]
        public ApiResult CheckUserIsUnbind(string userId, string userName)
        {
            ApiResult apiResult = new ApiResult();

            if (MemberFacade.GetMobileMember(userName).IsLoaded)
            {
                apiResult.Code = ApiResultCode.Success;
            }
            else
            {
                apiResult.Code = ApiResultCode.Error;
            }
            return apiResult;
        }

        /// <summary>
        /// 驗證手機號碼是否已被綁定
        /// </summary>
        /// <param name="userId">API的使用者編號</param>
        /// <param name="mobile">接收簡訊的行動電話號碼</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [MemberValidate(true, "userId")]
        public ApiResult CheckMobileIsUnbind(string userId, string mobile)
        {
            ApiResult apiResult = new ApiResult();

            if (RegExRules.CheckMobile(mobile) == false)
            {
                apiResult.Code = ApiResultCode.Error;
                apiResult.Message = "行動電話格式錯誤";
            }
            else if (MemberFacade.GetMobileMember(mobile).IsLoaded)
            {
                apiResult.Code = ApiResultCode.MobileAuthUsedByOtherMember;
                apiResult.Message = "這個手機號碼認證過了";
            }
            else
            {
                apiResult.Code = ApiResultCode.Success;
                apiResult.Message = "這個手機號碼未被綁定使用";
            }
            return apiResult;
        }



        /// <summary>
        /// 發送認證簡訊
        /// </summary>
        /// <param name="id">Unique Id</param>
        /// <param name="userId">API的使用者編號</param>
        /// <param name="mobile">接收簡訊的行動電話號碼</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [MemberValidate(true, "userId")]
        public ApiResult SendMobileAuthCode(int id, string userId, string mobile)
        {
            if (id != 0)
            {
                Member mem = mp.MemberGetbyUniqueId(id);
                if (mem.IsLoaded && mem.UserName.Equals(this.User.Identity.Name, StringComparison.OrdinalIgnoreCase))
                {
                    return ApiMemberService.SendMobileAuthCode(mem.UniqueId, mobile, false);
                }
            }
            return new ApiResult
            {
                Code = ApiResultCode.Error,
                Message = "使用者不存在"
            };
        }
        /// <summary>
        /// 發送認證簡訊
        /// </summary>
        /// <param name="userId">API的使用者編號</param>
        /// <param name="mobile">接收簡訊的行動電話號碼</param>
        /// <returns></returns>
        [HttpPost]
        [ApiUserValidator]
        public ApiResult SendMobileRegisterAuthCode(string userId, string mobile)
        {
            if (string.IsNullOrEmpty(mobile) == false)
            {
                MobileMember mm = MemberFacade.GetMobileMember(mobile);
                if (mm.IsLoaded)
                {
                    return ApiMemberService.SendMobileAuthCode(mm.UserId, mobile, false);
                }
            }
            return new ApiResult
            {
                Code = ApiResultCode.Error,
                Message = "使用者不存在"
            };
        }

        /// <summary>
        /// 忘記密碼，發送認證code
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="mobile"></param>
        /// <returns></returns>
        [HttpPost]
        [ApiUserValidator]
        public ApiResult SendMobileForgotPasswordAuthCode(string userId, string mobile)
        {
            MobileMember mm = MemberFacade.GetMobileMember(mobile);
            if (mm.IsLoaded)
            {
                return ApiMemberService.SendMobileAuthCode(mm.UserId, mm.MobileNumber, true);
            }
            return new ApiResult
            {
                Code = ApiResultCode.Error,
                Message = "使用者不存在"
            };
        }

        /// <summary>
        /// 檢查行動認證碼，如果成功即建立MobileMember
        /// </summary>
        /// <param name="id">Unique Id</param>
        /// <param name="userId">API的使用者編號</param>
        /// <param name="mobile">接收簡訊的行動電話號碼</param>
        /// <param name="code">驗證碼</param>
        /// <returns></returns>
        [HttpPost]
        [ApiUserValidator]
        public ApiResult ValidateMobileCode(int id, string userId, string mobile, string code)
        {
            ApiResult result = new ApiResult();
            Member mem = null;
            if (id > 0)
            {
                mem = MemberFacade.GetMember(id);
                if (mem.IsLoaded && mem.UserName.Equals(this.User.Identity.Name, StringComparison.OrdinalIgnoreCase))
                {
                    bool needSetPassword;
                    string resetPasswordKey;
                    result = ApiMemberService.ValidateMobileCode(mem, mobile, code, false,
                        out needSetPassword, out resetPasswordKey);
                    result.Data = new { needSetPassword, resetPasswordKey };
                    return result;
                }
            }

            return new ApiResult
            {
                Code = ApiResultCode.Error,
                Message = "使用者不存在"
            };
        }

        /// <summary>
        /// 檢查行動認證碼，如果成功即建立MobileMember
        /// </summary>
        /// <param name="userId">API的使用者編號</param>
        /// <param name="mobile">接收簡訊的行動電話號碼</param>
        /// <param name="code">驗證碼</param>
        /// <returns></returns>
        [HttpPost]
        [ApiUserValidator]
        public ApiResult ValidateMobileRegisterCode(string userId, string mobile, string code)
        {
            if (string.IsNullOrEmpty(mobile) == false)
            {
                MobileMember mm = MemberFacade.GetMobileMember(mobile);
                if (mm.IsLoaded)
                {
                    Member mem = MemberFacade.GetMember(mm.UserId);
                    if (mem.IsLoaded)
                    {
                        bool needSetPassword;
                        string resetPasswordKey;
                        ApiResult result = ApiMemberService.ValidateMobileCode(mem, mobile, code, false,
                            out needSetPassword, out resetPasswordKey);
                        result.Data = new { needSetPassword, resetPasswordKey };
                        return result;
                    }
                }
            }
            return new ApiResult
            {
                Code = ApiResultCode.Error,
                Message = "使用者不存在"
            };

        }

        /// <summary>
        /// 檢查行動認證碼，如果成功即建立MobileMember
        /// </summary>
        /// <param name="userId">API的使用者編號</param>
        /// <param name="mobile">接收簡訊的行動電話號碼</param>
        /// <param name="code">驗證碼</param>
        /// <returns></returns>
        [HttpPost]
        [ApiUserValidator]
        public ApiResult ValidateMobileForgotPasswordCode(string userId, string mobile, string code)
        {
            ApiResult apiResult = new ApiResult();

            Member mem = MemberFacade.GetMember(mobile);

            if (mem.IsLoaded)
            {
                bool needSetPassword;
                string resetPasswordKey;
                apiResult = ApiMemberService.ValidateMobileCode(mem, mobile, code, true,
                    out needSetPassword, out resetPasswordKey);
                apiResult.Data = new { needSetPassword, resetPasswordKey };
            }
            else
            {
                apiResult.Code = ApiResultCode.Error;
                apiResult.Message = "使用者不存在";
            }
            return apiResult;
        }

        /// <summary>
        /// 行動會員設定密碼
        /// </summary>
        /// <param name="id">Unique Id</param>
        /// <param name="userId"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [MemberValidate(true, "userId")]
        public ApiResult SetMobileMemberPassword(int id, string userId, string password)
        {
            ApiResult apiResult = new ApiResult();

            if (id != 0)
            {
                Member mem = mp.MemberGetbyUniqueId(id);
                if (mem.IsLoaded)
                {
                    if (mem.UserName.Equals(this.User.Identity.Name, StringComparison.OrdinalIgnoreCase))
                    {
                        apiResult = ApiMemberService.SetMobileMemberPassword(mem.UniqueId, password);
                    }
                    else
                    {
                        apiResult.Code = ApiResultCode.Error;
                        apiResult.Message = "使用者不存在";
                    }
                }
                else
                {
                    apiResult.Code = ApiResultCode.Error;
                    apiResult.Message = "使用者不存在";
                }
            }
            else
            {
                apiResult.Code = ApiResultCode.Error;
                apiResult.Message = "使用者不存在";
            }
            return apiResult;
        }
        /// <summary>
        /// [未開放的祕技]重設手機驗證簡訊次數---
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="mobile"></param>
        /// <returns></returns>
        [HttpPost]
        [MemberValidate(true, "userId")]
        [TestServerOnly]
        public ApiResult ResetMobileQueryTimes(string userId, string mobile)
        {
            var mais = mp.MobileAuthInfoGetList(mobile);
            foreach (var mai in mais)
            {
                mai.QueryTimes = 0;
                MemberFacade.SetMobileAuthInfo(mai);
            }
            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Message = "成功"
            };
        }

        /// <summary>
        /// [未開放的祕技]解除手機綁定
        /// </summary>
        /// <param name="id">UserId</param>
        /// <param name="userId">AppId</param>
        /// <param name="mobile"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [MemberValidate(true, "userId")]
        [TestServerOnly]
        public ApiResult UnbindMobile(int id, string userId, string mobile)
        {
            //應該要chekc user id 吧?
            if (id == 0)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = "錯誤"
                };
            }
            Member mem = mp.MemberGetbyUniqueId(id);
            if (mem.IsLoaded == false || mem.UniqueId != id)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = "錯誤"
                };
            }
            if (MemberFacade.UnbindMobile(mobile))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.Success,
                    Message = "成功"
                };
            }
            return new ApiResult
            {
                Code = ApiResultCode.Error,
                Message = "解除綁定失敗"
            };
        }


        /// <summary>
        /// [未開放的祕技]解除手機綁定
        /// </summary>
        /// <param name="userId">AppId</param>
        /// <param name="mobile"></param>
        /// <returns></returns>
        [HttpPost]
        [TestServerOnly]
        public ApiResult DeleteMobile(string userId, string mobile)
        {
            Member mem = null;
            if (RegExRules.CheckMobile(mobile))
            {
                MobileMember mm = MemberFacade.GetMobileMember(mobile);
                if (mm.IsLoaded)
                {
                    mem = MemberFacade.GetMember(mm.UserId);
                }
            }
            else
            {
                mem = MemberFacade.GetMember(mobile);
            }

            if (mem == null || mem.IsLoaded == false)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = "no mobile"
                };
            }

            try
            {
                string deletedUserName = MemberUtility.DeleteMember(mem.UserName, User.Identity.Name, "delede by DeleteMobile");
                return new ApiResult
                {
                    Code = ApiResultCode.Success,
                    Data = new { DeletedUserName = deletedUserName },
                    Message = "刪除手機會員"
                };
            }
            catch (Exception ex)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = ex.Message
                };
            }
        }
        /// <summary>
        /// 註冊手機會員
        /// </summary>
        /// <param name="model"></param>
        [HttpPost]
        [RequireHttps]
        [ApiUserValidator]
        public ApiResult RegisterMobileMember(RegisterMobileMemberInputModel model)
        {
            ApiResult apiResult = new ApiResult();
            RegisterContactMemberReplyData replyData = NewMemberUtility.RegisterMobileMember(model.Mobile, model.UserEmail);
            if (replyData.Reply == MemberRegisterReplyType.RegisterSuccess)
            {
                apiResult.Code = ApiResultCode.Success;
            }
            else
            {
                apiResult.Code = ApiResultCode.Error;
            }
            apiResult.Data = replyData;
            logger.InfoFormat("RegisterNewMember: {0}, {1}, {2}", model.Mobile, Helper.GetOrderFromType(), replyData.Reply);
            return apiResult;
        }

        /// <summary>
        /// 從code取得key, 用key設定密碼
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="mobile"></param>
        /// <param name="password"></param>
        /// <param name="resetPasswordKey"></param>
        /// <returns></returns>
        [HttpPost]
        [RequireHttps]
        [ApiUserValidator]
        public ApiResult SetMobileMemberPasswordByKey(string userId, string mobile, string password, string resetPasswordKey)
        {
            MobileMember mm = MemberFacade.GetMobileMember(mobile);
            if (mm.IsLoaded == false)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.MobileMemberNotFound,
                    Message = "沒有建立手機會員"
                };
            }
            if (string.IsNullOrEmpty(mm.ResetPasswordKey) || mm.ResetPasswordKey != resetPasswordKey)
            {
                //預防暴力攻擊的可能
                if (mm.ResetPasswordKey.Length > 0)
                {
                    MemberFacade.SetMobileMemberResetPasswordKeyEmpty(mm);
                }
                return new ApiResult
                {
                    Code = ApiResultCode.MobileResetPasswordKeyError,
                    Message = "重設密碼的金鑰錯誤"
                };
            }

            ApiResult apiResult = ApiMemberService.SetMobileMemberPassword(mm.UserId, password);
            return apiResult;
        }
        /// <summary>
        /// 上傳會員圖片
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ApiResult UpdateMemberPic()
        {
            string userId = HttpContext.Current.Request.Form["userId"];
            ApiUser apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser != null)
            {
                Helper.SetContextItem(LkSiteContextItem._DEVICE_TYPE, apiUser.UserDeviceType);
            }

            Member mem = MemberFacade.GetMember(User.Identity.Name);
            if (mem.IsLoaded == false)
            {
                return new ApiResult { Code = ApiResultCode.Error, Message = "未登入" };
            }

            var httpRequest = HttpContext.Current.Request;
            if (httpRequest.Files.Count > 0)
            {
                var postedFile = httpRequest.Files[0];
                if (postedFile.ContentLength == 0)
                {
                    return new ApiResult { Code = ApiResultCode.Error, Message = "找不到要上傳的檔案" };
                }
                string memberPicUrl;
                var updateStatus = MemberFacade.UpdateMemberPic(mem.UniqueId, new AspNetHttpPostedFileAdapter(postedFile), out memberPicUrl);
                if (updateStatus == UpdateMemberPicStatus.OK)
                {
                    return new ApiResult { Code = ApiResultCode.Success, Data = new { MemberPic = memberPicUrl } };
                }
                return new ApiResult { Code = ApiResultCode.Error, Message = Helper.GetEnumDescription(updateStatus) };
            }

            return new ApiResult { Code = ApiResultCode.Error, Message = Helper.GetEnumDescription(UpdateMemberPicStatus.FileNotFound) };
        }

        #region input models

        /// <summary>
        /// 
        /// </summary>
        public class UploadMemberPicModel : IApiUserInputModel
        {
            /// <summary>
            /// App Id
            /// </summary>
            public string UserId { get; set; }
        }

        /// <summary>
        /// 
        /// </summary>
        public class RegisterMobileMemberInputModel : IApiUserInputModel
        {
            /// <summary>
            /// App Id
            /// </summary>
            public string UserId { get; set; }

            /// <summary>
            /// 
            /// </summary>
            public string Mobile { get; set; }

            /// <summary>
            ///  通訊Email
            /// </summary>
            public string UserEmail { get; set; }
        }
        #endregion
    }
}

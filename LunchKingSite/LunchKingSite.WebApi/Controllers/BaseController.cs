﻿using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Xml.Serialization;
using LunchKingSite.BizLogic.Model.OAuth;
using LunchKingSite.WebLib.Component;
using ActionFilterAttribute = System.Web.Http.Filters.ActionFilterAttribute;
using System;
using System.Web.Http.Controllers;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.WebApi.Core;

namespace LunchKingSite.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class BaseController : ApiController
    {
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        /// <summary>
        /// AppId 跟 ClientId 要再確認是否重複了
        /// </summary>
        public string AppId {
            get
            {
                if (HttpContext.Current.Items["AppId"] != null)
                {
                    return HttpContext.Current.Items["AppId"] as string;
                }
                return string.Empty;
            }
        }
        /// <summary>
        /// AppId 跟 ClientId 要再確認是否重複了
        /// </summary>
        public string ClientId
        {
            get
            {
                if (HttpContext.Current.Items["ClientId"] != null)
                {
                    return HttpContext.Current.Items["ClientId"] as string;
                }
                return string.Empty;
            }
        }        
        /// <summary>
        /// 回傳呼叫的裝置類型
        /// </summary>
        public ApiUserDeviceType DeviceType
        {
            get
            {
                if (string.IsNullOrEmpty(this.AppId) == false && this.AppId.Equals(AndroidAppId, StringComparison.OrdinalIgnoreCase))
                {
                    return ApiUserDeviceType.Android;
                }
                if (string.IsNullOrEmpty(this.AppId) == false && this.AppId.Equals(IOSAppId, StringComparison.OrdinalIgnoreCase))
                {
                    return ApiUserDeviceType.IOS;
                }
                return ApiUserDeviceType.WebService;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public new PponPrincipal User
        {
            get
            {
                PponPrincipal user = base.User as PponPrincipal;
                if (user == null)
                {
                    PponIdentity pponUser = PponIdentity.Get(base.User.Identity.Name);
                    user = new PponPrincipal(pponUser);
                }
                return user;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public class PcpPermissionAttribute : ActionFilterAttribute
        {
            private string _role;
            /// <summary>
            /// 
            /// </summary>
            public string Role { get { return this._role; } }

            private string _function;
            /// <summary>
            /// 
            /// </summary>
            public string Function { get { return this._function; } }

            private string _action;
            /// <summary>
            /// 
            /// </summary>
            public string Action { get { return this._action; } }
            /// <summary>
            /// 
            /// </summary>
            /// <param name="role"></param>
            /// <param name="action"></param>
            /// <param name="function"></param>
            public PcpPermissionAttribute(string role, string action = "", string function = "")
            {
                this._role = role;
                this._function = function;
                this._action = action;
            }
            /// <summary>
            /// 
            /// </summary>
            /// <param name="actionContext"></param>
            public override void OnActionExecuting(System.Web.Http.Controllers.HttpActionContext actionContext)
            {
                ApiResultCode resultCode = new ApiResultCode();

                if (HttpContext.Current.User.IsInRole(this.Role))
                {
                    resultCode = ApiResultCode.Success;
                }
                else
                {
                    resultCode = ApiResultCode.OAuthTokerNoAuth;
                }

                if (resultCode != ApiResultCode.Success)
                {
                    throw new HttpErrorResponseException(((int)resultCode).ToString(CultureInfo.InvariantCulture), Helper.GetLocalizedEnum(resultCode));
                }
                base.OnActionExecuting(actionContext);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public class OAuthAppClientAuthAttribute : ActionFilterAttribute
        {
            /// <summary>
            /// 
            /// </summary>
            /// <param name="actionContext"></param>
            public override void OnActionExecuting(System.Web.Http.Controllers.HttpActionContext actionContext)
            {
                //HttpRequestMessage request = actionContext.Request;

                string clientId = string.Empty;
                string clientSecret = string.Empty;
                if (HttpContext.Current.Items["ClientId"] != null && HttpContext.Current.Items["ClientSecret"] != null)
                {
                    clientId = HttpContext.Current.Items["ClientId"].ToString();
                    clientSecret = HttpContext.Current.Items["ClientSecret"].ToString();
                }
                else
                {
                    var response = actionContext.Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError
                    {
                        Message = ErrorResponse.GetDescription(ErrorResponse.InvalidClientId, "")
                    });
                    actionContext.Response = response;
                    return;                    
                }

                OAuthClientModel client = OAuthFacade.GetClient(clientId);
                if (client == null)
                {

                    var response = actionContext.Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError
                    {
                        Message = ErrorResponse.GetDescription(ErrorResponse.InvalidClientId, clientId)
                    });
                    actionContext.Response = response;
                    return;
                }

                //ValidateEnabled
                if (client.Enabled == false)
                {
                    var response = actionContext.Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError
                    {
                        Message = ErrorResponse.GetDescription(ErrorResponse.InactiveClient, clientId)
                    });
                    actionContext.Response = response;
                    return;
                }

                //驗證client_secret
                if ( client.AppSecret != clientSecret)
                {
                    var response = actionContext.Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError
                    {
                        Message = ErrorResponse.GetDescription(ErrorResponse.InvalidClientCredentials, clientId)
                    });
                    actionContext.Response = response;
                    return;
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="controllerContext"></param>
        protected override void Initialize(HttpControllerContext controllerContext)
        {
            base.Initialize(controllerContext);
        }

        /// <summary>
        /// 儲存LOG紀錄
        /// </summary>
        /// <param name="methodName">函式名稱</param>
        /// <param name="apiUserId">呼叫函式的apiUserId</param>
        /// <param name="parameter">物件，紀錄傳入參數</param>
        /// <param name="returnValue">物件，紀錄回傳參數</param>
        /// <param name="ip"></param>
        /// <returns></returns>
        protected bool SetApiLog(string methodName, string apiUserId, object parameter, object returnValue, string ip)
        {
            if (config.SetApiLog)
            {
                return SystemFacade.SetApiLog(GetMethodName(methodName), apiUserId, parameter, returnValue, ip);
            }
            return true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="methodName"></param>
        /// <returns></returns>
        protected string GetMethodName(string methodName)
        {
            Type type = this.GetType();
            return string.Format("{0}.{1}", type.Name, methodName);
        }
        private static string _androidAppId;
        public static string AndroidAppId
        {
            get
            {

                if (_androidAppId == null)
                {
                    var client = OAuthFacade.GetClientByName("android17LifeApp");
                    _androidAppId = client == null ? string.Empty : client.AppId;
                }
                return _androidAppId;
            }
        }
        private static string _iOSAppId;
        public static string IOSAppId
        {
            get
            {
                if (_iOSAppId == null)
                {
                    var client = OAuthFacade.GetClientByName("ios17LifeApp");
                    _iOSAppId = client == null ? string.Empty : client.AppId;
                }
                return _iOSAppId;
            }
        }
    }
}

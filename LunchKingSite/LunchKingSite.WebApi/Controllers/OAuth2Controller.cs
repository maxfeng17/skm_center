﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.OAuth;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebApi.Core;
using LunchKingSite.WebApi.Core.OAuth;
using System.Web.Security;
using LunchKingSite.WebLib.Component;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.API;
using log4net;
using LunchKingSite.WebApi.Core.Attributes;
using LunchKingSite.WebLib.Component.MemberActions;
using Newtonsoft.Json.Linq;
using Helper = LunchKingSite.Core.Helper;
using Newtonsoft.Json.Linq;

namespace LunchKingSite.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [RequireHttps(true)]
    public class OAuth2Controller : BaseController
    {
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private static INotificationProvider np = ProviderFactory.Instance().GetProvider<INotificationProvider>();
        private static ILog logger = LogManager.GetLogger("oauth");
        private static string _LOGIN_PAGE_URL
        {
            get
            {
                return config.OAuthLoginUrl;
            }
        }

        /// <summary>
        /// Authorization Code Grant Flow第一步驟或者Implicit Grant Flow
        /// </summary>
        /// <param name="client_id">client_id</param>
        /// <param name="response_type">response_type</param>
        /// <param name="redirect_uri">redirect_uri</param>
        /// <param name="scope">scope</param>
        /// <param name="state">state</param>
        /// <returns>HttpResponseMessage</returns>
        [HttpGet, HttpPost]
        [AuthorizeOrLogin]//需要User登入才能存取
        [ClientValidate(true, "client_id", "redirect_uri")]
        public HttpResponseMessage Auth(string client_id, string response_type, string redirect_uri, string scope = "",
            string state = "")
        {
            OAuthClientModel client = OAuthFacade.GetClient(client_id);

            Member mem = mp.MemberGet(User.Identity.Name);

            if (!mem.IsLoaded)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new ErrorResponse
                {
                    Error = ErrorResponse.InvalidUser,
                    ErrorDescription = ErrorResponse.GetDescription(ErrorResponse.InvalidUser)
                });
            }

            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Redirect);
            UriBuilder ubRedirect = new UriBuilder(HttpUtility.UrlDecode(redirect_uri));

            //Authorization Code Grant Flow第一步驟
            if (string.Equals(response_type, "code", StringComparison.OrdinalIgnoreCase))
            {
                string code = OAuthFacade.CreateAuthorizeCode(mem.UniqueId, client_id);
                ubRedirect.AppendQueryArgument("code", code);
                if (string.IsNullOrEmpty(state) == false)
                {
                    ubRedirect.AppendQueryArgument("state", state);
                }
                response.Headers.Location = ubRedirect.Uri;
            }//Implicit Grant Flow
            else if (string.Equals(response_type, "token", StringComparison.OrdinalIgnoreCase))
            {
                string code = OAuthFacade.CreateAuthorizeCode(mem.UniqueId, client_id);
                TokenResponse token = OAuthFacade.ExchangeToken(code);
                System.Collections.Specialized.NameValueCollection nvQuery =
                    new System.Collections.Specialized.NameValueCollection();
                nvQuery.Add("access_token", token.AccessToken);
                nvQuery.Add("expires_in", config.OAuthTokenExpiredSeconds.ToString());
                nvQuery.Add("token_type", TokenResponse._RESPONSE_TYPE_BEARER);
                if (string.IsNullOrEmpty(state) == false)
                {
                    nvQuery.Add("state", state);
                }
                string tokenInfo = nvQuery.ToString("#", "&");
                response.Headers.Location = new Uri(ubRedirect.Uri, tokenInfo);
            }

            return response;
        }

        /// <summary>
        /// PayEasy Member Password Credentials Grant Flow
        /// </summary>
        /// <param name="loginInfo"></param>
        /// <param name="pezmemId"></param>
        /// <param name="password"></param>
        /// <param name="scope"></param>
        /// <returns></returns>
        [HttpGet, HttpPost]
        [ActionName("payeasy_access_token")]
        [ClientValidate(true, "client_id", "client_secret")]
        public dynamic AccessToken([ModelBinder]OAuthLoginInfo loginInfo, string pezmemId, string password, string scope = "")
        {
            SignInReplyData data = MemberUtility.PayeasyLogin(pezmemId, password);

            if (data.Reply != SignInReply.Success)
            {
                return new
                {
                    Error = ErrorResponse.InvalidUser,
                    ErrorDescription = data.ReplyMessage,
                    ExtId = data.ExtId
                };
            }

            OAuthClientModel client = OAuthFacade.GetClient(loginInfo.ClientId);

            //關連User和App
            if (!OAuthFacade.ClientHasUser(client.Id, data.SignInMember.UserName))
            {
                OAuthFacade.ClientSetUser(client.AppId, data.SignInMember.UniqueId);
            }


            try
            {
                string code = OAuthFacade.CreateAuthorizeCode(data.SignInMember.UniqueId, loginInfo.ClientId);
                TokenResponse accesstoken = OAuthFacade.ExchangeToken(code);

                return accesstoken;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return new ErrorResponse
                {
                    Error = ErrorResponse.ServerError,
                    ErrorDescription = ex.Message
                };
            }
        }

        /// <summary>
        /// Vbs Member Password Credentials Grant Flow 即將停用
        /// </summary>
        /// <param name="loginInfo"></param>
        /// <param name="vbsmemAccId"></param>
        /// <param name="password"></param>
        /// <param name="scope"></param>
        /// <returns></returns>
        [HttpGet, HttpPost]
        [ActionName("vbs_access_token")]
        [ClientValidate(true, "client_id", "client_secret")]
        public dynamic AccessTokenVbs([ModelBinder]OAuthLoginInfo loginInfo, string vbsmemAccId, string password, string scope = "")
        {
            SignInReplyData data = MemberUtility.VbsMemberLogin(vbsmemAccId, password, false);

            if (data.Reply != SignInReply.Success)
            {
                return new
                {
                    Error = ErrorResponse.InvalidUser,
                    ErrorDescription = data.ReplyMessage,
                    ExtId = data.ExtId
                };
            }

            OAuthClientModel client = OAuthFacade.GetClient(loginInfo.ClientId);

            //關連User和App
            if (!OAuthFacade.ClientHasUser(client.Id, data.SignInMember.UserName))
            {
                OAuthFacade.ClientSetUser(client.AppId, data.SignInMember.UniqueId);
            }


            try
            {
                string code = OAuthFacade.CreateAuthorizeCode(data.SignInMember.UniqueId, loginInfo.ClientId);
                TokenResponse accesstoken = OAuthFacade.ExchangeToken(code);

                return accesstoken;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return new ErrorResponse
                {
                    Error = ErrorResponse.ServerError,
                    ErrorDescription = ex.Message
                };
            }
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="loginData"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthAppClientAuth]
        public dynamic VbsAccessToken([FromBody]JObject loginData)
        {
            string userName;
            string password;

            var result = new ApiResult();
            try
            {
                //帳號一律大寫
                userName = loginData["UserName"].ToString().ToUpper();
                password = loginData["Password"].ToString();
            }
            catch
            {
                result.Code = ApiResultCode.InputError;
                result.Message = I18N.Phrase.ApiResultCodeInputError;
                return result;
            }

            bool noCheckPassword = false;

            if (config.IsEnableVbsBindAccount) //商家帳號綁訂17life會員帳號功能開關config
            {
                #region 17life App登入
                SingleSignOnSource source;
                Member member;
                string memberName;
                MemberLinkCollection mlCol = null;

                SignInReply reply = LoginProviders.Login(userName, password, true, ref mlCol, out memberName, out source, out member);  //驗證17

                if (reply == SignInReply.Success)
                {
                    var vbsMembershipBindAccount = mp.VbsMembershipBindAccountGetByUserId(member.UniqueId);
                    userName = vbsMembershipBindAccount.VbsAccountId;
                    noCheckPassword = true;
                }
                #endregion
            }

            #region 商家帳號登入
            var data = MemberUtility.VbsMemberLogin(userName, password, noCheckPassword);

            if (data.Reply != SignInReply.Success)
            {
                //帳號或密碼錯誤
                result.Code = ApiResultCode.BadUserInfo;
                result.Message = I18N.Phrase.ApiResultCodeBadUserInfo;
                return result;
            }
            var mem = mp.VbsMembershipGetByAccountId(userName);
            #endregion

            //成功登入，但須檢查是否需要更改密碼
            mp.VbsAccountAuditSet(mem.Id, mem.AccountId, LunchKingSite.Core.Helper.GetClientIP(), VbsAccountAuditAction.Login, true, mem.IsTemporaryLogin);

            if (mem.IsFirstLogin || mem.IsPasswordResetNeeded)
            {
                result.Code = ApiResultCode.LoginSuccessNeedChangePassword;
                result.Message = I18N.Phrase.ApiResultCodeLoginSuccessNeedChangePassword;
            }

            OAuthClientModel client = OAuthFacade.GetClient(ClientId);

            //關連User和App
            if (!OAuthFacade.ClientHasUser(client.Id, data.SignInMember.UserName))
            {
                OAuthFacade.ClientSetUser(client.AppId, data.SignInMember.UniqueId);
            }

            try
            {
                string code = OAuthFacade.CreateAuthorizeCode(data.SignInMember.UniqueId, ClientId);
                TokenResponse accesstoken = OAuthFacade.ExchangeToken(code);
                result.Code = ApiResultCode.Success;
                result.Data = ApiTokenResponse.GetApiTokenResponse(accesstoken);
                return result;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                result.Code = ApiResultCode.Error;
                result.Message = I18N.Phrase.ApiResultCodeError;
                return result;
            }
        }

        /// <summary>
        /// 商家核銷App登出
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [OAuthAppClientAuth]
        public dynamic VbsLogout()
        {
            try
            {
                return new ApiResult()
                {
                    Code = ApiResultCode.Success,
                    Message = "登出成功"
                };
            }
            catch (Exception ex)
            {
                return new ApiResult()
                {
                    Code = ApiResultCode.Error,
                    Message = "登出失敗",
                    Data = ex
                };
            }
        }

        /// <summary>
        /// Facebook Access Token Credentials Grant Flow
        /// </summary>
        /// <param name="loginInfo"></param>
        /// <param name="fbtoken"></param>
        /// <param name="scope"></param>
        /// <returns></returns>
        [HttpGet, HttpPost]
        [ActionName("facebook_access_token")]
        [ClientValidate(true, "client_id", "client_secret")]
        public dynamic AccessToken([ModelBinder]OAuthLoginInfo loginInfo, string fbtoken, string scope = "")
        {
            SignInReplyData data = MemberUtility.FBLoginIn(fbtoken);
            if (data.Reply != SignInReply.Success)
            {
                return new
                {
                    Error = ErrorResponse.InvalidUser,
                    ErrorDescription = data.ReplyMessage,
                    ExtId = data.ExtId
                };
            }

            OAuthClientModel client = OAuthFacade.GetClient(loginInfo.ClientId);

            //關連User和App
            if (!OAuthFacade.ClientHasUser(client.Id, data.SignInMember.UserName))
            {
                OAuthFacade.ClientSetUser(client.AppId, data.SignInMember.UniqueId);
            }


            try
            {
                string code = OAuthFacade.CreateAuthorizeCode(data.SignInMember.UniqueId, loginInfo.ClientId);
                TokenResponse accesstoken = OAuthFacade.ExchangeToken(code);

                return accesstoken;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return new ErrorResponse
                {
                    Error = ErrorResponse.ServerError,
                    ErrorDescription = ex.Message
                };
            }
        }

        /// <summary>
        /// Resource Owner Password Credentials Grant Flow
        /// </summary>
        /// <param name="loginInfo"></param>
        /// <param name="grant_type"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="scope"></param>
        /// <returns></returns>
        [HttpGet, HttpPost]
        [ActionName("resource_owner_access_token")]
        [ClientValidate(true, "client_id", "client_secret")]
        public dynamic AccessToken([ModelBinder]OAuthLoginInfo loginInfo, string grant_type, string username, string password, string scope = "")
        {
            if (!grant_type.Equals("password", StringComparison.OrdinalIgnoreCase))
            {
                return new ErrorResponse
                {
                    Error = ErrorResponse.UnauthorizedClient,
                    ErrorDescription = "Invalid grant_type"
                };
            }

            //驗證User登入
            if (!Membership.ValidateUser(username, password))
            {
                return new ErrorResponse
                {
                    Error = ErrorResponse.InvalidUser,
                    ErrorDescription = ErrorResponse.GetDescription(ErrorResponse.InvalidUser)
                };
            }

            Member mem = mp.MemberGet(username);

            if (!mem.IsLoaded)
            {
                return new ErrorResponse
                {
                    Error = ErrorResponse.InvalidUser,
                    ErrorDescription = ErrorResponse.GetDescription(ErrorResponse.InvalidUser)
                };
            }

            MemberLinkCollection mlCol = mp.MemberLinkGetList(mem.UniqueId);

            //"會員無串接資料""錯誤的登入方式"
            if (mlCol.Count == 0 || mlCol.Any(t => t.ExternalOrg != (int)SingleSignOnSource.ContactDigitalIntegration))
            {
                return new ErrorResponse
                {
                    Error = ErrorResponse.InvalidUser,
                    ErrorDescription = ErrorResponse.GetDescription(ErrorResponse.InvalidUser)
                };
            }


            OAuthClientModel client = OAuthFacade.GetClient(loginInfo.ClientId);

            //關連User和App
            if (!OAuthFacade.ClientHasUser(client.Id, username))
            {
                OAuthFacade.ClientSetUser(client.AppId, mem.UniqueId);
            }


            try
            {
                string code = OAuthFacade.CreateAuthorizeCode(mem.UniqueId, loginInfo.ClientId);
                TokenResponse token = OAuthFacade.ExchangeToken(code);

                return token;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return new ErrorResponse
                {
                    Error = ErrorResponse.ServerError,
                    ErrorDescription = ex.Message
                };
            }
        }

        /// <summary>
        /// Client Credentials Grant Flow. server to server auth, querystring or basic authentication
        /// </summary>
        /// <returns></returns>    
        [HttpGet, HttpPost]
        [ActionName("client_credentials_access_token")]
        [ClientValidate(true, "client_id", "client_secret")]
        public dynamic AccessToken([ModelBinder]OAuthLoginInfo loginInfo)
        {
            //string client_id = loginInfo.ClientId;
            //string client_secret = loginInfo.ClientSecret;

            OAuthClientModel client = OAuthFacade.GetClient(loginInfo.ClientId);

            try
            {
                var accessToken = OAuthFacade.CreateAppToken(loginInfo.ClientId);
                return new TokenResponse
                {
                    AccessToken = accessToken,
                    ExpiresIn = config.OAuthTokenExpiredSeconds, //2 hours
                };
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return new ErrorResponse
                {
                    Error = ErrorResponse.ServerError,
                    ErrorDescription = ex.Message
                };
            }
        }
        
        /// <summary>
        /// refresh token
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [OAuthAppClientAuth]
        public ApiResult RefreshAccessToken([FromBody]JObject loginData)
        {
            var result = new ApiResult();

            string refreshToken = loginData["RefreshToken"].ToString();
            OAuthClientModel client = OAuthFacade.GetClient(ClientId);

            try
            {
                OAuthTokenModel token = OAuthFacade.GetTokenByRefreshToken(refreshToken);
                //既然要refresh, token 本身過期也沒關係
                if (token != null && token.IsValid)
                {
                    logger.InfoFormat("client {0} want refresh token {1}.", client.AppId, refreshToken);
                    result.Code = ApiResultCode.Success;
                    result.Data = ApiTokenResponse.GetApiTokenResponse(OAuthFacade.RefreshToken(refreshToken));
                    return result;
                }
                else
                {
                    result.Code = ApiResultCode.OAuthTokenExpired;
                    result.Message = I18N.Phrase.ApiResultCodeOAuthTokenExpired;
                    return result;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                result.Code = ApiResultCode.Error;
                result.Message = I18N.Phrase.ApiResultCodeError;
                return result;
            }
        }
        
        /// <summary>
        /// Authorization Code Grant Flow 第二步驟
        /// </summary>
        /// <param name="client_id">client_id</param>
        /// <param name="client_secret">client_secret</param>
        /// <param name="redirect_uri">redirect_uri</param>
        /// <param name="code">authorize_code</param>
        /// <returns></returns>
        [HttpGet, HttpPost]
        [ActionName("authorization_code_access_token")]
        [ClientValidate(true, "client_id", "client_secret")]
        public dynamic AccessToken(string client_id, string client_secret, string redirect_uri, string code)
        {
            OAuthClientModel client = OAuthFacade.GetClient(client_id);

            try
            {
                var res = OAuthFacade.ExchangeToken(code);
                return res;

            }
            catch (Exception ex)
            {
                throw new HttpResponseException(
                    new HttpResponseMessage(HttpStatusCode.Unauthorized) { ReasonPhrase = ex.Message });
            }
        }
        
        /// <summary>
        /// 判斷access_token或refresh_token是否有效
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet, HttpPost]
        public dynamic IsValid(string token)
        {
            ApiResult result = new ApiResult() { Code = ApiResultCode.Success };

            result.Data = OAuthFacade.IsValid(token);

            return result;
        }

        /// <summary>
        /// 讓access_token或refresh_token失效
        /// </summary>
        /// <param name="access_token"></param>
        /// <returns></returns>
        [HttpGet, HttpPost]
        public dynamic SignOut(string access_token)
        {
            ApiResult result = new ApiResult() { Code = ApiResultCode.Success };

            result.Data = OAuthFacade.ExpireToken(access_token);

            return result;
        }

        /// <summary>
        /// 登入後，建立accessToken與裝置間的關聯。
        /// </summary>
        /// <param name="access_token"></param>
        /// <param name="device"></param>
        /// <returns></returns>
        public dynamic SignIn(string access_token, string device)
        {
            ApiResult result = new ApiResult() { Code = ApiResultCode.Success };

            OAuthTokenModel authToken = OAuthFacade.GetTokenByToken(access_token);
            if (authToken == null || authToken.IsExpired || authToken.IsValid == false)
            {
                return new ErrorResponse
                {
                    Error = ErrorResponse.InvalidToken,
                    ErrorDescription = string.Format("token not found.")
                };
            }

            try
            {
                //底下處理移除member_collect_notice中device_id跟user_id關連
                int userId = authToken.UserId.GetValueOrDefault();
                DeviceTokenCollection dtc = np.DeviceTokenCollectionGetList(device);

                MemberCollectNotice mcn = null;
                int device_id = 0;

                foreach (var token in dtc)
                {
                    //取得DeviceToken的id
                    device_id = token.Id;
                    //取得與userId關連的MemberCollectNotice
                    mcn = mp.MemberCollectNoticeGet(userId, device_id);

                    //如果關連存在，直接回傳。
                    if (mcn != null && mcn.IsLoaded)
                    {
                        result.Data = true;
                        return result;
                    }

                }

                //如果MemberCollectNotice不存在，則新增一筆關連資料。
                if (mcn == null || !mcn.IsLoaded)
                {
                    mcn = new MemberCollectNotice();
                    mcn.UserId = userId;
                    mcn.DeviceId = device_id;
                    mcn.TokenId = authToken.Id;
                    mcn.CreateTime = DateTime.Now;

                    mp.MemberCollectNoticeSet(mcn);
                }


                result.Data = true;
            }
            catch (Exception ex)
            {
                result.Code = ApiResultCode.Error;
                result.Data = false;
                result.Message = ex.Message;
            }

            return result;
        }

        /// <summary>
        /// 讓access_token或refresh_token失效，順便移除裝置關連
        /// </summary>
        /// <param name="access_token"></param>
        /// <param name="device"></param>
        /// <returns></returns>
        [HttpGet, HttpPost]
        [ActionName("SignOutDevice")]
        public dynamic SignOut(string access_token, string device)
        {
            ApiResult result = new ApiResult();

            OAuthTokenModel authToken = OAuthFacade.GetTokenByToken(access_token);
            if (authToken == null || authToken.IsExpired || authToken.IsValid == false)
            {
                return new ErrorResponse
                {
                    Error = ErrorResponse.InvalidToken,
                    ErrorDescription = string.Format("token not found.")
                };
            }

            try
            {
                //底下處理移除member_collect_notice中device_id跟user_id關連
                int userId = authToken.UserId.GetValueOrDefault();
                DeviceTokenCollection dtc = np.DeviceTokenCollectionGetList(device);

                foreach (var token in dtc)
                {
                    int device_id = token.Id;
                    mp.MemberCollectNoticeRemove(userId, device_id);
                }

                //底下讓access_token或refresh_token失效
                result.Code = ApiResultCode.Success;
                result.Data = OAuthFacade.ExpireToken(access_token);
            }
            catch (Exception ex)
            {
                result.Code = ApiResultCode.Error;
                result.Data = false;
                result.Message = ex.Message;
            }

            return result;
        }


        /// <summary>
        /// Get member
        /// </summary>
        /// <param name="access_token">access_token</param>
        /// <returns></returns>
        [HttpGet]
        public dynamic Me(string access_token)
        {
            OAuthTokenModel authToken = OAuthFacade.GetTokenByAccessToken(access_token);
            if (authToken == null || authToken.IsExpired || authToken.IsValid == false)
            {
                return new ErrorResponse
                {
                    Error = ErrorResponse.InvalidToken,
                    ErrorDescription = string.Format("token not found.")
                };
            }

            Member member = mp.MemberGet(authToken.UserId.GetValueOrDefault());
            if (member.IsLoaded == false)
            {
                return new ErrorResponse
                {
                    Error = ErrorResponse.InvalidUser,
                    ErrorDescription = string.Format("user not found.")
                };
            }
            return new OAuthMemberModel
            {
                Id = member.UniqueId,
                Name = member.UserName,
                Email = member.UserEmail,
                DisplayName = member.DisplayName,
                Gender = member.Gender == null ? "" : member.Gender.Value.ToString(),
                Birthday = member.Birthday == null ? "" : member.Birthday.Value.ToString("yyyy/MM/dd")
            };
        }

        /// <summary>
        /// Get Oauth_Client.app_id by access token.
        /// </summary>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        [HttpGet]
        [OAuthScope(TokenScope.SystemManagement)]
        public ApiResult GetAppIdByAccessToken(string accessToken)
        {
            OAuthTokenModel authToken = OAuthFacade.GetTokenByToken(accessToken);
            if (authToken == null || authToken.IsExpired || authToken.IsValid == false)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.OAuthTokenExpired
                };
            }

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = new { authToken.AppId, authToken.ExpiredTime }
            };
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.WebApi.Core;
using Newtonsoft.Json.Linq;

namespace LunchKingSite.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class MockController : BaseController
    {
        /// <summary>
        /// 
        /// </summary>
        protected class TaishinPayReqModel
        {
            /// <summary>
            /// 
            /// </summary>
            public string AccessToken { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string PayCode { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public int Amount { get; set; }
        }

        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static Dictionary<string, int> taishinPoints = new Dictionary<string, int>();
        private static List<TaishinPayReqModel> taishinPayReqs = new List<TaishinPayReqModel>();
        private static string linePayLastTransactionId = "";
        private static ReserveInputModel lastLineRequestInfo;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="paraObjs"></param>
        /// <returns></returns>
        [HttpPost]
        public dynamic TaishinBindPayReq(dynamic paraObjs)
        {
            string code = Guid.NewGuid().ToString();
            taishinPayReqs.Add(new TaishinPayReqModel {PayCode = code, Amount = (int)paraObjs.amount, AccessToken = (string)paraObjs.access_token});
            return new
            {
                rtnCode = 0,
                code = code
            };
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="paraObjs"></param>
        /// <returns></returns>
        [HttpPost]
        public dynamic TaishinAPIBindUserInfo(dynamic paraObjs)
        {
            string accessToken = paraObjs.access_token;
            if (taishinPoints.ContainsKey(accessToken) == false)
            {
                taishinPoints.Add(accessToken, 5000);
            }
            return new
            {
                rtnCode = 0,
                account_name = "test",
                account_amt = taishinPoints[accessToken]
            };
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="paraObjs"></param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage TaishinThirdPartyPay([FromBody]JObject paraObjs)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Redirect);
            UriBuilder ubRedirect = new UriBuilder(Helper.CombineUrl(config.SSLSiteUrl, "/mvc/ThirdPartyPay/TaishinPaySuccess"));

            string payCode = paraObjs.GetValue("code").Value<string>();

            TaishinPayReqModel model = taishinPayReqs.First(t => t.PayCode == payCode);
            taishinPoints[model.AccessToken] -= model.Amount;

            ubRedirect.AppendQueryArgument("order_id", paraObjs.GetValue("order_id").Value<string>());
            ubRedirect.AppendQueryArgument("code", model.PayCode);
            ubRedirect.AppendQueryArgument("rtnCode", "0");
            ubRedirect.AppendQueryArgument("rtnMsg", "for test");
            
            response.Headers.Location = ubRedirect.Uri;
            return response;
        }


        /*
         * 進行LinePay開發時的測試，調整底下3個小web.config參數
         * <add key="LinePayApiReserve" value="https://{site-url}/mock/linepay/v1/payments/request" />
         * <add key="LinePayApiConfirm" value="https://{site-url}/mock/linepay/v1/payments/{0}/confirm" />
         * <add key="LinePayApiRefund" value="https://{site-url}/mock/linepay/v1/payments/{0}/refund" />
         */
        /// <summary>
        /// LinePay 的測試請求網址
        /// </summary>
        /// <param name="paraObjs"></param>
        /// <returns></returns>
        [HttpPost]
        public dynamic LinePayRequest([FromBody]JObject paraObjs)
        {
            //測試用，乎略thread not safe issue.
            linePayLastTransactionId = DateTime.Now.ToString("yyyyMMdd") + DateTime.Now.Ticks.ToString().Substring(8);
            lastLineRequestInfo = new ReserveInputModel
            {
                OrderId = paraObjs["orderId"].Value<string>(),
                Amount = paraObjs["amount"].Value<int>(),
                ConfirmUrl = paraObjs["confirmUrl"].Value<string>(),
                CancelUrl = paraObjs["cancelUrl"].Value<string>(),
            };
            string jsonStr = @"
{""returnCode"":""0000"",""returnMessage"":""Success."",""info"":{""paymentUrl"":{""web"":""@payUrl"",""app"":""line://pay/payment/xxxxxxxxxxxxxxxxx""},""transactionId"":@transactionId}}
            ";
            jsonStr = jsonStr.Replace("@payUrl", Helper.CombineUrl(config.SiteUrl,
                "mock/linepay/web/payments/wait?transactionReserveId=" + Guid.NewGuid().ToString("N") + "&locale=zh-Hant_LP"));
            jsonStr = jsonStr.Replace("@transactionId", linePayLastTransactionId);
            return JObject.Parse(jsonStr);
        }
        /// <summary>
        /// LinePay 的測試付款網址
        /// </summary>
        /// <param name="transactionReserveId"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage LinePayPayments(string transactionReserveId)
        {
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Redirect);
            UriBuilder ubRedirect;
            if (lastLineRequestInfo.Amount < 1000)
            {
                ubRedirect = new UriBuilder(lastLineRequestInfo.ConfirmUrl + "&transactionId=" + linePayLastTransactionId);
            }
            else
            {
                ubRedirect = new UriBuilder(lastLineRequestInfo.CancelUrl + "&transactionId=" + linePayLastTransactionId);
            }
            response.Headers.Location = ubRedirect.Uri;
            return response;
        }

        public dynamic LinePayConfirm(string transactionId, [FromBody] JObject paraObjs)
        {
            string jsonStr = @"
{""returnCode"":""@returnCode"",""returnMessage"":""Success."",""info"":{""transactionId"":@transactionId,""orderId"":""@orderId"",""payInfo"":[{""method"":""CREDIT_CARD"",""amount"":@amount,""maskedCreditCardNumber"":""************0001"",""binNo"":""888888""}]}}
";
            if (transactionId == linePayLastTransactionId)
            {
                jsonStr = jsonStr.Replace("@returnCode", "0000");
            }
            else
            {
                jsonStr = jsonStr.Replace("@returnCode", "1111");
            }
            jsonStr = jsonStr.Replace("@transactionId", linePayLastTransactionId);
            jsonStr = jsonStr.Replace("@orderId", lastLineRequestInfo.OrderId);
            jsonStr = jsonStr.Replace("@amount", lastLineRequestInfo.Amount.ToString());
            return JObject.Parse(jsonStr);
        }

        public dynamic LinePayRefund(string transactionId, [FromBody] JObject paraObjs)
        {
            string jsonStr = @"
{""returnCode"":""@returnCode"",""returnMessage"":""Success."",""info"":{""transactionId"":@transactionId,""orderId"":""@orderId"",""payInfo"":[{""method"":""CREDIT_CARD"",""amount"":@amount,""maskedCreditCardNumber"":""************0001"",""binNo"":""888888""}]}}
";
            string amount = paraObjs["refundAmount"].Value<int>().ToString();
            if (transactionId == linePayLastTransactionId && amount == lastLineRequestInfo.Amount.ToString())
            {
                jsonStr = jsonStr.Replace("@returnCode", "0000");
            }
            else
            {
                jsonStr = jsonStr.Replace("@returnCode", "1111");
            }

            jsonStr = jsonStr.Replace("@transactionId", linePayLastTransactionId);
            jsonStr = jsonStr.Replace("@orderId", lastLineRequestInfo.OrderId);
            jsonStr = jsonStr.Replace("@amount", lastLineRequestInfo.Amount.ToString());
            return JObject.Parse(jsonStr);
        }
    }
}

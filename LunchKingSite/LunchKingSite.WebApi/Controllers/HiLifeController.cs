﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.BizLogic.Models.FamiPort;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.ModelCustom;
using LunchKingSite.Core.Models.Entities;
using LunchKingSite.WebApi.Core.Attributes;
using LunchKingSite.WebApi.Core.OAuth;
using LunchKingSite.DataOrm;
using LunchKingSite.Core.Interface;

namespace LunchKingSite.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class HiLifeController : BaseController
    {
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static IMGMProvider mgmp = ProviderFactory.Instance().GetProvider<IMGMProvider>();
        private static IMemberProvider member = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private static IHiLifeProvider hiLife = ProviderFactory.Instance().GetProvider<IHiLifeProvider>();
        private static readonly ILog logger = LogManager.GetLogger("HiLife");
        private static JsonSerializer js = new JsonSerializer();

        #region Api

        /// <summary>
        /// 取得兌換條碼(App->Server)
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [RequireHttps]
        [OAuthScope(TokenScope.Deal)]
        public ApiResult GetHiLifePincode(GetPincodeApiInput input)
        {
            HiLifeApiLog log = new HiLifeApiLog();
            HiLifeFacade.SetHiLifeApiLog(true, "GetHiLifePincode", js.Serialize(input), string.Empty, log);

            var result = new ApiResult();
            try
            {
                List<int> excludeCouponId = new List<int>();

                //filter gift active:1 濾掉正在送禮的
                var gift = mgmp.GiftGetByOid(input.OrderGuid).Where(x => x.IsActive == true && x.SenderId == User.Identity.Id).ToList();
                if (gift.Any())
                {
                    excludeCouponId.AddRange(gift.Select(g => g.CouponId ?? 0));
                }

                var pincodeData = hiLife.GetHiLifePincodeAndSee(input.OrderGuid, input.ExchangeCount, excludeCouponId);

                if (!pincodeData.Any())
                {
                    result.Code = ApiResultCode.DataNotFound;
                    HiLifeFacade.SetHiLifeApiLog(false, "GetHiLifePincode", string.Empty, js.Serialize(result), log);
                    return result;
                }


                var groupPincodeList = new List<HiLifePincodeModel>();
                foreach (var p in pincodeData)
                {
                    groupPincodeList.Add(new HiLifePincodeModel
                    {
                        CouponId = p.Id,
                        PincodeList = new List<string>()
                    {
                        config.HiLifeFixedFunctionPincode,//第一段固定功能條碼
                        p.Pincode//peztemp.pezcode廠商匯入序號為第二段條碼
                    },
                    });
                }

                //add log
                hiLife.AddHiLifeGetPincodeLog(pincodeData, User.Identity.Id, input.OrderGuid, input.ExchangeCount);
                result.Code = ApiResultCode.Success;
                result.Data = groupPincodeList;
            }
            catch (Exception ex)
            {
                logger.Info(string.Format("HiLife API GetHiLifePincode Exception={0} OrderGuid={1},ExchangeCount={2}", ex.ToString(), input.OrderGuid, input.ExchangeCount));
                result.Code = ApiResultCode.Error;
                result.Message = ex.Message;
            }
            

            HiLifeFacade.SetHiLifeApiLog(false, "GetHiLifePincode", string.Empty, js.Serialize(result), log);

            return result;
        }

        /// <summary>
        /// 退貨(App->中繼Server->HiLife)
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [RequireHttps]
        [OAuthScope(TokenScope.SystemManagement)]
        public ApiResult ReturnPincode(ReturnPincodeApiInput input)
        {
            HiLifeApiLog log = new HiLifeApiLog();
            HiLifeFacade.SetHiLifeApiLog(true, "ReturnPincode", js.Serialize(input), string.Empty, log);

            ApiResult result = new ApiResult
            {
                Code = ApiResultCode.DataNotFound
            };
            try
            {
                string returnMsg;
                result.Code = OrderFacade.HiLifePincodeReturn(input.OrderGuid, out returnMsg, HiLifeReturnType.DealClosed) ? ApiResultCode.Success : ApiResultCode.Error;
                result.Message = returnMsg;
            }
            catch (Exception ex)
            {
                logger.Info(string.Format("HiLife API ReturnPincode Exception={0} Bid={1},OrderGuid={2}", ex.ToString(), input.Bid, input.OrderGuid));
                result.Code = ApiResultCode.Error;
                result.Message = ex.Message;
            }

            HiLifeFacade.SetHiLifeApiLog(false, "ReturnPincode", string.Empty, js.Serialize(result), log);
            return result;
        }


        [RequireHttps]
        [HttpGet]
        public ApiResult VerifyTest(string input)
        {
            Guid bathGuid = Guid.NewGuid();
            ApiResult result = new ApiResult
            {
                Code = ApiResultCode.DataNotFound
            };
            List<string> wantVerifyPincodeList = input.Split(',').ToList();
            Dictionary<string, string> resultDic = new Dictionary<string, string>();
            foreach (var pincode in wantVerifyPincodeList)
            {
                HiLifeVerifyLog entity = new HiLifeVerifyLog();
                entity.ExchangeTranTime = DateTime.Now.ToString("yyyyMMdd_HHmmss");//15
                entity.StoreCode = "App_";//4
                entity.MachineCode = "0";//1
                entity.ExchangeDate = DateTime.Now.ToString("yyyyMMdd");//8
                entity.InvoiceNumber = "0123456789";//10
                entity.IsSuccessFlag = "1";//1
                entity.VerifyType = (int)HiLifeVerifyType.UserTimely;

                entity.BatchId = bathGuid;
                entity.HilifeInput = js.Serialize(input);
                entity.Pincode = pincode;
                var r = HiLifeFacade.PincodeCheckAndVerify(pincode, entity);
                if (!resultDic.ContainsKey(pincode))
                    resultDic.Add(pincode, r.Message);
            }
            result.Data = resultDic;
            return result;
        }


        /// <summary>
        /// 核銷Pincode(HiLife->中繼Server->17Life)
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [RequireHttps]
        public ApiResult VerifyPincode(ApiVerifyPincodeApiInput input)
        {
            HiLifeApiLog log = new HiLifeApiLog();
            HiLifeFacade.SetHiLifeApiLog(true, "VerifyPincode", js.Serialize(input), string.Empty, log);

            Guid bathGuid = Guid.NewGuid();
            ApiResult result = new ApiResult
            {
                Code = ApiResultCode.DataNotFound
            };

            //檢查input字數是否有誤
            if (input.Head.Length != 39 || input.Body.Length % 21 != 0)
            {
                HiLifeFacade.SaveParseErrorLog("HiLife input is wrong length", bathGuid, js.Serialize(input), null, string.Empty, HiLifeVerifyType.UserTimely);
                result.Code = ApiResultCode.InputError;

                HiLifeFacade.SetHiLifeApiLog(false, "VerifyPincode", string.Empty, js.Serialize(result), log);
                return result;
            }

            if (!input.Body.Contains("|"))
            {
                HiLifeFacade.SaveParseErrorLog("HiLife input not contain | ", bathGuid, js.Serialize(input), null, string.Empty, HiLifeVerifyType.UserTimely);
                result.Code = ApiResultCode.InputError;

                HiLifeFacade.SetHiLifeApiLog(false, "VerifyPincode", string.Empty, js.Serialize(result), log);
                return result;
            }

            List<string> wantVerifyPincodeList = input.Body.Split('|').ToList();
            if (!wantVerifyPincodeList.Any())
            {
                HiLifeFacade.SaveParseErrorLog("HiLife no input any pincode", bathGuid, js.Serialize(input), null, string.Empty, HiLifeVerifyType.UserTimely);
                result.Code = ApiResultCode.InputError;
                result.Message = "未輸入pincode";

                HiLifeFacade.SetHiLifeApiLog(false, "VerifyPincode", string.Empty, js.Serialize(result), log);
                return result;
            }

            try
            {
                foreach (var pincode in wantVerifyPincodeList)
                {
                    HiLifeVerifyLog entity = new HiLifeVerifyLog();
                    entity.ExchangeTranTime = input.Head.Substring(0, 15);//15
                    entity.StoreCode = input.Head.Substring(15, 4);//4
                    entity.MachineCode = input.Head.Substring(19, 1);//1
                    entity.ExchangeDate = input.Head.Substring(20, 8);//8
                    entity.InvoiceNumber = input.Head.Substring(28, 10);//10
                    entity.IsSuccessFlag = input.Head.Substring(38, 1);//1
                    if (input.IsBatch)
                    {
                        entity.VerifyType = (int)HiLifeVerifyType.BatchFile;
                    }
                    else
                    {
                        entity.VerifyType = (int)HiLifeVerifyType.UserTimely;
                    }

                    entity.BatchId = bathGuid;
                    entity.HilifeInput = js.Serialize(input);
                    entity.IpAddress = LunchKingSite.Core.Helper.GetClientIP();
                    string realPincode = pincode.Replace("_", "");
                    entity.Pincode = realPincode;

                    if (entity.IsSuccessFlag == "1")
                    {
                        result = HiLifeFacade.PincodeCheckAndVerify(realPincode, entity);

                        HiLifeFacade.SetHiLifeApiLog(false, "VerifyPincode", string.Empty, js.Serialize(result), log);
                        return result;
                    }
                    else
                    {
                        HiLifeFacade.SaveVerifyLog(entity, "IsSuccessFlag=2 cancle verify");
                        result.Code = ApiResultCode.CouponPartialFail;
                        result.Message = "非兌換動作";

                        HiLifeFacade.SetHiLifeApiLog(false, "VerifyPincode", string.Empty, js.Serialize(result), log);
                        return result;
                    }
                    
                }
            }
            catch (Exception e)
            {
                logger.Info(string.Format("HiLife API VerifyPincode Exception={0} Head={1},Body={2}", e.ToString(), input.Head, input.Body));
                result.Code = ApiResultCode.Error;
                result.Message = e.Message;
            }

            HiLifeFacade.SetHiLifeApiLog(false, "VerifyPincode", string.Empty, js.Serialize(result), log);

            return result;
        }
        
        #endregion
    }
}

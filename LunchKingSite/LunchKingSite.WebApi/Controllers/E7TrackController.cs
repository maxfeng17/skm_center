﻿using System;
using System.Web.Http;
using LunchKingSite.Core;
using System.Collections.Generic;
using System.Web;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.Component;
using Helper = LunchKingSite.Core.Helper;
using System.Threading.Tasks;
using LunchKingSite.Core.Interface;

namespace LunchKingSite.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class E7TrackController : ApiController
    {
        static IMessageQueueProvider mqp = ProviderFactory.Instance().GetProvider<IMessageQueueProvider>();
        static IPponEntityProvider pep = ProviderFactory.Instance().GetProvider<IPponEntityProvider>();
        /// <summary>
        /// 測試用 api
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public dynamic Record(List<string[]> model)
        {
            if (model != null)
            {
                foreach (string[] items in model)
                {
                    try
                    {
                        if (items[0] == "view")
                        {
                            if (items[1] == "deal")
                            {
                                if (items.Length != 5)
                                {
                                    continue;
                                }
                                var context = HttpContext.Current;
                                HttpCookie rsrcCookie = context.Request.Cookies[LkSiteCookie.ReferrerSourceId.ToString()];
                                Guid? visitorIdentity = CookieManager.GetVisitorIdentity();
                                OrderFromType deviceType = Helper.GetOrderFromType();
                                string userIp = Helper.GetClientIP(true);
                                Task.Run(() => TrackFacade.RecordTrack(
                                    items,
                                    context.Request.UserAgent,
                                    this.User.Identity.Name,
                                    rsrcCookie == null ? string.Empty : rsrcCookie.Value, visitorIdentity,
                                    userIp,
                                    deviceType));

                            }
                        }
                    }
                    catch (Exception)
                    {

                    }
                }
            }
            return Ok(".");
        }

        [HttpPost]
        public dynamic LineUserHitPage([FromBody]dynamic input)
        {
            pep.LineUserPageViewSet(new LunchKingSite.Core.Models.PponEntities.LineUserPageView
            {
                DealGuid = (Guid)input.dealGuid,
                Topic = (string)input.topic,
                LineUserId = (string)input.lineUserId
            });



            return Ok(".");
        }

    }
}

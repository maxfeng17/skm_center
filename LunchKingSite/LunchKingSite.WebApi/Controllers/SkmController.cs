﻿using System;
using System.Web.Http;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.WebApi.Core.OAuth;
using log4net;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.WebApi.Models.SkmModels;
using LunchKingSite.BizLogic.Facade;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.OAuth;
using LunchKingSite.BizLogic.Models.Skm;
using LunchKingSite.WebApi.Controllers.Base;
using LunchKingSite.DataOrm;
using LunchKingSite.WebApi.Core.Attributes;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.Interface;
using LunchKingSite.Core.Models.Entities;
using Pokeball = LunchKingSite.WebApi.Models.SkmModels.Pokeball;
using System.Transactions;
using LunchKingSite.BizLogic.Models.Wallet;
using LunchKingSite.Core.ModelCustom;
using Newtonsoft.Json;
using JsonSerializer = LunchKingSite.Core.JsonSerializer;
using LunchKingSite.BizLogic.Models.TurnCloud;
using Helper = LunchKingSite.Core.Helper;
using System.Data;
using LunchKingSite.BizLogic.Component.RocketMQTrans;
    namespace LunchKingSite.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [RequireHttps]
    [ForceCamelCase]
    [SkmExceptionHandler]
    public class SkmController : ApiControllerBase
    {
        #region Property

        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private static IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        private static ISkmProvider skmMp = ProviderFactory.Instance().GetProvider<ISkmProvider>();
        private static ICmsProvider cms = ProviderFactory.Instance().GetProvider<ICmsProvider>();
        private static ISellerProvider sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        private static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private static ILocationProvider lp = ProviderFactory.Instance().GetProvider<ILocationProvider>();
        private static ILog logger = LogManager.GetLogger("Skm");
        private static KeyValuePair<ActionEventInfo, DateTime> _aei;
        private static ISerializer json = ProviderFactory.Instance().GetSerializer();
        private static ISystemProvider _sysp;
        private const string SkmPublicImageSysName = "SkmPublicImage";
        private static IBeaconProvider bp = ProviderFactory.Instance().GetProvider<IBeaconProvider>();
        private static IEventProvider ep = ProviderFactory.Instance().GetProvider<IEventProvider>();
        private static ISkmEfProvider skmEfp = ProviderFactory.Instance().GetProvider<ISkmEfProvider>();
        private static IChannelEventProvider cep = ProviderFactory.Instance().GetProvider<IChannelEventProvider>();
        private static IExhibitionProvider exp = ProviderFactory.Instance().GetProvider<IExhibitionProvider>();

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public SkmController()
        {
            _sysp = ProviderFactory.Instance().GetProvider<ISystemProvider>();
            if (_aei.Key == null)
            {
                _aei = new KeyValuePair<ActionEventInfo, DateTime>();
            }
        }

        #region SKM Member API

        /// <summary>
        /// 取得新光三越站台會員串接 OauthKey
        /// </summary>
        [HttpGet]
        [OAuthScope(TokenScope.Skm)]
        public ApiResult GetSkmSiteOauthKey()
        {
            return new ApiResult()
            {
                Code = ApiResultCode.Success,
                Data = SystemDataManager.GetFromCache<List<SkmSiteOauthKeyModel>>(SystemDataManager._SKM_OAUTH_KEY)
            };
        }

        /// <summary>
        /// 置換 SKM Token
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult ExchangeSkmToken(SkmExchangeTokenModel model)
        {
            SkmFacade.ExchangeSkmToken(model);

            return new ApiResult
            {
                Code = ApiResultCode.Success
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult GetTicket(GetTicketModel model)
        {
            if (model == null || !model.IsValid)
            {
                logger.InfoFormat("GetTicket.Error->Model:{0}",
                    model == null ? string.Empty : new JsonSerializer().Serialize(model));
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }

            //驗證 skm 會員是否正常(call 環友 api)
            if (config.IsValidateSkmMember &&
                !SkmFacade.CheckSkmToken(model.DeviceId, model.CardCheckCode, model.RegistrationId, model.CardNumber, model.DeviceOs, model.WebLoginPassword, model.SkmToken))
            {
                logger.InfoFormat("GetTicket.Error->ValidateSkmMember Error:{0}", new JsonSerializer().Serialize(model));
                return new ApiResult
                {
                    Code = ApiResultCode.SkmMemberVaildateFail
                };
            }

            #region Check skm_member

            LunchKingSite.Core.Helper.SetContextItem(LkSiteContextItem._DEVICE_TYPE, ApiUserDeviceType.WebService);
            //get or add skm_member
            var skmMember = SkmFacade.MemberGetBySkmToken(model.SkmToken, model.IsShared);

            if (!skmMember.IsLoaded)
            {
                logger.InfoFormat("GetTicket.Error->Register Fail Error:{0}", new JsonSerializer().Serialize(model));
                return new ApiResult
                {
                    Code = ApiResultCode.UserRegisterFail
                };
            }

            if (!skmMember.IsEnable)
            {
                logger.InfoFormat("GetTicket.Error->Account Disable:{0}", skmMember.UserId);
                return new ApiResult
                {
                    Code = ApiResultCode.AccountDisable
                };
            }

            #endregion

            try
            {
                var ticketData =
                    new ResponseTicketModel(
                        ApiTokenResponse.GetApiTokenResponse(OAuthFacade.CreateMemberToken(config.SkmAppId,
                            skmMember.UserId)), skmMember.IsShared);

                //核發 17Life token
                return new ApiResult
                {
                    Code = ApiResultCode.Success,
                    Data = ticketData
                };
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("GetTicket.Error->Create Token({0}):{1}, {2}", skmMember.UserId, ex.Message, ex.StackTrace);
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult RefreshTicket(RefreshTokenInputModel model)
        {
            try
            {
                if (model.RefreshToken == string.Empty)
                {
                    return new ApiResult
                    {
                        Code = ApiResultCode.InputError
                    };
                }

                //驗證 refresh token
                var token = OAuthFacade.GetTokenByRefreshToken(model.RefreshToken);
                if (token == null || token.UserId == null)
                {
                    return new ApiResult
                    {
                        Code = ApiResultCode.InputError
                    };
                }

                //驗證 skm member
                var skmMember = skmMp.MemberGetByUserId((int)token.UserId);
                if (!skmMember.IsLoaded || !skmMember.IsEnable)
                {
                    return new ApiResult
                    {
                        Code = ApiResultCode.AccountDisable
                    };
                }

                //核發新 token
                var tokenResponse = OAuthFacade.RefreshToken(model.RefreshToken);
                return new ApiResult
                {
                    Code = ApiResultCode.Success,
                    Data = ApiTokenResponse.GetApiTokenResponse(tokenResponse)
                };
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("skm refresh ticket error({0}). {1}, {2}", model.RefreshToken, ex.Message, ex.StackTrace);
                return new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = ex.Message
                };
            }
        }

        /// <summary>
        /// 註銷(設定過期) token
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Skm)]
        public ApiResult ExpireTicket()
        {
            if (OAuthFacade.ExpireToken(HttpContext.Current.Items["AccessToken"].ToString()))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.Success
                };
            }

            return new ApiResult
            {
                Code = ApiResultCode.InputError
            };
        }

        /// <summary>
        /// 設定共同會員
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Skm)]
        public ApiResult SetMemberShared(SetMemberSharedInputModel model)
        {
            if (skmMp.MemberCancelSharedByUserId(MemberFacade.GetUniqueId(HttpContext.Current.User.Identity.Name), model.IsShared))
            {
                var skmMember = skmMp.MemberGetByUserId(User.Identity.Id);
                skmMp.SkmLogSet(new SkmLog() { SkmToken = skmMember.SkmToken, UserId = skmMember.UserId, LogType = (int)SkmLogType.IsSharedChange, InputValue = (model.IsShared) ? "IsShared=True" : "IsShared=False", OutputValue = "SetMemberShared is Success" });
                return new ApiResult
                {
                    Code = ApiResultCode.Success,
                    Data = new { skmMember.IsShared }
                };
            }

            return new ApiResult
            {
                Code = ApiResultCode.InputError,
                Data = new { IsShared = false }
            };
        }

        /// <summary>
        /// 取得共同會員資訊
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Skm)]
        public ApiResult GetMemberShared()
        {
            var skmMember = skmMp.MemberGetByUserId(User.Identity.Id);

            if (skmMember.IsLoaded)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.Success,
                    Data = new { skmMember.IsShared }
                };
            }
            else
            {
                return new ApiResult
                {
                    Code = ApiResultCode.Success,
                    Data = new { IsShared = false }
                };
            }
        }

        /// <summary>
        /// 取得共同行銷會員條款
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ApiResult GetMemberArticlesOfAgreement()
        {
            ApiResult result = new ApiResult();
            var cmsContent = cms.CmsContentGet(config.SkmAgreementOfCmsContentId);
            if (cmsContent.IsLoaded)
            {
                result.Data = cmsContent.Body;
                result.Code = ApiResultCode.Success;
            }
            return result;
        }

        #endregion

        #region 一般檔次/體驗商品收藏功能相關

        /// <summary>
        /// 取得會員收藏的檔次資料
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Skm)]
        public ApiResult<List<ApiMemberCollectDeal>> GetMemberCollectDealList(GetMemberCollectDealListInputModel model)
        {
            if (model.IsValid == false)
            {
                return new ApiResult<List<ApiMemberCollectDeal>>
                {
                    Code = ApiResultCode.InputError
                };
            }
            MemberCollectDealType collectType = model.CollectType;
            int userId = User.Identity.Id;
            var collectDeals = ApiMemberManager.GetSkmMemberCollectDealList(userId, collectType);

            if (model.DealVersion >= ExternalDealVersion.Burning)
            {
                var bcid = SkmFacade.SkmDefaultCategoryId;
                List<Guid> excludeBid = new List<Guid>();

                foreach (var d in collectDeals)
                {
                    if (pp.GetCategoryDealCountByBidAndCid(d.Bid, bcid) == 0)
                    {
                        excludeBid.Add(d.Bid);
                        continue;
                    }

                    var externalDeal = SkmCacheFacade.GetOnlineViewExternalDeal(d.Bid);
                    var be = skmMp.SkmBurningEventGet(externalDeal.Guid, externalDeal.DealVersion >= (int)ExternalDealVersion.SkmPay ? externalDeal.ShopCode : string.Empty);
                    if (be.IsLoaded && be.BurningPoint > 0)
                    {
                        d.IsBurningEvent = true;
                        d.BurningPoint = be.BurningPoint;
                    }

                    if (externalDeal.IsLoaded)
                    {
                        d.Free = externalDeal.Free ?? 0;
                        d.Buy = externalDeal.Buy ?? 0;
                        var dealIconData = SkmFacade.GetDealIconModelList();
                        var tempIcon = dealIconData.Where(x => x.Id == externalDeal.DealIconId).ToList();
                        d.IconInfo = tempIcon.Any() ? new SkmDealIcon(tempIcon.First()) : new SkmDealIcon();
                        d.IsSkmPay = externalDeal.IsSkmPay;
                        if (d.IsSkmPay)
                        {
                            d.BurningPoint = externalDeal.SkmPayBurningPoint;
                        }
                    }
                }

                //排除非燒點分類檔次
                collectDeals = collectDeals.Where(x => !excludeBid.Contains(x.Bid)).ToList();
            }

            return new ApiResult<List<ApiMemberCollectDeal>>
            {
                Code = ApiResultCode.Success,
                Data = collectDeals
            };
        }

        /// <summary>
        /// 收藏檔次/體驗商品
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Skm)]
        public ApiResult SetMemberCollectDeal(MemberCollectDealSetModel model)
        {
            Guid bGuid;
            if (!Guid.TryParse(model.Bid, out bGuid))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }

            var userId = MemberFacade.GetUniqueId(HttpContext.Current.User.Identity.Name);

            if (MemberFacade.MemberCollectDealSetForApp(userId, bGuid, model.Collect, model.Notice, model.CollectType))
            {
                MemberCollectDeal mcd = mp.MemberCollectDealGet(userId, bGuid);
                return new ApiResult
                {
                    Code = ApiResultCode.Success,
                    Data = new { IsCollected = mcd.IsLoaded && mcd.CollectStatus == (byte)MemberCollectDealStatus.Collected }
                };
            }

            return new ApiResult
            {
                Code = ApiResultCode.Error
            };
        }

        /// <summary>
        /// 取消所有已結檔的收藏檔次/體驗商品
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Skm)]
        public ApiResult RemoveMemberCollectDealOutOfDate()
        {
            mp.MemberCollectDealRemoveOutOfDateDeal(User.Identity.Id);
            return new ApiResult
            {
                Code = ApiResultCode.Success
            };
        }

        /// <summary>
        /// 取消登入會員的特定收藏檔次/體驗商品的收藏紀錄，指定方式為傳入檔次的BID陣列
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Skm)]
        public ApiResult RemoveMemberCollectDeal(MemberCollectDealRemoveModel model)
        {
            //驗證傳入的bidList是否正確
            List<Guid> bids = model.BidList.Select(Guid.Parse).ToList();
            mp.MemberCollectDealRemove(MemberFacade.GetUniqueId(HttpContext.Current.User.Identity.Name), bids);
            return new ApiResult
            {
                Code = ApiResultCode.Success
            };
        }
        #endregion

        #region 結帳

        /// <summary>
        /// 取得付款時需要的檔次資料 for 3.7(含) 以後
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Skm)]
        public ApiResult GetSkmDealDataForCheckout(DealDataForCheckoutModel model)
        {
            #region Init

            Guid bGuid;
            if (!Guid.TryParse(model.Bid, out bGuid))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }

            string userName = HttpContext.Current.User.Identity.Name;
            if (string.IsNullOrWhiteSpace(userName))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.UserNoSignIn
                };
            }

            #endregion

            //先過本站 ApiPponDealCheckOut
            string message;
            var skmDealData = new SkmPponDealDealCheckout
            {
                PponDealCheckout = PponDealApiManager.ApiPponDealCheckoutDataGet(bGuid, userName, null, false, out message, ApiUserManager.IsOldAppUserId(MemberFacade.GetUniqueId(HttpContext.Current.User.Identity.Name).ToString())),
                IsBurningEvent = false,
                BurningPoint = 0
            };

            if (!string.IsNullOrEmpty(message))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError,
                    Message = message,
                    Data = skmDealData
                };
            }

            var externalDeal = SkmCacheFacade.GetOnlineViewExternalDeal(bGuid);

            //隱藏檔踢掉不能買，這裡的externalDeal.Bid 是 MainBid
            if (SkmFacade.CheckExternalDealIsHidden(externalDeal.Bid))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.DealsNotSale
                };
            }

            if (externalDeal.IsSkmPay)
            {
                skmDealData.BurningPoint = externalDeal.SkmPayBurningPoint;
                skmDealData.PponDealCheckout.Price = externalDeal.Discount;
                skmDealData.IsSkmPay = true;
            }

            #region 處理館櫃資訊

            var shopCode = externalDeal.ShopCode;
            //取得此館下的APP顯示櫃文字
            IEnumerable<ViewExternalDealStoreDisplayName> storeDisplayNames = skmEfp.GetViewExternalDealStoreDisplayNameByDealGuid(externalDeal.Guid).Where(item => item.ShopCode == shopCode);
            //組合APP顯示櫃位文字
            string storeDisplayName = string.Join("\n", storeDisplayNames.Select(item => { return item.DisplayName; }).OrderBy(item => item));
            SkmShoppeCollection skmShoppes = skmMp.SkmShoppeGetAllShoppe(externalDeal.SellerGuid, shopCode);
            //預先排除不屬於此館的櫃位
            skmDealData.PponDealCheckout.Stores =
                skmDealData.PponDealCheckout.Stores.Where(item => skmShoppes.Any(shoppe => item.StoreGuid == shoppe.StoreGuid)).ToList();
            foreach (var s in skmDealData.PponDealCheckout.Stores)
            {
                SkmShoppe shoppe = skmShoppes.FirstOrDefault(item => item.StoreGuid == s.StoreGuid);
                if (shoppe == null || string.IsNullOrEmpty(shoppe.ShopCode))
                {
                    s.StoreName = string.Empty;
                }
                if (!string.IsNullOrEmpty(storeDisplayName))
                {
                    s.StoreName = storeDisplayName;
                }
                else
                {
                    s.StoreName = SkmFacade.GetSkmShoppeDisplayName(s.StoreGuid);
                }
            }

            #endregion

            var be = SkmFacade.GetBurningEventByBid(bGuid);
            if (be.IsLoaded)
            {
                skmDealData.IsBurningEvent = true;
                skmDealData.BurningPoint = be.BurningPoint;
            }

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Message = message,
                Data = skmDealData
            };
        }

        /// <summary>
        /// 取得Skm Pay 預備交易號碼
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Skm)]
        public ApiResult GetSkmPayPreTransNo(GetSkmPayPreTransNoRequest model)
        {
            ApiResult result = new ApiResult();

            Guid bGuid;
            if (!Guid.TryParse(model.Bid, out bGuid))
            {
                result.Code = ApiResultCode.InputError;
                return result;
            }

            ExternalDealCombo externalDealCombo = skmEfp.GetExternalDealComboByBid(bGuid);
            if (externalDealCombo == null || !externalDealCombo.MainBid.HasValue)
            {
                result.Code = ApiResultCode.DataNotFound;
                return result;
            }
            var externalDeal = SkmCacheFacade.GetOnlineViewExternalDeal(externalDealCombo.MainBid.Value);
            if (!externalDeal.IsSkmPay)
            {
                result.Code = ApiResultCode.DataNotFound;
                return result;
            }

            if (SkmFacade.CheckExternalDealIsHidden(externalDealCombo.MainBid))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.DealsNotSale
                };
            }

            WalletGetPreTransNoRequest walletGetPreTransNoRequest = new WalletGetPreTransNoRequest()
            {
                StoreId = externalDealCombo.ShopCode,
                TransItems = new List<WalletGetPreTransNoTransItem> { new WalletGetPreTransNoTransItem
                {
                    ItemName = externalDeal.Title,
                    Quantity = model.Quantity,
                    Amount = externalDeal.Discount,
                    Price = externalDeal.Discount * model.Quantity,
                    Point = externalDeal.SkmPayBurningPoint,
                    PicUrl = !string.IsNullOrEmpty(externalDeal.AppDealPic) ? ImageFacade.GetMediaPathsFromRawData(externalDeal.AppDealPic, MediaType.EventPromoAppMainImage)[0] : string.Empty
                }}
            };

            SkmPayOrder spOrder = new SkmPayOrder()
            {
                OrderNo = SkmFacade.GetSkmPayOrderNo(externalDealCombo.ShopCode),
                MainBid = externalDealCombo.MainBid ?? Guid.Empty,
                Bid = externalDealCombo.Bid ?? Guid.Empty,
                BurningSkmPoint = externalDeal.SkmPayBurningPoint,
                ExternalDealGuid = externalDeal.Guid,
                OrderAmount = externalDeal.Discount,
                PaymentAmount = externalDeal.Discount,
                Status = (int)SkmPayOrderStatus.Create,
                SkmTradeNo = string.Empty, //此刻尚不需產生, auth時才需要
                ShopCode = externalDealCombo.ShopCode,
                UserId = MemberFacade.GetUniqueId(HttpContext.Current.User.Identity.Name)
            };

            if (!skmEfp.SetSkmPayOrder(spOrder))
            {
                result.Code = ApiResultCode.SkmPayOrderAddError;
                return result;
            }

            WalletServerResponse<WalletGetPreTransNoResponse> walletServerResponse = SkmWalletServerUtility.GetPreTransNo(walletGetPreTransNoRequest);
            if (walletServerResponse.Code != WalletReturnCode.Success)
            {
                spOrder.Status = (int)SkmPayOrderStatus.GetPreTransFail;
                spOrder.Memo = walletServerResponse.Message;
                skmEfp.SetSkmPayOrder(spOrder);
                result.Code = ApiResultCode.SkmPayOrderAddError;
                return result;
            }

            spOrder.PreTransNo = walletServerResponse.Data.PreTransNo;
            spOrder.Status = (int)SkmPayOrderStatus.GetPreTransNo;
            if (!skmEfp.SetSkmPayOrder(spOrder))
            {
                logger.Warn("skm pay update pre_trans_no fail.");
                result.Code = ApiResultCode.SkmPayOrderAddError;
                return result;
            }

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = new { preTransNo = walletServerResponse.Data.PreTransNo }
            };
        }

        /// <summary>
        /// 取得付款時需要的檔次資料 for 3.6(含) 以前
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Skm)]
        public ApiResult GetDealDataForCheckout(DealDataForCheckoutModel model)
        {
            Guid bGuid;
            if (!Guid.TryParse(model.Bid, out bGuid))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }

            string userName = HttpContext.Current.User.Identity.Name;
            if (string.IsNullOrWhiteSpace(userName))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.UserNoSignIn
                };
            }

            string message;
            var pponDeal = PponDealApiManager.ApiPponDealCheckoutDataGet(bGuid, userName, null, false, out message, ApiUserManager.IsOldAppUserId(MemberFacade.GetUniqueId(HttpContext.Current.User.Identity.Name).ToString()));

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Message = message,
                Data = pponDeal
            };

        }

        /// <summary>
        /// 取得SkmPay支付選項
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult GetSkmPaymentOption(GetSkmPaymentOptionRequest model)
        {
            if (string.IsNullOrEmpty(model.BankNo) || model.ItemPrice <= 0)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = SkmFacade.GetSkmPaymentOption(model)
            };
        }

        /// <summary>
        /// SKM PAY MakeOrder
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Skm)]
        public ApiResult SkmPayMakeOrder(SkmPayMakeOrderRequest model)
        {
            string userName = HttpContext.Current.User.Identity.Name;

            SkmFacade.SetSkmPayOrderTransLog(0, 0, "Skm/SkmPayMakeOrder",
                true,
                new Regex("(\"VipAppPass\":\"\\w*\")").Replace(new JsonSerializer().Serialize(model), ""),//為了不紀錄VipAppPass內容
                "",
                MemberFacade.GetUniqueId(HttpContext.Current.User.Identity.Name), "write webapi request");

            if (string.IsNullOrWhiteSpace(userName))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.UserNoSignIn
                };
            }

            if (model == null || !model.IsValid)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }
            string errorMessage = string.Empty;
            if (!SkmFacade.CheckInvoiceVehicleType(model.ECInvoiceType, model.InvoiceVehicleBarcode, model.UniformNumber, model.DonateNo, out errorMessage))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError,
                    Message = errorMessage
                };
            }

            PaymentDTO paymentDto = model.DtoString.ToObject<PaymentDTO>();

            #region skmPay Check

            ExternalDealCombo externalDealCombo = skmEfp.GetExternalDealComboByBid(paymentDto.BusinessHourGuid);
            if (externalDealCombo == null || !externalDealCombo.MainBid.HasValue)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.PaymentCheckOrderFail
                };
            }

            var ved = SkmCacheFacade.GetOnlineViewExternalDeal(paymentDto.BusinessHourGuid);
            if (!ved.IsSkmPay)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.PaymentCheckOrderFail
                };
            }

            if (SkmFacade.CheckExternalDealIsHidden(ved.Bid))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.DealsNotSale
                };
            }

            var spOrder = skmEfp.GetSkmPayOrderByPreTransNo(model.PreTransNo);
            if (spOrder == null || spOrder.Id == 0 || spOrder.Status != (int)SkmPayOrderStatus.GetPreTransNo)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.PaymentCheckOrderFail
                };
            }

            #endregion

            ZeroPayResult payResult = new ZeroPayResult();
            string exceptionMessage = string.Empty;
            try
            {
                payResult = PaymentFacade.ZeroPayMakeOrder(paymentDto, userName, (ApiUserDeviceType)model.DeviceType);
            }
            catch (Exception ex)
            {
                payResult.ResultCode = ApiResultCode.Error;
                payResult.ReturnCode = ApiReturnCode.Error;
                payResult.Message = "建立訂單失敗，請重新購買。";
                exceptionMessage = ex.ToString(); //包含InnerException及StackTrace
            }

            spOrder.OrderGuid = payResult.ResultCode == ApiResultCode.Success ? payResult.Data.OrderGuid : new Guid();
            spOrder.Status = payResult.ResultCode == ApiResultCode.Success ? (int)SkmPayOrderStatus.MakeOrderFinsh : (int)SkmPayOrderStatus.MakeOrderFail;
            skmEfp.SetSkmPayOrder(spOrder);
            SkmFacade.SetSkmPayOrderTransLog(spOrder.Id, spOrder.Status, "PaymentFacade.ZeroPayMakeOrder",
                payResult.ResultCode == ApiResultCode.Success,
                new JsonSerializer().Serialize(new { paymentDto, userName, model.DeviceType }),
                new JsonSerializer().Serialize(payResult)
                , MemberFacade.GetUniqueId(HttpContext.Current.User.Identity.Name), exceptionMessage);

            //建立訂單失敗
            if (payResult.ResultCode != ApiResultCode.Success)
            {
                return new ApiResult
                {
                    Code = payResult.ResultCode,
                    Message = payResult.Message,
                    Data = payResult.Data
                };
            }

            List<SkmPayOrderDetail> skmPayOrderDetails = payResult.TrustIds.Select(item =>
            {
                return new SkmPayOrderDetail()
                {
                    SkmPayOrderId = spOrder.Id,
                    TrustId = item
                };
            }).ToList();
            skmEfp.SetSkmPayOrderDetails(skmPayOrderDetails);

            #region WalletServer

            //與Wallet交易
            var snObj = SkmFacade.GetSkmTradeNo(externalDealCombo.ShopCode, SkmFacade.SkmPayPlatFormId);
            if (snObj == null)
            {
                logger.Error(string.Format("GetSkmTradeNo.Error={0},ShopCode={1}", "snObj == null", externalDealCombo.ShopCode));
            }
            spOrder.SkmTradeNo = snObj.GetTradeNo();
            var walletSnLog = SkmFacade.AddSerialNumberLog(spOrder.Id, snObj, SkmSerialNumberLogTradeType.MakeOrderWallet);

            var authPaymentRequest = new WalletAuthPaymentRequest
            {
                PaymentToken = model.PaymentToken,
                EcOrderId = spOrder.SkmTradeNo,
                AuthVersion = model.AuthVersion
            };

            var walletServerResponse = SkmWalletServerUtility.AuthPayment(authPaymentRequest);

            //回傳結果若是非成功或是非需要走OTP則表示失敗
            if (walletServerResponse.Code != WalletReturnCode.Success &&
                walletServerResponse.Code != WalletReturnCode.NeedOTP)
            {
                spOrder.Status = (int)SkmPayOrderStatus.AuthPaymentFail;
                spOrder.Memo = walletServerResponse.Message;
                skmEfp.SetSkmPayOrder(spOrder);
                SkmFacade.SetSkmPayOrderTransLog(spOrder.Id, spOrder.Status, "SkmWalletServerUtility.AuthPayment",
                    walletServerResponse.Code == WalletReturnCode.Success,
                    new JsonSerializer().Serialize(authPaymentRequest),
                    new JsonSerializer().Serialize(walletServerResponse)
                    , MemberFacade.GetUniqueId(HttpContext.Current.User.Identity.Name));

                SkmFacade.CompleteSerialNumberLog(walletSnLog, false, Helper.GetEnumDescription(SkmPayOrderStatus.AuthPaymentFail));
                skmEfp.UpdateSkmSerialNumber(snObj.Id, false, false);
                return SkmFacade.SetOrderFail(paymentDto.BusinessHourGuid, payResult.Data.OrderGuid, ApiResultCode.SkmPayAuthPaymentError, walletServerResponse.Message);
            }

            SkmFacade.CompleteSerialNumberLog(walletSnLog, true, string.Empty);
            spOrder.PaymentToken = model.PaymentToken;
            spOrder.PaymentAmount = walletServerResponse.Data.PayAmount;
            spOrder.CardHash = walletServerResponse.Data.CardHash;
            spOrder.Status = walletServerResponse.Code == WalletReturnCode.Success ?
                (int)SkmPayOrderStatus.AuthPaymentSuccess : (int)SkmPayOrderStatus.WaitingOTPFinished;
            spOrder.RedeemPoint = walletServerResponse.Data.RedeemPt;
            spOrder.RedeemAmount = walletServerResponse.Data.RedeemAmt;
            spOrder.InstallPeriod = walletServerResponse.Data.InstallPeriod;
            spOrder.InstallDownPay = walletServerResponse.Data.InstallDownPay;
            spOrder.InstallEachPay = walletServerResponse.Data.InstallPay;
            spOrder.InstallFee = walletServerResponse.Data.InstallFee;

            skmEfp.SetSkmPayOrder(spOrder);
            SkmFacade.SetSkmPayOrderTransLog(spOrder.Id, spOrder.Status, "SkmWalletServerUtility.AuthPayment",
                walletServerResponse.Code == WalletReturnCode.Success,
                new JsonSerializer().Serialize(authPaymentRequest),
                new JsonSerializer().Serialize(walletServerResponse)
                , MemberFacade.GetUniqueId(HttpContext.Current.User.Identity.Name));

            #endregion

            #region Skm 發票API

            #region 組合發票資訊

            List<SkmPayInvoiceTenderDetail> skmPayInvoiceTenderDetails = new List<SkmPayInvoiceTenderDetail>();
            //判斷如果是信用卡消費才會加入一筆資料(基本上都會，OTP除外)
            if (!string.IsNullOrEmpty(walletServerResponse.Data.CardNumber))
            {
                skmPayInvoiceTenderDetails.Add(
                    new SkmPayInvoiceTenderDetail()
                    {
                        PayOrderNo = spOrder.SkmTradeNo,
                        NdType = ved.BrandCounterCode,
                        SellTender = walletServerResponse.Data.GatewayBankNo == config.NationalCreditCardCenterBankNo ?
                            TurnCloudSellTenderType.SkmPayNCCC : TurnCloudSellTenderType.SkmPay,
                        TenderPrice = walletServerResponse.Data.TradeAmount,
                        SecNo = walletServerResponse.Data.CardNumber,
                        TxAmount = walletServerResponse.Data.TradeAmount,
                        CardNo = walletServerResponse.Data.CardNumber,
                        UseBonus = walletServerResponse.Data.RedeemAmt > 0,
                        CardNo2 = walletServerResponse.Data.CardHash,
                        BonusUsePoint = walletServerResponse.Data.RedeemPt,
                        BonusPoint = walletServerResponse.Data.PostRedeemPt,
                        BonusDisAmt = walletServerResponse.Data.RedeemAmt,
                        Periods = walletServerResponse.Data.InstallPeriod,
                        HandAmt = walletServerResponse.Data.InstallDownPay,
                        EachAmt = walletServerResponse.Data.InstallPay
                    }
                );
            }
            //如果有燒點就需要再加一筆資料
            if (ved.SkmPayBurningPoint > 0)
            {
                skmPayInvoiceTenderDetails.Add(
                    new SkmPayInvoiceTenderDetail()
                    {
                        NdType = ved.BrandCounterCode,
                        SellTender = TurnCloudSellTenderType.MemberPointsDeduct,
                        TenderPrice = 0,
                        PointDeductTotal = ved.SkmPayBurningPoint
                    }
                );
            }

            var sellOriPrice = ved.SkmOrigPrice.HasValue && ved.SkmOrigPrice.Value > 0 ? (int)ved.SkmOrigPrice.Value : ved.Discount;

            //目前只有一個商品
            SkmPayInvoiceSellDetail skmPayInvoiceSellDetail = new SkmPayInvoiceSellDetail()
            {
                SellId = ved.ProductCode,
                StoreId = ved.SkmStoreId,
                NdType = ved.BrandCounterCode,
                SellOrderNoS = "000001",
                SellQty = 1,//先直接放1，因為目前沒有購買數量的概念
                SellPrice = ved.Discount,
                SellRate = ved.TaxRate == 0 ? (int)TurnCloudSellRateType.Free : (int)TurnCloudSellRateType.Taxable,
                SellRateAmt = ved.Discount - ved.ExcludingTaxPrice,
                SellAmt = walletServerResponse.Data.TradeAmount,
                ProductId = ved.ProductCode,
                SellOriPrice = sellOriPrice,
                GoodsName = ved.Title,
                SellDiscount = sellOriPrice - ved.Discount,  //折扣金額等於原價-折扣後的價格
                ProductDiscountType = sellOriPrice > spOrder.OrderAmount
                    ? (int)TurnCloudProductDiscountType.Amount : (int)TurnCloudProductDiscountType.None,
                PmtType = ved.SkmPayBurningPoint > 0 ? TurnCloudSellPaymentType.PointPlusCash : "",
                PointAdd = 0,
                PointDeduct = ved.SkmPayBurningPoint
            };

            SkmPayInvoice skmPayInvoice = new SkmPayInvoice()
            {
                PlatFormId = SkmFacade.SkmPayPlatFormId,
                SellNo = spOrder.SkmTradeNo.Substring(16, 6),
                SkmPayOrderId = spOrder.Id,
                SellShopid = spOrder.ShopCode,
                StoreId = ved.SkmStoreId,
                SellOrderNo = spOrder.OrderNo,
                SellPosid = SkmFacade.SkmTmCode[spOrder.ShopCode],
                SellDay = spOrder.LastModifyDate.Value,
                Tday = spOrder.LastModifyDate.Value,
                TotalAmt = skmPayInvoiceSellDetail.SellAmt,
                TotalSaleAmt = skmPayInvoiceTenderDetails.Sum(item => item.TenderPrice),
                TotalQty = 1,//先直接放1，因為目前沒有購買數量的概念
                DiscAmt = spOrder.RedeemAmount,
                RateAmt = skmPayInvoiceSellDetail.SellRateAmt,
                ExtraAmt = 0,
                SellTranstype = (int)TurnCloudSellTransType.Sell,
                VipNo = model.CardNo,
                CompId = model.UniformNumber,
                VipAppPass = model.VipAppPass,
                EuiVehicleNo = model.InvoiceVehicleBarcode,
                EuiUniversalStatus = !string.IsNullOrEmpty(model.InvoiceVehicleBarcode),
                EuiVehicleTypeNo = TurnCloudServerUtility.GetEuiVehicleTypeNo(model.ECInvoiceType),
                EuiDonateNo = model.DonateNo,
                EuiDonateStatus = !string.IsNullOrEmpty(model.DonateNo),
                PreSalesStatus = (int)TurnCloudPreDalesStatus.Sale
            };
            //發票為捐贈狀態rm_detail.eui_vehicle_no需要帶入會員載具編號(會員卡號)---from 發票商
            if (model.ECInvoiceType == SkmPayInvoiceCarrierType.Donation)
            {
                skmPayInvoice.EuiVehicleNo = model.CardNo;
            }
            #endregion

            #region 建立發票資料 in DB
            bool createInvoiceIsSuccess = false;
            string responseMessage = string.Empty;
            try
            {
                createInvoiceIsSuccess = SkmFacade.SetSkmPayInvoice(skmPayInvoice, skmPayInvoiceSellDetail, skmPayInvoiceTenderDetails);
                responseMessage = createInvoiceIsSuccess.ToString();
            }
            catch (Exception ex)
            {
                responseMessage = ex.ToString();
                logger.ErrorFormat("{0}->{1}", ex.Message, ex.StackTrace);
                if (ex.InnerException != null)
                {
                    logger.ErrorFormat("{0}->{1}", ex.InnerException.Message, ex.InnerException.StackTrace);
                }
            }
            #endregion

            //需要開立發票成功且不屬於OTP的才會直接開立發票
            if (createInvoiceIsSuccess && walletServerResponse.Code == WalletReturnCode.Success)
            {
                CreateInvoiceRequest createInvoiceRequest = null;
                CreateInvoiceResponse createInvoiceResponse = null;
                SkmPayOrderStatus skmPayOrderStatus = SkmFacade.CreateInvoice(skmPayInvoice, skmPayInvoiceSellDetail, skmPayInvoiceTenderDetails, snObj
                , out createInvoiceRequest
                , out createInvoiceResponse);

                //回存skm pay order狀態
                spOrder.Status = (int)skmPayOrderStatus;
                skmEfp.SetSkmPayOrder(spOrder);

                //add skm_pay_order_trans_log
                SkmFacade.SetSkmPayOrderTransLog(skmPayInvoice.SkmPayOrderId, (int)skmPayOrderStatus, "TurnCloudServerUtility.CreateInvoice",
                    skmPayOrderStatus == SkmPayOrderStatus.SentInvoice,
                    new JsonSerializer().Serialize(createInvoiceRequest),
                    new JsonSerializer().Serialize(createInvoiceResponse),
                    spOrder.UserId,
                    "markOrder_" + Helper.GetDescription(skmPayOrderStatus));

                if (skmPayOrderStatus == SkmPayOrderStatus.SentInvoice)
                {
                    //當所有交易皆成功 APP收件夾顯示訂單訊息
                    skmEfp.SetSkmInAppMessage(new SkmInAppMessage
                    {
                        UserId = spOrder.UserId,
                        EventType = (int)EventType.SkmPayMsg,
                        IsRead = false,
                        Subject = "【訂單通知】",
                        MsgContent = string.Format("您的訂單編號{0}已成功訂購，請記得於期限內至指定櫃位取貨!\n\r立即確認訂單>>", spOrder.OrderNo),
                        MsgType = (byte)SkmInAppMessageType.MakeOrder,
                        IsDel = false
                    });

                    //不走OTP時 & 開發票成功 則直接累加銷量 DealSalesInfo
                    PayEvents.OnOrdered(spOrder.Bid, skmPayInvoiceSellDetail.SellQty, 0, spOrder.OrderGuid);
                }
                else
                {
                    //wallet 退款
                    SkmFacade.ReturnWalletWhenBuyFail(spOrder, "SkmPayMakeOrder", out responseMessage);
                    return SkmFacade.SetOrderFail(paymentDto.BusinessHourGuid, payResult.Data.OrderGuid, ApiResultCode.CreateInvoiceFail, "購買失敗!" + responseMessage);
                }
            }
            else if (createInvoiceIsSuccess && walletServerResponse.Code == WalletReturnCode.NeedOTP)
            {
                bool s = SkmFacade.CreateSkmPayOtpWhenMakeOrder(spOrder.Id, payResult.Data, walletServerResponse.Data.OTPUrl, model.VipAppPass, SkmFacade.SkmPayPlatFormId);
            }
            else
            {
                return SkmFacade.SetOrderFail(paymentDto.BusinessHourGuid, payResult.Data.OrderGuid, ApiResultCode.CreateInvoiceFail, "購買失敗!!");
            }
            #endregion

            if (payResult.Data != null && payResult.Data.SkmAvailabilities != null)
            {
                foreach (var item in payResult.Data.SkmAvailabilities)
                {
                    item.BrandCounterName = SkmFacade.PrefixStoreName(item.BrandCounterName);
                }
            }

            var returnData = new SkmPayMakeOrderModel()
            {
                CouponId = payResult.Data.CouponId,
                CouponCode = payResult.Data.CouponCode,
                ThreeStageCode = payResult.Data.ThreeStageCode,
                PromoUrl = payResult.Data.PromoUrl,
                PromoImage = payResult.Data.PromoImage,
                ShowCoupon = payResult.Data.ShowCoupon,
                IsFami = payResult.Data.IsFami,
                IsSkm = payResult.Data.IsSkm,
                SkmQrCode = payResult.Data.SkmQrCode,
                SkmExpireTime = payResult.Data.SkmExpireTime,
                ProductCode = payResult.Data.ProductCode,
                SkmAvailabilities = payResult.Data.SkmAvailabilities,
                SpecialItemNo = payResult.Data.SpecialItemNo,
                IsCouponDownload = payResult.Data.IsCouponDownload,
                CouponType = payResult.Data.CouponType,
                IsATM = payResult.Data.IsATM,
                AtmData = payResult.Data.AtmData,
                OrderGuid = payResult.Data.OrderGuid,
                SequenceNumber = payResult.Data.SequenceNumber,
                UnUsed17PayMsg = payResult.Data.UnUsed17PayMsg,
                Used17PayMsg = payResult.Data.Used17PayMsg,
                TransDate = skmPayInvoice.SellDay.ToString("yyyy/MM/dd HH:mm"),
                TradeAmount = skmPayInvoice.TotalAmt,
                CreditCardAmount = walletServerResponse.Data.TradeAmount,
                DiscountAmount = walletServerResponse.Data.RedeemAmt, //分期強更後可以拿掉
                UsedPoint = ved.SkmPayBurningPoint,
                NeedOTP = walletServerResponse.Code == WalletReturnCode.NeedOTP,
                OTPUrl = walletServerResponse.Data.OTPUrl,
                ApiAccseeLimite = config.SkmGetPaymentResultAccseeLimite,

                //分期/紅利
                RedeemAmt = walletServerResponse.Data.RedeemAmt,
                RedeemPoint = walletServerResponse.Data.RedeemPt,
                PostRedeemPoint = walletServerResponse.Data.PostRedeemPt,
                InstallPeriod = walletServerResponse.Data.InstallPeriod,
                InstallDownPay = walletServerResponse.Data.InstallDownPay,
                InstallEachPay = walletServerResponse.Data.InstallPay,
                InstallFee = walletServerResponse.Data.InstallFee
            };

            return new ApiResult
            {
                Code = payResult.ResultCode,
                Data = returnData
            };
        }

        /// <summary>
        /// 結帳完成開立發票
        /// </summary>
        /// <param name="request">結帳完成開立發票請求資訊</param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Skm)]
        public ApiResult CompleteOTPOrder(CompleteOTPOrderRequest request)
        {
            ApiResult returnValue = SkmFacade.CompleteOTPOrder(request.PaymentToken, true);
            return returnValue;
        }


        #region  停車支付

        /// <summary>
        /// 取得Skm Parking 預備交易號碼
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Skm)]
        public ApiResult GetSkmParkingPreTransNo(GetSkmParkingPreTransNoRequest model)
        {
            try
            {
                if (ModelState.IsValid == false)
                {
                    return new ApiResult
                    {
                        Code = ApiResultCode.InputError
                    };
                }

                ApiResult result = new ApiResult();
                var userId = MemberFacade.GetUniqueId(HttpContext.Current.User.Identity.Name);
                SkmPayParkingOrder spkOrder = new SkmPayParkingOrder()
                {
                    OrderNo = SkmFacade.GetSkmPayParkingOrderNo(model.ShopCode), //要與精選優惠不同
                    SkmTradeNo = string.Empty, //24碼此刻尚不需產生, auth時才需要
                    ShopCode = model.ShopCode,
                    OrderAmount = model.OrderAmount,
                    MemberCardNo = model.CardNo,
                    DeviceType = model.DeviceType,
                    //2.14.6 API 欄位所需
                    //ParkingTansNo = 自訂6碼1100交易序號(如發票欄位 SkmTradeNo取六碼)= 發票rm_detail.sell_no交易序號 
                    //ParkingToken 車牌號碼(若非車辨進場,則填入會員卡號)
                    ParkingToken = string.IsNullOrEmpty(model.ParkingToken) ? model.CardNo : model.ParkingToken,
                    ParkingAreaNo = model.ParkingAreaNo,//停車場編號
                    ParkingLicensePictureUuid = model.ParkingLicensePictureUUID,//車牌照片UUID
                                                                                //停車票號(以會員條碼進場填入會員卡號，以車牌辨識進場，傳入停車票號車牌照片UUID)
                    DiscountAmount = model.DiscountAmount,
                    DiscountMinutes = model.DiscountMinutes,
                    IsDiscountPoint = model.IsDiscountByPoint,

                    PosId = SkmFacade.SkmParkingTmCode[model.ShopCode],
                    ParkingTicketNo = model.ParkingTicketNo,
                    Status = (int)SkmPayOrderStatus.Create,
                    UserId = userId,
                    CreateDate = DateTime.Now
                };
                if (!skmEfp.SetSkmPayParkingOrder(spkOrder))
                {
                    result.Code = ApiResultCode.SkmPayOrderAddError;
                }

                //add skm_parking_order_trans_log
                SkmFacade.SetSkmPayParkingOrderTransLog(spkOrder.Id, (int)SkmPayOrderStatus.Create, "Api/Skm/GetSkmParkingPreTransNo",
                    result.Code != ApiResultCode.SkmPayOrderAddError,
                    new JsonSerializer().Serialize(model),
                    "",
                    userId,
                    result.Code == ApiResultCode.SkmPayOrderAddError
                        ? "SKM PAY 建立訂單異常"
                        : SkmFacade.ParkingOrderStatusDesc[SkmPayOrderStatus.Create]);

                if (result.Code == ApiResultCode.SkmPayOrderAddError)
                {
                    return result;
                }

                WalletGetPreTransNoRequest walletGetPreTransNoRequest = new WalletGetPreTransNoRequest()
                {
                    StoreId = model.ShopCode,
                    TransItems = new List<WalletGetPreTransNoTransItem> { new WalletGetPreTransNoTransItem
                {
                    ItemName = SkmFacade.SkmPayParkingGoodsName,
                    Quantity = 1,
                    Amount = model.OrderAmount,
                    Price = model.OrderAmount,
                    Point = 0,
                    PicUrl = string.Empty
                }}
                };
                WalletServerResponse<WalletGetPreTransNoResponse> walletServerResponse = SkmWalletServerUtility.GetPreTransNo(walletGetPreTransNoRequest);
                if (walletServerResponse.Code != WalletReturnCode.Success)
                {
                    spkOrder.Status = (int)SkmPayOrderStatus.GetPreTransFail;
                    spkOrder.Memo = SkmFacade.ParkingOrderStatusDesc[SkmPayOrderStatus.GetPreTransFail];
                }
                else
                {
                    spkOrder.WalletPreTransNo = walletServerResponse.Data.PreTransNo;
                    spkOrder.Status = (int)SkmPayOrderStatus.GetPreTransNo;
                    spkOrder.Memo = SkmFacade.ParkingOrderStatusDesc[SkmPayOrderStatus.GetPreTransNo];
                }


                //add skm_parking_order_trans_log
                SkmFacade.SetSkmPayParkingOrderTransLog(spkOrder.Id, spkOrder.Status, "WalletServerUtility.GetPreTransNo",
                    walletServerResponse.Code == WalletReturnCode.Success,
                    new JsonSerializer().Serialize(walletGetPreTransNoRequest),
                    new JsonSerializer().Serialize(walletServerResponse),
                    userId,
                    walletServerResponse.Code == WalletReturnCode.Success
                        ? SkmFacade.ParkingOrderStatusDesc[SkmPayOrderStatus.GetPreTransNo]
                        : SkmFacade.ParkingOrderStatusDesc[SkmPayOrderStatus.GetPreTransFail]);


                if (!skmEfp.SetSkmPayParkingOrder(spkOrder) || walletServerResponse.Code != WalletReturnCode.Success)
                {
                    result.Code = ApiResultCode.SkmPayOrderAddError;
                }
                else
                {
                    result.Code = ApiResultCode.Success;
                    result.Data = new { preTransNo = walletServerResponse.Data.PreTransNo };
                }

                SkmFacade.UpdateResponseTransLog(spkOrder.Id, spkOrder.Status, "Api/Skm/GetSkmParkingPreTransNo",
                    result.Code == ApiResultCode.Success,
                    new JsonSerializer().Serialize(result),
                    userId,
                    SkmFacade.ParkingOrderStatusDesc[SkmPayOrderStatus.GetPreTransNo]);

                return result;
            }
            catch (Exception ex)
            {
                logger.Info(string.Format("GetSkmParkingPreTransNos.Para={0},Exception={1}", new JsonSerializer().Serialize(model), ex.ToString()));
                return new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = ex.ToString(),
                    Data = new { preTransNo = "" }
                };
            }
        }

        /// <summary>
        /// SkmParkingMakeOrder
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Skm)]
        public ApiResult SkmParkingMakeOrder(SkmPayParkingMakeOrderRequest model)
        {
            try
            {
                #region 檢查
                string userName = HttpContext.Current.User.Identity.Name;
                int userId = MemberFacade.GetUniqueId(userName);

                if (string.IsNullOrWhiteSpace(userName) || model == null || !model.IsValid)
                {
                    return new ApiResult()
                    {
                        Code = ApiResultCode.UserNoSignIn
                    };
                }

                //驗證發票載具或自然人憑證
                string errorMessage = string.Empty;
                if (!SkmFacade.CheckInvoiceVehicleType(model.ECInvoiceType, model.InvoiceVehicleBarcode, model.UniformNumber, model.DonateNo, out errorMessage))
                {
                    return new ApiResult()
                    {
                        Code = ApiResultCode.InputError,
                        Message = errorMessage
                    };
                }

                //驗證是否有先取得PreTransNo
                var spkOrder = skmEfp.GetSkmParkingOrderByPreTransNo(model.PreTransNo);
                if (spkOrder == null || spkOrder.Id == 0 || spkOrder.Status != (int)SkmPayOrderStatus.GetPreTransNo)
                {
                    return new ApiResult()
                    {
                        Code = ApiResultCode.PaymentCheckOrderFail
                    };
                }
                #endregion

                //add skm_parking_order_trans_log
                SkmFacade.SetSkmPayParkingOrderTransLog(spkOrder.Id, spkOrder.Status, "Api/Skm/SkmPayParkingMakeOrder",
                    true,
                    new Regex("(\"VipAppPass\":\"\\w*\")").Replace(new JsonSerializer().Serialize(model), ""),//為了不紀錄VipAppPass內容
                    "",
                    userId,
                    "");

                #region WalletServer

                //與Wallet交易
                var snObj = SkmFacade.GetSkmTradeNo(spkOrder.ShopCode, SkmFacade.SkmPayParkingPlatFormId);
                spkOrder.SkmTradeNo = snObj.GetTradeNo();//24碼 照用同一個pool
                                                         //2.14.6 1100 交易序號
                                                         //發票rm_detail.sell_no交易序號
                                                         //24碼序號 第16碼起取6碼
                spkOrder.ParkingTansNo = spkOrder.SkmTradeNo.Substring(16, 6);

                var walletSnLog = SkmFacade.AddSerialNumberLog(spkOrder.Id, snObj, SkmSerialNumberLogTradeType.MakeOrderWallet);
                var authPaymentRequest = new WalletAuthPaymentRequest
                {
                    PaymentToken = model.PaymentToken,
                    EcOrderId = spkOrder.SkmTradeNo,
                    AuthVersion = model.AuthVersion
                };

                var walletServerResponse = SkmWalletServerUtility.AuthPayment(authPaymentRequest);
                //回傳結果若是非成功或是非需要走OTP則表示失敗
                if (walletServerResponse.Code != WalletReturnCode.Success &&
                    walletServerResponse.Code != WalletReturnCode.NeedOTP)
                {
                    spkOrder.Status = (int)SkmPayOrderStatus.AuthPaymentFail;
                    spkOrder.Memo = SkmFacade.ParkingOrderStatusDesc[SkmPayOrderStatus.AuthPaymentFail];
                    skmEfp.SetSkmPayParkingOrder(spkOrder);

                    //auth fail log
                    SkmFacade.SetSkmPayParkingOrderTransLog(
                        spkOrder.Id,
                        spkOrder.Status,
                        "WalletServerUtility.AuthPayment",
                        walletServerResponse.Code == WalletReturnCode.Success,
                        new JsonSerializer().Serialize(authPaymentRequest),
                        new JsonSerializer().Serialize(walletServerResponse)
                        , userId
                        , SkmFacade.ParkingOrderStatusDesc[SkmPayOrderStatus.AuthPaymentFail]);

                    SkmFacade.CompleteSerialNumberLog(walletSnLog, false, LunchKingSite.Core.Helper.GetEnumDescription(SkmPayOrderStatus.AuthPaymentFail));
                    skmEfp.UpdateSkmSerialNumber(snObj.Id, false, false);

                    return new ApiResult
                    {
                        Code = ApiResultCode.SkmPayAuthPaymentError,
                        Message = walletServerResponse.Message
                    };
                }

                SkmFacade.CompleteSerialNumberLog(walletSnLog, true, string.Empty);

                spkOrder.WalletPaymentToken = model.PaymentToken;
                spkOrder.WalletPaymentAmount = walletServerResponse.Data.PayAmount;
                spkOrder.WalletCardHash = walletServerResponse.Data.CardHash;
                spkOrder.Status =
                                walletServerResponse.Code == WalletReturnCode.Success
                                ? (int)SkmPayOrderStatus.AuthPaymentSuccess
                                : (int)SkmPayOrderStatus.WaitingOTPFinished;
                spkOrder.Memo =
                                walletServerResponse.Code == WalletReturnCode.Success
                                ? SkmFacade.ParkingOrderStatusDesc[SkmPayOrderStatus.AuthPaymentSuccess]
                                : SkmFacade.ParkingOrderStatusDesc[SkmPayOrderStatus.WaitingOTPFinished];
                skmEfp.SetSkmPayParkingOrder(spkOrder);

                //auth success log
                SkmFacade.SetSkmPayParkingOrderTransLog(spkOrder.Id, spkOrder.Status, "WalletServerUtility.AuthPayment",
                    walletServerResponse.Code == WalletReturnCode.Success,
                    new JsonSerializer().Serialize(authPaymentRequest),
                    new JsonSerializer().Serialize(walletServerResponse)
                    , userId
                    , walletServerResponse.Code == WalletReturnCode.Success
                        ? SkmFacade.ParkingOrderStatusDesc[SkmPayOrderStatus.AuthPaymentSuccess]
                        : SkmFacade.ParkingOrderStatusDesc[SkmPayOrderStatus.WaitingOTPFinished]);

                #endregion

                #region Skm 發票API

                #region 組合發票資訊
                //rm_detail(SingleObject)        交易資訊(SkmPayInvoice)
                //rm_detail.sell_detail(Array)   銷售明細(SkmPayInvoiceSellDetail)
                //rm_detail.tender_detail(Array) 付款方式(SkmPayInvoiceTenderDetail)
                #region rm_detail.sell_detail(Array) 銷售明細

                //目前只有一個商品
                SkmPayInvoiceSellDetail skmPayInvoiceSellDetail = new SkmPayInvoiceSellDetail()
                {
                    SellId = SkmFacade.SkmPayParkingProductCode,
                    StoreId = SkmFacade.SkmPayParking6XBrandCounterCode,
                    NdType = SkmFacade.SkmPayParking7XBrandCounterCode,
                    SellOrderNoS = "000001",
                    SellQty = 1,//先直接放1，因為目前沒有購買數量的概念
                    SellPrice = spkOrder.OrderAmount,//售價(含稅)折扣後單價(必填) ---- 停車折扣是否反應在發票上? 是
                                                     //應稅
                    SellRate = (int)TurnCloudSellRateType.Taxable,
                    //稅額
                    SellRateAmt = spkOrder.OrderAmount - SkmFacade.GetSkmPayExcludingTaxPrice(spkOrder.OrderAmount, (SkmFacade.SkmPayParkingTaxRate / 100)),
                    //ved.Discount(優惠價ex:1710) - ved.ExcludingTaxPrice(未稅金額1629),
                    SellAmt = walletServerResponse.Data.TradeAmount, //小計
                    ProductId = SkmFacade.SkmPayParkingProductCode,
                    SellOriPrice = spkOrder.OrderAmount,//商品原價 沒有原價OrigPrice 就填優惠價 OrderAmount
                    GoodsName = SkmFacade.SkmPayParkingGoodsName,//商品名稱
                    SellDiscount = 0, //停車 SellOriPrice = SellPrice
                    ProductDiscountType = (int)TurnCloudProductDiscountType.None,
                    PmtType = "", //ved.SkmPayBurningPoint > 0 ? TurnCloudSellPaymentType.PointPlusCash : "", 有點數折扣則填入20 沒有就空白
                    PointAdd = 0,
                    PointDeduct = 0 //扣點點數ved.SkmPayBurningPoint
                };

                #endregion

                #region rm_detail.tender_detail(Array)付款方式

                List<SkmPayInvoiceTenderDetail> skmPayInvoiceTenderDetails = new List<SkmPayInvoiceTenderDetail>();
                //判斷如果是信用卡消費才會加入一筆資料(基本上都會，OTP除外)
                if (!string.IsNullOrEmpty(walletServerResponse.Data.CardNumber))
                {
                    skmPayInvoiceTenderDetails.Add(
                        new SkmPayInvoiceTenderDetail()
                        {
                            PayOrderNo = spkOrder.SkmTradeNo,
                            NdType = SkmFacade.SkmPayParking7XBrandCounterCode, //ved.BrandCounterCode 
                            SellTender = TurnCloudSellTenderType.SkmPay,
                            TenderPrice = walletServerResponse.Data.TradeAmount,
                            SecNo = walletServerResponse.Data.CardNumber,
                            TxAmount = walletServerResponse.Data.TradeAmount,
                            CardNo = walletServerResponse.Data.CardNumber,
                            UseBonus = walletServerResponse.Data.RedeemAmt > 0,
                            CardNo2 = walletServerResponse.Data.CardHash,
                            BonusUsePoint = walletServerResponse.Data.RedeemPt,
                            BonusPoint = walletServerResponse.Data.Point,
                            BonusDisAmt = walletServerResponse.Data.RedeemAmt,
                        }
                    );
                }

                #endregion

                #region rm_detail(SingleObject) 交易資訊 <有總計所以要寫最後>

                SkmPayInvoice skmPayInvoice = new SkmPayInvoice()
                {
                    PlatFormId = SkmFacade.SkmPayParkingPlatFormId,
                    ParkingToken = spkOrder.ParkingToken,//填入車牌號碼
                    PreSalesStatus = 0,//停車為一般銷售，所以此欄位不需填，僅需填入 sell_transtype = sell。
                    SellCasher = SkmFacade.SkmPayParkingSellCasher,
                    SellTranstype = (int)TurnCloudSellTransType.Sell,
                    //SellNo同EC 交易取同一個店別的取號機 (24碼序號 第16碼起取6碼)à同精選優惠方式。
                    SellNo = spkOrder.SkmTradeNo.Substring(16, 6),
                    SellOrderNo = spkOrder.OrderNo,//請自行編制，但不可與精選優惠相同，如相同會造成後端系統異常。
                    SkmPayOrderId = spkOrder.Id,
                    SellShopid = spkOrder.ShopCode,
                    StoreId = SkmFacade.SkmPayParking6XBrandCounterCode,
                    SellPosid = SkmFacade.SkmParkingTmCode[spkOrder.ShopCode],
                    SellDay = spkOrder.LastModifyDate.Value,
                    Tday = spkOrder.LastModifyDate.Value,
                    TotalAmt = skmPayInvoiceSellDetail.SellAmt,
                    TotalSaleAmt = skmPayInvoiceTenderDetails.Sum(item => item.TenderPrice),
                    TotalQty = 1, //先直接放1，因為目前沒有購買數量的概念
                                  //DiscAmt = spkOrder.RedeemAmount,
                    RateAmt = skmPayInvoiceSellDetail.SellRateAmt,
                    ExtraAmt = 0,
                    VipNo = model.CardNo,
                    CompId = model.UniformNumber,
                    VipAppPass = model.VipAppPass,
                    EuiVehicleNo = model.InvoiceVehicleBarcode,
                    EuiUniversalStatus = !string.IsNullOrEmpty(model.InvoiceVehicleBarcode),
                    EuiVehicleTypeNo = TurnCloudServerUtility.GetEuiVehicleTypeNo(model.ECInvoiceType),
                    EuiDonateNo = model.DonateNo,
                    EuiDonateStatus = !string.IsNullOrEmpty(model.DonateNo),
                };

                //發票為捐贈狀態rm_detail.eui_vehicle_no需要帶入會員載具編號(會員卡號)---from 發票商
                if (model.ECInvoiceType == SkmPayInvoiceCarrierType.Donation)
                {
                    skmPayInvoice.EuiVehicleNo = model.CardNo;
                }

                #endregion

                #endregion

                #region 建立發票資料 in DB
                bool createInvoiceIsSuccess = false;
                string responseMessage = string.Empty;
                try
                {
                    createInvoiceIsSuccess = SkmFacade.SetSkmPayInvoice(skmPayInvoice, skmPayInvoiceSellDetail, skmPayInvoiceTenderDetails);
                    responseMessage = createInvoiceIsSuccess.ToString();
                }
                catch (Exception ex)
                {
                    responseMessage = ex.ToString();
                    logger.ErrorFormat("{0}->{1}", ex.Message, ex.StackTrace);
                    if (ex.InnerException != null)
                    {
                        logger.ErrorFormat("{0}->{1}", ex.InnerException.Message, ex.InnerException.StackTrace);
                    }
                }
                #endregion

                //不屬於OTP的才會直接開立發票 && 需要建立發票資料成功 
                if (createInvoiceIsSuccess && walletServerResponse.Code == WalletReturnCode.Success)
                {
                    CreateInvoiceRequest createInvoiceRequest = null;
                    CreateInvoiceResponse createInvoiceResponse = null;

                    //呼叫發票API
                    SkmPayOrderStatus skmPayOrderStatus = SkmFacade.CreateInvoice(skmPayInvoice, skmPayInvoiceSellDetail, skmPayInvoiceTenderDetails, snObj
                        , out createInvoiceRequest
                        , out createInvoiceResponse);

                    //回存skm pay order狀態
                    spkOrder.Status = (int)skmPayOrderStatus;
                    spkOrder.Memo = SkmFacade.ParkingOrderStatusDesc[skmPayOrderStatus];
                    skmEfp.SetSkmPayParkingOrder(spkOrder);

                    //add skm pay parking order trans log
                    SkmFacade.SetSkmPayParkingOrderTransLog(skmPayInvoice.SkmPayOrderId, (int)skmPayOrderStatus, "TurnCloudServerUtility.CreateInvoice",
                        skmPayOrderStatus == SkmPayOrderStatus.SentInvoice,
                        new JsonSerializer().Serialize(createInvoiceRequest),
                        new JsonSerializer().Serialize(createInvoiceResponse),
                        spkOrder.UserId,
                        SkmFacade.ParkingOrderStatusDesc[skmPayOrderStatus]);

                    if (skmPayOrderStatus == SkmPayOrderStatus.SentInvoice)
                    {
                        //停車不用顯示APP收件夾訊息
                    }
                    else // 開發票失敗就馬上向wallet退款
                    {
                        SkmFacade.ReturnWalletWhenBuyFailWithParking(spkOrder, WalletRefundFromType.FromSkmParkingMakeOrderApi, out responseMessage);
                        return new ApiResult
                        {
                            Code = ApiResultCode.CreateInvoiceFail,
                            Data = SkmFacade.ParkingOrderStatusDesc[SkmPayOrderStatus.CreateInvoiceFail]
                        };
                    }
                }
                else if (createInvoiceIsSuccess && walletServerResponse.Code == WalletReturnCode.NeedOTP)
                {
                    bool success = SkmFacade.CreateSkmPayOtpWhenMakeOrder(spkOrder.Id, new ApiFreeDealPayReply(), walletServerResponse.Data.OTPUrl, model.VipAppPass, SkmFacade.SkmPayParkingPlatFormId);

                    //add skm pay parking order trans log
                    SkmFacade.SetSkmPayParkingOrderTransLog(skmPayInvoice.SkmPayOrderId, (int)spkOrder.Status, "SkmFacade.CreateSkmPayOtpWhenMakeOrder",
                        success,
                        new JsonSerializer().Serialize(walletServerResponse.Data.OTPUrl),
                        new JsonSerializer().Serialize(model.VipAppPass),
                        spkOrder.UserId,
                        "資料庫成功建立skm_pay_otp資料");
                }
                else
                {
                    return new ApiResult
                    {
                        Code = ApiResultCode.CreateInvoiceFail,
                        Data = SkmFacade.ParkingOrderStatusDesc[SkmPayOrderStatus.CreateInvoiceFail]
                    };
                }
                #endregion

                var returnData = new SkmPayParkingMakeOrderModel()
                {
                    TransDate = skmPayInvoice.SellDay.ToString("yyyy/MM/dd HH:mm"),
                    TradeAmount = skmPayInvoice.TotalAmt,
                    CreditCardAmount = walletServerResponse.Data.TradeAmount,
                    DiscountAmount = walletServerResponse.Data.RedeemAmt,
                    NeedOTP = walletServerResponse.Code == WalletReturnCode.NeedOTP,
                    OTPUrl = walletServerResponse.Data.OTPUrl,
                    ApiAccseeLimite = config.SkmGetPaymentResultAccseeLimite
                };

                var result = new ApiResult
                {
                    Code = ApiResultCode.Success,
                    Data = returnData
                };

                SkmFacade.UpdateResponseTransLog(spkOrder.Id, spkOrder.Status,
                    "Api/Skm/SkmPayParkingMakeOrder", true, new JsonSerializer().Serialize(result), userId, "Success");

                return result;
            }
            catch (Exception ex)
            {
                logger.Info(string.Format("SkmPayParkingMakeOrder.Para={0},Exception={1}", new JsonSerializer().Serialize(model), ex.ToString()));
                return new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = ex.ToString(),
                    Data = new SkmPayParkingMakeOrderModel() { ApiAccseeLimite = config.SkmGetPaymentResultAccseeLimite }
                };
            }
        }

        /// <summary>
        /// 結帳完成開立發票(停車)
        /// </summary>
        /// <param name="request">結帳完成開立發票請求資訊</param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Skm)]
        public ApiResult CompleteParkingOTPOrder(CompleteOTPOrderRequest request)
        {
            if (request == null || string.IsNullOrWhiteSpace(request.PaymentToken))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }

            ApiResult returnValue = SkmFacade.CompleteParkingOTPOrder(request.PaymentToken, true);
            return returnValue;
        }


        #endregion

        #region 停車支付退貨

        /// <summary>
        /// 查詢停車支付
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Skm)]
        public ApiResult GetParkingPaymentInfo(GetParkingPaymentInfoRequest request)
        {
            if (request == null || string.IsNullOrWhiteSpace(request.ShopCode) || string.IsNullOrWhiteSpace(request.MemberCardNo))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }

            DateTime sd = DateTime.Now;
            if (!string.IsNullOrEmpty(request.StartTime) && !DateTime.TryParse(request.StartTime, out sd))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }

            DateTime ed = DateTime.Now;
            if (!string.IsNullOrEmpty(request.EndTime) && !DateTime.TryParse(request.EndTime, out ed))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }

            var list = SkmFacade.GetParkingPaymentInfo(request);

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = list,
                Message = "查詢成功"
            };
        }

        /// <summary>
        /// 停車退貨
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Skm)]
        public ApiResult RefundParkingPayment(RefundParkingPaymentRequest request)
        {
            if (request == null || string.IsNullOrWhiteSpace(request.OrderNo) || string.IsNullOrWhiteSpace(request.ParkingTicketNo))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }

            return SkmFacade.RefundSkmPayParkingOrder(request);
        }

        #endregion

        /// <summary>
        /// 新光零元檔建立訂單
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Skm)]
        public ApiResult ZeroPayMakeOrder(ZeroPayMakeOrderInputModel model)
        {
            var result = new ApiResult();
            string userName = HttpContext.Current.User.Identity.Name;
            var userId = (int)HttpContext.Current.User.Identity.GetPropertyValue("Id");

            #region skmPay Check

            var skmLog = new SkmLog
            {
                SkmToken = userId.ToString(),
                UserId = userId,
                LogType = (int)SkmLogType.ZeroPayMakeOrder,
                InputValue = new JsonSerializer().Serialize(model)
            };

            if (string.IsNullOrWhiteSpace(userName))
            {
                result.Code = ApiResultCode.UserNoSignIn;
                skmLog.OutputValue = new JsonSerializer().Serialize(result);
                skmMp.SkmLogSet(skmLog);
                return result;
            }

            PaymentDTO paymentDto = model.DtoString.ToObject<PaymentDTO>();

            var ved = SkmCacheFacade.GetOnlineViewExternalDeal(paymentDto.BusinessHourGuid);
            if (ved.IsSkmPay)
            {
                result.Code = ApiResultCode.PaymentCheckOrderFail;
                skmLog.OutputValue = new JsonSerializer().Serialize(result);
                skmMp.SkmLogSet(skmLog);
                return result;
            }

            #endregion

            var payResult = PaymentFacade.ZeroPayMakeOrder(paymentDto, userName, (ApiUserDeviceType)model.DeviceType);

            #region 燒點

            if (payResult.ResultCode == ApiResultCode.Success && config.EnableCallBurningWebService)
            {
                try
                {
                    SKMCenterRocketMQTrans.AlibabaMQTrans(payResult.Data.OrderGuid, SKMCenterRocketMQTrans.ordeSatus.occupy);
                }
                catch (Exception e) {
                    logger.Error("call AlibabaMQTrans has Error =>" + e.Message);
                }
             

                var be = SkmFacade.GetBurningEventByBid(paymentDto.BusinessHourGuid);
                if (be.IsLoaded && be.BurningPoint > 0)
                {
                    //沒有會員卡號
                    if (string.IsNullOrEmpty(model.CardNo))
                    {
                        result = SkmFacade.SetOrderFail(paymentDto.BusinessHourGuid, payResult.Data.OrderGuid, ApiResultCode.SkmBurningFailCardNoIsNull, string.Empty);
                        skmLog.OutputValue = new JsonSerializer().Serialize(result);
                        skmMp.SkmLogSet(skmLog);
                        return result;
                    }

                    //var userId = (int)HttpContext.Current.User.Identity.GetPropertyValue("Id");
                    var orderLog = SkmFacade.GetSkmBurningOrderLog(be.Id, userId
                        , paymentDto.BusinessHourGuid, payResult.Data.OrderGuid, payResult.Data.CouponId);
                    if (!orderLog.IsLoaded)
                    {
                        result = SkmFacade.SetOrderFail(paymentDto.BusinessHourGuid, payResult.Data.OrderGuid, ApiResultCode.SkmBurningFailOrderLogError, string.Empty);
                        skmLog.OutputValue = new JsonSerializer().Serialize(result);
                        skmMp.SkmLogSet(skmLog);
                        return result;
                    }

                    //進行燒點
                    string message;
                    var vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(paymentDto.BusinessHourGuid);
                    if (!SkmFacade.OrderSkmBurningCoupon(be, orderLog, model.CardNo, (ApiUserDeviceType)model.DeviceType, model.DeviceCode, vpd.SellerGuid, out message))
                    {
                        //燒點失敗
                        result = SkmFacade.SetOrderFail(paymentDto.BusinessHourGuid, payResult.Data.OrderGuid, ApiResultCode.SkmBurningFail, message);
                        skmLog.OutputValue = new JsonSerializer().Serialize(result);
                        skmMp.SkmLogSet(skmLog);
                        return result;
                    }
                    else
                    {
                        if (config.IsEnableCashAndGiftCouponOnStore)
                        {
                            // 為電子贈品/電子抵用金 => 燒點成功就直接核銷
                            if (ved.ActiveType == (int)ActiveType.CashCoupon || ved.ActiveType == (int)ActiveType.GiftCertificate)
                            {
                                logger.Info("為電子贈品/電子抵用金 => 燒點成功就直接核銷 ved.ActiveType=" + ved.ActiveType);

                                #region 購買就直接核銷

                                SkmVerifyLog log = new SkmVerifyLog()
                                {
                                    TrustId = payResult.TrustIds.FirstOrDefault(),
                                    SkmId = userId,
                                    CreateTime = DateTime.Now,
                                    IsForce = false,
                                    VerifyAction = "verify",
                                    SkmTransId = "ZeroPayMakeOrderAndCashOrGiftCoupon",
                                    ShopeCode = "",
                                    BrandCounterCode = "",
                                    InputModel = new JsonSerializer().Serialize(new
                                    {
                                        model.CardNo,
                                        model.DeviceCode,
                                        model.ActivityToken,
                                        model.DeviceType,
                                        paymentDto.DeliveryInfo.StoreGuid,
                                        paymentDto.OrderGuid,
                                        paymentDto.BusinessHourGuid
                                    })
                                };

                                try
                                {
                                    VerificationStatus status = OrderFacade.VerifyCouponForSkm(payResult.TrustIds.FirstOrDefault(), "CashAndGiftCoupon", paymentDto.DeliveryInfo.StoreGuid);
                                    if (status == VerificationStatus.CouponOk)
                                    {
                                        log.SyncStatus = 1;
                                        log.SyncMsg = "OK";
                                        try
                                        {
                                            SKMCenterRocketMQTrans.AlibabaMQTrans(paymentDto.OrderGuid, SKMCenterRocketMQTrans.ordeSatus.confirm);
                                        }
                                        catch (Exception e)
                                        {
                                            logger.Error("call AlibabaMQTrans has Error =>" + e.Message);
                                        }
                                     

                                    }
                                    else
                                    {
                                        log.SyncStatus = 0;
                                        log.SyncMsg = "Fail:" + status.ToString();
                                    }
                                }
                                catch (Exception ex)
                                {
                                    log.SyncStatus = 0;
                                    log.SyncMsg = "Error:" + ex.Message;
                                }

                                SkmFacade.VerifyLogSet(log);

                                #endregion
                            }
                        }

                    }
                }
            }

            #endregion

            if (payResult.Data != null && payResult.Data.SkmAvailabilities != null)
            {
                foreach (var item in payResult.Data.SkmAvailabilities)
                {
                    item.BrandCounterName = SkmFacade.PrefixStoreName(item.BrandCounterName);
                }
                //累加銷量 DealSalesInfo
                PayEvents.OnOrdered(paymentDto.BusinessHourGuid, 1, 0, payResult.Data.OrderGuid);
            }

            result.Code = payResult.ResultCode;
            result.Data = payResult.Data;

            skmLog.OutputValue = new JsonSerializer().Serialize(result);
            skmMp.SkmLogSet(skmLog);
            return result;
        }

        /// <summary>
        /// 新光參加活動 makeorder
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Skm)]
        public ApiResult MakeActivity(ZeroPayMakeOrderInputModel model)
        {
            string userName = HttpContext.Current.User.Identity.Name;

            if (string.IsNullOrWhiteSpace(userName))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.UserNoSignIn
                };
            }

            var userId = (int)HttpContext.Current.User.Identity.GetPropertyValue("Id");

            if (model == null)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }

            if (string.IsNullOrEmpty(model.ActivityToken))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.OAuthTokerNotFound
                };
            }

            var activity = skmEfp.GetSkmActivityWithJoinDate(model.ActivityToken);
            if (activity.Id == 0)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.OAuthTokerNoAuth
                };
            }

            PaymentDTO paymentDto = model.DtoString.ToObject<PaymentDTO>();
            var actItem = skmEfp.GetSkmActivityItem(activity.Id, paymentDto.BusinessHourGuid);
            if (actItem.Id == 0)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.DataNotFound
                };
            }

            SkmActivityJoinResult resultData = new SkmActivityJoinResult();
            resultData.ActRecord.Range = skmEfp.GetSkmActivityDateRangeNow(activity.Id);
            resultData.ActRecord.ShowRangeIds = skmEfp.GetSkmActivityDateShowRangeNow(activity.Id);
            var beforeLogs = skmEfp.GetViewSkmActivityLog(activity.Id, userId, resultData.ActRecord.Range.DateRangeStart, resultData.ActRecord.Range.DateRangeEnd);
            resultData.ActRecord.Qty = beforeLogs.Sum(x => x.ChargesVal);

            var psList = pp.PponStoreGetListByBusinessHourGuid(paymentDto.BusinessHourGuid).ToList();
            var ps = psList.OrderBy(x => x.SortOrder).First();
            paymentDto.DeliveryInfo.StoreGuid = ps.StoreGuid;
            paymentDto.SelectedStoreGuid = ps.StoreGuid;

            var payResult = PaymentFacade.ZeroPayMakeOrder(paymentDto, userName,
            (ApiUserDeviceType)model.DeviceType);

            #region 燒點
            logger.Info("payResult.Message" + payResult.Message + "payResult.ResultCode" + payResult.ResultCode);
            if (payResult.ResultCode == ApiResultCode.Success)
            {
                var be = SkmFacade.GetBurningEventByBid(paymentDto.BusinessHourGuid);
                if (be.IsLoaded && be.BurningPoint > 0)
                {
                    if (string.IsNullOrEmpty(model.CardNo))
                    {
                        return SkmFacade.SetOrderFail(paymentDto.BusinessHourGuid, payResult.Data.OrderGuid, ApiResultCode.SkmBurningFailCardNoIsNull,
                            string.Empty, resultData);
                    }

                    var orderLog = SkmFacade.GetSkmBurningOrderLog(be.Id, userId
                        , paymentDto.BusinessHourGuid, payResult.Data.OrderGuid, payResult.Data.CouponId);
                    if (!orderLog.IsLoaded)
                    {
                        return SkmFacade.SetOrderFail(paymentDto.BusinessHourGuid, payResult.Data.OrderGuid, ApiResultCode.SkmBurningFailOrderLogError,
                            string.Empty, resultData);
                    }

                    string message;
                    if (!SkmFacade.OrderSkmBurningCoupon(be, orderLog, model.CardNo,
                        (ApiUserDeviceType)model.DeviceType, model.DeviceCode, model.SellerGuid, out message))
                    {
                        return SkmFacade.SetOrderFail(paymentDto.BusinessHourGuid, payResult.Data.OrderGuid, ApiResultCode.SkmBurningFail, message, resultData);
                    }

                    var activityLog = new SkmActivityLog
                    {
                        ActivityId = actItem.ActivityId,
                        ItemId = actItem.Id,
                        UserId = userId,
                        CreateTime = DateTime.Now,
                        OrderGuid = payResult.Data.OrderGuid,
                        CouponId = payResult.Data.CouponId
                    };
                    skmEfp.SaveSkmActivityLog(activityLog);
                    resultData.ThisTimeCharge = actItem.ChargesVal;
                    resultData.ThisTimeCost = actItem.ChargesCost;
                    var logs = skmEfp.GetViewSkmActivityLog(activity.Id, userId,
                        resultData.ActRecord.Range.DateRangeStart, resultData.ActRecord.Range.DateRangeEnd);
                    resultData.ActRecord.Qty = logs.Sum(x => x.ChargesVal);
                }
            }
            else
            {
                return new ApiResult
                {
                    Code = payResult.ResultCode,
                    Data = resultData
                };
            }

            #endregion

            PayEvents.OnOrdered(paymentDto.BusinessHourGuid, 1, 0, payResult.Data.OrderGuid);

            return new ApiResult
            {
                Code = payResult.ResultCode,
                Data = resultData
            };
        }

        /// <summary>
        /// 新光活動抽獎
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Skm)]
        public ApiResult PrizeDrawOrder(PrizeDrawOrderModel model)
        {
            string userName = HttpContext.Current.User.Identity.Name;
            if (string.IsNullOrWhiteSpace(userName))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.UserNoSignIn
                };
            }

            var userId = (int)HttpContext.Current.User.Identity.GetPropertyValue("Id");

            if (model == null)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }

            if (string.IsNullOrEmpty(model.EventToken))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }

            var prizeEvent = cep.GetPrizeDrawEventByToken(model.EventToken);
            if (prizeEvent.Id == 0)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }

            //燒點需要卡號
            if (string.IsNullOrEmpty(model.CardNo))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.SkmBurningFailCardNoIsNull
                };
            }

            //檢查參加資格(還不能取剩餘抽獎次數, 因為等等要抽獎)
            var verifyPrizeJoinResult = SkmFacade.VerifyPrizeJoin(prizeEvent, userId);
            if (verifyPrizeJoinResult != ApiResultCode.Success)
            {
                logger.InfoFormat("verifyPrizeJoinResult.Fail.Result->{0}.Event->{1}.UserId->{2}"
                    , verifyPrizeJoinResult.ToString()
                    , JsonConvert.SerializeObject(prizeEvent)
                    , userId);

                return new ApiResult
                {
                    Code = verifyPrizeJoinResult//DrawEventOverDailyLimit or DrawEventOverTotailLimit
                };
            }

            //使用prizeEvent.SellerGuid
            model.SellerGuid = (Guid)prizeEvent.SellerGuid;
            Guid bid = Guid.Empty;
            PrizeDrawRecord record = new PrizeDrawRecord();
            int preventingOverdrawId = 0;

            //抽獎後回填兌換地點   用來判斷哪筆recode
            Guid drawId = Guid.NewGuid();
            //驗證獎項
            var prizeItems = cep.GetPrizeDrawItems(prizeEvent.Id, false).Where(item => item.IsUse).ToList();
            //var prizeRecords = cep.GetPrizeDrawRecordByEventId(prizeEvent.Id, PrizeDrawRecordStatus.PrizeDrawCompleted);

            ApiResultCode verifyPrizeResultCode = SkmFacade.VerifyPrizeDraw(prizeEvent.Id, prizeItems);
            if (!verifyPrizeResultCode.Equals(ApiResultCode.Success))
            {
                logger.InfoFormat("VerifyPrizeDraw.Fail->{0}", verifyPrizeResultCode.ToString());
                return new ApiResult
                {
                    Code = verifyPrizeResultCode
                };
            }

            //有正確抽獎以及抽到非銘謝惠顧的獎項才進行超賣判斷
            PrizeDrawBoxItem prizeDrawBoxItem = SkmFacade.GetPrizeDrawHitBid(prizeItems);
            if (SkmFacade.PrizeDraw(prizeEvent.Id, prizeDrawBoxItem, userName, userId
                , (ApiUserDeviceType)model.DeviceType, drawId, out record))
            {
                ApiResultCode checkOverDrawReultCode;
                //防超賣
                if (!prizeDrawBoxItem.IsThank &&
                    !PponFacade.CheckPrizeOverdraw(record,
                        prizeItems.FirstOrDefault(p => p.ExternalDealId == record.Bid && p.Id == record.ItemId && p.IsRemove == false), out checkOverDrawReultCode, out preventingOverdrawId))
                {
                    return new ApiResult
                    {
                        Code = checkOverDrawReultCode
                    };
                }
            }
            else
            {
                return new ApiResult
                {
                    Code = ApiResultCode.DrawEventError
                };
            }

            #region MakeOrder

            ZeroPayResult payResult = SkmFacade.MakePrizeDrawOrder(record.Bid, userName, (ApiUserDeviceType)model.DeviceType, record.Id, prizeDrawBoxItem.IsThank);

            if (payResult.ReturnCode != ApiReturnCode.Success || payResult.ResultCode != ApiResultCode.Success)
            {
                logger.InfoFormat("MakePrizeDrawOrder.Fail.Result->{0}.Bid->{1}.userName->{2}.DeviceType->{3}.recordId->{4}"
                    , JsonConvert.SerializeObject(payResult)
                    , record.Bid
                    , userName
                    , ((ApiUserDeviceType)model.DeviceType).ToString()
                    , record.Id);
                return new ApiResult
                {
                    Code = ApiResultCode.MakePrizeDrawOrderFail
                };
            }

            #endregion

            #region 燒點

            int couponId = payResult.Data.CouponId;
            Guid orderGuid = payResult.Data.OrderGuid;

            SkmBurningEvent be = new SkmBurningEvent();
            bid = record.Bid;
            if (config.EnableCallBurningWebService)
            {
                be = SkmFacade.GetBurningEventByBid(bid);
                if (be.IsLoaded && be.BurningPoint > 0)
                {
                    if (string.IsNullOrEmpty(model.CardNo))
                    {
                        UpdatePrizeDrawStatus(PrizeDrawRecordStatus.Fail, record, preventingOverdrawId, couponId, orderGuid);
                        return SkmFacade.SetOrderFail(bid, orderGuid, ApiResultCode.SkmBurningFailCardNoIsNull, string.Empty);
                    }

                    var orderLog = SkmFacade.GetSkmBurningOrderLog(be.Id, userId, bid, orderGuid, couponId);
                    if (!orderLog.IsLoaded)
                    {
                        UpdatePrizeDrawStatus(PrizeDrawRecordStatus.Fail, record, preventingOverdrawId, couponId, orderGuid);
                        return SkmFacade.SetOrderFail(bid, Guid.NewGuid(), ApiResultCode.SkmBurningFailOrderLogError, string.Empty);
                    }

                    string message;
                    if (!SkmFacade.OrderSkmBurningCoupon(be, orderLog, model.CardNo, (ApiUserDeviceType)model.DeviceType, model.DeviceCode, model.SellerGuid, out message))
                    {
                        UpdatePrizeDrawStatus(PrizeDrawRecordStatus.Fail, record, preventingOverdrawId, couponId, orderGuid);
                        return SkmFacade.SetOrderFail(bid, orderGuid, ApiResultCode.SkmBurningFail, message);
                    }

                    UpdatePrizeDrawStatus(PrizeDrawRecordStatus.PrizeDrawCompleted, record, preventingOverdrawId, couponId, orderGuid);
                }
            }
            else
            {
                UpdatePrizeDrawStatus(PrizeDrawRecordStatus.PrizeDrawCompleted, record, preventingOverdrawId, couponId, orderGuid);
            }

            #endregion

            var drawRecords = cep.GetViewPrizeDrawRecord(prizeEvent.Id, userId);
            int remainDrawQty = SkmFacade.GetPrizeRemainQty(prizeEvent.DailyLimit, prizeEvent.TotalLimit, drawRecords);
            var nowDrawRecord = drawRecords.OrderByDescending(p => p.CreateTime).FirstOrDefault(p => p.Id == record.Id);
            var result = new DrawResultModel
            {
                ItemActiveType = nowDrawRecord.ActiveType.HasValue ? nowDrawRecord.ActiveType.Value : 0,
                ItemIsThanks = nowDrawRecord != null ? nowDrawRecord.IsThanks : false,
                ItemName = nowDrawRecord != null ? nowDrawRecord.ItemName : string.Empty,
                RemainDrawQty = remainDrawQty,
                DrawRecord = ReSetDetailModel(drawRecords.OrderBy(p => p.CreateTime).ToList()),
                TotalDrawQty = drawRecords.Count(),
                IsDailyLimit = prizeEvent.DailyLimit > 0 ? true : false,
                RecordId = record.Id//依據此Id回填當筆兌獎紀錄的兌換館別
            };

            PayEvents.OnOrdered(record.Bid, 1, 0, payResult.Data.OrderGuid);

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = result
            };
        }

        /// <summary>
        /// 新光抽獎成功後，選取兌換館別
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Skm)]
        public ApiResult PrizeDrawExchange(PrizeDrawExchangeModel model)
        {
            logger.Info("PrizeDrawExchange.model=" + JsonConvert.SerializeObject(model));
            string userName = HttpContext.Current.User.Identity.Name;
            if (string.IsNullOrWhiteSpace(userName))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.UserNoSignIn
                };
            }

            var userId = (int)HttpContext.Current.User.Identity.GetPropertyValue("Id");

            if (model == null)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }

            if (string.IsNullOrEmpty(model.EventToken))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }

            var prizeEvent = cep.GetPrizeDrawEventByToken(model.EventToken);
            if (prizeEvent.Id == 0)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }

            if (model.ShopCode.Contains('_'))
            {
                model.ShopCode = model.ShopCode.Split('_').LastOrDefault();
            }

            var prizeRecord = cep.GetPrizeDrawRecord(prizeEvent.Id, userId, model.RecordId);
            if (prizeRecord == null)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }

            prizeRecord.ExchangeShopCode = model.ShopCode;
            if (string.IsNullOrEmpty(model.ShopCode) && !cep.UpdatePrizeDrawRecord(prizeRecord))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.UpdatePrizeDrawExchangeShopCodeFail
                };
            }

            if (prizeRecord.IsSent == false)
            {

                //呼叫新光APII 傳送得獎資訊給資訊部做中獎清冊
                var burningEvent = SkmFacade.GetBurningEventByBid(prizeRecord.Bid);
                if (!burningEvent.IsLoaded)
                {
                    logger.Info("PrizeDrawExchange.!burningEvent.IsLoaded");
                    return new ApiResult
                    {
                        Code = ApiResultCode.SendPrizeDataFailInputError
                    };
                }

                var beCost = skmMp.SkmBurningCostCenterGet(burningEvent.Id).FirstOrDefault();
                if (beCost == null)
                {
                    logger.Info("PrizeDrawExchange.beCost == null");
                    return new ApiResult
                    {
                        Code = ApiResultCode.SendPrizeDataFailInputError
                    };
                }

                if (!prizeRecord.CouponId.HasValue)
                {
                    logger.Info("PrizeDrawExchange.!prizeRecord.CouponId.HasValue");
                    return new ApiResult
                    {
                        Code = ApiResultCode.SendPrizeDataFailInputError
                    };
                }

                //呼叫API
                var ed = pp.ExternalDealGet(burningEvent.ExternalGuid);
                Guid sellGuid = Guid.Empty;
                if (ed.IsHqDeal)
                {
                    sellGuid = config.SkmRootSellerGuid;
                }
                if (!SkmFacade.SendPrizeCoupon(burningEvent, beCost, prizeRecord.CouponId.Value, model.CardNo, model.ShopCode, userId, prizeRecord.Id, model.DeviceCode, sellGuid))
                {
                    logger.Info("PrizeDrawExchange.SendPrizeDataFail");
                    return new ApiResult
                    {
                        Code = ApiResultCode.SendPrizeDataFail
                    };
                }
            }

            try
            {
                prizeRecord.IsSent = true;
                cep.UpdatePrizeDrawRecord(prizeRecord);
            }
            catch (Exception e)
            {
                logger.Info(string.Format("PrizeDrawExchange.UpdatePrizeDrawRecord.Exception {0}. PrizeRecord{1}", e.ToString(), prizeRecord));
            }


            var sn = sp.SkmShoppeCollectionGetStore().FirstOrDefault(p => p.ShopCode == model.ShopCode);
            string shopName = sn != null ? sn.ShopName : string.Empty;
            string notice = prizeEvent.Notice.Replace("$@", shopName);
            var drawRecords = cep.GetViewPrizeDrawRecord(prizeEvent.Id, userId);
            int remainDrawQty = SkmFacade.GetPrizeRemainQty(prizeEvent.DailyLimit, prizeEvent.TotalLimit, drawRecords);
            //成功
            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = new
                {
                    Notice = notice,
                    DrawRecord = ReSetDetailModel(drawRecords.OrderBy(p => p.CreateTime).ToList()),
                    RemainDrawQty = remainDrawQty,
                    IsDailyLimit = prizeEvent.DailyLimit > 0 ? true : false,
                }
            };

        }

        /// <summary>
        /// 1. 更新抽獎紀錄 status/couponId/orderGuid 
        /// 2. 更新超賣判斷已失效 Processing = false (Processing=True代表購買中)
        /// </summary>
        /// <param name="status"></param>
        /// <param name="record"></param>
        /// <param name="preventingOverdrawId"></param>
        private void UpdatePrizeDrawStatus(PrizeDrawRecordStatus status, PrizeDrawRecord record, int preventingOverdrawId, int couponId, Guid orderGuid)
        {
            if (preventingOverdrawId > 0)
            {
                cep.SetPreventingOversellExpired(preventingOverdrawId);
            }

            record.CouponId = couponId;
            record.OrderGuid = orderGuid;
            record.Status = (byte)status;
            cep.UpdatePrizeDrawRecord(record);
        }

        private List<DrawDetailModel> ReSetDetailModel(List<ViewPrizeDrawRecord> viewList)
        {
            List<DrawDetailModel> detailList = new List<DrawDetailModel>();
            foreach (var item in viewList)
            {
                detailList.Add(new DrawDetailModel()
                {
                    ActiveType = item.ActiveType.HasValue ? item.ActiveType.Value : 0,
                    ItemName = item.ItemName,
                    CreateTime = item.CreateTime.ToString("yyyy/MM/dd HH:mm"),
                    ShopName = item.ShopName == null ? string.Empty : item.ShopName,
                    RecordId = item.Id,
                    IsThanks = item.IsThanks
                });
            }
            return detailList;
        }



        #endregion

        #region Channel

        /// <summary>
        /// 取得所有SKM分店
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ApiResult GetSellerList()
        {
            ApiResult result = new ApiResult();
            List<Guid> list = sp.SellerGetSkmParentList().Select(s => s.Guid).ToList();
            var shops = sp.SellerGetList(list);
            List<SellerInfo> data = new List<SellerInfo>();
            foreach (Seller item in shops)
            {
                SellerInfo info = new SellerInfo();
                var coordinate = item.Coordinate;
                var geography = LocationFacade.GetGeographyByCoordinate(coordinate);
                info.Coordinate = geography.Lat.ToString() + "," + geography.Long.ToString();
                info.SellerGuid = item.Guid;
                info.Name = item.SellerName;
                data.Add(info);
            }
            result.Data = data.OrderByDescending(x => x.Coordinate);
            result.Code = ApiResultCode.Success;
            return result;
        }

        #region 首頁2017/2 改版

        /// <summary>
        /// 分店首頁活動
        /// </summary>
        /// <param name="model">賣家Guid</param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult GetPromoIndex(SkmIndexModel model)
        {
            ApiResult result = new ApiResult();
            List<MerchantEventPromo> meps = PponFacade.MerchantEventPromoGetByToday(MerchantEventType.Skm);
            meps = meps.Where(x => x.SellerGuid == model.SellerGuid && x.Status == (int)SkmAppStyleStatus.Normal).OrderBy(x => x.Sort).ToList();

            List<SkmLatestActivity> latestActivities = SkmFacade.SkmLatestActivityGetBySellerGuid(model.SellerGuid)
                .Where(x => x.SDate <= DateTime.Now && x.EDate >= DateTime.Now && x.Status == (int)SkmAppStyleStatus.Normal)
                .OrderBy(x => x.Sort).Take(config.SkmLatestActivityTakeCnt).ToList();

            PromoIndex data = new PromoIndex { LatestActivityList = new List<LatestActivity>() };

            /*
             * LatestActivityList
             * */
            foreach (var activitie in latestActivities)
            {
                data.LatestActivityList.Add(new LatestActivity()
                {
                    ActivityTitle = activitie.ActivityTitle,
                    StoreFloor = activitie.StoreFloor,
                    StoreName = activitie.StoreName,
                    ImageUrl = config.MediaBaseUrl + "event/" + activitie.ImageUrl,
                    LinkUrl = activitie.LinkUrl,
                    Date = activitie.ActDate.ToString("yyyy/MM/dd"),
                    ActivityId = activitie.Id,
                    ActivityDescription = activitie.ActivityDescription,
                    MustLogin = activitie.IsMustLogin && activitie.LinkUrl.ToLower().IndexOf("17life.com", StringComparison.Ordinal) > -1,
                    StartDate = ApiSystemManager.DateTimeToDateTimeString(activitie.SDate),
                    EndDate = ApiSystemManager.DateTimeToDateTimeString(activitie.EDate),
                });
            }

            /*
             * eventList
             * */
            List<PromoEvent> eventList = new List<PromoEvent>();
            foreach (MerchantEventPromo promo in meps)
            {
                PromoEvent ent = new PromoEvent
                {
                    EventId = promo.Id,
                    BannerImageUrl = config.MediaBaseUrl + "Event/" + promo.MainPic + "?g=" + Guid.NewGuid(),
                    LinkUrl = promo.Url,
                    Title = promo.Title,
                    MustLogin = promo.IsMustLogin && promo.Url.ToLower().IndexOf("17life.com", StringComparison.Ordinal) > -1,
                    StartDate = ApiSystemManager.DateTimeToDateTimeString(promo.StartDate),
                    EndDate = ApiSystemManager.DateTimeToDateTimeString(promo.EndDate),
                };
                eventList.Add(ent);
            }

            #region  抽獎活動(跟屁蟲)

            var drawEvent = cep.GetIndexPrizeDrawEvent();
            if (drawEvent != null && !string.IsNullOrEmpty(drawEvent.SmallImg))
            {
                data.PrizeDrawEventList = new PrizeDrawModel();
                //跟屁蟲
                data.PrizeDrawEventList.SmallLogoData.Add(
                        new PrizeDrawItemModel()
                        {
                            Img = config.MediaBaseUrl + "PrizeDraw/" + drawEvent.SmallImg + "?g=" + Guid.NewGuid(),
                            Url = config.SSLSiteUrl + "/skmevent/prizedrawevent/" + drawEvent.Token,
                            Subject = drawEvent.Subject
                        });
            }
            #endregion


            Seller seller = sp.SellerGet(model.SellerGuid);

            data.EdmUrl = seller.SellerBlog;
            data.PromoEventList = eventList;

            result.Data = data;
            result.Code = ApiResultCode.Success;

            return result;
        }

        /// <summary>
        /// 新光精選檔次
        /// </summary>
        /// <param name="model">賣家Guid</param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult GetSkmChoiceDeal(SkmIndexModel model)
        {
            ApiResult result = new ApiResult();
            List<SkmLatestSellerDeal> skmAppDeals = SkmFacade.SkmLatestSellerDealGetBySellerGuid(model.SellerGuid).Where(x => x.Status == (int)SkmAppStyleStatus.Normal).ToList();

            SkmChoiceDeal data = new SkmChoiceDeal();
            List<CategoryDealList> chList = new List<CategoryDealList>();
            ViewCategoryDependencyCollection dependencyCol = sp.ViewCategoryDependencyGetByParentId(config.SkmCategordId);

            Dictionary<string, string> cats = new Dictionary<string, string>();
            foreach (var vcdc in dependencyCol.OrderBy(x => x.Seq))
            {
                if (vcdc.CategoryType == (int)CategoryType.DealCategory && vcdc.CategoryStatus == (int)CategoryStatus.Enabled && vcdc.Seq != 0)
                {
                    cats.Add(vcdc.CategoryId.ToString(), vcdc.CategoryName.ToString());
                }
            }

            /*
             * SellerDealList
             * */

            Dictionary<string, string> skmPublicImag = new JsonSerializer().Deserialize<Dictionary<string, string>>(_sysp.SystemDataGet(SkmPublicImageSysName).Data);

            foreach (var cid in cats.Keys)
            {
                var Deals = skmAppDeals.Where(x => x.CategoryId.ToString() == cid && x.SDate <= DateTime.Now && x.EDate >= DateTime.Now).Take(5).OrderBy(x => x.SDate);
                List<DealList> deals = new List<DealList>();
                CategoryDealList cl = new CategoryDealList();
                if (Deals.Any())
                {
                    foreach (SkmLatestSellerDeal skmAppDeal in Deals)
                    {
                        cl.CategoryId = Convert.ToInt32(skmAppDeal.CategoryId);
                        cl.CategoryName = cats.Where(x => x.Key == skmAppDeal.CategoryId.ToString()).Select(x => x.Value).FirstOrDefault();
                        if (skmPublicImag.ContainsKey(cid))
                        {
                            cl.categoryImageUrl = config.SiteUrl + "/media/Event/" + skmPublicImag[cid];  // 背景圖目前固定設在資料夾中，要更新可能直接從資料夾
                        }

                        deals.Add(new DealList()
                        {
                            Bid = skmAppDeal.LinkDealGuid,
                            ImageUrl = SkmFacade.SkmAppStyleImgPath(skmAppDeal.ImageUrl),
                            DealDescription = skmAppDeal.DealDescription,
                            DealName = skmAppDeal.DealTitle
                        });

                        cl.DealList = deals;
                    }

                    chList.Add(cl);
                }
            }

            data.SellerCategoryDealList = chList;
            result.Data = data;
            result.Code = ApiResultCode.Success;
            return result;
        }

        #endregion

        /// <summary>
        /// 分店首頁檔次分類
        /// </summary>
        /// <param name="model">賣家Guid</param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult GetSellerDealList(SellerInfo model)
        {
            ApiResult result = new ApiResult();
            List<MerchantEventPromo> meps = PponFacade.MerchantEventPromoGetByToday(MerchantEventType.Skm);
            meps = meps.Where(x => x.SellerGuid == model.SellerGuid && x.Status == (int)SkmAppStyleStatus.Normal).OrderBy(x => x.Sort).ToList();

            List<SkmLatestSellerDeal> skmAppDeals = SkmFacade.SkmLatestSellerDealGetBySellerGuid(model.SellerGuid).Where(x => x.Status == (int)SkmAppStyleStatus.Normal).ToList();
            List<SkmLatestActivity> latestActivities = SkmFacade.SkmLatestActivityGetBySellerGuid(model.SellerGuid)
                .Where(x => x.SDate <= DateTime.Now && x.EDate >= DateTime.Now && x.Status == (int)SkmAppStyleStatus.Normal).Take(4).ToList();
            SellerDealModel data = new SellerDealModel();
            List<CategoryDealList> chList = new List<CategoryDealList>();
            ViewCategoryDependencyCollection dependencyCol = sp.ViewCategoryDependencyGetByParentId(config.SkmCategordId);

            Dictionary<string, string> cats = new Dictionary<string, string>();
            foreach (var vcdc in dependencyCol.OrderBy(x => x.Seq))
            {
                if (vcdc.CategoryType == (int)CategoryType.DealCategory && vcdc.CategoryStatus == (int)CategoryStatus.Enabled)
                {
                    cats.Add(vcdc.CategoryId.ToString(), vcdc.CategoryName.ToString());
                }
            }

            data.LatestActivityList = new List<LatestActivity>();
            /*
             * LatestActivityList
             * */
            foreach (var activitie in latestActivities)
            {
                data.LatestActivityList.Add(new LatestActivity()

                {
                    ActivityTitle = activitie.ActivityTitle,
                    StoreFloor = activitie.StoreFloor,
                    StoreName = activitie.StoreName,
                    ImageUrl = config.MediaBaseUrl + "event/" + activitie.ImageUrl,
                    LinkUrl = activitie.LinkUrl,
                    Date = activitie.ActDate.ToString("yyyy/MM/dd"),
                    ActivityId = activitie.Id,
                    ActivityDescription = activitie.ActivityDescription,
                });
            }

            /*
             * SellerDealList
             * */

            Dictionary<string, string> skmPublicImag = new JsonSerializer().Deserialize<Dictionary<string, string>>(_sysp.SystemDataGet(SkmPublicImageSysName).Data);

            foreach (var cid in cats.Keys)
            {
                var Deals = skmAppDeals.Where(x => x.CategoryId.ToString() == cid && x.SDate <= DateTime.Now && x.EDate >= DateTime.Now).Take(5).OrderBy(x => x.SDate);
                List<DealList> deals = new List<DealList>();
                CategoryDealList cl = new CategoryDealList();
                if (Deals.Any())
                {
                    foreach (SkmLatestSellerDeal skmAppDeal in Deals)
                    {
                        cl.CategoryId = Convert.ToInt32(skmAppDeal.CategoryId);
                        cl.CategoryName = cats.Where(x => x.Key == skmAppDeal.CategoryId.ToString()).Select(x => x.Value).FirstOrDefault();
                        if (skmPublicImag.ContainsKey(cid))
                        {
                            cl.categoryImageUrl = config.SiteUrl + "/media/Event/" + skmPublicImag[cid];  // 背景圖目前固定設在資料夾中，要更新可能直接從資料夾
                        }

                        deals.Add(new DealList()
                        {
                            Bid = skmAppDeal.LinkDealGuid,
                            ImageUrl = SkmFacade.SkmAppStyleImgPath(skmAppDeal.ImageUrl),
                            DealDescription = skmAppDeal.DealDescription,
                            DealName = skmAppDeal.DealTitle
                        });

                        cl.DealList = deals;
                    }

                    chList.Add(cl);
                }
            }

            /*
             * eventList
             * */
            List<PromoEvent> eventList = new List<PromoEvent>();
            foreach (MerchantEventPromo promo in meps)
            {
                PromoEvent ent = new PromoEvent
                {
                    EventId = promo.Id,
                    BannerImageUrl = config.MediaBaseUrl + "Event/" + promo.MainPic + "?g=" + Guid.NewGuid(),
                    LinkUrl = promo.Url
                };
                eventList.Add(ent);
            }

            Seller seller = sp.SellerGet(model.SellerGuid);

            data.EDMUrl = seller.SellerBlog;
            data.sellerCategoryDealList = chList;
            data.PromoEventList = eventList;
            result.Data = data;
            result.Code = ApiResultCode.Success;
            return result;
        }

        #endregion

        #region 檔次相關

        /// <summary>
        /// 分類檔次列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult GetDealSynopsesListByDealType_Orig([FromBody]DealSynopsesListByDealType model)
        {
            ApiResult result = new ApiResult();

            //檢查傳入的參數
            if (!model.IsValid)
            {
                result.Code = ApiResultCode.InputError;
                return result;
            }

            //確認篩選條件
            SkmDealTypes dealType = model.Filters.Cast<SkmDealTypes>().FirstOrDefault();

            if (!config.IsEnableDealCategory)
            {
                model.Categories.Insert(0, SkmFacade.SkmDefaultCategoryId);
            }

            List<SkmDealTimeSlot> timeSlots = skmMp.SkmDealTimeSlotGetByDate(DateTime.Parse(DateTime.Now.ToString("yyyy/MM/dd")))
                .Where(x => x.Status == (int)DealTimeSlotStatus.Default
                            && x.CategoryType == SkmFacade.SkmDefaultCategoryId).ToList();//skm_deal_time_slot是用預設分類做上檔排序

            List<IViewPponDeal> ppdeals = GetSkmViewPponDeal(model.SellerGuid, ref timeSlots);
            DealSynopsesListModel data = new DealSynopsesListModel();

            if (timeSlots.Any())
            {
                var dealIconData = SkmFacade.GetDealIconModelList();

                //skm_deal_time_slot是用預設分類做上檔排序
                var timeSlotsByCategory = timeSlots.Where(x => x.DealType == (int)dealType
                                                      && x.CategoryType == SkmFacade.SkmDefaultCategoryId
                                                      && x.Status == (int)DealTimeSlotStatus.Default)
                                                .OrderBy(x => x.Sequence).ThenByDescending(x => x.Id).ToList();

                //避免在loop內重複撈取DB
                var timeSlotBids = timeSlotsByCategory.Select(p => p.BusinessHourGuid).ToList();

                ViewExternalDealCollection veds = SkmCacheFacade.GetOnlineViewExternalDeals(timeSlotBids.Distinct().ToList(), model.SellerGuid);
                List<SkmBurningEvent> sbes = SkmCacheFacade.GetOnlineSkmBurningEvents(veds.Select(x => x.Guid).ToList(), model.SellerGuid);

                var vedDict = veds.ToDictionary(x => x.Bid);
                var dealPropertys = pp.DealPropertyListGet(timeSlotBids).ToDictionary(x => x.BusinessHourGuid);
                var categoryDeals = pp.CategoryDealsGetList(timeSlotBids).ToList();

                int counter = 1;
                if (timeSlotsByCategory.Count > 0 && categoryDeals.Any(p => model.Categories.Contains(p.Cid)))
                {
                    List<Guid> memberCollectDealBids = new List<Guid>();

                    //會員收藏 不取用oath 用cookie內登入資訊
                    if (HttpContext.Current.User.Identity.IsAuthenticated)
                    {
                        int memberUnique = MemberFacade.GetUniqueId(HttpContext.Current.User.Identity.Name);
                        memberCollectDealBids = mp.MemberCollectDealGetByMemberUniqueId(memberUnique).Select(p => p.BusinessHourGuid).ToList();
                    }

                    foreach (SkmDealTimeSlot slot in timeSlotsByCategory)
                    {
                        if (vedDict.ContainsKey(slot.BusinessHourGuid))//增加防呆
                        {
                            //是否有提品
                            ViewExternalDeal ved = vedDict[slot.BusinessHourGuid];

                            var be = sbes.FirstOrDefault(p => p.ExternalGuid == ved.Guid && p.StoreCode == (ved.DealVersion >= (int)ExternalDealVersion.SkmPay ? ved.ShopCode : string.Empty)) ??
                                     new SkmBurningEvent();

                            var deal = ppdeals.FirstOrDefault(x => x.BusinessHourGuid == slot.BusinessHourGuid);
                            if (CheckDealSynopsesFail(dealType, ved, deal, timeSlots, slot, (DealOutputType)model.OutputType, be))
                            {
                                continue;
                            }

                            //分類篩選
                            var externalDealCategorys = JsonConvert.DeserializeObject<List<int>>(ved.CategoryList);
                            if (model.Categories.Intersect(externalDealCategorys).Any() == false)
                            {
                                continue;
                            }

                            var dp = dealPropertys[ved.Bid ?? Guid.Empty];
                            var newDeal = GetSKMDealSynopsis(ved, deal, dealIconData, counter, be, categoryDeals, dp);
                            if (memberCollectDealBids.Contains(newDeal.Bid))
                            {
                                newDeal.IsCollected = true;
                            }
                            data.DealList.Add(newDeal);

                            counter++;
                        }
                    }

                    data.Type = (int)dealType;
                    data.TotalDealCount = data.DealList.Count;
                    if (data.TotalDealCount > 1)
                    {
                        data.DealList = SortSkmDealSynopsis(data.DealList, (SkmCategorySortType)model.SortBy, dealType).ToList();
                    }
                    data.DealList = PagingSkmDealSynopsis(data.DealList, model.StartIndex).ToList();
                    Dictionary<string, string> skmPublicImag = new JsonSerializer().Deserialize<Dictionary<string, string>>(_sysp.SystemDataGet(SkmPublicImageSysName).Data);

                    var img = skmPublicImag.Where(x => SkmFacade.SkmDefaultCategoryId.ToString().Equals(x.Key)).ToList();
                    string pic = "";
                    if (img.Any())
                    {
                        pic = config.SiteUrl + "/media/Event/" + img.FirstOrDefault().Value;
                    }
                    data.SellerName = sp.SellerGet(model.SellerGuid).SellerName ?? "";
                    data.PublicImage = pic;
                }
                result.Code = data.TotalDealCount > 0 ? ApiResultCode.Success : ApiResultCode.DataNotFound;
            }
            else
            {
                result.Code = ApiResultCode.DataNotFound;
            }

            result.Data = data;
            return result;
        }


        /// <summary>
        /// 分類檔次列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult GetDealSynopsesListByDealType([FromBody]DealSynopsesListByDealType model)
        {
            ApiResult result = new ApiResult();
            List<string> phPool = new List<string>();
            bool bEnablePhTop10 = (SkmCategorySortType)model.SortBy == SkmCategorySortType.Default && model.Categories.Count > 0 && model.Categories[0] == SkmFacade.SkmDefaultCategoryId;

            //檢查傳入的參數
            if (!model.IsValid)
            {
                result.Code = ApiResultCode.InputError;
                return result;
            }

            //確認篩選條件
            SkmDealTypes dealType = model.Filters.Cast<SkmDealTypes>().FirstOrDefault();

            if (!config.IsEnableDealCategory)
            {
                model.Categories.Insert(0, SkmFacade.SkmDefaultCategoryId);
            }

            List<SkmDealTimeSlot> timeSlots = skmMp.SkmDealTimeSlotGetByDate(DateTime.Parse(DateTime.Now.ToString("yyyy/MM/dd")))
                .Where(x => x.Status == (int)DealTimeSlotStatus.Default
                            && x.DealType == (int)dealType
                            && x.CategoryType == SkmFacade.SkmDefaultCategoryId)
                            .OrderBy(x => x.Sequence).ThenByDescending(x => x.Id).ToList();//skm_deal_time_slot是用預設分類做上檔排序

            List<IViewPponDeal> ppdeals = GetSkmViewPponDeal(model.SellerGuid, ref timeSlots);
            DealSynopsesListModel data = new DealSynopsesListModel();

            if (timeSlots.Any())
            {
                var dealIconData = SkmFacade.GetDealIconModelList();

                //skm_deal_time_slot是用預設分類做上檔排序
                var timeSlotsByCategory = timeSlots;

                //避免在loop內重複撈取DB
                var timeSlotBids = timeSlotsByCategory.Select(p => p.BusinessHourGuid).ToList();
                ViewExternalDealCollection veds = pp.ViewExternalDealGetListByBid(timeSlotBids);
                var vedDict = veds.ToDictionary(x => x.Bid);
                List<SkmBurningEvent> sbes = skmMp.SkmBurningEventListGet(veds.Select(p => p.Guid).ToList(), false);
                var dealPropertys = pp.DealPropertyListGet(timeSlotBids).ToDictionary(x => x.BusinessHourGuid);
                var categoryDeals = pp.CategoryDealsGetList(timeSlotBids).ToList();

                int counter = 1;
                if (timeSlotsByCategory.Count > 0 && categoryDeals.Any(p => model.Categories.Contains(p.Cid)))
                {
                    List<Guid> memberCollectDealBids = new List<Guid>();

                    //會員收藏 不取用oath 用cookie內登入資訊
                    if (HttpContext.Current.User.Identity.IsAuthenticated)
                    {
                        int memberUnique = MemberFacade.GetUniqueId(HttpContext.Current.User.Identity.Name);
                        memberCollectDealBids = mp.MemberCollectDealGetByMemberUniqueId(memberUnique).Select(p => p.BusinessHourGuid).ToList();

                        //使用userId取得Umall Token
                        var skmMember = skmMp.MemberGetByUserId(memberUnique);
                        if (skmMember.IsLoaded && bEnablePhTop10) //千人千面只有在default排序才有用
                        {
                            //使用Umall Token取得Ph值
                            var skmMemberPh = skmMp.SkmMemberPhGetBySkmToken(skmMember.SkmToken);
                            if (skmMemberPh.IsLoaded)
                            {
                                if (!string.IsNullOrEmpty(skmMemberPh.Ph1)) { phPool.Add(skmMemberPh.Ph1); }
                                if (!string.IsNullOrEmpty(skmMemberPh.Ph2)) { phPool.Add(skmMemberPh.Ph2); }
                                if (!string.IsNullOrEmpty(skmMemberPh.Ph3)) { phPool.Add(skmMemberPh.Ph3); }
                            }
                        }
                    }

                    foreach (SkmDealTimeSlot slot in timeSlotsByCategory)
                    {
                        if (vedDict.ContainsKey(slot.BusinessHourGuid))//增加防呆
                        {
                            //是否有提品
                            ViewExternalDeal ved = vedDict[slot.BusinessHourGuid];

                            var be = sbes.FirstOrDefault(p => p.ExternalGuid == ved.Guid && p.StoreCode == (ved.DealVersion >= (int)ExternalDealVersion.SkmPay ? ved.ShopCode : string.Empty)) ??
                                     new SkmBurningEvent();

                            var deal = ppdeals.FirstOrDefault(x => x.BusinessHourGuid == slot.BusinessHourGuid);
                            if (CheckDealSynopsesFail(dealType, ved, deal, timeSlots, slot, (DealOutputType)model.OutputType, be))
                            {
                                continue;
                            }

                            //分類篩選
                            var externalDealCategorys = JsonConvert.DeserializeObject<List<int>>(ved.CategoryList);
                            if (model.Categories.Intersect(externalDealCategorys).Any() == false)
                            {
                                continue;
                            }

                            var dp = dealPropertys[ved.Bid ?? Guid.Empty];
                            var newDeal = GetSKMDealSynopsis(ved, deal, dealIconData, counter, be, categoryDeals, dp);
                            if (memberCollectDealBids.Contains(newDeal.Bid))
                            {
                                newDeal.IsCollected = true;
                            }
                            data.DealList.Add(newDeal);

                            counter++;
                        }
                    }

                    data.Type = (int)dealType;
                    data.TotalDealCount = data.DealList.Count;

                    if (bEnablePhTop10)
                    {
                        //計算前十筆資料
                        List<Guid> ignoreList = new List<Guid>();
                        List<SKMDealSynopsis> newDealList = new List<SKMDealSynopsis>();
                        int phCount = 1;
                        int phTake = 3;
                        foreach (var item in phPool)
                        {
                            if (phCount == 1)
                            {
                                phTake = 4;
                            }
                            else
                            {
                                phTake = 3;
                            }
                            newDealList.AddRange(data.DealList.Where(p => p.Ph == item).OrderByDescending(p => p.OrderedQuanitity).Take(phTake));
                            phCount++;
                        }

                        if (newDealList.Count != config.skmFirstShowLimit)
                        {
                            phTake = config.skmFirstShowLimit - newDealList.Count;
                            newDealList.AddRange(data.DealList.Where(p => !phPool.Contains(p.Ph)).OrderByDescending(p => p.OrderedQuanitity).Take(phTake));
                        }
                        ignoreList = newDealList.Select(p => p.Bid).ToList();
                        //首次取得分類檔次列表時
                        if (model.StartIndex == 0)
                        {
                            data.DealList = newDealList;
                        }
                        else
                        {
                            if (data.TotalDealCount > 1)
                            {
                                //排除一開始的前10筆資料
                                data.DealList = data.DealList.Where(p => !ignoreList.Contains(p.Bid)).ToList();
                                //設定DealList排序
                                data.DealList = SortSkmDealSynopsis(data.DealList, (SkmCategorySortType)model.SortBy, dealType).ToList();
                            }
                            //當model.StartIndex>10的時候，需先扣除原本的10筆排序
                            model.StartIndex = model.StartIndex - config.skmFirstShowLimit;
                        }
                    }
                    else 
                    {
                        //設定DealList排序
                        if (data.TotalDealCount > 1)
                        {
                            data.DealList = SortSkmDealSynopsis(data.DealList, (SkmCategorySortType)model.SortBy, dealType).ToList();
                        }
                    }
                    data.DealList = PagingSkmDealSynopsis(data.DealList, model.StartIndex).ToList();

                    Dictionary<string, string> skmPublicImag = new JsonSerializer().Deserialize<Dictionary<string, string>>(_sysp.SystemDataGet(SkmPublicImageSysName).Data);

                    var img = skmPublicImag.Where(x => SkmFacade.SkmDefaultCategoryId.ToString().Equals(x.Key)).ToList();
                    string pic = "";
                    if (img.Any())
                    {
                        pic = config.SiteUrl + "/media/Event/" + img.FirstOrDefault().Value;
                    }
                    data.SellerName = sp.SellerGet(model.SellerGuid).SellerName ?? "";
                    data.PublicImage = pic;
                }
                result.Code = data.TotalDealCount > 0 ? ApiResultCode.Success : ApiResultCode.DataNotFound;
            }
            else
            {
                result.Code = ApiResultCode.DataNotFound;
            }

            result.Data = data;
            return result;
        }

        /// <summary>
        /// 取得列表各類型檔次資訊 [優惠/體驗/注目] (for 3.7)
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult GetDealSynopsesListData([FromBody] GetDealSynopsesListData model)
        {
            if (!model.IsValid)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }

            if (config.IsEnableDealCategory)
            {
                return GetDealSynopsesListByCategory(model.SellerGuid, (int)DealOutputType.Mixed);
            }
            else
            {
                return GetDealSynopsesList(model.SellerGuid, SkmFacade.SkmDefaultCategoryId, model.OutputType);
            }
        }

        /// <summary>
        /// 取得列表各類型檔次資訊 [優惠/體驗/注目]
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult GetDealSynopsesListInfo([FromBody] DealSynopsesListInfoModel model)
        {
            ApiResult result = new ApiResult();

            //檢查傳入的參數
            if (!model.IsValid)
            {
                result.Code = ApiResultCode.InputError;
                return result;
            }

            return GetDealSynopsesList(model.SellerGuid, model.Categories[0], (int)DealOutputType.Default);
        }

        private ApiResult GetDealSynopsesList(Guid sellerGuid, int categoryId, int outputType)
        {
            ApiResult result = new ApiResult();

            //check 賣家(是否傳入正確分店)
            var seller = sp.SellerGet(sellerGuid);
            if (!seller.IsLoaded)
            {
                result.Code = ApiResultCode.DataNotFound;
                return result;
            }

            List<SkmDealTimeSlot> timeSlots = skmMp.SkmDealTimeSlotGetByDate(DateTime.Parse(DateTime.Now.ToString("yyyy/MM/dd")))
                .Where(x => x.Status == (int)DealTimeSlotStatus.Default && x.CategoryType == categoryId).ToList();

            List<IViewPponDeal> ppdeals = GetSkmViewPponDeal(sellerGuid, ref timeSlots);

            //避免在loop內重複撈取DB
            var timeSlotBids = timeSlots.Select(p => p.BusinessHourGuid).ToList();
            ViewExternalDealCollection veds = pp.ViewExternalDealGetListByBid(timeSlotBids);
            List<SkmBurningEvent> sbes = skmMp.SkmBurningEventListGet(veds.Select(p => p.Guid).ToList(), false);

            DealListInfo data = new DealListInfo();

            if (timeSlots.Any())
            {
                foreach (SkmDealTypes tag in Enum.GetValues(typeof(SkmDealTypes)))
                {
                    var subTimeSlots = timeSlots.Where(x => x.DealType == (int)tag && x.CategoryType == categoryId && x.Status == (int)DealTimeSlotStatus.Default).ToList();
                    if (!subTimeSlots.Any()) continue;

                    int counter = 0;
                    foreach (SkmDealTimeSlot slot in subTimeSlots)
                    {
                        //是否有提品
                        ViewExternalDeal ved = veds.FirstOrDefault(p => p.Bid == slot.BusinessHourGuid) ?? new ViewExternalDeal();

                        var be = sbes.FirstOrDefault(p => p.ExternalGuid == ved.Guid && p.StoreCode == (ved.DealVersion >= (int)ExternalDealVersion.SkmPay ? ved.ShopCode : string.Empty)) ??
                                 new SkmBurningEvent();

                        var deal = ppdeals.FirstOrDefault(x => x.BusinessHourGuid == slot.BusinessHourGuid);
                        if (CheckDealSynopsesFail(tag, ved, deal, timeSlots, slot, (DealOutputType)outputType, be))
                        {
                            continue;
                        }
                        counter++;
                    }

                    if (counter > 0)
                    {
                        data.CategoryId = categoryId;
                        data.SellerName = seller.SellerName;
                        data.DealTypeInfo.Add(new SkmDealType
                        {
                            DealType = tag,
                            DealTypeName = LunchKingSite.Core.Helper.GetLocalizedEnum(tag),
                            DealCount = counter
                        });
                    }
                }
                result.Code = ApiResultCode.Success;
            }
            else
            {
                result.Code = ApiResultCode.DataNotFound;
            }

            if (result.Code == ApiResultCode.DataNotFound)
            {
                result.Message = "查無商品資料";
            }

            result.Data = data;
            return result;
        }

        private ApiResult GetDealSynopsesListByCategory(Guid sellerGuid, int outputType)
        {
            ApiResult result = new ApiResult();

            //check 賣家(是否傳入正確分店)
            var seller = sp.SellerGet(sellerGuid);
            if (!seller.IsLoaded)
            {
                result.Code = ApiResultCode.DataNotFound;
                return result;
            }

            DealListInfo data = new DealListInfo();
            data.CategoryId = SkmFacade.SkmDefaultCategoryId;
            data.CategoryName = "全部商品";
            data.SellerName = seller.SellerName;

            data.CategoryListBySkmPay = new List<CategoryInfo>();
            data.CategoryListByZero = new List<CategoryInfo>();
            var mCategoryCol = SkmFacade.GetBelongSkmCategoryList().Where(p => p.IsShowFrontEnd);


            //all category and today
            List<SkmDealTimeSlot> timeSlots = skmMp.SkmDealTimeSlotGetByDate(DateTime.Parse(DateTime.Now.ToString("yyyy/MM/dd")))
                .Where(x => x.Status == (int)DealTimeSlotStatus.Default && x.CategoryType == data.CategoryId).ToList();


            //避免在loop內重複撈取DB
            var timeSlotBids = timeSlots.Select(p => p.BusinessHourGuid).ToList();
            List<IViewPponDeal> ppdeals = GetSkmViewPponDeal(sellerGuid, ref timeSlots);

            ViewExternalDealCollection veds = SkmCacheFacade.GetOnlineViewExternalDeals(timeSlotBids.Distinct().ToList(), sellerGuid);
            List<SkmBurningEvent> sbes = SkmCacheFacade.GetOnlineSkmBurningEvents(veds.Select(x => x.Guid).ToList(), sellerGuid);

            //檢查可行的上檔檔次(已先用預設類別(全部類別)找過一次)
            List<Guid> workedBidBySkmPay = new List<Guid>();
            List<Guid> workedBidByZero = new List<Guid>();
            if (timeSlots.Any())
            {
                foreach (SkmDealTypes tag in Enum.GetValues(typeof(SkmDealTypes)))
                {
                    var subTimeSlots = timeSlots.Where(x => x.DealType == (int)tag
                                                            && x.CategoryType == data.CategoryId
                                                            && x.Status == (int)DealTimeSlotStatus.Default).ToList();
                    //檢查是否有上檔(in timeSlots)
                    if (!subTimeSlots.Any()) continue;

                    int counter = 0;
                    foreach (SkmDealTimeSlot slot in subTimeSlots)
                    {
                        //是否有提品
                        ViewExternalDeal ved = veds.FirstOrDefault(p => p.Bid == slot.BusinessHourGuid) ??
                                               new ViewExternalDeal();

                        var be = sbes.FirstOrDefault(p =>
                                     p.ExternalGuid == ved.Guid && p.StoreCode ==
                                     (ved.DealVersion >= (int)ExternalDealVersion.SkmPay
                                         ? ved.ShopCode
                                         : string.Empty)) ??
                                 new SkmBurningEvent();

                        //檢查檔次內容
                        var deal = ppdeals.FirstOrDefault(x => x.BusinessHourGuid == slot.BusinessHourGuid);
                        if (CheckDealSynopsesFail(tag, ved, deal, timeSlots, slot, (DealOutputType)outputType, be))
                        {
                            continue;
                        }

                        counter++;

                        if (ved.IsSkmPay)
                        {
                            if (ved.Bid.HasValue)
                                workedBidBySkmPay.Add(ved.Bid.Value);
                        }
                        else
                        {
                            if (ved.Bid.HasValue)
                                workedBidByZero.Add(ved.Bid.Value);
                        }
                    }

                    if (counter > 0)
                    {
                        data.DealTypeInfo.Add(new SkmDealType
                        {
                            DealType = tag,
                            DealTypeName = LunchKingSite.Core.Helper.GetLocalizedEnum(tag),
                            DealCount = counter
                        });
                    }
                }

                //workedBid
                var workedDealCategoryIdsBySkmPay = pp.CategoryDealsGetList(workedBidBySkmPay).Select(p => p.Cid).ToList();
                var workedDealCategoryIdsByZero = pp.CategoryDealsGetList(workedBidByZero).Select(p => p.Cid).ToList();

                //商品分類
                foreach (var mc in mCategoryCol)
                {
                    List<Category> subList = new List<Category>();
                    if (workedDealCategoryIdsBySkmPay.Contains(mc.Id))
                    {
                        subList = GetSubCategory(mc.Id);

                        CategoryInfo info = new CategoryInfo()
                        {
                            CategoryId = mc.Id,
                            CategoryName = mc.Name,
                            SubCategoryList = subList.Where(p => workedDealCategoryIdsBySkmPay.Contains(p.Id))
                                .Select(p => new CategoryInfo
                                {
                                    CategoryId = p.Id,
                                    CategoryName = p.Name,
                                    SubCategoryList = new List<CategoryInfo>() //可以繼續下一階層, 但目前只有兩層
                                }).ToList()
                        };
                        data.CategoryListBySkmPay.Add(info);
                    }

                    if (workedDealCategoryIdsByZero.Contains(mc.Id))
                    {
                        if (!subList.Any())
                        {
                            subList = GetSubCategory(mc.Id);
                        }
                        CategoryInfo info = new CategoryInfo()
                        {
                            CategoryId = mc.Id,
                            CategoryName = mc.Name,
                            SubCategoryList = subList.Where(p => workedDealCategoryIdsByZero.Contains(p.Id))
                                .Select(p => new CategoryInfo
                                {
                                    CategoryId = p.Id,
                                    CategoryName = p.Name,
                                    SubCategoryList = new List<CategoryInfo>() //可以繼續下一階層, 但目前只有兩層
                                }).ToList()
                        };
                        data.CategoryListByZero.Add(info);
                    }
                }

                result.Code = ApiResultCode.Success;
            }
            else
            {
                if (result.Code == ApiResultCode.DataNotFound)
                {
                    result.Message = "查無商品資料";
                }
            }
            result.Data = data;
            return result;
        }

        private List<Category> GetSubCategory(int mainCid)
        {
            List<Category> subList = new List<Category>();
            var dp = sp.CategoryDependencyByRegionGet(mainCid);
            //子類別
            if (dp.Any())
            {
                List<string> subFilters = new List<string>();
                subFilters.Add(string.Format("{0} = {1}", Category.Columns.IsShowFrontEnd, true));
                subFilters.Add(string.Format("{0} in ({1})", Category.Columns.Id,
                    string.Join(",", dp.Select(p => p.CategoryId).ToList())));
                subList = sp.CategoryGetList(0, 0, Category.Columns.Rank.ToString(),
                    subFilters.ToArray()).ToList();
            }
            return subList;
        }

        #region 舊版檔次列表

        /// <summary>
        /// 分類檔次列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult GetDealSynopsesList([FromBody]CategoryFiltersModel model)
        {
            ApiResult result = new ApiResult();

            //檢查傳入的參數
            if (!model.IsValid)
            {
                result.Code = ApiResultCode.InputError;
                return result;
            }

            List<SkmDealTimeSlot> timeSlots = skmMp.SkmDealTimeSlotGetByDate(DateTime.Parse(DateTime.Now.ToString("yyyy/MM/dd")))
                .Where(x => x.Status == (int)DealTimeSlotStatus.Default && x.CategoryType == model.Categories[0])
                .OrderBy(x => x.Sequence).ToList();

            //取出新光的賣家
            List<Seller> sellers = sp.SellerGetSkmParentList().ToList();
            if (model.SellerGuid != Guid.Empty)
            {
                sellers = sellers.Where(x => x.Guid == model.SellerGuid).ToList();
            }

            List<ViewPponDeal> ppdeals = pp.ViewPponDealGetByBusinessHourGuidList(timeSlots.Select(x => x.BusinessHourGuid).Distinct().ToList())
                    .Where(x => x.SellerGuid == model.SellerGuid || x.SellerGuid == config.SkmRootSellerGuid).ToList();

            //過期未清算檔次
            var timeoutDeal = ppdeals.Where(x => x.SettlementTime != null && x.SettlementTime < DateTime.Now).Select(x => x.BusinessHourGuid).ToList();
            ppdeals = ppdeals.Where(x => !timeoutDeal.Contains(x.BusinessHourGuid)).ToList();
            var onDeal = ppdeals.Select(x => x.BusinessHourGuid).ToList();
            timeSlots = timeSlots.Where(x => onDeal.Contains(x.BusinessHourGuid)).ToList();

            IList<CategoryDeal> dealCategory = pp.CategoryDealsGetList(timeSlots.Select(x => x.BusinessHourGuid).Distinct().ToList())
                .Where(x => x.Cid == model.Categories[0]).ToList();

            DealListModel data = new DealListModel();

            if (timeSlots.Any())
            {
                foreach (SkmDealTypes tag in Enum.GetValues(typeof(SkmDealTypes)))
                {
                    foreach (Seller seller in sellers)
                    {
                        var _timeSlots = timeSlots.Where(x => x.DealType == (int)tag && x.CategoryType == model.Categories[0] && x.Status == (int)DealTimeSlotStatus.Default).ToList();
                        int counter = 1;
                        if (_timeSlots.Count > 0)
                        {
                            GroupDeal list = new GroupDeal();
                            foreach (SkmDealTimeSlot slot in _timeSlots)
                            {
                                //是否有提品
                                ViewExternalDeal ved = SkmCacheFacade.GetOnlineViewExternalDeal(slot.BusinessHourGuid);
                                var deal = ppdeals.FirstOrDefault(x => x.BusinessHourGuid == slot.BusinessHourGuid);

                                if (CheckDealSynopsesFail(tag, ved, deal, timeSlots, slot, DealOutputType.Default, new SkmBurningEvent()))
                                {
                                    continue;
                                }

                                SkmPponDeal skmDeal = PponDealApiManager.CreateSkmOrderPponDeal(deal);
                                SKMDealSynopsis newDeal = new SKMDealSynopsis(skmDeal, deal, dealCategory, slot.BusinessHourGuid, counter)
                                {
                                    StoreName = ved.StoreName,
                                    Shop = ved.ShopName
                                };
                                list.DealList.Add(newDeal);

                                counter++;
                            }
                            list.Type = (int)tag;
                            Dictionary<string, string> SkmPublicImag = new JsonSerializer().Deserialize<Dictionary<string, string>>(_sysp.SystemDataGet(SkmPublicImageSysName).Data);
                            var img = SkmPublicImag.Where(x => x.Key == model.Categories[0].ToString());
                            string pic = "";
                            if (img.Any())
                            {
                                pic = config.SiteUrl + "/media/Event/" + img.FirstOrDefault().Value;
                            }
                            list.SellerName = seller.SellerName;
                            data.Group.Add(list);
                            data.PublicImage = pic;
                        }

                    }
                }
                result.Code = ApiResultCode.Success;
            }
            else
            {
                result.Code = ApiResultCode.DataNotFound;
            }

            result.Data = data;
            return result;
        }

        #endregion 舊版檔次列表

        /// <summary>
        /// 檔次詳細資料
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>

        [HttpPost]
        [OAuthScope(TokenScope.Skm)]
        public ApiResult GetDealByBid(DealInfo model)
        {
            ApiResult result = new ApiResult();

            Guid bid;
            ApiImageSize imgSize;

            if (!Guid.TryParse(model.Bid.ToString(), out bid) || !Enum.TryParse(model.ImageSize, out imgSize))
            {
                result.Code = ApiResultCode.InputError;
                return result;
            }

            result.Data = SkmFacade.GetDealByBid(bid, imgSize);
            result.Code = result.Data == null ? ApiResultCode.DataNotFound : result.Code = ApiResultCode.Success;
            return result;
        }

        [HttpPost]
        public ApiResult GetDealByBidForShare(DealInfo model)
        {
            ApiResult result = new ApiResult();

            Guid bid;
            if (!Guid.TryParse(model.Bid.ToString(), out bid))
            {
                result.Code = ApiResultCode.InputError;
                return result;
            }

            result.Data = SkmFacade.GetDealByBid(bid, ApiImageSize.XS);
            result.Code = result.Data == null ? ApiResultCode.DataNotFound : result.Code = ApiResultCode.Success;
            return result;
        }

        #endregion 檔次相關

        #region 訂單相關

        #region 訂單列表明與明細各自一支的邏輯 (怕之後會改回來先隱藏)

        /*
        /// <summary>
        /// 訂單列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Skm)]
        public ApiResult GetCouponListMainListByNewFilterType()
        {
            ApiResult result = new ApiResult();
            NewCouponListFilterType theType = NewCouponListFilterType.None;
            string userName = HttpContext.Current.User.Identity.Name;
            var rtn = PponDealApiManager.ApiPponOrderListItemGetListByFilterType(userName, theType, false);

            result.Data = new OrderListModel(rtn);
            result.Code = ApiResultCode.Success;
            return result;
        }

        /// <summary>
        /// 訂單詳細資料
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Skm)]
        public ApiResult GetMemberOrderByOrderGuid(GetMemberOrderByOrderGuidModel model)
        {
            ApiUserOrder userOrder = PponDealApiManager.ApiUserOrderGetByOrderGuid(model.OrderGuid, model.UserId);
            if (userOrder != null)
            {
                return new ApiResult
                {
                    Data = userOrder,
                    Code = ApiResultCode.Success
                };
            }
            return new ApiResult { Code = ApiResultCode.DataNotFound };
        }
        */

        #endregion

        /// <summary>
        /// EC 訂單列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Skm)]
        public ApiResult GetEcOrderList()
        {
            var ecOrder = SkmFacade.GetEcOrderList(HttpContext.Current.User.Identity.Name);

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = ecOrder
            };
        }


        /// <summary>
        /// Coupon 列表 (訂單列表與明細合併) (0元檔)
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Skm)]
        public ApiResult GetOrderCouponList()
        {
            var apiResult = new ApiResult { Code = ApiResultCode.DataNotFound };

            string userName = HttpContext.Current.User.Identity.Name;
            var order = SkmFacade.GetSkmOrder(userName);

            if (order == null) return apiResult;

            if (order.OrderList.Any())
            {
                apiResult.Data = order.OrderList;
                apiResult.Code = ApiResultCode.Success;
            }

            return apiResult;
        }

        /// <summary>
        /// 重新整理憑證QrCode
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Skm)]
        public ApiResult RefreshCoupon(RefreshCouponModel model)
        {
            var ctls = mp.CashTrustLogGetListByOrderGuid(model.OrderGuid, OrderClassification.Skm);
            //核對憑證是否存在
            if (ctls.All(x => x.CouponSequenceNumber != model.SequenceNumber))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InvalidCoupon
                };
            }
            //檢查憑證狀態
            var ctl = ctls.First(x => x.CouponSequenceNumber == model.SequenceNumber);
            int userId = MemberFacade.GetUniqueId(HttpContext.Current.User.Identity.Name);
            if (ctl.UserId != userId) //非憑證持有者
            {
                return new ApiResult
                {
                    Code = ApiResultCode.BadUserInfo
                };
            }

            var orderData = op.OrderGet(ctl.OrderGuid);
            if (orderData.IsLoaded && orderData.OrderSettlementTime != null &&
                (DateTime)orderData.OrderSettlementTime < DateTime.Now)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InvalidInterval,
                    Message = "憑證已過期"
                };
            }

            switch (ctl.Status)
            {
                case (int)TrustStatus.Verified:
                    return new ApiResult
                    {
                        Code = ApiResultCode.CouponUsed
                    };
                case (int)TrustStatus.Refunded:
                case (int)TrustStatus.Returned:
                    return new ApiResult
                    {
                        Code = ApiResultCode.CouponReturned
                    };
                case (int)TrustStatus.ATM:
                    return new ApiResult
                    {
                        Code = ApiResultCode.InvalidCoupon
                    };
            }

            var vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(ctl.BusinessHourGuid ?? Guid.Empty);

            var qrCodeModel = new QrCodeModel();

            DateTime qrCodeExpireTime = DateTime.Now.AddMinutes(config.SkmQrCodeAppExpireTime).AddSeconds(1);

            ISkmQrcodeBase fatory = new SkmQrcodeFatory().CreateFatory(new SkmQrcodeDataModel(ctl, vpd) { });
            if (!string.IsNullOrEmpty(fatory.TrustCode))
            {
                qrCodeModel.QrCode = fatory.GetSkmQrCodeString();
                qrCodeModel.QrCodeExpireTime = qrCodeExpireTime.ToString("yyyy/MM/dd HH:mm:ss");
            }

            if (!string.IsNullOrEmpty(qrCodeModel.QrCode))
            {
                return new ApiResult
                {
                    Data = qrCodeModel,
                    Code = ApiResultCode.Success
                };
            }
            return new ApiResult { Code = ApiResultCode.InvalidCoupon };
        }

        /// <summary>
        /// 測試QrCode順序(資料都為預設值/空值)
        /// </summary>
        /// <param name="type">type=1測Qrcode1 / type=4測Qrcode2</param>
        /// <returns></returns>
        public ApiResult NewCoupon(int type = 1)
        {
            var qrCodeModel = new QrCodeModel();

            DateTime qrCodeExpireTime = DateTime.Now.AddMinutes(config.SkmQrCodeAppExpireTime).AddSeconds(1);

            ISkmQrcodeBase fatory = new SkmQrcodeFatory().CreateFatory(new SkmQrcodeDataModel() { ActiveType = type });
            if (!string.IsNullOrEmpty(fatory.TrustCode))
            {
                qrCodeModel.QrCode = fatory.GetSkmQrCodeString();
                qrCodeModel.QrCodeExpireTime = qrCodeExpireTime.ToString("yyyy/MM/dd HH:mm:ss");
            }

            if (!string.IsNullOrEmpty(qrCodeModel.QrCode))
            {
                return new ApiResult
                {
                    Data = qrCodeModel,
                    Code = ApiResultCode.Success
                };
            }
            return new ApiResult { Code = ApiResultCode.InvalidCoupon };
        }

        #endregion 訂單相關

        #region Beancon

        /// <summary>
        /// 取得Beacon監聽UUID資訊
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Skm)]
        public ApiResult ActivityListenInspection(ActivityListenInspectionModel model)
        {
            //取得裝置
            //SetDeviceListener()

            //回傳監聽UUID
            //回傳Beacon監聽UUID

            //回傳ApiResult
            ApiResult result = new ApiResult();

            List<Pokeball> pokeballList = new List<Pokeball>();
            Pokeball pokeball = new Pokeball();
            pokeball.Type = LunchKingSite.Core.ModelCustom.PokeballType.beaconListener.ToString();

            if (config.EnableSKMNewBeacon)
            {
                pokeball.Uuids = BeaconFacade.GetBeaconTriggerAppUuidList((int)BeaconTriggerAppType.SkmBeaconBeacon);
            }
            else
            {
                var actionEvent = GetActionEventInfo();
                if (actionEvent.IsLoaded)
                {
                    pokeball.Uuids.Add(actionEvent.SellerUuid.ToString());
                    pokeball.Uuids.Add(actionEvent.ElectricUuid.ToString());
                }
            }

            pokeballList.Add(pokeball);
            result.Data = new { pokeballList = pokeballList };

            result.Code = ApiResultCode.Success;
            return result;

        }

        /// <summary>
        /// Beacon事件訊息取得
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Skm)]
        public ApiResult ActivityTrigger(ActivityTriggerModel model)
        {
            var accessToken = HttpContext.Current.Items["AccessToken"].ToString();
            var memberUinqueId = Convert.ToInt32(HttpContext.Current.User.Identity.GetPropertyValue("Id"));

            //回傳ApiResult
            ApiResult result = new ApiResult();
            if (config.EnableSKMNewBeacon)
            {
                BeaconManager.IBeaconBase factory = new BeaconManager.BeaconFatory().CreateFatory(
                   new BeaconTriggerModel { major = model.Major, minor = model.Minor, uuId = model.UuId },
                    memberUinqueId,
                    model.IdentifierCode,
                    accessToken);

                //for debug
                result.Message = string.IsNullOrEmpty(factory.ErrorMessage) ? result.Message : factory.ErrorMessage;
                result.Data = new { pokeballList = factory.PokeballList };
                result.Code = ApiResultCode.Success;
            }
            else
            {
                #region 舊beacon程式
                List<Pokeball> pokeballList = new List<Pokeball>();
                Pokeball pokeball = new Pokeball();

                //電量檢查UUID排除
                var activity = GetActionEventInfo();

                if (activity.IsLoaded)
                {
                    if (activity.ElectricUuid.HasValue && string.Equals(model.UuId, activity.ElectricUuid.Value.ToString(), StringComparison.OrdinalIgnoreCase))
                    {
                        //電量資訊
                        if (model.Major > 0 && model.Major > 0)
                        {
                            //var beaconDevices = mp.ActionEventDeviceInfoListGet(activity.Id);
                            var beaconBatteryCapacity = ApiActionFilterManager.DecodeBatteryCapacityOfBeacon(model.Major);
                            var beaconMacAddress = ApiActionFilterManager.DecodeMacAddressOfBeacon(model.Major, model.Major);
                            //var beaconDevice = beaconDevices.FirstOrDefault(x => x.ElectricMacAddress.Substring(10, 7).Equals(beaconMacAddress, StringComparison.OrdinalIgnoreCase));

                            var beaconDevice = mp.ActionEventDeviceInfoGet(activity.Id, beaconMacAddress);
                            if (beaconDevice != null && beaconDevice.IsLoaded)
                            {
                                beaconDevice.ElectricPower = beaconBatteryCapacity;
                                beaconDevice.LastUpdateTime = DateTime.Now;
                                mp.ActionEventDeviceInfoSet(beaconDevice);
                            }

                            #region Beacon_Log 

                            SkmBeaconTriggerLog("SkmBeaconElectricLog", string.Format("accessToken:{0},skmMemberId:{1},uuId:{2}, major:{3}, minor:{4}", accessToken, memberUinqueId, model.UuId, model.Major, model.Minor));

                            #endregion Beacon_Log
                        }
                    }
                    else if (activity.SellerUuid.HasValue && string.Equals(model.UuId, activity.SellerUuid.Value.ToString(), StringComparison.OrdinalIgnoreCase))
                    {
                        bool isInTimeSpan = false;


                        //取得會員訊息中心記錄
                        DevicePushRecordCollection devicePushRecords = lp.DevicePushRecordListGet(0, memberUinqueId);
                        //取得今日推播訊息中心上限
                        int todayMsgBoxCount = devicePushRecords.Where(x => x.IsDeal == false).Count(x => x.CreateTime.Date.Equals(DateTime.Now.Date));
                        bool isLessThanDailyLimit = todayMsgBoxCount < (config.SkmBeaconNotificationDailyLimit + config.SkmBeaconMsgBoxDailyLimit);//推播及非推播每日總上限
                                                                                                                                                   //logger.Warn(config.SkmBeaconNotificationDailyLimit + config.SkmBeaconMsgBoxDailyLimit);

                        var lastPushRecord = devicePushRecords.ToList().OrderByDescending(x => x.CreateTime).FirstOrDefault();

                        if (lastPushRecord != null && lastPushRecord.Id > 0)
                        {
                            if (todayMsgBoxCount < config.SkmBeaconNotificationDailyLimit)
                            {
                                isInTimeSpan = (DateTime.Now < lastPushRecord.CreateTime.AddMinutes(config.SkmBeaconNotificationTimeSpanMinutes));
                            }
                            else
                            {
                                isInTimeSpan = (DateTime.Now < lastPushRecord.CreateTime.AddMinutes(config.SkmBeaconMsgBoxTimeSpanMinutes));
                            }
                        }

                        var notificationMessage = GetFilterSkmBeaconMessage(activity.Id, memberUinqueId, model.Major, model.Minor);

                        if (notificationMessage.Id > 0)
                        {

                            if (notificationMessage.Status.Equals((int)SkmBeaconStatus.Deal))
                            {
                                #region 收藏檔次推播
                                //收藏檔次推播
                                var deviceRecord = new DataOrm.DevicePushRecord();
                                deviceRecord.ActionId = notificationMessage.Id;
                                deviceRecord.DeviceId = default(int);
                                deviceRecord.CreateTime = DateTime.Now;
                                deviceRecord.MemberId = memberUinqueId;
                                deviceRecord.IdentifierType = 2;
                                //deviceRecord.IsRemove = true;
                                deviceRecord.IsDeal = true;
                                //deviceRecord.RemoveTime = DateTime.Now;
                                int newId = lp.DevicePushRecordSetNotExist(deviceRecord);

                                //不寫入訊息中心
                                pokeball.Type = "localPush";
                                pokeball.Message = notificationMessage.Subject;

                                if (notificationMessage.BeaconType == (int)SkmBeaconType.Deal)
                                {
                                    Models.SkmModels.Action actinEvent = new Models.SkmModels.Action { ActionType = "showDealDetail", Bid = notificationMessage.BusinessHourGuid.ToString() };
                                    pokeball.ActionList.Add(actinEvent);
                                }

                                #endregion 收藏檔次推播

                                #region Beacon_Log 

                                SkmBeaconTriggerLog("SkmBeaconTriggerLog", string.Format("accessToken:{0},skmMemberId:{1},uuId:{2}, major:{3}, minor:{4},SkmPushActionEventPushMsgId={5},recordId={6}", accessToken, memberUinqueId, model.UuId, model.Major, model.Minor, notificationMessage.Id, newId));

                                #endregion Beacon_Log
                            }
                            else if (isLessThanDailyLimit && !isInTimeSpan && notificationMessage.Status.Equals((int)SkmBeaconStatus.Normal))
                            {
                                #region 其他類型推播
                                //其他類型推播
                                var deviceRecord = new DataOrm.DevicePushRecord();
                                deviceRecord.ActionId = notificationMessage.Id;
                                deviceRecord.DeviceId = default(int);
                                deviceRecord.CreateTime = DateTime.Now;
                                deviceRecord.MemberId = memberUinqueId;
                                deviceRecord.IdentifierType = 2;
                                int newId = lp.DevicePushRecordSetNotExist(deviceRecord);

                                if (todayMsgBoxCount < config.SkmBeaconNotificationDailyLimit)
                                {
                                    pokeball.Type = "localPush";
                                    pokeball.Message = notificationMessage.Subject;

                                    if (notificationMessage.BeaconType == (int)SkmBeaconType.Deal)
                                    {
                                        Models.SkmModels.Action actinEvent = new Models.SkmModels.Action { ActionType = "showDealDetail", Bid = notificationMessage.BusinessHourGuid.ToString() };
                                        pokeball.ActionList.Add(actinEvent);
                                    }

                                    Models.SkmModels.Action actionMessage = new Models.SkmModels.Action { ActionType = "markMessageRead", MessageId = deviceRecord.Id.ToString(), TriggerType = "1" };

                                    pokeball.ActionList.Add(actionMessage);
                                }

                                #endregion 其他類型推播

                                #region Beacon_Log 

                                SkmBeaconTriggerLog("SkmBeaconTriggerLog", string.Format("accessToken:{0},skmMemberId:{1},uuId:{2}, major:{3}, minor:{4},SkmPushActionEventPushMsgId={5},recordId={6}", accessToken, memberUinqueId, model.UuId, model.Major, model.Minor, notificationMessage.Id, newId));

                                #endregion Beacon_Log

                            }

                            pokeballList.Add(pokeball);
                        }
                        else
                        {
                            //紀錄Beacon觸發情況，
                            #region Beacon_Log 

                            SkmBeaconTriggerLog("SkmBeaconTriggerLog", string.Format("accessToken:{0},skmMemberId:{1},uuId:{2}, major:{3}, minor:{4},SkmPushActionEventPushMsgId={5}", accessToken, memberUinqueId, model.UuId, model.Major, model.Minor, -1));

                            #endregion Beacon_Log
                        }
                    }

                }

                result.Data = new { pokeballList = pokeballList };
                result.Code = ApiResultCode.Success;
                #endregion
            }
            return result;
        }

        private void SkmBeaconTriggerLog(string logType, string logInfo)
        {
            try
            {
                PromoItem item = new PromoItem();
                item.Code = logType;
                item.UseTime = DateTime.Now;
                item.Memo = logInfo;
                ProviderFactory.Instance().GetProvider<IEventProvider>().PromoItemSet(item);
            }
            catch
            {
                // ignored
            }
        }

        #endregion Beancon

        #region 訊息中心 MessageBox

        /// <summary>
        /// 取得訊息列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Skm)]
        public ApiResult MessageCollection(MessageCollectionModel model)
        {
            var userId = Convert.ToInt32(HttpContext.Current.User.Identity.GetPropertyValue("Id"));

            #region Beacon 訊息

            List<BeaconMessageModel> beaconMsgPushRecords;
            if (!string.IsNullOrEmpty(model.MessageId))
            {
                //取子訊息列表
                int mid = Convert.ToInt16(model.MessageId);
                beaconMsgPushRecords = bp.GetBeaconAppSubeventList(mid, userId);
            }
            else
            {
                //取主訊息列表
                DateTime dueDate = DateTime.Now.AddDays(config.SKMBeaconMessageDueDays);
                beaconMsgPushRecords = bp.GetBeaconAppEventList(userId, dueDate);
            }

            List<MessageBoxObj> messagesList = new List<MessageBoxObj>();
            foreach (var record in beaconMsgPushRecords)
            {
                //因APP舊版的純文字是判斷pokeballList為空陣列
                var pokeballList = new List<Pokeball>();
                if (record.EventType != (int)EventType.Message)
                {
                    var prePokeBall = new SkmPrePokeball(record);
                    pokeballList = new List<Pokeball> { GetPokeBall(prePokeBall) };
                }

                var objectMsg = new MessageBoxObj
                {
                    Id = record.Id,
                    IsRead = record.IsRead ? true.ToString() : false.ToString(),
                    Subject = record.Subject,
                    Content = record.Content,
                    SendTime = ApiSystemManager.DateTimeToDateTimeString(record.CreateTime),
                    PokeballList = pokeballList,
                    IsInApp = false.ToString(),
                    OrderTime = record.CreateTime
                };
                messagesList.Add(objectMsg);
            }

            #endregion

            #region InAppMessage

            List<SkmInAppMessage> msgData = skmEfp.GetSkmInAppMessage(userId);
            foreach (var m in msgData)
            {
                var pokeballList = new List<Pokeball>();

                var prePokeBall = new SkmPrePokeball(m);
                pokeballList.Add(GetPokeBall(prePokeBall));

                var objectMsg = new MessageBoxObj
                {
                    Id = m.Id,
                    IsRead = m.IsRead ? true.ToString() : false.ToString(),
                    Subject = m.Subject,
                    Content = m.MsgContent,
                    SendTime = ApiSystemManager.DateTimeToDateTimeString(m.SendTime),
                    PokeballList = pokeballList,
                    IsInApp = true.ToString(),
                    OrderTime = m.SendTime
                };
                messagesList.Add(objectMsg);
            }

            #endregion InAPPMessage

            return new ApiResult()
            {
                Code = ApiResultCode.Success,
                Data = new { messages = messagesList.OrderBy(x => x.OrderTime).ToList() }
            };
        }

        /// <summary>
        /// 讀取訊息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Skm)]
        public ApiResult ReadMessage(ReadMessageModel model)
        {
            if (model.TriggerType == (int)SkmTriggerType.InAppMessageRead)
            {
                skmEfp.SetInAppMessageRead(model.MessageId);
            }
            else
            {
                var deviceRecord = lp.DevicePushRecordGet(model.MessageId);

                deviceRecord.IsRead = true;
                deviceRecord.TriggerType = model.TriggerType;
                deviceRecord.ReadTime = DateTime.Now;
                lp.DevicePushRecordSet(deviceRecord);
            }

            ApiResult result = new ApiResult();
            result.Data = new { isSuccess = true };
            result.Code = ApiResultCode.Success;
            return result;
        }

        /// <summary>
        /// 讀取訊息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Skm)]
        public ApiResult ReadMessages(ReadMessagesModel model)
        {
            foreach (ReadMessageBaseModel item in model.ReadMessages)
            {
                try
                {
                    if (item.TriggerType == (int)SkmTriggerType.InAppMessageRead)
                    {
                        skmEfp.SetInAppMessageRead(item.MessageId);
                    }
                    else
                    {
                        var deviceRecord = lp.DevicePushRecordGet(item.MessageId);
                        deviceRecord.IsRead = true;
                        deviceRecord.TriggerType = item.TriggerType;
                        deviceRecord.ReadTime = DateTime.Now;
                        lp.DevicePushRecordSet(deviceRecord);
                    }
                }
                catch (Exception ex)
                {
                    logger.ErrorFormat("SKM.ReadMessages.{0}->{1}", ex.Message, ex.StackTrace);
                }
            }

            ApiResult result = new ApiResult();
            result.Data = new { isSuccess = true };
            result.Code = ApiResultCode.Success;
            return result;
        }

        /// <summary>
        /// 刪除訊息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Skm)]
        public ApiResult RemoveMessage(RemoveMessageModel model)
        {
            string splitArray = model.MessageIds.Replace("[", "").Replace("]", "");
            List<int> removeList = (from string s in splitArray.Split(',') select Convert.ToInt32(s)).ToList<int>();

            if (model.IsInApp)
            {
                foreach (var id in removeList)
                {
                    skmEfp.SetInAppMessageDel(id);
                }
            }
            else
            {
                var access_token = HttpContext.Current.Items["AccessToken"].ToString();

                var token = OAuthFacade.GetTokenByToken(access_token);

                var userId = token.UserId;
                int deviceId = 0;

                var deviceData = lp.DeviceIdentyfierInfoGet(model.IdentifierCode);
                if (deviceData.IsLoaded)
                {
                    deviceId = deviceData.Id;
                }

                DevicePushRecordCollection devicePushRecords = lp.DevicePushRecordListGet(deviceId, userId);

                List<DataOrm.DevicePushRecord> records;
                if (token.UserId > 0)
                {
                    records = devicePushRecords.Where(x => !x.IsRemove).OrderByDescending(z => z.CreateTime).ToList();
                }
                else
                {
                    records = devicePushRecords.Where(x => !x.IsRemove && !x.MemberId.HasValue).OrderByDescending(z => z.CreateTime).ToList();
                }

                foreach (var recordId in removeList)
                {
                    var record = records.FirstOrDefault(x => x.Id == recordId);
                    if (record != null && record.IsLoaded)
                    {
                        record.IsRemove = true;
                        record.RemoveTime = DateTime.Now;
                        lp.DevicePushRecordSet(record);
                    }
                }
            }

            ApiResult result = new ApiResult();
            result.Data = new
            {
                messageIds = removeList
            };
            result.Code = ApiResultCode.Success;
            return result;
        }

        /// <summary>
        /// 刪除所有已讀訊息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Skm)]
        public ApiResult RemoveReadMessage(RemoveReadMessage model)
        {
            var access_token = HttpContext.Current.Items["AccessToken"].ToString();

            var token = OAuthFacade.GetTokenByToken(access_token);

            var id = token.UserId;

            //int userId = MemberFacade.GetUniqueId(userName);

            DevicePushRecordCollection devicePushRecords = lp.DevicePushRecordListGet(0, id);
            foreach (var record in devicePushRecords)
            {
                if (record != null && record.IsLoaded && record.IsRead && !record.IsRemove)
                {
                    record.IsRemove = true;
                    record.RemoveTime = DateTime.Now;
                    lp.DevicePushRecordSet(record);
                }
            }



            ApiResult result = new ApiResult();
            result.Data = new { isSuccess = true };
            result.Code = ApiResultCode.Success;
            return result;
        }

        #endregion 訊息中心 MessageBox

        #region Gateway

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Skm)]
        public ApiResult KeepAlive(KeepAliveModel model)
        {
            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = DateTime.Now,
                Message = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")
            };
        }

        /// <summary>
        /// 讓新光POS的Gateway呼叫核銷
        /// </summary>
        /// <param name="model"></param>
        /// <remarks>讓新光POS的Gateway呼叫核銷</remarks>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Skm)]
        //[SwaggerResponse((int)ApiResultCode.Success, "呼叫成功")]
        //[SwaggerResponse((int)ApiResultCode.Error, "發生錯誤")]
        public ApiResult GatewayVerify(VerifyGatewayModel model)
        {
            logger.InfoFormat("GatewayVerify request-{0}", new JsonSerializer().Serialize(model));
            ApiResult result = new ApiResult();
            SkmVerifyLog log = new SkmVerifyLog()
            {
                TrustId = model.TrustId,
                SkmId = model.SkmId,
                CreateTime = DateTime.Now,
                IsForce = model.IsForce,
                VerifyAction = model.VerifyAction,
                SkmTransId = model.SkmTransId,
                ShopeCode = model.ShopeCode,
                BrandCounterCode = model.BrandCounterCode,
                InputModel = new JsonSerializer().Serialize(model)
            };

            var reply = SkmFacade.GetSkmVerifyReply(model.TrustId, log, true);

            try
            {
                VerificationStatus status = OrderFacade.VerifyCouponForSkm(model.TrustId, "skmGatewayVerify", model.ShopeCode, model.BrandCounterCode);
                if (status == VerificationStatus.CouponOk)
                {
                    reply.VerifySuccess = true;
                    log.SyncStatus = 1;
                    log.SyncMsg = "OK";
                    result.Code = ApiResultCode.Success;
                    result.Message = "OK";

                    SkmPayOrder skmPayOrder = skmEfp.GetSkmPayOrderByTrustId(model.TrustId);
                    //若找不到SkmPay Order 表示不是EC訂單，就不做發票核銷的工作
                    if (skmPayOrder != null && skmPayOrder.Id > 0)
                    {
                        SkmPayOrderStatus skmPayOrderStatus;
                        bool isWriteOff = SkmFacade.InvoiceWriteOff(skmPayOrder, out skmPayOrderStatus);
                    }
                    try
                    {
                        SKMCenterRocketMQTrans.AlibabaMQTrans(skmPayOrder.OrderGuid, SKMCenterRocketMQTrans.ordeSatus.confirm);
                    }
                    catch (Exception e)
                    {
                        logger.Error("call AlibabaMQTrans has Error =>" + e.Message);
                    }
                  

                }
                else
                {
                    reply.VerifySuccess = false;
                    log.SyncStatus = 0;
                    log.SyncMsg = "Fail:" + status.ToString();
                    result.Code = ApiResultCode.Error;
                    result.Message = "Fail:" + status.ToString();
                }
            }
            catch (Exception ex)
            {
                log.SyncStatus = 0;
                log.SyncMsg = "Error:" + ex.Message;
                result.Code = ApiResultCode.Error;
                result.Message = "Error:" + ex.Message;
            }
            SkmFacade.VerifyLogSet(log);

            if (config.EnableCallBurningWebService && config.EnableSkmBurning && reply.TrustId != Guid.Empty)
            {
                reply = SkmFacade.SetSkmVerifyReply(reply);
                if (reply.IsLoaded)
                {
                    SkmFacade.SendVerifyReply(reply);
                }
            }

            logger.InfoFormat("GatewayVerify response-{0}", new JsonSerializer().Serialize(result));
            return result;
        }

        /// <summary>
        /// 讓新光POS的Gateway呼叫取消核銷
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Skm)]
        //[SwaggerResponse((int)ApiResultCode.Success, "呼叫成功")]
        //[SwaggerResponse((int)ApiResultCode.Error, "發生錯誤")]
        //[SwaggerResponse((int)ApiResultCode.CouponPartialFail, "處理此憑證部分失敗")]
        public ApiResult GatewayCancel(VerifyGatewayModel model)
        {
            ApiResult result = new ApiResult();
            SkmVerifyLog log = new SkmVerifyLog()
            {
                TrustId = model.TrustId,
                SkmId = model.SkmId,
                CreateTime = DateTime.Now,
                IsForce = model.IsForce,
                VerifyAction = model.VerifyAction,
                SkmTransId = model.SkmTransId,
                ShopeCode = model.ShopeCode,
                BrandCounterCode = model.BrandCounterCode
            };

            try
            {
                if (OrderFacade.UndoVerifiedStatus(model.TrustId, "SkmGateway", OrderClassification.LkSite))
                {
                    log.SyncStatus = 1;
                    log.SyncMsg = "OK";
                    result.Code = ApiResultCode.Success;
                    result.Message = "OK";
                    CashTrustLog ctlog = mp.CashTrustLogGet(model.TrustId);
                  
                    try
                    {
                        SKMCenterRocketMQTrans.AlibabaMQTrans(ctlog.OrderGuid, SKMCenterRocketMQTrans.ordeSatus.cancel);
                    }
                    catch (Exception e)
                    {
                        logger.Error("call AlibabaMQTrans has Error =>" + e.Message);
                    }
                }
                else
                {
                    log.SyncStatus = 0;
                    log.SyncMsg = "Fail" + ApiResultCode.CouponPartialFail.ToString();
                    result.Code = ApiResultCode.Error;
                    result.Message = "Fail" + ApiResultCode.CouponPartialFail.ToString();
                }
            }
            catch (Exception ex)
            {
                log.SyncStatus = 0;
                log.SyncMsg = "Error:" + ex.Message;
                result.Code = ApiResultCode.Error;
                result.Message = "Error:" + ex.Message;
            }
            SkmFacade.VerifyLogSet(log);

            if (config.EnableCallBurningWebService && config.EnableSkmBurning)
            {
                var reply = SkmFacade.GetSkmVerifyReply(model.TrustId, log);
                if (reply.IsLoaded)
                {
                    SkmFacade.SendVerifyReply(reply, true);
                }
            }

            return result;
        }

        /// <summary>
        /// 批次由SkmGateway以店家為group更新17Life DB
        /// </summary>
        /// <param name="models"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Skm)]
        //[SwaggerResponse((int)ApiResultCode.Success, "呼叫成功")]
        //[SwaggerResponse((int)ApiResultCode.Error, "發生錯誤")]
        public ApiResult GatewayShoppe(List<ShoppeGatewayModel> models)
        {
            ApiResult result = new ApiResult();
            string shopCode = models[0].ShopId;

            var skmSeller = skmMp.ShoppeGetSellerByShopeCode(shopCode); //查詢是否有此總館
            if (!skmSeller.IsLoaded || skmSeller.SellerGuid == Guid.Empty)
            {
                result.Code = ApiResultCode.SkmSellerIsNull;
                return result;
            }

            try
            {
                using (var tran = TransactionScopeBuilder.CreateReadCommitted())
                {
                    //IsExistSkmShoppe
                    SkmShoppeCollection shoppes = skmMp.SkmShoppeGetAllShoppe(skmSeller.StoreGuid ?? Guid.Empty, shopCode); //把該櫃位撈出來
                    SellerCollection sellerCol = new SellerCollection();
                    DateTime now = DateTime.Now;
                    shoppes.ForEach(x => x.IsAvailable = false);   //預設都先沒開啟，後續更新
                    foreach (var item in models)
                    {
                        var checkShoppe = skmMp.SkmShoppeGet(item.ShopId, item.BrandCounterId);
                        if (checkShoppe.IsLoaded)
                        {
                            if (!checkShoppe.IsAvailable)
                            {
                                skmMp.SkmShoppeChangeLogAdd(new SkmShoppeChangeLog
                                {
                                    ShopCode = checkShoppe.ShopCode,
                                    BrandCounterCode = checkShoppe.BrandCounterCode,
                                    ChangeMemo = "re-enable",
                                    PreviousObj = json.Serialize(checkShoppe),
                                    NextObj = json.Serialize(item)
                                });
                            }

                            Guid storeGuid = Guid.Empty;
                            //update shoppe
                            shoppes.Where(x => (x.ShopCode == item.ShopId && x.BrandCounterCode == item.BrandCounterId))
                                .ForEach(i =>
                                {
                                    i.BrandCounterName = item.BrandCounterName;
                                    i.SkmStoreId = item.SkmStoreId;
                                    i.SkmStoreName = item.SkmStoreName;
                                    i.ShopName = item.ShopName;
                                    i.CategoryId = item.CategoryId;
                                    i.CategoryName = item.CategoryName;
                                    i.SubcategoryId = item.SubcategoryId;
                                    i.SubcategoryName = item.SubcategoryName;
                                    i.IsAvailable = true;
                                    i.IsShoppe = true;
                                    i.Floor = item.Floor;
                                    i.ModifyDate = now;
                                    storeGuid = i.StoreGuid ?? Guid.Empty;
                                    i.SkmStoreName = item.SkmStoreName;
                                    i.BrandGuid = item.BrandGuid;
                                    i.SkmStoreGuid = item.StoreGuid;
                                });
                            if (storeGuid != Guid.Empty)
                            {
                                var seller = sp.SellerGet(storeGuid);
                                if (seller.IsLoaded && seller.SellerName != item.BrandCounterName)
                                {
                                    seller.SellerName = item.BrandCounterName;
                                    sellerCol.Add(seller);
                                }
                            }
                        }
                        else
                        {
                            Guid sellerGuid = Guid.NewGuid();
                            SkmShoppe shoppe = new SkmShoppe()
                            {
                                StoreGuid = sellerGuid,
                                ShopCode = item.ShopId,
                                ShopName = item.ShopName,
                                BrandCounterCode = item.BrandCounterId,
                                BrandCounterName = item.BrandCounterName,
                                SkmStoreId = item.SkmStoreId,
                                SkmStoreName = item.SkmStoreName,
                                CategoryId = item.CategoryId,
                                CategoryName = item.CategoryName,
                                SubcategoryId = item.SubcategoryId,
                                SubcategoryName = item.SubcategoryName,
                                Floor = item.Floor,
                                SellerGuid = skmSeller.StoreGuid,
                                IsAvailable = true,
                                IsShoppe = true,
                                ParentSellerGuid = skmSeller.SellerGuid ?? Guid.Empty,
                                ModifyDate = now,
                                BrandGuid = item.BrandGuid,
                                SkmStoreGuid = item.StoreGuid,
                            };
                            skmMp.ShoppeSet(shoppe);

                            skmMp.SkmShoppeChangeLogAdd(new SkmShoppeChangeLog
                            {
                                ShopCode = shoppe.ShopCode,
                                BrandCounterCode = shoppe.BrandCounterCode,
                                ChangeMemo = "Create New",
                                PreviousObj = json.Serialize(item),
                                NextObj = json.Serialize(shoppe)
                            });

                            //new seller
                            Seller seller = new Seller()
                            {
                                Guid = sellerGuid,
                                SellerName = item.BrandCounterName,
                                SellerId = SellerFacade.GetNewSellerID(),
                                CreateId = "sys@17life.com",
                                CreateTime = now,
                                Department = (int)DepartmentTypes.Ppon,
                                CityId = config.SkmCityId,
                                IsCloseDown = false
                            };
                            sp.SellerSet(seller);

                            //new sellerTree
                            SellerTree sellerTree = new SellerTree()
                            {
                                SellerGuid = sellerGuid,
                                ParentSellerGuid = skmSeller.StoreGuid ?? Guid.Empty,
                                RootSellerGuid = config.SkmRootSellerGuid,
                                CreateId = "sys@17life.com",
                                CreateTime = now
                            };
                            sp.SellerTreeSet(sellerTree);

                            //new store category
                            int shopcode = 0;
                            StoreCategory sc = new StoreCategory()
                            {
                                StoreGuid = sellerGuid,
                                CategoryCode = int.TryParse(item.ShopId, out shopcode) ? shopcode : 0,
                                Valid = 1,
                                CreateType = (int)LunchKingSite.Core.Enumeration.MrtReleaseshipStoreSettingType.Skm,
                                ModifyId = "sys@17life.com",
                                ModifyTime = now,
                            };
                            sp.StoreCategorySet(sc);
                        }
                    }
                    skmMp.ShoppeCollectionSet(shoppes);

                    foreach (var s in shoppes.ToList().Where(x => x.IsAvailable == false))
                    {
                        skmMp.SkmShoppeChangeLogAdd(new SkmShoppeChangeLog
                        {
                            ShopCode = s.ShopCode,
                            BrandCounterCode = s.BrandCounterCode,
                            ChangeMemo = "disable",
                            PreviousObj = json.Serialize(s),
                            NextObj = string.Empty
                        });
                    }

                    if (sellerCol.Any())
                    {
                        sp.SellerSaveAll(sellerCol);
                    }
                    tran.Complete();
                    result.Code = ApiResultCode.Success;
                    result.Message = "Success";
                }

                SortedDictionary<string, string> serverIPs =
                    ProviderFactory.Instance().GetSerializer().Deserialize<SortedDictionary<string, string>>(config.ServerIPs);
                string clearSellerCacheResult = SystemFacade.SyncCommand(serverIPs, SystemFacade._CHANNEL_SELLER_TREE_CLEAR, string.Empty, string.Empty);
                logger.InfoFormat("SkmResetSellerTreeCache:{0}", clearSellerCacheResult);
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("GatewayShoppe.{0}->{1}", ex.Message, ex.StackTrace);
                result.Code = ApiResultCode.Error;
                result.Message = "Error:" + ex.Message;
            }
            return result;
        }


        #endregion

        #region 策展

        /// <summary>
        /// 策展列表(選物推薦: 全卡別 and 沒有綁任何客群)
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Skm)]
        public ApiResult GetExhibitionEvents(GetExhibitionEventsModel model)
        {

            var result = new List<GetExhibitionEventsResponse>();

            if (!config.IsEnableCRMUserGroup)
            {
                result = GetExhibitionEventResponseData(model.SellerGuid, new List<SkmCardLevelType> { SkmCardLevelType.All });
            }
            else
            {
                result = GetExhibitionEventList(model.SellerGuid);
            }

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = result
            };
        }

        /// <summary>
        /// CRM 入口(會員專屬推薦專區: 符合卡別 / 客群)
        /// </summary>
        /// <param name="model">透過cardType判斷已登入</param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Skm)]
        public ApiResult GetCrmEvents(GetCrmEventsModel model)
        {
            var data = new GetCrmEventsResponse();
            List<SkmCardLevelType> clt = new List<SkmCardLevelType>();
            var rnd = new Random();

            switch (model.CardLevel)
            {
                case 0:
                    clt.Add(SkmCardLevelType.VIP);
                    break;
                case 1:
                    clt.Add(SkmCardLevelType.Centurion);
                    break;
                case 2:
                    clt.Add(SkmCardLevelType.Honor);
                    break;
                case 4:
                    clt.Add(SkmCardLevelType.Silver);
                    break;
                case 5:
                    clt.Add(SkmCardLevelType.Zero);
                    break;
                default:
                    break;
            }

            if (!config.IsEnableCRMUserGroup)
            {
                var exhEvents = GetExhibitionEventResponseData(model.SellerGuid, clt).OrderBy(x => rnd.Next()).ToList();

                if (exhEvents.Any())
                {
                    data.ExhibitionEvents = exhEvents.Count > config.MaxCrmExhibitionCount ? exhEvents.Take(config.MaxCrmExhibitionCount).ToList() : exhEvents;
                }

                List<SkmLatestActivity> latestActivities = SkmFacade.SkmLatestActivityGetBySellerGuid(model.SellerGuid)
                    .Where(x => x.SDate <= DateTime.Now && x.EDate >= DateTime.Now && x.Status == (int)SkmAppStyleStatus.Normal)
                    .OrderBy(x => rnd.Next()).Take(config.MaxCrmDataCount - data.ExhibitionEvents.Count).ToList();

                foreach (var activity in latestActivities)
                {
                    data.LatestActivityList.Add(new LatestActivity
                    {
                        ActivityTitle = activity.ActivityTitle,
                        StoreFloor = activity.StoreFloor,
                        StoreName = activity.StoreName,
                        ImageUrl = config.MediaBaseUrl + "event/" + activity.ImageUrl,
                        LinkUrl = activity.LinkUrl,
                        Date = activity.ActDate.ToString("yyyy/MM/dd"),
                        ActivityId = activity.Id,
                        ActivityDescription = activity.ActivityDescription,
                        MustLogin = activity.IsMustLogin && activity.LinkUrl.ToLower().IndexOf("17life.com", StringComparison.Ordinal) > -1,
                        StartDate = ApiSystemManager.DateTimeToDateTimeString(activity.SDate),
                        EndDate = ApiSystemManager.DateTimeToDateTimeString(activity.EDate),
                    });
                }
            }
            else
            {
                //已take要的則數
                data.ExhibitionEvents = GetCrmExhibitionEvent(model.SellerGuid, clt);

                //策展不夠max則,才需要往下找最新活動
                if (data.ExhibitionEvents.Count < config.MaxCrmDataCount)
                {
                    List<SkmLatestActivity> latestActivities = SkmFacade.SkmLatestActivityGetBySellerGuid(model.SellerGuid)
                        .Where(x => x.SDate <= DateTime.Now && x.EDate >= DateTime.Now && x.Status == (int)SkmAppStyleStatus.Normal)
                        .OrderBy(x => rnd.Next()).Take(config.MaxCrmDataCount - data.ExhibitionEvents.Count).ToList();

                    foreach (var activity in latestActivities)
                    {
                        data.LatestActivityList.Add(new LatestActivity
                        {
                            ActivityTitle = activity.ActivityTitle,
                            StoreFloor = activity.StoreFloor,
                            StoreName = activity.StoreName,
                            ImageUrl = config.MediaBaseUrl + "event/" + activity.ImageUrl,
                            LinkUrl = activity.LinkUrl,
                            Date = activity.ActDate.ToString("yyyy/MM/dd"),
                            ActivityId = activity.Id,
                            ActivityDescription = activity.ActivityDescription,
                            MustLogin = activity.IsMustLogin && activity.LinkUrl.ToLower().IndexOf("17life.com", StringComparison.Ordinal) > -1,
                            StartDate = ApiSystemManager.DateTimeToDateTimeString(activity.SDate),
                            EndDate = ApiSystemManager.DateTimeToDateTimeString(activity.EDate),
                        });
                    }
                }

            }
            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = data
            };
        }

        /// <summary>
        /// 取得策展內容
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Skm)]
        public ApiResult GetExhibitionContent(GetExhibitionDealModel model)
        {
            var exhdata = exp.GetViewExhibitionEvent(model.SellerGuid, model.ExhibitionId);
            if (exhdata == null)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.DataNotFound
                };
            }

            var data = new ExhibitionDealResponse
            {
                ExhibitionName = exhdata.Subject,
                EventType = exhdata.EventType,
                Content = exhdata.EventContent,
                EventUrl = string.Empty,
                Rules = exhdata.EventRule,
                StartDate = ApiSystemManager.DateTimeToDateTimeString(exhdata.StartDate),
                EndDate = ApiSystemManager.DateTimeToDateTimeString(exhdata.EndDate),
                MainBanner = SkmFacade.SkmAppStyleImgPath(exhdata.BannerPic),
                CategoryDeals = new List<SkmExhibitionCategoryDeal>(),
                TopDeals = new List<SKMDealSynopsis>(),
                Activities = new List<SkmExhibitionActivity>()
            };

            #region 取檔次

            if (exhdata.EventType == (byte)SkmExhibitionEventType.DealExhibition)
            {
                var categoryData = exp.GetExhibitionCategoryList(exhdata.ExhibitionEventId);
                var dealIconData = SkmFacade.GetDealIconModelList();

                //會員收藏
                List<Guid> memberCollectDealBids = new List<Guid>();
                if (HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    int memberUnique = MemberFacade.GetUniqueId(HttpContext.Current.User.Identity.Name);
                    memberCollectDealBids = mp.MemberCollectDealGetByMemberUniqueId(memberUnique).Select(p => p.BusinessHourGuid).ToList();
                }

                foreach (var cat in categoryData)
                {
                    var timeSlotData = exp.GetExhibitionDealTimeSlot(
                        DateTime.Parse(DateTime.Now.ToString("yyyy/MM/dd")), model.SellerGuid,
                        exhdata.ExhibitionEventId, cat.Id);
                    int counter = 1;

                    var exhibitionDeals = new SkmExhibitionCategoryDeal
                    {
                        CategoryName = cat.Name,
                        Deals = new List<SKMDealSynopsis>()
                    };

                    foreach (var slot in timeSlotData)
                    {
                        if (slot.IsHidden)
                        {
                            continue;
                        }

                        var ved = pp.ViewExternalDealGetListByDealGuid(slot.ExternalGuid, model.SellerGuid);
                        if (ved == null || !ved.IsLoaded || ved.Bid == null)
                        {
                            continue;
                        }

                        var deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid((Guid)ved.Bid);
                        if (!deal.IsLoaded)
                        {
                            continue;
                        }

                        if (!DateTime.Now.IsBetween(deal.BusinessHourOrderTimeS, deal.BusinessHourOrderTimeE))
                        {
                            continue;
                        }

                        if (ved.Status != (int)SKMDealStatus.Confirmed && ved.Status != (int)SKMDealStatus.OnProduct)
                        {
                            continue;
                        }
                        var be = skmMp.SkmBurningEventGet(ved.Guid, ved.DealVersion >= (int)ExternalDealVersion.SkmPay ? ved.ShopCode : string.Empty, false);
                        if (be.IsLoaded && be.Status != (byte)SkmBurningEventStatus.Create)
                        {
                            continue;
                        }

                        var newDeal = GetSKMDealSynopsis(ved, deal, dealIconData, counter, be);
                        //收藏
                        if (memberCollectDealBids.Contains(newDeal.Bid))
                        {
                            newDeal.IsCollected = true;
                        }

                        exhibitionDeals.Deals.Add(newDeal);
                        var exhDeal = exp.GetExhibitionDeal(cat.Id, slot.ExternalGuid);
                        if (exhDeal.IsMainDeal)
                        {
                            data.TopDeals.Add(newDeal);
                        }
                        counter++;
                    }

                    if (exhibitionDeals.Deals.Any())
                    {
                        data.CategoryDeals.Add(exhibitionDeals);
                    }
                }
            }

            if (exhdata.EventType == (byte)SkmExhibitionEventType.MultiEvent)
            {
                var actData = exp.GetExhibitionActivities(model.ExhibitionId);
                if (actData.Any())
                {
                    foreach (var d in actData)
                    {
                        string tempUrl;
                        var sea = new SkmExhibitionActivity
                        {
                            BannerUrl = SkmFacade.SkmAppStyleImgPath(d.BannerPic),
                            IsRsvLogin = IsRsvUrl(d.EventLink, out tempUrl),
                            ActivityUrl = tempUrl
                        };

                        data.Activities.Add(sea);
                    }
                }
            }

            #endregion

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = data
            };
        }

        /// <summary>
        /// 重置ExhibitionDealTimeSlot
        /// </summary>
        /// <param name="exhibitionEventId"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.SystemManagement)]
        public ApiResult ResetExhibitionDealTimeSlot(int exhibitionEventId)
        {
            int executeCount = 0;
            var exhibitionEvent = exp.GetExhibitionEvent(exhibitionEventId);

            if (exhibitionEvent.EventType != (byte)SkmExhibitionEventType.DealExhibition)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.Success,
                    Data = 0,
                    Message = "非商品類型策展"
                };
            }

            var sellerData = exp.GetExhibitionStore(exhibitionEventId);
            var categoryData = exp.GetExhibitionCategoryList(exhibitionEventId);

            foreach (var c in categoryData)
            {
                var deals = exp.GetExhibitionDealList(c.Id);
                foreach (var d in deals)
                {
                    foreach (var s in sellerData)
                    {
                        SkmFacade.SetExhibitionDealTimeSlot(s.StoreGuid, c.EventId, c.Id, d.ExternalDealGuid,
                            exhibitionEvent.StartDate, exhibitionEvent.EndDate);
                        executeCount++;
                    }
                }
            }

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = executeCount
            };
        }

        /// <summary>
        /// 測試餐廳URL
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.SystemManagement)]
        public string GetRsvUrl(TestRsv url)
        {
            var result = url.Url;
            IsRsvUrl(url.Url, out result);
            return result;
        }

        #endregion 策展

        #region SKM PAY

        /// <summary>
        /// 取得SKM PAY Banner 列表(圖片連結與ID)
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Skm)]
        public ApiResult GetSkmPayBanner()
        {
            var resultData = new List<SkmPayBannerRespose>();
            var bannerList = skmEfp.GetSkmPayBannerToday();
            if (bannerList.Any())
            {
                resultData.AddRange(bannerList.Select(x => new SkmPayBannerRespose(x)));
            }

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = resultData
            };
        }

        /// <summary>
        /// 取得SKM PAY Banner 內文
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [OAuthScope(TokenScope.Skm)]
        public ApiResult GetSkmPayBannerDetail(int id)
        {
            var data = skmEfp.GetSkmPayBanner(id);
            if (data.IsHidden || !DateTime.Now.IsBetween(data.StartTime, data.EndTime))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.DataNotFound
                };
            }

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = new SkmPayBannerDetailResponse
                {
                    Name = data.Name,
                    BannerUrl = SkmFacade.SkmAppStyleImgPath(data.BannerPath),
                    Content = data.ContentHtml
                }
            };
        }

        /// <summary>
        /// 依傳入的日期上傳當日
        /// </summary>
        /// <param name="execDate"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.SystemManagement)]
        public ApiResult ClearPos(DateTime execDate)
        {
            SkmFacade.SkmPayClearPosByDate(execDate);
            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = null
            };
        }

        #endregion SKM PAY

        #region Deep Link
        /// <summary>
        /// 取得可顯示的廣告清單
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ApiResult LunchAds()
        {
            return new ApiResult()
            {
                Code = ApiResultCode.Success,
                Data = SkmFacade.GetOnlineAdvertisingBoards()
            };
        }

        #endregion

        #region Common

        /// <summary>
        /// 取得 Skm App 圖檔
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ApiResult GetImages()
        {
            var imgs = new JsonSerializer().Deserialize<List<SkmHomeImage>>(config.SkmImages);

            foreach (var ig in imgs)
            {
                ig.ImgUrl = string.Format("{0}?v={1}", ig.ImgUrl, Guid.NewGuid());
            }

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = imgs
            };
        }

        /// <summary>
        /// 取得 Skm App 圖檔
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ApiResult GetRsvHomeUrl(string sellerGuid)
        {
            string inlineUrl = string.Empty;
            if (GetRsvUrl(config.SkmRsvUrl, sellerGuid, out inlineUrl))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.Success,
                    Data = inlineUrl
                };
            }

            return new ApiResult
            {
                Code = ApiResultCode.Error
            };

        }

        /// <summary>
        /// 組成RsvUrl
        /// </summary>
        /// <param name="rootUrl"></param>
        /// <param name="sellerGuid"></param>
        /// <returns></returns>
        private bool GetRsvUrl(string rootUrl, string sellerGuid, out string inlineUrl)
        {
            inlineUrl = string.Empty;
            var sg = Guid.Parse(sellerGuid);
            var shopCodes = skmMp.SkmShoppeGetShopCodeBySellerGuid(sg);
            int storeShopCode = 0;

            foreach (var sc in shopCodes)
            {
                int s = int.Parse(sc);
                storeShopCode = s / 10 * 10;
            }

            var rsvData = new JsonSerializer().Deserialize<List<RsvHomeUrl>>(config.SkmRsvData);

            var rsv = rsvData.FirstOrDefault(x => x.ShopCode == storeShopCode.ToString());
            if (rsv == null)
            {
                return false;
            }

            inlineUrl = string.Format("{0}{1}", rootUrl, rsv.Url);
            string result;
            if (IsRsvUrl(inlineUrl, out result))
            {
                inlineUrl = string.Format("{0}?T1103={1}", inlineUrl, result);
                return true;
            }
            return false;
        }


        /// <summary>
        /// 取SKM 美食地圖
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ApiResult GetAllRsvHomeUrl()
        {

            List<RsvHomeUrl> rsvHomes = new List<RsvHomeUrl>();

            rsvHomes.Add(new RsvHomeUrl
            {
                ShopCode = "110",
                Url = "/groups/skm-TaipeiStation"
            });

            rsvHomes.Add(new RsvHomeUrl
            {
                ShopCode = "100",
                Url = "/groups/skm-nanxi"
            });

            rsvHomes.Add(new RsvHomeUrl
            {
                ShopCode = "150",
                Url = "/groups/skm-TaipeiStation"
            });

            rsvHomes.Add(new RsvHomeUrl
            {
                ShopCode = "160",
                Url = "/groups/skm-TaoyuanDayou"
            });

            rsvHomes.Add(new RsvHomeUrl
            {
                ShopCode = "170",
                Url = "/groups/skm-TaoyuanStation"
            });

            rsvHomes.Add(new RsvHomeUrl
            {
                ShopCode = "200",
                Url = "/groups/skm-taichung"
            });

            rsvHomes.Add(new RsvHomeUrl
            {
                ShopCode = "330",
                Url = "/groups/skm-ChiayiChuiyang"
            });

            rsvHomes.Add(new RsvHomeUrl
            {
                ShopCode = "310",
                Url = "/groups/skm-TainanZhongshan"
            });

            rsvHomes.Add(new RsvHomeUrl
            {
                ShopCode = "320",
                Url = "/groups/skm-tainanplace"
            });

            rsvHomes.Add(new RsvHomeUrl
            {
                ShopCode = "300",
                Url = "/groups/skm-KaohsiungSanduo"
            });

            rsvHomes.Add(new RsvHomeUrl
            {
                ShopCode = "350",
                Url = "/groups/skm-KaohsiungZuoying"
            });

            rsvHomes.Add(new RsvHomeUrl
            {
                ShopCode = "120",
                Url = "/groups/skm-xinyi"
            });

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = rsvHomes
            };
        }

        #endregion Common

        #region Misc

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.SystemManagement)]
        public ApiResult GetSkmValidateCode(SkmValidateCodeModel model)
        {
            SkmValidateCodeResult result = new SkmValidateCodeResult();
            if (model.QrStr.Substring(0, 4) == "LIFE")
            {
                var base64EncodedBytes = System.Convert.FromBase64String(model.QrStr.Replace("LIFE", ""));
                var json = System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
                SkmQrcodeLite qr1 = new JsonSerializer().Deserialize<SkmQrcodeLite>(json);

                result.ValidateCodeSource = qr1.GetSkmValidateCode();
            }
            else
            {
                var base64EncodedBytes = System.Convert.FromBase64String(model.QrStr.Replace("LIFE2", ""));
                var json = System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
                SkmQrcodeLiteGetByFree qr2 = new JsonSerializer().Deserialize<SkmQrcodeLiteGetByFree>(json);

                result.ValidateCodeSource = qr2.GetSkmValidateCode();
            }
            result.ValidateCode = new Security(SymmetricCryptoServiceProvider.DefaultMD5)
                .Encrypt(result.ValidateCodeSource).Substring(0, 4);

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = result
            };
        }

        /// <summary>
        /// 回傳範本
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ApiResult<GroupDeal> Sample()
        {
            return new ApiResult<GroupDeal>
            {
                Code = ApiResultCode.Success,
                Message = "Hello World.",
                Data = new GroupDeal
                {
                    Type = 3,
                    DealList = new List<SKMDealSynopsis>(new SKMDealSynopsis[]
                    {
                        new SKMDealSynopsis(new SkmPponDealSynopsis
                        {
                            Bid = Guid.NewGuid(),
                            TravelPlace = "對面全家"
                        }),
                        new SKMDealSynopsis(new SkmPponDealSynopsis
                        {
                            Bid = Guid.NewGuid(),
                            TravelPlace = "元吉屋"
                        })
                    })
                }
            };
        }

        /// <summary>
        /// ap
        /// </summary>
        /// <returns></returns>
        public ApiResult GetIconStyle()
        {
            var iconStyles = new List<ExternalDealIconStyle>();
            string[] bgcolor = { "#D0021B", "#616EB8", "#EB5E02", "#89B52D", "#1F87BC" };
            var index = 0;
            foreach (var c in bgcolor)
            {
                iconStyles.Add(new ExternalDealIconStyle
                {
                    BackgroundColor = c,
                    ForeColor = "#fff",
                    StyleId = index + 1
                });
                index++;
            }

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = iconStyles
            };
        }

        /// <summary>
        /// 
        /// </summary>
        public override HelperDocumentSetting DocumentSetting
        {
            get
            {
                return new HelperDocumentSetting
                {
                    Title = "SKM新光API規格參考",
                    Version = "0.1",
                    UrlRegex = new Regex(@"api\/[\S]*", RegexOptions.IgnoreCase | RegexOptions.Compiled)
                };
            }
        }

        /// <summary>
        /// 回傳ApiResultCode
        /// </summary>
        /// <returns></returns>
        public ApiResult GetAllResult()
        {
            List<string> resultData = new List<string>();

            foreach (ApiResultCode ar in Enum.GetValues(typeof(ApiResultCode)))
            {
                resultData.Add((int)ar + "," + ar + "," + LunchKingSite.Core.Helper.GetLocalizedEnum(ar));
            }

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = resultData
            };
        }

        #endregion

        #region method

        private Pokeball GetPokeBall(SkmPrePokeball prePokeBall)
        {
            Pokeball pokeBall = new Pokeball();
            pokeBall.MessageId = prePokeBall.MessageId.ToString();
            pokeBall.Message = prePokeBall.Subject;

            switch (prePokeBall.EventType)
            {
                case (int)EventType.SubEvent:
                    pokeBall.Type = Helper.GetEnumDescription(PokeballActionType.GroupMessage);
                    break;
                case (int)EventType.Url:
                    pokeBall.Type = Helper.GetEnumDescription(PokeballActionType.FullscreenWebView);
                    pokeBall.Url = prePokeBall.ActionUrl;
                    break;
                case (int)EventType.Message:
                    return null;
                case (int)EventType.Bid:
                    pokeBall.Type = Helper.GetEnumDescription(PokeballActionType.ShowDealDetail);
                    pokeBall.Bid = prePokeBall.ActionBid.ToString();
                    break;
                case (int)EventType.SkmPayMsg:
                    var pokeBallActionType = GetSkmPayMsgPokeBallActionType(prePokeBall.MsgType);
                    pokeBall.Type = Helper.GetEnumDescription(pokeBallActionType);
                    break;
                case (int)EventType.OrderListCanceled:
                case (int)EventType.OrderListNotVerified:
                case (int)EventType.OrderListVerified:
                case (int)EventType.CouponListNotVerified:
                case (int)EventType.CouponListVerified:
                    pokeBall.Type = Helper.GetEnumDescription(PokeballActionType.DeepLink);
                    if (Enum.IsDefined(typeof(EventType), prePokeBall.EventType))
                    {
                        pokeBall.Url = Helper.GetEnumDescription((EventType)prePokeBall.EventType);
                    }
                    break;
                default:
                    break;
            }

            return pokeBall;
        }

        private PokeballActionType GetSkmPayMsgPokeBallActionType(byte type)
        {

            switch (type)
            {
                case (byte)SkmInAppMessageType.MakeOrder:
                    return PokeballActionType.OrderListUnused;

                case (byte)SkmInAppMessageType.ReturnOrder:
                    return PokeballActionType.OrderListReturn;

                default:
                    return PokeballActionType.None;
            }

        }

        private SKMDealSynopsis GetSKMDealSynopsis(ViewExternalDeal ved, IViewPponDeal deal, List<ExternalDealIconModel> dealIconData, int counter, SkmBurningEvent be
            , List<CategoryDeal> categoryDeals = null, DealProperty dp = null)
        {
            categoryDeals = categoryDeals ?? pp.CategoryDealsGetList(deal.BusinessHourGuid).ToList();

            SkmPponDeal skmDeal =
                dp == null ? PponDealApiManager.CreateSkmOrderPponDeal(deal)
                    : PponDealApiManager.CreateSkmOrderPponDeal(deal, dp);

            SKMDealSynopsis newDeal = new SKMDealSynopsis(skmDeal, deal, categoryDeals, deal.BusinessHourGuid, counter)
            {
                StoreName = ved.StoreName,
                Shop = ved.ShopName,
                IsBurningEvent = be.IsLoaded,
                BurningPoint = be.IsLoaded ? be.BurningPoint : ved.IsSkmPay ? ved.SkmPayBurningPoint : 0,
                Buy = ved.Buy ?? 0,
                Free = ved.Free ?? 0,
                IsSkmPay = ved.IsSkmPay,
                OrderedQuanitity=deal.OrderedQuantity.HasValue==true?deal.OrderedQuantity.Value:0,
                Ph=ved.Ph
            };

            var tempIcon = dealIconData.Where(x => x.Id == ved.DealIconId).ToList();
            newDeal.IconInfo = tempIcon.Any() ? new SkmDealIcon(tempIcon.First()) : new SkmDealIcon();
            return newDeal;
        }

        private bool IsRsvUrl(string url, out string rsvUrl)
        {
            var isRsv = false;
            rsvUrl = url;
            var replaceUrl = string.Empty;
            List<string> inLineUrl = new List<string>
            {
                "https://rsv.skm.com.tw",
                "http://skm-inline-beta.s3-website-us-east-1.amazonaws.com"
            };

            foreach (var s in inLineUrl)
            {
                if (url.ToLower().IndexOf(s, StringComparison.Ordinal) > -1)
                {
                    isRsv = true;
                    replaceUrl = s;
                    break;
                }
            }

            if (!isRsv) return false;

            var ru = new Rsv(url.Replace(replaceUrl, ""));
            rsvUrl = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(new JsonSerializer().Serialize(ru)));
            return true;
        }


        /// <summary>
        /// CRM策展
        /// (全卡別無法觀看以外, 所有策展類型都可觀看, 只要符合卡別or客群, 照下列規則抓取)
        /// 1. 先找符合某一卡別(非全卡別) and 客群名單先的策展們
        /// 2. 若找不到才找僅符合某一卡別的策展們˙
        /// 3. 至多找5筆,不足5筆找最新活動湊
        /// </summary>
        /// <param name="sellerGuid"></param>
        /// <param name="cardLevels"></param>
        /// <returns></returns>
        private List<GetExhibitionEventsResponse> GetCrmExhibitionEvent(Guid sellerGuid, List<SkmCardLevelType> cardLevels)
        {
            if (!cardLevels.Any())
            {
                return new List<GetExhibitionEventsResponse>();
            }

            var responseData = new List<GetExhibitionEventsResponse>();

            #region 1. 先尋找符合卡別&客群的

            var skmMember = skmMp.MemberGetByUserId(User.Identity.Id);
            if (!string.IsNullOrEmpty(skmMember.SkmToken))
            {
                var eventAndUserGroup = exp.GetViewExhibitionEventHaveUserGroupList(sellerGuid, cardLevels, skmMember.SkmToken);

                foreach (var e in eventAndUserGroup)
                {
                    GetExhibitionEventsResponse d = new GetExhibitionEventsResponse();
                    if (CheckExhibitionContentIsQualified(e.ExhibitionEventId, e.EventType, sellerGuid) == false)
                    {
                        continue;
                    }

                    d.ExhibitionEventId = e.ExhibitionEventId;
                    d.BannerImgUrl = SkmFacade.SkmAppStyleImgPath(e.BannerPic);
                    d.EventType = e.EventType;
                    d.EventUrl = e.BannerLink;
                    d.StartDate = ApiSystemManager.DateTimeToDateTimeString(e.StartDate);
                    d.EndDate = ApiSystemManager.DateTimeToDateTimeString(e.EndDate);
                    responseData.Add(d);

                    //已達MaxCrmDataCount則則跳出迴圈不要再多做
                    if (responseData.Count == config.MaxCrmDataCount)
                        break; ;
                }
            }

            #endregion

            #region 2. 找不齊 max則, 則再找僅符合卡別的

            if (responseData.Count < config.MaxCrmDataCount)
            {
                //只符合卡別的(排除掉有客群的)
                var events = exp.GetViewExhibitionEventNoUserGroupList(sellerGuid, cardLevels);

                if (events.Any())
                {
                    events = events.OrderBy(x => x.Seq).ToList();
                }

                foreach (var e in events)
                {
                    GetExhibitionEventsResponse d = new GetExhibitionEventsResponse();

                    if (CheckExhibitionContentIsQualified(e.ExhibitionEventId, e.EventType, sellerGuid) == false)
                    {
                        continue;
                    }

                    d.ExhibitionEventId = e.ExhibitionEventId;
                    d.BannerImgUrl = SkmFacade.SkmAppStyleImgPath(e.BannerPic);
                    d.EventType = e.EventType;
                    d.EventUrl = e.BannerLink;
                    d.StartDate = ApiSystemManager.DateTimeToDateTimeString(e.StartDate);
                    d.EndDate = ApiSystemManager.DateTimeToDateTimeString(e.EndDate);
                    responseData.Add(d);

                    //已達MaxCrmDataCount則則跳出迴圈不要再多做
                    if (responseData.Count == config.MaxCrmDataCount)
                        break; ;
                }
            }

            #endregion

            #region 3. 找不齊 max則, 則再找僅符合客群的(會是全卡別)

            if (responseData.Count < config.MaxCrmDataCount)
            {
                //只符合客群的 (會是全卡別)
                var eventAndUserGroupButAllCard = exp.GetViewExhibitionEventHaveUserGroupList(sellerGuid, new List<SkmCardLevelType> { SkmCardLevelType.All }, skmMember.SkmToken);

                if (eventAndUserGroupButAllCard.Any())
                {
                    eventAndUserGroupButAllCard = eventAndUserGroupButAllCard.OrderBy(x => x.Seq).ToList();
                }

                foreach (var e in eventAndUserGroupButAllCard)
                {
                    GetExhibitionEventsResponse d = new GetExhibitionEventsResponse();

                    if (CheckExhibitionContentIsQualified(e.ExhibitionEventId, e.EventType, sellerGuid) == false)
                    {
                        continue;
                    }

                    d.ExhibitionEventId = e.ExhibitionEventId;
                    d.BannerImgUrl = SkmFacade.SkmAppStyleImgPath(e.BannerPic);
                    d.EventType = e.EventType;
                    d.EventUrl = e.BannerLink;
                    d.StartDate = ApiSystemManager.DateTimeToDateTimeString(e.StartDate);
                    d.EndDate = ApiSystemManager.DateTimeToDateTimeString(e.EndDate);
                    responseData.Add(d);

                    //已達MaxCrmDataCount則則跳出迴圈不要再多做
                    if (responseData.Count == config.MaxCrmDataCount)
                        break;
                    ;
                }
            }

            #endregion

            return responseData;
        }

        private void ReSetModel()
        {

        }

        /// <summary>
        /// 商品策展必須得有檔次不然會被remove / 若多重策展必須得有活動不然會被remove
        /// </summary>
        /// <param name="eventId">策展編號</param>
        /// <param name="eventType">商品/多重</param>
        /// <param name="sellerGuid">分店編號</param>
        /// <returns></returns>
        private bool CheckExhibitionContentIsQualified(int eventId, int eventType, Guid sellerGuid)
        {
            int dealCount = 0;
            if (eventType == (byte)SkmExhibitionEventType.DealExhibition)
            {
                dealCount = exp.GetExhibitionDealCount(eventId, sellerGuid);
            }
            else if (eventType == (byte)SkmExhibitionEventType.MultiEvent)
            {
                dealCount = exp.GetExhibitionActivityCount(eventId);
            }
            else //單一活動,訊息策展免檢查, 預設dealCount=1直接符合
            {
                return true;
            }

            return dealCount > 0;//>0才會符合
        }

        /// <summary>
        /// 選物推薦 取得策展邏輯 (不限則數)
        /// 1.商品策展- 勾選全卡別 && 不含客群
        /// 2.多重活動- 勾選全卡別 && 不含客群
        /// (因單一跟多重策展類型不能勾選全卡別)
        /// </summary>
        /// <param name="sellerGuid"></param>
        /// <returns></returns>
        private List<GetExhibitionEventsResponse> GetExhibitionEventList(Guid sellerGuid)
        {
            //基本上只有商品策展跟多重活動在後台可以勾全卡別 && 不含客群
            var events = exp.GetViewExhibitionEventNoUserGroupList(
                sellerGuid, new List<SkmCardLevelType>() { SkmCardLevelType.All });

            if (events.Any())
            {
                events = events.OrderBy(x => x.Seq).ToList();
            }
            var responseData = new List<GetExhibitionEventsResponse>();

            foreach (var e in events)
            {
                GetExhibitionEventsResponse d = new GetExhibitionEventsResponse();
                if (CheckExhibitionContentIsQualified(e.ExhibitionEventId, e.EventType, sellerGuid) == false)
                {
                    continue;
                }

                d.ExhibitionEventId = e.ExhibitionEventId;
                d.BannerImgUrl = SkmFacade.SkmAppStyleImgPath(e.BannerPic);
                d.EventType = e.EventType;
                d.EventUrl = e.BannerLink;
                d.ExhibitionEventName = e.Subject;
                d.StartDate = ApiSystemManager.DateTimeToDateTimeString(e.StartDate);
                d.EndDate = ApiSystemManager.DateTimeToDateTimeString(e.EndDate);
                responseData.Add(d);
            }

            return responseData;
        }

        /// <summary>
        /// 舊寫法
        /// </summary>
        /// <param name="sellerGuid"></param>
        /// <param name="cardLevels"></param>
        /// <returns></returns>
        private List<GetExhibitionEventsResponse> GetExhibitionEventResponseData(Guid sellerGuid, List<SkmCardLevelType> cardLevels)
        {
            if (!cardLevels.Any())
            {
                return new List<GetExhibitionEventsResponse>();
            }

            var events = cardLevels.Count == 1 ? exp.GetViewExhibitionEvent(sellerGuid, cardLevels.First(), SkmExhibitionEventStatus.Published)
                : exp.GetViewExhibitionEvent(sellerGuid, cardLevels, SkmExhibitionEventStatus.Published);

            var data = new List<GetExhibitionEventsResponse>();

            if (events.Any())
            {
                events = events.OrderBy(x => x.Seq).ToList();
            }

            foreach (var e in events)
            {
                data.Add(new GetExhibitionEventsResponse
                {
                    ExhibitionEventId = e.ExhibitionEventId,
                    BannerImgUrl = SkmFacade.SkmAppStyleImgPath(e.BannerPic),
                    EventType = e.EventType,
                    DealCount = e.EventType == (byte)SkmExhibitionEventType.DealExhibition ? exp.GetExhibitionDealCount(e.ExhibitionEventId, sellerGuid)
                        : exp.GetExhibitionActivityCount(e.ExhibitionEventId),
                    EventUrl = e.BannerLink,
                    ExhibitionEventName = e.Subject,
                    StartDate = ApiSystemManager.DateTimeToDateTimeString(e.StartDate),
                    EndDate = ApiSystemManager.DateTimeToDateTimeString(e.EndDate),
                });
            }

            List<SkmExhibitionEventType> checkType = new List<SkmExhibitionEventType>
            {
                SkmExhibitionEventType.DealExhibition,
                SkmExhibitionEventType.MultiEvent
            };

            //僅只是防呆,不要誤會成直接把商品/多重活動策展直接remove
            //若商品策展必須得有檔次不然會被remove / 若多重策展必須得有活動不然會被remove
            data.RemoveAll(x => checkType.Contains((SkmExhibitionEventType)x.EventType) && x.DealCount == 0);
            return data;
        }

        /// <summary>
        /// 分頁
        /// </summary>
        /// <param name="deals"></param>
        /// <param name="startIndex"></param>
        /// <returns></returns>
        private IEnumerable<SKMDealSynopsis> PagingSkmDealSynopsis(List<SKMDealSynopsis> deals, int startIndex = 0)
        {
            var totalCount = deals.Count;

            if (config.SkmDealPageSize >= totalCount)
            {
                return deals;
            }

            if (startIndex == 0 || (totalCount > (startIndex + config.SkmDealPageSize)))
            {
                return deals.Skip(startIndex).Take(config.SkmDealPageSize);
            }

            if (totalCount > startIndex)
            {
                return deals.Skip(startIndex);
            }

            return new List<SKMDealSynopsis>();
        }

        /// <summary>
        /// 檔次排序
        /// </summary>
        /// <param name="deals"></param>
        /// <param name="sortType"></param>
        /// <param name="dealType">優惠/注目/商品 預設都會帶入優惠</param>
        /// <returns></returns>
        private IEnumerable<SKMDealSynopsis> SortSkmDealSynopsis(List<SKMDealSynopsis> deals, SkmCategorySortType sortType, SkmDealTypes dealType)
        {
            IEnumerable<SKMDealSynopsis> rtn;
            switch (sortType)
            {
                case SkmCategorySortType.TopNews:
                    rtn = deals.OrderByDescending(x => ApiSystemManager.DateTimeFromDateTimeString(x.DealStartTime)).ThenBy(x => x.Seq);
                    break;
                case SkmCategorySortType.TopOrderTotal:
                    rtn = deals.OrderByDescending(x => x.SoldNumShow);
                    break;
                case SkmCategorySortType.DiscountAsc:
                    rtn = deals.OrderBy(x => x.Discount / (x.OriPrice != 0 ? x.OriPrice : 1));
                    break;
                case SkmCategorySortType.PriceDesc:
                    rtn = deals.OrderByDescending(x => x.PriceBySort).ThenByDescending(x => x.ExchangePrice);
                    break;
                case SkmCategorySortType.PriceAsc:
                    rtn = deals.OrderBy(x => x.PriceBySort).ThenBy(x => x.ExchangePrice);
                    break;
                case SkmCategorySortType.MemberCollectAsc:
                    rtn = deals.OrderBy(x => x.DealMemberCollectCount);
                    break;
                case SkmCategorySortType.MemberCollectDesc:
                    rtn = deals.OrderByDescending(x => x.DealMemberCollectCount);
                    break;
                case SkmCategorySortType.BusinessHourOrderTimeEndAsc:
                    rtn = deals.OrderBy(x => ApiSystemManager.DateTimeFromDateTimeString(x.DealEndTime)).ThenBy(x => x.Seq);
                    break;
                case SkmCategorySortType.BusinessHourOrderTimeEndDesc:
                    rtn = deals.OrderByDescending(x => ApiSystemManager.DateTimeFromDateTimeString(x.DealEndTime)).ThenBy(x => x.Seq);
                    break;
                case SkmCategorySortType.BurningPointDesc:
                    rtn = deals.OrderByDescending(x => x.BurningPoint).ThenBy(x => x.Seq);
                    break;
                case SkmCategorySortType.BurningPointAsc:
                    rtn = deals.OrderBy(x => x.BurningPoint).ThenBy(x => x.Seq);
                    break;
                default:
                    rtn = deals.OrderBy(x => x.Seq);
                    break;
            }
            return rtn;
        }

        private List<IViewPponDeal> GetSkmViewPponDeal(Guid sellerGuid, ref List<SkmDealTimeSlot> timeSlots)
        {
            List<IViewPponDeal> ppdeals = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(timeSlots.Select(x => x.BusinessHourGuid).Distinct().ToList())
                .Where(x => x.SellerGuid == sellerGuid || x.SellerGuid == config.SkmRootSellerGuid).ToList();

            //過期未清算檔次
            var timeoutDeal = ppdeals.Where(x => x.SettlementTime != null && x.SettlementTime < DateTime.Now).Select(x => x.BusinessHourGuid).ToList();
            ppdeals = ppdeals.Where(x => !timeoutDeal.Contains(x.BusinessHourGuid)).ToList();
            //過濾 timeSlots 資料
            var onDeal = ppdeals.Select(x => x.BusinessHourGuid).ToList();
            timeSlots = timeSlots.Where(x => onDeal.Contains(x.BusinessHourGuid)).ToList();
            return ppdeals;
        }

        private bool CheckDealSynopsesFail(SkmDealTypes dealType, ViewExternalDeal ved, IViewPponDeal deal, List<SkmDealTimeSlot> timeSlots, SkmDealTimeSlot slot, DealOutputType outputType, SkmBurningEvent be)
        {
            if (!ved.IsLoaded || deal == null)
            {
                return true;
            }

            //專屬推薦不輸出
            if (ved.IsCrmDeal)
            {
                return true;
            }

            //抽獎活動獎品不輸出
            if (ved.IsPrizeDeal)
            {
                return true;
            }

            if (outputType == DealOutputType.OldSchool && ved.DealVersion != (int)ExternalDealVersion.Burning)
            {
                return true;
            }

            if (outputType == DealOutputType.SkmPay && !ved.IsSkmPay)
            {
                return true;
            }

            if (outputType == DealOutputType.Default && ved.IsSkmPay)
            {
                return true;
            }

            //是否屬於該類別
            string[] taglist = new JsonSerializer().Deserialize<string[]>(ved.TagList);
            if (!taglist.Contains(((int)dealType).ToString()))
            {
                return true;
            }

            //排除檔次過期
            if (deal.BusinessHourOrderTimeE < DateTime.Now)
            {
                return true;
            }

            List<SkmDealTimeSlot> dts = timeSlots.Where(x => x.BusinessHourGuid == slot.BusinessHourGuid && x.EffectiveStart == DateTime.Parse(DateTime.Now.ToString("yyyy/MM/dd"))).ToList();
            if (dts.Count > 0)
            {
                var skmDealTimeSlot = dts.FirstOrDefault();
                if (skmDealTimeSlot != null && skmDealTimeSlot.Status == (int)DealTimeSlotStatus.NotShowInPponDefault)
                {
                    return true;
                }
            }
            else
            {
                return true;
            }

            if (be.IsLoaded && be.Status != (byte)SkmBurningEventStatus.Create)
            {
                return true;
            }

            return false;
        }


        /// <summary>
        /// GetAppDealName
        /// </summary>
        /// <param name="couponUsage">couponUsage</param>
        /// <param name="appTitle">appTitle</param>
        /// <returns></returns>
        private static string GetAppDealName(string couponUsage, string appTitle)
        {
            string rtn = couponUsage;
            if (!string.IsNullOrWhiteSpace(appTitle))
            {
                rtn = appTitle;
            }
            return rtn;
        }

        private static SkmBeaconMessage GetFilterSkmBeaconMessage(int actionEventInfoId, int memberUserId, int major, int minor)
        {
            //取得Beacon裝置對應資訊
            var triggerBeaconDevice = mp.ActionEventDeviceInfoByMajorMinorGet(actionEventInfoId, major, minor);
            if (triggerBeaconDevice == null)
            {
                return new SkmBeaconMessage();
            }

            List<SkmBeaconMessage> sortBeaconMessageList = new List<SkmBeaconMessage>();

            //UserPushRecord

            DevicePushRecordCollection devicePushRecords = lp.DevicePushRecordListGet(0, memberUserId);

            var pushedActionIdList = devicePushRecords.Select(x => x.ActionId).ToList();

            //SkmBeaconMessages

            SkmBeaconMessageCollection skmCols = skmMp.SkmBeaconMessageGetList("", SkmBeaconMessage.Columns.EffectiveStart + " <= " + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), SkmBeaconMessage.Columns.EffectiveEnd + " > " + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));

            List<SkmBeaconMessage> skmBeaconMessageList = skmCols.Where(x => DateTime.Parse(x.TimeStart).TimeOfDay <= DateTime.Now.TimeOfDay && DateTime.Now.TimeOfDay <= DateTime.Parse(x.TimeEnd).TimeOfDay).ToList();
            // && x.Beacon != null && x.Beacon.ToString().IndexOf(triggerBeaconDeviceName) > 0
            List<SkmBeaconMessage> tempskmBeaconMessageList = new List<SkmBeaconMessage>();
            foreach (var skmBeaconMessage in skmBeaconMessageList.Where(x => x.Beacon != null))
            {

                string[] beaconIds = new JsonSerializer().Deserialize<string[]>(skmBeaconMessage.Beacon);
                if (config.SkmBeaconChecked)
                {
                    if (beaconIds.Contains(triggerBeaconDevice.Major.ToString() + triggerBeaconDevice.Minor.ToString()))
                    {
                        tempskmBeaconMessageList.Add(skmBeaconMessage);
                    }
                }
                else
                {
                    //測試期間不綁定特定Beacon
                    tempskmBeaconMessageList.Add(skmBeaconMessage);
                }
            }
            skmBeaconMessageList = tempskmBeaconMessageList;


            var skmBeaconTimeSlotCol = skmMp.SkmBeaconMessageTimeSlotGetList("", SkmBeaconMessageTimeSlot.Columns.EffectiveStart + " <= " + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), SkmBeaconMessageTimeSlot.Columns.EffectiveEnd + " > " + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));

            List<string> skmBeaconGroupMemberIDList = new List<string>();
            var skmMember = skmMp.MemberGetByUserId(memberUserId);

            //reSort順序
            //取置頂排序
            foreach (var skmBeaconTimeSlot in skmBeaconTimeSlotCol.Where(x => x.BeaconType == (int)SkmBeaconType.Top && x.Status == (int)DealTimeSlotStatus.Default).OrderBy(z => z.Sequence))
            {

                var message = skmBeaconMessageList.FirstOrDefault(x => x.BusinessHourGuid == skmBeaconTimeSlot.BusinessHourGuid);
                if (message != null && message.Id > 0 && !(sortBeaconMessageList.Any(x => x.BusinessHourGuid == skmBeaconTimeSlot.BusinessHourGuid || x.Subject == message.Subject)) && !pushedActionIdList.Contains(message.Id))
                {
                    if (!(sortBeaconMessageList.Any(x => x.Subject == message.Subject)))
                    {
                        sortBeaconMessageList.Add(message);
                    }
                }
            }


            //取得商品+個人收藏訊息 
            //var memberCollectDealBids = ApiMemberManager.GetApiMemberCollectDealList(memberUserId, MemberCollectDealType.Coupon).Select(x=>x.Bid).ToList();
            var memberCollectDealBids = mp.MemberCollectDealGetByMemberUniqueId(memberUserId).Select(p => p.BusinessHourGuid).ToList();
            if (memberCollectDealBids.Any())
            {
                foreach (var skmBeaconTimeSlot in skmBeaconTimeSlotCol.Where(x => x.BeaconType == (int)SkmBeaconType.Deal && x.Status == (int)DealTimeSlotStatus.Default).OrderBy(z => z.Sequence))
                {
                    var message = skmBeaconMessageList.FirstOrDefault(x => x.BusinessHourGuid == skmBeaconTimeSlot.BusinessHourGuid);
                    if (message != null && message.Id > 0 && memberCollectDealBids.Contains(message.BusinessHourGuid))
                    {
                        if (!(sortBeaconMessageList.Any(x => x.Subject == message.Subject)))
                        {
                            if (!pushedActionIdList.Contains(message.Id))
                            {
                                sortBeaconMessageList.Add(message);
                            }
                        }
                    }
                }
            }

            //取得專案 + 匯入資訊
            foreach (var skmBeaconTimeSlot in skmBeaconTimeSlotCol.Where(x => x.BeaconType == (int)SkmBeaconType.Project && x.Status == (int)DealTimeSlotStatus.Default).OrderBy(z => z.Sequence))
            {
                var message = skmBeaconMessageList.FirstOrDefault(x => x.BusinessHourGuid == skmBeaconTimeSlot.BusinessHourGuid);
                if (message != null && message.Id > 0 && !(sortBeaconMessageList.Any(x => x.BusinessHourGuid == skmBeaconTimeSlot.BusinessHourGuid || x.Subject == message.Subject)) && !pushedActionIdList.Contains(message.Id))
                {
                    if (!(sortBeaconMessageList.Any(x => x.Subject == message.Subject)))
                    {
                        skmBeaconGroupMemberIDList = skmMp.SkmBeaconGroupGetByGuid(message.BusinessHourGuid).Select(p => p.MemberId).ToList();
                        if (skmBeaconGroupMemberIDList.Any()) //有匯入名單則比較
                        {
                            if (IsContainSkmBeaconGroup(skmBeaconGroupMemberIDList, skmMember))
                            {
                                sortBeaconMessageList.Add(message);
                            }
                        }//沒匯入則直接加入
                        else
                        {
                            sortBeaconMessageList.Add(message);
                        }
                    }
                }
            }


            //取得商品
            foreach (var skmBeaconTimeSlot in skmBeaconTimeSlotCol.Where(x => x.BeaconType == (int)SkmBeaconType.Deal && x.Status == (int)DealTimeSlotStatus.Default).OrderBy(z => z.Sequence))
            {
                var message = skmBeaconMessageList.Where(x => x.BusinessHourGuid == skmBeaconTimeSlot.BusinessHourGuid && x.Status != (int)SkmBeaconStatus.Deal).FirstOrDefault();
                //var message = skmBeaconMessageList.FirstOrDefault(x => x.BusinessHourGuid == skmBeaconTimeSlot.BusinessHourGuid);
                if (message != null && message.Id > 0 && !pushedActionIdList.Contains(message.Id))
                {
                    if (!(sortBeaconMessageList.Any(x => x.Subject == message.Subject)))
                    {
                        sortBeaconMessageList.Add(message);
                    }
                }
            }

            //取行銷訊息_手動排序
            foreach (var skmBeaconTimeSlot in skmBeaconTimeSlotCol.Where(x => x.BeaconType == (int)SkmBeaconType.Event_Manual && x.Status == (int)DealTimeSlotStatus.Default).OrderBy(z => z.Sequence))
            {
                var message = skmBeaconMessageList.FirstOrDefault(x => x.BusinessHourGuid == skmBeaconTimeSlot.BusinessHourGuid);
                if (message != null && message.Id > 0 && !pushedActionIdList.Contains(message.Id))
                {
                    if (!(sortBeaconMessageList.Any(x => x.Subject == message.Subject)))
                    {
                        sortBeaconMessageList.Add(message);
                    }

                }
            }


            //取行銷訊息_計算排序
            foreach (var skmBeaconTimeSlot in skmBeaconTimeSlotCol.Where(x => x.BeaconType == (int)SkmBeaconType.Event_Count && x.Status == (int)DealTimeSlotStatus.Default).OrderBy(z => z.Sequence))
            {
                var message = skmBeaconMessageList.FirstOrDefault(x => x.BusinessHourGuid == skmBeaconTimeSlot.BusinessHourGuid);
                if (message != null && message.Id > 0 && !pushedActionIdList.Contains(message.Id))
                {
                    if (!(sortBeaconMessageList.Any(x => x.Subject == message.Subject)))
                    {
                        skmBeaconGroupMemberIDList = skmMp.SkmBeaconGroupGetByGuid(message.BusinessHourGuid).Select(p => p.MemberId).ToList();
                        if (IsContainSkmBeaconGroup(skmBeaconGroupMemberIDList, skmMember))
                        {
                            sortBeaconMessageList.Add(message);
                        }
                    }
                }
            }

            return sortBeaconMessageList.Any() ? sortBeaconMessageList.First() : new SkmBeaconMessage();
        }

        /// <summary>
        /// 判斷是否存在指定客群  skmbeacongroup.[member_id] == skm_member.[skm_token]
        /// </summary>
        /// <param name="skmBeaconGroupMemberIDList">指定群眾的關係表</param>
        /// <param name="skmMember">新光會員是以亂數token形式與17life綁定會員關係</param>
        /// <returns></returns>
        private static bool IsContainSkmBeaconGroup(List<string> skmBeaconGroupMemberIDList, SkmMember skmMember)
        {
            if (skmBeaconGroupMemberIDList.Any() && skmMember.Id > 0)
            {
                return skmBeaconGroupMemberIDList.Contains(skmMember.SkmToken);
            }
            return false;
        }

        private ActionEventInfo GetActionEventInfo()
        {
            if (_aei.Key != null && _aei.Key.IsLoaded && _aei.Value > DateTime.Now)
            {
                return _aei.Key;
            }

            var actionEventList = mp.ActionEventInfoListGet()
                .Where(x => x.ActionName.Equals("SkmBeaconEvent", StringComparison.OrdinalIgnoreCase)).ToList();

            if (!actionEventList.Any()) return new ActionEventInfo();

            _aei = new KeyValuePair<ActionEventInfo, DateTime>(actionEventList.First(),
                actionEventList.First().ActionEndTime);

            return _aei.Key;
        }

        #endregion method

        #region 手動執行

        /// <summary>
        /// [手動執行] Refresh 指定檔次訂購數
        /// </summary>
        /// <param name="mode"></param>
        /// <param name="bid"></param>
        /// <returns></returns>
        [HttpGet]
        [OAuthScope(TokenScope.SystemManagement)]
        public ApiResult RefreshDealSalesInfo(int mode, Guid bid)
        {
            switch (mode)
            {
                case (int)SkmRefreshDealSalesInfoMode.SkmTempTable:
                    pp.DealSalesInfoRefreshForSkm(bid);
                    break;
                case (int)SkmRefreshDealSalesInfoMode.SkmStoredProcedure:
                    pp.ExecSpRefreshDealSalesInfoForSkm(bid);
                    break;
                case (int)SkmRefreshDealSalesInfoMode.PayEventsOnRefresh:
                    PayEvents.OnRefresh(bid);
                    break;
            }
            return new ApiResult { Code = ApiResultCode.Success };
        }

        #endregion

        #region 提供Umall修改贈庫存

        /// <summary>
        /// 查詢贈品庫存
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.SkmUmall)]
        public ApiResult GetGiftInventory(GetGiftInventoryRequestModel model)
        {
            List<GiftOrderedAndInventoryQty> giftList = skmEfp.GetDealQuantityByExchangeItemId(model.ExchangeItemId).
                Select(item =>
                {
                    return new GiftOrderedAndInventoryQty()
                    {
                        ExternalDealGuid = item.ExternalDealGuid,
                        ShopCode = item.ShopCode,
                        ExchangeItemId = item.ExchangeItemId,
                        SkmEventId = item.SkmEventId,
                        OrderedQty = item.OrderedQuantity,
                        AvailableQty = (int)item.OrderTotalLimit - item.OrderedQuantity
                    };
                }).ToList();
            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = giftList
            };
        }

        /// <summary>
        /// 更新贈品庫存
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.SkmUmall)]
        public ApiResult UpdateGiftInventory(UpdateSkmGiftInventoryRequestModel model)
        {
            var giftList = model.SkmGiftInventory;
            bool isSuccess = true;
            SkmLog skmLog = new SkmLog() { SkmToken = "", UserId = 0, LogType = (int)SkmLogType.UmallUpdateGiftInventory, InputValue = new JsonSerializer().Serialize(model) };
            skmMp.SkmLogSet(skmLog);
            List<SkmGiftInventoryResponseModel> skmGiftInventoryResponseModels = new List<SkmGiftInventoryResponseModel>();
            foreach (SkmGiftInventoryModel data in model.SkmGiftInventory)
            {
                string message = string.Empty;
                SkmGiftInventoryResponseModel skmGiftInventoryResponseModel = new SkmGiftInventoryResponseModel()
                {
                    ExternalDealGuid = data.ExternalDealGuid,
                    ExchangeItemId = data.ExchangeItemId,
                    ShopCode = data.ShopCode,
                    SkmEventId = data.SkmEventId,
                    AddQty = data.AddQty,
                };
                try
                {
                    if (SkmFacade.SetSkmExternalDealInventory(data, out message))
                    {
                        skmGiftInventoryResponseModel.IsSuccess = true;
                    }
                    else
                    {
                        skmGiftInventoryResponseModel.IsSuccess = false;
                        skmGiftInventoryResponseModel.Message = message;
                        isSuccess = false;
                    }
                    skmGiftInventoryResponseModels.Add(skmGiftInventoryResponseModel);
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                }
            }
            ApiResult result = new ApiResult
            {
                Code = isSuccess ? ApiResultCode.Success : ApiResultCode.SkmUpdateGiftInventoryFail,
                Data = skmGiftInventoryResponseModels,
                Message = isSuccess ? "" : "更新庫存失敗"
            };
            skmLog.OutputValue = new JsonSerializer().Serialize(result);
            //DB欄位長度限制
            if (skmLog.OutputValue.Length > 1000) skmLog.OutputValue = skmLog.OutputValue.Substring(0, 1000);
            skmLog.SkmToken = isSuccess.ToString();
            skmMp.SkmLogSet(skmLog);
            return result;
        }

        #endregion

        #region [SKM資訊部] 查詢線上 建檔/策展/訂單 資料

        /// <summary>
        /// [SKM資訊部] 查詢skmpay訂單交易明細
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.SkmIt)]
        public ApiResult GetSkmpayOrderInfo(GetSkmpayOrderInfoRequestModel model)
        {
            #region check

            if (model.EndDate < model.StartDate)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError,
                    Data = new List<SkmpayOrderInfo>(),
                    Message = "結束日不得小於開始日。"
                };
            }
            if (model.StartDate.AddMonths(1) < model.EndDate)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError,
                    Data = new List<SkmpayOrderInfo>(),
                    Message = "查詢區間不可大於一個月。"
                };
            }

            #endregion

            List<SkmpayOrderInfo> data = skmEfp.ExecFnGetSkmpayOrderInfo(model.StartDate, model.EndDate).OrderBy(x => x.op_date).ToList();

            return new ApiResult
            {
                Code = data.Any() ? ApiResultCode.Success : ApiResultCode.DataNotFound,
                Data = data
            };
        }

        /// <summary>
        /// [SKM資訊部] 查詢上架商品
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.SkmIt)]
        public ApiResult GetSkmOnlineDeals(GetSkmInfoRequestModel model)
        {
            #region check
            if (model.EndOpDate < model.StartOpDate)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError,
                    Data = new List<SkmOnlineDeals>(),
                    Message = "結束日不得小於開始日。"
                };
            }
            if (model.StartOpDate.AddMonths(1) < model.EndOpDate)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError,
                    Data = new List<SkmOnlineDeals>(),
                    Message = "查詢區間不可大於一個月。"
                };
            }
            #endregion

            List<SkmOnlineDeals> data = skmEfp.ExecFnGetSkmOnlineDeals(model.StartOpDate, model.EndOpDate).OrderBy(x => x.start_date).ToList();
            return new ApiResult
            {
                Code = data.Any() ? ApiResultCode.Success : ApiResultCode.DataNotFound,
                Data = data
            };
        }

        /// <summary>
        /// [SKM資訊部] 查詢策展列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.SkmIt)]
        public ApiResult GetSkmCurationEvents(GetSkmInfoRequestModel model)
        {
            #region check
            if (model.EndOpDate < model.StartOpDate)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError,
                    Data = new List<SkmCurationEvents>(),
                    Message = "結束日不得小於開始日。"
                };
            }
            if (model.StartOpDate.AddMonths(1) < model.EndOpDate)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError,
                    Data = new List<SkmCurationEvents>(),
                    Message = "查詢區間不可大於一個月。"
                };
            }
            #endregion

            List<SkmCurationEvents> data = skmEfp.ExecFnGetSkmCurationEvents(model.StartOpDate, model.EndOpDate).OrderBy(x => x.curation_id).ToList();
            return new ApiResult
            {
                Code = data.Any() ? ApiResultCode.Success : ApiResultCode.DataNotFound,
                Data = data
            };
        }

        /// <summary>
        /// [SKM資訊部] 查詢策展商品關聯
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.SkmIt)]
        public ApiResult GetSkmCurationDeals(GetSkmInfoRequestModel model)
        {
            #region check
            if (model.EndOpDate < model.StartOpDate)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError,
                    Data = new List<SkmCurationDeals>(),
                    Message = "結束日不得小於開始日。"
                };
            }
            if (model.StartOpDate.AddMonths(1) < model.EndOpDate)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError,
                    Data = new List<SkmCurationDeals>(),
                    Message = "查詢區間不可大於一個月。"
                };
            }
            #endregion

            List<SkmCurationDeals> data = skmEfp.ExecFnGetSkmCurationDeals(model.StartOpDate, model.EndOpDate).OrderBy(x => x.curation_id).ToList();
            return new ApiResult
            {
                Code = data.Any() ? ApiResultCode.Success : ApiResultCode.DataNotFound,
                Data = data
            };
        }

        #endregion

        #region 自訂版位
        [HttpGet]
        public ApiResult GetIndexCustomizedBoard(Guid sellerGuid)
        {
            IEnumerable<IndexCustomizedBoardResponse> indexCustomizedBoardResponses =
                SkmFacade.GetCustomizedBoardList(sellerGuid).Select(item =>
                    GetIndexCustomizedBoardResponse(item, sellerGuid));

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = indexCustomizedBoardResponses,
            };
        }

        /// <summary>
        /// 重新整理response的值
        /// </summary>
        /// <param name="item">DB</param>
        /// <param name="sellerGuid"></param>
        /// <returns></returns>
        private IndexCustomizedBoardResponse GetIndexCustomizedBoardResponse(SkmCustomizedBoard item, Guid sellerGuid)
        {
            IndexCustomizedBoardResponse response = new IndexCustomizedBoardResponse();
            response.LayoutType = item.LayoutType;
            response.Image = item.ImageUrl;
            response.Url = item.LinkUrl;
            response.ContentType = (int)SkmCustomizedBoardContentyTypeEnum.DeepLink;

            //美食訂候位(預設)   inline
            //策展列表(預設)     deeplink
            //美麗台(預設)       weburl
            //策展列表頁         deeplink
            //策展主題頁         deeplink
            //商品頁             deeplink
            //網址               weburl
            //抽紅包           deepLink
            //登記抽           deepLink
            bool isValid = true;
            var value = string.Empty;
            switch ((SkmCustomizedBoardUrlTypeEnum)item.ContentType)
            {
                case SkmCustomizedBoardUrlTypeEnum.DefaultExhibitionList:
                    break;
                case SkmCustomizedBoardUrlTypeEnum.DefaultGoodEat:
                    response.ContentType = (int)SkmCustomizedBoardContentyTypeEnum.Inline;
                    string inlineUrl = string.Empty;
                    GetRsvUrl(item.LinkUrl, sellerGuid.ToString(), out inlineUrl);
                    response.Url = inlineUrl;
                    break;
                case SkmCustomizedBoardUrlTypeEnum.DefaultBeautyStage:
                    response.ContentType = (int)SkmCustomizedBoardContentyTypeEnum.Weburl;
                    break;
                case SkmCustomizedBoardUrlTypeEnum.Url:
                    response.ContentType = (int)SkmCustomizedBoardContentyTypeEnum.Weburl;
                    break;
                case SkmCustomizedBoardUrlTypeEnum.ExternalDeal:
                    //若商品連結是否有過期
                    //skmapp://www.skm.com/productpage?pbid=802c9859-fc93-4365-9f2d-234e20fdf79e
                    var pbid = string.Empty;
                    if (GetQueryStringValue(response.Url, "pbid", out pbid))
                    {
                        var deal = SkmCacheFacade.GetOnlineViewExternalDeal(Guid.Parse(pbid));
                        if (deal != null && deal.IsLoaded)
                        {
                            if (deal.Status != (int)SKMDealStatus.Confirmed
                                || deal.IsDel == true
                                || deal.BusinessHourOrderTimeS > DateTime.Now
                                || deal.BusinessHourOrderTimeE < DateTime.Now)
                            {
                                isValid = false;
                            }
                        }
                    }
                    break;
                case SkmCustomizedBoardUrlTypeEnum.CustomizedExhibitionList:
                    break;
                case SkmCustomizedBoardUrlTypeEnum.ExhibitionEvent:
                    //判斷單一策展是否有過期
                    //策展主題頁：skmapp://www.skm.com/exhibitionpage?store=b48f23f0-a7ff-4245-967d-43d20bbe71d8&sname=台北信義新天地&exid=3810
                    value = string.Empty;
                    int exid = 0;
                    if (GetQueryStringValue(response.Url, "exid", out value) && int.TryParse(value, out exid))
                    {
                        var exhibition = exp.GetExhibitionEvent(exid);
                        if (exhibition.Id != 0)
                        {
                            if (exhibition.Status != (byte)SkmExhibitionEventStatus.Published
                                || exhibition.EndDate < DateTime.Now)
                            {
                                isValid = false;
                            }
                        }
                    }
                    break;
                case SkmCustomizedBoardUrlTypeEnum.PrizeDrawEvent:
                    //skmapp://www.skm.com/luckydraw?type=0&token=EventToken
                    if (!GetQueryStringValue(response.Url, "token", out value))
                    {
                        isValid = false;
                        break;
                    }

                    var drawEvent = cep.GetPrizeDrawEvent(value);
                    if (drawEvent == null || drawEvent.Status == (int)SkmPrizeDrawEventStatus.Hide
                        || drawEvent.ED < DateTime.Now)
                    {
                        isValid = false;
                        break;
                    }

                    break;
                case SkmCustomizedBoardUrlTypeEnum.WeeklyDrawEvent:
                    //skmapp://www.skm.com/luckydraw?type=1&token=EventToken
                    if (!GetQueryStringValue(response.Url, "token", out value))
                    {
                        isValid = false;
                        break;
                    }

                    var skmActivity = skmEfp.GetSkmActivity(value);
                    if (skmActivity.Id == 0 || !DateTime.Now.IsBetween(skmActivity.JoinStartDate, skmActivity.JoinEndDate))
                    {
                        isValid = false;
                        break;
                    }

                    break;
            }

            //[左方](預設)該店策展列表
            //[右上](預設)inline
            //[右下](預設)美麗台
            if (isValid == false)
            {
                switch (response.LayoutType)
                {
                    case (int)SkmCustomizedBoardLayoutTypeEnum.IndexZone1:
                        //過期則回歸預設策展列表
                        response.Url = string.Format(SkmDeepLinkFormat.ExhibitionList, sellerGuid.ToString(), sp.SellerGet(sellerGuid).SellerName);
                        response.ContentType = (int)SkmCustomizedBoardContentyTypeEnum.DeepLink;
                        break;
                    case (int)SkmCustomizedBoardLayoutTypeEnum.IndexZone2:
                        //過期則回歸預設inline美食訂位
                        string inlineUrl = string.Empty;
                        GetRsvUrl(config.SkmRsvUrl, sellerGuid.ToString(), out inlineUrl);
                        response.Url = inlineUrl;
                        response.ContentType = (int)SkmCustomizedBoardContentyTypeEnum.Inline;
                        break;
                    case (int)SkmCustomizedBoardLayoutTypeEnum.IndexZone3:
                        //過期則回歸預設美麗台
                        response.Url = config.SkmBeautyStageUrl;
                        response.ContentType = (int)SkmCustomizedBoardContentyTypeEnum.Weburl;
                        break;
                }
            }

            return response;
        }

        /// <summary>
        /// 解析網址 並拿到參數值(value)
        /// </summary>
        /// <param name="url"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        private bool GetQueryStringValue(string url, string key, out string value)
        {
            value = string.Empty;

            //url sample
            //skmapp://www.skm.com/exhibitionpage?store=b48f23f0-a7ff-4245-967d-43d20bbe71d8&sname=台北信義新天地&exid=3810
            var urlArray = url.Split('?');
            if (!urlArray.Any())
                value = string.Empty;

            //store=b48f23f0-a7ff-4245-967d-43d20bbe71d8&sname=台北信義新天地&exid=3810
            var queryString = urlArray.LastOrDefault().Split('&');
            if (!queryString.Any())
                value = string.Empty;

            foreach (var qs in queryString)
            {
                if (qs.Split('=').Any()
                    && qs.Split('=').FirstOrDefault() == key)
                {
                    value = qs.Split('=').LastOrDefault();
                    return true;
                }
            }

            return false;
        }

        #endregion

        #region 憑證推播通知(測試)

        [DeveloperOnly]
        [HttpGet]
        public List<SkmPushRequest> GetPushData()
        {
            return SkmFacade.GetPushData();
        }

        [DeveloperOnly]
        [HttpGet]
        public List<SkmPushResponse> TryPost()
        {
            var failedResult = new List<SkmPushResponse>();
            foreach (var item in SkmFacade.GetPushData())
            {
                var response = SkmFacade.PostSkmPushData(item);
                if (!SkmFacade.IsSkmPushSuccessed(response))
                {
                    failedResult.Add(response);
                }
            }

            if (failedResult.Any())
            {
                logger.ErrorFormat("failedResults : {0}", JsonConvert.SerializeObject(failedResult));
            }
            return failedResult;
        }

        [DeveloperOnly]
        [HttpGet]
        public List<SkmInAppMessage> SetPushDataInAppMessage()
        {
            var messages = SkmFacade.PrePushDataInAppMessage();
            SkmFacade.SaveSkmInAppMessageCollection(messages);
            return messages;
        }

        [DeveloperOnly]
        [HttpPost]
        public List<ApiResult> TestSkmPush(string[] tokens)
        {

            var time = tokens[0];
            var localName = tokens[1];
            var temp = tokens.ToList();
            temp.RemoveAt(0);
            temp.RemoveAt(0);
            tokens = temp.ToArray();

            string pushLink;

            switch (localName)
            {
                case "取貨通知":
                case "取貨截止通知":
                    pushLink = Helper.GetEnumDescription(EventType.OrderListNotVerified);
                    break;
                case "優惠券通知":
                case "優惠券截止通知":
                    pushLink = Helper.GetEnumDescription(EventType.CouponListNotVerified);
                    break;
                default:
                    pushLink = Helper.GetEnumDescription(EventType.OrderListNotVerified);
                    break;
            }

            var request = new SkmPushRequest()
            {
                MerchantId = "121",
                LocalName = "【" + localName + "】" + string.Format("{0:HHmmss}", DateTime.Now),
                PushContent = "訂單推播測試(PushContent) " + string.Format("{0:HHmmss}", DateTime.Now),
                PushLink = pushLink,
                ProgramType = Helper.GetEnumDescription(SkmPushRequestProgramType.DeepLink),
                ExpectSendTime = time,
                Tokens = new List<string>()
            };

            foreach (var token in tokens)
            {
                request.AddToken(token);
            }

            var messages = new List<SkmInAppMessage>();
            var now = DateTime.Now;

            foreach (var token in tokens)
            {
                var member = skmMp.MemberGetBySkmToken(token);

                var message = new SkmInAppMessage()
                {
                    UserId = member.UserId,
                    EventType = (byte)EventType.OrderListNotVerified,
                    IsRead = false,
                    Subject = request.LocalName,
                    MsgContent = request.PushContent,
                    MsgType = (byte)SkmInAppMessageType.DeepLink,
                    IsDel = false,
                    ModifyTime = now,
                    SendTime = now
                };

                messages.Add(message);
            }

            var result = new List<ApiResult>();
            //呼叫資訊部API推播
            var response = SkmFacade.PostSkmPushData(request);
            //塞推播訊息至訊息中心
            var isSavedInAppMessageCollection = skmEfp.BulkInsertSkmInAppMessage(messages);

            result.Add(new ApiResult()
            {
                Data = request,
                Message = "Request"
            });
            result.Add(new ApiResult()
            {
                Data = response,
                Message = "Response:" + (SkmFacade.IsSkmPushSuccessed(response) ? "成功" : "失敗")
            });

            result.Add(new ApiResult()
            {
                Data = messages,
                Message = "新增訊息筆數:" + messages.Count + ";" + (isSavedInAppMessageCollection ? "成功" : "失敗")
            });

            return result;
        }

        #endregion

        /// <summary>
        /// 手退發票
        /// </summary>
        /// <param name="orderNo"></param>
        /// <returns></returns>
        [HttpGet]
        public ApiResult RefundInvoiceByOrderNo(string orderNo)
        {
            var skmPayOrder = skmEfp.GetSkmPayOrderByOrderNo(orderNo);
            if (skmPayOrder == null)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.DataNotFound
                };
            }
            
            SkmPayOrderStatus orderStatus;
            SkmFacade.InvoiceRefund(skmPayOrder, out orderStatus, true);
            logger.InfoFormat("獨退發票: 訂單編號 {0} 已處理", orderNo);
            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = orderStatus
            };
        }

    }

    /// <summary>
    /// 
    /// </summary>
    public class SkmValidateCodeModel
    {
        /// <summary>
        /// 
        /// </summary>
        public string QrStr { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class SkmValidateCodeResult
    {
        /// <summary>
        /// 
        /// </summary>
        public string ValidateCodeSource { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ValidateCode { get; set; }
    }

   

}


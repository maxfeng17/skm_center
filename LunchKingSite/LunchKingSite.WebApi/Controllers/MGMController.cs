﻿using System;
using System.Web.Http;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.WebApi.Core.OAuth;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Facade;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Models.FamiPort;
using LunchKingSite.BizLogic.Models.MGM;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.Interface;
using LunchKingSite.Core.ModelCustom;
using LunchKingSite.WebApi.Controllers.Base;
using LunchKingSite.DataOrm;
using LunchKingSite.WebApi.Core.Attributes;
using LunchKingSite.WebApi.Models;
using LunchKingSite.WebApi.Models.MGM;
using LunchKingSite.WebLib.Component.MGM;

namespace LunchKingSite.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [RequireHttps]
    public class MGMController : ApiControllerBase
    {
        #region Property

        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private static IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        private static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private static IFamiportProvider fami = ProviderFactory.Instance().GetProvider<IFamiportProvider>();
        private static IMGMProvider mgmp = ProviderFactory.Instance().GetProvider<IMGMProvider>();
        private static IHiLifeProvider hiLife = ProviderFactory.Instance().GetProvider<IHiLifeProvider>();
        #endregion

        #region 送禮API

        /// <summary>
        /// 取得可送禮物列表
        /// </summary>
        /// <param name="getSendGift"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult GetSendGift(MgmGetData getSendGift)
        {
            if (getSendGift == null || getSendGift.UserId == null)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }
            if (User.Identity.Id == 0)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.UserNoSignIn
                };
            }
            var data = new List<SendGiftData>();

            var uid = MemberFacade.GetUniqueId(User.Identity.Name);
            if (getSendGift.GetAll)
            {
                data = MGMFacade.GetGiftOrderListByUser(uid).Where(x => x.RemainCount > 0).ToList();
            }
            else
            {
                data = MGMFacade.GetGiftOrderListByUser(uid).Where(x => x.RemainCount > 0).Skip(getSendGift.Page * 10).Take(10).ToList();

            }

            data = new AntiFraud(uid).GetGiftOrderListByUser(data);

            var returnData = new ApiReturnGiftList();
            returnData.GiftList = data;
            returnData.TotalCount = data.Count;
            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = returnData
            };
        }

        /// <summary>
        /// 送禮，建立禮物
        /// </summary>
        /// <param name="giftCardModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult SendGift(GiftCardModel giftCardModel)
        {
            if (giftCardModel == null || !ModelState.IsValid)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }

            if (User.Identity.Id == 0)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.UserNoSignIn
                };
            }

            ApiResult apiResult = new ApiResult();
            try
            {
                GiftCard giftCard = new GiftCard()
                {
                    Message = giftCardModel.Message,
                    SenderId = giftCardModel.SenderId,
                    SenderName = giftCardModel.SenderName,
                    CardStyleId = giftCardModel.CardStyleId,
                    OrderId = giftCardModel.OrderId,
                    MatchMemberGroup = giftCardModel.MatchMemberGroup
                };

                string errMsg = string.Empty;
                if (MGMFacade.CreateGift(ref giftCard, ref errMsg))
                {
                    foreach (var matchMemeber in giftCard.MatchMemberGroup)
                    {
                        GiftCardUtility.SendGift(matchMemeber, giftCard);

                        //加入通訊錄
                        MemberContact mc = new MemberContact()
                        {
                            ContactValue = matchMemeber.Value,
                            ContactType = matchMemeber.Type,
                            UserId = User.Identity.Id,
                            Guid = Guid.NewGuid(),
                            ContactName = matchMemeber.Name
                        };
                        MemberFacade.MemberContactSet(mc);
                    }

                    return new ApiResult
                    {
                        //FB目前為一人，回傳accessCode Url
                        Data = new
                        {
                            AccessLink =
                               "https://" + config.MGMGiftGetUrl + "/g/" + giftCard.MatchMemberGroup.First().AccessCode
                        },
                        Code = ApiResultCode.Success
                    };
                }
                apiResult.Message = errMsg;
            }
            catch (Exception ex)
            {
                apiResult.Message = ex.Message;
            }

            apiResult.Code = ApiResultCode.Error;
            return apiResult;
        }

        /// <summary>
        /// 取得卡片樣式
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ApiResult GiftCardStyle()
        {
            if (User.Identity.Id == 0)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.UserNoSignIn
                };
            }
            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = GiftCardUtility.CardStyleGet,
                Message = ""
            };
        }

        /// <summary>
        /// 取得熱門禮物
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ApiResult GetHotDeals()
        {
            if (User.Identity.Id == 0)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.UserNoSignIn
                };
            }

            var recommendDeals = ViewPponDealManager.DefaultManager.ViewPponDealFoolGetByTopSalesDeal(4);
            List<PponDealSynopsisBasic> pponDealSynopsisBasicrecommendDeals = new List<PponDealSynopsisBasic>();

            foreach (ViewPponDeal vpd in recommendDeals)
            {
                PponDealSynopsis synopsis = PponDealApiManager.PponDealSynopsisGetByDeal(vpd, null, 0, VbsDealType.Ppon, true);
                PponDealSynopsisBasic basicDeal = PponDealApiManager.PponDealSynopsisToBasic(synopsis);
                pponDealSynopsisBasicrecommendDeals.Add(basicDeal);
            }

            int totalCount = pponDealSynopsisBasicrecommendDeals.Count;
            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = new { DealList = pponDealSynopsisBasicrecommendDeals, TotalCount = totalCount },
                Message = ""
            };
        }


        #endregion

        #region 收禮API

        /// <summary>
        /// 取得禮物數量
        /// </summary>
        /// <param name="getList"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult GetGiftCount(MgmGetData getList)
        {
            if (getList.UserId == null)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }
            if (User.Identity.Id == 0)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.UserNoSignIn
                };
            }

            var giftCount = MGMFacade.MemberFastInfoGetUserId(User.Identity.Id);
            var data = new GiftCount();

            //MemberFastInfo 計算newGiftCount有誤，直接算
            int newGiftCount = MGMFacade.GetMgmNewGiftCountByReceiverId(User.Identity.Id);
            data.NewGiftCount = newGiftCount;
            data.ReplyCardCount = giftCount.ReplyCardCount ?? 0;
            data.AsCount = giftCount.MessageCount;

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = data
            };
        }

        /// <summary>
        /// 禮物歸戶
        /// </summary>
        /// <param name="getGift"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult GetGift(MgmGetData getGift)
        {
            if (getGift.UserId == null)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }
            if (User.Identity.Id == 0)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.UserNoSignIn
                };
            }
            string message = "OK";
            int giftId = 0;
            if (getGift.AsCode != null)
            {
                var tmpStr = MGMFacade.GiftUserIdSet(getGift.AsCode, User.Identity.Id);
                if (tmpStr != 0)
                {
                    giftId = tmpStr;
                    MGMFacade.GiftMessageCountSet(User.Identity.Id, (int)GiftMessageType.Send);
                }
            }
            else
            {
                var asCodeList = MGMFacade.GiftAccessGetByUserId(User.Identity.Id);
                if (asCodeList.Any())
                {
                    foreach (var code in asCodeList)
                    {
                        var tmpStr = MGMFacade.GiftUserIdSet(code, User.Identity.Id);
                        if (tmpStr != 0)
                        {
                            giftId = tmpStr;
                            MGMFacade.GiftMessageCountSet(User.Identity.Id, (int)GiftMessageType.Send);
                        }
                    }
                }
            }
            if (giftId == 0)
            {
                message = "禮物已被領取，無法再次領取";
            }

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Message = message
            };
        }

        /// <summary>
        /// 取得禮物列表
        /// </summary>
        /// <param name="getList"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult GetGiftList(MgmGetData getList)
        {
            if (getList.UserId == null)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }
            if (User.Identity.Id == 0)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.UserNoSignIn
                };
            }

            var data = MGMFacade.GiftGetByReceiverId(User.Identity.Id);

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = data
            };
        }

        /// <summary>
        /// 取得卡片
        /// </summary>
        /// <param name="getCard"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult GetCardMsg(MgmGetData getCard)
        {
            if (getCard.UserId == null)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }
            if (User.Identity.Id == 0)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.UserNoSignIn
                };
            }
            string errMsg = string.Empty;
            var data = MGMFacade.GiftCardGet(User.Identity.Id, getCard.MsgId, getCard.Type, ref errMsg);

            if (errMsg != string.Empty)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = errMsg
                };
            }

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = data
            };
        }

        /// <summary>
        /// 送禮者取得卡片
        /// </summary>
        /// <param name="getCard"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult GetCardBySender(MgmGetData getCard)
        {

            if (User.Identity.Id == 0)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.UserNoSignIn
                };
            }

            var sendTime = Convert.ToDateTime(getCard.SendTime);
            var data = MGMFacade.GiftCardGetBySender(User.Identity.Id, sendTime);

            if (data.CardData == null)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.DataNotFound,
                };
            }

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = data
            };
        }

        /// <summary>
        /// 取得禮物明細
        /// </summary>
        /// <param name="getGift"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult GetGiftItem(MgmGetData getGift)
        {
            if (getGift.UserId == null)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }
            if (User.Identity.Id == 0)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.UserNoSignIn
                };
            }

            var data = MGMFacade.ApiGiftViweGetByAsCode(getGift.AsCode, User.Identity.Id);

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = data
            };
        }

        /// <summary>
        /// 更新禮物狀態
        /// </summary>
        /// <param name="upData"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult UpdateGift(MgmGetData upData)
        {
            var apiResult = new ApiResult();

            if (upData.UserId == null)
            {
                apiResult.Code = ApiResultCode.InputError;
                return apiResult;
            }

            if (User.Identity.Id == 0)
            {
                apiResult.Code = ApiResultCode.UserNoSignIn;
                return apiResult;
            }

            if (upData.Type == (int)MgmAcceptGiftType.Accept || upData.Type == (int)MgmAcceptGiftType.Reject
                || upData.Type == (int)MgmAcceptGiftType.Cancel || upData.Type == (int)MgmAcceptGiftType.Quick)
            {
                int errorType = -1;
                if (MGMFacade.UpDateGift(upData.AsCode, upData.Type, ref errorType))
                {
                    apiResult.Code = ApiResultCode.Success;
                    return apiResult;
                }
                //error
                apiResult.Code = ApiResultCode.Error;
                switch (errorType)
                {
                    case (int)MgmAcceptGiftType.Accept:
                        apiResult.Message = MgmAcceptGiftType.Accept.GetAttributeOfType<DescriptionAttribute>().Description;
                        break;
                    case (int)MgmAcceptGiftType.Reject:
                        apiResult.Message = MgmAcceptGiftType.Reject.GetAttributeOfType<DescriptionAttribute>().Description;
                        break;
                    case (int)MgmAcceptGiftType.Cancel:
                        apiResult.Message = MgmAcceptGiftType.Cancel.GetAttributeOfType<DescriptionAttribute>().Description;
                        break;
                    case (int)MgmAcceptGiftType.None:
                        apiResult.Message = MgmAcceptGiftType.None.GetAttributeOfType<DescriptionAttribute>().Description;
                        break;
                    case (int)MgmAcceptGiftType.Expired:
                        apiResult.Message = MgmAcceptGiftType.Expired.GetAttributeOfType<DescriptionAttribute>().Description;
                        break;
                    case (int)MgmAcceptGiftType.Quick: //快速領取使用一般接收資訊即可
                        apiResult.Message = MgmAcceptGiftType.Accept.GetAttributeOfType<DescriptionAttribute>().Description;
                        break;
                    default:
                        apiResult.Code = ApiResultCode.DataNotFound;
                        break;
                }
                return apiResult;
            }

            apiResult.Code = ApiResultCode.InputError;
            return apiResult;
        }


        /// <summary>
        /// 回覆卡片
        /// </summary>
        /// <param name="cardData"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult ReplyCard(MgmGetData cardData)
        {
            if (cardData.UserId == null)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }
            if (User.Identity.Id == 0)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.UserNoSignIn
                };
            }
            if (MGMFacade.CardReply(cardData.MsgId, cardData.Msg, cardData.SendName, cardData.CardStyleId, User.Identity.Id))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.Success,
                    Message = "卡片回覆成功"
                };
            }
            else
            {
                return new ApiResult
                {
                    Code = ApiResultCode.DataNotFound,
                    Message = "卡片回覆失敗"
                };
            }

        }


        /// <summary>
        /// 取得禮物去哪列表
        /// </summary>
        /// <param name="getList"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult GetGiftWhereList(MgmGetData getList)
        {
            if (getList == null || getList.UserId == null)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }
            if (User.Identity.Id == 0)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.UserNoSignIn
                };
            }

            var data = MGMFacade.ApiGiftWhereGetBySenderId(User.Identity.Id);

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = data
            };
        }

        /// <summary>
        /// 取得禮物去哪明細
        /// </summary>
        /// <param name="getItem"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResult GetGiftWhereItem(MgmGetData getItem)
        {
            if (getItem == null || getItem.UserId == null)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError
                };
            }
            if (User.Identity.Id == 0)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.UserNoSignIn
                };
            }

            var tmpdata = Convert.ToDateTime(getItem.SendTime);
            var data = MGMFacade.ApiGiftWhereItemGetBySenderId(getItem.OrderGuid, tmpdata);

            if (!data.ReceiverList.Any())
            {
                return new ApiResult
                {
                    Code = ApiResultCode.DataNotFound,
                };
            }

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = data
            };
        }


        #endregion

        #region 成套收禮(全家/萊爾富)

        /// <summary>
        /// 取得兌換條碼，指定禮物盒的couponId
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [RequireHttps]
        [OAuthScope(TokenScope.Deal)]
        public ApiResult GetHiLifePincodeByGift(GetHiLifePincodeByGiftInput input)
        {
            var result = new ApiResult();
            result.Code = ApiResultCode.DataNotFound;

            //get myGift 
            var gift = mgmp.GiftViweGetByAsCode(input.AsCode, User.Identity.Id);
            List<int> couponIds = new List<int>();

            if (gift.Any())
            {
                couponIds = gift.Select(x => x.CouponId ?? 0).ToList();
            }

            var pincodeData = hiLife.GetHiLifePincodeByGift(input.OrderGuid, input.ExchangeCount, couponIds);

            if (!pincodeData.Any())
            {
                result.Code = ApiResultCode.DataNotFound;
                return result;
            }

            var groupPincodeList = new List<HiLifePincodeModel>();
            foreach (var p in pincodeData)
            {
                groupPincodeList.Add(new HiLifePincodeModel
                {
                    CouponId = p.Id,
                    PincodeList = new List<string>()
                    {
                        config.HiLifeFixedFunctionPincode,//第一段固定功能條碼
                        p.Pincode//peztemp.pezcode廠商匯入序號為第二段條碼
                    },
                });
            }

            //add log
            hiLife.AddHiLifeGetPincodeLog(pincodeData, User.Identity.Id, input.OrderGuid, input.ExchangeCount);
            result.Code = ApiResultCode.Success;
            result.Data = groupPincodeList;
            return result;
        }

        /// <summary>
        /// 取得兌換條碼，指定禮物盒的couponId
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost]
        [RequireHttps]
        [OAuthScope(TokenScope.Deal)]
        public ApiResult GetFamiGroupBarcodeByGift(GetFamiGroupBarCodeByGiftInput input)
        {
            var result = new ApiResult();
            result.Code = ApiResultCode.DataNotFound;
            var groupBarcodeList = new List<FamiGroupBarcode>();

            //get myGift 
            var gift = mgmp.GiftViweGetByAsCode(input.AsCode, User.Identity.Id);
            List<int> couponIds = new List<int>();

            if (gift.Any())
            {
                couponIds = gift.Select(x => x.CouponId ?? 0).ToList();
            }

            var barcodeData = fami.GetFamilNetVerifyBarcodeByGift(input.OrderGuid, input.ExchangeCount, couponIds);

            if (!barcodeData.Any()) return result;

            foreach (var barcode in barcodeData)
            {
                var b = new FamiGroupBarcode
                {
                    CouponId = barcode.CouponId,
                    Pincode = barcode.PezCode,
                    Barcode = new List<string>()
                };

                if(input.CouponType == (int)DealCouponType.FamiSingleBarcode)
                {
                    if (!string.IsNullOrEmpty(barcode.Barcode2))
                    {
                        b.Barcode.Add(barcode.Barcode2);
                        groupBarcodeList.Add(b);
                    }
                }
                else
                {
                    b.Barcode.Add(barcode.Barcode1);
                    b.Barcode.Add(barcode.Barcode2);
                    b.Barcode.Add(barcode.Barcode3);
                    b.Barcode.Add(barcode.Barcode4);
                    groupBarcodeList.Add(b);
                }
            }

            fami.AddFamilyNetGetBarcodeLog(barcodeData, User.Identity.Id, input.OrderGuid, input.ExchangeCount);
            result.Code = ApiResultCode.Success;
            result.Data = groupBarcodeList;
            return result;
        }

        #endregion

        #region 檔次推薦

        /// <summary>
        /// 取得首頁活動策展推薦，排除宅配，只要憑證
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.Deal)]
        public ApiResult GetRecommendCurations(GetMainPageCurationsModel model)
        {
            if (User.Identity.Id == 0)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.UserNoSignIn
                };
            }
            List<ApiCurationInfo> curationInfos = MarketingFacade.GetApiCurationsByDefaultBanner();


            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = curationInfos
            };
        }

        /// <summary>
        /// 取得送禮盒歡迎訊息
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ApiResult GetGiftListHeader()
        {
            if (User.Identity.Id == 0)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.UserNoSignIn
                };
            }

            var data = MGMFacade.ApiGiftHeaderGet();

            if (!data.Any()) //至少要有兩筆
            {
                return new ApiResult
                {
                    Code = ApiResultCode.DataNotFound,
                };
            }

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = new { Style1 = data[0] ?? null, Style2 = data[1] ?? null }
            };
        }

        #endregion

        #region method


        #endregion method
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using LunchKingSite.BizLogic.Model.API;
using System.Threading;
using LunchKingSite.Core;
using LunchKingSite.WebLib.Component;
using LunchKingSite.BizLogic.Facade;
using System.Web.Http;
using LunchKingSite.DataOrm;
using LunchKingSite.Core.Component;
using LunchKingSite.WebApi.Controllers.Base;
using LunchKingSite.WebApi.Core.OAuth;
using LunchKingSite.WebApi.Core;

namespace LunchKingSite.WebApi.Controllers
{
    [RequireHttps]
    [ForceCamelCase]
    public class MembersController : ApiControllerBase
    {
        private IMemberProvider mp = ProviderFactory.Instance().GetDefaultProvider<IMemberProvider>();

        #region 會員認證  簡訊
        [HttpGet]
        public ApiResult SendSms(string userId, string mobile)
        {
            ApiResult apiResult = new ApiResult();

            if (!string.IsNullOrEmpty(userId))
            {
                int intuserId = 0;
                int.TryParse(userId, out intuserId);

                Member mem = mp.MemberGetbyUniqueId(intuserId);

                if (mem != null)
                {
                    if (mem.UniqueId.Equals(intuserId))
                    {
                        int sendCount;
                        SendMobileAuthCodeReply result = MemberUtility.SendMobileAuthCode(mem.UniqueId, mobile, out sendCount);

                        //故意等待，分擔一點簡訊好慢的感覺
                        Thread.Sleep(1000);

                        if (result == SendMobileAuthCodeReply.Success)
                        {
                            apiResult.Code = ApiResultCode.Success;
                            apiResult.Data = new { sendCount = sendCount };
                            apiResult.Message = "發送成功";
                        }
                        else if (result == SendMobileAuthCodeReply.TooManySmsSent)
                        {
                            apiResult.Code = ApiResultCode.Error;
                            apiResult.Data = new { sendCount = sendCount };
                            apiResult.Message = "發送次數超過上限";
                        }
                        else
                        {
                            apiResult.Code = ApiResultCode.Error;
                            apiResult.Data = new { sendCount = sendCount };
                            apiResult.Message = "發送失敗";
                        }
                    }
                    else
                    {
                        apiResult.Code = ApiResultCode.Error;
                        apiResult.Message = "使用者不存在";
                    }
                }
                else
                {
                    apiResult.Code = ApiResultCode.Error;
                    apiResult.Message = "使用者不存在";
                }
            }
            else
            {
                apiResult.Code = ApiResultCode.Error;
                apiResult.Message = "使用者不存在";
            }
            return apiResult;
        }

        [HttpGet]
        public ApiResult ValidateCode(string userId, string mobile, string code)
        {
            ApiResult apiResult = new ApiResult();

            if (!string.IsNullOrEmpty(userId))
            {
                int intuserId = 0;
                int.TryParse(userId, out intuserId);

                Member mem = mp.MemberGetbyUniqueId(intuserId);
                if (mem != null)
                {
                    if (mem.UniqueId.Equals(intuserId))
                    {
                        bool needSetPassword;
                        MemberValidateMobileCodeReply result = MemberFacade.ValidateMobileCode(mem.UniqueId, mobile, code, true, out needSetPassword);
                        //故意等一下，讓驗證過程好像有在做什麼的感覺
                        Thread.Sleep(1000);

                        if (result == MemberValidateMobileCodeReply.Success)
                        {
                            apiResult.Code = ApiResultCode.Success;
                            apiResult.Message = "驗證成功";
                        }
                        else if (result == MemberValidateMobileCodeReply.CodeError)
                        {
                            apiResult.Code = ApiResultCode.Error;
                            apiResult.Message = "驗證碼輸入有誤，麻煩您重新輸入。";
                        }
                        else if (result == MemberValidateMobileCodeReply.TimeError)
                        {
                            apiResult.Code = ApiResultCode.Error;
                            apiResult.Message = "驗證碼已過有效時間。";
                        }
                        else if (result == MemberValidateMobileCodeReply.MobileUsedByOtherMember)
                        {
                            apiResult.Code = ApiResultCode.Error;
                            apiResult.Message = "這個手機號碼己經被使用了。";
                        }
                        else
                        {
                            apiResult.Code = ApiResultCode.Error;
                            apiResult.Message = "系統繁忙中，請稍後再試!!";
                        }
                    }
                    else
                    {
                        apiResult.Code = ApiResultCode.Error;
                        apiResult.Message = "使用者不存在";
                    }
                }
                else
                {
                    apiResult.Code = ApiResultCode.Error;
                    apiResult.Message = "使用者不存在";
                }
            }
            else
            {
                apiResult.Code = ApiResultCode.Error;
                apiResult.Message = "使用者不存在";
            }
            return apiResult;
        }

        [HttpGet]
        public ApiResult SetPassword(string userId, string password)
        {
            ApiResult apiResult = new ApiResult();

            if (!string.IsNullOrEmpty(userId))
            {
                int intuserId = 0;
                int.TryParse(userId, out intuserId);

                Member mem = mp.MemberGetbyUniqueId(intuserId);
                if (mem != null)
                {
                    if (mem.UniqueId.Equals(intuserId))
                    {
                        MemberSetPasswordReply result = MemberFacade.SetMobleMemberPassword(mem.UniqueId, password);


                        if (result == MemberSetPasswordReply.Success)
                        {
                            apiResult.Code = ApiResultCode.Success;
                            apiResult.Message = "驗證成功";
                        }
                        else if (result == MemberSetPasswordReply.EmptyError)
                        {
                            apiResult.Code = ApiResultCode.Error;
                            apiResult.Message = "驗證碼輸入有誤，麻煩您重新輸入。";
                        }
                        else if (result == MemberSetPasswordReply.DataError)
                        {
                            apiResult.Code = ApiResultCode.Error;
                            apiResult.Message = "驗證碼已過有效時間。";
                        }
                        else if (result == MemberSetPasswordReply.OtherError)
                        {
                            apiResult.Code = ApiResultCode.Error;
                            apiResult.Message = "這個手機號碼己經被使用了。";
                        }
                        else
                        {
                            apiResult.Code = ApiResultCode.Error;
                            apiResult.Message = "系統繁忙中，請稍後再試!!";
                        }
                    }
                    else
                    {
                        apiResult.Code = ApiResultCode.Error;
                        apiResult.Message = "使用者不存在";
                    }
                }
                else
                {
                    apiResult.Code = ApiResultCode.Error;
                    apiResult.Message = "使用者不存在";
                }
            }
            else
            {
                apiResult.Code = ApiResultCode.Error;
                apiResult.Message = "使用者不存在";
            }
            return apiResult;
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.BizLogic.Models.LineShopping;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.WebApi.Core.OAuth;
using LunchKingSite.WebApi.Models;
using Newtonsoft.Json;

namespace LunchKingSite.WebApi.Controllers
{
    /// <summary>
    /// 導購
    /// </summary>
    public class ShoppingGuideController : BaseController
    {
        private ISysConfProvider _config = ProviderFactory.Instance().GetConfig();
        private IChannelProvider _cp = ProviderFactory.Instance().GetProvider<IChannelProvider>();
        private IPponProvider _pp = ProviderFactory.Instance().GetProvider<IPponProvider>();

        /// <summary>
        /// 取得導購分類
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.ShoppingGuide)]
        public ApiResult GetChannelData(GetDealTypeInfoModel model)
        {
            if (!model.IsValid)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError,
                    Message = "超出查詢日期範圍(N+2)。"
                };
            }

            if (!model.IsDealTypeExists)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError,
                    Message = "deal type id is not exist."
                };
            }

            var ccp = OAuthFacade.GetChannelClientProperty(AppId);

            ConcurrentDictionary<Guid, List<DealCategory>> dealCategoryCol;
            PponDealSpecialCityType cityType = PponDealSpecialCityType.PponAndPiinlife;

            switch ((ShoppingGuideDealType)model.ChannelId)
            {
                case ShoppingGuideDealType.Piinlife:
                    cityType = PponDealSpecialCityType.Piinlife;
                    break;
                case ShoppingGuideDealType.BeautyAndLeisure:
                case ShoppingGuideDealType.Coupon:
                case ShoppingGuideDealType.Delivery:
                case ShoppingGuideDealType.Food:
                case ShoppingGuideDealType.Travel:
                    cityType = PponDealSpecialCityType.WithOutSpecialCity;
                    break;
            }

            var bids = PponFacade.GetShoppingGuideDealBid((ShoppingGuideDealType)model.ChannelId, model.GetQueryDate()
                    , out dealCategoryCol
                    , ccp.GeneralGrossMargin ?? 1.0m
                    , ccp.GeneralCouponGrossMargin ?? 1.0m
                    , ccp.GeneralDeliveryGrossMargin ?? 1.0m, cityType, true, true);

            var queryToken = _cp.ShoppingGuideQueryCollectionSet(AppId, bids);

            var result = new DealTypeInfo
            {
                QueryToken = queryToken.ToString(),
                ChannelId = model.ChannelId,
                ChannelName = LunchKingSite.Core.Helper.GetEnumDescription((ShoppingGuideDealType)model.ChannelId),
                Qty = bids.Count,
                Pages = bids.Count % _config.ShoppingGuidePageSize > 0 ? bids.Count / _config.ShoppingGuidePageSize + 1 : bids.Count / _config.ShoppingGuidePageSize
            };

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = result
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.ShoppingGuide)]
        public ApiResult GetDealData(GetDealDataModel model)
        {
            if (!model.CheckPage)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError,
                    Message = "page value error."
                };
            }

            Guid queryToken;
            if (!Guid.TryParse(model.QueryToken, out queryToken))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError,
                    Message = "query token value error."
                };
            }

            var ccp = OAuthFacade.GetChannelClientProperty(AppId);
            var bids = _cp.ShoppingGuideQueryCollectionGet(queryToken, AppId).OrderBy(x => x).ToList();
            var maxPage = bids.Count % _config.ShoppingGuidePageSize > 0
                ? bids.Count / _config.ShoppingGuidePageSize + 1
                : bids.Count / _config.ShoppingGuidePageSize;

            if (model.Page > maxPage) model.Page = maxPage;
            var queryBids = bids.Skip((model.Page - 1) * _config.ShoppingGuidePageSize).Take(_config.ShoppingGuidePageSize).ToList();

            var dealCategoryCol = ChannelFacade.GetDealCategoryColByBid(queryBids);

            if (!dealCategoryCol.Any())
            {
                var vadcList = _pp.ViewAllDealCategoryGetListByBidList(queryBids).ToList();
                foreach (var vadc in vadcList)
                {
                    dealCategoryCol.AddOrUpdate(vadc.Bid,
                        new List<DealCategory> { new DealCategory { Id = vadc.Cid, Name = vadc.Name } },
                        (key, oldValue) =>
                        {
                            oldValue.Add(new DealCategory { Id = vadc.Cid, Name = vadc.Name });
                            return oldValue;
                        });
                }
            }

            var data = queryBids.Select(bid => PponFacade.DealInfoTranslateGet(bid, ccp.CpaCode, dealCategoryCol.First(x => x.Key == bid).Value)).ToList();

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = data
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.ShoppingGuide)]
        public ApiResult GetAreaList()
        {
            List<string> excludeCityCode = new List<string> { "SKM", "FAM" };

            var result = PponCityGroup.DefaultPponCityGroup.GetPponcityForPublic()
                .Where(x => !excludeCityCode.Contains(x.CityCode))
                .Select(pponCity => new DealAreaInfo { Id = pponCity.CityId, Name = pponCity.CityName, Code = pponCity.CityCode }).ToList();
            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = result
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.ShoppingGuide)]
        public ApiResult GetDealTypeList()
        {
            var result = SystemCodeManager.GetDealType().Select(x => new { Id = x.CodeId, Name = x.CodeName }).ToList();
            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = result
            };
        }

        #region LineShoppingGuide

        /// <summary>
        /// LineFeed檔次資料
        /// </summary>
        /// <returns></returns>
        [HttpGet, HttpPost]
        public List<LineProductData> GetLineProductFull()
        {
            var result = PponFacade.GetLineProductList();
            return result;
        }

        /// <summary>
        /// LineFeed檔次類別
        /// </summary>
        /// <returns></returns>
        [HttpGet, HttpPost]
        public List<LineProductCategory> GetLineCateFull()
        {
            var result = ChannelFacade.GetLineProductCategoryList();

            return result;
        }

        /// <summary>
        /// LinePartialFeed檔次資料
        /// </summary>
        /// <returns></returns>
        [HttpGet, HttpPost]
        public List<LineProductData> GetLineProductPartial()
        {
            var result = PponFacade.GetLineProductPartialList();

            return result;
        }

        /// <summary>
        /// 是否為排除的檔次(包含處理黑白名單)
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        [HttpPost, HttpGet]
        public ApiResult LineShopShouldExcludeIViewPponDeal(Guid bid)
        {
            var theDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid);
            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = PponFacade.ShouldExcludeIViewPponDeal(theDeal)
            };
        }

        /// <summary>
        /// 以Bid取得LineProductFeed分類資料
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        [HttpPost, HttpGet]
        public ApiResult GetLineProductCategoryByBid(Guid bid)
        {
            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = PponFacade.GetLineProductCategoryByBid(bid)
            };
        }

        #endregion

    }
}

﻿using System;
using System.Web;
using System.Web.Http;
using System.Web.Security;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.WebApi.Core;
using LunchKingSite.WebApi.Core.Attributes;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebApi.Core.OAuth;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core.Component;
using System.Runtime.Caching;
using LunchKingSite.BizLogic.Model.Refund;
using LunchKingSite.BizLogic.Models.ReturnForm;
using LunchKingSite.BizLogic.Models.Channel;
using System.Net.Mime;
using System.Net.Http;
using System.Net.Http.Headers;

namespace LunchKingSite.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class SystemController : ApiController
    {

        /// <summary>
        /// 測試用 api
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public dynamic Info()
        {
            string testDb = string.Empty;
            try
            {
                int uid = MemberFacade.GetUniqueId("unicorn@17life.com.tw");
                if (uid == 0)
                {
                    testDb = "data not fond";
                }
                testDb = "unicorn@17life.com has id " + uid;
            } 
            catch (Exception ex)
            {
                testDb = "fail, " + ex.Message;
            }
            return new
            {
                Host = Environment.MachineName,
                Time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                User = User.Identity.Name,
                Ip = HttpContext.Current.Request.UserHostAddress,
                TestDb = testDb
            };
        }

        [HttpGet]
        public dynamic Refresh()
        {
            SystemFacade.IncrementImageVer();
            return new
            {
                ImageVer = LunchKingSite.Core.Helper.GetImageVer()
            };
        }

        /// <summary>
        /// 檔次適用折價券，查詢與清除暫存
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("api/system/discountusage")]
        public HttpResponseMessage DiscountUsage()
        {
            int clearcache;
            Guid bid;
            Guid.TryParse(Request.GetQueryString("bid"), out bid);
            int.TryParse(Request.GetQueryString("clearcache"), out clearcache);
            if (bid != Guid.Empty)
            {
                if (clearcache == 1)
                {
                    DiscountManager.GetDealDiscountTerm(bid, true);
                    PromotionFacade.ClearDiscountCampaignGetByDayForBuyCache();
                }
            }
            List<string> campaignNames;
            var discountUseType = PromotionFacade.GetDiscountUseType(bid, out campaignNames);
            var notAllow = ProviderFactory.Instance().GetDefaultProvider<IOrderProvider>()
                .DiscountLimitGetByBid(bid).IsLoaded;

            string html = @"
<html>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
</head>
<body><pre>
" +
JsonConvert.SerializeObject(new
{
    _可使用折價券 = discountUseType ? "有" : "無",
    _參考列表 = campaignNames,
    _禁用折價券 = notAllow ? "禁用" : "未禁用"
}, Formatting.Indented)
+ @"
</pre></body></html>
";
            var response = new HttpResponseMessage();
            response.Content = new StringContent(html);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("text/html");
            return response;
        }

        /// <summary>
        /// 查詢PChome倉儲api
        /// </summary>
        /// <returns></returns>
        [Route("api/system/wms/{*pathInfo}")]
        [HttpGet]
        public dynamic Wms()
        {
            string[] uriSegments = Request.RequestUri.AbsoluteUri.Split("/");
            string key = uriSegments[uriSegments.Length - 2];
            string value = uriSegments[uriSegments.Length - 1];
            if (key == "prod")
            {
                return PchomeWmsAPI.GetProd(value);
            }
            else if (key == "inventory")
            {
                return PchomeWmsAPI.GetInventories(value);
            }
            if (key == "purchaseorder")
            {
                return PchomeWmsAPI.GetPurchaseOrder(value);
            }
            else if (key == "order")
            {
                return PchomeWmsAPI.GetOrder(value);
            }
            else if (key == "progressstatus")
            {
                return PchomeWmsAPI.GetOrderProgressStatus(value);
            }
            else if (key == "logistic")
            {
                return PchomeWmsAPI.GetLogistics(value);
            }
            else if (key == "logistictrace")
            {
                return PchomeWmsAPI.GetLogisticTrace(value);
            }
            else if (key == "ordersbyvendororderid")
            {
                return PchomeWmsAPI.GetOrdersByVendorOrderId(value);
            }
            else if (key == "getreturnbyreturnId")
            {
                return PchomeWmsAPI.GetReturns(value);
            }
            else if (key == "getrefundbyrefundId")
            {
                return PchomeWmsAPI.GetRefund(value);
            }
            else if (key == "getrefundtracebyrefundId")
            {
                return PchomeWmsAPI.GetRefundTrace(value);
            }
            return new
            {
                Message = "nothing happened"
            };
        }

        /// <summary>
        /// 查詢PChome電子票券api
        /// </summary>
        /// <returns></returns>
        [Route("api/system/eticket/{*pathInfo}")]
        [HttpGet]
        public dynamic Eticket()
        {
            string[] uriSegments = Request.RequestUri.AbsoluteUri.Split("/");
            string key = uriSegments[uriSegments.Length - 2];
            string value = uriSegments[uriSegments.Length - 1];
            if (key == "prod")
            {
                return PChomeChannelAPI.GetProd(value);
            }
            else if (key == "contactlist")
            {
                string start = value.Split("-")[0];
                start = start.Substring(0, 4) + "/" + start.Substring(4, 2) + "/" + start.Substring(6, 2);
                string end = value.Split("-")[1];
                end = end.Substring(0, 4) + "/" + end.Substring(4, 2) + "/" + end.Substring(6, 2);
                return PChomeChannelAPI.GetContactList(start, end);
            }
            else if (key == "contactdetail")
            {
                return PChomeChannelAPI.GetContactDetail(value);
            }
            else if (key == "complaintlist")
            {
                string start = value.Split("-")[0];
                start = start.Substring(0, 4) + "/" + start.Substring(4, 2) + "/" + start.Substring(6, 2);
                string end = value.Split("-")[1];
                end = end.Substring(0, 4) + "/" + end.Substring(4, 2) + "/" + end.Substring(6, 2);
                return PChomeChannelAPI.GetComplaintList(start, end);
            }
            else if (key == "complaintdetail")
            {
                return PChomeChannelAPI.GetComplaintDetail(value);
            }
            return new
            {
                Message = "nothing happened"
            };
        }

        [HttpGet]
        public dynamic Trigger()
        {
            string msg = string.Empty;
            try
            {
                PromotionFacade.UpDateDiscountUseType();
                msg = "PromotionFacade.UpDateDiscountUseType 完成";
            }
            catch (Exception ex)
            {
                msg = ex.ToString();
            }
            return new
            {
                Message = msg
            };
        }

        /// <summary>
        /// 
        /// </summary>
        public class PushTestModel
        {
            /// <summary>
            /// 
            /// </summary>
            public int PushId { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public int UserId { get; set; }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public dynamic PushTest(PushTestModel model)
        {
            int? pushId = null;
            int userId = 0;
            if (model != null)
            {
                pushId = model.PushId;
                if (model.UserId != 0)
                {
                    userId = model.UserId;
                }
            }
            if (pushId == null)
            {
                return new
                {
                    error = "pushId can't be null"
                };
            }
            if (userId == 0)
            {
                userId = MemberFacade.GetUniqueId("unicorn@17life.com");
            }

            var np = ProviderFactory.Instance().GetProvider<INotificationProvider>();
            var mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            PushApp push = np.GetPushApp(pushId.Value);

            if (push.ActionEventPushMessageId == null)
            {
                return new
                {
                    error = "no ActionEventPushMessageId"
                };
            }
            var aepm = mp.ActionEventPushMessageGetByActionEventPushMessageId(push.ActionEventPushMessageId.Value);
            try
            {
                var deviceInfos = np.GetUserDeviceInfoModels(userId);
                List<string> tokens = deviceInfos.Select(t => t.Token).ToList();
                DeviceTokenCollection deviceTokens = new DeviceTokenCollection();
                if (tokens.Count == 0)
                {
                    return new
                    {
                        error = "no tokens"
                    };
                }
                foreach (string token in tokens)
                {
                    if (token.Length < 64)
                    {
                        continue;
                    }
                    DeviceToken dt = new DeviceToken
                    {
                        Token = token,
                    };
                    if (token.Length == 64)
                    {
                        dt.MobileOsType = (int)MobileOsType.iOS;
                    }
                    else if (token.Length > 64)
                    {
                        dt.MobileOsType = (int)MobileOsType.Android;
                    }
                    deviceTokens.Add(dt);
                }
                if (deviceTokens.Count == 0)
                {
                    return new
                    {
                        error = "no devices"
                    };
                }
                new PushService().PushMessage2(push, false, deviceTokens);
                return new
                {
                    content = aepm.Subject,
                    userId,
                    userName = User.Identity.Name,
                    tokens,
                    modes = deviceInfos.Select(t => t.ShortToken + "-" + t.Model + "-" + t.OsType + "-" + t.OsVer)
                };
            }
            catch (Exception ex)
            {
                return new
                {
                    error = ex.Message
                };
            }
        }

        public static string GetUrlResult(string url, object obj)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = WebRequestMethods.Http.Post;
            request.ContentType = "application/x-www-form-urlencoded";


            string postData = "json_info=" + JsonConvert.SerializeObject(obj);
            var dataBytes = Encoding.ASCII.GetBytes(postData);
            request.ContentLength = dataBytes.Length;

            request.ContentLength = dataBytes.Length;
            using (var requestStream = request.GetRequestStream())
            {
                requestStream.Write(dataBytes, 0, dataBytes.Length);
                requestStream.Flush();
            }

            using (var response = (HttpWebResponse)request.GetResponse())
            {
                using (var stream = response.GetResponseStream())
                {
                    using (var reader = new StreamReader(stream))
                    {
                        return reader.ReadToEnd();
                    }
                }
            }
        }

        [HttpPost]
        [OAuthScope(TokenScope.SystemManagement)]
        public dynamic ZipFileConvert(SendZipData model)
        {
            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = LunchKingSite.Core.Helper.ZipFileConvert(model.base64string, model.fileName, model.password)
            };

        }

        /// <summary>
        /// 伺服器間的同步操作
        /// </summary>
        /// <returns></returns>
        [InternalOnly]
        public ApiResult Command(CommandInputModel data)
        {
            bool work = false;
            if (data == null || string.IsNullOrEmpty(data.Command))
            {
                return new ApiResult
                {
                    Code = ApiResultCode.InputError,
                    Message = "參數錯誤"
                };
            }

            switch (data.Command)
            {
                case SystemFacade._CLEAR_CACHE:
                    if (data.ArgumentName == "GroupName")
                    {
                        SystemFacade.ClearCacheByGroup(data.ArgumentValue);
                        work = true;
                    }
                    if (data.ArgumentName == "HomepageBlocks")
                    {
                        string indexCacheKey = string.Format("page://m-home/{0}", DateTime.Now.ToString("yyyyMMddHH"));
                        MemoryCache.Default.Remove(indexCacheKey);
                        string cacheKey = string.Format("api://GetHomepage/{0}", DateTime.Now.ToString("yyyyMMddHH"));
                        MemoryCache.Default.Remove(cacheKey);
                        work = true;
                    }
                    if (data.ArgumentName == "Banner")
                    {
                        HttpContext.Current.Cache.Remove(data.ArgumentValue);
                    }
                    break;
                case SystemFacade._API_SYSTEM_MANAGER:
                    ApiSystemManager.ReloadManagerData();
                    work = true;
                    break;
                case SystemFacade._MEMBER_ROLE_MANAGEMENT:
                    CommonFacade.CreateSystemFunctionPrivilege();
                    work = true;
                    break;
                case SystemFacade._CURATION_CLEAR_CACHE:
                    HttpRuntime.Cache.Remove("curation");
                    work = true;
                    break;
                case SystemFacade._API_PROMO_EVENT_MANAGER:
                    ApiPromoEventManager.LoadDataManager(true);
                    work = true;
                    break;
                case SystemFacade._CHANNEL_SELLER_TREE_CLEAR:
                    ChannelFacade.ResetSellerRole();
                    work = true;
                    break;
                case SystemFacade._RESET_FOR_DEAL_TIME_SLOTS_CHANGE:
                    ViewPponDealManager.DefaultManager.ResetForDealTimeSlotsChange();
                    break;
                case SystemFacade._CLEAR_CACHE_BY_OBJ:
                    if (data.ArgumentName == "ApiEventPromo")
                    {
                        SystemFacade.ClearCacheByCacheKey<ApiEventPromo>(data.ArgumentValue);
                        work = true;
                        break;
                    }
                    Type type = Type.GetType(data.ArgumentName);
                    if (type != null)
                    {
                        SystemFacade.ClearCacheByCacheKey(type, data.ArgumentValue);
                    }
                    break;
                case SystemFacade._RELOAD_CATEGORY:
                    CategoryManager.ReloadManager();
                    PponDealPreviewManager.ReloadManager();
                    work = true;
                    break;
                case SystemFacade._SET_RUNTIME_CONFIG:
                    ISysConfProvider config = ProviderFactory.Instance().GetConfig();
                    LunchKingSite.Core.Helper.SetPropertyValue(config, data.ArgumentName, data.ArgumentValue);
                    work = true;
                    break;
                case SystemFacade._INCR_IMG_VER:
                    LunchKingSite.Core.Helper.IncrImageVer();
                    break;
            }

            if (work == false)
            {
                return new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = "沒有任何事發生"
                };
            }
            return new ApiResult
            {
                Code = ApiResultCode.Success
            };
        }


        /// <summary>
        /// 補Line導購拋單 一次一天
        /// </summary>
        /// <param name="startDate"></param>
        public void GetLineShopOrderInfo(string startDate)
        {
            DateTime strtime = DateTime.ParseExact(startDate, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
            DateTime endtime = strtime.AddDays(1);
            OrderFacade.LineShopOrderInfoAPI(strtime, endtime);
        }

        /// <summary>
        /// 補Line導購二次拋單
        /// </summary>
        /// <param name="finishDate"></param>
        public void GetLineShopOrderFinish(string finishDate)
        {
            DateTime date = DateTime.ParseExact(finishDate, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
            OrderFacade.LineShopOrderFinishAPI(date);
        }

        /// <summary>
        /// 模擬其他會員的身份
        /// </summary>
        /// <returns></returns>
        [ApiUserValidator]
        [RequireHttps]
        [DeveloperOnly]
        public ApiResult Impersonate(ImpersonateInputModel model)
        {
            // 被檢視者(權限只有RegisteredUser，才可被模擬)
            if (string.IsNullOrEmpty(model.UserName))
            {
                return new ApiResult { Code = ApiResultCode.InputError };
            }

            string userName = model.UserName;
            Member mem = MemberFacade.GetMember(userName);
            if (mem.IsLoaded)
            {
                userName = mem.UserName;
            }

            // 被模擬者只能是一般的使用者
            string[] roleNames = Roles.GetRolesForUser(userName);
            bool isAllowImpersonate = roleNames.Length == 1 && roleNames[0].Equals(MemberRoles.RegisteredUser.ToString("g"));
            // 登入者(權限有Administrator或MemberAdmin，才可模擬)
            bool isAdministrator = Roles.IsUserInRole(MemberRoles.Administrator.ToString("g"));
            bool isMemberAdmin = Roles.IsUserInRole(MemberRoles.MemberAdmin.ToString("g"));

            if (isAllowImpersonate || isAdministrator || isMemberAdmin)
            {
                MemberUtility.UserForceSignIn(userName);
                return new ApiResult
                {
                    Code = ApiResultCode.Success
                };
            }
            return new ApiResult { Code = ApiResultCode.InputError, Message = "無管理者權限" };
        }

        /// <summary>
        /// 測試寄一般通知信
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.SystemManagement)]
        public ApiResult SendNowCommonNotification(SendNowCommonNotificationModel model)
        {
            EmailFacade.SendNowCommonNotification(model.Subject, model.Greeting, model.Message,
                new List<string> { model.MailTo });

            return new ApiResult
            {
                Code = ApiResultCode.Success
            };
        }

        /// <summary>
        /// 取得賣家通知信名單
        /// </summary>
        /// <param name="sellerGuid"></param>
        /// <returns></returns>
        [HttpPost]
        [OAuthScope(TokenScope.SystemManagement)]
        public ApiResult GetSellerContractsEmailList(Guid sellerGuid)
        {
            var data = ISPFacade.GetSellerContractsEmailList(sellerGuid);
            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = data
            };
        }

        [HttpPost]
        [OAuthScope(TokenScope.SystemManagement)]
        public ApiResult CheckPreventingOversell(PreventingOversell model) 
        {
            var cp = ProviderFactory.Instance().GetProvider<ISysConfProvider>();

            var vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(model.Bid);
            if (!vpd.IsLoaded) 
            {
                return new ApiResult
                {
                    Code = ApiResultCode.DataNotFound,
                };
            }

            int saleMultipleBase = (LunchKingSite.Core.Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.GroupCoupon)
                                                  ? vpd.SaleMultipleBase ?? 1
                                                  : 1);
            int pid = 0;
            string msg = string.Empty;
            var sp = SPs.PreventingOversell(vpd.BusinessHourGuid, model.Qty, model.UserId, saleMultipleBase, cp.PreventingOversellTimeoutSec, decimal.ToInt32(vpd.OrderTotalLimit ?? 0));
            sp.Execute();
            pid = Convert.ToInt32(sp.OutputValues[0]);
            bool result = Convert.ToInt32(sp.OutputValues[1]) == 1;
            msg = sp.OutputValues[2].ToString();

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = result,
                Message = string.Format("pid:{0}, msg:{1}", pid.ToString(), msg)
            };
        }

        [HttpPost]
        [OAuthScope(TokenScope.SystemManagement)]
        public ApiResult CheckPrizeOverdraw(PreventingOverdraw model)
        {
            var cp = ProviderFactory.Instance().GetProvider<ISysConfProvider>();

            int pid = 0;
            string msg = string.Empty;
            var sp = SPs.PrizeOverdraw(model.ItemId, model.Qty, model.UserId, cp.PreventingOversellTimeoutSec, model.Limit);
            sp.Execute();
            pid = Convert.ToInt32(sp.OutputValues[0]);
            bool result = Convert.ToInt32(sp.OutputValues[1]) == 1;
            msg = sp.OutputValues[2].ToString();

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = result,
                Message = string.Format("pid:{0}, msg:{1}", pid.ToString(), msg)
            };
        }

        [HttpPost]
        [OAuthScope(TokenScope.SystemManagement)]
        public ApiResult GetPrizeRecordCount(PrizeRecordModel model)
        {
            var cp = ProviderFactory.Instance().GetProvider<IChannelEventProvider>();

            int result = cp.GetPrizeDrawRecordCount(model.EventId, model.ItemId, (PrizeDrawRecordStatus)model.Status);

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = result
            };
        }

        [HttpPost]
        [OAuthScope(TokenScope.SystemManagement)]
        public ApiResult GetPrizeRecordItems(PrizeRecordModel model)
        {
            var cp = ProviderFactory.Instance().GetProvider<IChannelEventProvider>();

            var result = cp.GetPrizeDrawRecordItemIds(model.EventId, (PrizeDrawRecordStatus)model.Status);

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = result
            };
        }

        [HttpPost]
        [OAuthScope(TokenScope.SystemManagement)]
        public ApiResult EntrepotVerify(EntrepotVerifyModel model)
        {
            return LifeEntrepotApi.VerifyNotifity(model.CouponId, model.SequenceNumber, model.Code, (NotifyVoucherStatus)model.VoucherStatus, Guid.Empty, string.Empty);
        }

        [HttpPost]
        [DeveloperOnly]
        public ApiResult IgnoreRefundTest()
        {
            //退貨單
            var returnForms = ReturnFormRepository.FindScheduledForRefunding();

            //取得被列入忽略批次退貨的訂單
            var ignoreOrderDic = ReturnFormRepository.GetIgnoreBatchRefundOrders(returnForms);

            var data = new List<IgnoreRefundModel>();
            foreach (ReturnFormEntity form in returnForms)
            {
                //符合略過退貨，組織手動退貨清單
                if (ignoreOrderDic.ContainsKey(form.OrderGuid))
                {
                    var ignoreOrder = ignoreOrderDic[form.OrderGuid];
                    data.Add(new IgnoreRefundModel
                    {
                        ReturnFormId = form.Id,
                        OrderId = form.OrderId,
                        OrderGuid = form.OrderGuid,
                        ItemName = ignoreOrder.ItemName
                    });
                    continue;
                }

                //做退貨
                //ReturnService.Refund(form);
            }

            return new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = data
            };
        }

        #region Input Models
        public class PrizeRecordModel
        {
            public int EventId { get; set; }
            public int ItemId { get; set; }
            public int Status { get; set; }
        }
        public class PreventingOverdraw
        {
            public int ItemId { get; set; }
            public int UserId { get; set; }
            public int Qty { get; set; }
            public int Limit { get; set; }
        }
        public class PreventingOversell 
        {
            public Guid Bid { get; set; }
            public int UserId { get; set; }
            public int Qty { get; set; }
        }
        /// <summary>
        /// 
        /// </summary>
        public class CommandInputModel
        {
            /// <summary>
            /// 
            /// </summary>
            public string Command { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string ArgumentName { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string ArgumentValue { get; set; }
        }
        /// <summary>
        /// 
        /// </summary>
        public class ImpersonateInputModel : IApiUserInputModel
        {
            /// <summary>
            /// 這邊要傳的是 app user id
            /// </summary>
            public string UserId { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string UserName { get; set; }
        }

        public class SendZipData
        {
            /// <summary>
            ///  
            /// </summary>
            [JsonProperty("fileName")]
            public string fileName { get; set; }

            /// <summary>
            ///  
            /// </summary>
            [JsonProperty("password")]
            public string password { get; set; }

            /// <summary>
            ///  
            /// </summary>
            [JsonProperty("base64string")]
            public string base64string { get; set; }

        }

        /// <summary>
        /// 
        /// </summary>
        public class SendNowCommonNotificationModel
        {
            /// <summary>
            /// 
            /// </summary>
            [JsonProperty("subject")]
            public string Subject { get; set; }
            /// <summary>
            /// 
            /// </summary>
            [JsonProperty("greeting")]
            public string Greeting { get; set; }
            /// <summary>
            /// 
            /// </summary>
            [JsonProperty("message")]
            public string Message { get; set; }
            /// <summary>
            /// 
            /// </summary>
            [JsonProperty("mailTo")]
            public string MailTo { get; set; }
        }

        #endregion
    }



}

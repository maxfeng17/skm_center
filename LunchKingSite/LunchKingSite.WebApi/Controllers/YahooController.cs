﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using System;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Xml;
using LunchKingSite.WebApi.Core;
using LunchKingSite.WebApi.Core.OAuth;
using Helper = LunchKingSite.Core.Helper;

namespace LunchKingSite.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class YahooController : BaseController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="isPreview"></param>
        /// <returns></returns>
        [HttpGet]
        [OAuthScope(TokenScope.Deal)]
        public HttpResponseMessage GetDeals(string accessToken, bool isPreview = false)
        {
            //try
            //{
            //    string cpaCode = "YH_BMC";
            //    XmlDocument yahooDeal = PponFacade.YahooDealGetByDate(DateTime.Now, cpaCode, isPreview);
            //    string content = yahooDeal.OuterXml;
            //    ApiReturnObject rtnObject = new ApiReturnObject();
            //    rtnObject.Code = ApiReturnCode.Success;
            //    rtnObject.Data = string.Format("異動資料筆數共{0}筆", yahooDeal.GetElementsByTagName("goods").Count);
            //    SetApiLog("GetDeals", AppId, new { AccessToken = accessToken }, rtnObject, Helper.GetClientIP());
            //    return new HttpResponseMessage() { Content = new StringContent(content, Encoding.UTF8, "application/xml") };
            //}
            //catch (Exception ex)
            //{
            //    ApiReturnObject rtnObject = new ApiReturnObject();
            //    rtnObject.Code = ApiReturnCode.InputError;
            //    rtnObject.Data = ex;
            //    rtnObject.Message = "執行階段發生錯誤";
            //    SetApiLog("GetDeals", AppId, new { AccessToken = accessToken}, rtnObject, Helper.GetClientIP());
            //    throw new HttpErrorResponseException("B001", "執行階段發生錯誤");
            //}

            throw new HttpErrorResponseException("B001", "2016/8/25起終止與雅虎大團購合作");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="isPreview"></param>
        /// <returns></returns>
        [HttpGet]
        [OAuthScope(TokenScope.Deal)]
        public HttpResponseMessage GetPponDeals(string accessToken, bool isPreview = false)
        {
            //try
            //{
            //    string cpaCode = "YH_BMC";
            //    XmlDocument yahooDeal = PponFacade.YahooPponDealGetByDate(DateTime.Now, cpaCode, isPreview);
            //    string content = yahooDeal.OuterXml;
            //    ApiReturnObject rtnObject = new ApiReturnObject();
            //    rtnObject.Code = ApiReturnCode.Success;
            //    rtnObject.Data = string.Format("異動資料筆數共{0}筆", yahooDeal.GetElementsByTagName("goods").Count);
            //    SetApiLog("GetDeals", AppId, new { AccessToken = accessToken }, rtnObject, Helper.GetClientIP());
            //    return new HttpResponseMessage() { Content = new StringContent(content, Encoding.UTF8, "application/xml") };
            //}
            //catch (Exception ex)
            //{
            //    ApiReturnObject rtnObject = new ApiReturnObject();
            //    rtnObject.Code = ApiReturnCode.InputError;
            //    rtnObject.Data = ex;
            //    rtnObject.Message = "執行階段發生錯誤";
            //    SetApiLog("GetDeals", AppId, new { AccessToken = accessToken }, rtnObject, Helper.GetClientIP());
            //    throw new HttpErrorResponseException("B001", "執行階段發生錯誤");
            //}

            throw new HttpErrorResponseException("B001", "2016/8/25起終止與雅虎大團購合作");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="accessToken"></param>
        /// <param name="isPreview"></param>
        /// <returns></returns>
        [HttpGet]
        [OAuthScope(TokenScope.Deal)]
        public HttpResponseMessage NewGetPponDeals(string accessToken, bool isPreview = false)
        {
            //try
            //{
            //    string cpaCode = "YH_BMC";
            //    XmlDocument yahooDeal = PponFacade.NewYahooPponDealGetByDate(DateTime.Now, cpaCode, isPreview);
            //    string content = yahooDeal.OuterXml;
            //    ApiReturnObject rtnObject = new ApiReturnObject();
            //    rtnObject.Code = ApiReturnCode.Success;
            //    rtnObject.Data = string.Format("異動資料筆數共{0}筆", yahooDeal.GetElementsByTagName("goods").Count);
            //    SetApiLog("GetDeals", AppId, new { AccessToken = accessToken }, rtnObject, Helper.GetClientIP());
            //    return new HttpResponseMessage() { Content = new StringContent(content, Encoding.UTF8, "application/xml") };
            //}
            //catch (Exception ex)
            //{
            //    ApiReturnObject rtnObject = new ApiReturnObject();
            //    rtnObject.Code = ApiReturnCode.InputError;
            //    rtnObject.Data = ex;
            //    rtnObject.Message = "執行階段發生錯誤";
            //    SetApiLog("GetDeals", AppId, new { AccessToken = accessToken }, rtnObject, Helper.GetClientIP());
            //    throw new HttpErrorResponseException("B001", "執行階段發生錯誤");
            //}

            throw new HttpErrorResponseException("B001", "2016/8/25起終止與雅虎大團購合作");
        }

    }
}

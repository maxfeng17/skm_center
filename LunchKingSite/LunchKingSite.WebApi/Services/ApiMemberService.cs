﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;

namespace LunchKingSite.WebApi.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class ApiMemberService
    {
        /// <summary>
        /// 發送會員認證簡訊
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="mobile"></param>
        /// <param name="force">會員資料已存在，是否強制發送，於忘記密碼時使用</param>
        /// <returns></returns>
        public static ApiResult SendMobileAuthCode(int userId, string mobile, bool force)
        {
            ApiResult result = new ApiResult();
            int sendCount;
            DateTime nextCanSendSmsTime;
            SendMobileAuthCodeReply reply = MemberUtility.SendMobileAuthCode(userId, mobile, force, out sendCount, out nextCanSendSmsTime);

            switch (reply)
            {
                case SendMobileAuthCodeReply.Success:
                    result.Code = ApiResultCode.Success;
                    result.Data = new { sendCount = sendCount, mobile = mobile };
                    result.Message = "發送成功";
                    break;

                case SendMobileAuthCodeReply.MobileUsedByOtherMember:
                    result.Code = ApiResultCode.MobileAuthUsedByOtherMember;
                    result.Data = new { sendCount = sendCount, mobile = mobile };
                    result.Message = "這個手機號碼認證過了";
                    break;

                case SendMobileAuthCodeReply.TooManySmsSent:
                    result.Code = ApiResultCode.MobileAuthTooManySmsSent;
                    result.Data = new
                    {
                        sendCount = sendCount,
                        nextCanSendSmsTime = nextCanSendSmsTime,
                        mobile = mobile
                    };
                    result.Message = string.Format("下次可發送時間：{0}", nextCanSendSmsTime.ToString("yyyy/MM/dd HH:mm"));
                    break;

                default:
                    result.Code = ApiResultCode.MobileAuthSendSmsFail;
                    result.Data = new { sendCount = sendCount, mobile = mobile };
                    result.Message = "發送失敗";
                    break;
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mem"></param>
        /// <param name="mobile"></param>
        /// <param name="code"></param>
        /// <param name="force">忘記密碼時 force 需要設成 true</param>
        /// <param name="needSetPassword"></param>
        /// <param name="resetPasswordKey"></param>
        /// <returns></returns>
        public static ApiResult ValidateMobileCode(Member mem, string mobile, string code, bool force,
            out bool needSetPassword, out string resetPasswordKey)
        {
            ApiResult apiResult = new ApiResult();
            MemberValidateMobileCodeReply reply = MemberFacade.ValidateMobileCode(mem.UniqueId, mobile, code, force, 
                out needSetPassword, out resetPasswordKey);

            switch (reply)
            {
                case MemberValidateMobileCodeReply.Success:
                case MemberValidateMobileCodeReply.ActivedAndSkip:
                    apiResult.Code = ApiResultCode.Success;
                    apiResult.Message = "驗證成功";
                    break;

                case MemberValidateMobileCodeReply.CodeError:
                    apiResult.Code = ApiResultCode.MobileValidateCodeError;
                    apiResult.Message = "驗證碼輸入有誤，麻煩您重新輸入。";
                    break;

                case MemberValidateMobileCodeReply.TimeError:
                    apiResult.Code = ApiResultCode.MobileValidateTimeError;
                    apiResult.Message = "驗證碼已過有效時間。";
                    break;

                case MemberValidateMobileCodeReply.MobileUsedByOtherMember:
                    apiResult.Code = ApiResultCode.MobileValidateUsedByOtherMember;
                    apiResult.Message = "這個手機號碼己經被使用了。";
                    break;

                default:
                    apiResult.Code = ApiResultCode.MobileValidateError;
                    apiResult.Message = "系統繁忙中，請稍後再試!!";
                    break;
            }
            return apiResult;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public static ApiResult SetMobileMemberPassword(int userId, string password)
        {
            ApiResult apiResult = new ApiResult();
            MemberSetPasswordReply result = MemberFacade.SetMobleMemberPassword(userId, password);

            switch (result)
            {
                case MemberSetPasswordReply.Success:
                    apiResult.Code = ApiResultCode.Success;
                    apiResult.Message = "密碼設定完成";
                    break;

                case MemberSetPasswordReply.EmptyError:
                    apiResult.Code = ApiResultCode.MobileSetPasswordEmptyError;
                    apiResult.Message = "驗證碼輸入有誤，麻煩您重新輸入。";
                    break;

                case MemberSetPasswordReply.DataError:
                    apiResult.Code = ApiResultCode.MobileSetPasswordDataError;
                    apiResult.Message = "驗證碼已過有效時間。";
                    break;

                case MemberSetPasswordReply.OtherError:
                    apiResult.Code = ApiResultCode.MobileSetPasswordOtherError;
                    apiResult.Message = "這個手機號碼己經被使用了。";
                    break;

                case MemberSetPasswordReply.PasswordFormatError:
                    apiResult.Code = ApiResultCode.MobileSetPasswordFormatError;
                    apiResult.Message = "密碼格式錯誤。";
                    break;

                default:
                    apiResult.Code = ApiResultCode.Error;
                    apiResult.Message = "系統繁忙中，請稍後再試!!";
                    break;
            }
            return apiResult;
        }
    }
}

﻿namespace LunchKingSite.WebApi.Core
{
    /// <summary>
    /// 
    /// </summary>
    public interface IApiUserInputModel
    {
        /// <summary>
        /// api user id
        /// </summary>
        string UserId { get; set; }
    }
}
﻿using System;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.BizLogic.Model.OAuth;
using LunchKingSite.BizLogic.Facade;
using log4net;

namespace LunchKingSite.WebApi.Core.OAuth
{
    /// <summary>
    /// 
    /// </summary>
    public class AuthorizeOrLoginAttribute : AuthorizationFilterAttribute
    {
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static ILog logger = LogManager.GetLogger("oauth");
        private static string _LOGIN_PAGE_URL
        {
            get
            {
                return config.OAuthLoginUrl;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="actionContext"></param>
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            OAuthLoginInfo loginInfo = actionContext.Request.GetLoginInfo();

            if (!HttpContext.Current.Request.IsAuthenticated)
            {
                actionContext.Response = CreateRedirectResponse(actionContext, loginInfo.ClientId);
                return;
            }
            
            if (!string.IsNullOrEmpty(loginInfo.ClientId))
            {
                OAuthClientModel client = OAuthFacade.GetClient(loginInfo.ClientId);
                if (client == null || !OAuthFacade.ClientHasUser(client.Id, HttpContext.Current.User.Identity.Name))
                {
                    actionContext.Response = CreateRedirectResponse(actionContext, loginInfo.ClientId);
                    return;
                }
            }
            else
            {
                actionContext.Response = actionContext.Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError
                {
                    Message = ErrorResponse.GetDescription(ErrorResponse.InvalidClientId, loginInfo.ClientId)
                }); ;
                return;
            }

            base.OnAuthorization(actionContext);
        }

        private HttpResponseMessage CreateRedirectResponse(HttpActionContext actionContext, string client_id)
        {
            string redirect_uri = actionContext.Request.GetParams("redirect_uri");

            UriBuilder ubNext = new UriBuilder(actionContext.Request.RequestUri);
            UriBuilder ubCancel = new UriBuilder(HttpUtility.UrlDecode(redirect_uri));
            ubCancel.AppendQueryArgument("error", "access_denied");
            ubCancel.AppendQueryArgument("error_description", "access_denied");
            HttpResponseMessage response = actionContext.Request.CreateResponse(HttpStatusCode.Redirect);
            string loginUrl = string.Format(
                _LOGIN_PAGE_URL + "?app_key={0}&next_url={1}&cancel_url={2}",
                client_id,
                HttpUtility.UrlEncode(ubNext.ToString()),
                HttpUtility.UrlEncode(ubCancel.ToString()));

            logger.Debug("return url(login)-" + loginUrl);
            response.Headers.Location = new Uri(loginUrl);

            return response;
        }
    }
}
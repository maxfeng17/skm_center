﻿using System;
using System.Globalization;
using System.Net.Http;
using System.Web;
using System.Web.Http.Filters;
using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.OAuth;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebApi.Core.OAuth
{
    /// <summary>
    /// 
    /// </summary>
    public class OAuthScopeAttribute : ActionFilterAttribute
    {
        private TokenScope[] _scope;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="scope"></param>
        public OAuthScopeAttribute(params TokenScope[] scope)
        {
            this._scope = scope;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="actionContext"></param>
        public override void OnActionExecuting(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            var oauthLog = LogManager.GetLogger("Oauth");
            string id = "0";
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                id = HttpContext.Current.User.Identity.GetPropertyValue("Id").ToString();
            }
            oauthLog.DebugFormat("OnActionExecuting_{0}_1: OnActionExecuting Start.", id);
            HttpRequestMessage request = actionContext.Request;
            string accessToken = string.Empty;
            var authorizationHeader = request.Headers.Authorization;
            if (authorizationHeader != null &&
                (authorizationHeader.Scheme.Equals("OAuth", StringComparison.OrdinalIgnoreCase) ||
                    authorizationHeader.Scheme.Equals("Bearer", StringComparison.OrdinalIgnoreCase)))
            {
                accessToken = authorizationHeader.Parameter;
                oauthLog.DebugFormat("OnActionExecuting_{0}_2: oauth get from request.Headers.Authorization. token=>{1}", id, accessToken);
            }
            else//Form
            {
                if (HttpContext.Current.Request["access_token"] != null)
                {
                    accessToken = HttpContext.Current.Request["access_token"];
                    oauthLog.DebugFormat("OnActionExecuting_{0}_2: oauth get from Request[access_token]. token=>{1}", id, accessToken);
                }
                else if (HttpContext.Current.Request["accessToken"] != null)
                {
                    accessToken = HttpContext.Current.Request["accessToken"];
                    oauthLog.DebugFormat("OnActionExecuting_{0}_2: oauth get from Request[accessToken]. token=>{1}", id, accessToken);
                }
                else if (HttpContext.Current.Request.RequestContext.RouteData.Values["accessToken"] != null) //RouteData
                {
                    accessToken = HttpContext.Current.Request.RequestContext.RouteData.Values["accessToken"].ToString();
                    oauthLog.DebugFormat("OnActionExecuting_{0}_2: oauth get from RouteData.Values[accessToken]. token=>{1}", id, accessToken);
                }
            }


            OAuthTokenModel oauthToken = LunchKingSite.Core.Helper.GetContextItem(LkSiteContextItem._OAUTH_TOKEN) as OAuthTokenModel;
            ApiResultCode resultCode;
            if (oauthToken != null)
            {
                resultCode = CommonFacade.TokenResultToApiResultCode(CommonFacade.CheckToken(oauthToken, this._scope, false));
                oauthLog.DebugFormat("OnActionExecuting_{0}_3: oauthToken is not null, accessToken -> {1}, TokenResult:{2}", id, accessToken, resultCode);
            }
            else
            {
                OauthToken token = CommonFacade.OauthTokenGet(accessToken);
                resultCode = CommonFacade.TokenResultToApiResultCode(CommonFacade.CheckToken(token, this._scope, true));
                oauthLog.DebugFormat("OnActionExecuting_{0}_3: oauthToken is null, accessToken_1 -> {1}, oauthToken.id -> {2}, oauthToken.accessToken -> {3}, TokenResult:{4}"
                    , id, accessToken, token.Id, token.AccessToken ?? "Null", resultCode);
            }

            oauthLog.DebugFormat("OnActionExecuting_{0}_4: resultCode , accessToken -> {1}, TokenResult:{2}", id, accessToken, resultCode);
            if (resultCode != ApiResultCode.Success)
            {
                throw new HttpErrorResponseException(((int)resultCode).ToString(CultureInfo.InvariantCulture), LunchKingSite.Core.Helper.GetLocalizedEnum(resultCode));
            }
            base.OnActionExecuting(actionContext);
        }
    }
}

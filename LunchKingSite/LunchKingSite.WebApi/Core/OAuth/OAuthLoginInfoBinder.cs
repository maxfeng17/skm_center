﻿using LunchKingSite.BizLogic.Model.OAuth;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.ModelBinding;

namespace LunchKingSite.WebApi.Core.OAuth
{
    /// <summary>
    /// 
    /// </summary>
    public class OAuthLoginInfoBinder : IModelBinder
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="actionContext"></param>
        /// <param name="bindingContext"></param>
        /// <returns></returns>
        public bool BindModel(HttpActionContext actionContext, ModelBindingContext bindingContext)
        {
            OAuthLoginInfo loginInfo;
            if (actionContext.Request.Headers.Authorization == null ||
                Helper.TryDecodeBasicAuthentication(actionContext.Request.Headers.Authorization.ToString(), out loginInfo) == false)
            {
                loginInfo = new OAuthLoginInfo();
                HttpRequestMessage request = actionContext.Request;

                loginInfo.ClientId = request.GetParams("client_id");
                loginInfo.ClientSecret = request.GetParams("client_secret");
            }
            bindingContext.Model = loginInfo;
            return true;
        }
    }
}
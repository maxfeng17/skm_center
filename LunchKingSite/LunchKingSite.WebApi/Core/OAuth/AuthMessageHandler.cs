﻿using System;
using System.Linq;
using System.Text;
using System.Net.Http;
using System.Threading.Tasks;
using System.Threading;
using System.Web;
using System.Security.Principal;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.OAuth;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.WebLib.Component;
using System.Web.Http.Filters;
using LunchKingSite.BizLogic.Component;

namespace LunchKingSite.WebApi.Core.OAuth
{
    /// <summary>
    /// 
    /// </summary>
    public class AuthMessageHandler : DelegatingHandler
    {
        /// <summary>
        /// 
        /// </summary>
        protected ITokenProvider TokenProvider { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        protected IPrincipalProvider PrincipalProvider { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        protected IAppClientProvider AppClientProvider { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="tokenProvider"></param>
        /// <param name="principalProvider"></param>
        /// <param name="appClientProvider"></param>
        public AuthMessageHandler(ITokenProvider tokenProvider, IPrincipalProvider principalProvider, IAppClientProvider appClientProvider)
        {
            TokenProvider = tokenProvider;
            PrincipalProvider = principalProvider;
            AppClientProvider = appClientProvider;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            IIdentity identity = null;
            OAuthTokenModel authToken = null;
            //取得Access Token
            string token = OAuthFacade.ExtractToken(request.Headers.Authorization);

            //檢查Token合法，並取得會員identity
            if (token != null && TokenProvider.Verify(token, out identity, out authToken))
            {
                if (identity != null)
                {
                    //把identity放到request中，供後續的程序使用
                    request.Properties.Add("Identity", identity);
                    var principal = PrincipalProvider.CreatePrincipal(identity);

                    //設定Principal到Thread&HttpContext
                    Thread.CurrentPrincipal = principal;
                    HttpContext.Current.User = principal;
                }

                if (authToken != null && authToken.IsValid)
                {
                    HttpContext.Current.Items["AppId"] = authToken.AppId;
                    HttpContext.Current.Items["AccessToken"] = token;
                }
            }

            string appClientString = ExtractClientId(request);
            string clientId, clientSecret;
            if (!string.IsNullOrWhiteSpace(appClientString) &&
                AppClientProvider.Verify(appClientString, out clientId, out clientSecret))
            {
                if (!string.IsNullOrWhiteSpace(clientId) && !string.IsNullOrWhiteSpace(clientSecret))
                {
                    HttpContext.Current.Items.Add("ClientId", clientId);
                    HttpContext.Current.Items.Add("ClientSecret", clientSecret);
                }
            }            
            return base.SendAsync(request, cancellationToken);
        }
        
        /// <summary>
        /// 取出基於http base auth傳遞的client_id 與 client_secret資料
        /// client_id與client_secret兩參數會以:相連，並經過Base64編碼過，
        /// 字串包含在header中，
        /// Authorization: Basic {client_id:client_secret}
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        private string ExtractClientId(HttpRequestMessage request)
        {
            //Header
            var authorizationHeader = request.Headers.Authorization;
            if (authorizationHeader != null && authorizationHeader.Scheme == "Basic")
            {
                return authorizationHeader.Parameter;
            }
            return null;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    public interface ITokenProvider
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="token"></param>
        /// <param name="identity"></param>
        /// <param name="authToken"></param>
        /// <returns></returns>
        bool Verify(string token, out IIdentity identity, out OAuthTokenModel authToken);
    }
    /// <summary>
    /// 
    /// </summary>
    public class OAuth2TokenProvider : ITokenProvider
    {
        private static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();

        /// <summary>
        /// 判斷Token是否合法
        /// </summary>
        /// <param name="access_token"></param>
        /// <param name="identity"></param>
        /// <param name="authToken"></param>
        /// <returns></returns>
        public bool Verify(string access_token, out IIdentity identity, out OAuthTokenModel authToken)
        {
            identity = null;
            authToken = OAuthFacade.GetTokenByAccessToken(access_token);

            //從access_token取得member
            if (authToken != null && authToken.IsValid && !authToken.IsExpired)
            {
                LunchKingSite.Core.Helper.SetContextItem(LkSiteContextItem._OAUTH_TOKEN, authToken);

                Member member = mp.MemberGet(authToken.UserId.GetValueOrDefault());
                if (member != null && member.IsLoaded)
                {
                    identity = new PponIdentity(member, PponIdentity._USER);
                }
                return true;
            }
            return false;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public interface IPrincipalProvider
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="identity"></param>
        /// <returns></returns>
        IPrincipal CreatePrincipal(IIdentity identity);
    }
    /// <summary>
    /// 
    /// </summary>
    public class OAuth2PrincipalProvider : IPrincipalProvider
    {
        private static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="identity"></param>
        /// <returns></returns>
        public IPrincipal CreatePrincipal(IIdentity identity)
        {
            IPrincipal principal = null;

            if (identity.AuthenticationType == "Token")
            {
                //principal = new GenericPrincipal(identity, new[] {"Token"});
                principal = new PponPrincipal(identity, new[] { "Token" });
            }
            else if (identity.AuthenticationType == "User") //解出會員的角色寫入Principal
            {
                RoleCollection rc = mp.RoleGetList(identity.Name);
                //principal = new GenericPrincipal(identity, rc.Select(a => a.RoleName).ToArray());
                principal = new PponPrincipal(identity, rc.Select(a => a.RoleName).ToArray());
            }

            return principal;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    public interface IAppClientProvider
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="BasicString"></param>
        /// <param name="clientId"></param>
        /// <param name="clientSecret"></param>
        /// <returns></returns>
        bool Verify(string BasicString, out string clientId, out string clientSecret);
    }
    /// <summary>
    /// 
    /// </summary>
    public class OAuth2AppClientProvider : IAppClientProvider
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="basicString"></param>
        /// <param name="clientId"></param>
        /// <param name="clientSecret"></param>
        /// <returns></returns>
        public bool Verify(string basicString, out string clientId, out string clientSecret)
        {
            clientId = string.Empty;
            clientSecret = string.Empty;
            string decodeString = Encoding.GetEncoding("utf-8").GetString(Convert.FromBase64String(basicString));
            string[] clientString = decodeString.Split(":");
            if (clientString != null && clientString.Length == 2)
            {
                clientId = clientString[0];
                clientSecret = clientString[1];
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}

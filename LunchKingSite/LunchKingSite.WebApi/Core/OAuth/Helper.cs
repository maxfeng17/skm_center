﻿using System;
using System.Security.Cryptography;
using System.Text;
using LunchKingSite.BizLogic.Model.OAuth;

namespace LunchKingSite.WebApi.Core.OAuth
{
    /// <summary>
    /// 
    /// </summary>
    public class Helper
    {
        private const string _17LIFE_SERVER_KEY = "@17Life";


        private static string GetMD5(byte[] buff)
        {
            StringBuilder sBuilder = new StringBuilder();
            var data = MD5.Create().ComputeHash(buff);
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();   
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string GetMD5(string s)
        {
            return GetMD5(Encoding.ASCII.GetBytes(s));
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="appId"></param>
        /// <returns></returns>
        public static string GenerateToken(string appId)
        {
            return Guid.NewGuid().ToString("N") + "_" + GetMD5(appId + _17LIFE_SERVER_KEY + DateTime.Now);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="baseAuth"></param>
        /// <param name="loginInfo"></param>
        /// <returns></returns>
        public static bool TryDecodeBasicAuthentication(string baseAuth, out OAuthLoginInfo loginInfo)
        {
            loginInfo = null;
            if (string.IsNullOrEmpty(baseAuth))
            {
                return false;
            }
            string uncodeAuth = Encoding.ASCII.GetString(Convert.FromBase64String(baseAuth.TrimStart("Basic", ignoreCase: true).Trim()));
            string[] parts = uncodeAuth.Split(':');
            if (parts.Length != 2)
            {
                return false;
            }
            loginInfo = new OAuthLoginInfo
            {
                ClientId = parts[0],
                ClientSecret = parts[1]
            };
            return true;
        }
    }
}
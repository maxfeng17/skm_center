﻿using System;
using System.Net;
using System.Net.Http;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.OAuth;
using log4net;


namespace LunchKingSite.WebApi.Core.OAuth
{
    /// <summary>
    /// 
    /// </summary>
    public class ClientValidateAttribute : ActionFilterAttribute
    {
        private static ILog logger = LogManager.GetLogger("oauth");
        /// <summary>
        /// 
        /// </summary>
        public string[] Fields { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool ValidateEnabled { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ClientValidateAttribute() 
        {
            this.ValidateEnabled = true;
            this.Fields = new string[] { "client_id", "redirect_uri", "client_secret" };
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="enabled"></param>
        /// <param name="fields"></param>
        public ClientValidateAttribute(bool enabled, params string[] fields)
        {
            this.ValidateEnabled = enabled;
            this.Fields = fields;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="actionContext"></param>
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            OAuthLoginInfo loginInfo = actionContext.Request.GetLoginInfo();

            string redirect_uri = actionContext.Request.GetParams("redirect_uri");

            OAuthClientModel client = OAuthFacade.GetClient(loginInfo.ClientId);
            if (client == null)
            {
                logger.DebugFormat("client {0} not found", loginInfo.ClientId);

                var response = actionContext.Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError 
                {
                    Message = ErrorResponse.GetDescription(ErrorResponse.InvalidClientId, loginInfo.ClientId)
                });
                actionContext.Response = response;
                return;
            }

            //ValidateEnabled
            if (this.ValidateEnabled && client.Enabled == false)
            {
                var response = actionContext.Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError
                {
                    Message = ErrorResponse.GetDescription(ErrorResponse.InactiveClient, loginInfo.ClientId)
                });
                actionContext.Response = response;
                return;
            }

            //驗證client_secret
            if (this.Fields.Contains("client_secret") && client.AppSecret != loginInfo.ClientSecret)
            {
                var response = actionContext.Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError
                {
                    Message = ErrorResponse.GetDescription(ErrorResponse.InvalidClientCredentials, loginInfo.ClientId)
                });
                actionContext.Response = response;
                return;
            }

            //驗證redirect_uri
            if (this.Fields.Contains("redirect_uri") && (client.ReturnUrl == "" || HttpUtility.UrlDecode(redirect_uri).StartsWith(client.ReturnUrl, StringComparison.OrdinalIgnoreCase) == false))
            {
                logger.InfoFormat("client {0} redirect_uri not math. {1} not in {2}",
                    client.ReturnUrl, HttpUtility.UrlDecode(redirect_uri));
                var response = actionContext.Request.CreateErrorResponse(HttpStatusCode.BadRequest, new HttpError
                {
                    Message = ErrorResponse.GetDescription(ErrorResponse.InvalidReturnUrl)
                });
                actionContext.Response = response;
                return;
            }

            base.OnActionExecuting(actionContext);
        }
    }
}

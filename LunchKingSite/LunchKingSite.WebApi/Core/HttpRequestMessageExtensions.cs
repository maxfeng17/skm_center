﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using LunchKingSite.BizLogic.Model.OAuth;
using LunchKingSite.WebApi.Core.OAuth;
using System.Web;


namespace LunchKingSite.WebApi.Core
{
    /// <summary>
    /// Extends the HttpRequestMessage collection
    /// </summary>
    public static class HttpRequestMessageExtensions
    {
        /// <summary>
        /// Returns a dictionary of QueryStrings that's easier to work with 
        /// than GetQueryNameValuePairs KevValuePairs collection.
        /// 
        /// If you need to pull a few single values use GetQueryString instead.
        /// ref http://weblog.west-wind.com/posts/2013/Apr/15/WebAPI-Getting-Headers-QueryString-and-Cookie-Values
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static Dictionary<string, string> GetQueryStrings(this HttpRequestMessage request)
        {
            return request.GetQueryNameValuePairs()
                          .ToDictionary(kv => kv.Key, kv=> kv.Value, StringComparer.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Returns an individual querystring value
        /// </summary>
        /// <param name="request"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetQueryString(this HttpRequestMessage request, string key)
        {      
            // IEnumerable<KeyValuePair<string,string>> - right!
            var queryStrings = request.GetQueryNameValuePairs();
            if (queryStrings == null)
                return null;

            var match = queryStrings.FirstOrDefault(kv => string.Compare(kv.Key, key, true) == 0);
            if (string.IsNullOrEmpty(match.Value))
                return null;

            return match.Value;
        }

        private static readonly string MultipleBodyParameters = "MultipleBodyParameters";

        private static NameValueCollection TryReadBody(HttpRequestMessage request)
        {
            object result = null;

            // try to read out of cache first
            if (!request.Properties.TryGetValue(MultipleBodyParameters, out result))
            {
                var contentType = request.Content.Headers.ContentType;

                // only read if there's content and it's form data
                if (contentType == null || contentType.MediaType != "application/x-www-form-urlencoded")
                {
                    // Nope no data
                    result = null;
                }
                else
                {
                    // parsing the string like firstname=Hongmei&lastname=ASDASD            
                    string querystring = request.Content.ReadAsStringAsync().Result;
                    result = HttpUtility.ParseQueryString(querystring);
                    //result = request.Content.ReadAsFormDataAsync().Result;
                }

                request.Properties.Add(MultipleBodyParameters, result);
            }

            return result as NameValueCollection;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetParams(this HttpRequestMessage request, string key)
        {
            string stringValue = null;
            NameValueCollection col = TryReadBody(request);
            if (col != null)
            {
                stringValue = col[key];
            }

            // try reading query string if we have no POST/PUT match
            if (stringValue == null)
            {
                var query = request.GetQueryNameValuePairs();
                if (query != null)
                {
                    var matches = query.Where(kv => kv.Key.ToLower() == key.ToLower());
                    if (matches.Count() > 0)
                        stringValue = matches.First().Value;
                }
            }

            return stringValue;
        }


        /// <summary>
        /// Returns an individual HTTP Header value
        /// </summary>
        /// <param name="request"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetHeader(this HttpRequestMessage request, string key)
        {
            IEnumerable<string> keys = null;
            if (!request.Headers.TryGetValues(key, out keys))
                return null;

            return keys.First();
        }

        /// <summary>
        /// Retrieves an individual cookie from the cookies collection
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cookieName"></param>
        /// <returns></returns>
        public static string GetCookie(this HttpRequestMessage request, string cookieName)
        {
            CookieHeaderValue cookie = request.Headers.GetCookies(cookieName).FirstOrDefault();
            if (cookie != null)
                return cookie[cookieName].Value;

            return null;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public static OAuthLoginInfo GetLoginInfo(this HttpRequestMessage request)
        {
            OAuthLoginInfo loginInfo;

            if (request.Headers.Authorization == null ||
    Helper.TryDecodeBasicAuthentication(request.Headers.Authorization.ToString(), out loginInfo) == false)
            {
                loginInfo = new OAuthLoginInfo();
                loginInfo.ClientId = request.GetParams("client_id");
                loginInfo.ClientSecret = request.GetParams("client_secret");
            }

            return loginInfo;
        }
    }
}

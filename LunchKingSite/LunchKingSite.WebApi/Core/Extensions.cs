﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace LunchKingSite.WebApi.Core
{
    /// <summary>
    /// 
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="collection"></param>
        /// <returns></returns>
        public static IEnumerable<KeyValuePair<string, string>> ToPairs(this NameValueCollection collection)
        {
            if (collection == null)
            {
                throw new ArgumentNullException("collection");
            }

            return collection.Cast<string>().Select(key => new KeyValuePair<string, string>(key, collection[key]));
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="str"></param>
        /// <param name="removePart"></param>
        /// <param name="ignoreCase"></param>
        /// <returns></returns>
        public static string TrimStart(this string str, string removePart, bool ignoreCase)
        {
            int pos;
            if (ignoreCase)
            {
                pos = str.IndexOf(removePart, StringComparison.OrdinalIgnoreCase);
            }
            else
            {
                pos = str.IndexOf(removePart);
            }
            if (pos == 0)
            {
                return str.Substring(removePart.Length);
            }
            return str;
        }

        /// <summary>
        /// 	Trims the text to a provided maximum length.
        /// </summary>
        /// <param name = "value">The input string.</param>
        /// <param name = "maxLength">Maximum length.</param>
        /// <returns></returns>
        /// <remarks>
        /// 	Proposed by Rene Schulte
        /// </remarks>
        public static string TrimToMaxLength(this string value, int maxLength)
        {
            return (value == null || value.Length <= maxLength ? value : value.Substring(0, maxLength));
        }

        /// <summary>
        /// 	Trims the text to a provided maximum length and adds a suffix if required.
        /// </summary>
        /// <param name = "value">The input string.</param>
        /// <param name = "maxLength">Maximum length.</param>
        /// <param name = "suffix">The suffix.</param>
        /// <returns></returns>
        /// <remarks>
        /// 	Proposed by Rene Schulte
        /// </remarks>
        public static string TrimToMaxLength(this string value, int maxLength, string suffix)
        {
            return (value == null || value.Length <= maxLength ? value : string.Concat(value.Substring(0, maxLength), suffix));
        }
    }
    /// <summary>
    /// 
    /// </summary>
    public static class UriBuilderExtension
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="name"></param>
        /// <param name="value"></param>
        public static void AppendQueryArgument(this UriBuilder builder, string name, string value)
        {
            if (string.IsNullOrEmpty(builder.Query))
            {
                builder.Query = string.Format("{0}={1}", name, HttpUtility.UrlEncode(value));
            }
            else
            {
                string qry = builder.Query.TrimStart('?');
                if (string.IsNullOrEmpty(qry))
                {
                    builder.Query = string.Format("{0}={1}", name, HttpUtility.UrlEncode(value));
                }
                else
                {
                    builder.Query = string.Format("{0}&{1}={2}", qry, name, HttpUtility.UrlEncode(value));
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="argStr"></param>
        public static void AppendQueryArgument(this UriBuilder builder, string argStr)
        {
            if (string.IsNullOrEmpty(builder.Query))
            {
                builder.Query = argStr;
            }
            else
            {
                string qry = builder.Query.TrimStart('?');
                builder.Query = string.Format("{0}{1}", qry, argStr);
            }
        }
    }
    /// <summary>
    /// 
    /// </summary>
    public static class NameValueCollectionExtension
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nvCol"></param>
        /// <param name="querystring"></param>
        public static void LoadFromQueryString(this NameValueCollection nvCol, string querystring)
        {
            nvCol.Clear();
            string[] args = querystring.Split('&');
            foreach (string arg in args)
            {
                string[] item = arg.Split('=');
                if (item.Length == 2)
                {
                    nvCol.Add(item[0], HttpUtility.UrlDecode(item[1]));
                }
                else if (item.Length == 1)
                {
                    nvCol.Add(item[0], "");
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nvCol"></param>
        /// <param name="prefix"></param>
        /// <param name="sep"></param>
        /// <returns></returns>
        public static string ToString(this NameValueCollection nvCol, string prefix, string sep)
        {
            return ToString(nvCol, prefix, sep, true);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="nvCol"></param>
        /// <param name="prefix"></param>
        /// <param name="sep"></param>
        /// <param name="enocde"></param>
        /// <returns></returns>
        public static string ToString(this NameValueCollection nvCol, string prefix, string sep, bool enocde)
        {
            StringBuilder sb = new StringBuilder();
            foreach (string key in nvCol.Keys)
            {
                if (sb.Length > 0)
                {
                    sb.Append(sep);
                }
                sb.Append(key);
                sb.Append('=');
                sb.Append(HttpUtility.UrlEncode(nvCol[key]));
            }
            if (string.IsNullOrEmpty(prefix) == false)
            {
                sb.Insert(0, prefix);
            }
            return sb.ToString();
        }
    }
    /// <summary>
    /// 
    /// </summary>
    public static class StreamExtensions
    {
        /// <summary>
        /// 	Opens a StreamReader using the default encoding.
        /// </summary>
        /// <param name = "stream">The stream.</param>
        /// <returns>The stream reader</returns>
        public static StreamReader GetReader(this Stream stream)
        {
            return stream.GetReader(null);
        }

        /// <summary>
        /// 	Opens a StreamReader using the specified encoding.
        /// </summary>
        /// <param name = "stream">The stream.</param>
        /// <param name = "encoding">The encoding.</param>
        /// <returns>The stream reader</returns>
        public static StreamReader GetReader(this Stream stream, Encoding encoding)
        {
            if (stream.CanRead == false)
                throw new InvalidOperationException("Stream does not support reading.");

            encoding = (encoding ?? Encoding.Default);
            return new StreamReader(stream, encoding);
        }
        /// <summary>
        /// 	Reads all text from the stream using the default encoding.
        /// </summary>
        /// <param name = "stream">The stream.</param>
        /// <returns>The result string.</returns>
        public static string ReadToEnd(this Stream stream)
        {
            return stream.ReadToEnd(null);
        }

        /// <summary>
        /// 	Reads all text from the stream using a specified encoding.
        /// </summary>
        /// <param name = "stream">The stream.</param>
        /// <param name = "encoding">The encoding.</param>
        /// <returns>The result string.</returns>
        public static string ReadToEnd(this Stream stream, Encoding encoding)
        {
            using (var reader = stream.GetReader(encoding))
                return reader.ReadToEnd();
        }
    }
}
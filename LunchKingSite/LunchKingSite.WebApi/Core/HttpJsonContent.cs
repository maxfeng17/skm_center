using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace LunchKingSite.WebApi.Core
{
    /// <summary>
    /// 
    /// </summary>
    public class HttpJsonContent : HttpContent
    {
        private readonly MemoryStream contentStream = new MemoryStream();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        public HttpJsonContent(object value)
        {

            Headers.ContentType = new MediaTypeHeaderValue("application/json");
            var jw = new JsonTextWriter(new StreamWriter(contentStream));
            jw.Formatting = Formatting.Indented;
            Newtonsoft.Json.JsonSerializer serializer = new Newtonsoft.Json.JsonSerializer();
            serializer.Serialize(jw, value);
            jw.Flush();
            contentStream.Position = 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        protected override Task SerializeToStreamAsync(Stream stream, TransportContext context)
        {
            return this.contentStream.CopyToAsync(stream);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="length"></param>
        /// <returns></returns>
        protected override bool TryComputeLength(out long length)
        {
            length = contentStream.Length;
            return true;
        }
    }
}
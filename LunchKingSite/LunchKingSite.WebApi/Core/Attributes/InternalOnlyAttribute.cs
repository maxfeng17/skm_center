using System;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Configuration;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace LunchKingSite.WebApi.Core.Attributes
{
    /// <summary>
    /// 
    /// </summary>
    public class InternalOnlyAttribute : AuthorizationFilterAttribute
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="actionContext"></param>
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            if (!IsIntranet(HttpContext.Current.Request.UserHostAddress))
            {
                throw new HttpException((int)HttpStatusCode.Forbidden, "Access forbidden. -" + HttpContext.Current.Request.UserHostAddress);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userIP"></param>
        /// <returns></returns>
        private bool IsIntranet(string userIP)
        {
            IPAddress ipAddress;
            IPAddress.TryParse(userIP, out ipAddress);

            // If we got an IPV6 address, then we need to ask the network for the IPV4 address 
            // This usually only happens when the browser is on the same machine as the server.
            if (ipAddress.AddressFamily == System.Net.Sockets.AddressFamily.InterNetworkV6)
            {
                ipAddress = Dns.GetHostEntry(ipAddress).AddressList
                    .First(x => x.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork);
            }

            userIP = ipAddress.ToString();

            //可能要寫成設定比較好，10.15.0.102是iceman的ip
            return userIP != null && (userIP == "::1" || userIP == "127.0.0.1" || userIP == "10.15.0.89"
                || userIP.StartsWith("192.168"));
        }
    }

    /// <summary>
    /// 限制只能從17Life總部的IP呼叫
    /// </summary>
    public class DeveloperOnlyAttribute : AuthorizationFilterAttribute
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="actionContext"></param>
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            if (!IsAllowIp(HttpContext.Current.Request.UserHostAddress))
            {
                throw new HttpException((int)HttpStatusCode.Forbidden, "Access forbidden. -" + HttpContext.Current.Request.UserHostAddress);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userIP"></param>
        /// <returns></returns>
        private bool IsAllowIp(string userIP)
        {
            string devIp = WebConfigurationManager.AppSettings["DeveloperIp"];
            return string.Equals(devIp, userIP);
        }
    }

    /// <summary>
    /// 限制該API，只有在iceman, rb 上能執行，通常是開發中測試使用
    /// </summary>
    public class TestServerOnlyAttribute : AuthorizationFilterAttribute
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="actionContext"></param>
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            if (Environment.MachineName.Equals("contact-PC", StringComparison.OrdinalIgnoreCase) == false 
                && Environment.MachineName.Equals("BEAST", StringComparison.OrdinalIgnoreCase) == false
                && Environment.MachineName.Equals("Iceman", StringComparison.OrdinalIgnoreCase) == false)
            {
                throw new HttpException((int)HttpStatusCode.Forbidden, "Access forbidden. -" + HttpContext.Current.Request.UserHostAddress);
            }
        }
    }
}
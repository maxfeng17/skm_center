using System;
using System.Web.Http.Controllers;

namespace LunchKingSite.WebApi.Core.Attributes
{
    /// <summary>
    /// 不要再用了 by unicorn
    /// </summary>
    public class MvcStyleBindingAttribute : Attribute, IControllerConfiguration
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="controllerSettings"></param>
        /// <param name="controllerDescriptor"></param>
        public void Initialize(HttpControllerSettings controllerSettings, HttpControllerDescriptor controllerDescriptor)
        {
            controllerSettings.Services.Replace(typeof(IActionValueBinder), new MvcActionValueBinder());
        }
    }
}

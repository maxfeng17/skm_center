﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;

namespace LunchKingSite.WebApi.Core.Attributes
{
    /// <summary>
    /// 
    /// </summary>
    public class MemberValidateAttribute : ActionFilterAttribute
    {
        private static ILog logger = LogManager.GetLogger("oauth");
        /// <summary>
        /// 
        /// </summary>
        public string[] Fields { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool ValidateEnabled { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public MemberValidateAttribute() 
        {
            this.ValidateEnabled = true;
            this.Fields = new string[] { "userId", "userName", "orderGuid", "bid" };
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="enabled"></param>
        /// <param name="fields"></param>
        public MemberValidateAttribute(bool enabled, params string[] fields)
        {
            this.ValidateEnabled = enabled;
            this.Fields = fields;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="actionContext"></param>
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (!this.ValidateEnabled)
            {
                base.OnActionExecuting(actionContext);
                return;
            }

            var jsonS = new JsonSerializer();
            ApiResult result = new ApiResult() { Code = ApiResultCode.Success };

            //驗證            
            if (this.Fields.Contains("userId"))
            {
                string userId = actionContext.Request.GetParams("userId") ?? actionContext.ActionArguments["userId"] as string;

                if (string.IsNullOrWhiteSpace(userId) || ApiUserManager.ApiUserGetByUserId(userId) == null)
                {
                    result.Code = ApiResultCode.InputError;
                    result.Message = I18N.Phrase.ApiResultCodeInputError;
                    actionContext.Response = CreateErrorResponse(actionContext.Request, HttpStatusCode.OK, jsonS.Serialize(result));
                    return;
                }
            }

            if (this.Fields.Contains("userName"))
            {
                //取得會員名稱
                string userName = actionContext.Request.GetParams("userName") ?? actionContext.ActionArguments["userName"] as string;

                if (string.IsNullOrWhiteSpace(userName))
                {
                    userName = HttpContext.Current.User.Identity.Name;
                }

                if (string.IsNullOrWhiteSpace(userName))
                {
                    result.Code = ApiResultCode.UserNoSignIn;
                    result.Message = I18N.Phrase.ApiResultCodeUserNoSignIn;
                    actionContext.Response = CreateErrorResponse(actionContext.Request, HttpStatusCode.OK, jsonS.Serialize(result));
                    return;
                }

            }

            if (this.Fields.Contains("orderGuid"))
            {
                string orderGuid = actionContext.Request.GetParams("orderGuid") ?? actionContext.ActionArguments["orderGuid"] as string;
                Guid oGuid;
                if (!Guid.TryParse(orderGuid, out oGuid))
                {
                    result.Code = ApiResultCode.InputError;
                    result.Message = I18N.Phrase.ApiResultCodeInputError;
                    actionContext.Response = CreateErrorResponse(actionContext.Request, HttpStatusCode.OK, jsonS.Serialize(result));
                    return;
                }
            }

            if (this.Fields.Contains("bid"))
            {
                string bid = actionContext.Request.GetParams("bid") ?? actionContext.ActionArguments["bid"] as string;
                Guid bGuid;
                if (!Guid.TryParse(bid, out bGuid))
                {
                    result.Code = ApiResultCode.InputError;
                    result.Message = I18N.Phrase.ApiResultCodeInputError;
                    actionContext.Response = CreateErrorResponse(actionContext.Request, HttpStatusCode.OK, jsonS.Serialize(result));
                    return;
                }
            }

            base.OnActionExecuting(actionContext);
        }

        private HttpResponseMessage CreateErrorResponse(HttpRequestMessage request, HttpStatusCode statusCode, string message)
        {
            return request.CreateResponse(statusCode, message, "application/json");
        }

    }
}

﻿using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http.Filters;
using LunchKingSite.BizLogic.Model.API;
using Newtonsoft.Json.Serialization;

namespace LunchKingSite.WebApi.Core.Attributes
{
    /// <summary>
    /// 給 SKM 用的, 回傳 unhandled exception
    /// </summary>
    public class SkmExceptionHandlerAttribute : ExceptionFilterAttribute
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public override void OnException(HttpActionExecutedContext context)
        {
            var response = new HttpResponseMessage(HttpStatusCode.OK);                
            HttpErrorResponseException httpErrorResponseException = context.Exception as HttpErrorResponseException;
            ApiResult apiResult;
            if (httpErrorResponseException != null)
            {
                apiResult = new ApiResult()
                {
                    Code = httpErrorResponseException.Code,
                    Message = httpErrorResponseException.Message,                    
                };
            }
            else
            {
                apiResult = new ApiResult()
                {
                    Message = context.Exception.Message,
                    Code = LunchKingSite.Core.ApiResultCode.Error
                };                
            }
            //new formatter
            var camelFormatter = new JsonMediaTypeFormatter
            {
                Indent = true,
                SerializerSettings = { ContractResolver = new CamelCasePropertyNamesContractResolver() }
            };

            response.Content = new ObjectContent<ApiResult>(apiResult, camelFormatter);
            context.Response = response;
        }
    }
}

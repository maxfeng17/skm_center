﻿using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebApi.Core.Attributes
{
    /// <summary>
    /// 對 IApiUserInputModel 作 UserId(App)的驗證
    /// </summary>
    public class ApiUserValidatorAttribute : ActionFilterAttribute
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="actionContext"></param>
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            string userId = null;
            foreach (var valObj in actionContext.ActionArguments.Values)
            {
                IApiUserInputModel apiUserInputModel = valObj as IApiUserInputModel;
                if (apiUserInputModel != null)
                {
                    userId = apiUserInputModel.UserId;
                    break;
                }
            }
            if (userId == null && actionContext.ActionArguments.ContainsKey("userId"))
            {
                userId = actionContext.ActionArguments["userId"] as string;
            }
            if (string.IsNullOrWhiteSpace(userId))
            {
                ApiResult result = new ApiResult
                {
                    Code = ApiResultCode.InputError,
                    Message = I18N.Phrase.ApiReturnCodeApiUserIdError
                };
                actionContext.Response = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new HttpJsonContent(result)
                };
                return;
            }
            ApiUser apiUser = ApiUserManager.ApiUserGetByUserId(userId);
            if (apiUser == null)
            {
                ApiResult result = new ApiResult
                {
                    Code = ApiResultCode.InputError,
                    Message = I18N.Phrase.ApiReturnCodeApiUserIdError
                };
                actionContext.Response = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new HttpJsonContent(result)
                };
                return;
            }
            Helper.SetContextItem(LkSiteContextItem._API_USER_ID, apiUser.UserId);
            Helper.SetContextItem(LkSiteContextItem._DEVICE_TYPE, apiUser.UserDeviceType);
            base.OnActionExecuting(actionContext);
        }
    }
}
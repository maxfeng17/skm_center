﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace LunchKingSite.WebApi.Core.Attributes
{
    /// <summary>
    /// 強制產生的Json以CamelCase格式，
    /// </summary>
    public class ForceCamelCaseAttribute : Attribute, IControllerConfiguration
    {
        /// <summary>
        /// 
        /// </summary>CamelCaseJsonFormatter
        /// <param name="currentConfiguration"></param>
        /// <param name="currentDescriptor"></param>
        public void Initialize(HttpControllerSettings currentConfiguration, HttpControllerDescriptor currentDescriptor)
        {
            JsonMediaTypeFormatter currentFormatter = currentConfiguration.Formatters.OfType<JsonMediaTypeFormatter>().Single();
            //remove the current formatter
            currentConfiguration.Formatters.Remove(currentFormatter);

            //add the camel case formatter
            currentConfiguration.Formatters.Add(new CamelCaseJsonFormatter());
        }        
    }
    /// <summary>
    /// 當 Controller 宣告成 ForceCamelCase ，其中又有部份的Action 要回傳 PascalCase，型式的JSON，可以用此屬性做設定。
    /// </summary>
    public class ForcePascalCaseAttribute : ActionFilterAttribute
    {
        /// <summary>
        /// 
        /// </summary>
        public const string _USE_PASCAL_CASE = "UsePascalCase";
        /// <summary>
        /// 
        /// </summary>
        /// <param name="actionContext"></param>
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            base.OnActionExecuting(actionContext);
            if (actionContext.Request.Properties.ContainsKey(_USE_PASCAL_CASE) == false)
            {
                actionContext.Request.Properties.Add(_USE_PASCAL_CASE, true);
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class CamelCaseJsonFormatter : JsonMediaTypeFormatter
    {
        private HttpRequestMessage originalRequest;

        /// <summary>
        /// 
        /// </summary>
        public CamelCaseJsonFormatter()
        {
            this.Indent = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="request"></param>
        /// <param name="mediaType"></param>
        /// <returns></returns>
        //public MediaTypeFormatter GetPerRequestFormatterInstance(
        //  Type type,
        //  HttpRequestMessage request,
        //  MediaTypeHeaderValue mediaType)
        //{
        //    originalRequest = request;
        //    return base.GetPerRequestFormatterInstance(type, request, mediaType);
        //}

        public override MediaTypeFormatter GetPerRequestFormatterInstance(Type type, HttpRequestMessage request, MediaTypeHeaderValue mediaType)
        {
            originalRequest = request;
            return base.GetPerRequestFormatterInstance(type, request, mediaType);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="value"></param>
        /// <param name="writeStream"></param>
        /// <param name="content"></param>
        /// <param name="transportContext"></param>
        /// <returns></returns>
        public override Task WriteToStreamAsync(Type type, object value, Stream writeStream,
          HttpContent content, TransportContext transportContext)
        {
            if (originalRequest != null && originalRequest.Properties.ContainsKey(ForcePascalCaseAttribute._USE_PASCAL_CASE))
            {
                this.SerializerSettings = new JsonSerializerSettings
                {
                    ContractResolver = new DefaultContractResolver()
                };
            }
            else
            {
                this.SerializerSettings = new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                };
            }

            return base.WriteToStreamAsync(type, value, writeStream, content, transportContext);
        }
    }


}
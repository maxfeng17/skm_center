using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;

namespace LunchKingSite.WebApi.Core
{
    /// <summary>
    /// 
    /// </summary>
    public class HttpErrorResponseException : HttpResponseException
    {
        /// <summary>
        /// 
        /// </summary>
        public ApiResultCode Code { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        /// <param name="error"></param>
        public HttpErrorResponseException(string code, string error) :            
            base(new HttpResponseMessage()
            {
                Content = new ObjectContent<object>(
                    new {Code = code, Error = error, Data = "", Message = error}, 
                    new JsonMediaTypeFormatter())
            })
        {
            //TODO 有A轉B ，B再轉A的情況
            ApiResultCode codeValue;
            if (System.Enum.TryParse(code, out codeValue))
            {
                this.Code = codeValue;
            }
            else
            {
                this.Code = ApiResultCode.Error;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        /// <param name="error"></param>
        public HttpErrorResponseException(ApiResultCode code, string error) :
            base(new HttpResponseMessage()
            {
                Content = new ObjectContent<object>(
                    new ApiResult { Code = code, Data = "", Message = error }, new JsonMediaTypeFormatter())
            })
        {
            this.Code = code;
        }
    }
}